<?php
class Competitionmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function get_all_competitions() {
		$this->db->select('wf_competition.id, wf_competition.festival_id, wf_festival.year as festival_year, wf_festival.name as festival_name, wf_competition.slug, wf_competition.name, wf_competition.instructions');
		$this->db->from('wf_competition');
		$this->db->join('wf_festival', 'wf_festival.id = wf_competition.festival_id');
		$this->db->order_by('festival_id','desc');
		$query = $this->db->get();

		return $query->result();
	}

	function get_competition($competition_id) {
		$this->db->select('wf_competition.id, wf_competition.festival_id, wf_festival.year as festival_year, wf_festival.name as festival_name, wf_competition.slug, wf_competition.name, wf_competition.instructions');
		$this->db->from('wf_competition');
		$this->db->join('wf_festival', 'wf_festival.id = wf_competition.festival_id');
		$this->db->where('wf_competition.id', $competition_id);
		$query = $this->db->get();

		return $query->result();
	}

	function get_competition_by_slug($slug) {
		$this->db->select('wf_competition.id, wf_competition.festival_id, wf_festival.year as festival_year, wf_festival.name as festival_name, wf_competition.slug, wf_competition.name, wf_competition.instructions');
		$this->db->from('wf_competition');
		$this->db->join('wf_festival', 'wf_festival.id = wf_competition.festival_id');
		$this->db->where('wf_competition.slug', $slug);
		$query = $this->db->get();

		return $query->result();
	}

	function get_all_competition_films() {
		$this->db->select('wf_competition_films.id, competition_id, movie_id, video_url, video_password, wf_competition.name, wf_competition.festival_id');
		$this->db->from('wf_competition_films');
		$this->db->join('wf_competition', 'wf_competition.id = wf_competition_films.competition_id');
		$this->db->order_by('wf_competition.festival_id','desc');
		$query = $this->db->get();

		return $query->result();
	}

	function get_competition_films($competition_id) {
		$this->db->select('wf_competition_films.id, wf_competition_films.competition_id, wf_competition_films.movie_id, wf_movie.title_en, wf_movie.year, wf_movie.runtime_int, wf_movie.synopsis, wf_movie.synopsis_short, wf_competition_films.video_url, wf_competition_films.video_password');
		$this->db->from('wf_competition_films');
		$this->db->join('wf_movie', 'wf_movie.id = wf_competition_films.movie_id');
		$this->db->where('wf_competition_films.competition_id', $competition_id);
		$this->db->order_by('wf_movie.title_en','asc');
		$query = $this->db->get();

		return $query->result();
	}

	function get_all_competition_votes() {
		$this->db->select('wf_competition_votes.competition_id, wf_competition_votes.name, wf_competition_votes.ranking, wf_competition_votes.notes');
		$this->db->from('wf_competition_votes');
		$this->db->order_by('wf_competition_votes.name','asc');
		$query = $this->db->get();

		return $query->result();
	}

}
?>