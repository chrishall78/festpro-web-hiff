<?php
class Apiv1model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_films($festival_id, $timestamp = 0)
    {
		// Exclude: Receptions, Press Conferences
		//$eventTypeArray = array("6","14");

		// loads the full (public) schedule of films from the specified festival. + section name, premiere name, event type name, director name
		$this->db->select('wf_movie.id as film_id, wf_movie.slug, wf_movie.title as titleRomanized,wf_movie.title_en as titleAlpha,wf_movie.title_en as titleEnglish,wf_movie.year,wf_movie.runtime_int as runtime,wf_category.name as section, wf_type_event.name as eventType, wf_type_premiere.name as premiereType, wf_festivalmovie.id as countries, wf_festivalmovie.id as languages, wf_festivalmovie.id as genres, wf_festivalmovie.id as directorName, wf_movie.id as imageUrl, wf_movie.synopsis as synopsisLong, wf_movie.synopsis_short as synopsisShort, wf_movie.synopsis_original as synopsisOriginal');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->join('wf_type_premiere', 'wf_type_premiere.id = wf_festivalmovie.premiere_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		//$this->db->where_not_in('wf_type_event.id',$eventTypeArray);
		if ($timestamp != 0) {
			$this->db->where('wf_movie.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$this->db->order_by('wf_movie.title_en', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_screenings($start_date, $end_date, $timestamp = 0)
    {
		// loads all public, confirmed and published screening times from the passed start and end dates.
		$this->db->select('wf_screening.id as screening_id, wf_screening.movie_id as film_id, wf_screening.program_name as programName, wf_screening.program_movie_ids as films, wf_screening.program_movie_ids as filmIds, wf_screening.date as dateString, wf_screening.date as dateTime, wf_screening.time, wf_screening.url as ticketLink, wf_screening.Rush as rush, wf_screening.Free as free, wf_screeninglocation.displayname as location, wf_screening.total_runtime as totalRuntime');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		if ($timestamp != 0) {
			$this->db->where('wf_screening.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_all_personnel($movie_id_array, $timestamp = 0)
	{
		$this->db->select('wf_movie_personnel.movie_id as film_id, wf_movie_personnel.personnel_roles as role, wf_personnel.name as firstName, wf_personnel.lastname as lastName');
		$this->db->from('wf_movie_personnel');
		$this->db->join('wf_type_personnel', 'wf_movie_personnel.type_id = wf_type_personnel.id');
		$this->db->join('wf_personnel', 'wf_personnel.id = wf_movie_personnel.personnel_id');
		if (count($movie_id_array) > 0) {
			$this->db->where_in('wf_movie_personnel.movie_id', $movie_id_array);
		} else {
			$this->db->where_in('wf_movie_personnel.movie_id', array('99999'));			
		}
		if ($timestamp != 0) {
			$this->db->where('wf_movie_personnel.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$this->db->order_by('wf_type_personnel.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_all_sponsors($movie_id_array, $timestamp = 0)
	{
		$this->db->select('wf_sponsor_logos.movie_id as film_id, wf_type_sponsorlogo.name AS sponsorlogo_type, wf_type_sponsorlogo.description, wf_sponsor_logos.url_website, wf_sponsor_logos.url_logo, wf_sponsor.name');
		$this->db->from('wf_sponsor_logos');
		$this->db->join('wf_sponsor', 'wf_sponsor_logos.sponsor_id = wf_sponsor.id');
		$this->db->join('wf_type_sponsorlogo', 'wf_type_sponsorlogo.id = wf_sponsor_logos.sponsorlogo_id');
		$this->db->join('wf_movie', 'wf_sponsor_logos.movie_id = wf_movie.id');
		if (count($movie_id_array) > 0) {
			$this->db->where_in('wf_sponsor_logos.movie_id', $movie_id_array);
		} else {
			$this->db->where_in('wf_sponsor_logos.movie_id', array('99999'));			
		}
		if ($timestamp != 0) {
			$this->db->where('wf_sponsor_logos.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$this->db->order_by('wf_type_sponsorlogo.order','asc');
		$this->db->order_by('wf_sponsor.name','asc');
		$this->db->order_by('wf_movie.title_en','asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_photos($movie_id_array, $timestamp = 0)
    {
		$this->db->select('movie_id as film_id, order, url_cropxlarge as photo_xlarge, url_croplarge as photo_large, url_cropsmall as photo_small');
		$this->db->from('wf_movie_photos');
		if (count($movie_id_array) > 0) {
			$this->db->where_in('wf_movie_photos.movie_id', $movie_id_array);
		} else {
			$this->db->where_in('wf_movie_photos.movie_id', array('99999'));			
		}
		$this->db->order_by('wf_movie_photos.movie_id', 'asc');
		$this->db->order_by('wf_movie_photos.order', 'asc');
		if ($timestamp != 0) {
			$this->db->where('wf_movie_photos.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_videos($movie_id_array, $timestamp = 0)
    {
		$this->db->select('movie_id as film_id, order, service_name, url_video');
		$this->db->from('wf_movie_videos');
		if (count($movie_id_array) > 0) {
			$this->db->where_in('wf_movie_videos.movie_id', $movie_id_array);
		} else {
			$this->db->where_in('wf_movie_videos.movie_id', array('99999'));			
		}
		$this->db->order_by('wf_movie_videos.order', 'asc');
		if ($timestamp != 0) {
			$this->db->where('wf_movie_videos.updated_date >=', date("Y-m-d H:i:s",$timestamp));
		}
		$query = $this->db->get();

        return $query->result();
	}

}
?>