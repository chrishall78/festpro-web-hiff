<?php
class Hotelmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_hotels($guest_list, $order = "asc")
    {
		// loads all flights for hotels of a specific festival.
		$this->db->select('*');
		$this->db->from('fg_accomodations');
		$this->db->where_in('festivalguest_id',$guest_list);
		if ($order == "asc") {
			$this->db->order_by('fg_accomodations.checkin', 'asc'); 
			$this->db->order_by('fg_accomodations.checkout', 'asc'); 
		} else {
			$this->db->order_by('fg_accomodations.checkin', 'desc'); 
			$this->db->order_by('fg_accomodations.checkout', 'desc'); 
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guest_hotels($guest_id)
    {
		// loads all hotels for the specified guest.
		$this->db->select('*');
		$this->db->from('fg_accomodations');
		$this->db->where('festivalguest_id',$guest_id);
		$this->db->order_by('fg_accomodations.checkin', 'asc'); 
		$this->db->order_by('fg_accomodations.checkout', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_hotel_comps_all()
    {
		// loads all hotel comp types for the specified festival.
		$this->db->select('fg_hotelcompdeals.id, fg_hotelcompdeals.festival_id, fg_hotelcompdeals.name, fg_hotelcompdeals.hotel, fg_hotelcompdeals.roomtype, fg_hotelcompdeals.limit');
		$this->db->from('fg_hotelcompdeals');
		$this->db->join('wf_festival', 'wf_festival.id = fg_hotelcompdeals.festival_id');
		$this->db->order_by('wf_festival.slug', 'asc'); 
		$this->db->order_by('fg_hotelcompdeals.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_hotel_comps($festival_id)
    {
		// loads all hotel comp types for the specified festival.
		$this->db->select('*');
		$this->db->from('fg_hotelcompdeals');
		$this->db->where('festival_id',$festival_id);
		$this->db->order_by('fg_hotelcompdeals.name', 'asc'); 
		$this->db->order_by('fg_hotelcompdeals.roomtype', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_used_hotel_comps($guest_list)
    {
		// get all used hotel comps for the guests of the current festival.
		$this->db->select('festivalguest_id, compnights, hotelcomp_id, share');
		$this->db->from('fg_accomodations');
		$this->db->where_in('festivalguest_id',$guest_list);
		$this->db->order_by('fg_accomodations.hotelcomp_id', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

}
?>