<?php
class Countrymodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_countries()
    {
		// loads all unique countries.
		$this->db->select('wf_movie_country.movie_id, wf_type_country.id, wf_type_country.name, wf_type_country.slug');
		$this->db->from('wf_movie_country');
		$this->db->join('wf_type_country', 'wf_movie_country.country_id = wf_type_country.id');
		$this->db->order_by('wf_type_country.name', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_countries_in($movie_id_array)
    {
		// loads all unique countries from films for list purposes.
		$this->db->select('wf_movie_country.movie_id, wf_type_country.id, wf_type_country.name, wf_type_country.slug');
		$this->db->from('wf_movie_country');
		$this->db->join('wf_type_country', 'wf_movie_country.country_id = wf_type_country.id');
		$this->db->where_in('wf_movie_country.movie_id', $movie_id_array);
		$this->db->order_by('wf_type_country.name', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_movie_countries($movie_id)
    {
		// loads all countries for this specific film.
		$this->db->select('wf_movie_country.movie_id, wf_movie_country.id, wf_type_country.id AS country_id, wf_type_country.name, wf_type_country.slug');
		$this->db->from('wf_movie_country');
		$this->db->join('wf_type_country', 'wf_movie_country.country_id = wf_type_country.id');
		$this->db->where('wf_movie_country.movie_id', $movie_id);
		$this->db->order_by('wf_type_country.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function add_movie_country($movie_id)
	{
		// adds a new country to a movie with no value specified
		$this->db->set('movie_id',$movie_id);
		$this->db->set('country_id',0);
		$this->db->insert('wf_movie_country');

        return $this->db->insert_id();
	}
}
?>