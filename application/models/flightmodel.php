<?php
class Flightmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_flights($guest_list, $order = "asc")
    {
		// loads all flights for guests of a specific festival.
		$this->db->select('*');
		$this->db->from('fg_flights');
		$this->db->where_in('festivalguest_id',$guest_list);
		if ($order == "asc") {
			$this->db->order_by('fg_flights.ArrivalDate', 'asc'); 
			$this->db->order_by('fg_flights.DepartureDate', 'asc'); 
		} else {
			$this->db->order_by('fg_flights.ArrivalDate', 'desc'); 
			$this->db->order_by('fg_flights.DepartureDate', 'desc'); 
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guest_flights($guest_id)
    {
		// loads all flights for the specified guest.
		$this->db->select('*');
		$this->db->from('fg_flights');
		$this->db->where('festivalguest_id',$guest_id);
		$this->db->order_by('fg_flights.ArrivalDate', 'asc'); 
		$this->db->order_by('fg_flights.DepartureDate', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_flight_comps($festival_id)
    {
		// loads all flight comp types for the specified festival.
		$this->db->select('*');
		$this->db->from('fg_flightcompoptions');
		$this->db->where('fg_flightcompoptions.festival_id',$festival_id);
		$this->db->order_by('fg_flightcompoptions.airline', 'asc');
		$this->db->order_by('fg_flightcompoptions.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_used_flight_comps($guest_list)
    {
		// get all used flight comps for the guests of the current festival.
		$this->db->select('festivalguest_id, cost, mileage, flightcomp_id');
		$this->db->from('fg_flights');
		$this->db->where_in('festivalguest_id',$guest_list);
		$this->db->order_by('fg_flights.flightcomp_id', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_flight_deals($festival_id)
    {
		// loads all flight comp types for the specified festival.
		$this->db->select('*');
		$this->db->from('fg_flightcompdeals');
		$this->db->where('fg_flightcompdeals.festival_id',$festival_id);
		$this->db->order_by('fg_flightcompdeals.airline', 'asc');
		$query = $this->db->get();

        return $query->result();
	}


}
?>