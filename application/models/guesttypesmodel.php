<?php
class Guesttypesmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	/*  List of Types

		Airlines - airlines
		Airports - airports
		Guest - guests

	*/
	
	// Order is either 'asc' or 'desc'

    function get_all_type($type, $order, $limit = "no")
    {
		$table_name = "fg_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		if ($limit != "no") {
			$this->db->where('limit', 1);
		}
		$this->db->order_by('name',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_type_order($type, $order, $limit = "no")
    {
		$table_name = "fg_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		if ($limit != "no") {
			$this->db->where('limit', 1);
		}
		$this->db->order_by('order',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_type($type, $id)
    {
		$table_name = "fg_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where('id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_type_airports($origin = "all")
    {
		$this->db->select('*');
		$this->db->from("fg_type_airports");
		if ($origin == "domestic") {
			$this->db->where('domestic', 1);
		} else if ($origin == "international") {
			$this->db->where('domestic', 0);
		} else {
			// return all airports, domestic & intl
		}
		$this->db->order_by('priority','desc');
		$this->db->order_by('name','asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_type_deadlines_fees_all()
    {
		$this->db->select('*');
		$this->db->from("fg_deadlines_fees");
		$this->db->order_by('early_deadline','asc');
		$query = $this->db->get();

        return $query->result();
	}

	function add_type_value($type, $value, $color) {
		// adds a new guest type value
		$this->db->set('name',$value);
		if ($color == 0) {  } else { $this->db->set('color',$color); }
		$this->db->insert('fg_type_'.$type);
		
		return $this->db->insert_id();
	}
	
	function update_type_value($type, $value, $color, $id) {
		// updates a guest type value
		$this->db->set('name',$value);
		if ($color == 0) {  } else { $this->db->set('color',$color); }
		$this->db->where('id',$id);
		$this->db->update('fg_type_'.$type);
	}

}
?>