<?php
class Guestmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_guests($fest_id, $sortfield = array("fg_festivalguests.Lastname" => "asc"))
    {
		// loads all guests for the specified festival.
		$this->db->select('fg_festivalguests.id as guest_id, fg_filmaffiliations.movie_id, fg_festivalguests.id as Program_Title, fg_festivalguests.id as Film_Title, fg_festivalguests.slug, fg_festivalguests.Firstname, fg_festivalguests.MI, fg_festivalguests.Lastname, fg_type_guests.name as GuestType, AffiliationOptional, ContactAddress, Phone, Cell, Fax, Email, fg_festivalguests.WebsiteURL, fg_festivalguests.TwitterName, fg_festivalguests.FacebookPage, fg_festivalguests.LinkedinProfile, gb_pickedup, badge_pickedup, fg_type_guests.badge as gets_badge, fg_type_guests.giftbag as gets_bag, fg_festivalguests.id as ArrivalDate, fg_festivalguests.id as DepartureDate, fg_festivalguests.id as checkin, fg_festivalguests.id as checkout');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id', 'left');
		//$this->db->join('fg_flights', 'fg_festivalguests.id = fg_flights.festivalguest_id', 'left');
		//$this->db->join('fg_accomodations', 'fg_festivalguests.id = fg_accomodations.festivalguest_id', 'left');
		$this->db->where('festival_id',$fest_id);
		foreach ($sortfield as $key => $value) {
			$this->db->order_by($key, $value);
		}
		$this->db->group_by('guest_id');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guest_ids($fest_id)
    {
		// loads just the individual ids of the films.
		$this->db->select('fg_festivalguests.id as guest_id, fg_festivalguests.slug AS value, CONCAT(fg_festivalguests.Firstname,\' \',fg_festivalguests.Lastname) AS label', FALSE);
		$this->db->from('fg_festivalguests');
		$this->db->where('fg_festivalguests.festival_id',$fest_id);
		$this->db->order_by('fg_festivalguests.Lastname', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guests_search($festival_id, $field, $direction, $start, $limit, $search)
    {
		// loads all guests for the specified festival.
		$this->db->select('fg_festivalguests.id as guest_id, fg_filmaffiliations.movie_id, fg_festivalguests.id as Program_Title, fg_festivalguests.id as Film_Title, fg_festivalguests.slug, fg_festivalguests.Firstname, fg_festivalguests.MI, fg_festivalguests.Lastname, fg_type_guests.name as GuestType, AffiliationOptional, ContactAddress, Phone, Cell, Fax, Email, fg_festivalguests.WebsiteURL, fg_festivalguests.TwitterName, fg_festivalguests.FacebookPage, fg_festivalguests.LinkedinProfile, gb_pickedup, badge_pickedup, fg_type_guests.badge as gets_badge, fg_type_guests.giftbag as gets_bag, fg_festivalguests.id as ArrivalDate, fg_festivalguests.id as DepartureDate, fg_festivalguests.id as checkin, fg_festivalguests.id as checkout');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id', 'left');
		//$this->db->join('fg_flights', 'fg_festivalguests.id = fg_flights.festivalguest_id', 'left');
		//$this->db->join('fg_accomodations', 'fg_festivalguests.id = fg_accomodations.festivalguest_id', 'left');
		$this->db->where('fg_festivalguests.festival_id', $festival_id);
		$this->db->like('fg_festivalguests.Lastname', $search);
		$this->db->or_like('fg_festivalguests.Firstname', $search);
		$this->db->order_by($field, $direction);
		if ($field != "fg_festivalguests.Lastname") {
			$this->db->order_by("fg_festivalguests.Lastname", "asc");
		}
		$this->db->group_by('guest_id');
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guests_except($fest_id, $guest_id)
    {
		// loads just the individual ids of the films.
		$this->db->select('fg_festivalguests.id, CONCAT(fg_festivalguests.Firstname,\' \',fg_festivalguests.Lastname) AS name', FALSE);
		$this->db->from('fg_festivalguests');
		$this->db->where('fg_festivalguests.festival_id',$fest_id);
		$this->db->where_not_in('id',array($guest_id));
		$this->db->order_by('fg_festivalguests.Lastname', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_guest_film_affiliations($guest_id_array)
    {
		// loads all guest's film affiliation information.
		$this->db->select('wf_movie.title_en, wf_movie.slug, fg_filmaffiliations.id as aff_id, fg_filmaffiliations.movie_id, fg_filmaffiliations.festivalguest_id');
		$this->db->from('fg_filmaffiliations');
		$this->db->join('wf_movie', 'wf_movie.id = fg_filmaffiliations.movie_id', 'left');
		$this->db->where_in('fg_filmaffiliations.festivalguest_id',$guest_id_array);
		$this->db->order_by('wf_movie.title_en');
		$query = $this->db->get();

        return $query->result();
	}


    function get_guest_film_affiliation($guest_id)
    {
		// loads one guest's film affiliation information.
		$this->db->select('wf_movie.title_en as film_title, wf_movie.slug as film_slug, fg_filmaffiliations.id as aff_id, fg_filmaffiliations.movie_id');
		$this->db->from('fg_filmaffiliations');
		$this->db->join('wf_movie', 'wf_movie.id = fg_filmaffiliations.movie_id', 'left');
		$this->db->where('fg_filmaffiliations.festivalguest_id', $guest_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_affiliation($filmaff_id)
    {
		// loads one film affiliation record.
		$this->db->select('fg_filmaffiliations.id as aff_id, fg_filmaffiliations.movie_id');
		$this->db->from('fg_filmaffiliations');
		$this->db->where('fg_filmaffiliations.id', $filmaff_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_screening_appearance($screening_id, $guest_id)
    {
		// loads a screening appearance based on screening id and guest id.
		$this->db->select('*');
		$this->db->from('fg_screeningappearances');
		$this->db->where('fg_screeningappearances.screening_id', $screening_id);
		$this->db->where('fg_screeningappearances.festivalguest_id', $guest_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest_screening_appearances($guest_id)
    {
		// loads one guest's screening appearance information.
		$this->db->select('*');
		$this->db->from('fg_screeningappearances');
		$this->db->where('fg_screeningappearances.festivalguest_id', $guest_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest_special_appearances($guest_id)
    {
		// loads one guest's special/event appearance information.
		$this->db->select('*');
		$this->db->from('fg_specialappearances');
		$this->db->where('fg_specialappearances.festivalguest_id', $guest_id);
		$this->db->order_by('fg_specialappearances.date', 'asc'); 
		$this->db->order_by('fg_specialappearances.time', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_existing_events($guest_list)
    {
		// loads existing unique special events.
		$this->db->select('id, location, date, time');
		$this->db->from('fg_specialappearances');
		$this->db->where_in('festivalguest_id',$guest_list);
		$this->db->group_by(array("location", "date", "time"));
		$this->db->order_by('location', 'asc'); 
		$this->db->order_by('date', 'asc'); 
		$this->db->order_by('time', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest($guest_id)
    {
		// loads one guest's information.
		$this->db->select('*');
		$this->db->from('fg_festivalguests');
		$this->db->where('id', $guest_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest_by_slug($slug)
    {
		// loads one guest's information.
		$this->db->select('*');
		$this->db->from('fg_festivalguests');
		$this->db->where('slug', $slug);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_internal_guests_sort($fest_id, $field, $direction, $start, $limit)
    {
		// loads all guests for the specified festival.
		$this->db->select('fg_festivalguests.id as guest_id, fg_filmaffiliations.movie_id, fg_festivalguests.id as Program_Title, fg_festivalguests.id as Film_Title, fg_festivalguests.slug, fg_festivalguests.Firstname, fg_festivalguests.MI, fg_festivalguests.Lastname, fg_type_guests.name as GuestType, AffiliationOptional, ContactAddress, Phone, Cell, Fax, Email, fg_festivalguests.WebsiteURL, fg_festivalguests.TwitterName, fg_festivalguests.FacebookPage, fg_festivalguests.LinkedinProfile, gb_pickedup, badge_pickedup, fg_type_guests.badge as gets_badge, fg_type_guests.giftbag as gets_bag, fg_festivalguests.id as ArrivalDate, fg_festivalguests.id as DepartureDate, fg_festivalguests.id as checkin, fg_festivalguests.id as checkout');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id', 'left');
		//$this->db->join('fg_flights', 'fg_festivalguests.id = fg_flights.festivalguest_id', 'left');
		//$this->db->join('fg_accomodations', 'fg_festivalguests.id = fg_accomodations.festivalguest_id', 'left');
		$this->db->where('festival_id',$fest_id);
		$this->db->order_by($field, $direction);
		if ($field != "fg_festivalguests.Lastname") {
			$this->db->order_by("fg_festivalguests.Lastname", "asc");
		}
		$this->db->group_by('guest_id');
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest_toggle($id)
    {
		// get toggle values for guests
		$this->db->select('gb_pickedup, badge_pickedup');
		$this->db->from('fg_festivalguests');
		$this->db->where('fg_festivalguests.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_hotel_toggle($id)
    {
		// get toggle values for hotels
		$this->db->select('pickup as pickuph, translator as translatorh');
		$this->db->from('fg_accomodations');
		$this->db->where('fg_accomodations.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_flight_toggle($id)
    {
		// get toggle values for flights
		$this->db->select('pickup as pickupf, translator as translatorf');
		$this->db->from('fg_flights');
		$this->db->where('fg_flights.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_scapp_toggle($id)
    {
		// get toggle values for screening appearances
		$this->db->select('transport as transportsc, translator as translatorsc');
		$this->db->from('fg_screeningappearances');
		$this->db->where('fg_screeningappearances.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_spapp_toggle($id)
    {
		// get toggle values for special event appearances
		$this->db->select('transport as transportsp, translator as translatorsp');
		$this->db->from('fg_specialappearances');
		$this->db->where('fg_specialappearances.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function add_guest($values, $slug) {
		// add fg_festivalguests table
		$this->db->set("slug",$slug);
		$this->db->set("title_en",$values["title_en"]);
		$this->db->set("title",$values["title"]);
		$this->db->set("format_id",$values["format_id"]);
		$this->db->set("runtime",0);
		$this->db->set("runtime_int",0);
		$this->db->set("year",date("Y"));
		$this->db->insert("wf_movie");
		$fg_guest_id = $this->db->insert_id();

		return $fg_guest_id;
	}

	function add_flights_hotels($guest_array) {
		$guest_list = array();
		foreach ($guest_array as $guestList) {
			$guestList->ArrivalDate = $guestList->DepartureDate = $guestList->checkin = $guestList->checkout = 0;
			$guest_list[] = $guestList->guest_id;
		}
		$flights = $this->flight->get_all_flights($guest_list);
		$hotels = $this->hotel->get_all_hotels($guest_list);
		
		foreach ($guest_array as $thisGuest) {
			foreach ($flights as $thisFlight) {
				if ($thisGuest->guest_id == $thisFlight->festivalguest_id) {
					$thisGuest->ArrivalDate = $thisFlight->ArrivalDate; $thisGuest->DepartureDate = $thisFlight->DepartureDate;
				}
			}
			foreach ($hotels as $thisHotel) {
				if ($thisGuest->guest_id == $thisHotel->festivalguest_id) {
					$thisGuest->checkin = $thisHotel->checkin; $thisGuest->checkout = $thisHotel->checkout;
				}
			}
		}
		
		return $guest_array;
	}
	
	function toggle_tinyint($id, $field, $table, $value) {
        $this->db->where("id", $id);
        $this->db->update($table, array($field => $value));
	}


	function delete_guest($values) {
		// delete hotel shares
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_accomodationshares");

		// delete hotels
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_accomodations");

		// delete flights
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_flights");

		// delete film affiliations
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_filmaffiliations");

		// delete screening appearances
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_screeningappearances");

		// delete special appearances
		$this->db->where("festivalguest_id",$values['festivalguest_id']);
		$this->db->delete("fg_specialappearances");

		// finally delete guest
		$this->db->where("id",$values['festivalguest_id']);
		$this->db->delete("fg_festivalguests");
		
		return true;
	}
	
	function get_deadlines_fees($festival_id) {
		$this->db->select('guesttype_id, fg_type_guests.name AS guesttype_name, early_deadline, late_deadline, final_deadline, early_fee, late_fee, final_fee');
		$this->db->from('fg_deadlines_fees');
		$this->db->join('fg_type_guests', 'fg_deadlines_fees.guesttype_id = fg_type_guests.id');
		$this->db->where('fg_deadlines_fees.festival_id', $festival_id);
		$this->db->order_by('fg_type_guests.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}
	

	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);

		return $this->db->insert_id();
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}

	// Flexible function to check for an existing slug value
	function check_existing_slug($table, $slug) {
		// Checks for an existing slug when creating a new one.
		$this->db->select($table.'.slug');
		$this->db->from($table);
		$this->db->where($table.'.slug', $slug);
		$query = $this->db->get();
		$result = $query->result();
			
		if (count($result) == 0) {
			return false;
		} else {
			return true;
		}
	}

}
?>