<?php
class Syncmodel extends MY_Model {
// Dependencies: sectionmodel, filmtypesmodel

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function check_add_sponsor($name, $currentSponsorList) {
    	$sponsorId = 0;
    	$sponsorExists = false;
    	foreach ($currentSponsorList as $key => $thisSponsor) {
    		if ($name == $thisSponsor->name) {
    			$sponsorId = $thisSponsor->id;
    			$sponsorExists = true;
    		}
    	}
    	if (!$sponsorExists) {
    		$values = array("name" => $name);
    		$sponsorId = $this->sync->add_value($values, "wf_sponsor");
    	}
    	return $sponsorId;
    }

    function check_add_venue($name, $venueList, $locationIdArray) {
    	$venueId = 0;
    	$venueExists = false;
    	foreach ($venueList as $key => $thisVenue) {
    		if ($name == $thisVenue->name) {
    			$venueId = $thisVenue->id;
    			$venueExists = true;
    		}
    	}
    	if (!$venueExists) {
			$values = array("slug" => create_simple_slug($name), "name" => $name, "location_ids" => implode(",", $locationIdArray));
			$venueId = $this->sync->add_value($values, "wf_screeningvenue");
    	}
    	return $venueId;
    }

    function check_add_location($newLocation, $locationList) {
    	$locationId = 0;
    	$locationExists = false;
    	foreach ($locationList as $key => $thisLocation) {
    		if ($newLocation["name"] == $thisLocation->name) {
    			$locationId = $thisLocation->id;
    			$locationExists = true;
    		}
    	}
    	if (!$locationExists) {
			$values = array("slug" => create_simple_slug($newLocation["name"]), "name" => $newLocation["abbreviation"], "displayname" => $newLocation["name"], "seats" => $newLocation["capacity"], "festival_id" => 0);
			$locationId = $this->sync->add_value($values, "wf_screeninglocation");
    	}
    	return $locationId;
    }

    function check_add_personnel($newPersList, $currentPersList)
    {
    	// Initialize and get existing personnel roles and names
    	$newRoleId = $newPersonId = $highestOrder = 0;
		$currentTypeList = $this->filmtype->get_all_type("personnel", "asc");
	
		foreach ($newPersList as $key => $thisPers) {
			$role = trim($thisPers["role"]);
			$roleExists = false;

			// Check to see if role already exists in the database. If so, return the role ID.
			foreach ($currentTypeList as $thisType) {
				if ($thisType->name == $role) {
					$roleExists = true;
					$newPersList[$key]["role"] = $thisType->id;
				}
				if (intval($thisType->order) > $highestOrder) { $highestOrder = intval($thisType->order); }
			}

			// If role does not exist, create a new entry for it in the database and return the new ID.
			if (!$roleExists) {
				$slug = $this->filmtype->create_simple_slug($role);
				$values = array("name" => $role, "slug" => $slug, "order" => ++$highestOrder, "limit" => 0);
				$newRoleId = $this->sync->add_value($values, "wf_type_personnel");
				$newPersList[$key]["role"] = $newRoleId;
				// add the new role to our current role list so it's not created multiple times.
				$values["id"] = $newRoleId;
				$currentTypeList[] = (object)$values;
			}

			$name = trim($thisPers["name"]);
			$lastname = trim($thisPers["lastname"]);
			$personExists = false;

			// Check to see if person already exists in database. If so, return the personnel ID.
			foreach ($currentPersList as $currentPers) {				
				if ($name == trim($currentPers->name) && $lastname == trim($currentPers->lastname)) {
					$personExists = true;
					$newPersList[$key]["personnel_id"] = $currentPers->id;
				}
			}	

			// If the person does not exist, create a new entry for them in the database and return the new ID.
			if (!$personExists) { 
				$values = array("name" => $name, "lastname" => $lastname, "lastnamefirst" => 0);
				$newPersonId = $this->sync->add_value($values, "wf_personnel");
				$newPersList[$key]["personnel_id"] = $newPersonId;
			}
		}	

    	// Return the passed in array, with role and personnel ID numbers instead of names.
    	return $newPersList;
    }

	function check_add_section($newSectionList)
	{
		$sectionId = "0";
		$currentSectionList = $this->section->get_all_sections(0);
	
		foreach ($newSectionList as $thisSection) {
			if ($thisSection != "") {
				$exists = false;
				$currentSectionId = "0";
				
				foreach ($currentSectionList as $currentSection) {
					if (trim($thisSection) == $currentSection->name) {
						$exists = true;
						$currentSectionId = $currentSection->id;
					}
				}
				
				if (!$exists) { 
					$values = array ("name" => $thisSection, "slug" => create_simple_slug($thisSection), "festival_id" => 0, "Description" => "");
					$sectionId = $this->sync->add_value($values, "wf_category");
				} else {
					$sectionId = $currentSectionId;
				}
			}
		}	
		return $sectionId;
	}

	function check_add_film_type($typename, $newTypeList)
	{
		$color = "";
		$typeId = "0";
		$typeArray = array();
		if ($typename == "format") { $color == "#999999"; } else { $color == ""; }
		if ($typename == "country" || $typename == "language" || $typename == "genre") {
			$returnMultiple = true;
		} else {
			$returnMultiple = false;			
		}
		$currentTypeList = $this->filmtype->get_all_type($typename, "asc");
	
		foreach ($newTypeList as $thisType) {
			if ($thisType != "") {
				$exists = false;
				$currentTypeId = "0";
				
				foreach ($currentTypeList as $currentType) {
					if (trim($thisType) == $currentType->name) {
						$exists = true;
						$currentTypeId = $currentType->id;
					}
				}
				
				$slug = $this->filmtype->create_simple_slug($thisType);
				if ($returnMultiple == true) {
					if (!$exists) { 
						$typeArray[] = $this->filmtype->add_type_value($typename, $thisType, $color, $slug);
					} else {
						$typeArray[] = $currentTypeId;
					}
				} else {
					if (!$exists) { 
						$typeId = $this->filmtype->add_type_value($typename, $thisType, $color, $slug);
					} else {
						$typeId = $currentTypeId;
					}
				}
			}
		}

		if ($returnMultiple == true) {
			return implode(",",$typeArray);
		} else {
			return $typeId;
		}
	}

	function get_all_new_films($festival_id) {
		$this->db->select('wf_festivalmovie.id, wf_festivalmovie.movie_id, wf_festivalmovie.eventival_film_id, wf_festivalmovie.myhiff_1, wf_festivalmovie.myhiff_2, wf_festivalmovie.myhiff_3, wf_festivalmovie.myhiff_4');
		$this->db->from('wf_festivalmovie');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by('wf_festivalmovie.movie_id', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

	function delete_film($values) {
		// delete sponsor logos
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_sponsor_logos");

		// delete videos
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_videos");

		// delete photos
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_photos");

		// delete countries
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_country");

		// delete languages (spoken)
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_language");

		// delete languages (subtitle)
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_language_subtitles");

		// delete genres
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_genre");

		// delete film personnel
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_personnel");

		// delete screenings
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_screening");

		// delete festivalmovie
		$this->db->where("id",$values['festivalmovie_id']);
		$this->db->delete("wf_festivalmovie");

		// delete movie
		$this->db->where("id",$values['movie_id']);
		$this->db->delete("wf_movie");
		
		return true;
	}


	function add_film($values)
	{
		// wf_movie table
		$this->db->set("slug",$values["slug"]);
		$this->db->set("title_en",$values["title_en"]);
		$this->db->set("title",$values["title"]);
		$this->db->set("format_id",$values["format_id"]);
		$this->db->set("runtime_int",intval($values["runtime_int"]));
		$this->db->set("year",intval($values["year"]));
		$this->db->set("synopsis",$values["synopsis"]);
		$this->db->set("synopsis_short",$values["synopsis_short"]);
		$this->db->set("synopsis_original",$values["synopsis_original"]);
		$this->db->set("websiteurl",$values["websiteurl"]);
		$this->db->set("updated_date", date("Y-m-d H:i:s"));
		$this->db->insert("wf_movie");
		$wf_movie_id = $this->db->insert_id();

		// wf_festivalmovie table
		$this->db->set("eventival_film_id",$values["eventival_film_id"]);
		$this->db->set("festival_id",$values["festival_id"]);
		$this->db->set("category_id",$values["category_id"]);
		$this->db->set("event_id",$values["event_id"]);
		$this->db->set("premiere_id",$values["premiere_id"]);
		$this->db->set("movie_id",$wf_movie_id);
		$this->db->set("myhiff_1",$values["myhiff_1"]);
		$this->db->set("myhiff_2",$values["myhiff_2"]);
		$this->db->set("myhiff_3",$values["myhiff_3"]);
		$this->db->set("myhiff_4",$values["myhiff_4"]);
		$this->db->set("Confirmed",$values["Confirmed"]);
		$this->db->set("Published",$values["Published"]);
		$this->db->insert("wf_festivalmovie");
		$wf_festivalmovie_id = $this->db->insert_id();
		
		// Movie Country
		if ($values["film_countries"] != "") {
			$countries = explode(",", $values["film_countries"]);
			foreach ($countries as $thisItem) {
				$this->db->set("country_id",$thisItem);
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_country");
			}
		}

		// Movie Language - Spoken
		if ($values["film_languages"] != "") {
			$languages1 = explode(",", $values["film_languages"]);
			foreach ($languages1 as $thisItem) {
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("language_id",$thisItem);
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_language");
			}
		}

		// Movie Language - Subtitles
		if ($values["film_subtitles"] != "") {
			$languages2 = explode(",", $values["film_subtitles"]);
			foreach ($languages2 as $thisItem) {
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("language_id",$thisItem);
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_language_subtitles");
			}
		}
		
		// Movie Genre
		if ($values["film_genres"] != "") {
			$genres = explode(",", $values["film_genres"]);
			foreach ($genres as $thisItem) {
				$this->db->set("genre_id",$thisItem);
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_genre");
			}
		}

		// Personnel
		if (count($values["film_personnel"]) > 0) {
			foreach ($values["film_personnel"] as $key => $thisPerson) {
				$type_id = 0;
				$roles = explode(",", $thisPerson["role"]);
				if (count($roles) > 0) { $type_id = $roles[0]; }

				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("type_id",$type_id);
				$this->db->set("personnel_id",$thisPerson["personnel_id"]);
				$this->db->set("personnel_roles",$thisPerson["role"]);
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_personnel");
			}
		}

		// Photos
		if (count($values["film_photos"]) > 0) {
			$photo_count = 1;
			foreach ($values["film_photos"] as $key => $thisPhoto) {
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("order",$photo_count);
				$this->db->set("url_original",$thisPhoto["url"]);
				$this->db->set("url_cropxlarge",$thisPhoto["url"]);
				$this->db->set("url_croplarge",$thisPhoto["url"]);
				$this->db->set("url_cropsmall",$thisPhoto["url"]);
				$this->db->set("file_dir","");
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_movie_photos");
				$photo_count++;
			}
		}

		// Videos
		if ($values["trailer"] != "") {
			$service_name = "";
			$url_array = explode("/",$values["trailer"]);
			$domain = trim($url_array[2],"www.");
			$service_name = explode(".",$domain);
			if ($service_name[0] == "youtu") { $service_name[0] = "youtube"; }

			$this->db->set("movie_id",$wf_movie_id);
			$this->db->set("order","1");
			$this->db->set("service_name",strtolower($service_name[0]));
			$this->db->set("url_video",$values["trailer"]);
			$this->db->set("updated_date", date("Y-m-d H:i:s"));
			$this->db->insert("wf_movie_videos");
		}

		// Sponsors, Sponsor Logos and Links
		if (count($values["sponsor_logos"]) > 0) {
			foreach ($values["sponsor_logos"] as $key => $thisLogo) {
				$sponsor_link = "";
				foreach ($values["sponsor_links"] as $key => $thisLink) {
					if ($thisLogo["description"] == $thisLink["description"]) {
						$sponsor_link = $thisLink["url"];
					}
				}

				$this->db->set("movie_id",$wf_movie_id);
				$this->db->set("sponsor_id",$thisLogo["sponsor_id"]);
				$this->db->set("festival_id",$values["festival_id"]);
				$this->db->set("sponsorlogo_id","1");
				$this->db->set("url_website",$sponsor_link);
				$this->db->set("url_logo",$thisLogo["url"]);
				$this->db->set("logo_width","0");
				$this->db->set("logo_height","0");
				$this->db->set("file_dir","");
				$this->db->set("updated_date", date("Y-m-d H:i:s"));
				$this->db->insert("wf_sponsor_logos");
			}
		}

		return $wf_movie_id;
	}

	function add_screening($values)
	{
		// add wf_screening table
		$this->db->set("movie_id",$values["movie_id"]);
		$this->db->set("eventival_program_movie_ids",$values["eventival_program_movie_ids"]);
		$this->db->set("program_movie_ids",$values["program_movie_ids"]);
		$this->db->set("program_name",$values["program_name"]);
		$this->db->set("program_slug",$values["program_slug"]);
		$this->db->set("program_desc",$values["program_desc"]);
		$this->db->set("date",$values["date"]);
		$this->db->set("time",$values["time"]);
		$this->db->set("location_id",$values["location_id"]);
		$this->db->set("url",$values["url"]);
		$this->db->set("Free",$values["Free"]);
		$this->db->set("Rush",$values["Rush"]);
		$this->db->set("Private",$values["Private"]);
		$this->db->set("intro",$values["intro"]);
		$this->db->set("intro_length",$values["intro_length"]);
		$this->db->set("q_and_a",$values["q_and_a"]);
		$this->db->set("q_and_a_length",$values["q_and_a_length"]);
		$this->db->set("total_runtime",$values["total_runtime"]);
		$this->db->set("Published", $values["Published"]);
		$this->db->set("updated_date", date("Y-m-d H:i:s"));
		$this->db->insert("wf_screening");
		$wf_screening_id = $this->db->insert_id();

		return $wf_screening_id;
	}	
}
?>