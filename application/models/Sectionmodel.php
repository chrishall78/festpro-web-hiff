<?php
class Sectionmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_sections($festival_id)
    {
		// loads sections common to all film festivals or for this specific film festival.
		$this->db->select('id, name, slug, festival_id, color');
		$this->db->from('wf_category');
		$this->db->order_by('wf_category.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_festival_sections($festival_id)
    {
		// loads sections for this specific film festival.
		$this->db->select('id, name, slug, festival_id');
		$this->db->from('wf_category');
		//$this->db->where('wf_category.festival_id', '0');
		$this->db->where('wf_category.festival_id', $festival_id);
		$this->db->order_by('wf_category.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_movies_sections($festival_id, $movie_id_array)
    {
		// loads sections common to all film festivals or for this specific film festival.
		$this->db->select('wf_category.id, wf_category.name, wf_category.slug');
		$this->db->distinct(); 
		$this->db->from('wf_category');
		$this->db->join('wf_festivalmovie', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->where_in('wf_festivalmovie.movie_id', $movie_id_array);
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where_in('wf_category.festival_id', array('0',$festival_id));
		$this->db->order_by('wf_category.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_movies_sections_ids($festival_id, $movie_id_array)
    {
		// loads sections common to all film festivals or for this specific film festival.
		$this->db->select('wf_category.id, wf_category.name, wf_category.slug, wf_festivalmovie.movie_id');
		$this->db->distinct(); 
		$this->db->from('wf_category');
		$this->db->join('wf_festivalmovie', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->where_in('wf_festivalmovie.movie_id', $movie_id_array);
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where_in('wf_category.festival_id', array('0',$festival_id));
		$this->db->order_by('wf_category.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_sponsors()
    {
		// loads sections common to all film festivals or for this specific film festival.
		$this->db->select('*');
		$this->db->from('wf_sponsor');
		$this->db->order_by('wf_sponsor.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_movie_section($category_id)
    {
		// loads the section assigned to this specific film.
		$this->db->select('*');
		$this->db->from('wf_category');
		$this->db->where('wf_category.id', $category_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_section_except($id, $order = "asc")
    {
		$this->db->select('id, name, slug');
		$this->db->from("wf_category");
		$this->db->where('id !=', $id);
		$this->db->order_by('name',$order);
		$query = $this->db->get();

        return $query->result();
	}

	function check_add_section($newSectionList) {
		$currentSectionList = $this->section->get_all_sections(0);
	
		foreach ($newSectionList as $thisSection) {
			if ($thisSection != "") {
				$exists = false;
				
				foreach ($currentSectionList as $currentSection) {
					if (trim($thisSection) == $currentSection->name) { $exists = true; }
					//print "'".trim($thisSection)."' vs '".$currentSection->name."'<br>";
				}
				
				if (!$exists) { 
					//print trim($thisSection)." not matched.<br>";
					$values = array (
						"name" => $thisSection,
						"slug" => create_simple_slug($thisSection),
						"festival_id" => 0,
						"Description" => ""
					);
					$this->section->add_value($values, "wf_category");
				}
			}
		}	
		
		return $this->section->get_all_sections(0);
	}

	function check_section_value($category_id) {
		$this->db->select('wf_festivalmovie.id, wf_category.name');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_category','wf_festivalmovie.category_id = wf_category.id');
		$this->db->where('wf_festivalmovie.category_id', $category_id);
		$query = $this->db->get();

        return $query->result();
	}

	function replace_section_value($id_delete, $id_replace) {
		$values = array( 'category_id' => $id_replace );
		$query = $this->db->update('wf_festivalmovie', $values, 'category_id = '.$id_delete );
        return $query;
	}


}
?>