<?php
class Printtrafficmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_pt_details($festivalmovie_id)
    {
		// loads print traffic details for this film.
		$this->db->select('*');
		$this->db->from('wf_printtraffic');
		$this->db->where('festivalmovie_id', $festivalmovie_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_pt_shipments($festivalmovie_id)
    {
		// loads all shipment legs for this film.
		$this->db->select('*');
		$this->db->from('wf_printtraffic_shipments');
		$this->db->where('festivalmovie_id', $festivalmovie_id);
		$this->db->order_by('date asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_inbound_shipments($film_array, $field, $direction)
    {
    	// Documentary Film, Narrative Film, Short Film, Music Video
    	$event_filter = array("1","2","3","4");

		// loads all shipment legs for this film.
		$this->db->select('wf_printtraffic_shipments.id, wf_printtraffic_shipments.festivalmovie_id, wf_printtraffic_shipments.courier_id, wf_printtraffic_shipments.AccountNum, wf_printtraffic_shipments.TrackingNum, wf_printtraffic_shipments.outbound, wf_printtraffic_shipments.ShippingFee, wf_printtraffic_shipments.notes, wf_printtraffic_shipments.wePay, wf_printtraffic_shipments.confirmed, wf_printtraffic_shipments.arranged, wf_printtraffic_shipments.InShipped, wf_printtraffic_shipments.InExpectedArrival, wf_printtraffic_shipments.InReceived, wf_movie.id AS movie_id, wf_movie.slug, wf_movie.title_en, wf_movie.format_id, wf_type_courier.name AS Courier, wf_type_format.name AS VideoFormat, wf_type_event.name AS EventType');
		$this->db->from('wf_printtraffic_shipments');
		$this->db->join('wf_festivalmovie', 'wf_printtraffic_shipments.festivalmovie_id = wf_festivalmovie.id');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_type_courier', 'wf_printtraffic_shipments.courier_id = wf_type_courier.id', 'left');
		$this->db->join('wf_type_format', 'wf_movie.format_id = wf_type_format.id', 'left');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->where_in('wf_printtraffic_shipments.festivalmovie_id', $film_array);
		$this->db->where_in('wf_type_event.id', $event_filter);
		$this->db->where('wf_printtraffic_shipments.outbound', 0);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_outbound_shipments($film_array, $field, $direction)
    {
    	// Documentary Film, Narrative Film, Short Film, Music Video
    	$event_filter = array("1","2","3","4");

		// loads all shipment legs for this film.
		$this->db->select('wf_printtraffic_shipments.id, wf_printtraffic_shipments.festivalmovie_id, wf_printtraffic_shipments.courier_id, wf_printtraffic_shipments.AccountNum, wf_printtraffic_shipments.TrackingNum, wf_printtraffic_shipments.outbound, wf_printtraffic_shipments.ShippingFee, wf_printtraffic_shipments.notes, wf_printtraffic_shipments.wePay, wf_printtraffic_shipments.confirmed, wf_printtraffic_shipments.arranged, wf_printtraffic_shipments.OutScheduleBy, wf_printtraffic_shipments.OutShipBy, wf_printtraffic_shipments.OutShipped, wf_printtraffic_shipments.OutArriveBy, wf_movie.id AS movie_id, wf_movie.slug, wf_movie.title_en, wf_movie.format_id, wf_type_courier.name AS Courier, wf_type_format.name AS VideoFormat, wf_type_event.name AS EventType');
		$this->db->from('wf_printtraffic_shipments');
		$this->db->join('wf_festivalmovie', 'wf_festivalmovie.id = wf_printtraffic_shipments.festivalmovie_id');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_type_courier', 'wf_printtraffic_shipments.courier_id = wf_type_courier.id', 'left');
		$this->db->join('wf_type_format', 'wf_movie.format_id = wf_type_format.id', 'left');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->where_in('wf_type_event.id', $event_filter);
		$this->db->where_in('wf_printtraffic_shipments.festivalmovie_id', $film_array);
		$this->db->where('wf_printtraffic_shipments.outbound', 1);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$query = $this->db->get();

        return $query->result();
	}
}
?>