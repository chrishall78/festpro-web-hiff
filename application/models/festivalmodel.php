<?php
class Festivalmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_festivals()
    {
		// loads all festivals.
		$this->db->select('id, slug, name, year, startdate, enddate, timezone, locations, current, currentfront, showonfront, default_photo_url, updated_date');
		$this->db->from('wf_festival');
		$this->db->order_by('slug', 'desc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_festivals_front()
    {
		// loads all festivals that have been approved to show on the front end.
		$this->db->select('id, slug, name, year, startdate, enddate, timezone, locations, current, currentfront, showonfront, default_photo_url, program_updates, updated_date');
		$this->db->from('wf_festival');
		$this->db->where('showonfront', '1');
		$query = $this->db->get();

        return $query->result();
	}

    function get_festival($id)
    {
		// loads one festival.
		$this->db->select('id, slug, name, year, startdate, enddate, timezone, locations, current, currentfront, showonfront, default_photo_url, program_updates, pdf_flyer_html, updated_date');
		$this->db->from('wf_festival');
		$this->db->where('id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_current_festival()
    {
		// loads current festival.
		$this->db->select('id, slug, name, year, startdate, enddate, timezone, locations, current, currentfront, showonfront, default_photo_url, program_updates, pdf_flyer_html, updated_date');
		$this->db->from('wf_festival');
		$this->db->where('current', '1');
		$query = $this->db->get();

        return $query->result();
	}

    function get_current_festival_front()
    {
		// loads current festival.
		$this->db->select('id, slug, name, year, startdate, enddate, timezone, locations, current, currentfront, showonfront, default_photo_url, program_updates, updated_date');
		$this->db->from('wf_festival');
		$this->db->where('currentfront', '1');
		$query = $this->db->get();

        return $query->result();
	}

	function clear_current_festivals() {
		// updates a record
		$this->db->set("current",0);
		$this->db->update("wf_festival");
	}

	function clear_current_festivals_front() {
		// updates a record
		$this->db->set("currentfront",0);
		$this->db->update("wf_festival");
	}

}
?>