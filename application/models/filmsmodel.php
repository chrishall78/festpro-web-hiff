<?php
class Filmsmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function check_existing_slug($slug) {
		// Checks for an existing slug when creating a new one.
		$this->db->select('wf_movie.slug');
		$this->db->from('wf_movie');
		$this->db->where('wf_movie.slug', $slug);
		$query = $this->db->get();
		$result = $query->result();
		
		if (count($result) == 0) {
			return false;
		} else {
			return true;
		}
	}

    function get_all_films($festival_id)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,wf_movie.slug,category_id,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->order_by('wf_movie.title_en', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_internal_films($festival_id)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,wf_movie.slug,category_id,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.votes_1,wf_festivalmovie.votes_2,wf_festivalmovie.votes_3,wf_festivalmovie.votes_4,wf_festivalmovie.votes_5');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by('wf_movie.title_en', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_internal_films_sort($festival_id, $field, $direction, $start, $limit)
    {
		// loads the full (internal) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,format_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete,wf_festivalmovie.votes_1,wf_festivalmovie.votes_2,wf_festivalmovie.votes_3,wf_festivalmovie.votes_4,wf_festivalmovie.votes_5');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_internal_films_search($festival_id, $field, $direction, $start, $limit, $search)
    {
		// loads the full (internal) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete,wf_festivalmovie.votes_1,wf_festivalmovie.votes_2,wf_festivalmovie.votes_3,wf_festivalmovie.votes_4,wf_festivalmovie.votes_5');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->like('wf_movie.title_en', $search);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_search($festival_id, $field, $direction, $start, $limit, $search)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->like('wf_movie.title_en', $search);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_sort($festival_id, $field, $direction, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_category.slug AS section_slug, wf_type_event.slug AS event_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_filter($festival_id, $sort_field, $direction, $filter_field, $filter_value, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_category.slug AS section_slug, wf_type_event.slug AS event_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where($filter_field, $filter_value);
		$this->db->order_by($sort_field, $direction);
		if ($sort_field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_sort_country($festival_id, $field, $direction, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_type_country.id as country_id, wf_type_country.name AS country_name, wf_type_country.slug AS country_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->join('wf_movie_country', 'wf_movie_country.movie_id = wf_movie.id');
		$this->db->join('wf_type_country', 'wf_type_country.id = wf_movie_country.country_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->group_by("wf_movie.slug");
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_filter_country($festival_id, $sort_field, $direction, $filter_field, $filter_value, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_type_country.id as country_id, wf_type_country.name AS country_name, wf_type_country.slug AS country_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->join('wf_movie_country', 'wf_movie_country.movie_id = wf_movie.id');
		$this->db->join('wf_type_country', 'wf_type_country.id = wf_movie_country.country_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where($filter_field, $filter_value);
		$this->db->order_by($sort_field, $direction);
		if ($sort_field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_sort_genre($festival_id, $field, $direction, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_type_genre.id as genre_id, wf_type_genre.name AS genre_name, wf_type_genre.slug AS genre_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->join('wf_movie_genre', 'wf_movie_genre.movie_id = wf_movie.id');
		$this->db->join('wf_type_genre', 'wf_type_genre.id = wf_movie_genre.genre_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->group_by("wf_movie.slug");
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_films_filter_genre($festival_id, $sort_field, $direction, $filter_field, $filter_value, $start, $limit)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_movie.slug,wf_festivalmovie.movie_id,category_id,wf_category.name AS category_name, wf_type_format.name AS format_name, wf_type_event.id AS event_id, wf_type_event.name AS event_name,premiere_id,title,title_en,year,runtime_int,wf_movie.synopsis,synopsis_short,synopsis_original,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete, wf_type_genre.id as genre_id, wf_type_genre.name AS genre_name, wf_type_genre.slug AS genre_slug');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_category', 'wf_category.id = wf_festivalmovie.category_id', 'left');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->join('wf_movie_genre', 'wf_movie_genre.movie_id = wf_movie.id');
		$this->db->join('wf_type_genre', 'wf_type_genre.id = wf_movie_genre.genre_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where($filter_field, $filter_value);
		$this->db->order_by($sort_field, $direction);
		if ($sort_field != "wf_movie.title_en") {
			$this->db->order_by("wf_movie.title_en", "asc");
		}
		$this->db->limit($limit, $start);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_film_ids($festival_id)
    {
		// Exclude: Receptions, Press Conferences
		//$eventTypeArray = array("6","14");

		// loads just the individual ids of the films.
		$this->db->select('wf_festivalmovie.movie_id, wf_movie.slug AS value, wf_movie.title_en AS label, wf_movie.runtime_int');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		//$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		//$this->db->where_not_in('wf_type_event.id',$eventTypeArray);
		$this->db->order_by('wf_movie.title_en', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_film_ids_wsiw($festival_id)
    {
		// Exclude: Receptions, Press Conferences
		//$eventTypeArray = array("6","14");

		// loads just the individual ids of the films.
		$this->db->select('wf_festivalmovie.movie_id, wf_movie.slug AS value, wf_movie.title_en AS label, wf_movie.runtime_int, wf_festivalmovie.category_id, wf_festivalmovie.event_id, wf_movie.id as country_id, wf_movie.id as language_id, wf_movie.id as genre_id');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_type_event.id = wf_festivalmovie.event_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		//$this->db->where_not_in('wf_type_event.id',$eventTypeArray);
		$this->db->order_by('wf_movie.title_en', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_internal_film_ids($festival_id)
    {
		// loads just the individual ids of the films.
		$this->db->select('wf_festivalmovie.eventival_film_id, wf_festivalmovie.id, wf_festivalmovie.movie_id, wf_movie.slug AS value, wf_movie.title_en AS label, wf_movie.runtime_int');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by('wf_movie.title_en', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_film_ids2($festival_id)
    {
		// loads just the individual ids of the films.
		$this->db->select('wf_festivalmovie.id, wf_festivalmovie.movie_id');
		$this->db->from('wf_festivalmovie');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->order_by('wf_festivalmovie.id', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_film_titles($festival_id)
    {
		// load one specific film
		$this->db->select('wf_movie.id as movie_id, wf_movie.slug,wf_movie.title_en');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by('wf_movie.title_en', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}


    function get_film($movie_id)
    {
		// load one specific film -or- a list of films given an array of movie_id values
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,category_id,premiere_id,printtraffic_id,event_id,generalNotes,PBPage,UGPage,RentalFee,ScreeningFee,TransferFee,GuestFee,myhiff_1,myhiff_2,myhiff_3,myhiff_4,wf_movie.slug,title,title_en,year,format_id, wf_type_format.color,runtime_int,websiteurl,wf_movie.synopsis,synopsis_short,synopsis_original,writerLong,writerShort,writerOrigLang,user_id,aspectratio_id,sound_id,distribution_id,distributor,distributorContact,distributorAddress,distributorPhone,distributorFax,distributorEmail,PressContact,PressEmail,PressFax,PressPhone,PressWebsite,HawaiiConnections,wf_festivalmovie.votes_1,wf_festivalmovie.votes_2,wf_festivalmovie.votes_3,wf_festivalmovie.votes_4,wf_festivalmovie.votes_5');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_type_format', 'wf_type_format.id = wf_movie.format_id', 'left');
		if (is_string($movie_id)) {
			$this->db->where('wf_festivalmovie.movie_id', $movie_id);
		}
		if (is_array($movie_id)) {
			$this->db->where_in('wf_festivalmovie.movie_id', $movie_id);
			$this->db->order_by('wf_movie.title_en', 'asc');
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_film2($festivalmovie_id)
    {
		// load one specific film
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,category_id,premiere_id,printtraffic_id,event_id,generalNotes,PBPage,UGPage,RentalFee,ScreeningFee,TransferFee,GuestFee,myhiff_1,myhiff_2,myhiff_3,myhiff_4,wf_movie.slug,title,title_en,year,format_id,runtime_int,websiteurl,wf_movie.synopsis,synopsis_short,synopsis_original,writerLong,writerShort,writerOrigLang,user_id,aspectratio_id,sound_id,distribution_id,distributor,distributorContact,distributorAddress,distributorPhone,distributorFax,distributorEmail,PressContact,PressEmail,PressFax,PressPhone,PressWebsite,HawaiiConnections,wf_festivalmovie.votes_1,wf_festivalmovie.votes_2,wf_festivalmovie.votes_3,wf_festivalmovie.votes_4,wf_festivalmovie.votes_5');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->where('wf_festivalmovie.id', $festivalmovie_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_by_slug($slug)
    {
		// load one specific film by slug
		$this->db->select('wf_festivalmovie.id, wf_festivalmovie.festival_id, wf_festivalmovie.movie_id,category_id, wf_category.slug AS section_slug,premiere_id,printtraffic_id,event_id,generalNotes,PBPage,UGPage,RentalFee,ScreeningFee,TransferFee,GuestFee,myhiff_1,myhiff_2,myhiff_3,myhiff_4,wf_movie.slug,title,title_en,year,format_id,runtime_int,websiteurl,wf_movie.synopsis,synopsis_short,synopsis_original,writerLong,writerShort,writerOrigLang,user_id,aspectratio_id,sound_id,framerate_id,distribution_id,distributor,distributorContact,distributorAddress,distributorPhone,distributorFax,distributorEmail,PressContact,PressEmail,PressFax,PressPhone,PressWebsite,HawaiiConnections,wf_festivalmovie.Screener,wf_festivalmovie.Confirmed,wf_festivalmovie.Published,wf_festivalmovie.Complete,votes_1,votes_2,votes_3,votes_4,votes_5,dcp_encryption,key_received,key_tested,colorbar_end_time,titleid_end_time,in_time,end_credits_time,out_time');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_category', 'wf_festivalmovie.category_id = wf_category.id', 'left');
		$this->db->where('wf_movie.slug', $slug);
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_title($movie_id)
    {
		// load one specific film
		$this->db->select('wf_movie.id as movie_id, wf_movie.slug,wf_movie.title_en');
		$this->db->from('wf_movie');
		$this->db->where('wf_movie.id', $movie_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_toggle($id)
    {
		// get the status of these fields
		$this->db->select('Confirmed,Screener,Complete,Published');
		$this->db->from('wf_festivalmovie');
		$this->db->where('wf_festivalmovie.movie_id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function val_on($movie_id, $table_name, $field_name)
	{
        $this->db->where('movie_id', $movie_id);
        $this->db->update($table_name, array($field_name => '1'));
	}

	function val_off($movie_id, $table_name, $field_name)
	{
        $this->db->where('movie_id', $movie_id);
        $this->db->update($table_name, array($field_name => '0'));
	}

	function add_film($values, $slug)
	{
		// add wf_movie table
		$this->db->set("slug",$slug);
		$this->db->set("title_en",$values["title_en"]);
		$this->db->set("title","");
		$this->db->set("format_id",$values["format_id"]);
		$this->db->set("runtime_int",intval($values["runtime"]));
		$this->db->set("year",intval($values["year"]));
		$this->db->set("distributor","");
		$this->db->set("distributorContact","");
		$this->db->set("distributorAddress","");
		$this->db->set("distributorPhone","");
		$this->db->set("distributorFax","");
		$this->db->set("distributorEmail","");
		$this->db->set("PressContact","");
		$this->db->set("PressEmail","");
		$this->db->set("PressFax","");
		$this->db->set("PressPhone","");
		$this->db->set("PressWebsite","");
		$this->db->set("HawaiiConnections","");
		$this->db->set("updated_date", date("Y-m-d H:i:s"));
		$this->db->insert("wf_movie");
		$wf_movie_id = $this->db->insert_id();

		// add wf_festivalmovie table
		$this->db->set("festival_id",$values["festival_id"]);
		$this->db->set("category_id",$values["category_id"]);
		$this->db->set("event_id",$values["event_id"]);
		$this->db->set("premiere_id",$values["premiere_id"]);
		$this->db->set("movie_id",$wf_movie_id);
		$this->db->set("PBPage",0);
		$this->db->set("UGPage",0);
		$this->db->insert("wf_festivalmovie");
		$wf_festivalmovie_id = $this->db->insert_id();
		
		// add wf_printtraffic table	
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		//$this->db->set("user_id",$values["user_id"]);
		$this->db->insert("wf_printtraffic");

		// add wf_printtraffic_shipments table - outbound
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		$this->db->set("courier_id",7);
		$this->db->set("AccountNum","");
		$this->db->set("TrackingNum","");
		$this->db->set("ShippingFee",0);
		$this->db->set("InShipped","0000-00-00");
		$this->db->set("InExpectedArrival","0000-00-00");
		$this->db->set("InReceived","0000-00-00");
		$this->db->set("OutScheduleBy","0000-00-00");
		$this->db->set("OutShipBy","0000-00-00");
		$this->db->set("OutShipped","0000-00-00");
		$this->db->set("OutArriveBy","0000-00-00");
		$this->db->set("notes","");
		$this->db->set("wePay",0);
		$this->db->set("arranged",0);
		$this->db->set("confirmed",0);
		$this->db->set("outbound",1);
		$this->db->insert("wf_printtraffic_shipments");

		// add wf_printtraffic_shipments table - inbound
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		$this->db->set("courier_id",7);
		$this->db->set("AccountNum","");
		$this->db->set("TrackingNum","");
		$this->db->set("ShippingFee",0);
		$this->db->set("InShipped","0000-00-00");
		$this->db->set("InExpectedArrival","0000-00-00");
		$this->db->set("InReceived","0000-00-00");
		$this->db->set("OutScheduleBy","0000-00-00");
		$this->db->set("OutShipBy","0000-00-00");
		$this->db->set("OutShipped","0000-00-00");
		$this->db->set("OutArriveBy","0000-00-00");
		$this->db->set("notes","");
		$this->db->set("wePay",1);
		$this->db->set("confirmed",0);
		$this->db->set("outbound",0);
		$this->db->insert("wf_printtraffic_shipments");

		// Add Movie Country (if any)
		if ($values["country"] != "") {
			$this->db->set("country_id",$values["country"]);
			$this->db->set("movie_id",$wf_movie_id);
			$this->db->insert("wf_movie_country");
		}

		// Add Movie Language (if any)
		if ($values["language"] != "") {
			$this->db->set("language_id",$values["language"]);
			$this->db->set("movie_id",$wf_movie_id);
			$this->db->insert("wf_movie_language");
		}
		
		// Add Movie Genre (if any)
		if ($values["genre"] != "") {
			$this->db->set("genre_id",$values["genre"]);
			$this->db->set("movie_id",$wf_movie_id);
			$this->db->insert("wf_movie_genre");
		}

		return $wf_movie_id;
	}

	function publish_all_confirmed_films($film_id_array) {
		// Toggle 'Published' setting
		$this->db->set("Published", 1);
		$this->db->where_in("movie_id", $film_id_array);
		$this->db->where("Confirmed", 1);
		$this->db->update("wf_festivalmovie");

		// Set 'Updated Date' for all these films
		$this->db->set("updated_date", date("Y-m-d H:i:s"));
		$this->db->where_in("id", $film_id_array);
		$this->db->update("wf_movie");
	}

	function unpublish_all_films($festival_id) {
		$this->db->set("Published",0);
		$this->db->where('festival_id',$festival_id);
		$this->db->update("wf_festivalmovie");
	}


	// Film Views
	function get_film_view($movie_id) {
		$this->db->select('wf_movie_views.id, wf_movie_views.festival_id, wf_movie_views.movie_id, wf_movie_views.web, wf_movie_views.facebook, wf_movie_views.mobile');
		$this->db->from('wf_movie_views');
		$this->db->where('wf_movie_views.movie_id', $movie_id);
		$query = $this->db->get();

        return $query->result();
	}

	function add_film_view($movie_id, $type, $festival_id) {
		$this->db->set("festival_id", $festival_id);
		$this->db->set("movie_id", $movie_id);
		$type == "web" ? $this->db->set("web", 1) : $this->db->set("web", 0);
		$type == "facebook" ? $this->db->set("facebook", 1) : $this->db->set("facebook", 0);
		$type == "mobile" ? $this->db->set("mobile", 1) : $this->db->set("mobile", 0);
		$this->db->insert("wf_movie_views");

		return $this->db->insert_id();
	}

	function update_film_view($movie_id, $type, $value) {
		if ($type == "web") { $this->db->set("web", intval($value)+1); }
		if ($type == "facebook") { $this->db->set("web", intval($value)+1); }
		if ($type == "mobile") { $this->db->set("mobile", intval($value)+1); }
		$this->db->where('movie_id', $movie_id);
		$this->db->update("wf_movie_views");
		
		return intval($value)+1;
	}



	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
		
		return $id;
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}

	function delete_film($values) {
		// delete videos
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_videos");

		// delete photos
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_photos");

		// delete countries
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_country");

		// delete languages
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_language");

		// delete genres
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_genre");

		// delete film personnel
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_movie_personnel");

		// delete screenings
		$this->db->where("movie_id",$values['movie_id']);
		$this->db->delete("wf_screening");

		// delete print traffic details
		$this->db->where("festivalmovie_id",$values['festivalmovie_id']);
		$this->db->delete("wf_printtraffic");
	
		// delete print traffic shipments
		$this->db->where("festivalmovie_id",$values['festivalmovie_id']);
		$this->db->delete("wf_printtraffic_shipments");

		// delete festivalmovie
		$this->db->where("id",$values['festivalmovie_id']);
		$this->db->delete("wf_festivalmovie");

		// delete movie
		$this->db->where("id",$values['movie_id']);
		$this->db->delete("wf_movie");
		
		return true;
	}


	function import_film_from_tabfile($values, $slug, $personnel_to_add) {
		// add wf_movie table
		$this->db->set("slug",$slug);
		$this->db->set("title_en",$values["title_en"]);
		$this->db->set("title",$values["title"]);
		$this->db->set("format_id",$values["format_id"]);
		$this->db->set("color_id",$values["color_id"]);
		$this->db->set("synopsis",$values["synopsis"]);
		$this->db->set("synopsis_short",$values["synopsis_short"]);
		$this->db->set("synopsis_original",$values["synopsis_original"]);
		$this->db->set("runtime",$values["runtime_int"]);
		$this->db->set("runtime_int",$values["runtime_int"]);
		$this->db->set("year",$values["year"]);
		$this->db->set("distributor","");
		$this->db->set("distributorContact","");
		$this->db->set("distributorAddress","");
		$this->db->set("distributorPhone","");
		$this->db->set("distributorFax","");
		$this->db->set("distributorEmail","");
		$this->db->set("PressContact","");
		$this->db->set("PressEmail","");
		$this->db->set("PressFax","");
		$this->db->set("PressPhone","");
		$this->db->set("PressWebsite","");
		$this->db->set("HawaiiConnections","");
		$this->db->insert("wf_movie");
		$wf_movie_id = $this->db->insert_id();

		// add wf_festivalmovie table
		$this->db->set("movie_id",$wf_movie_id);
		$this->db->set("festival_id",$values["festival_id"]);
		$this->db->set("category_id",$values["category_id"]);
		$this->db->set("premiere_id",0);
		$this->db->set("event_id",$values["event_id"]);
		$this->db->set("Confirmed",0);
		$this->db->set("Published",0);
		$this->db->set("PBPage",0);
		$this->db->set("UGPage",0);
		$this->db->insert("wf_festivalmovie");
		$wf_festivalmovie_id = $this->db->insert_id();
		
		// add wf_printtraffic table	
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		$this->db->insert("wf_printtraffic");

		// add wf_printtraffic_shipments table - outbound
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		$this->db->set("courier_id",7);
		$this->db->set("AccountNum","");
		$this->db->set("TrackingNum","");
		$this->db->set("ShippingFee",0);
		//$this->db->set("InExpectedArrival","0000-00-00");
		//$this->db->set("InReceived","0000-00-00");
		$this->db->set("OutShipped","0000-00-00");
		$this->db->set("OutArriveBy","0000-00-00");
		$this->db->set("notes","");
		$this->db->set("wePay",0);
		$this->db->set("arranged",0);
		$this->db->set("confirmed",0);
		$this->db->set("outbound",1);
		$this->db->insert("wf_printtraffic_shipments");

		// add wf_printtraffic_shipments table - inbound
		$this->db->set("festivalmovie_id",$wf_festivalmovie_id);
		$this->db->set("courier_id",7);
		$this->db->set("AccountNum","");
		$this->db->set("TrackingNum","");
		$this->db->set("ShippingFee",0);
		$this->db->set("InExpectedArrival","0000-00-00");
		$this->db->set("InReceived","0000-00-00");
		//$this->db->set("OutShipped","0000-00-00");
		//$this->db->set("OutArriveBy","0000-00-00");
		$this->db->set("notes","");
		$this->db->set("wePay",1);
		$this->db->set("confirmed",0);
		$this->db->set("outbound",0);
		$this->db->insert("wf_printtraffic_shipments");

		// Add Movie Country (if any)
		if ($values["country"] != "") {
			$countryValues = explode(",",$values["country"]);
			foreach ($countryValues as $thisCountry) {
				$this->db->set("country_id",$thisCountry);
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->insert("wf_movie_country");
			}
		}

		// Add Movie Language (if any)
		if ($values["language"] != "") {
			$languageValues = explode(",",$values["language"]);
			foreach ($languageValues as $thisLanguage) {
				$this->db->set("language_id",$thisLanguage);
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->insert("wf_movie_language");
			}
		}
		
		// Add Movie Genre (if any)
		if ($values["genre"] != "") {
			$genreValues = explode(",",$values["genre"]);
			foreach ($genreValues as $thisGenre) {
				$this->db->set("genre_id",$thisGenre);
				$this->db->set("movie_id",$wf_movie_id);
				$this->db->insert("wf_movie_genre");
			}
		}

		// Add each Personnel type to wf_movie_personnel for this film		
		foreach ($personnel_to_add as $key => $value) {
			if ($values[$key] != "") {
				$dirValues = explode(",",$values[$key]);
				foreach ($dirValues as $thisPersonnel) {
					$this->db->set("personnel_id",$thisPersonnel);
					$this->db->set("type_id",$value);
					$this->db->set("personnel_roles",$value);
					$this->db->set("movie_id",$wf_movie_id);
					$this->db->insert("wf_movie_personnel");
				}
			}
		}
		return $wf_movie_id;
	}

}
?>