<?php
class Filmtypesmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	/*  List of Types

		Admission - admit
		Aspect Ratio - aspectratio
		Color - color
		Country  - country
		Courier - courier
		Distribution  - distribution
		Event - event
		Frame Rate - framerate
		Genre - genre
		Language - language
		Personnel - personnel
		Premiere - premiere
		Press Options - pressoptions
		Showing - showing
		Sponsor Logo - sponsorlogo
		Sound Format - sound
		Video Format - format
	*/
	
	// Order is either 'asc' or 'desc'

    function get_all_type($type, $order = "asc", $limit = "no")
    {
		$table_name = "wf_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		if ($limit != "no") {
			$this->db->where('limit', 1);
		}
		$this->db->order_by('name',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_type_order($type, $order = "asc", $limit = "no")
    {
		$table_name = "wf_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		if ($limit != "no") {
			$this->db->where('limit', 1);
		}
		$this->db->order_by('order',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_movies_type($type, $order = "asc", $movie_id_array)
    {
		$table_name = "wf_type_".$type;

		$this->db->select($table_name.'.id, '.$table_name.'.name, '.$table_name.'.slug');
		$this->db->distinct();
		$this->db->from($table_name);
		$this->db->join('wf_movie_'.$type, $table_name.'.id = wf_movie_'.$type.'.'.$type.'_id');
		$this->db->where_in('wf_movie_'.$type.'.movie_id', $movie_id_array);
		$this->db->order_by($table_name.'.name',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_events_type($order = "asc", $movie_id_array)
    {
		// Exclude: Receptions, Press Conferences
		//$eventTypeArray = array("6","14");

		$this->db->select('wf_type_event.id, wf_type_event.name, wf_type_event.slug, wf_type_event.color');
		$this->db->distinct();
		$this->db->from('wf_type_event');
		$this->db->join('wf_festivalmovie', 'wf_type_event.id = wf_festivalmovie.event_id');
		$this->db->where_in('wf_festivalmovie.movie_id', $movie_id_array);
		//$this->db->where_not_in('wf_type_event.id',$eventTypeArray);
		$this->db->order_by('wf_type_event.name',$order);
		$query = $this->db->get();

        return $query->result();
	}

    function get_type($type, $id)
    {
		$table_name = "wf_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where('id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_type_except($type, $id, $order = "asc")
    {
		$table_name = "wf_type_".$type;

		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where('id !=', $id);
		$this->db->order_by('name',$order);
		$query = $this->db->get();

        return $query->result();
	}


	function check_filmtype_slug($slug, $table) {
		// Checks for an existing slug when creating a new one.
		$this->db->select($table.'.slug');
		$this->db->from($table);
		$this->db->where($table.'.slug', $slug);
		$query = $this->db->get();
		$result = $query->result();
		
		if (count($result) == 0) {
			return false;
		} else {
			return true;
		}
	}

	function create_simple_slug($name) {
		// replace dash or space with underscore
		$newslug = str_replace(array('-',' '), "_", $name);	
		// remove all characters except a-z, A-Z, 0-9, underscore
		$slug = ""; $slug_array = str_split(strtolower(strip_tags($newslug)));
		foreach ($slug_array as $char) { if (preg_match("/[\w]/",$char) == 1) { $slug .= $char; } }
		// limit slug to 40 characters and make lowercase
		$slug = substr($slug,0,39);
		// strip any trailing underscores
		$slug = trim($slug,"_");
		
		return $slug;
	}

	function add_type_value($type, $value, $color, $slug = "") {
		// adds a new film type value
		$this->db->set('name',$value);
		switch($type) {
			case "event":
			case "language":
			case "genre":
			case "country":	$this->db->set('slug',$slug); break;
			default: break;
		}
		if ($color != "") { $this->db->set('color',$color); }
		$this->db->insert('wf_type_'.$type);
		return $this->db->insert_id();
	}
	
	function update_type_value($type, $value, $color, $slug = "", $id) {
		// updates a film type value
		$this->db->set('name',$value);
		switch($type) {
			case "event":
			case "language":
			case "genre":
			case "country":	$this->db->set('slug',$slug); break;
		}
		if ($color != "") { $this->db->set('color',$color); }
		$this->db->where('id',$id);
		$this->db->update('wf_type_'.$type);
	}

	function update_type_order($type, $order, $id) {
		// updates a film type order
		$this->db->set('order',$order);
		$this->db->where('id',$id);
		$this->db->update('wf_type_'.$type);
	}

	function delete_type_value($type, $id) {
		// deletes a film type value (no checks)
		$this->db->where('id',$id);
		$this->db->delete('wf_type_'.$type);
	}

	function check_add_film_type($typename, $newTypeList) {
		$color = "";
		if ($typename == "format") { $color == "#999999"; } else { $color == ""; }
		$currentTypeList = $this->filmtype->get_all_type($typename, "asc");
	
		foreach ($newTypeList as $thisType) {
			if ($thisType != "") {
				$exists = false;
				
				foreach ($currentTypeList as $currentType) {
					if ($typename == "country" || $typename == "language" || $typename == "genre") {
						$thisTypeMultiple = explode(", ",$thisType);
						foreach ($thisTypeMultiple as $thisCLG) {
							if (trim($thisCLG) == $currentType->name) { $exists = true; }
						}
					} else {
						if (trim($thisType) == $currentType->name) { $exists = true; }
					}
					//print "'".trim($thisType)."' vs '".$currentType->name."'<br>";
				}
				
				if (!$exists) { 
					//print trim($thisType)." not matched.<br>";
					$slug = $this->filmtype->create_simple_slug($thisType);
					$this->filmtype->add_type_value($typename, $thisType, $color, $slug);
				}
			}
		}	
		
		return $this->filmtype->get_all_type($typename, "asc");
	}

	function check_clg($type, $id) {
		$this->db->select('wf_movie_'.$type.'.id, wf_type_'.$type.'.name');
		$this->db->from('wf_movie_'.$type);
		$this->db->join('wf_type_'.$type, 'wf_movie_'.$type.'.'.$type.'_id = wf_type_'.$type.'.id');
		$this->db->where($type.'_id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function check_movie_value($type, $id) {
		$this->db->select('wf_movie.id, wf_type_'.$type.'.name');
		$this->db->from("wf_movie");
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		if ($type == "event" || $type == "premiere") {
			$this->db->join('wf_type_'.$type, 'wf_festivalmovie.'.$type.'_id = wf_type_'.$type.'.id');
		} else {
			$this->db->join('wf_type_'.$type, 'wf_movie.'.$type.'_id = wf_type_'.$type.'.id');
		}
		$this->db->where($type.'_id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function check_shipment_value($type, $id) {
		$this->db->select('wf_printtraffic_shipments.id, wf_type_'.$type.'.name');
		$this->db->from("wf_printtraffic_shipments");
		$this->db->join('wf_type_'.$type, 'wf_printtraffic_shipments.'.$type.'_id = wf_type_'.$type.'.id');
		$this->db->where($type.'_id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	// Possible bugs with this 'like' clause. i.e. id '1' will match if '10' exists.
	function check_personnel_value($type, $id) {
		$this->db->select('wf_movie_personnel.id, wf_movie_personnel.personnel_roles');
		$this->db->from("wf_movie_personnel");
		//$this->db->join('wf_type_'.$type, 'wf_movie_'.$type.'.'.$type.'_id = wf_type_'.$type.'.id');
		$this->db->like('wf_movie_personnel.personnel_roles', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function replace_clg_value($type, $id_delete, $id_replace) {
		$values = array( $type.'_id' => $id_replace, 'updated_date' => date("Y-m-d H:i:s") );
		$query = $this->db->update('wf_movie_'.$type, $values, $type.'_id = '.$id_delete );
        return $query;
	}

	function replace_movie_value($type, $id_delete, $id_replace) {
		if ($type == "event" || $type == "premiere") {
			$values = array( $type.'_id' => $id_replace );
			$query = $this->db->update('wf_festivalmovie', $values, $type.'_id = '.$id_delete );
		} else {
			$values = array( $type.'_id' => $id_replace, 'updated_date' => date("Y-m-d H:i:s") );
			$query = $this->db->update('wf_movie', $values, $type.'_id = '.$id_delete );
		}
        return $query;
	}

	function replace_shipment_value($type, $id_delete, $id_replace) {
		$values = array( $type.'_id' => $id_replace );
		$query = $this->db->update('wf_printtraffic_shipments', $values, $type.'_id = '.$id_delete );
        return $query;
	}

	function replace_personnel_value($personnel_id, $id_replace, $roles) {
		$values = array( 'type_id' => $id_replace,  'personnel_roles' => $roles, 'updated_date' => date("Y-m-d H:i:s") );
		$query = $this->db->update('wf_movie_personnel', $values, 'id = '.$personnel_id );
        return $query;
	}


}
?>