<?php
class Photovideomodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function load_all_photos()
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_photos($movie_id_array)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$this->db->where_in('wf_movie_photos.movie_id', $movie_id_array);
		$this->db->order_by('wf_movie_photos.order', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_first_photos($movie_id_array)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$this->db->where('wf_movie_photos.order', 1);
		$this->db->where_in('wf_movie_photos.movie_id', $movie_id_array);
		$this->db->order_by('wf_movie_photos.order', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_movie_photos($movie_id)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$this->db->where('wf_movie_photos.movie_id', $movie_id);
		$this->db->order_by('wf_movie_photos.order', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_first_movie_photo($movie_id)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$this->db->where('wf_movie_photos.order', 1);
		$this->db->where('wf_movie_photos.movie_id', $movie_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_movie_photo($photo_id)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_photos');
		$this->db->where('wf_movie_photos.id', $photo_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_videos($movie_id_array)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_videos');
		$this->db->where_in('wf_movie_videos.movie_id', $movie_id_array);
		$this->db->order_by('wf_movie_videos.order', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_movie_videos($movie_id)
    {
		$this->db->select('*');
		$this->db->from('wf_movie_videos');
		$this->db->where('wf_movie_videos.movie_id', $movie_id);
		$this->db->order_by('wf_movie_videos.order', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

}
?>