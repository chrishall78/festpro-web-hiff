<?php
class Genremodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_genres()
    {
		// loads all unique genres from films for list purposes.
		$this->db->select('wf_movie_genre.movie_id, wf_type_genre.id, wf_type_genre.name, wf_type_genre.slug');
		$this->db->from('wf_movie_genre');
		$this->db->join('wf_type_genre', 'wf_movie_genre.genre_id = wf_type_genre.id');
		$this->db->order_by('wf_type_genre.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_genres_in($movie_id_array)
    {
		// loads all unique genres from films for list purposes.
		$this->db->select('wf_movie_genre.movie_id, wf_type_genre.id, wf_type_genre.name, wf_type_genre.slug');
		$this->db->from('wf_movie_genre');
		$this->db->join('wf_type_genre', 'wf_movie_genre.genre_id = wf_type_genre.id');
		$this->db->where_in('wf_movie_genre.movie_id', $movie_id_array);
		$this->db->order_by('wf_type_genre.name', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_movie_genres($movie_id)
    {
		// loads all genres for this specific film.
		$this->db->select('wf_movie_genre.movie_id, wf_movie_genre.id, wf_type_genre.id AS genre_id, wf_type_genre.name, wf_type_genre.slug');
		$this->db->from('wf_movie_genre');
		$this->db->join('wf_type_genre', 'wf_movie_genre.genre_id = wf_type_genre.id');
		$this->db->where('wf_movie_genre.movie_id', $movie_id);
		$this->db->order_by('wf_type_genre.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function add_movie_genre($movie_id)
	{
		// adds a new genre to a movie with no value specified
		$this->db->set('movie_id',$movie_id);
		$this->db->set('genre_id',0);
		$this->db->insert('wf_movie_genre');

        return $this->db->insert_id();
	}
}
?>