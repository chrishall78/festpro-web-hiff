<?php
class Schedulemodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function get_program_by_slug($slug, $start_date, $end_date)
	{
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_name, wf_screening.program_slug, wf_screening.program_desc, wf_screening.program_movie_ids, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.Rush, wf_screening.Free, wf_screening.Private, wf_screening.location_id, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, wf_festivalmovie.festival_id, wf_screeninglocation.name, wf_screeninglocation.displayname');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_screening.program_slug', $slug);
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		$query = $this->db->get();

		return $query->result();
	}

	function get_internal_program_by_slug($slug, $start_date, $end_date)
	{
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_name, wf_screening.program_slug, wf_screening.program_desc, wf_screening.program_movie_ids, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.Rush, wf_screening.Free, wf_screening.Private, wf_screening.location_id, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, wf_festivalmovie.festival_id, wf_screeninglocation.name, wf_screeninglocation.displayname');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_screening.program_slug', $slug);
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$query = $this->db->get();

		return $query->result();
	}

    function get_schedule($start_date, $end_date)
    {
		// loads all published screening times from the passed start and end dates. (public screenings only - all fields)
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_name, wf_screening.program_slug, wf_screening.program_desc, wf_screening.program_movie_ids, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.Rush, wf_screening.Free, wf_screening.Private, wf_screening.location_id, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, wf_festivalmovie.event_id, wf_type_event.slug as event_slug, wf_type_event.color as event_color, wf_screeninglocation.name, wf_screeninglocation.displayname');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_schedule_by_venue($start_date, $end_date, $venue_slug)
    {
		// loads all published screening times from the passed start and end dates for a specific venue. (public screenings only - all fields)
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_name, wf_screening.program_slug, wf_screening.program_movie_ids, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.Rush, wf_screening.Free, wf_screening.Private, wf_screening.location_id, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, wf_festivalmovie.event_id, wf_type_event.slug as event_slug, wf_type_event.color as event_color, wf_screeninglocation.name, wf_screeninglocation.displayname');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		$this->db->where('wf_screeninglocation.slug', $venue_slug);
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}


    function get_movie_schedule($start_date, $end_date, $movie_id)
    {
		// loads all published screening times from the passed start and end dates. (public screenings only - limited fields)
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_movie_ids, wf_screening.program_name, wf_screening.program_slug, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.Rush, wf_screening.Free, wf_screeninglocation.name, wf_screeninglocation.displayname');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		$this->db->like('wf_screening.program_movie_ids', $movie_id);
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_program_ids($start_date, $end_date)
    {
		// loads all published screening times from the passed start and end dates. (public screenings only - all fields)
		$this->db->select('wf_screening.movie_id, wf_screening.program_slug AS value, wf_screening.program_name AS label, wf_screening.movie_id AS runtime_int');
		$this->db->from('wf_screening');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_festivalmovie.Confirmed', '1');
		$this->db->where('wf_festivalmovie.Published', '1');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->where('wf_screening.Published', '1');
		$this->db->where('wf_screening.program_name !=', '');
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}


    function get_internal_schedule($start_date, $end_date)
    {
		// loads all screening times from the passed start and end dates. 
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_movie_ids, wf_screening.program_name, wf_screening.program_slug, wf_screening.program_desc, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.host, wf_screening.notes, wf_screening.Rush, wf_screening.Free, wf_screening.Private, wf_screening.location_id, wf_screening.location_id2, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.sponsor_trailer1_name, wf_screening.sponsor_trailer2_name, wf_screening.AudienceCount, wf_screening.ScreeningRevenue, wf_screening.QandACount, wf_screening.q_and_a_length, wf_screening.total_runtime, wf_screeninglocation.name, wf_screeninglocation.displayname, wf_screeninglocation.seats, wf_movie.slug, wf_movie.format_id, wf_movie.title_en, wf_movie.runtime_int, wf_festivalmovie.category_id, wf_festivalmovie.id as festivalmovie_id');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_movie', 'wf_screening.movie_id = wf_movie.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_internal_schedule_minimal($start_date, $end_date)
    {
		// loads all screening times from the passed start and end dates. (public screenings only) 
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.program_movie_ids, wf_screening.date, wf_screening.time, wf_screening.Private');
		$this->db->from('wf_screening');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->where('wf_screening.Private', '0');
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_internal_movie_screenings($start_date, $end_date, $movie_id)
    {
		// loads all internal screening times from the passed start and end dates. (all fields)
		$this->db->select('wf_screening.id, wf_screening.Published, wf_screening.movie_id, wf_screening.program_movie_ids, wf_screening.program_name, wf_screening.program_slug, wf_screening.date, wf_screening.time, wf_screening.url, wf_screening.host, wf_screening.notes, wf_screening.Private, wf_screening.Rush, wf_screening.Free, wf_screening.location_id, wf_screening.location_id2, wf_screeninglocation.name as location, wf_screening.intro, wf_screening.ScreeningRevenue, wf_screening.AudienceCount, wf_screening.QandACount, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer2, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, , wf_screening.sponsor_trailer1_name, wf_screening.sponsor_trailer2_name');		
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->like('wf_screening.program_movie_ids', $movie_id);
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc');
		$query = $this->db->get();

		return $query->result();
	}

	function get_internal_screening_ids($festival_movie_ids)
	{
		$this->db->select('wf_screening.id as movie_id');
		$this->db->from('wf_screening');
		$this->db->where_in('movie_id', $festival_movie_ids);
		$query = $this->db->get();

		return $query->result();
	}

	function publish_all_public_screenings($screening_id_array, $film_id_array) {
		// Toggle 'Published' setting
		$this->db->set("Published", 1);
		$this->db->where("Private", 0);
		$this->db->where_in("id", $screening_id_array);
		$this->db->update("wf_screening");

		// Set 'Updated Date' for all these films
		$this->db->set("updated_date", date("Y-m-d H:i:s"));
		$this->db->where_in("id", $film_id_array);
		$this->db->update("wf_movie");
	}

	function unpublish_all_screenings($program_movie_ids) {
		$this->db->set("Published",0);
		$this->db->where_in("id", $program_movie_ids);
		$this->db->update("wf_screening");
	}




    function get_screening_toggle($id)
    {
		// load one specific film
		$this->db->select('program_movie_ids,Private,Rush,Free,Published');
		$this->db->from('wf_screening');
		$this->db->where('wf_screening.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

	function val_on($screening_id, $table_name, $field_name)
	{
        $this->db->where('id', $screening_id);
        $this->db->update($table_name, array($field_name => '1'));
	}

	function val_off($screening_id, $table_name, $field_name)
	{
        $this->db->where('id', $screening_id);
        $this->db->update($table_name, array($field_name => '0'));
	}


	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
	}

	function delete_value($id, $tablename) {
		// deletes a record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}

}
?>