<?php
class Reportsmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /*  FILM MODULE REPORTS  */
    function get_ballot_counts($festival_id)
    {
		// loads and calculates all the physical votes for films from the specified festival.
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,wf_festivalmovie.premiere_id,wf_festivalmovie.event_id,category_id,title,title_en,wf_type_event.name AS event_name, wf_festivalmovie.id as house_seats, wf_movie.slug, (votes_1+votes_2+votes_3+votes_4+votes_5) AS votes_num, votes_1,votes_2,votes_3,votes_4,votes_5, ((votes_1*-3)+(votes_2*-2)+(votes_3)+(votes_4*2)+(votes_5*3)) AS votes_total, wf_festivalmovie.id as adjusted_total');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->order_by('event_name', 'asc');
		$this->db->order_by('title_en', 'asc');
		$this->db->group_by('title_en');
		$query = $this->db->get();

        return $query->result();
	}

    function get_rating_counts($festival_id)
    {
		// loads and calculates all the online audience ratings for films from the specified festival + film page views.
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,wf_festivalmovie.category_id,wf_movie.title_en,wf_festivalmovie.category_id as section,wf_type_event.name AS event_name,wf_movie.slug,wf_movie_ratings.total_votes, wf_movie_ratings.total_value, (wf_movie_ratings.total_value / wf_movie_ratings.total_votes) AS rating, wf_movie_ratings.used_ips, wf_movie_views.web, wf_movie_views.facebook, wf_movie_views.mobile');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->join('wf_movie_ratings', 'wf_movie_ratings.id = wf_movie.slug', 'left');
		$this->db->join('wf_movie_views', 'wf_movie.id = wf_movie_views.movie_id', 'left');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->order_by('web', 'desc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_profit_loss($festival_id)
    {
		// selects all of the festival revenue and expenses fields for a profit/loss statement.
		$this->db->select('wf_festivalmovie.id,wf_festivalmovie.movie_id,wf_festivalmovie.category_id,wf_movie.title_en,wf_festivalmovie.category_id as section,wf_type_event.name AS genre,wf_movie.slug,wf_screening.program_movie_ids,wf_screening.program_name,SUM(wf_screening.ScreeningRevenue) AS FilmRevenue,wf_festivalmovie.RentalFee, wf_festivalmovie.ScreeningFee, wf_festivalmovie.TransferFee, wf_festivalmovie.GuestFee');
		$this->db->from('wf_movie');
		$this->db->join('wf_festivalmovie', 'wf_movie.id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_event', 'wf_festivalmovie.event_id = wf_type_event.id', 'left');
		$this->db->join('wf_screening', 'wf_screening.movie_id = wf_movie.id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->group_by('wf_movie.title_en');
		$this->db->order_by('wf_movie.title_en', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_profit_loss_shorts($festival_id, $movie_id_array)
    {
		// selects all of the movie ids for screenings with more than one film.
		$this->db->select('wf_screening.movie_id,wf_screening.program_movie_ids,wf_screening.program_name');
		$this->db->from('wf_screening');
		$this->db->where_in('wf_screening.movie_id', $movie_id_array);
		$this->db->where('CAST(wf_screening.movie_id AS CHAR) != wf_screening.program_movie_ids');
		$query = $this->db->get();
        return $query->result();
	}

	function get_profit_loss_expenses($movie_id_array) {
		// selects all of the expenses for screenings with more than one film.
		$this->db->select('wf_festivalmovie.movie_id, wf_festivalmovie.RentalFee, wf_festivalmovie.ScreeningFee, wf_festivalmovie.TransferFee, wf_festivalmovie.GuestFee');
		$this->db->from('wf_festivalmovie');
		$this->db->where_in('wf_festivalmovie.movie_id', $movie_id_array);
		$query = $this->db->get();
        return $query->result();
	}

    function get_profit_loss_shipping($festival_id)
    {
		// specifically pulls out the shipping fees since they make the statement above too complex.
		$this->db->select('wf_festivalmovie.id, wf_festivalmovie.movie_id, SUM(wf_printtraffic_shipments.ShippingFee) AS ShippingFee');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_printtraffic_shipments', 'wf_printtraffic_shipments.festivalmovie_id = wf_festivalmovie.id');
		$this->db->where('wf_festivalmovie.festival_id', $festival_id);
		$this->db->group_by('wf_festivalmovie.id');
		$query = $this->db->get();

        return $query->result();
	}

    function get_internal_schedule_export($start_date, $end_date)
    {
		// loads all screening times from the passed start and end dates.
		$this->db->select('wf_screening.id, wf_screening.movie_id, wf_screening.program_movie_ids, wf_screening.program_name, wf_screening.date, wf_screening.time, wf_screening.host, wf_screening.notes, wf_screening.Private, wf_screening.Rush, wf_screening.Free, wf_screening.location_id, wf_screening.location_id2, wf_screeninglocation.name as location, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer1_name, wf_screening.sponsor_trailer2, wf_screening.sponsor_trailer2_name, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length, wf_screening.total_runtime, wf_movie.title_en, wf_movie.runtime_int, wf_type_format.name as format_name, wf_festivalmovie.colorbar_end_time, wf_festivalmovie.titleid_end_time, wf_festivalmovie.in_time, wf_festivalmovie.end_credits_time, wf_festivalmovie.out_time');
		$this->db->from('wf_screening');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->join('wf_movie', 'wf_screening.movie_id = wf_movie.id');
		$this->db->join('wf_festivalmovie', 'wf_screening.movie_id = wf_festivalmovie.movie_id');
		$this->db->join('wf_type_format', 'wf_movie.format_id = wf_type_format.id', 'left');
		$this->db->where('wf_screening.date >=', $start_date);
		$this->db->where('wf_screening.date <=', $end_date);
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	// Print Traffic Shipment Exports

    function get_pt_shipments($type, $fest_id) {
		// loads all film data; Sorted by English Title.

		switch($type) {
			case "inbound": $this->db->select('wf_movie.id as movie_id, wf_movie.title_en as EnglishTitle, wf_movie.runtime as Screenings, wf_printtraffic_shipments.outbound AS Outbound, wf_printtraffic.InboundContact, wf_printtraffic.InboundEmail, wf_printtraffic.InboundPhone, wf_printtraffic.InboundNotes, wf_printtraffic_shipments.InShipped AS ShipDate, wf_printtraffic_shipments.InExpectedArrival AS ExpectedArrivalDate, wf_printtraffic_shipments.InReceived AS ReceivedDate, wf_printtraffic_shipments.courier_id AS Courier, wf_printtraffic_shipments.ShippingFee, wf_printtraffic_shipments.AccountNum AS AccountNumber, wf_printtraffic_shipments.TrackingNum AS TrackingNumber, wf_printtraffic_shipments.notes');
				break;
			case "outbound": $this->db->select('wf_movie.id as movie_id, wf_movie.title_en as EnglishTitle, wf_movie.runtime as Screenings, wf_printtraffic_shipments.outbound AS Outbound, wf_printtraffic.OutboundContact, wf_printtraffic.OutboundEmail, wf_printtraffic.OutboundPhone, wf_printtraffic.OutboundNotes, wf_printtraffic_shipments.OutScheduleBy AS ScheduleByDate, wf_printtraffic_shipments.OutShipBy AS ShipByDate, wf_printtraffic_shipments.OutShipped AS ActualShippedDate, wf_printtraffic_shipments.OutArriveBy AS ArriveByDate, wf_printtraffic_shipments.courier_id AS Courier, wf_printtraffic_shipments.ShippingFee, wf_printtraffic_shipments.AccountNum AS AccountNumber, wf_printtraffic_shipments.TrackingNum AS TrackingNumber, wf_printtraffic_shipments.notes');
				break;
			default: break;
		}
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_printtraffic', 'wf_festivalmovie.id = wf_printtraffic.festivalmovie_id');
		$this->db->join('wf_printtraffic_shipments', 'wf_festivalmovie.id = wf_printtraffic_shipments.festivalmovie_id');

		switch($type) {
			case "inbound": $this->db->where('wf_printtraffic_shipments.outbound', 0);
				break;
			case "outbound": $this->db->where('wf_printtraffic_shipments.outbound', 1);
				break;
			default: break;
		}
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->group_by('wf_movie.title_en');
		$this->db->order_by('wf_movie.title_en asc'); 
		$query = $this->db->get();

        return $query->result();
	}


	// Guide Exports

    function get_all_films_export($fest_id) {
		// loads all film data; Sorted by English Title.
		$this->db->select('wf_movie.id as movie_id, wf_movie.title_en as EnglishTitle, wf_movie.title as OriginalTitle, wf_category.name as Category, wf_movie.runtime as Countries, wf_movie.runtime as Languages, wf_movie.runtime as Genres, wf_movie.year as Year, wf_type_format.name as Format, wf_movie.runtime_int as Runtime, wf_movie.synopsis_short as ShortSynopsis, wf_movie.writerShort as ShortWriterCredit, wf_movie.synopsis as LongSynopsis, wf_movie.writerLong as LongWriterCredit, wf_movie_personnel.personnel_roles as Director, wf_movie_personnel.personnel_roles as Producer, wf_movie_personnel.personnel_roles as Cinematographer, wf_movie_personnel.personnel_roles as Scriptwriter, wf_movie_personnel.personnel_roles as Cast, wf_type_premiere.name as Premiere, wf_movie.runtime as Screenings, wf_festivalmovie.myhiff_1 as SimilarFilms1, wf_festivalmovie.myhiff_2 as SimilarFilms2, wf_festivalmovie.myhiff_3 as SimilarFilms3, wf_festivalmovie.myhiff_4 as SimilarFilms4');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_category', 'wf_festivalmovie.category_id = wf_category.id', 'left');
		$this->db->join('wf_type_format', 'wf_movie.format_id = wf_type_format.id', 'left');
		$this->db->join('wf_type_premiere', 'wf_festivalmovie.premiere_id = wf_type_premiere.id', 'left');
		$this->db->join('wf_movie_personnel', 'wf_movie_personnel.movie_id = wf_movie.id', 'left');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->group_by('wf_movie.title_en');
		$this->db->order_by('wf_movie.title_en asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_table_of_contents_export($fest_id) {
		// loads all film titles and distributor information; Sorted by English Title.
		$this->db->select('wf_category.id as category_id, wf_category.name as Name, wf_category.Description');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_category', 'wf_festivalmovie.category_id = wf_category.id', 'left');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->group_by('wf_category.name');
		$this->db->order_by('wf_category.name asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_table_of_contents_films($fest_id, $cat_id) {
		// loads all film titles and distributor information; Sorted by English Title.
		$this->db->select('wf_movie.title_en, wf_festivalmovie.PBPage, wf_festivalmovie.UGPage');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->where('wf_festivalmovie.category_id', $cat_id);
		$this->db->order_by('wf_movie.title_en asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_printsource_index_export($fest_id) {
		// loads all film titles and distributor information; Sorted by English Title.
		$this->db->select('wf_movie.title_en as EnglishTitle, wf_movie.distributor as Name, wf_movie.distributorContact as Contact, wf_movie.distributorAddress as Address, wf_movie.distributorPhone as Phone, wf_movie.distributorFax as Fax, wf_movie.distributorEmail as Email');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->group_by('wf_movie.title_en');
		$this->db->order_by('wf_movie.title_en asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_country_index_export($fest_id) {
		// loads all countries, film titles and the corresponding guide pages; Sorted by Country.
		$this->db->select('wf_type_country.name as Country, wf_movie.title_en as EnglishTitle, wf_festivalmovie.PBPage as ProgramBookPage, wf_festivalmovie.UGPage as UserGuidePage');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_movie_country', 'wf_movie_country.movie_id = wf_movie.id');
		$this->db->join('wf_type_country', 'wf_movie_country.country_id = wf_type_country.id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->order_by('wf_type_country.name asc, wf_movie.title_en asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_director_index_export($fest_id) {
		// loads all director names, film titles and the corresponding guide pages; Sorted by Director's Last Name.
		$this->db->select('wf_personnel.lastname as DirectorLastName, wf_personnel.name as DirectorFirstName, wf_movie.title_en as EnglishTitle, wf_festivalmovie.PBPage as ProgramBookPage, wf_festivalmovie.UGPage as UserGuidePage');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_movie_personnel', 'wf_movie_personnel.movie_id = wf_movie.id');
		$this->db->join('wf_personnel', 'wf_personnel.id = wf_movie_personnel.personnel_id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->where('wf_movie_personnel.type_id', 1);
		//$this->db->where('wf_movie_personnel.personnel_roles LIKE "%1%"');
		$this->db->order_by('wf_personnel.lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_index_export($fest_id) {
		// loads all film titles and the corresponding guide pages; Sorted by English Title.
		$this->db->select('wf_movie.title_en as EnglishTitle, wf_movie.title as OriginalTitle, wf_festivalmovie.PBPage as ProgramBookPage, wf_festivalmovie.UGPage as UserGuidePage');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->group_by('wf_movie.title_en');
		$this->db->order_by('wf_movie.title_en asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_schedule_export($fest_id) {
		// loads film schedule; Sorted by date.
		$this->db->select('wf_screening.date as Date, wf_screening.time as StartTime, wf_screening.time as EndTime, wf_movie.title_en as Title, wf_movie.runtime_int as Runtime, wf_screeninglocation.name as Countries, wf_screeninglocation.name as Venue, wf_category.name as Category, wf_festivalmovie.PBPage as ProgramBookPage, wf_festivalmovie.UGPage as UserGuidePage, wf_screening.location_id2, wf_festivalmovie.movie_id, wf_screening.program_name, wf_screening.program_movie_ids, wf_festivalmovie.myhiff_1 as SimilarFilms1, wf_festivalmovie.myhiff_2 as SimilarFilms2, wf_festivalmovie.myhiff_3 as SimilarFilms3, wf_festivalmovie.myhiff_4 as SimilarFilms4, wf_screening.intro, wf_screening.fest_trailer, wf_screening.sponsor_trailer1, wf_screening.sponsor_trailer1_name, wf_screening.sponsor_trailer2, wf_screening.sponsor_trailer2_name, wf_screening.q_and_a, wf_screening.intro_length, wf_screening.fest_trailer_length, wf_screening.sponsor_trailer1_length, wf_screening.sponsor_trailer2_length, wf_screening.q_and_a_length');
		$this->db->from('wf_screening');
		$this->db->join('wf_festivalmovie', 'wf_festivalmovie.movie_id = wf_screening.movie_id');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_category', 'wf_festivalmovie.category_id = wf_category.id', 'left');
		$this->db->join('wf_screeninglocation', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->where('wf_festivalmovie.Confirmed', 1);
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->where('wf_screening.Private', 0);
		$this->db->where('wf_screening.date !=', '0000-00-00');
		$this->db->order_by('wf_screening.date asc, wf_screening.time asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_schedule_program_export($movie_id) {
		// loads all film titles and distributor information; Sorted by English Title.
		$this->db->select('wf_movie.title_en, wf_movie.runtime_int');
		$this->db->from('wf_movie');
		$this->db->where('wf_movie.id', $movie_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_trailers_export($fest_id) {
		// loads all film titles and distributor information; Sorted by English Title.
		$this->db->select('wf_movie.title_en as EnglishTitle, wf_movie_videos.url_video as VideoUrl');
		$this->db->from('wf_festivalmovie');
		$this->db->join('wf_movie', 'wf_festivalmovie.movie_id = wf_movie.id');
		$this->db->join('wf_movie_videos', 'wf_festivalmovie.movie_id = wf_movie_videos.movie_id');
		$this->db->where('wf_festivalmovie.festival_id', $fest_id);
		$this->db->order_by('wf_movie.title_en asc');
		$query = $this->db->get();

        return $query->result();
	}


    function get_film_review_export($fest_id) {
		// loads all film data sheets for this festival.
		$this->db->select('wf_movie_staging.EnglishTitle, wf_movie_staging.OriginalLanguageTitle, wf_movie_staging.Year, wf_movie_staging.RunTime, wf_movie_staging.Synopsis, wf_movie_staging.Country, wf_movie_staging.Language, wf_movie_staging.Genre, wf_movie_staging.ExhibitionFormat, wf_movie_staging.AspectRatio, wf_movie_staging.Sound, wf_movie_staging.Color, wf_movie_staging.FootageLength, wf_movie_staging.FootageMeasure, wf_movie_staging.FootageType, wf_movie_staging.InsuranceValue, wf_movie_staging.Personnel, wf_movie_staging.PreviousScreenings, wf_movie_staging.Website, wf_movie_staging.HawaiiConnections, wf_movie_staging.distributorType, wf_movie_staging.distributorName, wf_movie_staging.distributorAddress, wf_movie_staging.distributorPhone, wf_movie_staging.distributorFax, wf_movie_staging.distributorEmail, wf_movie_staging.PressContact, wf_movie_staging.PressPhone, wf_movie_staging.PressFax, wf_movie_staging.PressEmail, wf_movie_staging.PressWebsite, wf_movie_staging.Shipping_In_Name, wf_movie_staging.Shipping_In_Address, wf_movie_staging.Shipping_In_Phone, wf_movie_staging.Shipping_In_Fax, wf_movie_staging.Shipping_In_Email, wf_movie_staging.Shipping_In_Date, wf_movie_staging.Shipping_Out_Name, wf_movie_staging.Shipping_Out_Address, wf_movie_staging.Shipping_Out_Phone, wf_movie_staging.Shipping_Out_Fax, wf_movie_staging.Shipping_Out_Email, wf_movie_staging.Shipping_Out_Date, wf_movie_staging.SubmittingName, wf_movie_staging.SubmittingDate');
		$this->db->from('wf_movie_staging');
		$this->db->where('wf_movie_staging.festival_id', $fest_id);
		$this->db->order_by('wf_movie_staging.SubmittingDate asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    /*  GUEST MODULE REPORTS  */

	function get_guest_allinfo_export($fest_id) {
		// loads all guest, flight & hotel information for this festival.
		$this->db->select('fg_filmaffiliations.movie_id, fg_festivalguests.id as Guest_ID, fg_festivalguests.Firstname as First_Name, fg_festivalguests.MI, fg_festivalguests.Lastname as Last_Name, fg_type_guests.name as Guest_Type, fg_festivalguests.AffiliationOptional as Film_Or_Company, fg_festivalguests.id as Program_Title,  fg_festivalguests.id as Film_Title, fg_festivalguests.Address, fg_festivalguests.City, fg_festivalguests.State, fg_festivalguests.PostalCode as Postal_Code, fg_festivalguests.Phone, fg_festivalguests.Cell as Mobile, fg_festivalguests.Fax, fg_festivalguests.Email, fg_festivalguests.TwitterName as Twitter_Name, fg_festivalguests.WebsiteURL as Website_URL, fg_festivalguests.Bio, fg_festivalguests.notes as Guest_Notes, fg_flights.Airline as Arrival_Airline, fg_flights.ArrivalCity as Arrival_City_From, fg_flights.ArrivalCity2 as Arrival_City_To, fg_flights.ArrivalDate as Arrival_Date, fg_flights.ArrivalTime as Arrival_Time, fg_flights.FlightNumber as Arrival_Flight_Number, fg_flights.DepartureAirline as Departure_Airline, fg_flights.DepartureCity as Departure_City_From, fg_flights.DepartureCity2 as Departure_City_To, fg_flights.DepartureDate as Departure_Date, fg_flights.DepartureTime as Departure_Time, fg_flights.DepartureFlightNum as Departure_Flight_Number, fg_flights.cost as Cost, fg_flights.mileage as Airline_Miles, fg_flights.pickup as Flight_Pickup, fg_flights.translator as Flight_Translator, fg_flights.Notes as Flight_Notes, fg_accomodations.hotel as Hotel_Name, fg_accomodations.checkin as Check_In_Date, fg_accomodations.checkout as Check_Out_Date, fg_accomodations.checkouttime as Check_Out_Time, fg_accomodations.compnights as Comp_Nights, fg_accomodations.paidnights as Paid_Nights, fg_accomodations.pickup as Hotel_Pickup, fg_accomodations.translator as Hotel_Translator, fg_accomodations.notes as Hotel_Notes, fg_festivalguests.updated_date as Last_Updated');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->join('fg_flights', 'fg_flights.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->join('fg_accomodations', 'fg_accomodations.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->order_by('fg_festivalguests.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_guest_export($fest_id) {
		// loads all guest information for this festival.
		$this->db->select('fg_filmaffiliations.movie_id, fg_festivalguests.id as Guest_ID, fg_festivalguests.Firstname as First_Name, fg_festivalguests.MI, fg_festivalguests.Lastname as Last_Name, fg_type_guests.name as Guest_Type, fg_festivalguests.AffiliationOptional as Film_Or_Company, fg_festivalguests.id as Program_Title, fg_festivalguests.id as Film_Title, fg_festivalguests.Address, fg_festivalguests.City, fg_festivalguests.State, fg_festivalguests.PostalCode as Postal_Code, fg_festivalguests.Phone, fg_festivalguests.Cell as Mobile, fg_festivalguests.Fax, fg_festivalguests.Email, fg_festivalguests.TwitterName as Twitter_Name, fg_festivalguests.WebsiteURL as Website_URL, fg_festivalguests.Bio, fg_festivalguests.notes as Notes, fg_festivalguests.updated_date as Last_Updated');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->order_by('fg_festivalguests.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_guest_byprogram_export($fest_id, $movie_id_array) {
		// loads all guest, flight & hotel information for this festival.
		$this->db->select('fg_filmaffiliations.movie_id, fg_festivalguests.id as Guest_ID, fg_festivalguests.Firstname as First_Name, fg_festivalguests.MI, fg_festivalguests.Lastname as Last_Name, fg_type_guests.name as Guest_Type, fg_festivalguests.AffiliationOptional as Film_Or_Company, fg_festivalguests.id as Program_Title, wf_movie.title_en as Film_Title, fg_festivalguests.Address, fg_festivalguests.City, fg_festivalguests.State, fg_festivalguests.PostalCode as Postal_Code, fg_festivalguests.Phone, fg_festivalguests.Cell as Mobile, fg_festivalguests.Fax, fg_festivalguests.Email, fg_festivalguests.TwitterName as Twitter_Name, fg_festivalguests.WebsiteURL as Website_URL, fg_festivalguests.Bio, fg_festivalguests.notes as Guest_Notes, fg_flights.Airline as Arrival_Airline, fg_flights.ArrivalCity as Arrival_City_From, fg_flights.ArrivalCity2 as Arrival_City_To, fg_flights.ArrivalDate as Arrival_Date, fg_flights.ArrivalTime as Arrival_Time, fg_flights.FlightNumber as Arrival_Flight_Number, fg_flights.DepartureAirline as Departure_Airline, fg_flights.DepartureCity as Departure_City_From, fg_flights.DepartureCity2 as Departure_City_To, fg_flights.DepartureDate as Departure_Date, fg_flights.DepartureTime as Departure_Time, fg_flights.DepartureFlightNum as Departure_Flight_Number, fg_flights.cost as Cost, fg_flights.mileage as Airline_Miles, fg_flights.pickup as Flight_Pickup, fg_flights.translator as Flight_Translator, fg_flights.Notes as Flight_Notes, fg_accomodations.hotel as Hotel_Name, fg_accomodations.checkin as Check_In_Date, fg_accomodations.checkout as Check_Out_Date, fg_accomodations.checkouttime as Check_Out_Time, fg_accomodations.compnights as Comp_Nights, fg_accomodations.paidnights as Paid_Nights, fg_accomodations.pickup as Hotel_Pickup, fg_accomodations.translator as Hotel_Translator, fg_accomodations.notes as Hotel_Notes, fg_festivalguests.updated_date as Last_Updated');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id');
		$this->db->join('wf_movie', 'wf_movie.id = fg_filmaffiliations.movie_id');
		$this->db->join('fg_flights', 'fg_flights.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->join('fg_accomodations', 'fg_accomodations.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->where_in('fg_filmaffiliations.movie_id', $movie_id_array);
		$this->db->order_by('fg_festivalguests.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_guest_byfilm_export($fest_id, $movie_id) {
		// loads all guest, flight & hotel information for guests afilliated with this film.
		$this->db->select('fg_filmaffiliations.movie_id, fg_festivalguests.id as Guest_ID, fg_festivalguests.Firstname as First_Name, fg_festivalguests.MI, fg_festivalguests.Lastname as Last_Name, fg_type_guests.name as Guest_Type, fg_festivalguests.AffiliationOptional as Film_Or_Company, fg_festivalguests.id as Program_Title, wf_movie.title_en as Film_Title, fg_festivalguests.Address, fg_festivalguests.City, fg_festivalguests.State, fg_festivalguests.PostalCode as Postal_Code, fg_festivalguests.Phone, fg_festivalguests.Cell as Mobile, fg_festivalguests.Fax, fg_festivalguests.Email, fg_festivalguests.TwitterName as Twitter_Name, fg_festivalguests.WebsiteURL as Website_URL, fg_festivalguests.Bio, fg_festivalguests.notes as Guest_Notes, fg_flights.Airline as Arrival_Airline, fg_flights.ArrivalCity as Arrival_City_From, fg_flights.ArrivalCity2 as Arrival_City_To, fg_flights.ArrivalDate as Arrival_Date, fg_flights.ArrivalTime as Arrival_Time, fg_flights.FlightNumber as Arrival_Flight_Number, fg_flights.DepartureAirline as Departure_Airline, fg_flights.DepartureCity as Departure_City_From, fg_flights.DepartureCity2 as Departure_City_To, fg_flights.DepartureDate as Departure_Date, fg_flights.DepartureTime as Departure_Time, fg_flights.DepartureFlightNum as Departure_Flight_Number, fg_flights.cost as Cost, fg_flights.mileage as Airline_Miles, fg_flights.pickup as Flight_Pickup, fg_flights.translator as Flight_Translator, fg_flights.Notes as Flight_Notes, fg_accomodations.hotel as Hotel_Name, fg_accomodations.checkin as Check_In_Date, fg_accomodations.checkout as Check_Out_Date, fg_accomodations.checkouttime as Check_Out_Time, fg_accomodations.compnights as Comp_Nights, fg_accomodations.paidnights as Paid_Nights, fg_accomodations.pickup as Hotel_Pickup, fg_accomodations.translator as Hotel_Translator, fg_accomodations.notes as Hotel_Notes, fg_festivalguests.updated_date as Last_Updated');
		$this->db->from('fg_festivalguests');
		$this->db->join('fg_type_guests', 'fg_festivalguests.guesttype_id = fg_type_guests.id');
		$this->db->join('fg_filmaffiliations', 'fg_filmaffiliations.festivalguest_id = fg_festivalguests.id');
		$this->db->join('wf_movie', 'wf_movie.id = fg_filmaffiliations.movie_id');
		$this->db->join('fg_flights', 'fg_flights.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->join('fg_accomodations', 'fg_accomodations.festivalguest_id = fg_festivalguests.id', 'left');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->where('fg_filmaffiliations.movie_id', $movie_id);
		$this->db->order_by('fg_festivalguests.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}


	function get_flights_export($fest_id) {
		// loads all guest flight information for this festival.
		$this->db->select('fg_flights.Firstname as First_Name, fg_flights.MI, fg_flights.Lastname as Last_Name, fg_flights.Airline as Arrival_Airline, fg_flights.ArrivalCity as Arrival_City_From, fg_flights.ArrivalCity2 as Arrival_City_To, fg_flights.ArrivalDate as Arrival_Date, fg_flights.ArrivalTime as Arrival_Time, fg_flights.FlightNumber as Arrival_Flight_Number, fg_flights.DepartureAirline as Departure_Airline, fg_flights.DepartureCity as Departure_City_From, fg_flights.DepartureCity2 as Departure_City_To, fg_flights.DepartureDate as Departure_Date, fg_flights.DepartureTime as Departure_Time, fg_flights.DepartureFlightNum as Departure_Flight_Number, fg_flights.cost as Cost, fg_flights.mileage as Airline_Miles, fg_flights.pickup as Needs_Pickup, fg_flights.translator as Needs_Translator, fg_flights.Notes');
		$this->db->from('fg_flights');
		$this->db->join('fg_festivalguests', 'fg_flights.festivalguest_id = fg_festivalguests.id');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->order_by('fg_flights.DepartureDate asc'); 
		$this->db->order_by('fg_flights.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}

	function get_hotels_export($fest_id) {
		// loads all guest hotel information for this festival.
		$this->db->select('fg_festivalguests.Firstname as First_Name, fg_festivalguests.MI, fg_festivalguests.Lastname as Last_Name, fg_accomodations.hotel as Hotel_Name, fg_accomodations.checkin as Check_In_Date, fg_accomodations.checkout as Check_Out_Date, fg_accomodations.checkouttime as Check_Out_Time, fg_accomodations.compnights as Comp_Nights, fg_accomodations.paidnights as Paid_Nights, fg_accomodations.pickup as Needs_Pickup, fg_accomodations.translator as Needs_Translator, fg_accomodations.notes as Notes');
		$this->db->from('fg_accomodations');
		$this->db->join('fg_festivalguests', 'fg_accomodations.festivalguest_id = fg_festivalguests.id');
		$this->db->where('fg_festivalguests.festival_id', $fest_id);
		$this->db->order_by('fg_accomodations.checkin asc'); 
		$this->db->order_by('fg_festivalguests.Lastname asc'); 
		$query = $this->db->get();

        return $query->result();
	}


    /*  COMMON FUNCTIONS  */

	function csv_download($report, $file_name) {		
		$this->load->helper('download');
		
		$csv_file = ""; $delim = ","; $newline = "\n"; $enclosure = '"';

		foreach ($report as $thisRecord) { $report_vars = get_object_vars($thisRecord); $report_keys = array_keys($report_vars); break; }

		foreach ($report_keys as $name) {
			$csv_file .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $name).$enclosure.$delim;
		}
		$csv_file = rtrim($csv_file);
		$csv_file .= $newline;

		foreach ($report as $thisRecord) {
			foreach ($report_keys as $item)
			{
				if ($item == "ShortSynopsis" || $item == "LongSynopsis") {
					$search = array('&nbsp;', '&rsquo;', '&lsquo;', '&#39;', '&ldquo;', '&rdquo;', '&quot;', '&ndash;', '&mdash;', '&not;', '&hellip;', '&amp;');
					$replace = array(' ', '\'', '\'', '\'', '"', '"', '"', '-', '-', '-', '...', '&');
					$synopsis = strip_tags($thisRecord->$item);
					$synopsis = str_replace($search, $replace, $synopsis);
					//$synopsis = html_entity_decode($synopsis, ENT_QUOTES, "UTF-8");
					$synopsis = trim($synopsis);
					$csv_file .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $synopsis).$enclosure.$delim;
				} else {
					$csv_file .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $thisRecord->$item).$enclosure.$delim;
				}
			}
			$csv_file = rtrim($csv_file);
			$csv_file .= $newline;
		}
		
		force_download($file_name,$csv_file);
	}

}
?>