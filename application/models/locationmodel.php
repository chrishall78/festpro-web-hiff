<?php
class Locationmodel extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_locations()
    {
		// loads all screening locations.
		$this->db->select('*');
		$this->db->from('wf_screeninglocation');
		$this->db->order_by('wf_screeninglocation.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_all_venues()
    {
		// loads all screening venues.
		$this->db->select('*');
		$this->db->from('wf_screeningvenue');
		$this->db->order_by('wf_screeningvenue.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

	function get_festival_locations($loc_array)
	{
		// loads screening locations sorted by audience display name.
		$this->db->select('wf_screeninglocation.id, wf_screeninglocation.slug, wf_screeninglocation.name, wf_screeninglocation.displayname, wf_screeninglocation.format, wf_screeninglocation.color, wf_screeninglocation.seats');
		$this->db->distinct(); 
		$this->db->from('wf_screeninglocation');
		$this->db->join('wf_screening', 'wf_screening.location_id = wf_screeninglocation.id');
		$this->db->where_in('wf_screeninglocation.id',$loc_array);
		$this->db->where('wf_screening.Published', 1);
		$this->db->order_by('wf_screeninglocation.displayname', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

	function get_internal_festival_locations($loc_array)
	{
		// loads screening locations sorted by internal name.
		$this->db->select('*');
		$this->db->from('wf_screeninglocation');
		$this->db->where_in('id',$loc_array);
		$this->db->order_by('wf_screeninglocation.name', 'asc');
		$query = $this->db->get();

        return $query->result();
	}

    function get_location($id)
    {
		// loads one screening location.
		$this->db->select('*');
		$this->db->from('wf_screeninglocation');
		$this->db->where('wf_screeninglocation.id', $id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_location_except($id, $order = "asc")
    {
		$this->db->select('*');
		$this->db->from("wf_screeninglocation");
		$this->db->where('id !=', $id);
		$this->db->order_by('name',$order);
		$query = $this->db->get();

        return $query->result();
	}

	function check_location_screening_value($location_id)
	{
		// Check if location is being used in any screenings - as location_id -or- location_id2
		$this->db->select('wf_screening.id, wf_screening.location_id, wf_screening.location_id2');
		$this->db->from('wf_screening');
		$this->db->where('wf_screening.location_id', $location_id);
		$this->db->or_where('wf_screening.location_id2', $location_id);
		$query = $this->db->get();

        return $query->result();
	}

	function check_location_festival_value($location_id)
	{
		// Check if location is being used in any festivals
		$this->db->select('wf_festival.id, wf_festival.name, wf_festival.year, wf_festival.locations');
		$this->db->from('wf_festival');
		$this->db->like('wf_festival.locations', $location_id);
		$query = $this->db->get();

        return $query->result();
	}

	function replace_location_value_screening1($id_delete, $id_replace)
	{
		$values = array( 'location_id' => $id_replace );
		$query = $this->db->update('wf_screening', $values, 'location_id = '.$id_delete );
        return $query;
	}

	function replace_location_value_screening2($id_delete, $id_replace)
	{
		$values = array( 'location_id2' => $id_replace );
		$query = $this->db->update('wf_screening', $values, 'location_id2 = '.$id_delete );
        return $query;
	}

	function replace_location_value_festival($id, $locations)
	{
		$values = array( 'locations' => $locations );
		$query = $this->db->update('wf_festival', $values, 'id = '.$id );
        return $query;
	}




}
?>