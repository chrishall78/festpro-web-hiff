<?php
class Personnelmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_personnel($movie_id_array, $search_string)
    {
		// loads all personnel from these films
		$this->db->select('wf_movie_personnel.id as personnel_id, wf_movie_personnel.movie_id, wf_type_personnel.id, wf_type_personnel.name AS role, wf_movie_personnel.personnel_roles, wf_personnel.name, wf_personnel.lastname');
		$this->db->from('wf_movie_personnel');
		$this->db->join('wf_type_personnel', 'wf_movie_personnel.type_id = wf_type_personnel.id');
		$this->db->join('wf_personnel', 'wf_personnel.id = wf_movie_personnel.personnel_id');
		$this->db->where_in('wf_movie_personnel.movie_id', $movie_id_array);
		if ($search_string != "") {
			$this->db->where('wf_type_personnel.name', $search_string);
		}
		$this->db->order_by('wf_type_personnel.name', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_movie_personnel($movie_id, $search_string)
    {
		// loads all personnel for this specific film.
		$this->db->select('wf_movie_personnel.movie_id, wf_movie_personnel.id, wf_type_personnel.name AS role, wf_movie_personnel.personnel_roles, wf_type_personnel.id AS type_id, wf_personnel.name, wf_personnel.lastname, wf_personnel.lastnamefirst');
		$this->db->from('wf_movie_personnel');
		$this->db->join('wf_type_personnel', 'wf_movie_personnel.type_id = wf_type_personnel.id');
		$this->db->join('wf_personnel', 'wf_personnel.id = wf_movie_personnel.personnel_id');
		$this->db->where('wf_movie_personnel.movie_id', $movie_id);
		if ($search_string != "") {
			$this->db->where('wf_type_personnel.name', $search_string);
		}
		$this->db->order_by('wf_type_personnel.order', 'asc');
		$this->db->order_by('wf_personnel.lastname', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_qa_personnel($movie_id, $screening_id)
    {
		// loads all personnel for this specific film.
		$this->db->select('wf_movie_personnel.movie_id, wf_movie_personnel.id, wf_type_personnel.name AS role, wf_movie_personnel.qa_appearances, wf_movie_personnel.personnel_roles, wf_type_personnel.id AS type_id, wf_personnel.name, wf_personnel.lastname, wf_personnel.lastnamefirst');
		$this->db->from('wf_movie_personnel');
		$this->db->join('wf_type_personnel', 'wf_movie_personnel.type_id = wf_type_personnel.id');
		$this->db->join('wf_personnel', 'wf_personnel.id = wf_movie_personnel.personnel_id');
		$this->db->where('wf_movie_personnel.movie_id', $movie_id);
		$this->db->where('wf_movie_personnel.qa_appearances LIKE', '%'.$screening_id."%");
		$this->db->order_by('wf_type_personnel.order', 'asc'); 
		$this->db->order_by('wf_personnel.lastname', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_personnel_id($movie_pers_id) {
		// Get the personnel_id to return
		$this->db->select('wf_movie_personnel.personnel_id AS id');
		$this->db->from('wf_movie_personnel');
		$this->db->where('id',$movie_pers_id);
		$query = $this->db->get();
        return $query->result();
	}

	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
		
		if ($tablename == "wf_movie_personnel") {
			$result = $this->personnel->get_personnel_id($id);
			return $result[0]->id;
		} else {
			return $this->db->insert_id();
		}
	}

    function del_movie_personnel($movie_pers_id)
    {
		$result = $this->personnel->get_personnel_id($movie_pers_id);

		// Actually delete the record.
		$this->db->where('id',$movie_pers_id);
		$this->db->delete("wf_movie_personnel");
		
        return $result[0]->id;
	}

    function del_personnel($pers_id)
    {
		// Actually delete the record.
		$this->db->where('id',$pers_id);
		$this->db->delete("wf_personnel");
	}

    function get_personnel_list()
    {
		// loads all countries from films for list purposes.
		$this->db->select('wf_personnel.id, wf_personnel.name, wf_personnel.lastname, wf_personnel.lastnamefirst');
		$this->db->from('wf_personnel');
		$this->db->order_by('wf_personnel.lastname', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}


	// Inserts multiple people related to the film (if any), separated by ', '.
	function check_add_personnel($typename, $newPersList) {
		$currentPersList = $this->personnel->get_personnel_list();
	
		foreach ($newPersList as $thisPers) {
			if ($thisPers != "") {
				$thisPersIndividual = explode(", ",$thisPers);
				foreach ($thisPersIndividual as $thisPersInd) {
				
					$thisPersNames = explode(" ",$thisPersInd);
					if (count($thisPersNames) == 1) { $count = 1; }
					elseif (count($thisPersNames) == 2) { $count = 2; }
					elseif (count($thisPersNames) > 2) { $count = 99; }
					
					$exists = false; $firstmiddle = ""; $last = "";
					
					if (count($currentPersList) > 0) {
						foreach ($currentPersList as $currentPers) {					
							if ($count == 1) {
								if ($thisPersNames[0] == $currentPers->name && $currentPers->lastname == "") { $exists = true; }
							} elseif ($count == 2) {
								if ($thisPersNames[0] == $currentPers->name && $thisPersNames[1] == $currentPers->lastname) {						
								$exists = true;
								}
							} elseif ($count == 99) {
								$firstmiddle = "";
								$realcount = count($thisPersNames)-1;
								for ($x=0; $x < $realcount; $x++) {
									$firstmiddle .= $thisPersNames[$x]." ";						
								}
								$firstmiddle = trim($firstmiddle); $last = $thisPersNames[$realcount];
		
								if ($firstmiddle == $currentPers->name && $thisPersNames[$realcount] == $currentPers->lastname) { $exists = true; }
								//print "'".trim($thisPers)."' vs '".$firstmiddle." ".$currentPers->lastname."'<br>";
							}
						}
					} else {
						if ($count == 99) {
							$firstmiddle = "";
							$realcount = count($thisPersNames)-1;
							for ($x=0; $x < $realcount; $x++) {
								$firstmiddle .= $thisPersNames[$x]." ";						
							}
							$firstmiddle = trim($firstmiddle); $last = $thisPersNames[$realcount];		
						}
					}
					
					if (!$exists) { 
						if ($count == 1) {
							$values = array("name" => $thisPersNames[0], "lastname" => "", "lastnamefirst" => 0);
						} elseif ($count == 2) {
							$values = array("name" => $thisPersNames[0], "lastname" => $thisPersNames[1], "lastnamefirst" => 0);
						} elseif ($count == 99) {
							$values = array("name" => $firstmiddle, "lastname" => $last, "lastnamefirst" => 0);
						}
						
						$this->personnel->add_value($values, "wf_personnel");
					}
				}
			}
		}	
		
		return $this->personnel->get_personnel_list();
	}



/*
1	Director
2 	Producer
3 	Screenwriter
5 	Cinematographer
6 	Castlist
7 	Editor
8 	Music Director
*/

}
?>