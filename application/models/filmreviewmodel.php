<?php
class Filmreviewmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_film_review_items($festival_id, $field, $direction)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('*');
		$this->db->from('wf_movie_staging');
		$this->db->where('wf_movie_staging.festival_id', $festival_id);
		$this->db->order_by($field, $direction);
		if ($field != "wf_movie_staging.EnglishTitle") {
			$this->db->order_by("wf_movie_staging.EnglishTitle", "asc");
		}
		$query = $this->db->get();

        return $query->result();
	}

    function get_film_review($review_id)
    {
		// loads the full (public) schedule of films from the specified festival.
		$this->db->select('*');
		$this->db->from('wf_movie_staging');
		$this->db->where('wf_movie_staging.id', $review_id);
		$query = $this->db->get();

        return $query->result();
	}
	
	function insert_film_review_entry($post) {
		//print_r($post);
	
		// adds new film review entry
		$this->db->set("festival_id",$post["festival_id"]);
		$this->db->set("EnglishTitle",$post["EnglishTitle"]);
		$this->db->set("OriginalLanguageTitle",$post["OriginalLanguageTitle"]);
		$this->db->set("Year",$post["Year"]);
		$this->db->set("RunTime",$post["RunTime"]);
		$this->db->set("FootageLength",$post["FootageLength"]);
		$this->db->set("FootageMeasure",$post["FootageMeasure"]);
		$this->db->set("FootageType",$post["FootageType"]);

		$countries = $languages = $genres = "";
		if (isset($post["Country"])) {
			foreach ($post["Country"] as $ctry) { $countries .= $ctry.","; } $countries = trim($countries, ",");
			$this->db->set("Country",$countries);
		}
		if (isset($post["Language"])) {
			foreach ($post["Language"] as $lang) { $languages .= $lang.","; } $languages = trim($languages, ",");
			$this->db->set("Language",$languages);
		}
		if (isset($post["Genre"])) {
			foreach ($post["Genre"] as $gnre) { $genres .= $gnre.","; } $genres = trim($genres, ",");
			$this->db->set("Genre",$genres);
		}

		$this->db->set("ExhibitionFormat",$post["ExhibitionFormat"]);
		$this->db->set("AspectRatio",$post["AspectRatio"]);
		$this->db->set("Sound",$post["Sound"]);
		$this->db->set("Color",$post["Color"]);
		$this->db->set("PremiereStatus",$post["PremiereStatus"]);
		
		$PersToAdd = $post["PersNumToAdd"];
		$PersAdded = 0; $count = 1; $personnel = "";
		while ($PersAdded < $PersToAdd) {
			$fname = "pers-first-name-".$count;
			$lname = "pers-last-name-".$count;
			$role = "pers-role-".$count;
			$lnf = "pers-last-name-first-".$count;
			if (isset($post[$fname])) {
				// Found personnel with this number
				$personnel .= "{".$post[$fname]."|".$post[$lname]."|".$post[$role]."|".$post[$lnf]."},";
				$PersAdded++;
				$count++;
			} else {
				$count++;
				
				// Stop infinite loops if the number to add is somehow bigger than the amount of people submitted
				if ($count >= 30 && $PersToAdd <= 30) { break; }
			}		
		} $personnel = trim($personnel,",");
		$this->db->set("Personnel",$personnel);

		$this->db->set("PreviousScreenings",$post["PreviousScreenings"]);
		$this->db->set("Website",$post["Website"]);
		$this->db->set("Synopsis",$post["Synopsis"]);
		$this->db->set("distributorType",$post["distributorType"]);
		$this->db->set("distributorName",$post["distributorName"]);
		$this->db->set("distributorAddress",$post["distributorAddress"]);
		$this->db->set("distributorPhone",$post["distributorPhone"]);
		$this->db->set("distributorFax",$post["distributorFax"]);
		$this->db->set("distributorEmail",$post["distributorEmail"]);
		$this->db->set("InsuranceValue",$post["InsuranceValue"]);
		$this->db->set("Shipping_In_Name",$post["Shipping_In_Name"]);
		$this->db->set("Shipping_In_Address",$post["Shipping_In_Address"]);
		$this->db->set("Shipping_In_Phone",$post["Shipping_In_Phone"]);
		$this->db->set("Shipping_In_Fax",$post["Shipping_In_Fax"]);
		$this->db->set("Shipping_In_Email",$post["Shipping_In_Email"]);
		$this->db->set("Shipping_In_Date",$post["Shipping_In_Date"]);
		$this->db->set("Shipping_Out_Name",$post["Shipping_Out_Name"]);
		$this->db->set("Shipping_Out_Address",$post["Shipping_Out_Address"]);
		$this->db->set("Shipping_Out_Phone",$post["Shipping_Out_Phone"]);
		$this->db->set("Shipping_Out_Fax",$post["Shipping_Out_Fax"]);
		$this->db->set("Shipping_Out_Email",$post["Shipping_Out_Email"]);
		$this->db->set("Shipping_Out_Date",$post["Shipping_Out_Date"]);
		$this->db->set("PressContact",$post["PressContact"]);
		$this->db->set("PressPhone",$post["PressPhone"]);
		$this->db->set("PressFax",$post["PressFax"]);
		$this->db->set("PressEmail",$post["PressEmail"]);
		$this->db->set("PressWebsite",$post["PressWebsite"]);
		$this->db->set("HawaiiConnections",$post["HawaiiConnections"]);
		$this->db->set("TV_OK_ID",$post["TV_OK_ID"]);
		$this->db->set("Radio_OK_ID",$post["Radio_OK_ID"]);
		$this->db->set("Web_OK_ID",$post["Web_OK_ID"]);
		$this->db->set("VOD_OK_ID",$post["VOD_OK_ID"]);
		$this->db->set("SubmittingName",$post["PressContact"]);
		$this->db->set("SubmittingDate",$post["SubmittingDate"]);
		$this->db->set("Imported",$post["Imported"]);

		$this->db->insert("wf_movie_staging");

		return $this->db->insert_id();		
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}
}
?>