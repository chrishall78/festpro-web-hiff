<?php
class Guestreviewmodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_guest_review_items($festival_id, $field, $direction)
    {
		// loads all of the submitted accreditation data for the specified festival.
		$this->db->select('*, fg_type_guests.name as DelegateType, fg_guests_staging.id as guest_id');
		$this->db->from('fg_guests_staging');
		$this->db->where('fg_guests_staging.festival_id', $festival_id);
		//$this->db->where('fg_guests_staging.Imported', 0);
		$this->db->join('fg_type_guests', 'fg_type_guests.id = fg_guests_staging.DelegateCategory');
		$this->db->order_by($field, $direction);
		if ($field != "fg_guests_staging.Lastname") {
			$this->db->order_by("fg_guests_staging.Lastname", "asc");
		}
		//$this->db->limit(10);
		$query = $this->db->get();

        return $query->result();
	}

    function get_guest_review($review_id)
    {
		// loads one specific accreditation form.
		$this->db->select('*');
		$this->db->from('fg_guests_staging');
		$this->db->where('fg_guests_staging.id', $review_id);
		$query = $this->db->get();

        return $query->result();
	}
	
	function insert_guest_review_entry($post, $photoUrl = null, $filePath = null) {
		//print_r($post);
	
		// adds new film review entry
		$this->db->set("festival_id",$post["festival_id"]);
		
		$this->db->set("FirstName",$post["FirstName"]);
		$this->db->set("LastName",$post["LastName"]);
		$this->db->set("Film",$post["Film"]);
		$this->db->set("FilmOrCompany",$post["FilmOrCompany"]);
		$this->db->set("GuestAddress",$post["GuestAddress"]);
		$this->db->set("GuestCity",$post["GuestCity"]);
		$this->db->set("GuestState",$post["GuestState"]);
		$this->db->set("GuestPostal",$post["GuestPostal"]);
		$this->db->set("GuestCity",$post["GuestCity"]);

		$this->db->set("EmailAddress",$post["EmailAddress"]);
		$this->db->set("MobilePhone",$post["MobilePhone"]);
		$this->db->set("DayPhone",$post["DayPhone"]);
		$this->db->set("WebsiteURL",$post["WebsiteURL"]);
		$this->db->set("FacebookPage",$post["FacebookPage"]);
		$this->db->set("TwitterName",trim($post["TwitterName"], "@"));
		$this->db->set("LinkedinProfile",$post["LinkedinProfile"]);

		if (isset($post["ArrivalCity"])) { $this->db->set("ArrivalCity",$post["ArrivalCity"]); } else { $this->db->set("ArrivalCity",""); }
		if (isset($post["ArrivalCity2"])) { $this->db->set("ArrivalCity2",$post["ArrivalCity2"]); } else { $this->db->set("ArrivalCity2",""); }
		if (isset($post["ArrivalDate"])) { $this->db->set("ArrivalDate",date("Y-m-d",strtotime($post["ArrivalDate"]))); } else { $this->db->set("ArrivalDate","0000-00-00"); }
		if (isset($post["ArrivalTime"])) { $this->db->set("ArrivalTime",date("H:i:s",strtotime($post["ArrivalTime"]))); } else { $this->db->set("ArrivalTime","00:00:00"); }
		if (isset($post["ArrivalAirline"])) { $this->db->set("ArrivalAirline",$post["ArrivalAirline"]); } else { $this->db->set("ArrivalAirline",""); }
		if (isset($post["ArrivalFlightNum"])) { $this->db->set("ArrivalFlightNum",$post["ArrivalFlightNum"]); } else { $this->db->set("ArrivalFlightNum",""); }

		if (isset($post["DepartureCity"])) { $this->db->set("DepartureCity",$post["DepartureCity"]); } else { $this->db->set("DepartureCity",""); }
		if (isset($post["DepartureCity2"])) { $this->db->set("DepartureCity2",$post["DepartureCity2"]); } else { $this->db->set("DepartureCity2",""); }
		if (isset($post["DepartureDate"])) { $this->db->set("DepartureDate",date("Y-m-d",strtotime($post["DepartureDate"]))); } else { $this->db->set("DepartureDate","0000-00-00"); }
		if (isset($post["DepartureTime"])) { $this->db->set("DepartureTime",date("H:i:s",strtotime($post["DepartureTime"]))); } else { $this->db->set("DepartureTime","00:00:00"); }
		if (isset($post["DepartureAirline"])) { $this->db->set("DepartureAirline",$post["DepartureAirline"]); } else { $this->db->set("DepartureAirline",""); }
		if (isset($post["DepartureFlightNum"])) { $this->db->set("DepartureFlightNum",$post["DepartureFlightNum"]); } else { $this->db->set("DepartureFlightNum",""); }

		/*
		$this->db->set("AttendOpening",$post["AttendOpening"]);
		$this->db->set("AttendClosing",$post["AttendClosing"]);
		*/

		$this->db->set("Biography",$post["Biography"]);
		if ($photoUrl != null) { $this->db->set("GuestPhotoUrl",$photoUrl); }
		if ($filePath != null) { $this->db->set("GuestPhotoFilePath",$filePath); }
		$this->db->set("InvoiceEmail",$post["EmailAddress"]);
		$this->db->set("TravelMethod",$post["TravelMethod"]);
		$this->db->set("DelegateCategory",$post["DelegateCategory"]);
		$this->db->set("TranslatorLang",$post["TranslatorLang"]);
		$this->db->set("Fee",$post["Fee"]);
		$this->db->set("SubmittedDate",date("Y-m-d"));
		$this->db->set("DelegateStatus","Submitted");
		$this->db->set("Imported","0");

		$this->db->insert("fg_guests_staging");

		return $this->db->insert_id();		
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}
}
?>