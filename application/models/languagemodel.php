<?php
class Languagemodel extends CI_Model {

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function get_all_languages()
	{
		// loads all unique languages from films for list purposes.
		$this->db->select('wf_movie_language.movie_id, wf_type_language.id, wf_type_language.name, wf_type_language.slug');
		$this->db->from('wf_movie_language');
		$this->db->join('wf_type_language', 'wf_movie_language.language_id = wf_type_language.id');
		$this->db->order_by('wf_type_language.name', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

		return $query->result();
	}

	function get_all_languages_in($movie_id_array)
	{
		// loads all unique languages from films for list purposes.
		$this->db->select('wf_movie_language.movie_id, wf_type_language.id, wf_type_language.name, wf_type_language.slug');
		$this->db->from('wf_movie_language');
		$this->db->join('wf_type_language', 'wf_movie_language.language_id = wf_type_language.id');
		$this->db->where_in('wf_movie_language.movie_id', $movie_id_array);
		$this->db->order_by('wf_type_language.name', 'asc'); 
		$this->db->distinct(); 
		$query = $this->db->get();

		return $query->result();
	}

	function get_movie_languages($movie_id)
	{
		// loads all languages for this specific film.
		$this->db->select('wf_movie_language.movie_id, wf_movie_language.id, wf_type_language.id AS language_id, wf_type_language.name, wf_type_language.slug');
		$this->db->from('wf_movie_language');
		$this->db->join('wf_type_language', 'wf_movie_language.language_id = wf_type_language.id');
		$this->db->where('wf_movie_language.movie_id', $movie_id);
		$this->db->order_by('wf_type_language.name', 'asc'); 
		$query = $this->db->get();

		return $query->result();
	}

	function get_movie_subtitle_languages($movie_id)
	{
		// loads all languages for this specific film.
		$this->db->select('wf_movie_language_subtitles.movie_id, wf_movie_language_subtitles.id, wf_type_language.id AS language_id, wf_type_language.name, wf_type_language.slug');
		$this->db->from('wf_movie_language_subtitles');
		$this->db->join('wf_type_language', 'wf_movie_language_subtitles.language_id = wf_type_language.id');
		$this->db->where('wf_movie_language_subtitles.movie_id', $movie_id);
		$this->db->order_by('wf_type_language.name', 'asc'); 
		$query = $this->db->get();

		return $query->result();
	}

	function add_movie_language($movie_id)
	{
		// adds a new language to a movie with no value specified
		$this->db->set('movie_id',$movie_id);
		$this->db->set('language_id',0);
		$this->db->insert('wf_movie_language');

		return $this->db->insert_id();
	}
}
?>