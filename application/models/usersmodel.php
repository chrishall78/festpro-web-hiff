<?php
class Usersmodel extends CI_Model {

    var $username = '';
    var $password = '';
    var $email = '';
    var $first_name = '';
    var $last_name = '';
    var $registration_date = '';
	var $last_login = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_users()
    {
		// loads all users, alphabetical order.
		$this->db->select('id, FirstName, LastName, Username, Regdate, Email, LastLogin, Enabled, Admin, Programming, LimitedAccess');
		$this->db->from('users');
		$this->db->order_by('Enabled', 'desc'); 
		$this->db->order_by('LastName', 'asc'); 
		$query = $this->db->get();

        return $query->result();
	}

    function get_user($user_id)
    {
		// load one specific user by id
		$this->db->select('id, FirstName, LastName, Username, Password, Regdate, Email, LastLogin, Enabled, Admin, Programming, LimitedAccess');
		$this->db->from('users');
		$this->db->where('id',$user_id);
		$query = $this->db->get();

        return $query->result();
	}

    function get_username($username)
    {
		// load one specific user by username
		$this->db->select('id, FirstName, LastName, Username, Password, Regdate, Email, LastLogin, Enabled, Admin, Programming, LimitedAccess');
		$this->db->from('users');
		$this->db->where('Username',$username);
		$query = $this->db->get();

        return $query->result();
	}

    function insert_user($encrypted_password)
    {
        $this->Username = $this->input->post('username');
        $this->Password = $encrypted_password;
        $this->Email = $this->input->post('email');
        $this->FirstName = $this->input->post('first_name');
        $this->LastName = $this->input->post('last_name');
        $this->Regdate = date("Y-m-d");
        $this->LastLogin = NULL;

        $this->db->insert('users', $this);
    }

    function update_user($id, $encrypted_password)
    {
        $this->Username = $this->input->post('username');
		if ($encrypted_password != "") {
        	$this->password = $encrypted_password;
		}
        $this->Email = $this->input->post('email');
        $this->FirstName = $this->input->post('first_name');
        $this->LastName = $this->input->post('last_name');
        $this->Regdate = $this->input->post('registration_date');
        $this->LastLogin = $this->input->post('last_login');
        $this->db->update('users', $this, array('id' => $id));
    }

    function delete_user($id)
    {
		$this->db->delete('users', array('id' => $id)); 
	}

    function set_last_login($username)
    {
		// Set the last time this user has logged in.
        $user_data = array('LastLogin' => date("Y-m-d H:i:s"));
 
		$this->db->where('Username',$username);
        $this->db->update('users', $user_data);
	}

    function enable_user($id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', array('Enabled' => '1'));
    }

    function disable_user($id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', array('Enabled' => '0'));
    }


	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);
		
		return $id;
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}
    
}
?>