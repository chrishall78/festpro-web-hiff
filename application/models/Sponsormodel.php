<?php
class Sponsormodel extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function get_all_sponsors() {
		$this->db->select('name, id');
		$this->db->from('wf_sponsor');
		$this->db->order_by('name','asc');
		$this->db->distinct();
		$query = $this->db->get();

        return $query->result();
	}

	function get_sponsor_logo_simple($sponsor_id) {
		$this->db->select('*');
		$this->db->from('wf_sponsor_logos');
		$this->db->where('wf_sponsor_logos.sponsor_id', $sponsor_id);
		$query = $this->db->get();

        return $query->result();
	}

	function load_all_sponsor_logos() {
		$this->db->select('wf_sponsor_logos.id, wf_sponsor_logos.movie_id, wf_sponsor_logos.sponsor_id, wf_sponsor_logos.sponsorlogo_id, wf_type_sponsorlogo.name AS sponsorlogo_type, wf_type_sponsorlogo.description, wf_type_sponsorlogo.order, wf_sponsor_logos.url_website, wf_sponsor_logos.url_logo, wf_sponsor_logos.logo_width, wf_sponsor_logos.logo_height, wf_movie.title_en, wf_movie.slug, wf_sponsor.name');
		$this->db->from('wf_sponsor_logos');
		$this->db->join('wf_sponsor', 'wf_sponsor_logos.sponsor_id = wf_sponsor.id');
		$this->db->join('wf_type_sponsorlogo', 'wf_type_sponsorlogo.id = wf_sponsor_logos.sponsorlogo_id');
		$this->db->join('wf_movie', 'wf_sponsor_logos.movie_id = wf_movie.id');
		$this->db->order_by('wf_type_sponsorlogo.order','asc');
		$this->db->order_by('wf_sponsor.name','asc');
		$this->db->order_by('wf_movie.title_en','asc');
		$query = $this->db->get();

        return $query->result();		
	}

	function get_all_sponsor_logos($movie_id, $festival_id) {
		$this->db->select('wf_sponsor_logos.id, wf_sponsor_logos.movie_id, wf_sponsor_logos.sponsor_id, wf_sponsor_logos.sponsorlogo_id, wf_type_sponsorlogo.name AS sponsorlogo_type, wf_type_sponsorlogo.description, wf_type_sponsorlogo.order, wf_sponsor_logos.url_website, wf_sponsor_logos.url_logo, wf_sponsor_logos.logo_width, wf_sponsor_logos.logo_height, wf_movie.title_en, wf_movie.slug, wf_sponsor.name');
		$this->db->from('wf_sponsor_logos');
		$this->db->join('wf_sponsor', 'wf_sponsor_logos.sponsor_id = wf_sponsor.id');
		$this->db->join('wf_type_sponsorlogo', 'wf_type_sponsorlogo.id = wf_sponsor_logos.sponsorlogo_id');
		$this->db->join('wf_movie', 'wf_sponsor_logos.movie_id = wf_movie.id');
		if ($movie_id != 0) {
			$this->db->where('wf_sponsor_logos.movie_id', $movie_id);
		}
		$this->db->where('wf_sponsor_logos.festival_id', $festival_id);
		$this->db->order_by('wf_type_sponsorlogo.order','asc');
		$this->db->order_by('wf_sponsor.name','asc');
		$this->db->order_by('wf_movie.title_en','asc');
		$query = $this->db->get();

        return $query->result();		
	}

	function get_sponsor_logo($sponsor_id, $movie_id, $festival_id) {
		$this->db->select('wf_sponsor_logos.id, wf_sponsor_logos.movie_id, wf_sponsor_logos.sponsor_id, wf_sponsor_logos.sponsorlogo_id, wf_type_sponsorlogo.name AS sponsorlogo_type, wf_type_sponsorlogo.description, wf_type_sponsorlogo.order, wf_sponsor_logos.url_website, wf_sponsor_logos.url_logo, wf_sponsor_logos.logo_width, wf_sponsor_logos.logo_height');
		$this->db->from('wf_sponsor_logos');
		$this->db->join('wf_type_sponsorlogo', 'wf_type_sponsorlogo.id = wf_sponsor_logos.sponsorlogo_id');
		$this->db->where('wf_sponsor_logos.sponsor_id', $sponsor_id);
		$this->db->where('wf_sponsor_logos.movie_id', $movie_id);
		$this->db->where('wf_sponsor_logos.festival_id', $festival_id);
		$query = $this->db->get();

        return $query->result();		
	}

	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);

		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);

		return $this->db->insert_id();
	}

	function delete_value($id, $tablename) {
		// adds a new record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}
}
?>