<?php
$preview = false;
if (isset($logged_in)) {
	if ($logged_in == true) { $preview = true; }
}

// check to see film is published before displaying | preview if logged in
if ($film[0]->Published == 0 && $preview == false) {
	header('Location: /films/');
} else {
	// Required to support the Ajax Star Rater
	// require_once('assets/scripts/ajax_star_rater/_drawrating.php');
	$currentdate = time();
	$newtitle = switch_title($film[0]->title_en);
	
	// Current path of images
	$default_image = "/".$upload_dir."/001-default-photo.jpg";
	$default_image_text = "<img src=\"".$default_image."\" width=\"677\" height=\"381\" border=\"0\" alt=\"Default Festival Image\">";
	$no_trailer_text = "<div class=\"no_trailer\">This trailer is not available right now. Please try again later.</div>";
	$trailerfirst = true;
	$photo_url = ""; $video_html = ""; $phototab = ""; $trailertab = "";

	if (isset($video->html)) {
		$video_html = $video->html;
		$w = $video->width;
		$h = $video->height;
		$r = $h / $w;
		$new_w = 677;
		$new_h = round($new_w * $r);

		$search = array($w,$h);
		$replace = array($new_w,$new_h);
		$video_html = str_replace($search, $replace, $video_html);
	}

	if (count($film_photos) > 0) {
		$phototab .= "\t<li><a href=\"#\">View ".count($film_photos)." Photos</a></li>\n";
	} else {
		$phototab .= "\t<li><a href=\"#\">No Photos Available</a></li>\n";
	}
	if (count($film_videos) > 0) {
		$trailertab .= "\t<li><a href=\"#\">Watch Trailer</a></li>\n";
	} else {
		$trailertab .= "\t<li><a href=\"#\">No Trailer Available</a></li>\n";
	}

	print "<div style=\"margin-top:2px;\">&nbsp;</div>";
	print "<fieldset class=\"ui-corner-all";
	if ($film[0]->Published == 0) { print " unpublished"; }
	print "\">";
	print "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"film_detail\"><tbody>";
	
	print "\t\t<tr valign=\"top\"><td colspan=\"4\"><h1 class=\"title\">".$newtitle;
	if ($film[0]->title != "") { print " (".$film[0]->title.")"; }
	print "</h1></td></tr>\n";
	print "\t\t<tr valign=\"top\"><td colspan=\"4\">\n";

	print "\t\t\t<!-- tab panes -->\n";
	print "\t\t\t<div class=\"panes\" ";
	if (isset($video->height)) {
		if ($video->height > 381) { print " style=\"height:".$video->height."px;\""; }
	}
	print ">\n"; 

	// No photos or trailers
	if (count($film_photos) == 0 && count($film_videos) == 0) {
		print "\t\t\t\t<div class=\"pane01\">".$default_image_text."</div>\n";
		print "\t\t\t\t<div class=\"pane02\">".$no_trailer_text."</div>\n";
		$trailerfirst = false;
	// Trailer but no photos
	} else if (count($film_videos) >= 1 && count($film_photos) == 0) {
		print "\t\t\t\t<div class=\"pane01\">";
		$video_html != "" ? print $video_html : print $no_trailer_text;
		print "</div>\n";
		print "\t\t\t\t<div class=\"pane02\">".$default_image_text."</div>\n";
		$trailerfirst = true;
	// Photos but no trailer
	} else if (count($film_photos) >= 1 && count($film_videos) == 0) {
		print "\t\t\t\t<div class=\"pane01 theme-default\">\n";
		print "<div id=\"featured_films\" class=\"nivoSlider\">\n";
		foreach ($film_photos as $thisPhoto) {
			if ($thisPhoto->url_cropxlarge != "") {
				$photo_url = $thisPhoto->url_cropxlarge;
			} else {
				$photo_url = $thisPhoto->url_croplarge;
			}
			print "<img width=\"677\" height=\"381\" border=\"0\" src=\"".$photo_url."\" alt=\"".$newtitle."\" />\n";
		}
		print "</div>\n";
		print "\t\t\t\t</div>\n";
		print "\t\t\t\t<div class=\"pane02\">".$no_trailer_text."</div>\n";
		$trailerfirst = false;
	// Both photos and trailer
	} else {
		print "\t\t\t\t<div class=\"pane01 theme-default\">\n";
		print "<div id=\"featured_films\" class=\"nivoSlider\">\n";
		foreach ($film_photos as $thisPhoto) {
			if ($thisPhoto->url_cropxlarge != "") {
				$photo_url = $thisPhoto->url_cropxlarge;
			} else {
				$photo_url = $thisPhoto->url_croplarge;
			}
			print "<img width=\"677\" height=\"381\" border=\"0\" src=\"".$photo_url."\" alt=\"".$newtitle."\" />\n";
		}
		print "</div>\n";
		print "\t\t\t\t</div>\n";
		print "\t\t\t\t<div class=\"pane02\">\n";
		$video_html != "" ? print $video_html : print $no_trailer_text;
		print "\t\t\t\t</div>\n";
		$trailerfirst = false;
	}
	print "\t\t\t</div>\n";

	print "<!-- the tabs -->\n";
	print "<ul class=\"filmTabs\">\n";
	if ($trailerfirst == true) {
		print $trailertab.$phototab;
	} else {
		print $phototab.$trailertab;
	}
	print "</ul>\n\n";


	print "\t\t\t<div class=\"film_detail_info\">\n";
	print "\t\t\t\t<div class=\"col1\">";
		$runtime_total = 0;
		if ($film[0]->runtime_int != 0) {
			$runtime_total = $film[0]->runtime_int;
		} else {
			foreach ($full_schedule as $thisScreening) {
				if ($thisScreening->movie_id == $film[0]->movie_id) {
					$runtime_start = 0;
					$film_array = explode(",",$thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilmID) {
						foreach ($filmids as $thisFilm2) {
							if ($thisFilmID == $thisFilm2->movie_id) {
								$runtime_start += $thisFilm2->runtime_int;
							}
						}
					}
					$runtime_total = $runtime_start;
				}
			}
		}

		if ($director != "") {
			print "\t\t\t<p>Directed by: <span>".$director." ".$film[0]->year." | ".$runtime_total." min.</span></p>\n";
		} else {
			print "\t\t\t<p>Completed in: <span>".$film[0]->year." | ".$runtime_total." min.</span></p>\n";
		}
		if ($film[0]->section_slug != "") {
			$section_name = "";
			foreach ($sections as $thisSection) {
				if ($film[0]->section_slug == $thisSection->slug) {
					$section_name = $thisSection->name; break;
				}
			}
			print "\t\t\t<p>Section: <a href=\"/films/section/".$film[0]->section_slug."\">".$section_name."</a></p>\n";
		}
		$premiere_array = convert_to_array2($premiere_type);
		$premiere_name = "";
		if ($premiere_id != 0) {
			$premiere_name = $premiere_array[$premiere_id];
		}
		if ($premiere_name != "") {
			print "\t\t\t<p>Premiere: <span>".$premiere_name."</span></p>\n";
		}
		if ($film[0]->websiteurl != "") {
			print "\t\t\t<p><a href=\"".$film[0]->websiteurl."\" target=\"_blank\">Film Website</a></span></p>\n";
		}
	print "\t\t\t\t</div><div class=\"col2\">";
		print "\t\t\t<p>Countries: <span>".$country."</span></p>\n";
		print "\t\t\t<p>Languages: <span>".$language;
		if ($subtitle != "") { print ", with ".$subtitle." subtitles"; }
		print "</span></p>\n";
		print "\t\t\t<p>Genres: <span>".$genre."</span></p>\n";
	print "\t\t\t\t</div><div class=\"col3\">";
		print "<div id=\"share_button\" style=\"text-align:left;\">";
		print "<span class=\"st_facebook_hcount\" displayText=\"Facebook\" style=\"display:block; padding:0 0 3px 0;\"></span>";
		print "<span class=\"st_twitter_hcount\" displayText=\"Twitter\" style=\"display:block; padding:0 0 3px 0;\"></span>";
		print "<span class=\"st_pinterest_hcount\" displayText=\"Pinterest\" style=\"display:block; padding:0 0 3px 0;\"></span>";
		//print "<span class=\"st_googleplus_hcount\" displayText=\"Share\" style=\"display:block; padding:0 0 3px 0;\"></span>";
		print "</div>";

		print "\t\t\t\t</div>";

		//if ($currentdate >= $first_scrn_date && $first_scrn_date != 0) {
			// On or after the first screening date, allow rating
			//print "<p style=\"margin: 10px 0 0 0;\">Rate this film:<div class=\"starrater\">".rating_bar($film[0]->slug,'5')."</div>";
		//} else {
			// Before the first screening date or no screenings, just show a static rating
			//print "<p style=\"margin: 10px 0 0 0;\">Rating not available yet:<div class=\"starrater\">".rating_bar($film[0]->slug,'5','static')."</div>";
		//}	
	print "\t\t\t<br clear=\"all\"></div>\n";

	print "\t\t<a name=\"synopsis\"></a><h3>Synopsis</h3>\n";
	print "<div class=\"film_detail_synopsis\">";

	// Sponsor Logos
	if (count($sponsor_logos) > 0) {
		$width = 150;
		print "<div class=\"film_detail_sponsors\">\n";
		foreach($sponsorlogo_type as $thisLogoType) {
			$typecount = 0;
			foreach ($sponsor_logos as $thisSponsor) {
				if ($thisSponsor->sponsorlogo_id == $thisLogoType->id) {
					if ($typecount == 0) { print "<p><strong>".$thisLogoType->description."</strong></p>\n";}
				
					if ($thisSponsor->url_logo != "") {
						print "<p>";
						if ($thisSponsor->url_website != "") { print "<a href=\"".$thisSponsor->url_website."\">"; }
						print "<img src=\"".$thisSponsor->url_logo."\" alt=\"".$thisSponsor->name."\" width=\"".$width."\" />";
						if ($thisSponsor->url_website != "") { print "</a>"; }
						print "</p>";
					} else {
						print "<p class=\"sponsor_text\">";
						if ($thisSponsor->url_website != "") { print "<a href=\"".$thisSponsor->url_website."\">"; }
						print $thisSponsor->name;
						if ($thisSponsor->url_website != "") { print "</a>"; }
						print "</p>";
					}
					$typecount++;
				}
			}
		}
		print "</div>\n";
	}

	// Synopsis	
	if ($film[0]->synopsis == "") {
		$syn_short = str_replace("&#13;&#10;", "<br>", stripslashes($film[0]->synopsis_short));
		print "<p>".$syn_short."</p>";
		if ($film[0]->writerShort != "") {
			print "<p><strong>Synopsis written by: </strong>".$film[0]->writerShort."</p>";
		}
	} else {
		$syn_long = str_replace("&#13;&#10;", "<br>", stripslashes($film[0]->synopsis));
		print "<p>".$syn_long."</p>";
		if ($film[0]->writerLong != "") {
			print "<p><strong>Synopsis written by: </strong>".$film[0]->writerLong."</p>";
		}
	}
	if ($film[0]->synopsis_original != "") {
		$syn_original = str_replace("&#13;&#10;", "<br>", stripslashes($film[0]->synopsis_original));
		print "<p>".$syn_original."</p>";
		if ($film[0]->writerOrigLang != "") {
			print "<p><strong>Synopsis written by: </strong>".$film[0]->writerOrigLang."</p>";
		}
	}
	print "</div>\n";
	
	print "\t\t</td></tr>\n";

	// Screenings
	print "\t\t<tr valign=\"top\"><td colspan=\"4\"><a name=\"screenings\"></a><h3>Screenings</h3></td></tr>\n";

	$shorts_screenings = "";
	foreach ($full_schedule as $thisScreening) {
		$film_array = explode(",",$thisScreening->program_movie_ids);
		foreach ($film_array as $thisFilmID) {
			if ($film[0]->movie_id == $thisFilmID) {
				$shorts_screenings .= "1";
			}
		}
	}
	if (count($full_schedule) > 0 && $shorts_screenings != "") {
		$previousDate = "";
		
		$counter = 0; $prevScrnDate = ""; $prevScrnTime = ""; $prevScrnMovie = "";
		foreach ($full_schedule as $thisScreening) {
			if (($counter != 0) && ($thisScreening->date == $prevScrnDate) && ($thisScreening->time == $prevScrnTime) && ($thisScreening->movie_id == $prevScrnMovie) ) {
				// Display nothing	
			} else {
				$film_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($film_array as $thisFilmID) {
					if ($film[0]->movie_id == $thisFilmID) {
						$tickets_img = find_screening_status($thisScreening->date,$thisScreening->url,$thisScreening->Rush,$thisScreening->Free);
					
						$datetime = $thisScreening->date." ".$thisScreening->time;
						$date = date("l, F d", strtotime($datetime));
						$time = date("g:i A", strtotime($datetime));
			
						$location = $thisScreening->displayname;
		
						print "\t<tr valign=\"top\">\n";
						print "\t\t<td width=\"25%\">".$date."</td>\n";
						print "\t\t<td width=\"25%\">".$time."</td>\n";
						print "\t\t<td width=\"25%\">".$location."</td>\n";
						print "\t\t<td width=\"25%\" align=\"center\">".$tickets_img."</td>\n";  
						print "\t</tr>\n";

						if (count($film_array) > 1) {
							print "\t<tr valign=\"top\">\n";
							print "\t\t<td align=\"right\" style=\"line-height: 120%;\"><b>Plays with:&nbsp;</b></td>\n";
							print "\t\t<td colspan=\"3\" style=\"line-height: 120%;\">";
							if ($thisScreening->program_name != "") { print "<a href=\"/films/program/".$thisScreening->program_slug."\" style=\"display: block; font-size: larger; padding-bottom: 5px; text-decoration: underline;\">".$thisScreening->program_name."</a>\n"; }
							foreach ($film_array as $thisFilmID2) {
								foreach ($filmids as $thisFilm2) {
									if ($thisFilmID2 == $thisFilm2->movie_id) {
										if ($thisFilmID != $thisFilmID2) {
											print "<a href=\"/films/detail/".$thisFilm2->value."\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a><br>";
										}
									}
								}
							}
							print "\t</tr>\n";
						}
					}
				}
			}
			$prevScrnDate = $thisScreening->date;
			$prevScrnTime = $thisScreening->time;
			$prevScrnMovie = $thisScreening->movie_id;
			$counter++;
		}
	} else {
		print "\t\t<tr valign=\"top\"><td colspan=\"4\" align=\"center\">Screening information is not available for this film.</td></tr>\n";
	}

	print "\t\t<tr valign=\"top\"><td colspan=\"4\"><a name=\"castcrew\"></a><h3>Cast & Crew</h3></td></tr>\n";
	
	if (count($personnel) > 0) {
		$order = 0; $current_order = 100;
		print "\t\t<tr valign=\"top\"><td colspan=\"4\">";
		print "\t<table width=\"100%\" cellpadding=\"3\" cellspacing=\"0\"><tbody style=\"border-top:none;\">\n";

		// Determine what roles to display for this film/event
		$roles_array = array();
		foreach ($personnel as $thisPerson) {
			foreach (explode(",",$thisPerson->personnel_roles) as $thisRole) {
				$roles_array[] = $thisRole;
			}
		} $roles_array = array_unique($roles_array);

		// Print the cast & crew, ordered by role
		foreach ($personnel_type as $thisType) {
			foreach ($roles_array as $thisRole) {
				if ($thisRole == $thisType->id) {
					print "\t<tr valign=\"top\">\n";
					print "\t\t<td width=\"20%\"><b>".$thisType->name."</b></td>\n";
					print "\t\t<td width=\"80%\">";
				
					$namelist = "";
					foreach ($personnel as $thisPerson) {
						foreach (explode(",",$thisPerson->personnel_roles) as $thisRole) {
							if ($thisType->id == $thisRole) {
								if ($thisPerson->lastnamefirst == 0) {
									$namelist .= $thisPerson->name. " ".$thisPerson->lastname.", ";
								} else {
									$namelist .= $thisPerson->lastname. " ".$thisPerson->name.", ";
								}
							}
						}
					}

					print trim($namelist, ", ")."</td>\n";
					print "\t</tr>\n";
				}
			}
		}

		print "</tbody></table>";
		print "</td></tr>\n";
	} else {
		print "\t\t<tr valign=\"top\"><td colspan=\"4\" align=\"center\">Cast & Crew information is not available for this film.</td></tr>\n";
	}

	// Show print source items based on configuration variable.	
	if (FP_FRONT_SHOW_SIMILAR_FILMS == 1) {
		if ($film[0]->myhiff_1 != 0 || $film[0]->myhiff_2 != 0 || $film[0]->myhiff_3 != 0) {
	
			$myhiff = array(
				array("movie_id" => $film[0]->myhiff_1, "slug" => "", "title_en" => "", "photo" => $default_image),
				array("movie_id" => $film[0]->myhiff_2, "slug" => "", "title_en" => "", "photo" => $default_image),
				array("movie_id" => $film[0]->myhiff_3, "slug" => "", "title_en" => "", "photo" => $default_image)
			);
	
			foreach ($films as $thisFilm) {
				if ($thisFilm->movie_id == $myhiff[0]["movie_id"]) { $myhiff[0]["slug"] = $thisFilm->slug; $myhiff[0]["title_en"] = switch_title($thisFilm->title_en); }
				if ($thisFilm->movie_id == $myhiff[1]["movie_id"]) { $myhiff[1]["slug"] = $thisFilm->slug; $myhiff[1]["title_en"] = switch_title($thisFilm->title_en); }
				if ($thisFilm->movie_id == $myhiff[2]["movie_id"]) { $myhiff[2]["slug"] = $thisFilm->slug; $myhiff[2]["title_en"] = switch_title($thisFilm->title_en); }
			}
			foreach ($myhiff_photos as $thisFilmPhoto) {
				if ($thisFilmPhoto->movie_id == $myhiff[0]["movie_id"]) { $myhiff[0]["photo"] = $thisFilmPhoto->url_cropsmall; }
				if ($thisFilmPhoto->movie_id == $myhiff[1]["movie_id"]) { $myhiff[1]["photo"] = $thisFilmPhoto->url_cropsmall; }
				if ($thisFilmPhoto->movie_id == $myhiff[2]["movie_id"]) { $myhiff[2]["photo"] = $thisFilmPhoto->url_cropsmall; }
			}
	
			print "\t\t<tr valign=\"top\"><td colspan=\"4\"><a name=\"mightlike\"></a><h3>You Might Also Like...</h3></td></tr>\n";
			print "\t\t<tr valign=\"top\"><td colspan=\"4\">";
			
			foreach ($myhiff as $thisRelatedFilm) {
				if ($thisRelatedFilm["movie_id"] != 0) {
					print "\t<div class=\"related_film ui-corner-all\">";
					print "\t\t<a href=\"/films/detail/".$thisRelatedFilm["slug"]."\"><img height=\"120\" src=\"".$thisRelatedFilm["photo"]."\">".$thisRelatedFilm["title_en"]."</a>";
					print "</div>";
				}
			}
			print "</td></tr>\n";
		}
	}

	// Show print source items based on configuration variable.	
	if (FP_FRONT_SHOW_PRINT_SOURCE == 1) {
		print "\t\t<tr valign=\"top\"><td colspan=\"4\"><a name=\"printsource\"></a><h3>Print Source</h3></td></tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Distributor:</b></td><td colspan=\"3\">".$film[0]->distributor."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Contact Name:</b></td><td colspan=\"3\">".$film[0]->distributorContact."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Mailing Address:</b></td><td colspan=\"3\">".$film[0]->distributorAddress."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Email Address:</b></td><td colspan=\"3\">".$film[0]->distributorEmail."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Phone Number:</b></td><td colspan=\"3\">".$film[0]->distributorPhone."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><b>Fax Number:</b></td><td colspan=\"3\">".$film[0]->distributorFax."</td>\n";
		print "\t\t</tr>\n";
	}
	print "</tbody></table>";
	
	?>
	
	<script type="text/javascript" language="javascript">
	$(document).ready(function() {	
		/*
		$('a.rater').on('click', function() {
			var parameters = $(this).attr("href").split("?");
			var id_value = parameters[1].split("&");
			var id_to_update = id_value[1].split("=");
	
			$.ajax({
				success: function(msg, text, xhr) {
					//alert(msg);
					$('#unit_long'+id_to_update[1]).replaceWith(msg);
				},
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: "",
				url: '/assets/scripts/ajax_star_rater/rpc.php?'+parameters[1],
				type: 'GET',
				dataType: 'html'
			}); 
	
			return false;
		});*/
	
		// setup ul.tabs to work as tabs for each div directly under div.panes
		$("ul.filmTabs").jtTabs("div.panes > div");

<?php if (count($film_photos) > 0) { ?>
		$('#featured_films').nivoSlider({
			effect: 'fade',
			directionNav: false,
			pauseOnHover: true,
			animSpeed: 600,
	        pauseTime: 8000
		});
<?php } ?>
	});
	</script>
<?php
}
?>