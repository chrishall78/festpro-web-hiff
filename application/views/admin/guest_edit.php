<?php
if ($saved_guest_title != "none") {
	print "<div align=\"center\" style=\"font-weight:bold; color:#CC0000;\">".$saved_guest_title." has been successfully updated.</div>";
}

// Declare variable arrays for dropdowns
$guesttypes_array = convert_to_array($guesttypes);
$films_array = convert_films_to_array($films);
$languages_array = convert_to_array($languages);

$flight_comp_array = convert_to_array($flight_comps);
$flight_comp_array2 = convert_flightcomp_to_array($flight_comps);
$airlines_array = convert_to_array($airlines);
$hotel_comp_array = convert_hotelcomp_to_array($hotel_comps, $hotel_used);
$hotel_shares_array = convert_to_array($hotel_shares);
$existing_events_array = convert_events_to_array($existing_events);

$flight_json = json_encode($flights);
$flight_comp_json = json_encode($flight_comps);
$hotel_json = json_encode($hotels);
$eventapp_json = json_encode($event_appearances);
$existingevents_json = json_encode($existing_events);


// Film Affiliations
$aff_id_string = $film_id_string = "";
foreach ($film_affiliations as $thisFilmAff) {
	$aff_id_string .= $thisFilmAff->aff_id.",";
	$film_id_string .= $thisFilmAff->movie_id.",";
}
$aff_id_string = trim($aff_id_string,",");
$film_id_string = trim($film_id_string,",");

$attributes = array('name' => 'UpdateGuest', 'id' => 'UpdateGuest');
$hidden = array('festivalguest_id'=>$guest[0]->id,'festival_id'=>$guest[0]->festival_id,'guest_slug'=>$guest[0]->slug);
print form_open_multipart('admin/guest_edit/save', $attributes, $hidden);
print "<input type=\"hidden\" name=\"film_aff_ids\" id=\"film_aff_ids\" value=\"".$aff_id_string."\">";
print "<input type=\"hidden\" name=\"film_aff_movie_ids\" id=\"film_aff_movie_ids\" value=\"".$film_id_string."\">";

?>

<table id="topGuestEdit" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody style="border-top:none;">
	<tr>
		<td width="100"><?php print form_submit(array('name'=>'Save', 'id'=>'Save', 'value'=>'SAVE')); ?></td>
		<td width="180"><label for="Firstname">First Name:</label> <span class="req">*</span> <?php print form_input(array('name'=>'Firstname', 'id'=>'Firstname', 'value'=>$guest[0]->Firstname, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		<td width="80"><label for="MI">Middle Initial: </label> <?php print form_input(array('name'=>'MI', 'id'=>'MI', 'value'=>$guest[0]->MI, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		<td width="180"><label for="Lastname">Last Name: </label> <span class="req">*</span> <?php print form_input(array('name'=>'Lastname', 'id'=>'Lastname', 'value'=>$guest[0]->Lastname, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		<td width="360"><label for="guesttype_id">Guest Type:</label> <span class="req">*</span><?php print form_dropdown('guesttype_id', $guesttypes_array, $guest[0]->guesttype_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
		<td width="100" align="right">
<?php if ($this->session->userdata('limited') == 0) { ?>
		<button name="Delete" id="Delete" class="ui-widget ui-corner-all ui-state-error">DELETE</button>
<?php } ?>
		</td>
	</tr>
	</tbody>
</table>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Guest Details</a></li>
		<li><a href="#tabs-2">Flights</a></li>
		<li><a href="#tabs-3">Hotels</a></li>
		<li><a href="#tabs-4">Screening Appearances</a></li>
		<li><a href="#tabs-5">Event Appearances</a></li>
		<li><a href="#tabs-6">Photo</a></li>
	</ul>
	<!-- Guest Details Tab -->
	<div id="tabs-1">
		<fieldset class="ui-corner-all">
		<table class="GuestDetailsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td width="10%"><label for="Address">Address:</label></td>
				<td width="14%"><?php print form_input(array('name'=>'Address', 'id'=>'Address', 'value'=>$guest[0]->Address, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td width="8%"><label for="City">City:</label></td>
				<td width="16%"><?php print form_input(array('name'=>'City', 'id'=>'City', 'value'=>$guest[0]->City, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td width="8%"><label for="State">State:</label></td>
				<td width="16%"><?php print form_input(array('name'=>'State', 'id'=>'State', 'value'=>$guest[0]->State, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td width="8%"><label for="PostalCode">Postal Code:</label></td>
				<td width="16%"><?php print form_input(array('name'=>'PostalCode', 'id'=>'PostalCode', 'value'=>$guest[0]->PostalCode, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<?php
				if ($guest[0]->Email != "") {
					print "\t\t\t\t<td><label for=\"Email\"><a href=\"mailto:".$guest[0]->Email."\" style=\"text-decoration:underline;\">Email:</a></label> <span class=\"req\">*</span></td>\n";
				} else {
					print "\t\t\t\t<td><label for=\"Email\">Email:</label> <span class=\"req\">*</span></td>\n";
				} ?>
				<td><?php print form_input(array('name'=>'Email', 'id'=>'Email', 'value'=>$guest[0]->Email, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td><label for="Phone">Phone:</label></td>
				<td><?php print form_input(array('name'=>'Phone', 'id'=>'Phone', 'value'=>$guest[0]->Phone, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td><label for="Cell">Mobile:</label></td>
				<td><?php print form_input(array('name'=>'Cell', 'id'=>'Cell', 'value'=>$guest[0]->Cell, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<td><label for="Fax">Fax:</label></td>
				<td><?php print form_input(array('name'=>'Fax', 'id'=>'Fax', 'value'=>$guest[0]->Fax, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<?php
				if ($guest[0]->TwitterName != "") {
					print "\t\t\t\t<td><label for=\"TwitterName\"><a href=\"http://twitter.com/".$guest[0]->TwitterName."\" target=\"_blank\" style=\"text-decoration:underline;\">Twitter:</a></label></td>\n";
				} else {
					print "\t\t\t\t<td><label for=\"TwitterName\">Twitter:</label></td>\n";
				} ?>
				<td><?php print form_input(array('name'=>'TwitterName', 'id'=>'TwitterName', 'value'=>$guest[0]->TwitterName, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>

				<?php
				if ($guest[0]->FacebookPage != "") {
					print "\t\t\t\t<td><label for=\"FacebookPage\"><a href=\"".$guest[0]->FacebookPage."\" target=\"_blank\" style=\"text-decoration:underline;\">Facebook Page:</a></label></td>\n";
				} else {
					print "\t\t\t\t<td><label for=\"FacebookPage\">Facebook Page:</label></td>\n";
				} ?>
				<td><?php print form_input(array('name'=>'FacebookPage', 'id'=>'FacebookPage', 'value'=>$guest[0]->FacebookPage, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>

				<?php
				if ($guest[0]->LinkedinProfile != "") {
					print "\t\t\t\t<td><label for=\"LinkedinProfile\"><a href=\"".$guest[0]->LinkedinProfile."\" target=\"_blank\" style=\"text-decoration:underline;\">Linkedin:</a></label></td>\n";
				} else {
					print "\t\t\t\t<td><label for=\"LinkedinProfile\">Linkedin:</label></td>\n";
				} ?>
				<td><?php print form_input(array('name'=>'LinkedinProfile', 'id'=>'LinkedinProfile', 'value'=>$guest[0]->LinkedinProfile, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				<?php
				if ($guest[0]->WebsiteURL != "") {
					print "\t\t\t\t<td><label for=\"WebsiteURL\"><a href=\"".$guest[0]->WebsiteURL."\" target=\"_blank\" style=\"text-decoration:underline;\">Website:</a></label></td>\n";
				} else {
					print "\t\t\t\t<td><label for=\"WebsiteURL\">Website:</label></td>\n";
				} ?>
				<td><?php print form_input(array('name'=>'WebsiteURL', 'id'=>'WebsiteURL', 'value'=>$guest[0]->WebsiteURL, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="gb_pickedup">Picked up Gift Bag:</label></td>
				<td><?php $guest[0]->gb_pickedup == 1 ? print form_checkbox(array('name'=>'gb_pickedup', 'id'=>'gb_pickedup', 'value'=>'1', 'checked'=>true)) : print form_checkbox(array('name'=>'gb_pickedup', 'id'=>'gb_pickedup', 'value'=>'1', 'checked'=>false)); ?></td>
				<td><label for="badge_pickedup">Picked Up Badge:</label></td>
				<td><?php $guest[0]->badge_pickedup == 1 ? print form_checkbox(array('name'=>'badge_pickedup', 'id'=>'badge_pickedup', 'value'=>'1', 'checked'=>true)) : print form_checkbox(array('name'=>'badge_pickedup', 'id'=>'badge_pickedup', 'value'=>'1', 'checked'=>false)); ?></td>

				<td colspan="4" rowspan="2">
					<label for="notes">General Notes</label>
					<?php print form_textarea(array('name'=>'notes', 'id'=>'notes', 'value'=>$guest[0]->notes, 'rows'=>'5', 'cols'=>'72', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
			</tr>
			<tr valign="top">
				<td><label for="language_id">Translator Required:</label></td>
				<td colspan="3"><?php print form_dropdown('language_id', $languages_array, $guest[0]->language_id,"class='select-large ui-widget-content ui-corner-all'"); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="AffiliationOptional">Company Affiliation:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'AffiliationOptional', 'id'=>'AffiliationOptional', 'value'=>$guest[0]->AffiliationOptional, 'class'=>'text-larger ui-widget-content ui-corner-all')); ?></td>

				<td colspan="4" rowspan="2">
					<div id="biochars" style="float:right; width:200px; text-align:right; display:none;"><span id="biocount">0</span> of 560 characters</div>
					<label for="Bio">Guest Bio</label>
					<?php print form_textarea(array('name'=>'Bio', 'id'=>'Bio', 'value'=>$guest[0]->Bio, 'rows'=>'5', 'cols'=>'72', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
			</tr>
			<tr valign="top">
				<td><label for="filmaffiliations">Film Affiliation(s):</label></td>
				<td colspan="3"><div id="film_aff">
				<?php
					foreach ($film_affiliations as $thisFilmAff) {
						print form_dropdown('film_affiliation_'.$thisFilmAff->aff_id, $films_array, $thisFilmAff->movie_id,"class='select-film ui-widget-content ui-corner-all'");
						print " <a href=\"#\" id=\"film_aff_remove_".$thisFilmAff->aff_id."\" class=\"remove\"><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/cancel.png\" alt=\"Remove Film Affiliation\" title=\"Remove Film Affiliation\"></a><br />";
					}
				?>
					<div id="addFilmAffHere"><input type="hidden" name="addFilmAffId" id="addFilmAffId" value="0" /></div>
					<button name="addFilmAff" id="addFilmAff">Add Film</button>
				</div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td><td colspan="7"><p style="font-size:0.8em; margin:0;">Removing film affiliations and then saving will also remove any screening appearances for those film(s).</p></td>
			</tr>
			</tbody>
		</table>
		</fieldset>
	</div>

	<!-- Flights Tab -->
	<div id="tabs-2">
		<fieldset class="ui-corner-all">
		<table class="FlightsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<th width="7%">&nbsp;</th>
				<th width="22%">Arriving Info</th>
				<th width="22%">Departing Info</th>
				<th width="5%">Cost</th>
				<th width="5%">Mileage</th>
				<th width="20%">Comp Info</th>
				<th width="12%">Airport Pickup</th>
				<th width="7%">Translator</th>
			</tr>
<?php
	if (count($flights) == 0) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"8\" align=\"center\">There are no flights for this guest yet. Please click the 'Add Flight' button below to add one.</td>\n";
		print "\t\t</tr>\n";
	}
	foreach ($flights as $thisFlight) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\"><button id=\"flight-".$thisFlight->id."\" name=\"flight-".$thisFlight->id."\" data-id=\"".$thisFlight->id."\" class=\"edit\">Edit</button></td>\n";

		print "\t\t\t<td>";
		if ($thisFlight->ArrivalDate == "0000-00-00" || $thisFlight->ArrivalDate == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($thisFlight->ArrivalDate))." "; }
		if ($thisFlight->ArrivalTime != "00:00:00") { print date("g:iA",strtotime($thisFlight->ArrivalTime))."<br />"; } else { print "<br />"; }
		if ($thisFlight->ArrivalCity != "" && $thisFlight->ArrivalCity2 != "") { print $thisFlight->ArrivalCity."-".$thisFlight->ArrivalCity2." / "; }
		elseif ($thisFlight->ArrivalCity == "" && $thisFlight->ArrivalCity2 != "") { print $thisFlight->ArrivalCity2." / "; }
		elseif ($thisFlight->ArrivalCity != "" && $thisFlight->ArrivalCity2 == "") { print  $thisFlight->ArrivalCity." / "; }
		if ($thisFlight->Airline != 0) { print $airlines_array[$thisFlight->Airline]; }
		if ($thisFlight->FlightNumber != "") { print " #".$thisFlight->FlightNumber; }
		print "</td>\n";

		print "\t\t\t<td>";
		if ($thisFlight->DepartureDate == "0000-00-00" || $thisFlight->DepartureDate == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($thisFlight->DepartureDate))." "; }
		if ($thisFlight->DepartureTime != "00:00:00") { print date("g:iA",strtotime($thisFlight->DepartureTime))."<br />"; } else { print "<br />"; }
		if ($thisFlight->DepartureCity != "" && $thisFlight->DepartureCity2 != "") { print $thisFlight->DepartureCity."-".$thisFlight->DepartureCity2." / "; }
		elseif ($thisFlight->DepartureCity == "" && $thisFlight->DepartureCity2 != "") { print $thisFlight->DepartureCity2." / "; }
		elseif ($thisFlight->DepartureCity != "" && $thisFlight->DepartureCity2 == "") { print  $thisFlight->DepartureCity." / "; }
		if ($thisFlight->DepartureAirline != 0) { print $airlines_array[$thisFlight->DepartureAirline]; }
		if ($thisFlight->DepartureFlightNum != "") { print " #".$thisFlight->DepartureFlightNum; }
		print "</td>\n";

		print "\t\t\t<td>$".$thisFlight->cost."</td>\n";
		print "\t\t\t<td>".$thisFlight->mileage."</td>\n";
		print "\t\t\t<td>";
		if ($thisFlight->flightcomp_id == 0) {
			print "None";
		} else {
			print $flight_comp_array[$thisFlight->flightcomp_id];
		}
		print "</td>\n";
		if ($thisFlight->pickup != "0") {
			print "\t<td><a id=\"pickupf-".$thisFlight->id."\" href=\"#\" data-id=\"".$thisFlight->id."\" data-type=\"pickupf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"pickupf-".$thisFlight->id."\" href=\"#\" data-id=\"".$thisFlight->id."\" data-type=\"pickupf\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisFlight->translator != "0") {
			print "\t<td><a id=\"translatorf-".$thisFlight->id."\" href=\"#\" data-id=\"".$thisFlight->id."\" data-type=\"translatorf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorf-".$thisFlight->id."\" href=\"#\" data-id=\"".$thisFlight->id."\" data-type=\"translatorf\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		
		print "\t\t<tr valign=\"top\">\n";
		if ($thisFlight->Notes != "") {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\"><label>Notes:</label> ".$thisFlight->Notes."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\"><label>Notes:</label>&nbsp;</td>\n";		
		}
		print "\t\t</tr>\n";
	}
?>
			<tr valign="top">
				<td align="center" colspan="8">
					<div id="addFlightHere"><input type="hidden" id="flight-id-new" name="flight-id-new" value="0" /></div>
				
					<button id="addFlightButton">Add Flight</button>
				</td>
			</tr>
			</tbody>
		</table>
		</fieldset>
	</div>

	<!-- Hotels Tab -->
	<div id="tabs-3">
		<fieldset class="ui-corner-all">
		<table class="HotelsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<th width="5%">&nbsp;</th>
				<th width="10%">Hotel Name</th>
				<th width="14%">Checking In</th>
				<th width="14%">Checking Out</th>
				<th width="6%">Comp Nights</th>
				<th width="6%">Paid Nights</th>
				<th width="6%">Total Nights</th>
				<th width="12%">Sharing with?</th>
				<th width="12%">Hotel Pickup</th>
				<th width="10%">Translator</th>
			</tr>
<?php
	if (count($hotels) == 0) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"8\" align=\"center\">There are no hotels for this guest yet. Please click the 'Add Hotels' button below to add one.</td>\n";
		print "\t\t</tr>\n";
	}
	foreach ($hotels as $thisHotel) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\"><button id=\"hotel-".$thisHotel->id."\" name=\"hotel-".$thisHotel->id."\" data-id=\"".$thisHotel->id."\" class=\"edit\">Edit</button></td>\n";

		print "\t\t\t<td>".$thisHotel->hotel."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($thisHotel->checkin))."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($thisHotel->checkout))." ".date("g:iA",strtotime($thisHotel->checkouttime))."</td>\n";

		print "\t\t\t<td>".$thisHotel->compnights."</td>\n";
		print "\t\t\t<td>".$thisHotel->paidnights."</td>\n";
		$totalnights = strtotime($thisHotel->checkout) - strtotime($thisHotel->checkin);
		print "\t\t\t<td>".($totalnights / 86400)."</td>\n";

		if ($thisHotel->share == 0) {
			print "\t\t\t<td>None</td>\n";
		} else {
			print "\t\t\t<td>".$thisHotel->share."</td>\n";
		}
		if ($thisHotel->pickup != "0") {
			print "\t<td><a id=\"pickuph-".$thisHotel->id."\" href=\"#\" data-id=\"".$thisHotel->id."\" data-type=\"pickuph\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"pickuph-".$thisHotel->id."\" href=\"#\" data-id=\"".$thisHotel->id."\" data-type=\"pickuph\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisHotel->translator != "0") {
			print "\t<td><a id=\"translatorh-".$thisHotel->id."\" href=\"#\" data-id=\"".$thisHotel->id."\" data-type=\"translatorh\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorh-".$thisHotel->id."\" href=\"#\" data-id=\"".$thisHotel->id."\" data-type=\"translatorh\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		
		print "\t\t<tr valign=\"top\">\n";
		if ($thisHotel->notes != "") {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><label>Notes:</label> ".$thisHotel->notes."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\">&nbsp;</td>\n";		
		}
		print "\t\t</tr>\n";
	}
?>
			<tr valign="top">
				<td align="center" colspan="10">
					<div id="addHotelHere"><input type="hidden" id="hotel-id-new" name="hotel-id-new" value="0" /></div>
				
					<button id="addHotelButton">Add Hotel</button>
				</td>
			</tr>
			</tbody>
		</table>
		</fieldset>
	</div>

	<!-- Screenings Tab -->
	<div id="tabs-4">
		<fieldset class="ui-corner-all">
<?php
	$screen_app_form = "";
	// There are film affiliations, print the screening details.
	if (count($film_affiliations) > 0) {
		foreach ($film_affiliations as $thisFilm) {
			$film_screenings = $this->schedule->get_internal_movie_screenings($festival[0]->startdate, $festival[0]->enddate, $thisFilm->movie_id);
			$film_attendance = $this->guest->get_guest_screening_appearances($guest[0]->id);
?>
		<table class="ScreeningAppTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td colspan="7"><h3><?php print "<a href=\"/admin/film_edit/update/".$thisFilm->film_slug."#tabs-4\" target=\"_blank\">".$thisFilm->film_title."</a>"; ?></h3></td>
			</tr>
			<tr valign="top">
				<th width="10%">&nbsp;</th>
				<th width="14%">Total Runtime</th>
				<th width="12%">Date</th>
				<th width="10%">Time</th>
				<th width="14%">Location</th>
				<th width="20%">Transportation Needed?</th>
				<th width="20%">Translator Needed?</th>
			</tr>
<?php
	if (count($film_screenings) == 0) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"7\" align=\"center\">There are no screenings for this film yet. To add one, <a href=\"/admin/film_edit/update/".$thisFilm->film_slug."#tabs-4\" target=\"_blank\" style=\"text-decoration:underline;\">click here to edit the film</a>. (This opens a new window/tab.)</td>\n";
		print "\t\t</tr>\n";
	}
	foreach ($film_screenings as $thisScreening) {	
		$runtime = 0;
		$film_program_ids = explode(",",$thisScreening->program_movie_ids);
		$film_breakdown = "";

		if ($thisScreening->intro == 1) {
			$film_breakdown .= "Intro (".$thisScreening->intro_length."), ";
			$runtime += $thisScreening->intro_length;
		}
		if ($thisScreening->fest_trailer == 1) {
			$film_breakdown .= "HIFF Trailer (".$thisScreening->fest_trailer_length."), ";
			$runtime += $thisScreening->fest_trailer_length;
		}
		if ($thisScreening->sponsor_trailer1 == 1) {
			$film_breakdown .= "Sponsor Trailer 1 (".$thisScreening->sponsor_trailer1_length."), ";
			$runtime += $thisScreening->sponsor_trailer1_length;
		}
		if ($thisScreening->sponsor_trailer2 == 1) {
			$film_breakdown .= "Sponsor Trailer 2 (".$thisScreening->sponsor_trailer2_length."), ";
			$runtime += $thisScreening->sponsor_trailer2_length;
		}
		foreach ($film_program_ids as $scheduledFilm) {
			foreach ($films as $thisFilm) {
				if ($scheduledFilm == "$thisFilm->movie_id") {
					$film_breakdown .= switch_title($thisFilm->title_en)." (".$thisFilm->runtime_int."), ";
					$runtime += $thisFilm->runtime_int;
					break;
				}
			}
		}		
		if ($thisScreening->q_and_a == 1) {
			$film_breakdown .= "Q&amp;A Session (".$thisScreening->q_and_a_length.")";
			$runtime += $thisScreening->q_and_a_length;
		}
		$film_breakdown = trim($film_breakdown,", ");
		$runtime_string = calculate_runtime($runtime);
		
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td>\n";

		$screen_app_form .= "\n<form id=\"screening_app_".$thisScreening->id."\">\n";
		
		$last_attend_id = 0;
		$nomatch = 0;
		foreach ($film_attendance as $thisAttend) {
			if ($thisAttend->screening_id == $thisScreening->id) {
				print "<button id=\"attend-".$thisScreening->id."\" name=\"attend-".$thisScreening->id."\" data-formname=\"#screening_app_".$thisScreening->id."\" class=\"remove ui-state-error\">Remove</button>";
				$last_attend_id = $thisAttend->id;
			} else {
				$nomatch++;
			}
		}		
		if (count($film_attendance) == 0 || count($film_attendance) == $nomatch) {
			print "<button id=\"attend-".$thisScreening->id."\" name=\"attend-".$thisScreening->id."\" data-formname=\"#screening_app_".$thisScreening->id."\" class=\"add\">Add</button>";
		}

		$screening_fields = array(
			'guest_id' => $guest[0]->id,
			'attend_id' => $last_attend_id,
			'screening_id' => $thisScreening->id,
			'runtime' => $runtime_string,
			'date' => date("F j, Y",strtotime($thisScreening->date)),
			'time' => date("g:iA",strtotime($thisScreening->time)),
			'location' => $thisScreening->location
		);
		$screen_app_form .= form_hidden($screening_fields);
		$screen_app_form .= "</form>\n\n";

		print "\t\t\t</td>\n";
		print "\t\t\t<td>".$runtime_string."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($thisScreening->date))."</td>\n";
		print "\t\t\t<td>".date("g:iA",strtotime($thisScreening->time))."</td>\n";
		print "\t\t\t<td>".$thisScreening->location;
		if ($thisScreening->location_id2 != 0) { print " / ".$thisScreening->location2; }
		print "</td>\n";
		
		$nomatch = 0;
		foreach ($film_attendance as $thisAttend) {
			if ($thisAttend->screening_id == $thisScreening->id) {
				if ($thisAttend->transport != "0") {
					print "\t<td><a id=\"transportsc-".$thisAttend->id."\" href=\"#\" data-id=\"".$thisAttend->id."\" data-type=\"transportsc\" class=\"icon_tick\"></a></td>\n";
				} else {
					print "\t<td><a id=\"transportsc-".$thisAttend->id."\" href=\"#\" data-id=\"".$thisAttend->id."\" data-type=\"transportsc\" class=\"icon_cross\"></a></td>\n";
				}
				if ($thisAttend->translator != "0") {
					print "\t<td><a id=\"translatorsc-".$thisAttend->id."\" href=\"#\" data-id=\"".$thisAttend->id."\" data-type=\"translatorsc\" class=\"icon_tick\"></a></td>\n";
				} else {
					print "\t<td><a id=\"translatorsc-".$thisAttend->id."\" href=\"#\" data-id=\"".$thisAttend->id."\" data-type=\"translatorsc\" class=\"icon_cross\"></a></td>\n";
				}
			} else {
				$nomatch++;
			}
		}
		if (count($film_attendance) == 0 || count($film_attendance) == $nomatch) { print "\t<td>&nbsp;</td>\n"; print "\t<td>&nbsp;</td>\n"; }
		print "\t\t</tr>\n";

	}
?>        
			</tbody>
		</table>
<?php
		}
	} else { // No film affiliations
?>
		<table class="ScreeningAppTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
				<tr valign="top">
					<td align="center">This guest is not affiliated with any films. Please go to the <b>Guest Details</b> tab and add a film affiliation, then click the 'SAVE' button to refresh the page.</td>
				</tr>
			</tbody>
		</table>
<?php
	}
?>        
		</fieldset>
	</div>

	<!-- Event Appearances Tab -->
	<div id="tabs-5">
		<fieldset class="ui-corner-all">
		<table class="SpecialAppTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<th width="10%">&nbsp;</th>
				<th width="22%">Location</th>
				<th width="17%">Date</th>
				<th width="15%">Time</th>
				<th width="18%">Transportation Needed?</th>
				<th width="18%">Translator Needed?</th>
			</tr>
<?php
	if (count($event_appearances) == 0) {
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"6\" align=\"center\">There are no event appearances for this guest yet. To add one, please click the 'Add Event Appearance' button below.</td>\n";
		print "\t\t</tr>\n";
	}
	foreach ($event_appearances as $thisAppearance) {	
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><button id=\"appear-".$thisAppearance->id."\" name=\"appear-".$thisAppearance->id."\" data-id=\"".$thisAppearance->id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td>".$thisAppearance->location."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($thisAppearance->date))."</td>\n";
		print "\t\t\t<td>".date("g:iA",strtotime($thisAppearance->time))."</td>\n";
		if ($thisAppearance->transport != "0") {
			print "\t<td><a id=\"transportsp-".$thisAppearance->id."\" href=\"#\" data-id=\"".$thisAppearance->id."\" data-type=\"transportsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"transportsp-".$thisAppearance->id."\" href=\"#\" data-id=\"".$thisAppearance->id."\" data-type=\"transportsp\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisAppearance->translator != "0") {
			print "\t<td><a id=\"translatorsp-".$thisAppearance->id."\" href=\"#\" data-id=\"".$thisAppearance->id."\" data-type=\"translatorsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorsp-".$thisAppearance->id."\" href=\"#\" data-id=\"".$thisAppearance->id."\" data-type=\"translatorsp\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
	}
?>
			<tr valign="top">
				<td align="center" colspan="10">
					<div id="addEventAppHere"><input type="hidden" id="appearance-id-new" name="appearance-id-new" value="0" /></div>
				
					<button id="addEventAppButton">Add Event Appearance</button>
				</td>
			</tr>
			</tbody>
		</table>
		</fieldset>
	</div>
	<div id="tabs-6">
		<fieldset class="ui-corner-all">
			<table class="PhotoTab" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tbody style="border-top:none;">
					<tr valign="top">
						<td width="50%">
							<h3>Upload a Guest Photo</h3>
							<label for="guest-photo-new">Attach a Photo</label> <span class="req">*</span><br />
							<?php print form_upload(array('name'=>'guest-photo-new', 'id'=>'guest-photo-new', 'value'=>'', 'class'=>'text-upload', 'size'=>'40')); ?>
							<p>Any uploaded photo will replace the current photo, if there is one. Supported image file formats are: <strong>.JPG&nbsp;&nbsp;.GIF&nbsp;&nbsp;.PNG</strong></p>
							<button id="uploadPhotoButton">Upload Photo</button>
						</td><td width="50%">
							<h3>Current Guest Photo</h3>
						<?php
							if ($guest[0]->PhotoUrl != "") {
								print "<p><a href=\"".$guest[0]->PhotoUrl."\" target=\"_blank\">Download Guest Photo</a></p>\n";
								print "<div id=\"addGuestPhotoHere\"></div>\n";
							} else {
								print "<p>No Photo Uploaded</p>\n";
								print "<div id=\"addGuestPhotoHere\"></div>\n";
							} 
						?>                            
						</td>
					</tr>
				</tbody>
			</table>
		</fieldset>
	</div>
</div>
<?php
	print form_close();

	// Print Screening App Forms - Here because they can't be within the "Save / Delete" form above.
	print $screen_app_form;

	// Start Flight Dialogs
	$dType = "add"; $dTypeCaps = "Add";  $count = 0;
	while ($count < 2) {
		if ($count != 0) { $dType = "edit"; $dTypeCaps = "Edit"; }
?>
<div id="<?php print $dType; ?>Flight-dialog" title="<?php print $dTypeCaps; ?> Flight">
<form name="<?php print $dType; ?>FlightForm" id="<?php print $dType; ?>FlightForm">

<?php if ($count == 0) { // Add Flight IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="flight_id" id="flight_id">
<?php } else { // Edit Flight IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="flight_id_update" id="flight_id_update">
<?php } ?>

	<table class="FlightDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="49%" colspan="2">
				<h3>Arriving Flight</h3>
			</td>
			<td width="2%" rowspan="5">&nbsp;</td>
			<td width="49%" colspan="2">
				<h3>Departing Flight</h3>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="arr-date-<?php print $dType; ?>">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'arr-date-'.$dType, 'id'=>'arr-date-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="arr-time-<?php print $dType; ?>">Time:</label>&nbsp;&nbsp;&nbsp; like '7:00 PM'<br />
				<?php print form_input(array('name'=>'arr-time-'.$dType, 'id'=>'arr-time-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="dep-date-<?php print $dType; ?>">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'dep-date-'.$dType, 'id'=>'dep-date-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="dep-time-<?php print $dType; ?>">Time:</label>&nbsp;&nbsp;&nbsp; like '7:00 PM'<br />
				<?php print form_input(array('name'=>'dep-time-'.$dType, 'id'=>'dep-time-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="arr-city1-<?php print $dType; ?>">From Airport:</label> <span class="req">*</span><br />
				<?php print_airport_select($air_domestic, $air_intl,'arr-city1-'.$dType, "", 'select ui-widget-content ui-corner-all'); ?>
			</td>
			<td colspan="2">
				<label for="dep-city1-<?php print $dType; ?>">From Airport:</label> <span class="req">*</span><br />
				<?php print_airport_select($air_domestic, $air_intl,'dep-city1-'.$dType, "HNL", 'select ui-widget-content ui-corner-all'); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="arr-city2-<?php print $dType; ?>">To Airport:</label> <span class="req">*</span><br />
				<?php print_airport_select($air_domestic, $air_intl,'arr-city2-'.$dType, "HNL", 'select ui-widget-content ui-corner-all'); ?>
			</td>
			<td colspan="2">
				<label for="dep-city2-<?php print $dType; ?>">To Airport:</label> <span class="req">*</span><br />
				<?php print_airport_select($air_domestic, $air_intl,'dep-city2-'.$dType, "", 'select ui-widget-content ui-corner-all'); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="arr-airline-<?php print $dType; ?>">Airline:</label> <span class="req">*</span><br />
				<?php print form_dropdown('arr-airline-'.$dType, $airlines_array, 0,"id='arr-airline-".$dType."' class='select-small ui-widget-content ui-corner-all'"); ?>
			</td>
			<td>
				<label for="arr-fltnum-<?php print $dType; ?>">Flight Number:</label><br />
				<?php print form_input(array('name'=>'arr-fltnum-'.$dType, 'id'=>'arr-fltnum-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="dep-airline-<?php print $dType; ?>">Airline:</label> <span class="req">*</span><br />
				<?php print form_dropdown('dep-airline-'.$dType, $airlines_array, 0,"id='dep-airline-".$dType."' class='select-small ui-widget-content ui-corner-all'"); ?>
			</td>
			<td>
				<label for="dep-fltnum-<?php print $dType; ?>">Flight Number:</label><br />
				<?php print form_input(array('name'=>'dep-fltnum-'.$dType, 'id'=>'dep-fltnum-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="5">
				<h3>Other Flight Details</h3>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="flight-comp-<?php print $dType; ?>">Flight Comp Options:</label><br />
				<?php print form_dropdown('flight-comp-'.$dType, $flight_comp_array2, 0,"id='flight-comp-".$dType."' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="2%" rowspan="3">&nbsp;</td>
			<td colspan="2" rowspan="3">
				<label for="flight-notes-<?php print $dType; ?>">Notes:</label><br />
				<?php print form_textarea(array('name'=>'flight-notes-'.$dType, 'id'=>'flight-notes-'.$dType, 'value'=>'', 'rows'=>'6', 'cols'=>'41', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="flight-cost-<?php print $dType; ?>">Cost:</label><br />
				<?php print "$".form_input(array('name'=>'flight-cost-'.$dType, 'id'=>'flight-cost-'.$dType, 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="flight-mileage-<?php print $dType; ?>">Mileage:</label><br />
				<?php print form_input(array('name'=>'flight-mileage-'.$dType, 'id'=>'flight-mileage-'.$dType, 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="flight-pickup-<?php print $dType; ?>">Airport Pickup?</label><br />
				<?php print form_checkbox(array('name'=>'flight-pickup-'.$dType, 'id'=>'flight-pickup-'.$dType, 'value'=>'1', 'checked'=>false)); ?>
			</td>
			<td>
				<label for="flight-translator-<?php print $dType; ?>">Translator Req'd?</label><br />
				<?php print form_checkbox(array('name'=>'flight-translator-'.$dType, 'id'=>'flight-translator-'.$dType, 'value'=>'1', 'checked'=>false)); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<?php
	$count++; }
	// End Flight Dialogs

	// Start Hotel Dialogs
	$dType = "add"; $dTypeCaps = "Add";  $count = 0;
	while ($count < 2) {
		if ($count != 0) { $dType = "edit"; $dTypeCaps = "Edit"; }
?>
<div id="<?php print $dType; ?>Hotel-dialog" title="<?php print $dTypeCaps; ?> Hotel">
<form name="<?php print $dType; ?>HotelForm" id="<?php print $dType; ?>HotelForm">

<?php if ($count == 0) { // Add Hotel IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="hotel_id" id="hotel_id">
<?php } else { // Edit Hotel IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="hotel_id_update" id="hotel_id_update">
<?php } ?>

	<table class="HotelDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="25%" colspan="3">
				<label for="hotel-<?php print $dType; ?>">Hotel Name:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'hotel-'.$dType, 'id'=>'hotel-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%" colspan="3">
				<label for="checkin-<?php print $dType; ?>">Check In Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'checkin-'.$dType, 'id'=>'checkin-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="checkout-<?php print $dType; ?>">Check Out Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'checkout-'.$dType, 'id'=>'checkout-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="checkouttime-<?php print $dType; ?>">Check Out Time:</label><br />
				<?php print form_input(array('name'=>'checkouttime-'.$dType, 'id'=>'checkouttime-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="6">
				<label for="hotel-comp-<?php print $dType; ?>">Hotel Comp Options:</label><br />
				<?php print form_dropdown('hotel-comp-'.$dType, $hotel_comp_array, 0,"id='hotel-comp-".$dType."' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="2" rowspan="4">
				<label for="hotel-notes-<?php print $dType; ?>">Notes</label><br />
				<?php print form_textarea(array('name'=>'hotel-notes-'.$dType, 'id'=>'hotel-notes-'.$dType, 'value'=>'', 'rows'=>'8', 'cols'=>'41', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="comp-nights-<?php print $dType; ?>">Comp Nights:</label><br />
				<?php print form_input(array('name'=>'comp-nights-'.$dType, 'id'=>'comp-nights-'.$dType, 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="paid-nights-<?php print $dType; ?>">Paid Nights:</label><br />
				<?php print form_input(array('name'=>'paid-nights-'.$dType, 'id'=>'paid-nights-'.$dType, 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="total-nights-<?php print $dType; ?>">Total Nights:</label><br />
				<?php print "<span id=\"total-nights-".$dType."\">0</span>"; ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="6">
				<label for="hotel-share-<?php print $dType; ?>">Sharing With:</label><br />
				<?php print form_dropdown('hotel-share-'.$dType, $hotel_shares_array, 0,"id='hotel-share-".$dType."' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="3">
				<label for="hotel-pickup-<?php print $dType; ?>">Hotel Pickup?</label><br />
				<?php print form_checkbox(array('name'=>'hotel-pickup-'.$dType, 'id'=>'hotel-pickup-'.$dType, 'value'=>'1', 'checked'=>false)); ?>
			</td>
			<td colspan="3">
				<label for="hotel-translator-<?php print $dType; ?>">Translator Req'd?</label><br />
				<?php print form_checkbox(array('name'=>'hotel-translator-'.$dType, 'id'=>'hotel-translator-'.$dType, 'value'=>'1', 'checked'=>false)); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<?php
	$count++; }
	// End Hotel Dialogs

	// Start Event Appearance Dialogs
	$dType = "add"; $dTypeCaps = "Add";  $count = 0;
	while ($count < 2) {
		if ($count != 0) { $dType = "edit"; $dTypeCaps = "Edit"; }
?>
<div id="<?php print $dType; ?>EventApp-dialog" title="<?php print $dTypeCaps; ?> Event Appearance">
<form name="<?php print $dType; ?>EventAppForm" id="<?php print $dType; ?>EventAppForm">

<?php if ($count == 0) { // Add Event Appearance IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="eventapp_id" id="eventapp_id">
<?php } else { // Edit Event Appearance IDs ?>
	<input type="hidden" value="<?php print $guest[0]->id; ?>" name="guest_id" id="guest_id">
	<input type="hidden" value="0" name="eventapp_id_update" id="eventapp_id_update">
<?php } ?>

	<table class="EventAppDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
<?php if ($dType == "add") { ?>
		<tr valign="top">
			<td colspan="2">
				<label for="event-existing-<?php print $dType; ?>">Add to existing event:</label><br />
				<?php print form_dropdown('event-existing-'.$dType, $existing_events_array, 0,"id='event-existing-".$dType."' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
<?php } ?>
		<tr valign="top">
			<td colspan="2">
				<label for="event-location-<?php print $dType; ?>">Location Name:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'event-location-'.$dType, 'id'=>'event-location-'.$dType, 'value'=>'', 'class'=>'text-large ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td width="50%">
				<label for="event-date-<?php print $dType; ?>">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'event-date-'.$dType, 'id'=>'event-date-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="50%">
				<label for="event-time-<?php print $dType; ?>">Time:</label> <span class="req">*</span>&nbsp;&nbsp;&nbsp; like '7:00 PM'<br />
				<?php print form_input(array('name'=>'event-time-'.$dType, 'id'=>'event-time-'.$dType, 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="event-transport-<?php print $dType; ?>">Transportation?</label><br />
				<?php print form_checkbox('event-transport-'.$dType,'1',false,' id="event-transport-'.$dType.'"'); ?>
			</td>
			<td>
				<label for="event-translator-<?php print $dType; ?>">Translator Req'd?</label><br />
				<?php print form_checkbox('event-translator-'.$dType,'1',false,' id="event-translator-'.$dType.'"'); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<?php
	$count++; }
	// End Event Appearance Dialogs
?>

<script type="text/javascript">
$(function() {
	$("#tabs").tabs();
	$("input:submit").button();
	$("button").button();
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});
	
	$("#UpdateGuest").validate({
		rules: { Firstname: "required", Lastname: "required", guesttype_id: { required:true, min:1 }, Email: { required:true, email:true }, FacebookPage: "url", LinkedinProfile: "url", WebsiteURL: "url" },
		messages: { Firstname: "Please enter a first name.", Lastname: "Please enter a last name.", guesttype_id: "Please select a guest type.", Email: "Please enter a valid email address.", FacebookPage: "Please enter a valid URL (including http://).", LinkedinProfile: "Please enter a valid URL (including http://).", WebsiteURL: "Please enter a valid URL (including http://)." }
	});

	// Guest Details Tab
	$('#film_aff').on('click', 'a.remove', function() {
		$(this).prev('select').remove();
		$(this).next('br').remove();
		$(this).remove();
		var toUpdate = $('#film_aff_movie_ids');
		toUpdate.val('');
		$('#film_aff SELECT').each(function() {
			toUpdate.val(toUpdate.val() + $(this).val()+',');
		});
	});

	$('#film_aff').on('change', 'select', function() {
		var toUpdate = $('#film_aff_movie_ids');
		toUpdate.val('');
		$('#film_aff SELECT').each(function() {
			toUpdate.val(toUpdate.val() + $(this).val()+',');
		});
	});

	$('#addFilmAff').on('click', function() {
		$.ajax({
			success: function(msg, text, xhr) {
				$('#addFilmAffHere').replaceWith(msg);
				var new_filmaff_id = $('#addFilmAffId').val();
				
				// Update #film_aff_ids with new value
				var existing_film_ids = $('#film_aff_ids').val();
				$('#film_aff_ids').val(existing_film_ids+","+new_filmaff_id);
			},
			error: function(xhr, msg1, msg2){
				alert( "Failure! " + xhr + msg1 + msg2); },
			data: $('#addFlightForm').serialize(),
			url: '/admin/guest_edit/add_filmaff/',
			type: 'POST',
			dataType: 'html'
		});
		return false;
	});

	// Flights Tab
	$("#addFlight-dialog").dialog({
		autoOpen: false,
		height: 575,
		width: 635,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Add Flight': function() {
				if ($("#addFlightForm").validate().form() == true) {

					$.ajax({
						success: function(msg, text, xhr) {
							$('#addFlightHere').replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									flightJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_flight_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addFlightForm').serialize(),
						url: '/admin/guest_edit/add_flight/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		},
		close: function() {
			$("#addFlightForm INPUT[type='text']").each(function() { $(this).val(""); });
			$("#addFlightForm INPUT[type='checkbox']").each(function() { $(this).prop('checked',false); });
			$("#addFlightForm TEXTAREA").each(function() { $(this).val(""); });
			$("#addFlightForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
		}
	});
	$('#addFlightButton').on('click', function() { $('#addFlight-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); return false; });
	$('#arr-date-add, #arr-date-edit, #dep-date-add, #dep-date-edit').each(function() {
		$(this).datepicker();
	});

	var flightJSON = <?php print $flight_json; ?>;
	var flightcompJSON = <?php print $flight_comp_json; ?>;
	
	$('#flight-comp-add').on('change', function() { update_flight_mileage("add", flightcompJSON); });
	$('#flight-comp-edit').on('change', function() { update_flight_mileage("edit", flightcompJSON); });

	$('.FlightsTab').on('click','button.edit', function(e) {
		e.preventDefault();
		open_edit_flight_dialog($(this).data('id').toString(), flightJSON);
		$(this).removeClass('ui-state-focus');
	});

	$("#editFlight-dialog").dialog({
		autoOpen: false,
		height: 575,
		width: 635,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var flight_id = $('#flight_id_update').val();
				var decision = confirm("Are you sure you really want to delete this Flight?");
				if (decision == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#flight-'+flight_id).closest("tr").next("tr").remove();
							$('#flight-'+flight_id).closest("tr").remove();
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editFlightForm').serialize(),
						url: '/admin/guest_edit/delete_flight/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			},
<?php } ?>
			'Update Flight': function() {
				var flight_id = $('#flight_id_update').val();
				if ($("#editFlightForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#flight-'+flight_id).closest("tr").next("tr").remove();
							$('#flight-'+flight_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									flightJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_flight_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editFlightForm').serialize(),
						url: '/admin/guest_edit/update_flight/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		}
	});

	// Hotels Tab
	$("#addHotel-dialog").dialog({
		autoOpen: false,
		height: 400,
		width: 615,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Add Hotel': function() {
				if ($("#addHotelForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#addHotelHere').replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									hotelJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_hotel_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addHotelForm').serialize(),
						url: '/admin/guest_edit/add_hotel/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		},
		close: function() {
			$("#addHotelForm INPUT[type='text']").each(function() { $(this).val(""); });
			$("#addHotelForm INPUT[type='checkbox']").each(function() { $(this).prop('checked',false); });
			$("#addHotelForm TEXTAREA").each(function() { $(this).val(""); });
			$("#addHotelForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
		}
	});
	$('#addHotelButton').on('click', function() { $('#addHotel-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); return false; });

	$('#checkin-add, #checkout-add, #checkin-edit, #checkout-edit').each(function() {
		$(this).datepicker();
	});

	var hotelJSON = <?php print $hotel_json; ?>;

	$('.HotelsTab').on('click','button.edit', function(e) {
		e.preventDefault();
		open_edit_hotel_dialog($(this).data('id').toString(), hotelJSON);
		$(this).removeClass('ui-state-focus');
	});

	$("#editHotel-dialog").dialog({
		autoOpen: false,
		height: 400,
		width: 615,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var hotel_id = $('#hotel_id_update').val();
				var decision = confirm("Are you sure you really want to delete this Hotel?");
				if (decision == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#hotel-'+hotel_id).closest("tr").next("tr").remove();
							$('#hotel-'+hotel_id).closest("tr").remove();
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editHotelForm').serialize(),
						url: '/admin/guest_edit/delete_hotel/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			},
<?php } ?>
			'Update Hotel': function() {
				var hotel_id = $('#hotel_id_update').val();
				if ($("#editHotelForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#hotel-'+hotel_id).closest("tr").next("tr").remove();
							$('#hotel-'+hotel_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									hotelJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_hotel_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editHotelForm').serialize(),
						url: '/admin/guest_edit/update_hotel/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		}
	});
	$('#checkin-add').on('change', function() { update_total_nights("add"); });
	$('#checkout-add').on('change', function() { update_total_nights("add") });
	$('#checkin-edit').on('change', function() { update_total_nights("edit") });
	$('#checkout-edit').on('change', function() { update_total_nights("edit") });

	$('#comp-nights-add').on('change', function() { update_paid_nights("add") });
	$('#comp-nights-edit').on('change', function() { update_paid_nights("edit") });
	$('#paid-nights-add').on('change', function() { update_comp_nights("add") });
	$('#paid-nights-edit').on('change', function() { update_comp_nights("edit") });


	// Screenings Tab
	$('.ScreeningAppTab').on('click','button.remove', function(e) {
		e.preventDefault();
		toggle_screening_app($(this).data('formname').toString(), this, 'delete');
		$(this).removeClass('ui-state-focus');
	});
	$('.ScreeningAppTab').on('click','button.add', function(e) {
		e.preventDefault();
		toggle_screening_app($(this).data('formname').toString(), this, 'add');
		$(this).removeClass('ui-state-focus');
	});


	// Appearances Tab
	$("#addEventAppForm").validate({
		rules: { "event-location-add": "required", "event-date-add": "required", "event-time-add": "required" },
		messages: { "event-location-add": "Please enter a location.", "event-date-add": "Please enter a date.", "event-time-add": "Please enter a time." }
	});
	$("#addEventApp-dialog").dialog({
		autoOpen: false,
		height: 350,
		width: 330,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Add Appearance': function() {
				if ($("#addEventAppForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#addEventAppHere').replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									eventappJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_eventapp_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addEventAppForm').serialize(),
						url: '/admin/guest_edit/add_event_appearance/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		}
	});
	$('#addEventAppButton').on('click', function() { $('#addEventApp-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); return false; });
	$('#event-date-add').datepicker(); $('#event-date-edit').datepicker();

	var eventappJSON = <?php print $eventapp_json; ?>;

	$('.SpecialAppTab').on('click','button.edit', function(e) {
		e.preventDefault();
		open_edit_eventapp_dialog($(this).data('id').toString(), eventappJSON);
		$(this).removeClass('ui-state-focus');
	});

	var existingeventsJSON = <?php print $existingevents_json; ?>;
	$('#event-existing-add').on('change', function() { update_eventapp_values(existingeventsJSON); });

	$("#editEventAppForm").validate({
		rules: { "event-location-edit": "required", "event-date-edit": "required", "event-time-edit": "required" },
		messages: { "event-location-edit": "Please enter a location.", "event-date-edit": "Please enter a date.", "event-time-edit": "Please enter a time." }
	});
	$("#editEventApp-dialog").dialog({
		autoOpen: false,
		height: 300,
		width: 330,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var eventapp_id = $('#eventapp_id_update').val();
				var decision = confirm("Are you sure you really want to delete this Event Appearance?");
				if (decision == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#appear-'+eventapp_id).closest("tr").remove();
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editEventAppForm').serialize(),
						url: '/admin/guest_edit/delete_event_appearance/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			},
<?php } ?>
			'Update': function() {
				var eventapp_id = $('#eventapp_id_update').val();
				if ($("#editEventAppForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#appear-'+eventapp_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									eventappJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_edit/return_eventapp_json/<?php print $guest[0]->id."/"; ?>',
								type: 'POST',
								dataType: 'json'
							});
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editEventAppForm').serialize(),
						url: '/admin/guest_edit/update_event_appearance/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		}
	});

	$('#uploadPhotoButton').on('click', function() {
		var selectedFile = $('#guest-photo-new').val();
		return false;
	});

	$('#tabs').on('click','a.icon_cross', function(e) {
		e.preventDefault();
		toggle_value_guest($(this).data('type').toString(), $(this).data('id'));
	});
	$('#tabs').on('click','a.icon_tick', function(e) {
		e.preventDefault();
		toggle_value_guest($(this).data('type').toString(), $(this).data('id'));
	});
	
	$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });
	$('#Save').on('click', function() { $('#UpdateGuest').submit(); });
<?php if ($this->session->userdata('limited') == 0) {
	$guestname = addslashes($guest[0]->Firstname." ".$guest[0]->Lastname);
?>	
	$('#Delete').on('click', function() { delete_guest(<?php print $guest[0]->id.",'".$guestname."'"; ?>); return false; });
<?php } ?>
});
</script>
