<?php
	$airlines_array = convert_to_array($airlines);

	print "<div id=\"tabs\" class=\"smaller-tabs\">\n";
	print "\t<ul>\n";
	$x = 1;
	foreach ($days as $thisDay) {
		print "\t\t<li><a href=\"#tabs-".$x."\">".date("n/j",strtotime($thisDay))."</a></li>\n";
		$x++;
	}
	print "\t</ul>\n";

	$x = 1;
	foreach ($days as $thisDay) {
		$totalguests = 0;
		print "\t<div id=\"tabs-".$x."\">\n";
    	print "\t\t<fieldset class=\"ui-corner-all\">\n";
		
		foreach ($guestids as $thisGuest)	{
			$showguest = 0;
			foreach ($flights as $thisFlight) {
				if ($thisFlight->festivalguest_id == $thisGuest->guest_id && ($thisDay == $thisFlight->ArrivalDate || $thisDay == $thisFlight->DepartureDate)) { $showguest = 1; }
			}
			foreach ($hotels as $thisHotel) {
				if ($thisHotel->festivalguest_id == $thisGuest->guest_id && ($thisDay == $thisHotel->checkin || $thisDay == $thisHotel->checkout)) { $showguest = 1; }
			}
			
			if ($showguest == 1) { // Don't show guest if they don't have a hotel or flight for this day
				print "\t\t\t<h3 style=\"text-align:left;\"><a href=\"/admin/guest_edit/update/".$thisGuest->value."\">".$thisGuest->label."</a></h3>\n";
				print "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\"><tbody style=\"border-top:none;\">";
				$row = 0;
				foreach ($flights as $thisFlight) {
					if ($thisFlight->festivalguest_id == $thisGuest->guest_id && ($thisDay == $thisFlight->ArrivalDate || $thisDay == $thisFlight->DepartureDate)) {
						if ($row == 0) {
							print "<tr><th width=\"10%\">&nbsp;</th><th width=\"15%\">Date/Time</th><th width=\"5%\">From</th><th width=\"5%\">To</th><th width=\"15%\">Airline</th><th width=\"10%\">Flight #</th><th width=\"10%\">Pickup?</th><th width=\"10%\">Translator?</th><th width=\"20%\">Notes</th></tr>";
						}
						if ($thisDay == $thisFlight->ArrivalDate) {
							print "<tr><td><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/airplane.png\" alt=\"Flight\" title=\"Flight\"> Arrival</td><td>".date("n/j/Y",strtotime($thisFlight->ArrivalDate))." ".date("g:i A",strtotime($thisFlight->ArrivalTime))."</td><td>".$thisFlight->ArrivalCity."</td><td>".$thisFlight->ArrivalCity2."</td><td>";
							if ($thisFlight->Airline != 0) { print $airlines_array[$thisFlight->Airline]; }
							print "</td><td>".$thisFlight->FlightNumber."</td>";
							if ($thisFlight->pickup == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							if ($thisFlight->translator == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							print "<td>".$thisFlight->Notes."</td>";
							print "</tr>";
						} elseif ($thisDay == $thisFlight->DepartureDate) {
							print "<tr><td><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/airplane.png\" alt=\"Flight\" title=\"Flight\"> Departure</td><td>".date("n/j/Y",strtotime($thisFlight->DepartureDate))." ".date("g:i A",strtotime($thisFlight->DepartureTime))."</td><td>".$thisFlight->DepartureCity."</td><td>".$thisFlight->DepartureCity2."</td><td>";
							if ($thisFlight->DepartureAirline != 0) { print $airlines_array[$thisFlight->DepartureAirline]; }
							print "</td><td>".$thisFlight->DepartureFlightNum."</td>";
							if ($thisFlight->pickup == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							if ($thisFlight->translator == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							print "<td>".$thisFlight->Notes."</td>";
							print "</tr>";
						}
						$row++;
					}
				}
				$row = 0;
				foreach ($hotels as $thisHotel) {
					if ($thisHotel->festivalguest_id == $thisGuest->guest_id && ($thisDay == $thisHotel->checkin || $thisDay == $thisHotel->checkout)) {
						if ($row == 0) {
							print "<tr><th width=\"10%\">&nbsp;</th><th width=\"15%\">Date/Time</th><th colspan=\"4\" width=\"35%\">Hotel Name</th><th width=\"10%\">Pickup?</th><th width=\"10%\">Translator?</th><th width=\"20%\">Notes</th></tr>";
						}
						if ($thisDay == $thisHotel->checkin) {
							print "<tr><td><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/building.png\" alt=\"Hotel\" title=\"Hotel\"> Check In</td><td>".date("n/j/Y",strtotime($thisHotel->checkin))."</td><td colspan=\"4\">".$thisHotel->hotel."</td>";
							if ($thisHotel->pickup == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							if ($thisHotel->translator == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							print "<td>".$thisHotel->notes."</td></tr>";
						} elseif ($thisDay == $thisHotel->checkout) {
							print "<tr><td><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/building.png\" alt=\"Hotel\" title=\"Hotel\"> Check Out</td><td>".date("n/j/Y",strtotime($thisHotel->checkout))." ".date("g:i A",strtotime($thisHotel->checkouttime))."</td><td colspan=\"4\">".$thisHotel->hotel."</td>";
							if ($thisHotel->pickup == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							if ($thisHotel->translator == 0) { print "<td>No</td>"; } else { print "<td>Yes</td>"; }
							print "<td>".$thisHotel->notes."</td></tr>";
						}
						$row++;
					}
				}
				print "</tbody></table><br />";
				$totalguests++;
			}			
		}
		
		if ($totalguests == 0) {
			print "<p>No guests have flights or hotels on this date.</p>\n";
		}
        print "\t\t</fieldset>\n";
		print "\t</div>\n";
		$x++;
	}
	print "</div>"; // End tabs
?>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();
				
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});		
	});
</script>
