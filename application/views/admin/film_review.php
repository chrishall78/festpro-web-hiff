<?php
	$festivals_array = convert_festival_to_array2($festivals);
	$videoformat_array = convert_to_array2($videoformat);
	$soundformat_array = convert_to_array2($soundformat);
	$color_array = convert_to_array2($color);
	$aspectratio_array = convert_to_array2($aspectratio);
	$premiere_array = convert_to_array2($premiere);
	$distribution_array = convert_to_array2($distribution);

	$sections_array = convert_to_array($sections);
	$events_array = convert_to_array($eventtypes);
?>
<p>To submit a film manually through this form, <a href="/filmmakers/" target="_blank">please click here</a>.</p>

	<fieldset class="ui-corner-all" style="padding:0;">
		<table class="FilmDataReviewTab" width="100%" cellpadding="0" cellspacing="0" border="0">
        	<thead>
			<tr valign="top">
				<th style="width:96px; text-align:center;"><form action="/admin/film_reports/preview_film_review/download/" method="post" id="film-review-export"><button id="exportFilmReview">Export</button></form></th>
				<th style="width:200px">English Title<br /><span><a href="/admin/film_review/sort/film/nameasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by English Title" /></a> <a href="/admin/film_review/sort/film/namedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by English Title" /></a></span></th>
				<th style="width:113px">Submission Date<br /><span><a href="/admin/film_review/sort/film/dateasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Submission Date" /></a> <a href="/admin/film_review/sort/film/datedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Submission Date" /></a></span></th>
				<th style="width:180px">Contact Name<br /><span><a href="/admin/film_review/sort/film/contactasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Contact Name" /></a> <a href="/admin/film_review/sort/film/contactdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Contact Name" /></a></span></th>
                <th style="width:188px">Distributor<br /><span><a href="/admin/film_review/sort/film/distrasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Distributor Info" /></a> <a href="/admin/film_review/sort/film/distrdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Distributor Info" /></a></span></th>
                <th style="width:80px">Countries </th>
                <th style="width:80px">Languages</th>
                <th style="width:80px">Genres</th>
			</tr>
            </thead>
    	    <tbody>
<?php
	$x = 1;
	foreach ($film_items as $thisFilm) {
		$country_string = $language_string = $genre_string = "";
		if ($thisFilm->Country != "") { $filmCountries = explode(",",$thisFilm->Country); } else { $filmCountries = array(); }
		if ($thisFilm->Language != "") { $filmLanguages = explode(",",$thisFilm->Language); } else { $filmLanguages = array(); }
		if ($thisFilm->Genre != "") { $filmGenres = explode(",",$thisFilm->Genre); } else { $filmGenres = array(); }

		foreach ($filmCountries as $thisCountry) {
			foreach ($countries as $country) {
				if ($thisCountry == $country->id) { $country_string .= $country->name.", "; break; }
			}
		}
		if ($country_string == "") { $country_string = "None"; }
		else { $country_string = trim($country_string, ", "); }

		foreach ($filmLanguages as $thisLanguage) {
			foreach ($languages as $language) {
				if ($thisLanguage == $language->id) { $language_string .= $language->name.", "; break; }
			}
		}		
		if ($language_string == "") { $language_string = "None"; }
		else { $language_string = trim($language_string, ", "); }

		foreach ($filmGenres as $thisGenre) {
			foreach ($genres as $genre) {
				if ($thisGenre == $genre->id) { $genre_string .= $genre->name.", "; break; }
			}
		}		
		if ($genre_string == "") { $genre_string = "None"; }
		else { $genre_string = trim($genre_string, ", "); }

		if ($thisFilm->Imported == 1) {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n"; }
		} else {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		}
	
		print "\t\t\t\t<td rowspan=\"2\" align=\"center\">";
		print "<button id=\"film-review-".$thisFilm->id."\" value=\"".$thisFilm->EnglishTitle."\" data-id=\"".$thisFilm->id."\" class=\"import\">Import</button><br />";
		print "<button id=\"film-view-".$thisFilm->id."\" data-id=\"".$thisFilm->id."\" class=\"view\">View</button>";
		print "</td>\n";
		print "\t\t\t\t<td><strong>".$thisFilm->EnglishTitle."</strong></td>\n";
		print "\t\t\t\t<td>".date("m-d-Y",strtotime($thisFilm->SubmittingDate))."</td>\n";
		if ($thisFilm->Imported == 1) {
			$importedName = "Unknown";
			if ($thisFilm->ImportedBy != 0) {
				foreach ($user_list as $thisUser) {
					if ($thisFilm->ImportedBy == $thisUser->id) {
						$importedName = $thisUser->Username;
						break;
					}
				}
			}
			print "\t\t\t\t<td colspan=\"5\" align=\"center\">Imported by <strong>".$importedName."</strong> to <strong>".$festivals_array[$thisFilm->ImportedFestival]."</strong> on <strong>".date("m-d-Y",strtotime($thisFilm->ImportedDate))."</strong>.</td>\n";
		} else {		
			print "\t\t\t\t<td>".$thisFilm->SubmittingName."</td>\n";
			print "\t\t\t\t<td>".$thisFilm->distributorName."</td>\n";
			print "\t\t\t\t<td>".$country_string."</td>\n";
			print "\t\t\t\t<td>".$language_string."</td>\n";
			print "\t\t\t\t<td>".$genre_string."</td>\n";
		}

		print "\t\t\t</tr>\n";

		if ($thisFilm->Imported == 1) {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n"; }
		} else {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		}

		print "\t\t\t\t<td colspan=\"7\"><div id=\"film-info-".$thisFilm->id."\" class=\"hidden\">";
?>
				<fieldset class="ui-corner-all">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
                	<tbody>
                	<tr>
						<td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Technical Data</b></td><td rowspan="12" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="34%"><b>General Information</b></td><td rowspan="7" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Distribution</b></td>
                    </tr><tr>
                        <td><label>Original Title:</label></td><td><?php print $thisFilm->OriginalLanguageTitle; ?></td>
                        <td colspan="2" rowspan="5"><label>Personnel:</label><br />
                        <?php
						$role_array = convert_to_array2($personnelroles); 
						$personnel = preg_split("/}/",$thisFilm->Personnel);
						
						print "<ol style=\"margin-left:20px;\">\n";
						foreach ($personnel as $thisPerson) {
							if ($thisPerson != "") {
								$thisPerson = ltrim($thisPerson,","); $thisPerson = ltrim($thisPerson,"{");
								$fields = preg_split("/\|/",$thisPerson);
								$firstname = $fields[0]; $lastname = $fields[1]; $roles = preg_split("/,/",$fields[2]); $lnf = $fields[3];
								$roles_string = "";
								foreach ($roles as $thisRole) {
									if ($thisRole != 0 && $thisRole != "") {
										$roles_string .= $role_array[$thisRole].", ";
									}
								} $roles_string = trim($roles_string,", ");
							
								print "<li><u>".$firstname." ".$lastname."</u> (".$roles_string.")</li>";
							}
						}
						print "</ol>\n";
                        
						?>
                       	</td>
                        <td><label>Distributor Name:</label></td><td colspan="1"><?php print $thisFilm->distributorName; ?></td>
                    </tr><tr>
                        <td><label>Languages:</label></td><td><?php print $language_string; ?></td>
                        <td><label>Distributor Contact:</label></td><td colspan="1"><?php print $thisFilm->distributorType; ?></td>
                    </tr><tr>
                        <td><label>Countries:</label></td><td><?php print $country_string; ?></td>
                        <td><label>Address:</label></td><td colspan="1"><?php print $thisFilm->distributorAddress; ?></td>
                    </tr><tr>
                        <td><label>Genres:</label></td><td><?php print $genre_string; ?></td>
                        <td><label>Phone:</label></td><td colspan="1"><?php print $thisFilm->distributorPhone; ?></td>
                    </tr><tr>
                        <td><label>Run Time:</label></td><td><?php print $thisFilm->RunTime; ?> mins</td>
                        <td><label>Fax:</label></td><td colspan="1"><?php print $thisFilm->distributorFax; ?></td>
                    </tr><tr>
                        <td><label>Year Completed:</label></td><td><?php print $thisFilm->Year; ?></td>
                        <td><label>Previous<br />Screenings:</label></td><td><?php print $thisFilm->PreviousScreenings; ?></td>
                        <td><label>Email:</label></td><td colspan="1"><?php $thisFilm->distributorEmail != "" ? print "<a href=\"mailto:".$thisFilm->distributorEmail."\">".$thisFilm->distributorEmail."</a>" : print ""; ?></td>
                    </tr><tr>
                        <td><label>Premiere Status:</label></td><td><?php $thisFilm->PremiereStatus != 0 ? print $premiere_array[$thisFilm->PremiereStatus] : print "Not Specified"; ?></td>
                        <td><label>Website:</label></td><td colspan="4"><a href="<?php print $thisFilm->Website; ?>">[link]</a></td>
                    </tr><tr>
                        <td><label>Aspect Ratio:</label></td><td><?php $thisFilm->AspectRatio != 0 ? print $aspectratio_array[$thisFilm->AspectRatio] : print "Not Specified"; ?></td>
                        <td colspan="5" rowspan="5"><label>Synopsis:</label><br /><?php print $thisFilm->Synopsis; ?></td>
                    </tr><tr>
                        <td><label>Sound:</label></td><td><?php $thisFilm->Sound != 0 ? print $soundformat_array[$thisFilm->Sound] : print "Not Specified"; ?></td>
                    </tr><tr>
                        <td><label>Color:</label></td><td><?php $thisFilm->Color != 0 ? print $color_array[$thisFilm->Color] : print "Not Specified"; ?></td>
                    </tr><tr>
                        <td><label>Exhibition Format:</label></td><td><?php $thisFilm->ExhibitionFormat != 0 ? print $videoformat_array[$thisFilm->ExhibitionFormat] : print "Not Specified"; ?></td>
                    </tr><tr>
                        <td><label>Total Footage (Film Only):</label></td><td><?php print $thisFilm->FootageLength; ?><br /><?php print $thisFilm->FootageMeasure; ?></td>
                    </tr>
                	</tbody>
                </table>
                </fieldset>
                
				<fieldset class="ui-corner-all">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                	<tbody>
                	<tr>
                        <td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Inbound Shipping</b></td><td rowspan="7" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="34%"><b>Outbound Shipping</b></td><td rowspan="12" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Press &amp; Publicity</b></td>
                    </tr><tr>
                        <td width="12%"><label>Shipper Name:</label></td><td><?php print $thisFilm->Shipping_In_Name; ?></td>
                        <td width="12%"><label>Shipper Name:</label></td><td><?php print $thisFilm->Shipping_Out_Name; ?></td>
                        <td><label>Contact Name:</label></td><td><?php print $thisFilm->PressContact; ?></td>
                    </tr><tr>
                        <td><label>Address:</label></td><td><?php print $thisFilm->Shipping_In_Address; ?></td>
                        <td><label>Address:</label></td><td><?php print $thisFilm->Shipping_Out_Address; ?></td>
                        <td><label>Phone:</label></td><td><?php print $thisFilm->PressPhone; ?></td>
                    </tr><tr>
                        <td><label>Date of Arrival:</label></td><td><?php if ($thisFilm->Shipping_In_Date != "0000-00-00") { print date("m-d-Y",strtotime($thisFilm->Shipping_In_Date)); } else { print "Not Specified"; } ?></td>
                        <td><label>Date of Arrival:</label></td><td><?php if ($thisFilm->Shipping_Out_Date != "0000-00-00") { print date("m-d-Y",strtotime($thisFilm->Shipping_Out_Date)); } else { print "Not Specified"; } ?></td>
                        <td><label>Fax:</label></td><td><?php print $thisFilm->PressFax; ?></td>
                    </tr><tr>
                        <td><label>Phone:</label></td><td><?php print $thisFilm->Shipping_In_Phone; ?></td>
                        <td><label>Phone:</label></td><td><?php print $thisFilm->Shipping_Out_Phone; ?></td>
                        <td><label>Email:</label></td><td><?php $thisFilm->PressEmail != "" ? print "<a href=\"mailto:".$thisFilm->PressEmail."\">".$thisFilm->PressEmail."</a>" : print ""; ?></td>
                    </tr><tr>
                        <td><label>Fax:</label></td><td><?php print $thisFilm->Shipping_In_Fax; ?></td>
                        <td><label>Fax:</label></td><td><?php print $thisFilm->Shipping_Out_Fax; ?></td>
                        <td><label>Website:</label></td><td><a href="<?php print $thisFilm->PressWebsite; ?>">[link]</a></td>
                    </tr><tr>
                        <td><label>Email:</label></td><td><?php $thisFilm->Shipping_In_Email != "" ? print "<a href=\"mailto:".$thisFilm->Shipping_In_Email."\">".$thisFilm->Shipping_In_Email."</a>" : print ""; ?></td>
                        <td><label>Email:</label></td><td><?php $thisFilm->Shipping_Out_Email != "" ? print "<a href=\"mailto:".$thisFilm->Shipping_Out_Email."\">".$thisFilm->Shipping_Out_Email."</a>" : print ""; ?></td>
                        <td colspan="2"><label>Local Connections &amp; Filming:</label><br /><?php print $thisFilm->HawaiiConnections; ?></td>
                    </tr><tr>
                        <td colspan="5">&nbsp;</td>
                        <td colspan="2"></td>
                    </tr>
                	</tbody>
                </table>
                </fieldset>
<?php
		print "</div></td>\n";
		print "\t\t\t</tr>\n";
		$x++;
	}
?>
			</tbody>
		</table>
    </fieldset>

<div id="filmImport-dialog" title="Import Solicited Film">
<form name="filmImportForm" id="filmImportForm">
<input type="hidden" name="film-import-id" id="film-import-id" value="0" />
<input type="hidden" name="festival-import-id" id="festival-import-id" value="<?php print $festival[0]->id; ?>" />
<input type="hidden" name="user-import-id" id="user-import-id" value="<?php print $current_user[0]->id; ?>" />

	<p>You're trying to import <span id="film_name">[film name]</span> to the <b><?php print $festival[0]->year." ".$festival[0]->name; ?></b>.</p>
    <table class="ImportDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td><div style="float:right; margin-right:15px;"><span style="font-size:11px;">Uppercase?</span> <input type="checkbox" name="uppercase" id="uppercase" value="" /></div>
            	<label for="title_en">English Title:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'title_en', 'id'=>'title_en', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="section-import">Choose a Section:</label> <span class="req">*</span><br />
                <?php print form_dropdown('section-import', $sections_array, 0,"id='section-import' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
        </tr>
        <tr valign="top">
            <td>&nbsp;
            	
            </td>
            <td>
				<label for="eventtype-import">Choose an Event Type:</label> <span class="req">*</span><br />
                <?php print form_dropdown('eventtype-import', $events_array, 0,"id='eventtype-import' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<script type="text/javascript">
$(function() {
	<!-- Whole page -->
	$("#tabs").tabs();
	$("input:submit").button();
	$("button").button();
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

	$("#exportFilmReview").on('click', function() { $("#film-review-export").submit(); $(this).removeClass("ui-state-focus"); });

	$('#uppercase').on('click', function() {
		var title = $("#title_en"); if( $(this).is(':checked') ) { $(this).val(title.val()); title.val(title.val().toUpperCase()); } else { title.val($(this).val()); }
	});

	// validate film import form on keyup and submit
	$("#filmImportForm").validate({
		rules: { "title_en": "required", "section-import": { required:true, min: 1 }, "eventtype-import": { required:true, min: 1 } },
		messages: { "title_en": "Please enter an english title.", "section-import": "Please select a section.", "eventtype-import": "Please select an event type." }
	});

	$("#filmImport-dialog").dialog({
		autoOpen: false,
		height: 430,
		width: 600,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var decision = confirm("Are you sure you really want to delete this Submitted Film? This cannot be undone.");
				if (decision == true) {
					var toUpdate = $('#film-import-id').val();

					$.ajax({
						success: function(msg, text, xhr) {
							var button = "#film-review-"+toUpdate;
							$(button).closest("tr").next("tr").remove();
							$(button).closest("tr").remove();
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#filmImportForm').serialize(),
						url: '/admin/film_review/delete_film/',
						type: 'POST',
						dataType: 'html'
					}); 

					$(this).dialog('close');
				}
			},
<?php } ?>
			'Import Film': function() {
				if ($("#filmImportForm").validate().form() == true) {
					var toUpdate = $('#film-import-id').val();

					$.ajax({
						success: function(msg, text, xhr) {
							$('#film-review-'+toUpdate).unbind('click');
							$('#film-view-'+toUpdate).unbind('click');
							
							$('#film-review-'+toUpdate).closest('tr').replaceWith(msg);

							if ( $('#film-review-'+toUpdate).closest('tr').hasClass('oddrow') == true || $('#film-review-'+toUpdate).closest('tr').hasClass('oddrow-imported') == true ) {
								$('#film-review-'+toUpdate).closest('tr').next('tr').removeClass('oddrow');
								$('#film-review-'+toUpdate).closest('tr').next('tr').addClass('oddrow-imported');
							} else if ( $('#film-review-'+toUpdate).closest('tr').hasClass('evenrow') == true || $('#film-review-'+toUpdate).closest('tr').hasClass('evenrow-imported') == true ) {
								$('#film-review-'+toUpdate).closest('tr').next('tr').removeClass('evenrow');
								$('#film-review-'+toUpdate).closest('tr').next('tr').addClass('evenrow-imported');
							}
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#filmImportForm').serialize(),
						url: '/admin/film_review/import_film/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
	            }
			}
		},
		close: function() {
			$("#filmImportForm INPUT[type='text']").each(function() { $(this).val(""); });
			$("#filmImportForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
			$("#filmImportForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
			$('#film-import-id').val(0);
		}
	});

	$('.FilmDataReviewTab').on('click','button.import', function() {
		var film_review_id = $(this).data('id');
		open_film_import_dialog(film_review_id, $(this).val() );
		$(this).removeClass("ui-state-focus");
		return false;
	});
	$('.FilmDataReviewTab').on('click','button.view', function() {
		var film_review_id = $(this).data('id');
		show_film_detail(film_review_id);
		$(this).removeClass("ui-state-focus");
		return false;
	});

	jQuery(".FilmDataReviewTab").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
	$("#tabs").tabs({
	   show: function(event, ui) {
	     $(".floatHeader1").css('display','none');
	   }
	});
	$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });
});
</script>