<?php
	print "<div>\n";
	print "<button id=\"add-guest\">Add Guest</button><h2>".$festival[0]->year." ".$festival[0]->name." Guest Listing (".date("n/d",strtotime($festival[0]->startdate))." - ".date("n/d/Y",strtotime($festival[0]->enddate)).")</h2></div><br clear=\"all\">";
	print "<fieldset class=\"ui-corner-all\" style=\"padding:0px; background-color:#FFFFFF;\">";
	print "<table id=\"guest_listing_table\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">\n";
	print "\t<thead>\n";	
	print "\t<tr>\n";
	print "\t\t<th width=\"50\" style=\"text-align:center;\">(".count($guests).")</th>\n";
	print "\t\t<th width=\"130\">Guest Name <span><a href=\"/admin/guest_listing/sort/nameasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Guest Name\" /></a> <a href=\"/admin/guest_listing/sort/namedesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Guest Name\" /></a></span></th>\n";
	print "\t\t<th width=\"120\">Guest Type <span><a href=\"/admin/guest_listing/sort/typeasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Guest Type\" /></a> <a href=\"/admin/guest_listing/sort/typedesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Guest Type\" /></a></span></th>\n";
	print "\t\t<th width=\"180\">Program/Film Title <span><a href=\"/admin/guest_listing/sort/programasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Program/Film Title\" /></a> <a href=\"/admin/guest_listing/sort/programdesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Program/Film Title\" /></a></span></th>\n";
	print "\t\t<th width=\"140\">Company <span><a href=\"/admin/guest_listing/sort/affiliationasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Company\" /></a> <a href=\"/admin/guest_listing/sort/affiliationdesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Company\" /></a></span></th>\n";
	print "\t\t<th width=\"140\">Contact Info</th>\n";
	print "\t\t<th width=\"120\">Arrival Date <span><a href=\"/admin/guest_listing/sort/arrivalasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Arrival Date\" /></a> <a href=\"/admin/guest_listing/sort/arrivaldesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Arrival Date\" /></a></span></th>\n";
	print "\t\t<th width=\"140\">Departure Date <span><a href=\"/admin/guest_listing/sort/departureasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Departure Date\" /></a> <a href=\"/admin/guest_listing/sort/departuredesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Departure Date\" /></a></span></th>\n";
	print "\t\t<th width=\"55\">Bag Pickup</th>\n";
	print "\t\t<th width=\"55\">Badge Pickup</th>\n";
	print "\t</tr>\n";
	print "\t</thead>\n\n";
	print "\t<tbody>\n";

	$x = 1;
	if (count($guests) == 0) {
		print "\t<tr valign=\"top\" class=\"oddrow\">\n";
		print "\t\t<td colspan=\"12\" align=\"center\">There are no guests listed for this festival. Would you like to add one?</td>\n";
		print "\t</tr>\n";
	} else {
		foreach ($guests as $thisGuest) {			
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		
			print "\t<td><div class=\"button ui-state-default\"><a href=\"/admin/guest_edit/update/".$thisGuest->slug."\">Edit</a></div></td>\n";
			print "\t<td>".$thisGuest->Lastname.", ".$thisGuest->Firstname." ".$thisGuest->MI."</td>\n";
			print "\t<td>".$thisGuest->GuestType."</td>\n";

			// Program & Film Titles
			print "\t<td>";
			if ($thisGuest->Program_Title != "") {
				print "<strong style=\"color:black;\">".$thisGuest->Program_Title."</strong><br />";
			}
			if ($thisGuest->Film_Title != "") {
				foreach ($guest_films as $thisGuestFilm) {
					if ($thisGuest->guest_id == $thisGuestFilm->festivalguest_id) {
						print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/film.png\" alt=\"Film\" title=\"Film\"> <a href=\"/admin/film_edit/update/".$thisGuestFilm->slug."\">".switch_title($thisGuestFilm->title_en)."</a><br />";
					}
				}
			}
			print "</td>\n";
			
			print "\t<td>";
			if ($thisGuest->AffiliationOptional != "") { print $thisGuest->AffiliationOptional."<br /><br />"; }
			print "</td>\n";

			// Contact Info
			print "\t<td>";
			if ($thisGuest->Phone != "") {
				print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/telephone.png\" alt=\"Primary Phone\" title=\"Primary Phone\"> ".$thisGuest->Phone."<br />";
			}
			if ($thisGuest->Cell != "") {
				print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/phone.png\" alt=\"Mobile Phone\" title=\"Mobile Phone\"> ".$thisGuest->Cell."<br />";
			}
			if ($thisGuest->Fax != "") {
				print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/printer.png\" alt=\"Fax\" title=\"Fax\"> ".$thisGuest->Fax."<br />";
			}
			if ($thisGuest->Email != "") {
				print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/email.png\" alt=\"Email\" title=\"Email\"> <a href=\"mailto:".$thisGuest->Email."\" target=\"_blank\">".$thisGuest->Email."</a><br />";
			}
			if ($thisGuest->TwitterName != "") {
				print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/twitter.png\" alt=\"Twitter\" title=\"Twitter\"> <a href=\"http://twitter.com/".$thisGuest->TwitterName."\" target=\"_blank\">@".$thisGuest->TwitterName."</a><br />";
			}
			print "</td>\n";
			
			// Arriving Flights/Hotels
			print "\t<td>";
				if ($thisGuest->ArrivalDate != "") { print $thisGuest->ArrivalDate; }
				if ($thisGuest->checkin != "") { print $thisGuest->checkin; }
			print "</td>\n";

			// Departing Flights/Hotels
			print "\t<td>";
				if ($thisGuest->DepartureDate != "") { print $thisGuest->DepartureDate; }
				if ($thisGuest->checkout != "") { print $thisGuest->checkout; }
			print "</td>\n";
			
			// Bag / Badge picked up
			if ($thisGuest->gb_pickedup != "0") {
				print "\t<td class=\"cent\"><a id=\"gb_pickedup-".$thisGuest->guest_id."\" href=\"#\" data-id=\"".$thisGuest->guest_id."\" data-type=\"gb_pickedup\" class=\"icon_tick\"></a></td>\n";
			} else {
				print "\t<td class=\"cent\"><a id=\"gb_pickedup-".$thisGuest->guest_id."\" href=\"#\" data-id=\"".$thisGuest->guest_id."\" data-type=\"gb_pickedup\" class=\"icon_cross\"></a></td>\n";
			}
			if ($thisGuest->badge_pickedup != "0") {
				print "\t<td class=\"cent\"><a id=\"badge_pickedup-".$thisGuest->guest_id."\" href=\"#\" data-id=\"".$thisGuest->guest_id."\" data-type=\"badge_pickedup\" class=\"icon_tick\"></a></td>\n";
			} else {
				print "\t<td class=\"cent\"><a id=\"badge_pickedup-".$thisGuest->guest_id."\" href=\"#\" data-id=\"".$thisGuest->guest_id."\" data-type=\"badge_pickedup\" class=\"icon_cross\"></a></td>\n";
			}
			print "\t</tr>\n";
		
			$x++;
		}
	}
	print "\t</tbody>\n";
	print "</table>\n";
	print "</fieldset>\n";
	
	print "<button id=\"add-guest2\">Add Guest</button><br /><br />";
?>

<div class="GuestDialog" title="Add A Guest">
	<form name="addGuestForm" id="addGuestForm">
    <?php print form_hidden("festival_id",$festival[0]->id); ?>
    <table width="100%">
    	<tbody style="border-top:none;">
    	<tr valign="top">
        	<td width="50%"><label for="Firstname">First Name</label><span class="req"> *</span><br /><input type="text" name="Firstname" id="Firstname" class="text ui-widget-content ui-corner-all" /></td>
            <td width="50%"><label for="Lastname">Last Name</label><span class="req"> *</span><br /><input type="text" name="Lastname" id="Lastname" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
        <tr valign="top">
            <td><label for="GuestType">Guest Type</label><span class="req"> *</span><br /><select name="GuestType" id="GuestType" class="select ui-widget-content ui-corner-all"><option value="0">Select a Guest Type</option>
<?php foreach ($guesttypes as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
            <td><label for="AffiliationOptional">Company / Film Affiliation</label><br />
            <input type="text" name="AffiliationOptional" id="AffiliationOptional" class="text ui-widget-content ui-corner-all" /><br />
            
            <select name="FilmAffiliation" id="FilmAffiliation" class="select ui-widget-content ui-corner-all"><option value="0">Select a Film</option>
<?php foreach ($films as $rec) {	print "\t\t<option value=\"".$rec->movie_id."\">".switch_title($rec->title_en)."</option>\n"; } ?>
            </select></td>
        </tr>
        </tbody>
    </table>    
	</form>
</div>

<script type="text/javascript">
	$(function() {
		$("input:submit").button();
		$("button").button();				

		$("#addGuestForm").validate({
			rules: { Firstname: "required", Lastname: "required", GuestType: { required: true, min:1 } },
			messages: { Firstname: "Please enter a first name.", Lastname: "Please enter a last name.", GuestType: "Please select a guest type." }
		});
		$(".GuestDialog").dialog({
			autoOpen: false,
			height: 430,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() { $(this).dialog('close'); },
				'Add Guest': function() {
					if ($('#addGuestForm').validate().form() == true) {
                		$.ajax({
							success: function(msg) {
								window.location="/admin/guest_edit/update/"+msg; },
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addGuestForm').serialize(),
							url: '/admin/guest_listing/add/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
		            }
				}
			}
		});
		$('#add-guest, #add-guest2').each(function() {
			$(this).on('click', function() {
				$('.GuestDialog').dialog('open');
				$(this).removeClass("ui-state-focus");
			});
		});

		$('#guest_listing_table').on('click','a.icon_cross', function(e) {
			e.preventDefault();
			toggle_value_guest($(this).data('type').toString(), $(this).data('id'));
		});
		$('#guest_listing_table').on('click','a.icon_tick', function(e) {
			e.preventDefault();
			toggle_value_guest($(this).data('type').toString(), $(this).data('id'));
		});

		jQuery("#guest_listing_table").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
	});
</script>
