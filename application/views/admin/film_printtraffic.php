<?php
	$Inbound_Buttons = "";
	$Outbound_Buttons = "";

	$videoformat_array = convert_to_array2($videoformat);
	$courier_array = convert_to_array($courier);

	$outbound_array = array("0"=>"Choose Shipment Type","1"=>"Inbound (Receiving)","2"=>"Outbound (Shipping)");
	$payment_array = array("1"=>"Yes","0"=>"No");
	$films_array = convert_films_to_array2($films);

?>
<button id="add-shipment-leg1">Add Inbound/Outbound Shipment</button>
<p style="float:right;">Please note that only shipments for films marked as CONFIRMED will be listed on this page.</p>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Outbound Prints<?php if (count($outbound) > 0) { print " (".count($outbound).")"; } ?></a></li>
		<li><a href="#tabs-2">Inbound Prints<?php if (count($inbound) > 0) { print " (".count($inbound).")"; } ?></a></li>
	</ul>

	<div id="tabs-1">
	<fieldset class="ui-corner-all">
		<table class="FilmShippingHeader" width="100%" cellpadding="0" cellspacing="0" border="0">
			<thead>
			<tr valign="top">
				<th width="70" align="center"><form id="pt-shipments-outbound" method="post" action="/admin/film_reports/preview_shipments/outbound/download/"><button id="outboundShipments">Export</button></form></th>
				<th width="150">
					Film Title <span><a href="/admin/film_printtraffic/sort/shipping/nameasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by English Title" /></a> <a href="/admin/film_printtraffic/sort/shipping/namedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Film Title" /></a></span>
					<br />Video Format <span><a href="/admin/film_printtraffic/sort/shipping/formatasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Video Format" /></a> <a href="/admin/film_printtraffic/sort/shipping/formatdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Video Format" /></a></span>
				</th>
				<th width="150">
					Event Type <span><a href="/admin/film_printtraffic/sort/shipping/eventasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Event Type" /></a> <a href="/admin/film_printtraffic/sort/shipping/eventdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Event Type" /></a></span>
					<br />Shipped Date <span><a href="/admin/film_printtraffic/sort/shipping/shippedasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Shipped Date" /></a> <a href="/admin/film_printtraffic/sort/shipping/shippeddesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Shipped Date" /></a></span>
				</th>
				<th width="150">
					Courier <span><a href="/admin/film_printtraffic/sort/shipping/courierasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Courier" /></a> <a href="/admin/film_printtraffic/sort/shipping/courierdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Courier" /></a></span>
					<br />Arrive By Date <span><a href="/admin/film_printtraffic/sort/shipping/arriveasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Arrive By Date" /></a> <a href="/admin/film_printtraffic/sort/shipping/arrivedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Arrive By Date" /></a></span>
				</th>
				<th width="150">
					Tracking #<br />Account #
				</th>
				<th width="120">Shipping Fee <span><a href="/admin/film_printtraffic/sort/shipping/feeasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Shipping Fee" /></a> <a href="/admin/film_printtraffic/sort/shipping/feedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Shipping Fee" /></a></span></th>
				<th width="175">Notes</th>
			</tr>            
			</thead>
			<tbody>
				<tr>
					<td colspan="7" style="padding: 0px;">

					<div id="Outbound-ReplaceMe"><input type="hidden" name="new-outbound-id" id="new-outbound-id" value="0" /></div>

<?php
	if (count($outbound) == 0) {
		print "\t\t<div align=\"center\">There are no outbound shipments listed for this festival.<br />Films must be marked as 'Confirmed' on the Film Listing page before their shipments will show on this page.<br />One inbound and outbound shipment is automatically created when you add a film.</div>\n";
	} else {
		$x = 1;
		foreach ($outbound as $thisShipment) {
			$tracking_url = ""; $tracking_link = 0;
			$plain_id = $thisShipment->id; $id = "-".$plain_id;

			if ($thisShipment->TrackingNum != "" && ($courier_array[$thisShipment->courier_id] == "FedEx" || $courier_array[$thisShipment->courier_id] == "DHL" || $courier_array[$thisShipment->courier_id] == "UPS" || $courier_array[$thisShipment->courier_id] == "USPS")) {
				$tracking_url = get_tracking_url($courier_array[$thisShipment->courier_id],$thisShipment->TrackingNum);
				$tracking_link = 1;
			}
	
			print "\t\t<form name=\"filmShippingForm".$id."\" id=\"filmShippingForm".$id."\">\n";
			print "\t\t<input type=\"hidden\" name=\"film-name".$id."\" id=\"film-name".$id."\" value=\"".switch_title($thisShipment->title_en)."\" />\n";
			print "\t\t<input type=\"hidden\" name=\"movie_id".$id."\" id=\"movie_id".$id."\" value=\"".$thisShipment->movie_id."\" />\n";
			print "\t\t<table class=\"FilmShippingTab\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t\t\t<tbody>\n";
	
			// First Row
			if ($thisShipment->confirmed == 1) {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-shipped\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-shipped\">\n"; }
			} else {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
			}
			print "\t\t\t\t<td width=\"70\" rowspan=\"2\" align=\"center\"><div class=\"button ui-state-default ui-corner-all\"><a href=\"/admin/film_edit/update/".$thisShipment->slug."/#tabs-6\">Update</a></div>";
			if ($thisShipment->confirmed == 0) {
				print "<button id=\"shipping-ship".$id."\" data-id=\"".$plain_id."\" class=\"ship\">Ship</button>";
			} else {
				print "<button id=\"shipping-ship".$id."\" data-id=\"".$plain_id."\" class=\"unship\">Un-Ship</button>";				
			}
			print "</td>\n";
			print "\t\t\t\t<td width=\"150\"><strong>".switch_title($thisShipment->title_en)."</strong></td>\n";
			print "\t\t\t\t<td width=\"150\">".$thisShipment->EventType."</td>\n";
			print "\t\t\t\t<td width=\"150\">".$courier_array[$thisShipment->courier_id]."</td>\n";
			if ($tracking_link == 1) {
				print "\t\t\t\t<td width=\"150\"><a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/icons/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"> ".$thisShipment->TrackingNum."</a></td>\n";
			} else {
				print "\t\t\t\t<td width=\"150\">".$thisShipment->TrackingNum."</td>\n";
			}
			print "\t\t\t\t<td width=\"120\">$".$thisShipment->ShippingFee."</td>\n";
			print "\t\t\t\t<td width=\"175\" rowspan=\"2\">".$thisShipment->notes."</td>\n";
			print "\t\t\t</tr>\n";
	
			// Second Row
			if ($thisShipment->confirmed == 1) {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow2-shipped\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow2-shipped\">\n"; }
			} else {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow2\">\n"; }
			}
			if ($thisShipment->format_id != 0) {
				print "\t\t\t\t<td><strong>".$videoformat_array[$thisShipment->format_id]."</strong></td>\n";
			} else {
				print "\t\t\t\t<td></td>\n";
			}
			$thisShipment->OutShipped == "0000-00-00" ? print "\t\t\t\t<td><span class=\"date\">&nbsp;</span></td>\n" : print "\t\t\t\t<td><span class=\"date\">".date("m-d-Y",strtotime($thisShipment->OutShipped))."</span></td>\n";
			$thisShipment->OutArriveBy == "0000-00-00" ? print "\t\t\t\t<td><span class=\"date\">&nbsp;</span></td>\n" : print "\t\t\t\t<td><span class=\"date\">".date("m-d-Y",strtotime($thisShipment->OutArriveBy))."</span></td>\n";
			print "\t\t\t\t<td>".$thisShipment->AccountNum."</td>\n";
			print "\t\t\t\t<td><label>We Pay:</label> ";
			if (intval($thisShipment->wePay) == 1) {
				print "Yes";
			} else {
				print "No";
			}
			print "</td>\n";
			print "\t\t\t</tr>\n";
	
			print "\t\t\t</tbody>\n";
			print "\t\t</table>\n";
			print "\t\t</form>\n";
	
			if ($thisShipment->confirmed == 0) {
				$Outbound_Buttons .= "\t\t$('#shipping-ship".$id."').on('click', function() { outbound_shipped(".$plain_id."); return false; } );\n";
			}
	
	
			$x++;		
		}
	}
?>
					</td>
				</tr>
			</tbody>
	   </table>
	</fieldset>
	</div>

	<div id="tabs-2">
	<fieldset class="ui-corner-all">
		<table class="FilmReceivingHeader" width="100%" cellpadding="0" cellspacing="0" border="0">
			<thead>
			<tr valign="top">
				<th width="70" align="center"><form id="pt-shipments-inbound" method="post" action="/admin/film_reports/preview_shipments/inbound/download/"><button id="inboundShipments">Export</button></form></th>
				<th width="150">
					Film Title <span><a href="/admin/film_printtraffic/sort/receiving/nameasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Film Title" /></a> <a href="/admin/film_printtraffic/sort/receiving/namedesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Film Title" /></a></span>
					<br />Video Format <span><a href="/admin/film_printtraffic/sort/receiving/formatasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Video Format" /></a> <a href="/admin/film_printtraffic/sort/receiving/formatdesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Video Format" /></a></span>
				</th>
				<th width="150">
					Event Type <span><a href="/admin/film_printtraffic/sort/receiving/eventasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Event Type" /></a> <a href="/admin/film_printtraffic/sort/receiving/eventdesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Event Type" /></a></span>
					<br />Received Date <span><a href="/admin/film_printtraffic/sort/receiving/receiveasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Received Date" /></a> <a href="/admin/film_printtraffic/sort/receiving/receivedesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Received Date" /></a></span>
				</th>
				<th width="150">
					Courier <span><a href="/admin/film_printtraffic/sort/receiving/courierasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Courier" /></a> <a href="/admin/film_printtraffic/sort/receiving/courierdesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Courier" /></a></span>
					<br />Expected Arrival <span><a href="/admin/film_printtraffic/sort/receiving/arrivalasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Expected Arrival" /></a> <a href="/admin/film_printtraffic/sort/receiving/arrivaldesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Expected Arrival" /></a></span>
				</th>
				<th width="150">
					Tracking #<br />Account #
				</th>
				<th width="120">Shipping Fee <span><a href="/admin/film_printtraffic/sort/receiving/feeasc/#tabs-2"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Shipping Fee" /></a> <a href="/admin/film_printtraffic/sort/receiving/feedesc/#tabs-2"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Shipping Fee" /></a></span></th>
				<th width="175">Notes</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7" style="padding: 0px;">
					
					<div id="Inbound-ReplaceMe"><input type="hidden" name="new-inbound-id" id="new-inbound-id" value="0" /></div>

<?php
	if (count($inbound) == 0) {
		print "\t\t<div align=\"center\">There are no inbound shipments listed for this festival.<br />Films must be marked as 'Confirmed' on the Film Listing page before their shipments will show on this page.<br />One inbound and outbound shipment is automatically created when you add a film.</div>\n";
	} else {
		$x = 1;
		foreach ($inbound as $thisShipment) {
			$tracking_url = ""; $tracking_link = 0;
			$plain_id = $thisShipment->id; $id = "-".$plain_id;

			if ($thisShipment->TrackingNum != "" && ($courier_array[$thisShipment->courier_id] == "FedEx" || $courier_array[$thisShipment->courier_id] == "DHL" || $courier_array[$thisShipment->courier_id] == "UPS" || $courier_array[$thisShipment->courier_id] == "USPS")) {
				$tracking_url = get_tracking_url($courier_array[$thisShipment->courier_id],$thisShipment->TrackingNum);
				$tracking_link = 1;
			}
	
			print "\t\t<form name=\"filmReceivingForm".$id."\" id=\"filmReceivingForm".$id."\">\n";
			print "\t\t<input type=\"hidden\" name=\"film-name".$id."\" id=\"film-name".$id."\" value=\"".switch_title($thisShipment->title_en)."\" />\n";
			print "\t\t<input type=\"hidden\" name=\"movie_id".$id."\" id=\"movie_id".$id."\" value=\"".$thisShipment->movie_id."\" />\n";
			print "\t\t<table class=\"FilmReceivingTab\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t\t\t<tbody>\n";
	
			// First Row
			if ($thisShipment->confirmed == 1) {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-received\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-received\">\n"; }
			} else {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
			}	
			print "\t\t\t\t<td width=\"70\" rowspan=\"2\" align=\"center\"><div class=\"button ui-state-default ui-corner-all\"><a href=\"/admin/film_edit/update/".$thisShipment->slug."/#tabs-6\">Update</a></div>";
			if ($thisShipment->confirmed == 0) {
				print "<button id=\"receiving-receive".$id."\" data-id=\"".$plain_id."\" class=\"receive\">Receive</button>";
			} else {
				print "<button id=\"receiving-receive".$id."\" data-id=\"".$plain_id."\" class=\"unreceive\">Un-Receive</button>";				
			}
			print "</td>\n";
			print "\t\t\t\t<td width=\"150\"><strong>".switch_title($thisShipment->title_en)."</strong></td>\n";
			print "\t\t\t\t<td width=\"150\">".$thisShipment->EventType."</td>\n";
			print "\t\t\t\t<td width=\"150\">".$courier_array[$thisShipment->courier_id]."</td>\n";
			if ($tracking_link == 1) {
				print "\t\t\t\t<td width=\"150\"><a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/icons/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"> ".$thisShipment->TrackingNum."</a></td>\n";
			} else {
				print "\t\t\t\t<td width=\"150\">".$thisShipment->TrackingNum."</td>\n";
			}
			print "\t\t\t\t<td width=\"120\">$".$thisShipment->ShippingFee."</td>\n";
			print "\t\t\t\t<td width=\"175\" rowspan=\"2\">".$thisShipment->notes."</td>\n";
			print "\t\t\t</tr>\n";
	
			// Second Row
			if ($thisShipment->confirmed == 1) {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow2-received\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow2-received\">\n"; }
			} else {
				if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n"; }
				if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow2\">\n"; }
			}

			if ($thisShipment->format_id != 0) {
				print "\t\t\t\t<td><strong>".$videoformat_array[$thisShipment->format_id]."</strong></td>\n";
			} else {
				print "\t\t\t\t<td></td>\n";				
			}
			$thisShipment->InReceived == "0000-00-00" ? print "\t\t\t\t<td><span class=\"date\">&nbsp;</span></td>\n" : print "\t\t\t\t<td><span class=\"date\">".date("m-d-Y",strtotime($thisShipment->InReceived))."</span></td>\n";
			$thisShipment->InExpectedArrival == "0000-00-00" ? print "\t\t\t\t<td><span class=\"date\">&nbsp;</span></td>\n" : print "\t\t\t\t<td><span class=\"date\">".date("m-d-Y",strtotime($thisShipment->InExpectedArrival))."</span></td>\n";
			print "\t\t\t\t<td>".$thisShipment->AccountNum."</td>\n";
			print "\t\t\t\t<td><label>We Pay:</label> ";
			if (intval($thisShipment->wePay) == 1) {
				print "Yes";
			} else {
				print "No";
			}
			print "</td>\n";
			print "\t\t\t</tr>\n";
	
			print "\t\t\t</tbody>\n";
			print "\t\t</table>\n";
			print "\t\t</form>\n";
	
			if ($thisShipment->confirmed == 0) {
				$Inbound_Buttons .= "\t\t$('#receiving-receive".$id."').on('click', function() { inbound_received(".$plain_id."); return false; } );\n";
			}
	
			$x++;		
		}
	}
?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	</div>
</div>
<button id="add-shipment-leg2">Add Inbound/Outbound Shipment</button>

<div id="ptAdd-dialog" title="Add Inbound/Outbound Shipment Leg">
<form name="ptAddForm" id="ptAddForm">
	<table class="ptAddDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="33%" colspan="4">
				<label for="festivalmovie_id-new">Film:</label> <span class="req">*</span><br />
				<?php print form_dropdown('festivalmovie_id-new', $films_array, 0,"id='festivalmovie_id-new' class='select ui-widget-content ui-corner-all'"); ?>

			</td>
			<td width="34%" colspan="4">
				<label for="outbound-new">Inbound or Outbound?:</label> <span class="req">*</span><br />
				<?php print form_dropdown('outbound-new', $outbound_array, 0,"id='outbound-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="33%" colspan="4">
				<label for="ShippingFee-new">Shipping Fee:</label><br />
				$<?php print form_input(array('name'=>'ShippingFee-new', 'id'=>'ShippingFee-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top" class="InboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InReceived-new">Received Date:</label><br />
				<?php print form_input(array('name'=>'InReceived-new', 'id'=>'InReceived-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InExpectedArrival-new">Expected Arrival Date:</label><br />
				<?php print form_input(array('name'=>'InExpectedArrival-new', 'id'=>'InExpectedArrival-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top" class="OutboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="OutShipped-new">Shipped Date:</label><br />
				<?php print form_input(array('name'=>'OutShipped-new', 'id'=>'OutShipped-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span><label for="OutArriveBy-new">Arrive By Date:</label><br />
				<?php print form_input(array('name'=>'OutArriveBy-new', 'id'=>'OutArriveBy-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="courier_id-new">Courier:</label> <span class="req">*</span><br />
				<?php print form_dropdown('courier_id-new', $courier_array, 0,"id='courier_id-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="4">
				<label for="AccountNum-new">Account Number:</label><br />
				<?php print form_input(array('name'=>'AccountNum-new', 'id'=>'AccountNum-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="4">
				<label for="TrackingNum-new">Tracking Number:</label><br />
				<?php print form_input(array('name'=>'TrackingNum-new', 'id'=>'TrackingNum-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="courier_id-new">We Pay?:</label><br />
				<?php print form_dropdown('wePay-new', $payment_array, 1,"id='wePay-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="8">
				<label for="notes-new">Shipping Notes:</label><br />
				<?php print form_textarea(array('name'=>'notes-new', 'id'=>'notes-new', 'value'=>'', 'rows'=>'4', 'cols'=>'57', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<script type="text/javascript">
$(function() {
	<!-- Whole page -->
	$("#tabs").tabs();
	$("input:submit").button();
	$("button").button();
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

	$("#inboundShipments").on('click', function() { $("#pt-shipment-inbound").submit(); $(this).removeClass("ui-state-focus"); });
	$("#outboundShipments").on('click', function() { $("#pt-shipment-outbound").submit(); $(this).removeClass("ui-state-focus"); });

	$("#ptAddForm").validate({
		rules: { "festivalmovie_id-new": { required:true, min: 1 }, "outbound-new": { required:true, min: 1 }, "courier_id-new": { required:true, min: 1 }, "AccountNum-new": "digits", "TrackingNum-new": "digits" },
		messages: { "festivalmovie_id-new": "Please select a film.", "outbound-new": "Please choose a shipment type.", "courier_id-new": "Please choose a courier.", "AccountNum-new": "Please enter whole numbers only.", "TrackingNum-new": "Please enter whole numbers only." }
	});

	$("#ptAdd-dialog").dialog({
		autoOpen: false,
		height: 480,
		width: 600,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Add Shipment Leg': function() {
				var shipment_type = $('#outbound-new').val();
				if ($("#ptAddForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#ajaxFeedback').html("The shipment leg was added!");
															
							if (shipment_type == 1) {
								$('#Inbound-ReplaceMe').replaceWith(msg);
								$('#tabs').tabs("option", "active", 1);
							}
							if (shipment_type == 2) {
								$('#Outbound-ReplaceMe').replaceWith(msg);
								$('#tabs').tabs("option", "active", 0);
							}
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#ptAddForm').serialize(),
						url: '/admin/film_printtraffic/add_shipment/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		},
		close: function() {
			$("#ptAddForm INPUT[type='text']").each(function() { $(this).val(""); });
			$("#ptAddForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
			$("#ptAddForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
			$("#ptAddForm TEXTAREA").each(function() { $(this).val(""); });
			$(".InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			$(".OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
		}
	});

	$("#outbound-new").on('change', function() {
		if ($("option:selected", this).val() == 1) {
			$(".InboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
			$(".OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
		}
		else if ($("option:selected", this).val() == 2) {
			$(".InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			$(".OutboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");

		} else if ($("option:selected", this).val() == 0) {
			$(".InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			$(".OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
		}		
	});
	$(".InboundRow INPUT").datepicker();
	$(".OutboundRow INPUT").datepicker();

	$('#add-shipment-leg1').button().on('click', function() { $('#ptAdd-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); });
	$('#add-shipment-leg2').button().on('click', function() { $('#ptAdd-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); });
	
	$('#tabs-1').on('click','button.ship', function() {
		outbound_shipped( $(this).data('id') );
		$(this).removeClass("ui-state-focus");
		return false;
	});
	$('#tabs-1').on('click','button.unship', function() {
		outbound_unshipped( $(this).data('id') );
		$(this).removeClass("ui-state-focus");
		return false;
	});

	$('#tabs-2').on('click','button.receive', function() {
		inbound_received( $(this).data('id') );
		$(this).removeClass("ui-state-focus");
		return false;
	});
	$('#tabs-2').on('click','button.unreceive', function() {
		inbound_unreceived( $(this).data('id') );
		$(this).removeClass("ui-state-focus");
		return false;
	});

	jQuery(".FilmShippingHeader").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
	jQuery(".FilmReceivingHeader").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader2'});
	$("#tabs").tabs({
	   show: function(event, ui) { $(".floatHeader1").css('display','none'); $(".floatHeader2").css('display','none'); }
	});
});
</script>