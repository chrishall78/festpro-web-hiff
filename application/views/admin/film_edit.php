<?php
	if ($title == "No Film Specified") { print "<p>Sorry, no film was specified to update. Please click the back button and try again.</p>"; }
	else {
	
	if ($saved_film_title != "none") {
		if ($film_added == "yes") {
			print "<div align=\"center\" style=\"font-weight:bold; color:#CC0000;\">".$saved_film_title." has been successfully added.</div>";
		} else {
			print "<div align=\"center\" style=\"font-weight:bold; color:#CC0000;\">".$saved_film_title." has been successfully updated.</div>";
		}
	}
	$attributes = array('name' => 'UpdateFilm', 'id' => 'UpdateFilm');
	$yes_no = array(0=>'--',1=>'Yes',2=>'No Code');

	// Film/Event Tab
	$films_array = convert_films_to_array($films);
	$videoformat_array = convert_to_array($videoformat);
	$premiere_array = convert_to_array($premiere);
	$eventtypes_array = convert_to_array($eventtypes);
	$soundformat_array = convert_to_array($soundformat);
	$aspectratio_array = convert_to_array($aspectratio);
	$framerate_array = convert_to_array($framerate);
	$sections_array = convert_to_array($sections);
	$countries_array = convert_to_array($countries);
	$languages_array = convert_to_array($languages);
	$genres_array = convert_to_array($genres);
	$sponsors_array = convert_to_array($sponsors);

	// Synopsis Tab
	// Multimedia Tab

	// Screenings Tab
	$film_screening_array = convert_film_screenings_to_array($films);
	$screening_json = json_encode($film_screenings);

	// Print Traffic Tab
	$courier_array = convert_to_array2($pt_courier);
	$print_traffic_json = json_encode($pt_shipments);

	// Press/Print Source Tab
	$distribution_array = convert_to_array($distribution);

	// test for existence of print traffic detail record
	count($pt_details) > 0 ? $print_traffic_id = $pt_details[0]->id : $print_traffic_id = "";
	$hidden = array('festivalmovie_id'=>$film[0]->id,'movie_id'=>$film[0]->movie_id,'printtraffic_id'=>$print_traffic_id,'pt_shipment_ids'=>$pt_shipment_ids,'country_ids'=>$country_ids,'language_ids'=>$language_ids,'genre_ids'=>$genre_ids,'film_slug'=>$film[0]->slug);
	print form_open('admin/film_edit/save', $attributes, $hidden);
?>

<table id="topFilmEdit" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody style="border-top:none;">
	<tr>
		<td width="80"><?php print form_submit(array('name'=>'Save', 'id'=>'Save', 'value'=>'SAVE')); ?></td>
		<td width="280"><div style="float:right; margin-right:25px;"><span style="font-size:11px;">Uppercase?</span> <input type="checkbox" name="uppercase" id="uppercase" value="" /></div><label for="title_en">English Title:</label> <span class="req">*</span> <?php print form_input(array('name'=>'title_en', 'id'=>'title_en', 'value'=>$film[0]->title_en, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		<td width="280"><label for="title">Original/Romanized Title: </label> <?php print form_input(array('name'=>'title', 'id'=>'title', 'value'=>$film[0]->title, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		<td width="360">
			<table cellpadding="0" cellspacing="0" border="0" id="topFilmSettings">
				<thead>
					<tr>
						<th><label for="Generate PDF">Generate PDF</label></th>
						<th><label for="Confirmed">Confirmed</label></th>
						<th><label for="Published">Published</label></th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
					<tr align="center">
						<td width="30%">
							<?php
								if ($film[0]->Confirmed == 1) {
									print "<a class=\"choose_photo\" href=\"/admin/film_listing/generate_pdf/".$film[0]->slug."/english/\" title=\"English\">English</a>";
									if ($film[0]->title != "") {
										print " | <a class=\"choose_photo\" href=\"/admin/film_listing/generate_pdf/".$film[0]->slug."/original/\" title=\"Original Language\">Original</a>\n";
									}
									print "<div class=\"hiddenphoto\">";
									$urls = "";
									foreach ($film_photos as $thisPhoto) { $urls .= $thisPhoto->url_cropsmall."|"; }
									$urls = trim($urls,"|");
									print $urls."</div>";
							   } else {
									print "\tConfirm First\n";
								}
							?>
						</td>
						<td width="25%">
						<?php $film[0]->Confirmed == 1 ? print form_checkbox(array('name'=>'Confirmed', 'id'=>'Confirmed', 'value'=>'1', 'checked'=>true)) : print form_checkbox(array('name'=>'Confirmed', 'id'=>'Confirmed', 'value'=>'1', 'checked'=>false)); ?>
						</td>
						<td width="25%">
						<?php $film[0]->Published == 1 ? print form_checkbox(array('name'=>'Published', 'id'=>'Published', 'value'=>'1', 'checked'=>true)) : print form_checkbox(array('name'=>'Published', 'id'=>'Published', 'value'=>'1', 'checked'=>false)); ?>
						</td>
						<td width="20%">
							<a href="/films/detail/<?php print $film[0]->slug; ?>" target="_blank">Film Preview</a>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Film/Event Info</a></li>
		<li><a href="#tabs-2">Synopsis</a></li>
		<li><a href="#tabs-3">Multimedia</a></li>
		<li><a href="#tabs-4">Screenings</a></li>
		<li><a href="#tabs-5">Press/Print Source</a></li>
		<li><a href="#tabs-6">Print Traffic</a></li>
	</ul>

	<!-- Film/Event Tab -->
	<div id="tabs-1" class="ui-helper-clearfix">


	<div class="field-half ui-corner-all">
	<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="4"><h3>Film/Event Information</h3></td>
		</tr>
		<tr valign="top">
			<td width="25%"><label for="title">Year Completed:</label></td>
			<td width="25%"><?php print form_dropdown('year', $yearcompletion, $film[0]->year,"class='select ui-widget-content ui-corner-all'"); ?></td>
			<td width="25%"><label for="runtime_int">Runtime (min):</label> <span class="req">*</span></td>
			<td width="25%"><?php print form_input(array('name'=>'runtime_int', 'id'=>'runtime_int', 'value'=>$film[0]->runtime_int, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr valign="top">
			<td><label for="eventtype">Event Type:</label></td>
			<td><?php print form_dropdown('eventtype', $eventtypes_array, $film[0]->event_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
			<td><label for="premiere">Premiere Type:</label></td>
			<td><?php print form_dropdown('premiere', $premiere_array, $film[0]->premiere_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
		</tr>
		<tr valign="top">
			<td><label for="websiteurl">Film Website:</label></td>
			<td colspan="3"><?php print form_input(array('name'=>'websiteurl', 'id'=>'websiteurl', 'value'=>$film[0]->websiteurl, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		</tr>
		</tbody>
	</table>

	<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="3" align="center">
				<label for="sections" class="centered_title">Section</label>
				<?php print form_dropdown("sections", $sections_array, $film[0]->category_id," class='select-large ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td width="33%" align="center">
				<label for="countries" class="centered_title">Countries</label>
				<?php
				if (count($film_countries) == 0) {
					print "<span id=\"noCountries\">No Countries Assigned<br /></span>";
				} else {
					print "<span id=\"noCountries\" style=\"display:none;\">No Countries Assigned<br /></span>";
				}
				foreach ($film_countries as $thisCountry) {
					print form_dropdown("countries-".$thisCountry->id, $countries_array, $thisCountry->country_id," class='select ui-widget-content ui-corner-all'")."<br />";
				}
				?>
				<div id="addCountryHere"><input type="hidden" name="addCountryId" id="addCountryId" value="0" /></div>
				<button name="addCountry" id="addCountry">Add Country</button>
			</td>
			<td width="33%" align="center">
				<label for="languages" class="centered_title">Languages</label>
				<?php
				if (count($film_languages) == 0) {
					print "<span id=\"noLanguages\">No Languages Assigned<br /></span>";
				} else {
					print "<span id=\"noLanguages\" style=\"display:none;\">No Languages Assigned<br /></span>";
				}
				foreach ($film_languages as $thisLanguage) {
					print form_dropdown("languages-".$thisLanguage->id, $languages_array, $thisLanguage->language_id," class='select ui-widget-content ui-corner-all'")."<br />";
				}
				?>
				<div id="addLanguageHere"><input type="hidden" name="addLanguageId" id="addLanguageId" value="0" /></div>
				<button name="addLanguage" id="addLanguage">Add Language</button>
			</td>
			<td width="33%" align="center">
				<label for="genres" class="centered_title">Genres</label>
				<?php
				if (count($film_genres) == 0) {
					print "<span id=\"noGenres\">No Genres Assigned<br /></span>";
				} else {
					print "<span id=\"noGenres\" style=\"display:none;\">No Genres Assigned<br /></span>";
				}
				foreach ($film_genres as $thisGenre) {
					print form_dropdown("genres-".$thisGenre->id, $genres_array, $thisGenre->genre_id," class='select ui-widget-content ui-corner-all'")."<br />";
				}
				?>
				<div id="addGenreHere"><input type="hidden" name="addGenreId" id="addGenreId" value="0" /></div>
				<button name="addGenre" id="addGenre">Add Genre</button>
			</td>
		</tr>
		</tbody>
	</table>

	<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="4"><label for="generalNotes" class="centered_title">General Notes</label></td>
		</tr>
		<tr valign="top">
			<td colspan="4"><?php print form_textarea(array('name'=>'generalNotes', 'id'=>'generalNotes', 'value'=>$film[0]->generalNotes, 'rows'=>'4', 'cols'=>'72', 'class'=>'text ui-widget-content ui-corner-all')); ?><br /><br /></td>
		</tr>
		<tr valign="top">
			<td colspan="4"><label for="myhiff_1" class="centered_title">You May Also Like...</label></td>
		</tr>
		<tr valign="top">
			<td colspan="2">
			<?php	print form_dropdown('myhiff_1', $films_array, $film[0]->myhiff_1,"id='myhiff_1' class='select ui-widget-content ui-corner-all'");
					print "<br />";
					print form_dropdown('myhiff_3', $films_array, $film[0]->myhiff_3,"id='myhiff_3' class='select ui-widget-content ui-corner-all'");
			?>
			</td>
			<td colspan="2">
			<?php	print form_dropdown('myhiff_2', $films_array, $film[0]->myhiff_2,"id='myhiff_2' class='select ui-widget-content ui-corner-all'");
					print "<br />";
					print form_dropdown('myhiff_4', $films_array, $film[0]->myhiff_4,"id='myhiff_4' class='select ui-widget-content ui-corner-all'");
			?>
			</td>
		</tr>
		</tbody>
	</table>
	</div>

	<div class="field-half ui-corner-all">
			<table id="FilmPersonnel" class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tbody style="border-top:none;">
				<tr valign="top">
					<td colspan="4"><h3>Film Personnel</h3></td>
				</tr>
				<tr valign="top">
					<th width="5%">&nbsp;</th>
					<th width="25%">First Name</th>
					<th width="45%">Last/Family Name</th>
					<th width="25%">Role(s)</th>
				</tr>
			<?php
				foreach ($film_personnel as $thisPerson) {
					print "\t\t\t\t<tr valign=\"top\">\n";
					print "\t\t\t\t\t<td><button id=\"pers-".$thisPerson->id."\" data-id=\"".$thisPerson->id."\" class=\"edit\">Edit</button></td>\n";
					print "\t\t\t\t\t<td>".$thisPerson->name."</td>\n";
					print "\t\t\t\t\t<td>".$thisPerson->lastname."</td>\n";
					print "\t\t\t\t\t<td>";
					$roles = explode(",",$thisPerson->personnel_roles);
					foreach ($roles as $thisRole) {
						foreach ($personnel as $thisType) {
							if  ($thisRole == $thisType->id) {
								print $thisType->name."<br/>";
								break;
							}
						}
					}
					print "</td>\n";
					print "\t\t\t\t</tr>\n";
				}           
			?>
				</tbody>
			</table>
			<div id="add_personnel_here"><input type="hidden" id="new_personnel_id" name="new_personnel_id" value="0" /><input type="hidden" id="new_qa_ids" name="new_qa_ids" value="[]" /></div>
			<div align="center"><button id="addPersonnel">Add Personnel</button></div>            
	</div>

	<div class="field-half ui-corner-all">
			<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tbody style="border-top:none;">
				<tr valign="top">
					<td colspan="4"><h3>Sponsors</h3></td>
				</tr>
<?php
	if (count($sponsor_logos) > 0) {
		$standard_width = 150;
		$standard_height = 150;
		
		if ($sponsor_logos[0]->logo_width != 0 && $sponsor_logos[0]->logo_height != 0) {
			if ($sponsor_logos[0]->logo_width > $sponsor_logos[0]->logo_height) {
				$logo_aspect_ratio = $sponsor_logos[0]->logo_height / $sponsor_logos[0]->logo_width;

				if ($sponsor_logos[0]->logo_width <= $standard_width) {
					$width = $sponsor_logos[0]->logo_width;
				} else {
					$width = $standard_width;
					$height = $standard_width * $logo_aspect_ratio;
				}
			} elseif ($sponsor_logos[0]->logo_height > $sponsor_logos[0]->logo_width) {
				$logo_aspect_ratio = $sponsor_logos[0]->logo_width / $sponsor_logos[0]->logo_height;

				if ($sponsor_logos[0]->logo_height <= $standard_height) {
					$height = $sponsor_logos[0]->logo_height;
				} else {
					$height = $standard_height;
					$width = $standard_height * $logo_aspect_ratio;
				}
			} else {
				$logo_aspect_ratio = 1;
			}           
		} else {
			$width = $standard_width;
		}
?>
				<tr valign="top">
					<td colspan="4">
					<p align="center"><b>See the <a href="/admin/film_settings/#tabs-3">Film Settings</a> page to add/edit sponsors.</b></p>

<?php
		foreach ($sponsor_logos as $thisSponsor) {
?>
					<table cellpadding="0" cellspacing="0"><tbody style="border:none;">
						<tr valign="top">
							<td width="160">
							<?php
								if ($thisSponsor->url_logo != "") {
									if ($thisSponsor->url_website != "") { print "<a href=\"".$thisSponsor->url_website."\">"; }
									print "<img src=\"".$thisSponsor->url_logo."\" height=\"\" width=\"".$width."\" />";
									if ($thisSponsor->url_website != "") { print "</a>"; }
								} else {
									print "<p>No Logo Provided</p>";
								}
							?>
							</td>
							<td>
							<?php
								if ($thisSponsor->url_website != "") { print "<a href=\"".$thisSponsor->url_website."\">"; }
								print $thisSponsor->name;
								if ($thisSponsor->url_website != "") { print "</a>"; }
							?>
							</td>
						</tr>
					</tbody></table>
<?php
		}
?>                  
					</td>
				</tr>
<?php
	} else {
?>
				<tr valign="top">
					<td colspan="4">
					<p>No Sponsors have been assigned to this film. Please go to the <a href="/admin/film_settings/#tabs-3">Film Settings</a> page to manage these.</p>
					</td>
				</tr>
<?php
	}
?>
		</tbody>
	</table>
	</div>

	<div class="field-half ui-corner-all">
		<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td colspan="4"><h3>Film Exhibition / Technical Notes</h3></td>
			</tr>
			<tr valign="top">
				<td width="25%"><label for="videoformat">Video Format:</label></td>
				<td width="30%"><?php print form_dropdown('format', $videoformat_array, $film[0]->format_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
				<td width="25%"><label for="dcp_encryption">DCP Encryption:</label></td>
				<td width="15%"><?php print form_dropdown('dcp_encryption', $yes_no, $film[0]->dcp_encryption,"class='select-small ui-widget-content ui-corner-all'"); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="soundformat">Sound Format:</label></td>
				<td><?php print form_dropdown('sound', $soundformat_array, $film[0]->sound_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
				<td><label for="key_received">Key Received:</label></td>
				<td><?php print form_checkbox(array('name'=>'key_received', 'id'=>'key_received', 'value'=>'1', 'checked'=>$film[0]->key_received)); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="aspectratio">Aspect Ratio:</label></td>
				<td><?php print form_dropdown('aspectratio', $aspectratio_array, $film[0]->aspectratio_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
				<td><label for="key_tested">Key Tested:</label></td>
				<td><?php print form_checkbox(array('name'=>'key_tested', 'id'=>'key_tested', 'value'=>'1', 'checked'=>$film[0]->key_tested)); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="framerate">Frame Rate:</label></td>
				<td colspan="3"><?php print form_dropdown('framerate', $framerate_array, $film[0]->framerate_id,"class='select ui-widget-content ui-corner-all'"); ?></td>
			</tr>
			<tr valign="top">
				<td colspan="4"><label class="centered_title">Time Codes</label></td>
			</tr>
			<tr valign="top">
				<td><label for="colorbar_end_time">Color Bar &amp; Tone End:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'colorbar_end_time', 'id'=>'colorbar_end_time', 'value'=>$film[0]->colorbar_end_time, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="titleid_end_time">Title ID / Countdown End:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'titleid_end_time', 'id'=>'titleid_end_time', 'value'=>$film[0]->titleid_end_time, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="in_time">IN:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'in_time', 'id'=>'in_time', 'value'=>$film[0]->in_time, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="end_credits_time">End Credits:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'end_credits_time', 'id'=>'end_credits_time', 'value'=>$film[0]->end_credits_time, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			<tr valign="top">
				<td><label for="out_time">OUT:</label></td>
				<td colspan="3"><?php print form_input(array('name'=>'out_time', 'id'=>'out_time', 'value'=>$film[0]->out_time, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="field-half ui-corner-all">
		<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td colspan="4"><h3>Expenses / Revenue</h3></td>
			</tr>
			<tr valign="top">
				<td width="26%">
					<label for="FilmRevenue" title="This is the combined total of revenue from all screenings. Enter these values on the Screenings tab." style="cursor: help; border-bottom:1px dashed black;">Film Revenue</label><br />$&nbsp;
					<?php print $screening_revenue; ?>
				</td>
				<td width="22%">
					<label for="RentalFee">Rental Fee</label><br />$&nbsp;
					<?php print form_input(array('name'=>'RentalFee', 'id'=>'RentalFee', 'value'=>$film[0]->RentalFee, 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="26%">
					<label for="ScreeningFee">Screening Fee</label><br />$&nbsp;
					<?php print form_input(array('name'=>'ScreeningFee', 'id'=>'ScreeningFee', 'value'=>$film[0]->ScreeningFee, 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="26%" rowspan="2">
					<?php $total = intval($screening_revenue) - intval($film[0]->RentalFee) - intval($film[0]->TransferFee) - intval($film[0]->ScreeningFee) - intval($film[0]->GuestFee) - intval($shipping_fees);
						print "&nbsp;$".$screening_revenue." - Revenue<br/>";
						print "-$".$film[0]->RentalFee." - Rental<br/>";
						print "-$".$film[0]->ScreeningFee." - Screening<br/>";
						print "-$".$shipping_fees." - Shipping<br/>";
						print "-$".$film[0]->TransferFee." - Dubbing<br/>";
						print "-$".$film[0]->GuestFee." - Guests<br/>";
						print "<hr width=\"115\" align=\"left\" style=\"margin:0;\"><strong>$".$total." - ";
						if ($total > 0) { print "(Profit)<br/>"; }
						if ($total == 0) { print "(Break Even)<br/>"; }
						if ($total < 0) { print "(Loss)<br/>"; }
						print "</strong>";
					 ?>
				</td>
			</tr>
			<tr valign="top">
				<td>
					<label for="ShippingFees" title="This is the combined total of shipping fees from all print traffic legs. Enter these values on the Print Traffic tab." style="cursor: help; border-bottom:1px dashed black;">Shipping Fees</label><br />$&nbsp;
					<?php print $shipping_fees; ?>
				</td>
				<td>
					<label for="GuestFee">Guest Fee</label><br />$&nbsp;
					<?php print form_input(array('name'=>'GuestFee', 'id'=>'GuestFee', 'value'=>$film[0]->GuestFee, 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td>
					<label for="TransferFee">Dubbing Fee</label><br />$&nbsp;
					<?php print form_input(array('name'=>'TransferFee', 'id'=>'TransferFee', 'value'=>$film[0]->TransferFee, 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
			</tr>
			</tbody>
		</table>

	</div>
	<div class="field-half ui-corner-all">
		<table class="FilmEventTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td colspan="4"><br><h3>Audience Award Ballot Counts</h3></td>
			</tr>
			<tr valign="top">
				<td colspan="4">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tbody style="border-top:none;">
							<tr valign="top">
								<td width="20%"><label for="votes_1">1 (Poor)</label><br /><?php print form_input(array('name'=>'votes_1', 'id'=>'votes_1', 'value'=>$film[0]->votes_1, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
								<td width="20%"><label for="votes_2">2</label><br /><?php print form_input(array('name'=>'votes_2', 'id'=>'votes_2', 'value'=>$film[0]->votes_2, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
								<td width="20%"><label for="votes_3">3</label><br /><?php print form_input(array('name'=>'votes_3', 'id'=>'votes_3', 'value'=>$film[0]->votes_3, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
								<td width="20%"><label for="votes_4">4</label><br /><?php print form_input(array('name'=>'votes_4', 'id'=>'votes_4', 'value'=>$film[0]->votes_4, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
								<td width="20%"><label for="votes_5">5 (Excellent)</label><br /><?php print form_input(array('name'=>'votes_5', 'id'=>'votes_5', 'value'=>$film[0]->votes_5, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>

	</div>

	<!-- Synopsis Tab -->
	<div id="tabs-2">
	<fieldset class="ui-corner-all">
	<table class="SynopsisTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="35%"><label>Long Synopsis:</label></td>
			<td width="20%"><label>Program Book Page:</label> <?php print form_input(array('name'=>'PBPage', 'id'=>'PBPage', 'value'=>$film[0]->PBPage, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td width="20%"><label>User Guide Page:</label> <?php print form_input(array('name'=>'UGPage', 'id'=>'UGPage', 'value'=>$film[0]->UGPage, 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td width="25%" align="right"><label for="writerLong">Writer Credit:</label> <?php print form_input(array('name'=>'writerLong', 'id'=>'writerLong', 'value'=>$film[0]->writerLong, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr valign="top">
			<td colspan="4">
			<?php print form_textarea(array('name'=>'synopsis', 'id'=>'synopsis', 'value'=>$film[0]->synopsis, 'rows'=>'8', 'cols'=>'155', 'class'=>'ckeditor')); ?>
			</td>
		</tr>

		<tr valign="top"><td colspan="4">&nbsp;</td></tr>
		<tr valign="top">
			<td width="75%" colspan="3"><label>Short Synopsis:</label></td>
			<td width="25%" align="right"><label for="writerShort">Writer Credit:</label> <?php print form_input(array('name'=>'writerShort', 'id'=>'writerShort', 'value'=>$film[0]->writerShort, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr valign="top">
			<td colspan="4">
			<?php print form_textarea(array('name'=>'synopsis_short', 'id'=>'synopsis_short', 'value'=>$film[0]->synopsis_short, 'rows'=>'5', 'cols'=>'155', 'class'=>'ckeditor')); ?>
			</td>
		</tr>
		<tr valign="top"><td colspan="4">&nbsp;</td></tr>
		<tr valign="top">
			<td width="75%" colspan="3"><label>Original Language Synopsis:</label></td>
			<td width="25%" align="right"><label for="writerOrigLang">Writer Credit:</label> <?php print form_input(array('name'=>'writerOrigLang', 'id'=>'writerOrigLang', 'value'=>$film[0]->writerOrigLang, 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr valign="top">
			<td colspan="4">
			<?php print form_textarea(array('name'=>'synopsis_original', 'id'=>'synopsis_original', 'value'=>$film[0]->synopsis_original, 'rows'=>'5', 'cols'=>'155', 'class'=>'ckeditor')); ?>
			</td>
		</tr>
		</tbody>
	</table>
	</fieldset>

	</div>
	<!-- Multimedia Tab -->
	<div id="tabs-3">
	<fieldset class="ui-corner-all">

	<table class="MultimediaTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td><h3>Film Stills / Photos</h3></td>
		</tr>
		<tr>
			<td>
			<div id="photo_container" class="ui-corner-all">
<?php
	print "<ul id=\"photo_sortable\">";
	foreach ($film_photos as $thisPhoto) {
		print "<li id=\"photo_".$thisPhoto->id."\" name=\"photo-".$thisPhoto->id."\" class=\"ui-state-default ui-corner-all\">";
		print "<div><img src=\"".$thisPhoto->url_cropsmall."\" height=\"120\" border=\"0\"></div>";
		if ($this->session->userdata('limited') == 0) {
			print "<div><a href=\"#\" id=\"photo-del-".$thisPhoto->id."\" data-id=\"".$thisPhoto->id."\" class=\"delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete this Photo\" title=\"Delete this Photo\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a></div>";
		}
		print "</li>";
	}
	print "</ul>";
?>
			</div>
			</td>
		</tr>
		<tr>
			<td><button id="addPhoto">Add Photo</button>&nbsp;&nbsp;&nbsp;<span>The first photo above will be used as the default photo on the film listing page.</span></td>
		</tr>
		
		<tr valign="top">
			<td><h3>Film Trailers / Clips</h3></td>
		</tr>
		<tr>
			<td>
			<div id="video_container" class="ui-corner-all">
<?php
	print "<ul id=\"video_sortable\">";
	foreach ($film_videos as $thisVideo) {
		switch ($thisVideo->service_name) {
			case "hulu":
			case "revision3":
			case "qik":
			case "viddler":
			case "vimeo":
			case "youtube": $video_object = $this->oembed->call($thisVideo->service_name, $thisVideo->url_video);
							break;
		
			case "bliptv":
			case "dailymotion":
			case "googlevideo":
			case "metacafe": $video_object = $this->oembed->call('oohembed', $thisVideo->url_video);
							 break;
			default: $video_object = "No video service was specified.";
					 break;
		}
		
		print "<li id=\"video-".$thisVideo->id."\" name=\"video-".$thisVideo->id."\" class=\"ui-state-default ui-corner-all\">";
		if ( is_object($video_object) == TRUE) {
			print "<div>".$video_object->html."</div>";
		} else {
			print "<div style=\"width:200px;\">Sorry, this video cannot be embedded properly. Please check the video's settings to see that embedding is allowed.</div>";
		}
		print "<div><a href=\"#\" id=\"video-edit-".$thisVideo->id."\" data-id=\"".$thisVideo->id."\" data-url=\"".$thisVideo->url_video."\" data-order=\"".$thisVideo->order."\" class=\"edit\"><img src=\"/assets/images/icons/edit.png\" alt=\"Edit this Video\" title=\"Edit this Video\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a></div>";
		print "</li>";
	}
	print "<li id=\"video_replace_me\" style=\"display:none;\"><input type=\"hidden\" id=\"video-id-new\" value=\"0\" /></li>";
	print "</ul>";
?>
			</div>
			</td>
		</tr>
		<tr>
			<td><button id="addVideo">Add Video</button>&nbsp;&nbsp;&nbsp;<span>Only the first video above will be displayed on this film's detail page.</span></td>
		</tr>
		</tbody>
	</table>
	</fieldset>

	</div>
	<!-- Screenings Tab -->
	<div id="tabs-4">
	<fieldset class="ui-corner-all">
	<table class="ScreeningsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<th width="10%">&nbsp;</th>
			<th width="15%">Total Runtime</th>
			<th width="13%">Date</th>
			<th width="11%">Time</th>
			<th width="15%">Location</th>
			<th width="12%">Host</th>
			<th width="7%">Private</th>
			<th width="7%">Standby</th>
			<th width="7%">Free</th>
			<th width="7%">Published</th>
		</tr>
<?php
	if (count($film_screenings) == 0) {
		print "\t\t<tr valign=\"top\" id=\"noScreenings\">\n";
		print "\t\t\t<td colspan=\"11\" align=\"center\">There are no screenings for this film yet. Please click the 'Add Screening' button below to add one.</td>\n";
		print "\t\t</tr>\n";
	} else {
		print "\t\t<tr valign=\"top\" id=\"noScreenings\" style=\"display:none;\">\n";
		print "\t\t\t<td colspan=\"11\" align=\"center\">There are no screenings for this film yet. Please click the 'Add Screening' button below to add one.</td>\n";
		print "\t\t</tr>\n";
	}

	foreach ($film_screenings as $thisScreening) {
	
		$runtime = 0;
		$film_program_ids = explode(",",$thisScreening->program_movie_ids);
		$film_breakdown = "";

		if ($thisScreening->program_name != "") {
			$film_breakdown .= "<strong>".$thisScreening->program_name."</strong> - ";
		}

		if ($thisScreening->intro == 1) {
			$film_breakdown .= "Intro (".$thisScreening->intro_length."), ";
			$runtime += $thisScreening->intro_length;
		}
		if ($thisScreening->fest_trailer == 1) {
			$film_breakdown .= "Festival Trailer (".$thisScreening->fest_trailer_length."), ";
			$runtime += $thisScreening->fest_trailer_length;
		}
		if ($thisScreening->sponsor_trailer1 == 1) {
			if ($thisScreening->sponsor_trailer1_name == "") {
				$film_breakdown .= "Sponsor Trailer 1";
			} else {
				$film_breakdown .= $thisScreening->sponsor_trailer1_name;
			}
			$film_breakdown .= " (".$thisScreening->sponsor_trailer1_length."), ";
			$runtime += $thisScreening->sponsor_trailer1_length;
		}
		if ($thisScreening->sponsor_trailer2 == 1) {
			if ($thisScreening->sponsor_trailer2_name == "") {
				$film_breakdown .= "Sponsor Trailer 2";
			} else {
				$film_breakdown .= $thisScreening->sponsor_trailer2_name;
			}
			$film_breakdown .= " (".$thisScreening->sponsor_trailer2_length."), ";
			$runtime += $thisScreening->sponsor_trailer2_length;
		}
		foreach ($film_program_ids as $scheduledFilm) {
			foreach ($films as $thisFilm) {
				if ($scheduledFilm == "$thisFilm->movie_id") {
					$film_breakdown .= switch_title($thisFilm->title_en)." (".$thisFilm->runtime_int."), ";
					$runtime += $thisFilm->runtime_int;
					break;
				}
			}
		}       
		if ($thisScreening->q_and_a == 1) {
			$film_breakdown .= "Q&amp;A Session (".$thisScreening->q_and_a_length.")";
			$runtime += $thisScreening->q_and_a_length;
		}
		$film_breakdown = trim($film_breakdown,", ");
		$runtime_string = calculate_runtime($runtime);
		
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\"><button id=\"scrn-".$thisScreening->id."\" name=\"scrn-".$thisScreening->id."\" data-id=\"".$thisScreening->id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td>".$runtime_string."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($thisScreening->date))."</td>\n";
		print "\t\t\t<td>".date("g:iA",strtotime($thisScreening->time))."</td>\n";
		print "\t\t\t<td>".$thisScreening->location;
		if ($thisScreening->location_id2 != 0) { print " / ".$thisScreening->location2; }
		print "</td>\n";
		if ($thisScreening->host != "") {
			print "\t\t\t<td>".$thisScreening->host."</td>\n";
		} else {
			print "\t\t\t<td>None</td>\n";
		}

		if ($thisScreening->Private != "0") {
			print "\t<td class=\"cent\"><a id=\"Private-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Private-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisScreening->Rush != "0") {
			print "\t<td class=\"cent\"><a id=\"Rush-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Rush-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisScreening->Free != "0") {
			print "\t<td class=\"cent\"><a id=\"Free-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Free-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisScreening->Published != "0") {
			print "\t<td class=\"cent\"><a id=\"Published-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Published-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";

		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><strong>Screening Breakdown:</strong> ".$film_breakdown."<br><strong>Screening Notes:</strong> ".$thisScreening->notes."</td>\n";
		print "\t\t</tr>\n";
	}
?>        
		
		<tr valign="top">
			<td align="center" colspan="10">
				<div id="addScreeningHere"><input type="hidden" id="screening-id-new" name="screening-id-new" value="0" /></div>
			
				<button id="addScreeningButton">Add Screening</button>
			</td>
		</tr>
		</tbody>
	</table>
	</fieldset>
	</div>


	<!-- Press/Print Source Tab -->
	<div id="tabs-5">
	<fieldset class="ui-corner-all">
	<table class="PressTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="4"><h3>Press Contact</h3></td>
		</tr>
		<tr valign="top">
			<td width="25%">
				<label for="PressContact">Press Contact</label><br />
				<?php print form_input(array('name'=>'PressContact', 'id'=>'PressContact', 'value'=>$film[0]->PressContact, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="PressEmail">Email Address</label><br />
				<?php print form_input(array('name'=>'PressEmail', 'id'=>'PressEmail', 'value'=>$film[0]->PressEmail, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="PressPhone">Phone Number</label><br />
				<?php print form_input(array('name'=>'PressPhone', 'id'=>'PressPhone', 'value'=>$film[0]->PressPhone, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="PressFax">Fax Number</label><br />
				<?php print form_input(array('name'=>'PressFax', 'id'=>'PressFax', 'value'=>$film[0]->PressFax, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="PressWebsite">Press Website</label><br />
				<?php print form_input(array('name'=>'PressWebsite', 'id'=>'PressWebsite', 'value'=>$film[0]->PressWebsite, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="3">
				<label for="HawaiiConnections">Hawaii Connections or Filming</label><br />
				<?php print form_textarea(array('name'=>'HawaiiConnections', 'id'=>'HawaiiConnections', 'value'=>$film[0]->HawaiiConnections, 'rows'=>'3', 'cols'=>'73', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>

		<tr valign="top">
			<td colspan="4"><br /><h3>Distributor / Print Source</h3></td>
		</tr>
		<tr valign="top">
			<td width="25%">
				<label for="distributor">Distributor Name</label><br />
				<?php print form_input(array('name'=>'distributor', 'id'=>'distributor', 'value'=>$film[0]->distributor, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="distributorEmail">Email Address</label><br />
				<?php print form_input(array('name'=>'distributorEmail', 'id'=>'distributorEmail', 'value'=>$film[0]->distributorEmail, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="distributorPhone">Phone Number</label><br />
				<?php print form_input(array('name'=>'distributorPhone', 'id'=>'distributorPhone', 'value'=>$film[0]->distributorPhone, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="25%">
				<label for="distributorFax">Fax Number</label><br />
				<?php print form_input(array('name'=>'distributorFax', 'id'=>'distributorFax', 'value'=>$film[0]->distributorFax, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<label for="distributorContact">Contact Name</label><br />
				<?php print form_input(array('name'=>'distributorContact', 'id'=>'distributorContact', 'value'=>$film[0]->distributorContact, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td>
				<label for="distributorAddress">Mailing Address</label><br />
				<?php print form_textarea(array('name'=>'distributorAddress', 'id'=>'distributorAddress', 'value'=>$film[0]->distributorAddress, 'rows'=>'3', 'cols'=>'33', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="distributorContact">Distribution Type</label><br />
				<?php print form_dropdown('distribution', $distribution_array, $film[0]->distribution_id,"class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		</tbody>
	</table>
	</fieldset>
	</div>

	<!-- Print Traffic Tab -->
	<div id="tabs-6">
	<fieldset class="ui-corner-all">
	<table class="PrintTrafficTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="6"><h3>Inbound Shipment Details</h3></td>
			<td width="4%" rowspan="4">&nbsp;
				
			</td>
			<td colspan="6"><h3>Outbound Shipment Details</h3></td>
		</tr>
		<tr valign="top">
			<td width="16%" colspan="2">
				<label for="InboundContact">Contact Name</label><br />
				<?php print form_input(array('name'=>'InboundContact', 'id'=>'InboundContact', 'value'=>$pt_details[0]->InboundContact, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="16%" colspan="2">
				<label for="InboundEmail">Contact Email</label><br />
				<?php print form_input(array('name'=>'InboundEmail', 'id'=>'InboundEmail', 'value'=>$pt_details[0]->InboundEmail, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="16%" colspan="2">
				<label for="InboundPhone">Contact Phone</label><br />
				<?php print form_input(array('name'=>'InboundPhone', 'id'=>'InboundPhone', 'value'=>$pt_details[0]->InboundPhone, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="16%" colspan="2">
				<label for="OutboundContact">Contact Name</label><br />
				<?php print form_input(array('name'=>'OutboundContact', 'id'=>'OutboundContact', 'value'=>$pt_details[0]->OutboundContact, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="16%" colspan="2">
				<label for="OutboundEmail">Contact Email</label><br />
				<?php print form_input(array('name'=>'OutboundEmail', 'id'=>'OutboundEmail', 'value'=>$pt_details[0]->OutboundEmail, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="16%" colspan="2">
				<label for="OutboundPhone">Contact Phone</label><br />
				<?php print form_input(array('name'=>'OutboundPhone', 'id'=>'OutboundPhone', 'value'=>$pt_details[0]->OutboundPhone, 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="6">
				<label for="InboundNotes">Inbound Notes</label><br />
				<?php print form_textarea(array('name'=>'InboundNotes', 'id'=>'InboundNotes', 'value'=>$pt_details[0]->InboundNotes, 'rows'=>'4', 'cols'=>'71', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="3">
				<label for="OutboundNotes">Outbound Notes</label><br />
				<?php print form_textarea(array('name'=>'OutboundNotes', 'id'=>'OutboundNotes', 'value'=>$pt_details[0]->OutboundNotes, 'rows'=>'4', 'cols'=>'34', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="3">
				<label for="OutboundShippingAddress">Receiving Address</label><br />
				<?php print form_textarea(array('name'=>'OutboundShippingAddress', 'id'=>'OutboundShippingAddress', 'value'=>$pt_details[0]->OutboundShippingAddress, 'rows'=>'4', 'cols'=>'34', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top" align="center">
			<td colspan="6"><form id="addInboundForm" name="addInboundForm"><input type="hidden" name="festivalmovie_id_in" id="festivalmovie_id_in" value="<?php print $film[0]->id; ?>" /></form><button id="addInboundShipment">Add Inbound Shipment</button></td>
			<td colspan="6"><form id="addOutboundForm" name="addOutboundForm"><input type="hidden" name="festivalmovie_id_out" id="festivalmovie_id_out" value="<?php print $film[0]->id; ?>" /></form><button id="addOutboundShipment">Add Outbound Shipment</button></td>
		</tr>
		</tbody>
	</table>

	<p>&nbsp;</p>
	<table class="PrintTrafficTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top" class="Outbound">
			<th width="9%" style="text-align:center;">Outbound</th>
			<th width="13%">Who Pays?</th>
			<th width="13%">Courier</th>
			<th width="13%">Account Num</th>
			<th width="13%">Tracking Num</th>
			<th width="13%">Shipping Fee</th>
			<th width="13%">Ship Date</th>
			<th width="13%">Arrive By</th>
		</tr>
<?php
	$rowcount = 1;
	foreach ($pt_shipments as $thisShipment) {  
		$tracking_url = "";
		$tracking_link = 0;  
		$id = "-".$thisShipment->id;
		$plain_id = $thisShipment->id;
		if ($thisShipment->courier_id != 0) {
			if ($thisShipment->TrackingNum != "" && ($courier_array[$thisShipment->courier_id] == "FedEx" || $courier_array[$thisShipment->courier_id] == "DHL" || $courier_array[$thisShipment->courier_id] == "UPS" || $courier_array[$thisShipment->courier_id] == "USPS")) {
				$tracking_url = get_tracking_url($courier_array[$thisShipment->courier_id],$thisShipment->TrackingNum);
				$tracking_link = 1;
			}
		}

		// Outbound Shipments Only
		$columns = 7;
		if ($thisShipment->outbound == 1) {
			print "\t\t\t\t<tr valign=\"top\"";
			if ( ($rowcount % 2) == 0) { print " class=\"evenrow\""; } else { print " class=\"oddrow\""; }
			print ">\n";
			print "\t\t\t\t\t<td><button id=\"ptout-".$plain_id."\" name=\"ptout-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
			if ($this->session->userdata('limited') == 0) {
				print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
			}
			print "</td>\n";
			if ($thisShipment->wePay == 1) {
				print "\t\t\t\t\t<td>We Pay</td>\n";
			} else {
				print "\t\t\t\t\t<td>Receiver Pays</td>\n";
			}
			if ($thisShipment->courier_id == 0) {
				print "\t\t\t\t\t<td>None Selected</td>\n";
			} else {
				print "\t\t\t\t\t<td>".$courier_array[$thisShipment->courier_id]."</td>\n";
			}
			print "\t\t\t\t\t<td>".$thisShipment->AccountNum."</td>\n";
			print "\t\t\t\t\t<td>";
			if ($tracking_link == 1) {
				print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/icons/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
			}
			print $thisShipment->TrackingNum."</td>\n";
			print "\t\t\t\t\t<td>$".$thisShipment->ShippingFee."</td>\n";
			//print "\t\t\t\t\t<td>";
			//if ($thisShipment->OutScheduleBy != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->OutScheduleBy)); }
			//print "</td>\n";
			//print "\t\t\t\t\t<td>";
			//if ($thisShipment->OutShipBy != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->OutShipBy)); }
			//print "</td>\n";
			print "\t\t\t\t\t<td>";
			if ($thisShipment->OutShipped != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->OutShipped)); }
			print "</td>\n";
			print "\t\t\t\t\t<td>";
			if ($thisShipment->OutArriveBy != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->OutArriveBy)); }
			print "</td>\n";
			print "\t\t\t\t</tr>\n";

			print "\t\t\t\t<tr valign=\"top\"";
			if ($rowcount % 2 == 0) { print " class=\"evenrow2\""; } else { print " class=\"oddrow2\""; }
			print ">\n";
			print "\t\t\t\t\t<td colspan=\"1\">";
			if ($thisShipment->confirmed == 1) {
				print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"ship\">Shipped</button>";
			} else {
				print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"ship\">Not Shipped</button>";
			}
			print "</td>\n";
			print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$thisShipment->notes."</td>\n";
			print "\t\t\t\t</tr>\n";
			$rowcount++;
		}
	}
?>
		<tr valign="top" style="display:none;">
			<td colspan="10"><div id="add_outbound_shipment_here"><input type="hidden" name="added_outbound_pt_id" id="added_outbound_pt_id" value="0" /></div></td>
		</tr>
	</table>

	<p>&nbsp;</p>
	<table class="PrintTrafficTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top" class="Inbound">
			<th width="9%" style="text-align:center;">Inbound</th>
			<th width="13%">Who Pays?</th>
			<th width="13%">Courier</th>
			<th width="13%">Account Num</th>
			<th width="13%">Tracking Num</th>
			<th width="13%">Shipping Fee</th>
			<th width="13%">Received Date</th>
			<th width="13%">Expected Arrival</th>
		</tr>
<?php
	$rowcount = 1;
	foreach ($pt_shipments as $thisShipment) {
		$tracking_url = "";
		$tracking_link = 0;  
		$id = "-".$thisShipment->id;
		$plain_id = $thisShipment->id;
		if ($thisShipment->courier_id != 0) {
			if ($thisShipment->TrackingNum != "" && ($courier_array[$thisShipment->courier_id] == "FedEx" || $courier_array[$thisShipment->courier_id] == "DHL" || $courier_array[$thisShipment->courier_id] == "UPS" || $courier_array[$thisShipment->courier_id] == "USPS")) {
				$tracking_url = get_tracking_url($courier_array[$thisShipment->courier_id],$thisShipment->TrackingNum);
				$tracking_link = 1;
			}
		}

		// Inbound Shipments Only
		$columns = 7;
		if ($thisShipment->outbound == 0) {
			print "\t\t\t\t<tr valign=\"top\"";
			if ( ($rowcount % 2) == 0) { print " class=\"evenrow\""; } else { print " class=\"oddrow\""; }
			print ">\n";
			print "\t\t\t\t\t<td><button id=\"ptin-".$plain_id."\" name=\"ptin-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
			if ($this->session->userdata('limited') == 0) {
				print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
			}
			print "</td>\n";
			if ($thisShipment->wePay == 1) {
				print "\t\t\t\t\t<td>We Pay</td>\n";
			} else {
				print "\t\t\t\t\t<td>Receiver Pays</td>\n";
			}
			if ($thisShipment->courier_id == 0) {
				print "\t\t\t\t\t<td>None Selected</td>\n";
			} else {
				print "\t\t\t\t\t<td>".$courier_array[$thisShipment->courier_id]."</td>\n";
			}
			print "\t\t\t\t\t<td>".$thisShipment->AccountNum."</td>\n";
			print "\t\t\t\t\t<td>";
			if ($tracking_link == 1) {
				print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/icons/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
			}
			print $thisShipment->TrackingNum."</td>\n";
			print "\t\t\t\t\t<td>$".$thisShipment->ShippingFee."</td>\n";
			//print "\t\t\t\t\t<td>";
			//if ($thisShipment->InShipped != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->InShipped)); }
			//print "</td>\n";
			print "\t\t\t\t\t<td>";
			if ($thisShipment->InReceived != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->InReceived)); }
			print "</td>\n";
			print "\t\t\t\t\t<td>";
			if ($thisShipment->InExpectedArrival != "0000-00-00") { print date("m/d/Y",strtotime($thisShipment->InExpectedArrival)); }
			print "</td>\n";
			print "\t\t\t\t</tr>\n";

			print "\t\t\t\t<tr valign=\"top\"";
			if ($rowcount % 2 == 0) { print " class=\"evenrow2\""; } else { print " class=\"oddrow2\""; }
			print ">\n";
			print "\t\t\t\t\t<td colspan=\"1\">";
			if ($thisShipment->confirmed == 1) {
				print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"receive\">Received</button>";
			} else {
				print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"receive\">Not Received</button>";
			}
			print "</td>\n";
			print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$thisShipment->notes."</td>\n";
			print "\t\t\t\t</tr>\n";
			$rowcount++;
		}
	}
?>
		<tr valign="top" style="display:none;">
			<td colspan="10"><div id="add_inbound_shipment_here"><input type="hidden" name="added_inbound_pt_id" id="added_inbound_pt_id" value="0" /></div></td>
		</tr>
	</table>

	</fieldset> </div>

</div><br />
<?php if ($this->session->userdata('limited') == 0) { ?>
		<button name="Delete" id="Delete" class="ui-widget ui-corner-all ui-state-error">DELETE FILM</button>
<?php }
	print form_close();

	// End page form, begin dialog boxes

	// Film/Event Info
	$personnel_array = convert_to_array2($personnel);
	$personnel_json = json_encode($film_personnel);

	// Screenings
	$location_array = convert_to_array($locations);

	// Print Traffic
	$courier_array = convert_to_array($pt_courier);
	$outbound_array = array("0"=>"Choose Shipment Type","1"=>"Inbound (Receiving)","2"=>"Outbound (Shipping)");
	$payment_array = array("1"=>"Yes","0"=>"No");
	$films_array = convert_films_to_array2($films);

	// Multimedia
	$highest_id = 0;
	foreach ($film_videos as $video) {
		if ($video->order >= $highest_id) {
			$highest_id = $video->order;
		}
	}
?>

<div id="addPersonnel-dialog" title="Add Film Personnel">
<form name="addPersonnelForm" id="addPersonnelForm">
	<table class="PersonnelDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="28%">
				<label for="pers-first-name-new">First Name</label> <br />
				<?php print form_input(array('name'=>'pers-first-name-new', 'id'=>'pers-first-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="28%">
				<label for="pers-last-name-new">Last Name</label> <br />
				<?php print form_input(array('name'=>'pers-last-name-new', 'id'=>'pers-last-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="20%">
				<label for="pers-last-name-first-new">Last Name First?</label><br />
				<?php print form_checkbox(array('name'=>'pers-last-name-first-new','id'=>'pers-last-name-first-new','value'=>'1','checked'=>false)); ?>
			</td>
			<td width="24%" rowspan="4">
				<label for="pers-role-new">Role(s)</label> <span class="req">*</span><br />
				<?php print form_multiselect('pers-role-new[]', $personnel_array, '', "id='pers-role-new' class='multiselect ui-widget-content ui-corner-all'"); ?>
				<span>Hold CTRL / OPTION to select multiple roles</span>
			</td>
		</tr>
		</tbody>
	</table>
	<?php print form_hidden('pers-movie-id',$film[0]->movie_id); ?>
</form>
</div>

<div id="editPersonnel-dialog" title="Edit Film Personnel">
<form name="editPersonnelForm" id="editPersonnelForm">
	<table class="PersonnelDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="28%">
				<label for="pers-first-name">First Name</label> <br />
				<?php print form_input(array('name'=>'pers-first-name', 'id'=>'pers-first-name', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="28%">
				<label for="pers-last-name">Last Name</label> <br />
				<?php print form_input(array('name'=>'pers-last-name', 'id'=>'pers-last-name', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="20%">
				<label for="pers-last-name-first">Last Name First?</label><br />
				<?php print form_checkbox(array('name'=>'pers-last-name-first', 'id'=>'pers-last-name-first', 'value'=>'1', 'checked'=>false)); ?>
			</td>
			<td width="24%" rowspan="4">
				<label for="pers-role-new">Role(s)</label> <span class="req">*</span><br />
				<?php print form_multiselect('pers-role[]', $personnel_array, '', "id='pers-role' class='multiselect ui-widget-content ui-corner-all'"); ?>
				<span>Hold CTRL / OPTION to select multiple roles</span>
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" value="0" name="pers-id" id="pers-id">
	<?php print form_hidden('pers-movie-id',$film[0]->movie_id); ?>
</form>
</div>

<div id="addPhoto-dialog" title="Add Photo">
<form name="addPhotoForm" id="addPhotoForm" enctype="multipart/form-data" target="uploadPhotoFrame" method="post" action="/admin/film_edit/upload_photo/">
	<input type="hidden" name="movie_id_photo" id="movie_id_photo" value="<?php print $film[0]->movie_id; ?>" />
	<table class="PhotoDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="100%">
				<label for="photo-upload-new">Attach a Photo</label> <span class="req">*</span><br />
				<?php print form_upload(array('name'=>'photo-upload-new', 'id'=>'photo-upload-new', 'value'=>'', 'class'=>'text-upload', 'size'=>'55')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<p>Choose a photo to upload. Photos smaller than 1050 pixels wide by 600 pixels tall are not recommended. Please limit uploaded photos to a resolution lower than 4000 x 2500. Files exceeding these sizes might fail to be added.</p>
				<p><strong>Supported image file formats are: .JPG&nbsp;&nbsp;.GIF&nbsp;&nbsp;.PNG</strong></p>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="addPhoto2-dialog" title="Photo Preview / Crop">
<form name="addPhoto2Form" id="addPhoto2Form">
	<table class="PhotoDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td>
				<p>Choose how this photo should be cropped. Thumbnails will be created in several sizes.</p>
				<div id="photo-crop-preview" class="ui-corner-all">
					<img id="photo-crop-image" src="/assets/images/transparent.png" width="525" border="0">
					<!-- Sample Data, this should get replaced by the uploaded photo -->
					<div id="photo-crop-data">                    
					<input type='hidden' name='x1' value='0' id='x1' />
					<input type='hidden' name='y1' value='0' id='y1' />
					<input type='hidden' name='w' value='525' id='w' />
					<input type='hidden' name='h' value='300' id='h' />
					<input type='hidden' name='orig-filename1' id='orig-filename1' value='' />
					<input type='hidden' name='orig-filename2' id='orig-filename2' value='' />
					<input type='hidden' name='photo-new-id' value='0' />
					<input type='hidden' name='photo-new-path' value='0' />
					</div>
					<!-- End Sample Data -->
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="photoIframe">
	<iframe width="1" height="1" id="uploadPhotoFrame" name="uploadPhotoFrame" src="about:blank" frameborder="0"></iframe>
</div>

<div id="addVideo-dialog" title="Add Video">
<form name="addVideoForm" id="addVideoForm">
	<table class="VideoDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="100%">
				<label for="video-trailerurl">Trailer URL:</label> <span class="req">*</span>
				<?php print form_input(array('name'=>'video-trailerurl', 'id'=>'video-trailerurl', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<p>Please paste in the full url for the video you want to add. Supported services include:</p>
				<p>blip.tv, Dailymotion, Hulu, Metacafe, Qik, Revision3, Vimeo, YouTube</p>
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" name="highest-id" id="highest-id" value="<?php print $highest_id; ?>" />
	<input type="hidden" name="video-movie-id" id="video-movie-id" value="<?php print $film[0]->movie_id; ?>" />
</form>
</div>

<div id="editVideo-dialog" title="Edit Video">
<form name="editVideoForm" id="editVideoForm">
	<table class="VideoDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="100%">
				<label for="video-trailerurl-update">Trailer URL:</label> <span class="req">*</span>
				<?php print form_input(array('name'=>'video-trailerurl-update', 'id'=>'video-trailerurl-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<p>Please paste in the full url for the video you'd like to change this existing video to.<br />Supported services include:</p>
				<p>blip.tv, Dailymotion, Hulu, Metacafe, Qik, Revision3, Vimeo, YouTube</p>
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" name="video-current-order" id="video-current-order" value="0" />
	<input type="hidden" name="video-id-update" id="video-id-update" value="0" />
</form>
</div>

<div id="addScreening-dialog" title="Add Screening">
<form name="addScreeningForm" id="addScreeningForm">
	<input type="hidden" name="total_films" id="total_films" value="1" />
	<input type="hidden" name="total-runtime" id="total-runtime" value="0" />
    <table class="ScreeningDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td colspan="2">
				<h3>Screening Details</h3>
            </td>
            <td>
				<h3>Program / Notes</h3>
            </td>
            <td>
				<h3>Screening Breakdown</h3>
            </td>
        </tr>
        <tr valign="top">
            <td width="17%">
				<label for="scrn-published-new">Published:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-published-new', 'id'=>'scrn-published-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-rush-new">Standby:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-rush-new', 'id'=>'scrn-rush-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;
            </td>
            <td width="17%">
				<label for="scrn-private-new">Private:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-private-new', 'id'=>'scrn-private-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-free-new">Free:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-free-new', 'id'=>'scrn-free-new', 'value'=>'1', 'checked'=>false)); ?>
            </td>
            <td width="33%">
				<label for="scrn-program-name-new">Program Name:</label><br />
				<?php print form_input(array('name'=>'scrn-program-name-new', 'id'=>'scrn-program-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td width="33%" rowspan="7">
				<fieldset class="ui-corner-all">
			    <table class="addScreeningBreakdown" width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tbody style="border-top:none;">
			        <tr valign="top">
                    	<td width="65%"><label>Film Introduction</label></td>
                    	<td width="25%"><input name="intro-length" id="intro-length" type="text" size="1" value="5" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td width="10%"><input name="intro" id="intro" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Festival Trailer</label></td>
                    	<td><input name="fest-trailer-length" id="fest-trailer-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="fest-trailer" id="fest-trailer" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer1-name" id="sponsor-trailer1-name" type="text" size="1" value="Sponsor Trailer 1" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer1-length" id="sponsor-trailer1-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer1" id="sponsor-trailer1" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer2-name" id="sponsor-trailer2-name" type="text" size="1" value="Sponsor Trailer 2" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer2-length" id="sponsor-trailer2-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer2" id="sponsor-trailer2" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Film 1</label> <span class="req">*</span></td>
                    	<td colspan="2"><div id="scrn-film-01-length"><?php print $film[0]->runtime_int; ?></span> min</td>
                    </tr>
			        <tr valign="top">
                    	<td colspan="3"><?php print form_dropdown_film('scrn-film-01', $film_screening_array, $film[0]->movie_id, "id='scrn-film-01' class='select-film ui-widget-content ui-corner-all'"); ?></td>
                    </tr>
                    <tr valign="top">
                    	<td colspan="3">                       
                        <div id="addNewFilmsHere"></div>
                        
                        <div align="center"><button id="addFilmScreeningDialog">Add Another Film</button></div>
                        </td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Q&amp;A Session</label></td>
                    	<td><input name="q-and-a-length" id="q-and-a-length" type="text" size="2" value="15" class="time-int ui-widget-content ui-corner-all"> min</td>
                        <td><input name="q-and-a" id="q-and-a" type="checkbox" value="1" class="time-int" /></td>
                    </tr>
                    </tbody>
                </table>
                <div id="scrn-runtime-1">Program Runtime:</div><div id="scrn-runtime-2"><?php print calculate_runtime(0); ?></div>
				</fieldset>				
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="scrn-date-new">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'scrn-date-new', 'id'=>'scrn-date-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="scrn-time-new">Time:</label> <span class="req">*</span> like: '7:00 PM'<br />
				<?php print form_input(array('name'=>'scrn-time-new', 'id'=>'scrn-time-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td rowspan="2">
                <label for="scrn-program-desc-new">Program Description:</label><br />
                <?php print form_textarea(array('name'=>'scrn-program-desc-new', 'id'=>'scrn-program-desc-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
            <td colspan="2">
				<label for="scrn-location-new">Screening Location:</label> <span class="req">*</span><br />
                <?php print form_dropdown('scrn-location-new', $location_array, 0,"id='scrn-location-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
		<tr valign="top">
            <td colspan="2">
				<label for="scrn-location2-new">Interlock? (2nd Location):</label><br />
                <?php print form_dropdown('scrn-location2-new', $location_array, 0,"id='scrn-location2-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td rowspan="4">
                <label for="scrn-notes-new">Screening Notes (private):</label><br />
                <?php print form_textarea(array('name'=>'scrn-notes-new', 'id'=>'scrn-notes-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-ticket-new">Ticket Link:</label> <a href="#" id="addTicketLink" style="float:right; padding-right:5px;">Add tix.com link</a><br />
				<?php print form_input(array('name'=>'scrn-ticket-new', 'id'=>'scrn-ticket-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-host-new">Host / Introducer:</label><br />
				<?php print form_input(array('name'=>'scrn-host-new', 'id'=>'scrn-host-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="audience-count-new">Audience Count:</label><br />
				<?php print form_input(array('name'=>'audience-count-new', 'id'=>'audience-count-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="screening-revenue-new">Screening Revenue:</label><br />
				$ <?php print form_input(array('name'=>'screening-revenue-new', 'id'=>'screening-revenue-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<div id="editScreening-dialog" title="Edit Screening">
<form name="editScreeningForm" id="editScreeningForm">
	<input type="hidden" name="screening_id_update" id="screening_id_update" value="0" />
	<input type="hidden" name="program_movie_ids_update" id="program_movie_ids_update" value="0" />
	<input type="hidden" name="total_films_update" id="total_films_update" value="0" />
	<input type="hidden" name="total-runtime-update" id="total-runtime-update" value="0" />
    <table class="ScreeningDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td colspan="6">
				<h3>Screening Details</h3>
            </td>
            <td>
				<h3>Program / Notes</h3>
            </td>
            <td>
				<h3>Screening Breakdown</h3>
            </td>
        </tr>
        <tr valign="top">
        	<td width="17%" colspan="3">
				<label for="scrn-published-update">Published:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-published-update', 'id'=>'scrn-published-update', 'value'=>'1', 'checked'=>false)); ?>

				<br /><label for="scrn-private-update">Private:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-private-update', 'id'=>'scrn-private-update', 'value'=>'1', 'checked'=>false)); ?>
        	</td>
        	<td width="17%" colspan="3">
				<label for="scrn-rush-update">Standby:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-rush-update', 'id'=>'scrn-rush-update', 'value'=>'1', 'checked'=>false)); ?>

				<br /><label for="scrn-free-update">Free:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-free-update', 'id'=>'scrn-free-update', 'value'=>'1', 'checked'=>false)); ?>
        	</td>
            <td width="33%">
				<label for="scrn-program-name-update">Program Name:</label><br />
				<?php print form_input(array('name'=>'scrn-program-name-update', 'id'=>'scrn-program-name-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td width="33%" rowspan="7">
				<fieldset class="ui-corner-all">
			    <table class="addScreeningBreakdown" width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tbody style="border-top:none;">
			        <tr valign="top">
                    	<td width="65%"><label>Film Introduction</label></td>
                    	<td width="25%"><input name="intro-length-update" id="intro-length-update" type="text" size="1" value="5" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td width="10%"><input name="intro-update" id="intro-update" type="checkbox" value="1" checked="checked" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Festival Trailer</label></td>
                    	<td><input name="fest-trailer-length-update" id="fest-trailer-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="fest-trailer-update" id="fest-trailer-update" type="checkbox" value="1" checked="checked" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer1-name-update" id="sponsor-trailer1-name-update" type="text" size="1" value="" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer1-length-update" id="sponsor-trailer1-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer1-update" id="sponsor-trailer1-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer2-name-update" id="sponsor-trailer2-name-update" type="text" size="1" value="" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer2-length-update" id="sponsor-trailer2-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer2-update" id="sponsor-trailer2-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
                    <tr valign="top">
                    	<td colspan="3">
                        <div id="addNewFilmsHere-update"></div>
                        
                        <div align="center"><button id="addFilmScreeningDialog-update">Add Another Film</button></div>
                        </td>
                    </tr>
			        <tr valign="top">
                    	<td>
                        	<label>Q&amp;A Session</label><br />
                        	<span id="q-and-a-personnel"></span>
                        </td>
                    	<td><input name="q-and-a-length-update" id="q-and-a-length-update" type="text" size="2" value="15" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                        <td><input name="q-and-a-update" id="q-and-a-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
                    </tbody>
                </table>
                <div id="scrn-runtime-1-update">Program Runtime:</div><div id="scrn-runtime-2-update"><?php print calculate_runtime(0); ?></div>
				</fieldset>				
            </td>
        </tr>
		<tr valign="top">
            <td colspan="3">
				<label for="scrn-date-update">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'scrn-date-update', 'id'=>'scrn-date-update', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="3">
				<label for="scrn-time-update">Time:</label> <span class="req">*</span> like: '7:00 PM'<br />
				<?php print form_input(array('name'=>'scrn-time-update', 'id'=>'scrn-time-update', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td rowspan="2">
				<label for="scrn-program-desc-update">Program Description:</label><br />
				<?php print form_textarea(array('name'=>'scrn-program-desc-update', 'id'=>'scrn-program-desc-update', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-location-update">Screening Location:</label> <span class="req">*</span><br />
                <?php print form_dropdown('scrn-location-update', $location_array, 0,"id='scrn-location-update' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-location2-update">Interlock? (2nd Location):</label><br />
                <?php print form_dropdown('scrn-location2-update', $location_array, 0,"id='scrn-location2-update' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td rowspan="4">
                <label for="scrn-notes-update">Screening Notes:</label><br />
                <?php print form_textarea(array('name'=>'scrn-notes-update', 'id'=>'scrn-notes-update', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-ticket-update">Ticket Link:</label> <a href="#" id="updateTicketLink" style="float:right; padding-right:5px;">Add tix.com link</a><br />
				<?php print form_input(array('name'=>'scrn-ticket-update', 'id'=>'scrn-ticket-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-host-update">Host / Introducer:</label><br />
				<?php print form_input(array('name'=>'scrn-host-update', 'id'=>'scrn-host-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="audience-count-update">Audience #:</label><br />
				<?php print form_input(array('name'=>'audience-count-update', 'id'=>'audience-count-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="2">
				<label for="screening-revenue-update">Screening $:</label><br />
				$ <?php print form_input(array('name'=>'screening-revenue-update', 'id'=>'screening-revenue-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="2">
				<label for="q-and-a-count-update">Q&amp;A Count:</label><br />
				<?php print form_input(array('name'=>'q-and-a-count-update', 'id'=>'q-and-a-count-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<div id="addPrintTraffic-dialog" title="Add Inbound/Outbound Shipment">
<form name="addPrintTrafficForm" id="addPrintTrafficForm">
	<input type="hidden" name="festivalmovie_id-new" id="festivalmovie_id-new" value="<?php print $film[0]->id; ?>" />
	<table class="ptAddDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="4">
				<label for="festivalmovie_id-new">Film:</label><br />
				<?php print switch_title($film[0]->title_en); ?>
			</td>
			<td colspan="4">
				<label for="outbound-new">Inbound or Outbound?:</label> <span class="req">*</span><br />
				<?php print form_dropdown('outbound-new', $outbound_array, 0,"id='outbound-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="4">
				<label for="ShippingFee-new">Shipping Fee:</label><br />
				$<?php print form_input(array('name'=>'ShippingFee-new', 'id'=>'ShippingFee-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top" class="InboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InReceived-new">Received Date:</label><br />
				<?php print form_input(array('name'=>'InReceived-new', 'id'=>'InReceived-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InExpectedArrival-new">Expected Arrival Date:</label><br />
				<?php print form_input(array('name'=>'InExpectedArrival-new', 'id'=>'InExpectedArrival-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top" class="OutboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="OutShipped-new">Shipped Date:</label><br />
				<?php print form_input(array('name'=>'OutShipped-new', 'id'=>'OutShipped-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span><label for="OutArriveBy-new">Arrive By Date:</label><br />
				<?php print form_input(array('name'=>'OutArriveBy-new', 'id'=>'OutArriveBy-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="courier_id-new">Courier:</label> <span class="req">*</span><br />
				<?php print form_dropdown('courier_id-new', $courier_array, 0,"id='courier_id-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="4">
				<label for="AccountNum-new">Account Number:</label><br />
				<?php print form_input(array('name'=>'AccountNum-new', 'id'=>'AccountNum-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="4">
				<label for="TrackingNum-new">Tracking Number:</label><br />
				<?php print form_input(array('name'=>'TrackingNum-new', 'id'=>'TrackingNum-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="courier_id-new">We Pay?:</label><br />
				<?php print form_dropdown('wePay-new', $payment_array, 1,"id='wePay-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="8">
				<label for="notes-new">Shipping Notes:</label><br />
				<?php print form_textarea(array('name'=>'notes-new', 'id'=>'notes-new', 'value'=>'', 'rows'=>'4', 'cols'=>'57', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="editPrintTraffic-dialog" title="Edit Inbound/Outbound Shipment">
<form name="editPrintTrafficForm" id="editPrintTrafficForm">
	<input type="hidden" name="confirmed-update" id="confirmed-update" value="0" />
	<input type="hidden" name="printtraffic_id-update" id="printtraffic_id-update" value="0" />
	<input type="hidden" name="festivalmovie_id-update" id="festivalmovie_id-update" value="0" />
	<table class="ptEditDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td colspan="4">
				<label for="festivalmovie_id-update">Film:</label><br />
				<?php print switch_title($film[0]->title_en); ?>
			</td>
			<td colspan="4">
				<label for="outbound-update">Inbound or Outbound?:</label> <span class="req">*</span><br />
				<?php print form_dropdown('outbound-update', $outbound_array, 0,"id='outbound-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="4">
				<label for="ShippingFee-update">Shipping Fee:</label><br />
				$<?php print form_input(array('name'=>'ShippingFee-update', 'id'=>'ShippingFee-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top" class="InboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InReceived-update">Received Date:</label><br />
				<?php print form_input(array('name'=>'InReceived-update', 'id'=>'InReceived-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="InExpectedArrival-update">Expected Arrival Date:</label><br />
				<?php print form_input(array('name'=>'InExpectedArrival-update', 'id'=>'InExpectedArrival-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top" class="OutboundRow">
			<td colspan="4">
				<span class="calendar-icon"></span> <label for="OutShipped-update">Shipped Date:</label><br />
				<?php print form_input(array('name'=>'OutShipped-update', 'id'=>'OutShipped-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				<span class="calendar-icon"></span><label for="OutArriveBy-update">Arrive By Date:</label><br />
				<?php print form_input(array('name'=>'OutArriveBy-update', 'id'=>'OutArriveBy-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all ui-state-error', 'disabled'=>'disabled')); ?>
			</td>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="courier_id-update">Courier:</label> <span class="req">*</span><br />
				<?php print form_dropdown('courier_id-update', $courier_array, 0,"id='courier_id-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="4">
				<label for="AccountNum-update">Account Number:</label><br />
				<?php print form_input(array('name'=>'AccountNum-update', 'id'=>'AccountNum-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="4">
				<label for="TrackingNum-update">Tracking Number:</label><br />
				<?php print form_input(array('name'=>'TrackingNum-update', 'id'=>'TrackingNum-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="4">
				<label for="wePay-update">We Pay?:</label><br />
				<?php print form_dropdown('wePay-update', $payment_array, 1,"id='wePay-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td colspan="8">
				<label for="notes-update">Shipping Notes:</label><br />
				<?php print form_textarea(array('name'=>'notes-update', 'id'=>'notes-update', 'value'=>'', 'rows'=>'4', 'cols'=>'57', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="pdfPhoto-dialog" title="Generate PDF - Choose a Photo">
</div>

<script type="text/javascript">
	function reflowTab(tabObject) {
		tabObject.isotope('reLayout');
	};

	$(function() {  
		// ******************
		// Whole page
		// ******************
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		var tab1 = $('#tabs-1');
		tab1.isotope({ itemSelector: '.field-half', 'layoutMode': 'masonry' });

		$('#ui-id-1').on('click', function() { reflowTab(tab1); });
		$('#uppercase').on('click', function() {
			var title = $("#title_en"); if( $(this).is(':checked') ) { $(this).val(title.val()); title.val(title.val().toUpperCase()); } else { title.val($(this).val()); }
		});
		$("#UpdateFilm").validate({
			rules: { title_en: "required", runtime_int: { required:true, digits:true }, websiteurl: "url", votes_1: "digits", votes_2: "digits", votes_3: "digits", votes_4: "digits", votes_5: "digits", PBPage: "digits", UGPage: "digits", InboundEmail: "email", OutboundEmail: "email", PressEmail: "email", PressWebsite: "url", distributorEmail: "email" },
			messages: { title_en: "Please enter an english title for this film.", runtime_int: "Please enter the run time for this film. (Whole numbers only, round up any seconds)", websiteurl: "Please enter a valid URL (including http://).", votes_1: "Please enter whole number.", votes_2: "Please enter whole number.", votes_3: "Please enter whole number.", votes_4: "Please enter whole number.", votes_5: "Please enter whole number.", PBPage: "Please enter whole number.", UGPage: "Please enter whole number.", InboundEmail: "Please enter a valid email address.", OutboundEmail: "Please enter a valid email address.", PressEmail: "Please enter a valid email address.", PressWebsite: "Please enter a valid URL (including http://).", distributorEmail: "Please enter a valid email address."},
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					var invalidPanels = $(validator.invalidElements()).closest(".ui-tabs-panel", form);
					if (invalidPanels.size() > 0) {
						$.each($.unique(invalidPanels.get()), function(){
							$(this).siblings(".ui-tabs-nav")
								.find("a[href='#" + this.id + "']")
								.addClass("ui-state-error")
								.show("pulsate",{times: 3});
						});
					}
				} else {
					$(".ui-tabs-nav").each(function() { $(this).removeClass("ui-state-error"); });
				}
			}
		});

		$('#pdfPhoto-dialog').dialog({
			autoOpen: false,
			width: 475,
			height: "auto",
			modal: false,
			close: function() { }
		});

		$('#pdfPhoto-dialog').on('click', '.photo', function() {
			$('#pdfPhoto-dialog').dialog('close');
		})

		$('#topFilmEdit').on('click', '.choose_photo', function(e) {
			var film_urls = $(this).nextAll('.hiddenphoto').html();
			if (film_urls != "") {
				e.preventDefault();
				
				var film_name = $("#title_en").val();
				var film_href = $(this).attr("href");
				var url_array = film_urls.split("|");
				var photo_html = "<p>Choose a photo to be added to the PDF.</p>";
				var count = 0;
		
				jQuery.each(url_array, function(index, value) {
					var plusone = parseInt(count + 1);
					photo_html = photo_html + '<a href="'+film_href+count+'/" class="photo"><img src="'+value+'" alt="Photo #'+plusone+'" title="Photo #'+plusone+'" width="210" height="120"></a>';
					count++;
				});
		
				$('#pdfPhoto-dialog').html(photo_html);
				$('#pdfPhoto-dialog').dialog('option','title','Generate PDF - '+film_name+' - '+$(this).attr('title'));		
				$('#pdfPhoto-dialog').dialog('open');
			}
		});

		

		// ******************
		// Film/Event Info Tab
		// ******************
		$('#addCountry').on('click', function() { add_cntry_lang_genre("Country", <?php print $film[0]->movie_id; ?>); $(this).removeClass("ui-state-focus"); return false; });
		$('#addLanguage').on('click', function() { add_cntry_lang_genre("Language", <?php print $film[0]->movie_id; ?>); $(this).removeClass("ui-state-focus"); return false; });
		$('#addGenre').on('click', function() { add_cntry_lang_genre("Genre", <?php print $film[0]->movie_id; ?>); $(this).removeClass("ui-state-focus"); return false; });     

		$("#addPersonnelForm").validate({
			rules: { "pers-role-new[]": "required" },
			messages: { "pers-role-new[]": "Please select at least one role." }
		});
		$("#addPersonnel-dialog").dialog({
			autoOpen: false,
			height: 450,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Personnel': function() {
					if ($("#addPersonnelForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#add_personnel_here').replaceWith(msg);
								var new_id = $('#new_personnel_id').val();
								var qa_ids = $('#new_qa_ids').val();
								
								$('#pers-'+new_id).button();
								reflowTab( $('#tabs-1') );
							},
							complete: function (xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										personnelJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_edit/return_personnel_json/<?php print $film[0]->movie_id; ?>',
									type: 'POST',
									dataType: 'json'
								}); 
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addPersonnelForm').serialize(),
							url: '/admin/film_edit/add_personnel/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$("#addPersonnelForm INPUT[type='text']").each(function() { $(this).val(""); });
				$("#addPersonnelForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
				$("#addPersonnelForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
			}
		});
		$('#addPersonnel').on('click', function() { $('#addPersonnel-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); return false; });

		$("#editPersonnelForm").validate({
			rules: { "pers-role[]": "required" },
			messages: { "pers-role[]": "Please select at least one role." }
		});
		$("#editPersonnel-dialog").dialog({
			autoOpen: false,
			height: 450,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var decision = confirm("Are you sure you really want to delete this Film Personnel?");
					if (decision == true) {
						var pers_id = $('#pers-id').val();
						$.ajax({
							success: function(msg, text, xhr) {
								$('#pers-'+pers_id).closest('tr').slideUp();
								var timeoutID = window.setTimeout(function () {
									$('#pers-'+pers_id).closest('tr').remove()
									reflowTab( $('#tabs-1') );
								}, 2000);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editPersonnelForm').serialize(),
							url: '/admin/film_edit/delete_personnel/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				},
<?php } ?>
				'Update Personnel': function() {
					if ($("#editPersonnelForm").validate().form() == true) {
						var pers_id = $('#pers-id').val();
						$.ajax({
							success: function(msg, text, xhr) {
								$('#pers-'+pers_id).unbind('click');
								$('#pers-'+pers_id).closest('tr').replaceWith(msg);
								reflowTab( $('#tabs-1') );
							},
							complete: function (xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										personnelJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_edit/return_personnel_json/<?php print $film[0]->movie_id; ?>',
									type: 'POST',
									dataType: 'json'
								});
								var qa_buttons = $('#pers-qa-'+pers_id).val();
								$('#pers-'+pers_id).button();
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editPersonnelForm').serialize(),
							url: '/admin/film_edit/update_personnel/',
							type: 'POST',
							dataType: 'html',
							async: false
						});
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$("#editPersonnelForm INPUT[type=text]").each(function() { $(this).val(""); });
				$("#editPersonnelForm INPUT[type=checkbox]").each(function() { $(this).prop('checked',false); });
				$("#editPersonnelForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
			}
		});

		var personnelJSON = <?php print $personnel_json; ?>;
		$('#FilmPersonnel').on('click','button.edit', function(e) {
			e.preventDefault();
			open_edit_pers_dialog($(this).data('id').toString(), personnelJSON);
			$(this).removeClass("ui-state-focus");
		});

		// ******************
		// Synopsis Tab
		// ******************


		// ******************
		// Multimedia Tab
		// ******************
		$("#addPhotoForm").validate({
			rules: { "photo-upload-new": { required: true, accept: "png|jpe?g|gif" } },
			messages: { "photo-upload-new": "Please select an image file (.JPG .GIF .PNG)" }
		});
		$("#addPhoto-dialog").dialog({
			autoOpen: false,
			height: 300,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Upload Photo': function() {
					$('#addPhotoForm').submit();
					$('#photo-crop-image').attr('width','525').attr('src','/assets/images/loadingAnimation.gif');
					
					$.frameReady(function() {
						$('#photo-crop-data', window.parent.document).html( $("#photo-crop-data").html() );
						if ($("#photo-crop-data").html() == "The filetype you are attempting to upload is not allowed. Please press the 'Cancel' button and try again.") {
							$('#photo-crop-image', window.parent.document).css('display','none');
						} else {
							$('#photo-crop-image', window.parent.document).attr('src',$('#photo-new-path').val());
						}
					}, "top.uploadPhotoFrame");
					$(this).dialog('close');
					$('#addPhoto2-dialog').dialog('open');
				}
			},
			close: function() {
				$('#photo-upload-new').replaceWith('<input type="file" size="55" class="text-upload" id="photo-upload-new" value="" name="photo-upload-new">');
			}
		});
		$('#addPhoto').on('click', function() {
			$('#addPhoto-dialog').dialog('open');
			$('#uploadPhotoFrame').attr('src','about:blank');
			$(this).removeClass("ui-state-focus");
			return false;
		});

		var do_not_delete = 0;
		$("#addPhoto2-dialog").dialog({
			autoOpen: false,
			height: 675,
			width: 580,
			modal: true,
			open: function() {
				$('#photo-crop-image').imgAreaSelect({ hide:false, zIndex:1100, minWidth:525, minHeight:300, aspectRatio: '21:12', x1:0, y1:0, x2:525, y2:300, handles: true, onInit: preview, onSelectChange: preview });
			},
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Finish': function() {
					if ($("#addPhoto2Form").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#photo_sortable').append(msg);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addPhoto2Form').serialize(),
							url: '/admin/film_edit/upload_photo_crop/',
							type: 'POST',
							dataType: 'html',
							async: false
						}); 
	
						do_not_delete = 1;
						var new_photo_id = $('#photo-new-id').val();
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				if (do_not_delete == 0) {
					$.ajax({
						success: function(msg, text, xhr) {
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addPhoto2Form').serialize(),
						url: '/admin/film_edit/remove_uploaded_photos/',
						type: 'POST',
						dataType: 'html'
					});
				}
				$('#photo-crop-image').imgAreaSelect({ hide:true });
			}
		});

		$("#addVideoForm").validate({
			rules: { "video-trailerurl": { required: true, url: true } },
			messages: { "video-trailerurl": { required: "Please enter a video URL.", url: "Please enter a valid URL. (http://www.domain.com/directory/file.html)" } }
		});
		$("#addVideo-dialog").dialog({
			autoOpen: false,
			height: 270,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Video': function() {
					if ($("#addVideoForm").validate().form() == true) {
						var video_url = $('#video-trailerurl').val();
						$.ajax({
							success: function(msg, text, xhr) {
								$('#video_replace_me').replaceWith(msg);

								var video_id = $('#video-id-new').val();
								if (video_id == 0) {
									var timeoutID2 = window.setTimeout(function () { $('#video-0').remove(); }, 3000);
								}
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addVideoForm').serialize(),
							url: '/admin/film_edit/add_video/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$('#video-trailerurl').val('');
			}
		});
		$('#addVideo').on('click', function() {
			$('#addVideo-dialog').dialog('open');
			$(this).removeClass("ui-state-focus");
			return false;
		});

		$("#editVideo-dialog").dialog({
			autoOpen: false,
			height: 270,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var decision = confirm("Are you sure you really want to delete this Video?");
					if (decision == true) {
						var delete_id = $('#video-id-update').val();
						$.ajax({
							success: function(msg, text, xhr) {
								$('#video_sortable li#video-'+delete_id).remove();
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editVideoForm').serialize(),
							url: '/admin/film_edit/delete_video/',
							type: 'POST',
							dataType: 'html'
						});
						$(this).dialog('close');
					}
				},
<?php } ?>
				'Update Video': function() {
					if ($("#editVideoForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								var update_id = $('#video-id-update').val();
								$('#video_sortable li#video-'+update_id).replaceWith(msg);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editVideoForm').serialize(),
							url: '/admin/film_edit/update_video/',
							type: 'POST',
							dataType: 'html'
						}); 

						var update_id = $('#video-id-update').val();
						var video_url = $('#video-trailerurl-update').val();
						var video_order = $('#video-current-order').val();
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$('#video-trailerurl').val('');
			}
		});

		$("#photo_sortable").sortable({
			distance: 10, update: function(event, ui) {
				$.ajax({
					success: function(msg, text, xhr) {
						$('#ajaxFeedback').html(msg);
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $(this).sortable('serialize'),
					url: '/admin/film_edit/save_photo_sort/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
		});
		$("#photo_sortable").disableSelection();
		$("#video_sortable").sortable({
			distance: 10, update: function(event, ui) {
				$.ajax({
					success: function(msg, text, xhr) {
						$('#ajaxFeedback').html(msg)
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $(this).sortable('serialize'),
					url: '/admin/film_edit/save_video_sort/',
					type: 'POST',
					dataType: 'html'
				}); 
			 }
		});
		$("#video_sortable").disableSelection();

<?php if ($this->session->userdata('limited') == 0) { ?>
		$('.MultimediaTab').on('click','a.delete', function(e) {
			e.preventDefault();
			delete_photo($(this).data('id'), "#photo_sortable");
		});
<?php } ?>
		$('.MultimediaTab').on('click','a.edit', function(e) {
			e.preventDefault();
			open_edit_video_dialog($(this).data('id'), $(this).data('url').toString(), $(this).data('order'));
		});


		// ******************
		// Screenings Tab
		// ******************
		$('#addTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-new')); return false; });
		$('#updateTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-update')); return false; });
		$('.select-film').on('change', load_runtime);
		$('.select-film2').on('change', load_runtime);
		$('.time-int').on('change', recalculate_total_runtime);
		$('.time-int2').on('change', recalculate_total_runtime_update);
		recalculate_total_runtime();
		recalculate_total_runtime_update();
		
		$('#addFilmScreeningDialog').on('click', function() {
			add_screening_film();
			$(this).removeClass("ui-state-focus");
			return false;
		});
		$('#addFilmScreeningDialog-update').on('click', function() {
			add_screening_film_update(0);
			$(this).removeClass("ui-state-focus");
			return false;
		});
		$("#addScreeningForm").validate({
			rules: { "scrn-date-new": { required:true, daterange:['<?php print date("Y-m-d",strtotime($festival[0]->startdate)); ?>','<?php print date("Y-m-d",strtotime($festival[0]->enddate)); ?>'] }, "scrn-time-new": "required", "scrn-location-new": { required:true, min: 1 }, "scrn-film-01": { required:true, min: 1 } },
			messages: { "scrn-date-new":{ required: "Please enter a date." }, "scrn-time-new": "Please enter a time.", "scrn-location-new": "Please select a screening location.", "scrn-film-01": "Please select at least one film." }
		});
		$("#addScreening-dialog").dialog({
			autoOpen: false,
			height: 515,
			width: 855,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Screening': function() {
					if ($("#addScreeningForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#addScreeningHere').replaceWith(msg);
								var new_screening_id = $('#screening-id-new').val();
								$('#noScreenings').css('display','none');
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addScreeningForm').serialize(),
							url: '/admin/film_edit/add_screening/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$("#addScreeningForm INPUT[type='text']").each(function() { $(this).val(''); });
				$("#addScreeningForm INPUT[type='checkbox']").each(function() { $(this).prop('checked', false); });
				$("#addScreeningForm SELECT:not('#scrn-film-01') OPTION").each(function() { $(this).prop('selected', false); });
				$('#intro').prop('checked', true);
				$('#fest-trailer').prop('checked', true);
				$('#intro-length').val(5);
				$('#fest-trailer-length').val(3);
				$('#sponsor-trailer1-length').val(3);
				$('#sponsor-trailer2-length').val(3);
				$('#q-and-a-length').val(15);
				$('#total_films').val(1);
				$('#total-runtime').val( parseInt($('#intro-length').val()) + parseInt($('#fest-trailer-length').val()) );
				$('DIV.newFilmEntry').each(function() { $(this).remove(); });
				recalculate_total_runtime();
			}
		});
		$('#addScreeningButton').on('click', function() { $('#addScreening-dialog').dialog('open'); $(this).removeClass("ui-state-focus"); return false; });
		$('#scrn-date-new').datepicker();
		$('#scrn-date-update').datepicker();

		var screeningJSON = <?php print $screening_json; ?>;

		$("#editScreeningForm").validate({
			rules: { "scrn-date-update": { required:true, daterange:['<?php print date("Y-m-d",strtotime($festival[0]->startdate)); ?>','<?php print date("Y-m-d",strtotime($festival[0]->enddate)); ?>'] }, "scrn-time-update": "required", "scrn-location-update": { required:true, min: 1 } },
			messages: { "scrn-date-update":{ required: "Please enter a date." }, "scrn-time-update": "Please enter a time.", "scrn-location-update": "Please select a screening location." }
		});
		$("#editScreening-dialog").dialog({
			autoOpen: false,
			height: 515,
			width: 855,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var screening_id = $('#screening_id_update').val();
					var decision = confirm("Are you sure you really want to delete this Screening?");
					if (decision == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#scrn-'+screening_id).closest("tr").next("tr").remove();
								$('#scrn-'+screening_id).closest("tr").remove();
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editScreeningForm').serialize(),
							url: '/admin/film_edit/delete_screening/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				},
<?php } ?>
				'Update Screening': function() {
					var screening_id = $('#screening_id_update').val();
					if ($("#editScreeningForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#scrn-'+screening_id).closest("tr").next("tr").remove();
								$('#scrn-'+screening_id).closest("tr").replaceWith(msg);
							},
							complete: function(xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										screeningJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_edit/return_screening_json/<?php print $festival[0]->startdate."/".$festival[0]->enddate."/".$film[0]->movie_id; ?>',
									type: 'POST',
									dataType: 'json'
								});                             
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editScreeningForm').serialize(),
							url: '/admin/film_edit/update_screening/',
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$('DIV.newFilmEntry').each(function() { $(this).remove(); });
				$('#total_films_update').val(0);
			}
		});

		$('.ScreeningsTab').on('click','button.edit', function(e) {
			e.preventDefault();
			var screening_id = $(this).data('id').toString();
			open_edit_scrn_dialog(screening_id, screeningJSON);
			var timeoutID = window.setTimeout(function () { recalculate_total_runtime_update(); }, 2000);
			$(this).removeClass("ui-state-focus");
		});

		$('.ScreeningsTab').on('click','a.icon_cross', function(e) {
			e.preventDefault();
			toggle_value_screening($(this).data('type'), $(this).data('id'));
		});
		$('.ScreeningsTab').on('click','a.icon_tick', function(e) {
			e.preventDefault();
			toggle_value_screening($(this).data('type'), $(this).data('id'));
		});

		// ******************
		// Print Traffic Tab
		// ******************
		var ptrafficJSON = <?php print $print_traffic_json; ?>;
		$('#addInboundShipment').on('click', function() {
			$("#outbound-new").val(1);
			$(".ptAddDialog .InboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
			$(".ptAddDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");

			$('#addPrintTraffic-dialog').dialog('open');
			$(this).removeClass("ui-state-focus");
			return false;
		});
		$('#addOutboundShipment').on('click', function() {
			$("#outbound-new").val(2);
			$(".ptAddDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			$(".ptAddDialog .OutboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");

			$('#addPrintTraffic-dialog').dialog('open');
			$(this).removeClass("ui-state-focus");
			return false;
		});

		$("#outbound-new").on('change', function() {
			if ($("option:selected", this).val() == 1) {
				$(".ptAddDialog .InboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
				$(".ptAddDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			}
			else if ($("option:selected", this).val() == 2) {
				$(".ptAddDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
				$(".ptAddDialog .OutboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");

			} else if ($("option:selected", this).val() == 0) {
				$(".ptAddDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
				$(".ptAddDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			}       
		});

		$("#outbound-update").on('change', function() {
			if ($("option:selected", this).val() == 1) {
				$(".ptEditDialog .InboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
				$(".ptEditDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error");
			}
			else if ($("option:selected", this).val() == 2) {
				$(".ptEditDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error");
				$(".ptEditDialog .OutboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");

			} else if ($("option:selected", this).val() == 0) {
				$(".ptEditDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error");
				$(".ptEditDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error");
			}       
		});

		$(".InboundRow INPUT").datepicker();
		$(".OutboundRow INPUT").datepicker();

		$("#addPrintTrafficForm").validate({
			rules: { "outbound-new": { required:true, min: 1 }, "courier_id-new": { required:true, min: 1 }, "AccountNum-new": "digits", "TrackingNum-new": "digits" },
			messages: { "outbound-new": "Please choose a shipment type.", "courier_id-new": "Please choose a courier.", "AccountNum-new": "Please enter whole numbers only.", "TrackingNum-new": "Please enter whole numbers only." }
		});
		$("#addPrintTraffic-dialog").dialog({
			autoOpen: false,
			height: 480,
			width: 620,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Shipment Leg': function() {
					var festivalmovie_id = $('#festivalmovie_id-new').val();
					var shipment_type = $('#outbound-new').val();
					if ($("#addPrintTrafficForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								if (shipment_type == 1) {
									$('#add_inbound_shipment_here').parents("tr").replaceWith(msg);
								}
								if (shipment_type == 2) {
									$('#add_outbound_shipment_here').parents("tr").replaceWith(msg);
								}
							},
							complete: function(xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										ptrafficJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_edit/return_ptraffic_json/<?php print $film[0]->id; ?>',
									type: 'POST',
									dataType: 'json'
								});                             
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addPrintTrafficForm').serialize(),
							url: '/admin/film_edit/add_shipment/'+festivalmovie_id,
							type: 'POST',
							dataType: 'html'
						}); 
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$("#addPrintTrafficForm INPUT[type='text']").each(function() { $(this).val(""); });
				$("#addPrintTrafficForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
				$("#addPrintTrafficForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
				$("#addPrintTrafficForm TEXTAREA").each(function() { $(this).val(""); });
				$(".ptAddDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
				$(".ptAddDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			}
		});

		$("#editPrintTraffic-dialog").dialog({
			autoOpen: false,
			height: 480,
			width: 620,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Save Shipment Leg': function() {
					var printtraffic_id = $('#printtraffic_id-update').val();
					var shipment_type = $('#outbound-update').val();
					if ($("#editPrintTrafficForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								if (shipment_type == 1) {
									$('#ptin-'+printtraffic_id).parents("tr").next("tr").remove();
									$('#ptin-'+printtraffic_id).parents("tr").replaceWith(msg);
								}
								if (shipment_type == 2) {
									$('#ptout-'+printtraffic_id).parents("tr").next("tr").remove();
									$('#ptout-'+printtraffic_id).parents("tr").replaceWith(msg);
								}
							},
							complete: function(xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										ptrafficJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_edit/return_ptraffic_json/<?php print $film[0]->id; ?>',
									type: 'POST',
									dataType: 'json'
								});                             
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editPrintTrafficForm').serialize(),
							url: '/admin/film_edit/edit_shipment/'+printtraffic_id,
							type: 'POST',
							dataType: 'html'
						});
						$(this).dialog('close');
					}
				}
			},
			close: function() {
				$("#editPrintTrafficForm INPUT[type='text']").each(function() { $(this).val(""); });
				$("#editPrintTrafficForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
				$("#editPrintTrafficForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
				$("#editPrintTrafficForm TEXTAREA").each(function() { $(this).val(""); });
				$(".ptEditDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
				$(".ptEditDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
			}
		});

		$('#tabs-6').on('click','button.edit', function(e) {
			e.preventDefault();
			open_edit_ptraffic_dialog($(this).data('id').toString(), ptrafficJSON);
			$(this).removeClass("ui-state-focus");
		});
		$('#tabs-6').on('click','button.ship', function(e) {
			e.preventDefault();
			pt_ship_shipment($(this).data('id').toString(), $(this).data('confirmed'));
			$(this).removeClass("ui-state-focus");
		});
		$('#tabs-6').on('click','button.receive', function(e) {
			e.preventDefault();
			pt_receive_shipment($(this).data('id').toString(), $(this).data('confirmed'));
			$(this).removeClass("ui-state-focus");
		});
		$('#tabs-6').on('click','a.delete', function(e) {
			e.preventDefault();
			delete_shipment($(this).data('id'));
		});


		$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE FILM' || $(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });
		$('#Save').on('click', function() { $('#UpdateFilm').submit(); });
<?php if ($this->session->userdata('limited') == 0) { ?>
		$('#Delete').on('click', function() { delete_film(<?php print $film[0]->id.",'".addslashes(switch_title($film[0]->title_en))."'"; ?>); return false; });
<?php } ?>
	});
</script>
<?php } // end No Film Specified check ?>