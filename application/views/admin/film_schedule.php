<?php
// Build array of used video formats to display legend
$videoformat_array = convert_to_array2($videoformat);
$location_array = convert_to_array($locations);
$film_screening_array = convert_film_screenings_to_array($films);

// If less than half of the screenings have been published, show all rows.
$expanded_default = true;
$total_screenings = count($schedule);
$published_screenings = 0;
foreach ($schedule as $thisScreening) {
	if ($thisScreening->Published == 1) { $published_screenings++; }
}
if ($total_screenings > 0) {
	$published_ratio = $published_screenings / $total_screenings;
	if ($published_ratio >= 0.5) {
		$expanded_default = false;
	}
}

// Test to see if screening locations for this festival have formats assigned
$unique_formats = array();
foreach ($locations as $thisLocation) {
	$videoformats = explode(",",$thisLocation->format);
	foreach ($videoformats as $value) {
		if ($value != "") {
			$unique_formats[] = $videoformat_array[$value];
		}
	}
} $unique_formats = array_unique($unique_formats); sort($unique_formats);

// If no screening locations have assigned formats, look at formats for scheduled films
if (empty($unique_formats)) {
	$unique_formats = array();
	foreach ($schedule as $thisScreening) {
		if ($thisScreening->format_id != 0) {
			$unique_formats[] = $videoformat_array[$thisScreening->format_id];
		}
	} $unique_formats = array_unique($unique_formats); sort($unique_formats);
}

// Get list of unique sections from film list.
$unique_sections = array();
foreach ($films as $thisFilm) {
	$unique_sections[] = $thisFilm->category_id;
} $unique_sections = array_unique($unique_sections); sort($unique_sections);
//print_r($unique_sections);


print "<div><h2>".$festival[0]->year." ".$festival[0]->name." Schedule (".date("n/d",strtotime($festival[0]->startdate))." - ".date("n/d/Y",strtotime($festival[0]->enddate)).")</h2></div>";
?>

<div id="videoFormat-legend" class="ui-corner-all" title="Video Format Legend">
    <div class="legend-entry">
        <span style="color:#000000; display:inline-block; width:140px;"><b>Sections:</b></span>
    </div>
<?php

	$unique_sections2 = array();
	foreach ($unique_sections as $thisSectionId) {
		foreach ($sections as $thisSection) {
			if ($thisSection->id == $thisSectionId) {
				$unique_sections2[$thisSection->name] = $thisSection->color;
			}
		}
	} ksort($unique_sections2);

	foreach ($unique_sections2 as $key => $color) {
		print "\t\t<div class=\"legend-entry\">\n";
		print "\t\t\t<div style=\"background-color:#".$color.";\" class=\"venue_format_box\"></div> <span>".$key."</span>\n";
		print "\t\t</div>\n";
	}
?>
	<!--
	<br clear="all" />
    <div class="legend-entry">
        <span style="color:#000000; display:inline-block; width:140px;"><b>Video Formats:</b></span>
    </div>
    -->
<?php
/*
	foreach ($unique_formats as $thisFormat) {
		$color = "CCCCCC";
		foreach ($videoformat as $thisVideoFormat) { if ($thisVideoFormat->name == $thisFormat) { $color = $thisVideoFormat->color; break; } }
		print "\t\t<div class=\"legend-entry\">\n";
		print "\t\t\t<div style=\"background-color:#".$color.";\" class=\"venue_format_box\"></div> <span>".$thisFormat."</span>\n";
		print "\t\t</div>\n";
	}
*/
?>
    <br clear="all" />
	<div class="legend-entry">
        <span style="color:#000000; display:inline-block; width:140px;"><b>Published Screenings:</b></span>
    </div>
	<div class="legend-entry">
		<div class="venue_format_box" style="background-color:white;"></div> <span>Public</span>
	</div>
	<div class="legend-entry">
		<div class="venue_format_box private"></div> <span>Private</span>
	</div>
    <br clear="all" />
	<div class="legend-entry">
        <span style="color:#000000; display:inline-block; width:140px;"><b>Unpublished Screenings:</b></span>
    </div>
	<div class="legend-entry">
		<div class="venue_format_box unpublished"></div> <span>Public</span>
	</div>
	<div class="legend-entry">
		<div class="venue_format_box private unpublished"></div> <span>Private</span>
	</div>
    <br clear="all" />
</div>

<?php
print "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"schedule_header\"><thead><tr><td class=\"header\">";
print "<div style=\"width:40px;\">Date</div>";
print "<div style=\"width:60px; text-align:left;\">Location</div>";
print "<div>8:00</div>";
print "<div>8:30</div>";
print "<div>9:00</div>";
print "<div>9:30</div>";
print "<div>10:00</div>";
print "<div>10:30</div>";
print "<div>11:00</div>";
print "<div>11:30</div>";
print "<div>12:00</div>";
print "<div>12:30</div>";
print "<div>1:00</div>";
print "<div>1:30</div>";
print "<div>2:00</div>";
print "<div>2:30</div>";
print "<div>3:00</div>";
print "<div>3:30</div>";
print "<div>4:00</div>";
print "<div>4:30</div>";
print "<div>5:00</div>";
print "<div>5:30</div>";
print "<div>6:00</div>";
print "<div>6:30</div>";
print "<div>7:00</div>";
print "<div>7:30</div>";
print "<div>8:00</div>";
print "<div>8:30</div>";
print "<div>9:00</div>";
print "<div>9:30</div>";
print "<div>10:00</div>";
print "<div>10:30</div>";
print "<div>11:00</div>";
print "<div>11:30</div>";
print "</tr></thead>";
print "<tbody><tr><td colspan=\"32\">\n";
print "<div id=\"addDeleteScreening\" class=\"ui-corner-all\">\n";
print "<a href=\"javascript:void(0);\" id=\"addScreeningIcon\"><img src=\"/assets/images/icons/splash_new_orange.png\" width=\"48\" height=\"48\" border=\"0\" alt=\"DRAG ME TO THE GRID\" title=\"DRAG ME TO THE GRID\" /></a>";
if ($this->session->userdata('limited') == 0) {
	print "<a href=\"javascript:void(0);\" id=\"deleteScreeningIcon\"><img src=\"/assets/images/icons/trash_empty.png\" width=\"48\" height=\"48\" border=\"0\" alt=\"DROP SCREENINGS ON ME\" title=\"DROP SCREENINGS ON ME\" /></a>";
}
if ($expanded_default == true) {
	print "<a href=\"#\" id=\"expandCollapse\" class=\"collapse\"></a>";	
} else {
	print "<a href=\"#\" id=\"expandCollapse\" class=\"expand\"></a>";
}
print "</div>";
print "</td></tr>";
print "<tr><td colspan=\"32\">\n";

if (count($locations) == 0) {
	print "\t<div align=\"center\">Please define your screening locations on the <a href=\"/admin/film_settings/\">film settings page</a> and go to the Festivals tab to assign screening locations for your current festival.<br />This must be done before you can use this page to create your festival's schedule.</div>\n";		
} else {
	foreach ($festivaldates as $thisDate) {
		$container_screening_count = 0; $container_empty = "";
		foreach ($locations as $thisLocation) {
			foreach ($schedule as $thisScreening) {
				if ($thisDate == $thisScreening->date && $thisLocation->id == $thisScreening->location_id) { $container_screening_count++; }
			}
		}
		if ($container_screening_count == 0) {
			if ($expanded_default == true) {
				$container_empty = "empty_off";
			} else {
				$container_empty = "empty";
			}
		}

		$x = substr($thisDate,0,4).substr($thisDate,5,2).substr($thisDate,8,2);
		print "<div id=\"".$x."\" class=\"date_container ".$container_empty."\">\n";
	
		$interlock_id = 0;
		foreach ($locations as $thisLocation) {
			$screening_count = 0; $empty = "";
			foreach ($schedule as $thisScreening) {
				if ($thisDate == $thisScreening->date && $thisLocation->id == $thisScreening->location_id) {
					if ($thisScreening->location_id2 != 0) { $interlock_id = $thisScreening->location_id2; }
					$screening_count++;
				}
			}
			// make special exception and show 2nd location used in interlock
			if ($thisLocation->id != $interlock_id) {
				if ($screening_count == 0) {
					if ($expanded_default == true) {
						$empty = "empty_off";
					} else {
						$empty = "empty";
					}
				}
			} else {
				$interlock_id = 0;
			}

			$format_string = "<br />";
			/*
			if ($thisLocation->format != "") {
				foreach (explode(",",$thisLocation->format) as $thisFormat) {
					foreach ($videoformat as $thisFormatInfo) {
						if ($thisFormat == $thisFormatInfo->id) {
							$format_string .= "<div class=\"venue_format_box\" style=\"background-color:#".$thisFormatInfo->color."\"></div>";
						}
					}
				}
			}
			*/
			print "\t<div id=\"".$x."-".$thisLocation->id."\" class=\"location_row ".$empty."\">\n";
			print "\t\t<div id=\"venue-".$thisLocation->id."-date\" class=\"vc_date\">".date("M d",strtotime($thisDate))."</div>\n";
			print "\t\t<div id=\"venue-".$thisLocation->id."-name\" class=\"vc_name\">".$thisLocation->name.$format_string."</div>\n";
	
			$c = 1;
			for ($y=8; $y<=23; $y++) {
				if ($y == 8) { $z = "0"; } else { $z = ""; }
				$timestamp1 = strtotime($thisDate." ".$z.$y.":00:00 -1000");
				$timestamp2 = strtotime($thisDate." ".$z.$y.":15:00 -1000");
				$timestamp3 = strtotime($thisDate." ".$z.$y.":30:00 -1000");
				$timestamp4 = strtotime($thisDate." ".$z.$y.":45:00 -1000");
			
				print "\t\t<div id=\"d-".$thisLocation->id."-".$timestamp1."-".$c."\" title=\"".date("M d",strtotime($thisDate))." at ".date("g:ia",strtotime($y.":00:00"))." - ".$thisLocation->name."\" class=\"vc_1 vc\"></div>\n";
				print "\t\t<div id=\"d-".$thisLocation->id."-".$timestamp2."-".$c."\" title=\"".date("M d",strtotime($thisDate))." at ".date("g:ia",strtotime($y.":15:00"))." - ".$thisLocation->name."\" class=\"vc_2 vc\"></div>\n";
				print "\t\t<div id=\"d-".$thisLocation->id."-".$timestamp3."-".$c."\" title=\"".date("M d",strtotime($thisDate))." at ".date("g:ia",strtotime($y.":30:00"))." - ".$thisLocation->name."\" class=\"vc_3 vc\"></div>\n";
	
				if ($y == 23) {
					print "\t\t<div id=\"d-".$thisLocation->id."-".$timestamp4."-".$c."\" title=\"".date("M d",strtotime($thisDate))." at ".date("g:ia",strtotime($y.":45:00"))." - ".$thisLocation->name."\" class=\"vc_4 vc last_cell\"></div>\n";
				} else {
					print "\t\t<div id=\"d-".$thisLocation->id."-".$timestamp4."-".$c."\" title=\"".date("M d",strtotime($thisDate))." at ".date("g:ia",strtotime($y.":45:00"))." - ".$thisLocation->name."\" class=\"vc_4 vc\"></div>\n";
				}
				
			}
	
			foreach ($schedule as $thisScreening) {
				if ($thisDate == $thisScreening->date && $thisLocation->id == $thisScreening->location_id) {
					$title = switch_title($thisScreening->title_en);
					$introducer = $thisScreening->host;
	
					$startTime = strtotime($thisScreening->date." ".$thisScreening->time);
					$screeningTime = strtotime($thisScreening->date." 08:00:00");
					$startTimeOffset = 97 + abs( ($startTime - $screeningTime) / 60 );
					
					$length = 0;
					$film_ids = explode(",",$thisScreening->program_movie_ids);
					foreach ($film_ids as $thisFilmID) {
						foreach ($films as $thisFilm) {
							if ($thisFilmID == $thisFilm->movie_id) { $length = $length + $thisFilm->runtime_int; }
						}
					}
					if ($length == 0) { $length = 90; }
					
					if ($thisScreening->intro == 1) { $length += $thisScreening->intro_length; }
					if ($thisScreening->fest_trailer == 1) { $length += $thisScreening->fest_trailer_length; }
					if ($thisScreening->sponsor_trailer1 == 1) { $length += $thisScreening->sponsor_trailer1_length; }
					if ($thisScreening->sponsor_trailer2 == 1) { $length += $thisScreening->sponsor_trailer2_length; }
					if ($thisScreening->q_and_a == 1) { $length += $thisScreening->q_and_a_length; }
					$endTime = $startTime + intval($length*60);

					print "\t\t<div id=\"screening-".$thisScreening->id."\" class=\"floating ui-corner-all";
					if ($thisScreening->Published == 0) {
						print " unpublished";
					}
					if ($thisScreening->Private == 1) {
						print " private";
					}
					if ($thisScreening->location_id2 != 0) {
						print " interlock";
					}
					print "\" style=\"width:".$length."px; left:".$startTimeOffset."px\">\n";
	
					$handle_color = "999999"; $format_name = "";
					foreach ($sections as $thisSection) {
						if ($thisScreening->category_id == $thisSection->id) {
							$handle_color = $thisSection->color;
							$format_name = $thisSection->name;
							break 1;
						}
					}

					/*
					$handle_color = "999999"; $format_name = "";
					foreach ($videoformat as $thisFormatInfo) {
						if ($thisScreening->format_id == $thisFormatInfo->id) {
							$handle_color = $thisFormatInfo->color;
							$format_name = $thisFormatInfo->name;
							break 1;
						}
					}
					*/

					if ($thisScreening->q_and_a == 1) {
						print "\t\t\t<div class=\"qa\"></div>\n";
					}

					foreach ($outbound as $outShipments) {
						if ($outShipments->festivalmovie_id == $thisScreening->festivalmovie_id) {
							if ($outShipments->outbound == 1 && $outShipments->confirmed == 1) {
								print "\t\t\t<div class=\"out\"></div>\n"; break 1;
							}
						}
					}
					foreach ($inbound as $inShipments) {
						if ($inShipments->festivalmovie_id == $thisScreening->festivalmovie_id) {
							if ($inShipments->outbound == 0 && $inShipments->confirmed == 1) {
								print "\t\t\t<div class=\"in\"></div>\n"; break 1;
							}
						}
					}

					print "\t\t\t<div class=\"h\" style=\"background-color:#".$handle_color."\"></div>\n";
					print "\t\t\t<p class=\"information\"><a href=\"/admin/film_edit/update/".$thisScreening->slug."#tabs-4\">";
					if ($thisScreening->program_name != "") {
						print substr($thisScreening->program_name,0,16);
						if (strlen($thisScreening->program_name) > 16 ) { print "&hellip;"; }
					} else {
						print substr($title,0,16);
						if (strlen($title) > 16 ) { print "&hellip;"; }
					}
					print "</a></p>";
					print "<p class=\"information\"><span class=\"time\">".date("g:i",$startTime)." - ".date("g:i",$endTime)."</span></p>\n";
					if ($format_name != "") {
						print "<p class=\"information\">".$format_name."</p>";
					}
					if ($introducer != "") {
						print "<p class=\"information\">".$introducer."</p>";
					}
					print "\t\t</div>\n";
				}
			}
			print "<div id=\"addScreeningHere-".$thisLocation->id."-".$x."\"><input type=\"hidden\" id=\"newid-".$thisLocation->id."-".$x."\" name=\"newid-".$thisLocation->id."-".$x."\" value=\"0\"></div>\n";
			print "<br clear=\"all\">\n";
			print "</div>\n"; // ending location_row
		}		
		print "</div>\n";
		print "<div class=\"page-break\"></div>\n";
	}
}
print "</td></tr>\n";
print "</tbody></table>";
?>

<div id="addScreening-dialog" title="Add Screening">
<form name="addScreeningForm" id="addScreeningForm">
	<input type="hidden" name="total_films" id="total_films" value="1" />
	<input type="hidden" name="total-runtime" id="total-runtime" value="0" />
	<input type="hidden" id="divToUpdate" name="divToUpdate" value="" />
    <table class="ScreeningDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td colspan="2">
				<h3>Screening Details</h3>
            </td>
            <td>
				<h3>Program / Notes</h3>
            </td>
            <td>
				<h3>Screening Breakdown</h3>
            </td>
        </tr>
        <tr valign="top">
            <td width="17%">
				<label for="scrn-published-new">Published:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-published-new', 'id'=>'scrn-published-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-rush-new">Standby:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-rush-new', 'id'=>'scrn-rush-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;
            </td>
            <td width="17%">
				<label for="scrn-private-new">Private:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-private-new', 'id'=>'scrn-private-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-free-new">Free:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-free-new', 'id'=>'scrn-free-new', 'value'=>'1', 'checked'=>false)); ?>
            </td>
            <td width="33%">
				<label for="scrn-program-name-new">Program Name:</label><br />
				<?php print form_input(array('name'=>'scrn-program-name-new', 'id'=>'scrn-program-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td width="33%" rowspan="7">
				<fieldset class="ui-corner-all">
			    <table class="addScreeningBreakdown" width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tbody style="border-top:none;">
			        <tr valign="top">
                    	<td width="65%"><label>Film Introduction</label></td>
                    	<td width="25%"><input name="intro-length" id="intro-length" type="text" size="1" value="5" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td width="10%"><input name="intro" id="intro" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Festival Trailer</label></td>
                    	<td><input name="fest-trailer-length" id="fest-trailer-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="fest-trailer" id="fest-trailer" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer1-name" id="sponsor-trailer1-name" type="text" size="1" value="Sponsor Trailer 1" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer1-length" id="sponsor-trailer1-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer1" id="sponsor-trailer1" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer2-name" id="sponsor-trailer2-name" type="text" size="1" value="Sponsor Trailer 2" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer2-length" id="sponsor-trailer2-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer2" id="sponsor-trailer2" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Film 1</label> <span class="req">*</span></td>
                    	<td colspan="2"><div id="scrn-film-01-length"><?php print "0" ?></span> min</td>
                    </tr>
			        <tr valign="top">
                    	<td colspan="3"><?php print form_dropdown_film('scrn-film-01', $film_screening_array, 0, "id='scrn-film-01' class='select-film ui-widget-content ui-corner-all'"); ?></td>
                    </tr>
                    <tr valign="top">
                    	<td colspan="3">                       
                        <div id="addNewFilmsHere"></div>
                        
                        <div align="center"><button id="addFilmScreeningDialog">Add Another Film</button></div>
                        </td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Q&amp;A Session</label></td>
                    	<td><input name="q-and-a-length" id="q-and-a-length" type="text" size="2" value="15" class="time-int ui-widget-content ui-corner-all"> min</td>
                        <td><input name="q-and-a" id="q-and-a" type="checkbox" value="1" class="time-int" /></td>
                    </tr>
                    </tbody>
                </table>
                <div id="scrn-runtime-1">Program Runtime:</div><div id="scrn-runtime-2"><?php print calculate_runtime(0); ?></div>
				</fieldset>				
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="scrn-date-new">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'scrn-date-new', 'id'=>'scrn-date-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="scrn-time-new">Time:</label> <span class="req">*</span> like: '7:00 PM'<br />
				<?php print form_input(array('name'=>'scrn-time-new', 'id'=>'scrn-time-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td rowspan="2">
                <label for="scrn-program-desc-new">Program Description:</label><br />
                <?php print form_textarea(array('name'=>'scrn-program-desc-new', 'id'=>'scrn-program-desc-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
            <td colspan="2">
				<label for="scrn-location-new">Screening Location:</label> <span class="req">*</span><br />
                <?php print form_dropdown('scrn-location-new', $location_array, 0,"id='scrn-location-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
		<tr valign="top">
            <td colspan="2">
				<label for="scrn-location2-new">Interlock? (2nd Location):</label><br />
                <?php print form_dropdown('scrn-location2-new', $location_array, 0,"id='scrn-location2-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td rowspan="4">
                <label for="scrn-notes-new">Screening Notes (private):</label><br />
                <?php print form_textarea(array('name'=>'scrn-notes-new', 'id'=>'scrn-notes-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-ticket-new">Ticket Link:</label> <a href="#" id="addTicketLink" style="float:right; padding-right:5px;">Add tix.com link</a><br />
				<?php print form_input(array('name'=>'scrn-ticket-new', 'id'=>'scrn-ticket-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-host-new">Host / Introducer:</label><br />
				<?php print form_input(array('name'=>'scrn-host-new', 'id'=>'scrn-host-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="audience-count-new">Audience Count:</label><br />
				<?php print form_input(array('name'=>'audience-count-new', 'id'=>'audience-count-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="screening-revenue-new">Screening Revenue:</label><br />
				$ <?php print form_input(array('name'=>'screening-revenue-new', 'id'=>'screening-revenue-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<script type="text/javascript">
	$(function() {

		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		$('#addTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-new')); return false; });
		$('#updateTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-update')); return false; });

		$('.select-film, .select-film2').each(function() {
			$(this).on('change', load_runtime);
		});
		$('.time-int').on('change', recalculate_total_runtime);
		$('.time-int2').on('change', recalculate_total_runtime_update);

		recalculate_total_runtime();
		recalculate_total_runtime_update();

		jQuery("#schedule_header").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});

		// Each date container is droppable, which activates the droppables within it upon dragging over it.
		$('.date_container').droppable({ create:sch_container_init, over:sch_container_enable, tolerance:'pointer', addClasses:false, delay:0 });

		$('#addScreeningIcon').draggable({delay:0, distance:5, zIndex:2000,
			revert:function(is_valid_drop) {
				if (is_valid_drop === false) {
					$('.date_container .vc').droppable('disable');
				}
				// always revert
				return true;
			}
		});
<?php if ($this->session->userdata('limited') == 0) { ?>
		$('#deleteScreeningIcon').droppable({drop:sch_drop_delete, tolerance:'touch'});
<?php } ?>
		$('#schedule_header DIV.floating').each(function(index, element) {
			$(this).draggable({handle:'.h', zIndex:100,
				revert:function(dropObject) {
					if (dropObject === false) {
						// revert only on invalid drop
						$('.date_container .vc').droppable('disable');
						return true;
					} else {
						// do not revert
						return false;
					}
				}
			});
		});

		// validate add screening form on keyup and submit
		$("#addScreeningForm").validate({
			rules: { "scrn-date-new": { required:true, daterange:['<?php print date("Y-m-d",strtotime($festival[0]->startdate)); ?>','<?php print date("Y-m-d",strtotime($festival[0]->enddate)); ?>'] }, "scrn-time-new": "required", "scrn-location-new": { required:true, min: 1 }, "scrn-film-01": { required:true, min: 1 } },
			messages: { "scrn-date-new":{ required: "Please enter a date." }, "scrn-time-new": "Please enter a time.", "scrn-location-new": "Please select a screening location.", "scrn-film-01": "Please select at least one film." }
		});

		$('#addFilmScreeningDialog').on('click', function() { add_screening_film(); $(this).removeClass("ui-state-focus"); return false; });
		$("#addScreening-dialog").dialog({
			autoOpen: false,
			height: 515,
			width: 855,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Screening': function() {
					var toUpdate = $('#divToUpdate').val();

					if ($("#addScreeningForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#addScreeningHere-'+toUpdate).replaceWith(msg);
								var new_screening_id = $('#newid-'+toUpdate).val();
								$('#screening-'+new_screening_id).draggable({handle:'.h', zIndex:100,
									revert:function(dropObject) {
										if (dropObject === false) {
											// revert only on invalid drop
											$('.date_container .vc').droppable('disable');
											return true;
										} else {
											// do not revert
											return false;
										}
									}
								});
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addScreeningForm').serialize(),
							url: '/admin/film_schedule/add_screening/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
		            }
				}
			},
			close: function() {
				$("#addScreeningForm INPUT[type='text']").each(function() { $(this).val(''); });
				$("#addScreeningForm INPUT[type='checkbox']").each(function() { $(this).prop('checked', false); });
				$("#addScreeningForm SELECT OPTION").each(function() { $(this).prop('selected', false); });
				$('#intro').prop('checked', true);
				$('#fest-trailer').prop('checked', true);
				$('#intro-length').val(5);
				$('#fest-trailer-length').val(3);
				$('#sponsor-trailer1-length').val(3);
				$('#sponsor-trailer2-length').val(3);
				$('#q-and-a-length').val(15);
				$('#total_films').val(1);
				$('#scrn-film-01').val(0);
				$('#scrn-film-01-length').html('0 min');
				$('#total-runtime').val( parseInt($('#intro-length').val()) + parseInt($('#fest-trailer-length').val()) );
				$('DIV.newFilmEntry').each(function() { $(this).remove(); });
				recalculate_total_runtime();
			}
		});
		$('#scrn-date-new').datepicker();

		$('#expandCollapse').on("click", function() {
			if ($(this).hasClass('expand') == true) {
				$('#schedule_header').find(".empty").addClass('empty_off').removeClass('empty');	
				$(this).removeClass('expand').addClass('collapse');
			} else if ($(this).hasClass('collapse') == true) {
				$('#schedule_header').find(".empty_off").addClass('empty').removeClass('empty_off');
				$(this).removeClass('collapse').addClass('expand');
			}
			return false;
		});
	});
</script>