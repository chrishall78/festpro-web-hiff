<div id="festival_nav">
	<div class="nav_topbar">
    	<a href="/login/logout/"><strong>Hello <?php print $this->session->userdata('username'); ?>.</strong> Logout?</a>
		<a href="/admin/user_account/"<?php if ($selected_page=="user_account") {print" class=\"bold\"";} ?>>My Account</a>
<?php if ($this->session->userdata('admin') == 1) { ?>
		<a href="/admin/user_manage/"<?php if ($selected_page=="user_manage") {print" class=\"bold\"";} ?>>Manage Users</a>
<?php } ?>
		<a href="/" target="_blank">View Program Guide</a>
    </div>
<?php
	$festival_array = convert_festival_to_array($festivals);

	print "<div id=\"changeFest\">\n";
	print form_open('admin/user_account/change_festival/', array("name"=>"changeFestForm","id"=>"changeFestForm"))."\n";
	print form_dropdown('changeFestival', $festival_array, $this->session->userdata('festival'), "id='changeFestival' class='select ui-widget-content ui-corner-all'")."\n";
	print "<button id=\"changeButton\">Change</button>\n";
	print form_close()."\n";
	print "</div>\n";	

	if (substr($selected_page,0,4) == "film") {
		print "\t\t<div class=\"nav_header_active\"><a href=\"/admin/film_listing/\">Programming Suite</a></div>\n";
		print "\t\t<div class=\"nav_header\"><a href=\"/admin/guest_listing/\">Guests Suite</a></div>\n";
	} elseif (substr($selected_page,0,5) == "guest") {
		print "\t\t<div class=\"nav_header\"><a href=\"/admin/film_listing/\">Programming Suite</a></div>\n";
		print "\t\t<div class=\"nav_header_active\"><a href=\"/admin/guest_listing/\">Guests Suite</a></div>\n";
	} elseif (substr($selected_page,0,4) == "user") {
		print "\t\t<div class=\"nav_header\"><a href=\"/admin/film_listing/\">Programming Suite</a></div>\n";
		print "\t\t<div class=\"nav_header\"><a href=\"/admin/guest_listing/\">Guests Suite</a></div>\n";
	}
?>
    <div class="nav_header2"><span>Powered by:</span> <a href="http://www.festpro.com/"><img src="/assets/images/FestPro-Small.jpg" height="20" width="113" border="0" alt="FESTPRO" title="FESTPRO" /></a></div>
	<br clear="all" />
<?php if (substr($selected_page,0,4) == "film") { ?>
	<div class="nav_subheader clearfix">
		<a href="/admin/film_dashboard/"<?php if ($selected_page=="film_dashboard") {print" class=\"bold\"";} ?>>Dashboard</a>
		<a href="/admin/film_listing/"<?php if ($selected_page=="film_listing" || $selected_page=="film_edit") {print" class=\"bold\"";} ?>>Films</a>
		<a href="/admin/film_schedule/"<?php if ($selected_page=="film_schedule") {print" class=\"bold\"";} ?>>Grid Schedule</a>
		<a href="/admin/film_schedule_list/"<?php if ($selected_page=="film_schedule_list") {print" class=\"bold\"";} ?>>List Schedule</a>
		<a href="/admin/film_review/"<?php if ($selected_page=="film_review") {print" class=\"bold\"";} ?>>Film Review</a>
		<a href="/admin/film_printtraffic/"<?php if ($selected_page=="film_printtraffic") {print" class=\"bold\"";} ?>>Print Traffic</a>
		<a href="/admin/film_updates/"<?php if ($selected_page=="film_updates") {print" class=\"bold\"";} ?>>Program Updates</a>
		<?php if ($this->session->userdata('limited') == 0) { ?>
			<a href="/admin/film_settings/"<?php if ($selected_page=="film_settings") {print" class=\"bold\"";} ?>>Settings</a>
		<?php } ?>
		<a href="/admin/film_reports/"<?php if ($selected_page=="film_reports") {print" class=\"bold\"";} ?>>Reports</a>
		<!-- <a href="/admin/film_eventivalsync/"<?php if ($selected_page=="film_eventivalsync") {print" class=\"bold\"";} ?>>Eventival Sync</a> -->
    </div>
<?php } elseif (substr($selected_page,0,5) == "guest") { ?>
	<div class="nav_subheader clearfix">
		<a href="/admin/guest_listing/"<?php if ($selected_page=="guest_listing") {print" class=\"bold\"";} ?>>Guest Listing</a>
		<a href="/admin/guest_flights_hotels/"<?php if ($selected_page=="guest_flights_hotels") {print" class=\"bold\"";} ?>>Flights/Hotels</a>
		<!-- <a href="/admin/guest_transportation/"<?php //if ($selected_page=="guest_transportation") {print" class=\"bold\"";} ?>>Transportation</a> -->
		<a href="/admin/guest_review/"<?php if ($selected_page=="guest_review") {print" class=\"bold\"";} ?>>Guest Review</a>
		<?php if ($this->session->userdata('limited') == 0) { ?>
			<a href="/admin/guest_settings/"<?php if ($selected_page=="guest_settings") {print" class=\"bold\"";} ?>>Guest Settings</a>
		<?php } ?>
		<a href="/admin/guest_reports/"<?php if ($selected_page=="guest_reports") {print" class=\"bold\"";} ?>>Reports</a>
    </div>
<?php } elseif (substr($selected_page,0,4) == "user") { ?>
	<div class="nav_subheader clearfix">
<?php if ($this->session->userdata('admin') == 1) { ?>
		<a href="/admin/user_manage/"<?php if ($selected_page=="user_manage") {print" class=\"bold\"";} ?>>Manage Users</a>
<?php } ?>
		<a href="/admin/user_account/"<?php if ($selected_page=="user_account") {print" class=\"bold\"";} ?>>My Account</a>
    </div>
<?php } ?>

</div>
