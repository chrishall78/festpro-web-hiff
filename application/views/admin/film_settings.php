<?php
	// Film Details Tab
	$countries_array = convert_to_array($countries);
	$languages_array = convert_to_array($languages);
	$genres_array = convert_to_array($genres);

	$aspectratio_array = convert_to_array($aspectratio);
	$color_array = convert_to_array($color);
	$distribution_array = convert_to_array($distribution);

	$eventtypes_array = convert_to_array($eventtypes);
	$courier_array = convert_to_array($courier);
	$personnel_array = convert_to_array($personnel);
	$personnel_order_array = convert_to_array($personnel_order);

	$premiere_array = convert_to_array($premiere);
	$soundformat_array = convert_to_array($soundformat);

	// Sections Tab
	$sections_array = convert_to_array($sections);
	$festivals_array = convert_festival_to_array($festivals);
	$sponsors_array = convert_to_array($sponsors);  
	$sections_json = json_encode($sections);

	// Sponsors Tab
	$films_array = convert_films_to_array3($films);
	$sponsorlogo_json = json_encode($sponsor_logos);
	$sponsorlogo_array = convert_to_array($sponsorlogo);
	$sponsortype_json = json_encode($sponsorlogo);

	// Screening Locations Tab
	$videoformat_array = convert_to_array2($videoformat);
	$locations_array = convert_to_array($locations);
	$locations_json = json_encode($locations);
	$venues_array = convert_to_array($venues);
	$venues_json = json_encode($venues);

	// Festivals Tab
	$festivals_array2 = convert_festival_to_array($festivals);
	$locations_array2 = convert_to_array2($locations);
	$festivals_array3 = convert_festival_to_array2($festivals);
	$festivals_json = json_encode($festivals);

	// Competitions Tab
	if (count($competitions) > 0) {
		$competition_array = convert_obj_to_array($competitions, "id", array("festival_year","festival_name","name"), " ", "--");
	} else {
		$competition_array = array();
	}
	$films_array2 = convert_films_to_array($films);

?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Film Details</a></li>
		<li><a href="#tabs-2">Sections</a></li>
		<li><a href="#tabs-3">Sponsors</a></li>
		<li><a href="#tabs-4">Screening Locations / Venues</a></li>
		<li><a href="#tabs-5">Festivals</a></li>
		<li><a href="#tabs-6">Competitions</a></li>
	</ul>

	<div id="tabs-1">
<?php
	// Film Details Tab
?>
	<fieldset class="ui-corner-all">
	<table class="FilmDetailsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="32%" colspan="2">
			<?php print_type_AUD($countries_array, "Countries", "country"); ?>
			</td>
			<td width="32%" colspan="2">
			<?php print_type_AUD($languages_array, "Languages", "language"); ?>
			</td>
			<td width="36%" colspan="2">
			<?php print_type_AUD($genres_array, "Genres", "genre"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p>&nbsp;</p></td>
		</tr>
		<tr valign="top">
			<td colspan="2">
			<?php print_type_AUD($aspectratio_array, "Aspect Ratio", "aspectratio"); ?>
			</td>
			<td colspan="2">
			<?php print_type_AUD($color_array, "Color", "color"); ?>
			</td>
			<td colspan="2">
			<?php print_type_AUD($distribution_array, "Distribution Types", "distribution"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p>&nbsp;</p></td>
		</tr>
		<tr valign="top">
			<td colspan="2">
			<?php print_type_AUD($eventtypes_array, "Event Types", "event"); ?>
			</td>
			<td colspan="2">
			<?php print_type_AUD($courier_array, "Package Couriers", "courier"); ?>
			</td>
			<td colspan="2">
			<?php print_type_AUD($personnel_array, "Personnel Roles", "personnel"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p>&nbsp;</p></td>
		</tr>
		<tr valign="top">
			<td colspan="2">
			<?php print_type_AUD($premiere_array, "Premiere Status", "premiere"); ?>
			</td>
			<td colspan="2">
			<?php print_type_AUD($soundformat_array, "Sound Format", "sound"); ?>
			</td>
			<td colspan="2">
			<?php print_type_video_AUD($videoformat, "Video Format", "format"); ?>
			</td>
		</tr>
		</tbody>
	</table><br />
	</fieldset>    
	
	</div>

	<div id="tabs-2">
<?php
	// Sections Tab
?>
	<fieldset class="ui-corner-all">
	<form id="addSection" name="addSection">
	<table class="SectionsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="4"><h3>Add a Section</h3></td>
		</tr>
		<tr valign="top">
			<td width="25%"><label for="section-new">Section Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'section-new', 'id'=>'section-new', 'class'=>'text ui-widget-content ui-corner-all'));
				print "<div class=\"customWidget addColor\"><div id=\"colorSelectorSection\"><div style=\"background-color: rgb(0, 0, 0);\"></div></div></div>";
				print "<input type=\"hidden\" value=\"000000\" id=\"section-color\" name=\"section-color\">&nbsp;&nbsp;";
			?>
			</td>
			<td width="25%"><label for="sec-festival-new">Applies to</label><span class="req"> *</span><br />
			<?php print form_dropdown('sec-festival-new', $festivals_array, 0, "id='sec-festival-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="40%" rowspan="2"><label for="description-new">Description</label><br />
			<?php print form_textarea(array('name'=>'description-new', 'id'=>'description-new', 'value'=>'', 'rows'=>'4', 'cols'=>'57', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="10%" rowspan="2"><br />
			<?php print form_submit('section-add', 'Add'); ?>
			</td>
		</tr>
		</tbody>
	</table>
	</form>

	<form id="updateSection" name="updateSection">
	<table class="SectionsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="4"><h3>Edit/Delete a Section</h3></td>
		</tr>

		<tr>
			<td colspan="4"><label for="section">Current Section List</label><span class="req"> *</span><br />
			<?php
				print form_dropdown('section', $sections_array, 0, "id='section' class='select-section ui-widget-content ui-corner-all'");
				print "&nbsp;&nbsp;&nbsp;<a href=\"#\" id=\"section-delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected Section\" title=\"Delete selected Section\" width=\"16\" height=\"16\" border=\"0\"></a>\n<br />";
			?>
			</td>
		</tr>
		<tr valign="top">
			<td width="25%"><label for="section-current">Section Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'section-current', 'id'=>'section-current', 'class'=>'text ui-widget-content ui-corner-all'));
				print "<div class=\"customWidget updateColor\"><div id=\"colorSelector2Section\"><div style=\"background-color: rgb(0, 0, 0);\"></div></div></div>";
				print "<input type=\"hidden\" value=\"000000\" id=\"section-color-update\" name=\"section-color-update\">&nbsp;&nbsp;";
			?>
			</td>
			<td width="25%"><label for="sec-festival-current">Applies to</label><span class="req"> *</span><br />
			<?php print form_dropdown('sec-festival-current', $festivals_array, 0, "id='sec-festival-current' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="40%" rowspan="2"><label for="description-current">Description</label><br />
			<?php print form_textarea(array('name'=>'description-current', 'id'=>'description-current', 'value'=>'', 'rows'=>'4', 'cols'=>'57', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="10%" rowspan="2"><br />
			<?php print form_submit('section-update', 'Update'); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><!-- <label for="sec-sponsor-current">Assigned Sponsor</label> --><br />
			<?php //print form_dropdown('sec-sponsor-current', $sponsors_array, 0, "id='sec-sponsor-current' class='select-wide ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>

		</tbody>
	</table>
	</form><br />
	</fieldset>    

	</div>

	<div id="tabs-3">
<?php
	// Sponsors Tab
?>
	<fieldset class="ui-corner-all">
	<table class="SponsorsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="50%"><h3>Add a Sponsor</h3></td>
			<td width="50%" rowspan="4">
			<?php
			print_type_splogo_AUD($sponsorlogo, "Sponsor Type", "sponsortype");
			 ?>
			</td>
		</tr>
		<tr valign="top">
			<td>
			<form id="addSponsor" name="addSponsor">
			<label for="sponsor-new">Sponsor Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'sponsor-new', 'id'=>'sponsor-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			&nbsp;
			<?php print form_submit(array('name'=>'sponsor-add', 'id'=>'sponsor-add', 'value'=>'Add')); ?>
			</form>
			</td>
		</tr>
		<tr>
			<td><h3>Edit a Sponsor</h3></td>
		</tr>
		<tr>
			<td>
			<form id="updateSponsor" name="updateSponsor">
			<label for="sponsor">Current Sponsor List</label><span class="req"> *</span><br />
			<?php
				print form_dropdown('sponsor', $sponsors_array, 0, "id='sponsor' class='select ui-widget-content ui-corner-all'");
				//print "&nbsp;<a href=\"#\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected Section\" title=\"Delete selected Section\" width=\"16\" height=\"16\" border=\"0\"></a>\n<br />";
			?>
			<br />
			<label for="sponsor-current">Sponsor Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'sponsor-current', 'id'=>'sponsor-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			&nbsp;
			<?php print form_submit(array('name'=>'sponsor-update', 'id'=>'sponsor-update', 'value'=>'Update')); ?>
			</form>
			</td>
		</tr>
		</tbody>
	</table>
 
	<br clear="all" />
	
	<form id="addSponsorLogo" name="addSponsorLogo" method="post" action="/admin/film_settings/add_sponsor_logo/" enctype="multipart/form-data">
	<input type="hidden" name="festival-id" id="festival-id" value="<?php print $festival[0]->id; ?>" />
	<table class="SponsorsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="4"><h3>Add Sponsor Logos &amp; Links</h3></td>
		</tr>
		<tr valign="top">
			<td width="45%"><label for="sponsor-logo">Select a Sponsor</label><span class="req"> *</span><br />
			<?php print form_dropdown('sponsor-logo', $sponsors_array, 0, "id='sponsor-logo' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="45%" rowspan="4"><label for="sponsored-films">Sponsored Film(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('sponsored-films-new[]', $films_array, 0, "id='sponsored-films-new' class='select-films ui-widget-content ui-corner-all'"); ?>
			<br /><span>**Hold CTRL or COMMAND to select multiple films</span>
			</td>
			<td width="10%" rowspan="4"><br />
			<?php print form_submit(array('name'=>'sponsor-logo-add', 'id'=>'sponsor-logo-add', 'value'=>'Add')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="logofile-new">Sponsor Logo</label><br />
			<?php print form_upload(array('name'=>'logofile-new', 'id'=>'logofile-new', 'size'=>'35', 'class'=>'text-file ui-corner-all')); ?>
			<br>(Only .GIF, .JPG, .PNG files are supported. Please limit image resolution to less than 3000x3000.)
			</td>
		</tr>
		<tr valign="top">
			<td><label for="sponsor-logo">Sponsor Type</label><span class="req"> *</span><br />
			<?php print form_dropdown('sponsorlogo-type-new', $sponsorlogo_array, 0, "id='sponsorlogo-type-new' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="websitelink-new">Website Link</label> (Enter a full link like: 'http://www.somedomain.com/')<br />
			<?php print form_input(array('name'=>'websitelink-new', 'id'=>'websitelink-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>
	</form><br />

	<table id="EditSponsorLogos" class="SponsorsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="5"><h3>Edit Sponsor Logos &amp; Links</h3></td>
		</tr>
		<tr>
			<th width="10%">&nbsp;</th>
			<th width="28%">Sponsor Name &amp; Link</th>
			<th width="10%">Sponsor Type</th>
			<th width="28%">Films Sponsored</th>
			<th width="14%">Sponsor Logo</th>
		</tr>
			
<?php
		$sponsored_films = array();
		$sponsored_films_count = array();
		foreach ($sponsor_logos as $thisLogo) {
			if (!isset($sponsored_films[$thisLogo->sponsor_id])) { $sponsored_films[$thisLogo->sponsor_id] = "<a href=\"/films/detail/".$thisLogo->slug."\" target=\"_blank\">".$thisLogo->title_en."</a>";}
			else { $sponsored_films[$thisLogo->sponsor_id] .= "<br/><a href=\"/films/detail/".$thisLogo->slug."\" target=\"_blank\">".$thisLogo->title_en."</a>"; }
			if (!isset($sponsored_films_count[$thisLogo->sponsor_id])) { $sponsored_films_count[$thisLogo->sponsor_id] = 1; }
			else { $sponsored_films_count[$thisLogo->sponsor_id]++; }
		}

		$SponsorLogoButtons = "";
		
		$previous_id = 0; $width = 120;
		if (count($sponsor_logos) == 0) {
			print "\t\t<tr valign=\"top\"><td colspan=\"4\" align=\"center\">No sponsor logos &amp; links have been added yet.</td></tr>";
		} else {
			foreach ($sponsor_logos as $thisLogo) {
				if ($thisLogo->sponsor_id != $previous_id) {
					print "<tr valign=\"top\">";
					print "<td align=\"center\">";
					print "<button class=\"update\" data-id=\"".$thisLogo->sponsor_id."\" id=\"logo-".$thisLogo->sponsor_id."\">Update</button><br />";
					print "</td>";
					if ($thisLogo->url_website != "") {
						print "<td><a href=\"".$thisLogo->url_website."\" target=\"_blank\">".$thisLogo->name."</a>";
					} else {
						print "<td>".$thisLogo->name;
					}
					if ($sponsored_films_count[$thisLogo->sponsor_id] != "") {
						print "<br />".$sponsored_films_count[$thisLogo->sponsor_id]." films/events";
					}
					print "</td>";
					print "<td>".$thisLogo->sponsorlogo_type."<br /></td>";
					if ($sponsored_films[$thisLogo->sponsor_id] != "") {
						print "<td>".$sponsored_films[$thisLogo->sponsor_id]."</td>";
					} else {
						print "<td>No Films Sponsored?</td>";               
					}
					if ($thisLogo->url_logo != "") {
						if ($thisLogo->url_website != "") {
							print "<td><a href=\"".$thisLogo->url_website."\"><img src=\"".$thisLogo->url_logo."\" height=\"\" width=\"".$width."\" border=\"0\" /></a></td>";
						} else {
							print "<td><img src=\"".$thisLogo->url_logo."\" height=\"\" width=\"".$width."\" /></td>";
						}
					} else {
						print "<td>No Logo Provided</td>";
					}
					print "</tr>";
					$previous_id = $thisLogo->sponsor_id;
					$SponsorLogoButtons .= "$('#logo-".$thisLogo->sponsor_id."').on('click', function(){ open_edit_logo_dialog(\"".$thisLogo->sponsor_id."\", sponsorLogoJSON); return false; });\n";
				}
			}
		}
?>


		</tbody>
	</table><br />
	</fieldset>    
	</div>

	<div id="tabs-4">
<?php
	// Screening Locations / Venues Tab
?>

	<fieldset class="ui-corner-all">
	
	<form id="addLocation" name="addLocation">
	<table class="LocationsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Add a Screening Location</h3></td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="screeninglocation-new">Location Name (Internal)</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'screeninglocation-new', 'id'=>'screeninglocation-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="3"><label for="screeningformat-new[]">Video Format(s)</label><br />
			<?php print form_multiselect('screeningformat-new[]', $videoformat_array, '', "id='screeningformat-new' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="displayname-new">Audience Display Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'displayname-new', 'id'=>'displayname-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="seats-new"># of Seats</label><br />
			<?php print form_input(array('name'=>'seats-new', 'id'=>'seats-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
			<strong>Automatically add this location to <?php print $festival[0]->year." ".$festival[0]->name; ?>?</strong><span class="req"> * </span>
			<?php print form_checkbox(array('name'=>'location-addtofestival', 'id'=>'location-addtofestival', 'value'=>'1', 'checked'=>true,' class="ui-widget-content ui-corner-all"')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'screeninglocation-add', 'id'=>'screeninglocation-add', 'value'=>'Add')); ?></td>
		</tr>
		</tbody>
	</table>
	</form>

	<form id="updateLocation" name="updateLocation">
	<table class="LocationsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Edit/Delete a Screening Location</h3></td>
		</tr>
		<tr>
			<td colspan="3"><label for="screeninglocation">Current Screening Location List</label><span class="req"> *</span><br />
			<?php
				print form_dropdown('screeninglocation', $locations_array, 0, "id='screeninglocation' class='select-location ui-widget-content ui-corner-all'");
				print "&nbsp;&nbsp;&nbsp;<a href=\"#\" id=\"location-delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected Screening Location\" title=\"Delete selected Screening Location\" width=\"16\" height=\"16\" border=\"0\"></a>\n<br />";
			?>
			</td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="screeninglocation-current">Location Name (Internal)</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'screeninglocation-current', 'id'=>'screeninglocation-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="3"><label for="screeningformat-current[]">Video Format(s)</label><br />
			<?php print form_multiselect('screeningformat-current[]', $videoformat_array, '', "id='screeningformat-current' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="displayname-current">Audience Display Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'displayname-current', 'id'=>'displayname-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="seats-current"># of Seats</label><br />
			<?php print form_input(array('name'=>'seats-current', 'id'=>'seats-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'screeninglocation-update', 'id'=>'screeninglocation-update', 'value'=>'Update')); ?></td>
		</tr>
		</tbody>
	</table>
	</form><br clear="all" />

	<form id="addVenue" name="addVenue">
	<table class="LocationsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Add a Screening Venue</h3></td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="screeningvenue-new">Venue Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'screeningvenue-new', 'id'=>'screeningvenue-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="6"><label for="venuelocations-new[]">Screening Location(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('venuelocations-new[]', $locations_array2, '', "id='venuelocations-new' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="venueaddress-new">Street Address</label><br />
			<?php print form_input(array('name'=>'venueaddress-new', 'id'=>'venueaddress-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuecity-new">City</label><br />
			<?php print form_input(array('name'=>'venuecity-new', 'id'=>'venuecity-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuestate-new">State/Province</label><br />
			<?php print form_input(array('name'=>'venuestate-new', 'id'=>'venuestate-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuezipcode-new">Zip/Postal Code</label><br />
			<?php print form_input(array('name'=>'venuezipcode-new', 'id'=>'venuezipcode-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuephone-new">Phone Number</label><br />
			<?php print form_input(array('name'=>'venuephone-new', 'id'=>'venuephone-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'screeningvenue-add', 'id'=>'screeningvenue-add', 'value'=>'Add')); ?></td>
		</tr>
		</tbody>
	</table>
	</form>

	<form id="updateVenue" name="updateVenue">
	<table class="LocationsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Edit/Delete a Screening Venue</h3></td>
		</tr>
		<tr>
			<td colspan="3"><label for="screeningvenue">Current Screening Venue List</label><span class="req"> *</span><br />
			<?php
				print form_dropdown('screeningvenue', $venues_array, 0, "id='screeningvenue' class='select-venue ui-widget-content ui-corner-all'");
				print "&nbsp;&nbsp;&nbsp;<a href=\"#\" id=\"venue-delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected Screening Venue\" title=\"Delete selected Screening Venue\" width=\"16\" height=\"16\" border=\"0\"></a>\n<br />";
			?>
			</td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="screeningvenue-current">Venue Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'screeningvenue-current', 'id'=>'screeningvenue-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="6"><label for="venuelocations-current[]">Screening Location(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('venuelocations-current[]', $locations_array2, '', "id='venuelocations-current' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="venueaddress-current">Street Address</label><br />
			<?php print form_input(array('name'=>'venueaddress-current', 'id'=>'venueaddress-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuecity-current">City</label><br />
			<?php print form_input(array('name'=>'venuecity-current', 'id'=>'venuecity-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuestate-current">State/Province</label><br />
			<?php print form_input(array('name'=>'venuestate-current', 'id'=>'venuestate-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuezipcode-current">Zip/Postal Code</label><br />
			<?php print form_input(array('name'=>'venuezipcode-current', 'id'=>'venuezipcode-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="venuephone-current">Phone Number</label><br />
			<?php print form_input(array('name'=>'venuephone-current', 'id'=>'venuephone-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'screeningvenue-update', 'id'=>'screeningvenue-update', 'value'=>'Update')); ?></td>
		</tr>
		</tbody>
	</table>
	</form><br clear="all" />

	</fieldset>
	
	</div>

	<div id="tabs-5">
<?php
	// Festivals Tab
?>

	<fieldset class="ui-corner-all">
	<form id="addFestival" name="addFestival">
	<table class="FestivalsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Add a Festival</h3></td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="festival-new">Festival Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'festival-new', 'id'=>'festival-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="4"><label for="locations-new[]">Screening Location(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('locations-new[]', $locations_array2, '', "id='locations-new' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="year-new">Year</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'year-new', 'id'=>'year-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="startdate-new">Start Date</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'startdate-new', 'id'=>'startdate-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="enddate-new">End Date</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'enddate-new', 'id'=>'enddate-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'festival-add', 'id'=>'festival-add', 'value'=>'Add')); ?></td>
		</tr>
		</tbody>
	</table>
	</form>

	<form id="updateFestival" name="updateFestival">
	<table class="FestivalsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
			<td colspan="3"><h3>Edit a Festival</h3></td>
		</tr>
		<tr>
			<td colspan="3"><label for="festival">Current Festival List</label><span class="req"> *</span><br />
			<?php print form_dropdown('festival', $festivals_array2, 0, "id='festival' class='select-festival ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td width="40%"><label for="festival-current">Festival Name</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'festival-current', 'id'=>'festival-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
			<td width="40%" rowspan="4"><label for="locations-current[]">Screening Location(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('locations-current[]', $locations_array2, '', "id='locations-current' class='multiselect ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="10%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><label for="year-current">Year</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'year-current', 'id'=>'year-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="startdate-current">Start Date</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'startdate-current', 'id'=>'startdate-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="enddate-current">End Date</label><span class="req"> *</span><br />
			<?php print form_input(array('name'=>'enddate-current', 'id'=>'enddate-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2" align="center"><?php print form_submit(array('name'=>'festival-update', 'id'=>'festival-update', 'value'=>'Update')); ?></td>
		</tr>
		</tbody>
	</table>
	</form>

	<table class="FestivalsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr>
		<tr>
			<td colspan="6"><h3>Set Current Festival</h3></td>
		</tr>
		<tr>
			<td colspan="3">
			<form id="setFestival" name="setFestival">
			<?php
				print "<label for=\"set-current-festival\">Administration Pages</label><br /> ";
				print form_dropdown('set-current-festival', $festivals_array2, $festivaladmin[0]->id, "id='set-current-festival' class='select ui-widget-content ui-corner-all'");
				print "&nbsp;&nbsp;&nbsp;";
				print form_submit(array('name'=>'current-festival-update', 'id'=>'current-festival-update', 'value'=>'Update'));
			?>
			</form>
			</td>
			<td colspan="3">
			<form id="setFestivalFront" name="setFestivalFront">
			<?php
				print "<label for=\"set-current-festival2\">Front End Program Guide</label><br /> ";
				print form_dropdown('set-current-festival2', $festivals_array2, $festivalfront[0]->id, "id='set-current-festival2' class='select ui-widget-content ui-corner-all'");
				print "&nbsp;&nbsp;&nbsp;";
				print form_submit(array('name'=>'current-festival-update2', 'id'=>'current-festival-update2', 'value'=>'Update'));
			?>
			</form>
			</td>
		</tr>
		</tbody>
	</table><br />
	</fieldset>    

	</div>

	<div id="tabs-6">
<?php
	// Competitions Tab
?>
	<fieldset class="ui-corner-all">

	<table class="CompetitionsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="50%"><h3>Add a Competition</h3></td>
			<td width="50%"><h3>Edit a Competition</h3></td>
		</tr>
		<tr valign="top">
			<td>
			<form id="addCompetition" name="addCompetition">
			<?php
				print "<input type=\"hidden\" id=\"festival-name-new\" name=\"festival-name-new\" value=\"\" />";
				print "<label for=\"comp-name-new\">Competition Name</label><span class=\"req\"> *</span><br />";
				print form_input(array('name'=>'comp-name-new', 'id'=>'comp-name-new', 'class'=>'text ui-widget-content ui-corner-all'))."<br />";
				print "<label for=\"comp-festival-new\">Festival</label><span class=\"req\"> *</span><br />";
				print form_dropdown('comp-festival-new', $festivals_array2, $festivaladmin[0]->id, "id='comp-festival-new' class='select ui-widget-content ui-corner-all'")."<br />";
				print "<label for=\"comp-instructions-new\">Instructions</label><br />";
				print form_textarea(array('name'=>'comp-instructions-new', 'id'=>'comp-instructions-new', 'value'=>'', 'rows'=>'4', 'cols'=>'46', 'class'=>'text ui-widget-content ui-corner-all'))."<br />";
				print form_submit(array('name'=>'competition-add', 'id'=>'competition-add', 'value'=>'Add'));
			?>
			</form>
			</td>
			<td>
			<form id="updateCompetition" name="updateCompetition">
			<?php
				print "<input type=\"hidden\" id=\"festival-name-update\" name=\"festival-name-update\" value=\"\" />";
				print "<label for=\"comp-competition-update\">Select a Competition</label><span class=\"req\"> *</span><br />";
				print form_dropdown('comp-competition-update', $competition_array, 0, "id='comp-competition-update' class='select ui-widget-content ui-corner-all'")."<br />";
				print "<label for=\"comp-name-update\">Competition Name</label><span class=\"req\"> *</span><br />";
				print form_input(array('name'=>'comp-name-update', 'id'=>'comp-name-update', 'class'=>'text ui-widget-content ui-corner-all'))."<br />";
				print "<label for=\"comp-festival-update\">Festival</label><span class=\"req\"> *</span><br />";
				print form_dropdown('comp-festival-update', $festivals_array2, 0, "id='comp-festival-update' class='select ui-widget-content ui-corner-all'")."<br />";
				print "<label for=\"comp-instructions-update\">Instructions</label><br />";
				print form_textarea(array('name'=>'comp-instructions-update', 'id'=>'comp-instructions-update', 'value'=>'', 'rows'=>'4', 'cols'=>'46', 'class'=>'text ui-widget-content ui-corner-all'))."<br />";
				print form_submit(array('name'=>'competition-update', 'id'=>'competition-update', 'value'=>'Update'));
			?>
			</form>
			</td>
		</tr>
		<tr>
			<td colspan="2"><br /><h3>Add Competition Films</h3></td>
		</tr>
		<tr>
			<td colspan="2">
<?php if (count($competition_array) > 0) { ?>
			<form id="addCompetitionFilms" name="addCompetitionFilms">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tbody style="border-top:none;">
					<tr valign="top">
						<td width="20%"><label for="compfilm-compname-new">Competition</label><span class="req"> *</span><br /><?php print form_dropdown('compfilm-compname-new', $competition_array, 0, "id='compfilm-compname-new' class='select-small ui-widget-content ui-corner-all'"); ?></td>
						<td width="20%"><label for="compfilm-filmname-new">Film Name</label><span class="req"> *</span><br /><?php print form_dropdown('compfilm-filmname-new', $films_array2, 0, "id='compfilm-filmname-new' class='select-small ui-widget-content ui-corner-all'"); ?></td>
						<td width="20%"><label for="compfilm-video-url-new">Video URL</label><br /><?php print form_input(array('name'=>'compfilm-video-url-new', 'id'=>'compfilm-video-url-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
						<td width="20%"><label for="compfilm-video-password-new">Video Password</label><br /><?php print form_input(array('name'=>'compfilm-video-password-new', 'id'=>'compfilm-video-password-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
						<td width="20%"><br /><?php print form_submit(array('name'=>'competition-films-add', 'id'=>'competition-films-add', 'value'=>'Add')); ?></td>
					</tr>
				</table>
			</form>
<?php } else { ?>
			<p>Please add at least one Competition and refresh the page before you can add films for them.</p>
<?php } ?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><h3>Edit/Delete Competition Films</h3></td>
		</tr>
		<tr>
			<td colspan="2">
				<table id="EditCompetitionFilms" width="100%" cellpadding="0" cellspacing="0" border="0">
					<thead>
						<tr>
							<th width="10%">&nbsp;</th>
							<th width="18%">Festival</th>
							<th width="18%">Competition</th>
							<th width="18%">Film</th>
							<th width="18%">Video Url</th>
							<th width="18%">Video Password</th>
						</tr>
					</thead>
					<tbody style="border-top:none;">
<?php
	$x = 1; $cf_array = array(); $prev = "";
	if (count($competition_films) == 0) {
		print "\t<tr valign=\"top\" class=\"oddrow\">\n";
		print "\t\t<td colspan=\"6\" align=\"center\">There are no competition films defined yet.</td>\n";
		print "\t</tr>\n";
	} else {
		$comp_slug = ""; $prev_slug = "";
		foreach ($competition_films as $thisFilm) {
			$festival_name = "";
			foreach ($festivals as $thisFestival) {
				if ($thisFestival->id == $thisFilm->festival_id) { $festival_name = $thisFestival->year." ".$thisFestival->name; break; }
			}
			$movie_name = "";
			foreach ($filmids as $thisMovie) {
				if ($thisMovie->movie_id == $thisFilm->movie_id) { $movie_name = $thisMovie->label; break; }
			}

			foreach ($competitions as $thisCompetition) {
				if ($thisCompetition->id == $thisFilm->competition_id) { $comp_slug = $thisCompetition->slug; }
			}
			if ($comp_slug == "" || $comp_slug != $prev_slug) {
				print "<tr class=\"vote_link\">";
				print "<td>&nbsp;</td><td colspan=\"5\"><a href=\"/competitions/index/".$comp_slug."\" target=\"_blank\">Voting Link: ".$festival_name." ".$thisFilm->name."</a></td>";
				print "</tr>";
			}
			$prev_slug = $comp_slug;

			if (($x % 2) == 1) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
				}
			}
			if (($x % 2) == 0) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
				}
			}
			print "\t\t<td><button id=\"cf-".$thisFilm->id."\" data-id=\"".$thisFilm->id."\" class=\"edit\">Edit</button>";
			if ($this->session->userdata('limited') == 0) {
				print "&nbsp;<a href=\"#\" id=\"cf-del-".$thisFilm->id."\" data-id=\"".$thisFilm->id."\" data-filmname=\"".switch_title($movie_name)."\" class=\"delete\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete this Competition Film\" title=\"Delete this Competition Film\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a>";
			}
			print "</td>\n";
			print "\t\t<td>".$festival_name."</td>\n";
			print "\t\t<td>".$thisFilm->name."</td>\n";
			print "\t\t<td>".switch_title($movie_name)."</td>\n";
			print "\t\t<td>".$thisFilm->video_url."</td>\n";
			print "\t\t<td>".$thisFilm->video_password."</td>\n";
			print "\t</tr>\n";
		
			$x++; $cf_array[] = $thisFilm->id; $prev = $festival_name;
		}
	}
?>
						<tr id="competitions_replaceme">
							<td colspan="6"><input type="hidden" name="competitions_new_id" id="competitions_new_id" value="0" /></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>


	</fieldset>
	</div>
</div>


<!-- Start Dialog Boxes -->
<div id="editPersonnelOrder-dialog" title="Edit Personnel Order">
<form name="editPersonnelOrderForm" id="editPersonnelOrderForm">
	<style>
		#personnel_sort { list-style-type: none; margin: 0; padding: 0; width: 300px; }
		#personnel_sort li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1em; height: 15px; cursor:move; }
		#personnel_sort li span { position: absolute; margin-left: -1.3em; }
	</style>

	<ul id="personnel_sort">
<?php
	$order = "";
	foreach ($personnel_order_array as $key => $thisPersonnel) {
		if ($thisPersonnel != "--") {
			print "\t\t<li class=\"ui-state-default\" id=\"".$key."\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>".$thisPersonnel."</li>\n";
			$order .= $key.",";
		}
	} $order = trim($order, ",");
?>
	</ul>
	<input type="hidden" id="personnel_order" name="personnel_order" value="<?php print $order; ?>" />
</form>
</div>

<div id="editSponsorLogo-dialog" title="Edit Sponsor Logo/Link">
<form name="editSponsorLogoForm" id="editSponsorLogoForm" enctype="multipart/form-data">
	<input type="hidden" name="festival-id" id="festival-id" value="<?php print $festival[0]->id; ?>" />
	<input type="hidden" name="currentlogo-url" id="currentlogo-url" value="" />
	<input type="hidden" name="sponsor-name" id="sponsor-name" value="" />
	<input type="hidden" name="logo-ids-to-update" id="logo-ids-to-update" value="" />
	<input type="hidden" name="original-film-ids" id="original-film-ids" value="" />
	<input type="hidden" name="sponsor-logo-update" id="sponsor-logo-update" value="" />
	<table class="SponsorLogoDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="50%"><label for="sponsor-name-update">Sponsor Name</label><span class="req"> *</span><br />
			<div id="sponsor-name-update"></div>
			</td>
			<td width="50%" rowspan="4"><label for="sponsored-films-update">Sponsored Film(s)</label><span class="req"> *</span><br />
			<?php print form_multiselect('sponsored-films-update[]', $films_array, 0, "id='sponsored-films-update' class='select-films ui-widget-content ui-corner-all'"); ?>
			<br /><span>**Hold CTRL or OPTION to select multiple films</span>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="sponsorlogo-type-update">Sponsor Type</label><span class="req"> *</span><br />
			<?php print form_dropdown('sponsorlogo-type-update', $sponsorlogo_array, 0, "id='sponsorlogo-type-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="websitelink-update">Website Link</label><br />
			<?php print form_input(array('name'=>'websitelink-update', 'id'=>'websitelink-update', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<!--
		<tr valign="top">
			<td><label for="logofile-update">Change Sponsor Logo</label><br />
			<?php //print form_upload(array('name'=>'logofile-update', 'id'=>'logofile-update', 'size'=>'30', 'class'=>'text-file ui-corner-all')); ?>
			</td>
		</tr>
		-->
		<tr valign="top">
			<td><label for="currentlogo-update">Current Sponsor Logo</label><br />
			<div id="currentlogo-update"></div>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="editCompetitionFilms-dialog" title="Edit Competition Film">
<form name="editCompetitionFilmsForm" id="editCompetitionFilmsForm">
	<input type="hidden" name="comp-competition-film-update" id="comp-competition-film-update" value="0" />
	<table class="CompetitionDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td><label for="compfilm-compname-update">Competition</label><span class="req"> *</span><br />
			<?php print form_dropdown('compfilm-compname-update', $competition_array, 0, "id='compfilm-compname-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="compfilm-filmname-update">Film</label><span class="req"> *</span><br />
			<?php print form_dropdown('compfilm-filmname-update', $films_array2, 0, "id='compfilm-filmname-update' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="compfilm-video-url-update">Video Url</label><br />
			<?php print form_input(array('name'=>'compfilm-video-url-update', 'id'=>'compfilm-video-url-update', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="compfilm-video-password-update">Video Password</label><br />
			<?php print form_input(array('name'=>'compfilm-video-password-update', 'id'=>'compfilm-video-password-update', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>
</form>
</div>

<div id="deleteFilmSetting-dialog" title="Delete Film Detail Setting">
<form name="deleteFilmSettingForm" id="deleteFilmSettingForm">
	<input type="hidden" name="setting-type" id="setting-type" value="" />
	<input type="hidden" name="todelete-id" id="todelete-id" value="" />
	<div id="filmSettingReplacement"></div>
</form>
</div>

<div id="deleteSection-dialog" title="Delete Film Section">
<form name="deleteSectionForm" id="deleteSectionForm">
	<input type="hidden" name="section-todelete-id" id="section-todelete-id" value="" />
	<div id="sectionReplacement"></div>
</form>
</div>

<div id="deleteLocation-dialog" title="Delete Screening Location">
<form name="deleteLocationForm" id="deleteLocationForm">
	<input type="hidden" name="location-todelete-id" id="location-todelete-id" value="" />
	<div id="locationReplacement"></div>
</form>
</div>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		// Film Details Tab
		$('.select').on('change', load_value);
		$('.select-video').on('change', load_value_color);

<?php
function print_filmdetail_js($field, $limited) {
	print "\t\t$('#".$field."-add').on('click', function() { if ($('#add-".$field."').validate().form() == true) { add_film_type('".$field."', $('#".$field."-new').val(), 0); } return false; });\n";
	print "\t\t$('#".$field."-update').on('click', function() { if ($('#update-".$field."').validate().form() == true) { update_film_type('".$field."', $('#".$field."-current').val(), 0, $('#".$field."').val()); } return false; });\n";
	if ($limited == 0) {
		print "\t\t$('#".$field."-delete').on('click', function() { if ($('#update-".$field."').validate().form() == true) { delete_film_type('".$field."', $('#".$field."').val()); } return false; });\n";
	}
	print "\t\t$('#add-".$field."').validate({ rules: { '".$field."-new': 'required' }, messages: { '".$field."-new': 'Please enter a value.' } });\n";
	print "\t\t$('#update-".$field."').validate({ rules: { '".$field."': 'required', '".$field."-current': 'required' }, messages: { '".$field."': 'Please select a value.', '".$field."-current': 'Please enter a value.' } });\n\n";
}

		$this->session->userdata('limited') == 0 ? $limited = 0 : $limited = 1;

		print_filmdetail_js("country", $limited);
		print_filmdetail_js("language", $limited);
		print_filmdetail_js("genre", $limited);

		print_filmdetail_js("aspectratio", $limited);
		print_filmdetail_js("color", $limited);
		print_filmdetail_js("distribution", $limited);

		print_filmdetail_js("event", $limited);
		print_filmdetail_js("courier", $limited);
		print_filmdetail_js("personnel", $limited);

		print_filmdetail_js("premiere", $limited);
		print_filmdetail_js("sound", $limited);
		//print_filmdetail_js("format", $limited);
?>

		$('#format-add').on('click', function() { if ($("#add-format").validate().form() == true) { add_film_type("format", $("#format-new").val(), $("#format-color").val()); } return false; });
		$('#format-update').on('click', function() { if ($("#update-format").validate().form() == true) { update_film_type("format", $("#format-current").val(), $("#format-color-update").val(), $("#format").val()); } return false; });
		$('#format-delete').on('click', function() { if ($("#update-format").validate().form() == true) { delete_film_type("format", $("#format").val()); } return false; });
		$("#add-format").validate({ rules: { "format-new": "required" }, messages: { "format-new": "Please enter a value." } });
		$("#update-format").validate({ rules: { "format": "required", "format-current": "required" }, messages: { "format": "Please select a value.", "format-current": "Please enter a value." } });

		$('#colorSelector').ColorPicker({
			color: '#000000',
			onShow: function (colpkr) { $(colpkr).fadeIn(500); return false; },
			onHide: function (colpkr) { $(colpkr).fadeOut(500); return false; },
			onChange: function (hsb, hex, rgb) {
				$('#colorSelector div').css('backgroundColor', '#' + hex);
				$('#format-color').val(hex);
			}
		});        
		$('#colorSelector2').ColorPicker({
			color: '#000000',
			onShow: function (colpkr) { $(colpkr).fadeIn(500); return false; },
			onHide: function (colpkr) { $(colpkr).fadeOut(500); return false; },
			onChange: function (hsb, hex, rgb) {
				$('#colorSelector2 div').css('backgroundColor', '#' + hex);
				$('#format-color-update').val(hex);
			}
		});
		$('#colorSelectorSection').ColorPicker({
			color: '#000000',
			onShow: function (colpkr) { $(colpkr).fadeIn(500); return false; },
			onHide: function (colpkr) { $(colpkr).fadeOut(500); return false; },
			onChange: function (hsb, hex, rgb) {
				$('#colorSelectorSection div').css('backgroundColor', '#' + hex);
				$('#section-color').val(hex);
			}
		});        
		$('#colorSelector2Section').ColorPicker({
			color: '#000000',
			onShow: function (colpkr) { $(colpkr).fadeIn(500); return false; },
			onHide: function (colpkr) { $(colpkr).fadeOut(500); return false; },
			onChange: function (hsb, hex, rgb) {
				$('#colorSelector2Section div').css('backgroundColor', '#' + hex);
				$('#section-color-update').val(hex);
			}
		});

		$('#order_personnel_button').on('click', function() {
			$("#editPersonnelOrder-dialog").dialog('open');
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#personnel_sort").sortable({
			placeholder: "ui-state-highlight",
			update: function(event, ui) {
				var orderstring = "";
				var thisorder = $(this).sortable('toArray');
				jQuery.each(thisorder, function(index, value) {
					orderstring += value+",";
				});
				$("#personnel_order").val(orderstring.slice(0,-1));
			}
		}).disableSelection();

		$("#editPersonnelOrder-dialog").dialog({
			autoOpen: false,
			height: 600,
			width: 350,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Update Order': function() {
					$.ajax({
						success: function(msg, text, xhr) {
							//alert(msg);
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editPersonnelOrderForm').serialize(),
						url: '/admin/film_settings/update_personnel_order/',
						type: 'POST',
						dataType: 'html'
					}); 

					$(this).dialog('close');
				}
			}
		});

		$("#deleteFilmSetting-dialog").dialog({
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var setting_type = $('#setting-type').val();
					var todelete_id = $('#todelete-id').val();
					if ($("#deleteFilmSettingForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$("#"+setting_type+" option:selected").remove();
								$("#"+setting_type+"-current").val("");
								$("#"+setting_type+"-current").css("background","none repeat-x scroll 50% top #F2EA1D");
								var timeoutID = window.setTimeout(function () { $("#"+setting_type+"-current").css("background","none repeat-x scroll 50% top #FFFFFF") }, 2000);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#deleteFilmSettingForm').serialize(),
							url: '/admin/film_settings/delete_type_replace/'+setting_type+'/'+todelete_id+'/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
					}
				}
<?php } ?>
			}
		});
		$("#deleteFilmSettingForm").validate({ rules: { "replacement-value": { required:true, min: 1 } }, messages: { "replacement-value": "Please select a replacement value." } });

		// Sections Tab
		var sectionJSON = <?php print $sections_json; ?>;
		$('.select-section').on('change', function() { load_value_section($("option:selected", this).val(),sectionJSON); });
		$("#addSection").submit(function() {
			if ($("#addSection").validate().form() == true) { add_film_setting("#addSection", "#section"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateSection").submit(function() {
			if ($("#updateSection").validate().form() == true) { update_film_setting("#updateSection", "#section"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#addSection").validate({
			rules: { "section-new": "required", "sec-festival-new": "required" },
			messages: { "section-new": "Please enter a section name.", "sec-festival-new": "Please select a festival." }
		});
		$("#updateSection").validate({
			rules: { section: {required:true, min:1 }, "section-current": "required", "sec-festival-current": "required" },
			messages: { section: "Please select a section to edit.", "section-current": "Please enter a section name.", "sec-festival-current": "Please select a festival." }
		});
		$("#section-delete").on('click', function(e) {
			e.preventDefault();
			var section_id = $("#section option:selected").val();
			delete_film_section(section_id);
		});

		$("#deleteSection-dialog").dialog({
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var bkgcolor = "#F2EA1D";
					var todelete_id = $('#section-todelete-id').val();
					if ($("#deleteSectionForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$("#section option:selected").remove();
								$("#sec-festival-current").val(0);
								$("#description-current").val("");
								$("#section-current").val("");
								$("#section-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
								var timeoutID = window.setTimeout(function () { $("#section-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#deleteSectionForm').serialize(),
							url: '/admin/film_settings/delete_section_replace/'+todelete_id+'/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
					}
				}
<?php } ?>
			}
		});
		$("#deleteSectionForm").validate({ rules: { "replacement-value-section": { required:true, min: 1 } }, messages: { "replacement-value-section": "Please select a replacement value." } });


		// Sponsors Tab
		var sponsorLogoJSON = <?php print $sponsorlogo_json; ?>;
		$("#addSponsor").submit(function() {
			if ($("#addSponsor").validate().form() == true) { add_film_setting("#addSponsor", "#sponsor"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateSponsor").submit(function() {
			if ($("#updateSponsor").validate().form() == true) { update_film_setting("#updateSponsor", "#sponsor"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#addSponsor").validate({
			rules: { "sponsor-new": "required" },
			messages: { "sponsor-new": "Please enter a sponsor name." }
		});
		$("#updateSponsor").validate({
			rules: { "sponsor": { required:true, min:1 }, "sponsor-current": "required" },
			messages: { "sponsor": "Please select a sponsor to edit.", "sponsor-current": "Please enter a sponsor name." }
		});

		var sponsorTypeJSON = <?php print $sponsortype_json; ?>;
		$('#sponsortype').on('change', function() { load_value_sptype($("option:selected", this).val(),sponsorTypeJSON); });
		$("#add-sponsortype").submit(function() {
			if ($("#add-sponsortype").validate().form() == true) {
				add_film_setting("#add-sponsortype", "#sponsortype");
			}
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#update-sponsortype").submit(function() {
			if ($("#update-sponsortype").validate().form() == true) {
				update_film_setting("#update-sponsortype", "#sponsortype");
			}
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#add-sponsortype").validate({
			rules: { "sponsortype-name-new": "required", "sponsortype-desc-new": "required", "sponsortype-order-new": { required:true, digits:true } },
			messages: { "sponsortype-name-new": "Please enter a name.", "sponsortype-desc-new": "Please enter a description.", "sponsortype-order-new": "Please enter an order." }
		});
		$("#update-sponsortype").validate({
			rules: { "sponsortype": { required:true, min:1 }, "sponsortype-name-current": "required", "sponsortype-desc-current": "required", "sponsortype-order-current": { required:true, digits:true } },
			messages: { "sponsortype": "Please select a sponsor type to edit.", "sponsortype-name-current": "Please enter a name.", "sponsortype-desc-current": "Please enter a description.", "sponsortype-order-current": "Please enter an order." }
		});
		$("#addSponsorLogo").validate({
			rules: { "sponsor-logo": { required:true, min:1 }, "sponsorlogo-type-new": { required:true, min:1 }, "logofile-new": { accept: "png|jpe?g|gif" }, "websitelink-new": "url", "sponsored-films-new[]": "required" },
			messages: { "sponsor-logo": "Please select a sponsor.", "sponsorlogo-type-new": "Please select a sponsor type.", "logofile-new": "Please select an image file (.JPG .GIF .PNG)", "websitelink-new": "Please enter a valid URL (including http://).", "sponsored-films-new[]": "Please select at least one sponsored film." }
		});
		$("#editSponsorLogoForm").validate({
			rules: { "sponsorlogo-type-update": { required:true, min:1 }, "websitelink-update": "url", "sponsored-films-update[]": "required" },
			messages: { "sponsorlogo-type-update": "Please select a sponsor type.", "websitelink-update": "Please enter a valid URL (including http://).", "sponsored-films-update[]": "Please select at least one sponsored film." }
		});
		
		$("#editSponsorLogo-dialog").dialog({
			autoOpen: false,
			height: 500,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var logo_id = $('#sponsor-logo-update').val();
					var sponsor_name = $('#sponsor-name').val();
					var decision = confirm("Are you sure you really want to delete the sponsor logo for "+sponsor_name+"?");
					if (decision == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#logo-'+logo_id).closest("tr").remove();
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editSponsorLogoForm').serialize(),
							url: '/admin/film_settings/delete_sponsor_logo/',
							type: 'POST',
							dataType: 'html'
						}); 
					
						$(this).dialog('close');
					}
				},
<?php } ?>
				'Update Sponsor Logo/Link': function() {
					var logo_id = $('#sponsor-logo-update').val();
					if ($("#editSponsorLogoForm").validate().form() == true) {

						$.ajax({
							success: function(msg, text, xhr) {
								$('#logo-'+logo_id).closest("tr").replaceWith(msg);
							},
							complete: function(xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										sponsorLogoJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_settings/return_sponsorlogo_json/'+$('#festival-id').val(),
									type: 'POST',
									dataType: 'json'
								});
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editSponsorLogoForm').serialize(),
							url: '/admin/film_settings/update_sponsor_logo/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
					}
				}
			}
		});

		$('#EditSponsorLogos').on('click','button.update', function() {
			var logo_id = $(this).data('id').toString();
			open_edit_logo_dialog(logo_id, sponsorLogoJSON);
			$(this).removeClass("ui-state-focus");
			return false;
		});

		// Screening Locations / Venues Tab
		var locationsJSON = <?php print $locations_json; ?>;
		$('.select-location').on('change', function() { load_value_location($("option:selected", this).val(),locationsJSON); });
		$("#addLocation").submit(function() {
			if ($("#addLocation").validate().form() == true) { add_film_setting("#addLocation", "#screeninglocation"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateLocation").submit(function() {
			if ($("#updateLocation").validate().form() == true) { update_film_setting("#updateLocation", "#screeninglocation"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#addLocation").validate({
			rules: { "screeninglocation-new": "required", "displayname-new": "required", "seats-new": "digits" },
			messages: { "screeninglocation-new": "Please enter a location name.", "displayname-new": "Please select a display name.", "seats-new": "Whole numbers only" }
		});
		$("#updateLocation").validate({
			rules: { "screeninglocation": { required:true, min:1 }, "screeninglocation-current": "required", "displayname-current": "required", "seats-current": "digits" },
			messages: { "screeninglocation": "Please select a screening location to edit.", "screeninglocation-current": "Please enter a location name.", "displayname-current": "Please select a display name.", "seats-current": "Whole numbers only" }
		});

		$("#location-delete").on('click', function(e) {
			e.preventDefault();
			var location_id = $("#screeninglocation option:selected").val();
			delete_screening_location(location_id);
		});
		$("#deleteLocation-dialog").dialog({
			autoOpen: false,
			height: 250,
			width: 500,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var bkgcolor = "#F2EA1D";
					var todelete_id = $('#location-todelete-id').val();
					if ($("#deleteLocationForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$("#screeninglocation option:selected").remove();
								$("#screeninglocation-current").val("");
								$("#displayname-current").val("");
								$("#seats-current").val("");
								$("#screeningformat-current").find("option").attr("selected", false);
								$("#screeninglocation-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
								window.setTimeout(function () { $("#screeninglocation-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#deleteLocationForm').serialize(),
							url: '/admin/film_settings/delete_location_replace/'+todelete_id+'/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
					}
				}
<?php } ?>
			}
		});
		$("#deleteLocationForm").validate({ rules: { "replacement-value-location": { required:true, min: 1 } }, messages: { "replacement-value-location": "Please select a replacement value." } });

		// Venues
		var venuesJSON = <?php print $venues_json; ?>;
		$('.select-venue').on('change', function() { load_value_venue($("option:selected", this).val(),venuesJSON); });
		$("#addVenue").submit(function() {
			if ($("#addVenue").validate().form() == true) { add_film_setting("#addVenue", "#screeningvenue"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateVenue").submit(function() {
			if ($("#updateVenue").validate().form() == true) { update_film_setting("#updateVenue", "#screeningvenue"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#addVenue").validate({
			rules: { "screeningvenue-new": "required", "venuelocations-new[]": { required:true } },
			messages: { "screeningvenue-new": "Please enter a venue name.", "venuelocations-new[]": "Please select at least one location." }
		});
		$("#updateVenue").validate({
			rules: { "screeningvenue": { required:true, min:1 }, "screeningvenue-current": "required", "venuelocations-current[]": { required:true } },
			messages: { "screeningvenue": "Please select a screening venue to edit.", "screeningvenue-current": "Please enter a venue name.", "venuelocations-current[]": "Please select at least one location." }
		});
		$("#venue-delete").on('click', function(e) {
			e.preventDefault();
			//var venue_id = $("#screeningvenue option:selected").val();
			//delete_screening_venue(venue_id);
		});


		// Festivals Tab
		var festivalJSON = <?php print $festivals_json; ?>;
		$('.select-festival').on('change', function() { load_value_festival($("option:selected", this).val(),festivalJSON); });
		$("#addFestival").submit(function() {
			if ($("#addFestival").validate().form() == true) { add_film_setting("#addFestival", "#festival"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateFestival").submit(function() {
			if ($("#updateFestival").validate().form() == true) { update_film_setting("#updateFestival", "#festival"); }
			$(this).removeClass("ui-state-focus"); return false;
		});

		$("#startdate-new, #enddate-new, #startdate-current, #enddate-current").each(function() {
			$(this).datepicker();
		});
		$("#setFestival").submit(function() { set_current_festival("#setFestival"); return false; });
		$("#setFestivalFront").submit(function() { set_current_festival("#setFestivalFront"); return false; });

		$("#addFestival").validate({
			rules: { "festival-new": "required", "year-new": { required: true, digits:true }, "startdate-new": { required:true, date:true }, "enddate-new": { required:true, date:true, greaterThan: "#startdate-new" }, "locations-new[]": "required" },
			messages: { "festival-new": "Please enter a name.", "year-new": "Please enter a year.", "startdate-new": { required:"Please enter a start date.", date:"Please enter a valid date." }, "enddate-new": { required:"Please enter an end date.", date:"Please enter a valid date." }, "locations-new[]": "Please select at least one screening location." }
		});
		$("#updateFestival").validate({
			rules: { "festival": { required: true, min: 1 }, "festival-current": "required", "year-current": { required: true, digits:true }, "startdate-current": { required:true, date:true }, "enddate-current": { required:true, date:true, greaterThan: "#startdate-current" }, "locations-current[]": "required" },
			messages: { "festival": "Please select a festival to edit.", "festival-current": "Please enter a name.", "year-current": "Please enter a year.", "startdate-current": { required:"Please enter a start date.", date:"Please enter a valid date." }, "enddate-current": { required:"Please enter an end date.", date:"Please enter a valid date." }, "locations-current[]": "Please select at least one screening location." }
		});

		// Competitions Tab
		var competitionsJSON = <?php print json_encode($competitions); ?>;
		$('#festival-name-new').val( $('#comp-festival-new option:selected').text() );
		$('#comp-festival-new').on('change', function() { $('#festival-name-new').val( $("option:selected", this).text() ); });
		$('#festival-name-update').val( $('#comp-festival-update option:selected').text() );
		$('#comp-festival-update').on('change', function() { $('#festival-name-update').val( $("option:selected", this).text() ); });
		$('#comp-competition-update').on('change', function() { load_value_competition($("option:selected", this).val(),competitionsJSON); });
		$("#addCompetition").submit(function() {
			if ($("#addCompetition").validate().form() == true) { add_film_setting("#addCompetition", "#comp-competition-update"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#updateCompetition").submit(function() {
			if ($("#updateCompetition").validate().form() == true) { update_film_setting("#updateCompetition", "#comp-competition-update"); }
			$(this).removeClass("ui-state-focus"); return false;
		});
		$("#addCompetition").validate({
			rules: { "comp-name-new": "required", "comp-festival-new": "required" },
			messages: { "comp-name-new": "Please enter a competition name.", "comp-festival-new": "Please select a festival." }
		});
		$("#updateCompetition").validate({
			rules: { "comp-competition-update": {required:true, min:1 }, "comp-name-update": "required", "comp-festival-update": "required" },
			messages: { "comp-competition-update": "Please select a competition to edit.", "comp-name-update": "Please enter a competition name.", "comp-festival-update": "Please select a festival." }
		});

		var competitionfilmsJSON = <?php print json_encode($competition_films); ?>;
		$('#competition-films-add').on('click', function() {
			if ($("#addCompetitionFilms").validate().form() == true) {
				add_film_setting("#addCompetitionFilms", "#competitions_replaceme");
				$.ajax({
					success: function(msg, text, xhr) {
						competitionfilmsJSON = msg;
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					url: '/admin/film_settings/return_competition_films_json/',
					type: 'POST',
					dataType: 'json',
					async: false
				});
			} return false;
		});
		$("#addCompetitionFilms").validate({
			rules: { "compfilm-compname-new": { required:true, min:1 }, "compfilm-filmname-new": { required:true, min:1 }, "compfilm-video-url-new": "url" },
			messages: { "compfilm-compname-new": "Please select a competition.", "compfilm-filmname-new": "Please select a film.", "compfilm-video-url-new": "Please enter a video url." }
		});

		$('#EditCompetitionFilms').on('click','a.delete', function(e) {
			e.preventDefault();
			delete_competition_film($(this).data('id'), $(this).data('filmname'));
		});
		$('#EditCompetitionFilms').on('click','button.edit', function(e) {
			e.preventDefault();
			open_edit_competition_dialog($(this).data('id').toString(), competitionfilmsJSON);
			$(this).removeClass('ui-state-focus');
		});
		$("#editCompetitionFilmsForm").validate({
			rules: { "compfilm-compname-update": { required:true, min:1 }, "compfilm-filmname-update": { required:true, min:1 }, "compfilm-video-url-update": "url" },
			messages: { "compfilm-compname-update": "Please select a competition.", "compfilm-filmname-update": "Please select a film.", "compfilm-video-url-update": "Please enter a video url." }
		});
		$("#editCompetitionFilms-dialog").dialog({
			autoOpen: false,
			height: 400,
			width: 350,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Update': function() {
					var compfilms_id = $('#comp-competition-film-update').val();
					if ($("#editCompetitionFilmsForm").validate().form() == true) {
						$.ajax({
							success: function(msg, text, xhr) {
								$('#cf-'+compfilms_id).closest("tr").replaceWith(msg);
								$('#cf-'+compfilms_id).button();
							},
							complete: function(xhr, text) {
								$.ajax({
									success: function(msg, text, xhr) {
										competitionfilmsJSON = msg;
									},
									error: function(xhr, msg1, msg2){
										alert( "Failure! " + xhr + msg1 + msg2); },
									url: '/admin/film_settings/return_competition_films_json/',
									type: 'POST',
									dataType: 'json'
								});
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#editCompetitionFilmsForm').serialize(),
							url: '/admin/film_settings/update_competition_film/',
							type: 'POST',
							dataType: 'html'
						});
						$(this).dialog('close');
					}
				}
			}
		});

		$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });  
	});
</script>