<?php
	print "<div id=\"tabs\">\n";
	print "\t<ul>\n";
	$x = 1;
	foreach ($days as $thisDay) {
		print "\t\t<li><a href=\"#tabs-".$x."\">".$thisDay."</a></li>\n";
		$x++;
	}
	print "\t</ul>\n";

	$x = 1;
	foreach ($days as $thisDay) {
		print "\t<div id=\"tabs-".$x."\">\n";
    	print "\t\t<fieldset class=\"ui-corner-all\">\n";
		
		print "<p>Content Here</p>";

        print "\t\t</fieldset>\n";
		print "\t</div>\n";
		$x++;
	}
	print "</div>"; // End tabs
?>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();
				
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});		
	});
</script>
