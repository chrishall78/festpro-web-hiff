<?php
	$festivals_array = convert_festival_to_array2($festivals);
	$language_array = convert_to_array2($languages);
	$status_array = array("Submitted"=>"Submitted","Waiting for Payment"=>"Waiting for Payment","Approved: Waived"=>"Approved: Waived","Approved: Paid"=>"Approved: Paid","Declined"=>"Declined");
	$guesttypes_array = convert_to_array($guesttypes);
?>
<p>To submit an accreditation manually through this form, <a href="/accreditation/" target="_blank">please click here</a>.</p>
<fieldset class="ui-corner-all" style="padding:0px; background-color:#FFFFFF;">
		<table class="GuestReviewTab" width="100%" cellpadding="0" cellspacing="0" border="0">
        	<thead>
			<tr valign="top">
				<th style="width:10%; text-align:center;">(<?php print count($guest_items); ?>)<!-- <button>Export</button> --></th>
				<th style="width:15%">Guest Name<br /><span><a href="/admin/guest_review/sort/nameasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Guest Name" /></a> <a href="/admin/guest_review/sort/namedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Guest Name" /></a></span></th>
				<th style="width: 8%">Film<br /><span><a href="/admin/guest_review/sort/filmasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Film/Company" /></a> <a href="/admin/guest_review/sort/filmdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Film" /></a></span></th>
				<th style="width: 8%">Company<br /><span><a href="/admin/guest_review/sort/companyasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Company" /></a> <a href="/admin/guest_review/sort/companydesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Company" /></a></span></th>
                <th style="width:15%">Travel Method<br /><span><a href="/admin/guest_review/sort/travelmethodasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Travel Method" /></a> <a href="/admin/guest_review/sort/travelmethoddesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Travel Method" /></a></span></th>
                <th style="width:10%">Guest Category <br /><span><a href="/admin/guest_review/sort/categoryasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Guest Category" /></a> <a href="/admin/guest_review/sort/categorydesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Guest Category" /></a></span></th>
                <th style="width:10%">Badge Fee <br /><span><a href="/admin/guest_review/sort/feeasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Badge Fee" /></a> <a href="/admin/guest_review/sort/feedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Badge Fee" /></a></span></th>
				<th style="width:10%">Submission Date<br /><span><a href="/admin/guest_review/sort/dateasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Submission Date" /></a> <a href="/admin/guest_review/sort/datedesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Submission Date" /></a></span></th>
                <th style="width:15%">Guest Status <br /><span><a href="/admin/guest_review/sort/statusasc/"><img src="/assets/images/icons/sortasc.png" width="10" height="5" alt="Sort Ascending by Guest Status" /></a> <a href="/admin/guest_review/sort/statusdesc/"><img src="/assets/images/icons/sortdesc.png" width="10" height="5" alt="Sort Descending by Guest Status" /></a></span></th>
			</tr>
            </thead>
    	    <tbody>
<?php
	$x = 1;
	foreach ($guest_items as $thisGuest) {
		$country_string = $category_string = "";

		// Process countries from ids into a string
		foreach ($countries as $country) {
			if ($thisGuest->GuestCountry == $country->id) { $country_string .= $country->name.", "; break; }
		}
		$country_string = trim($country_string, ", ");

		if ($thisGuest->Imported == 1) {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n"; }
		} else {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		}
	
		print "\t\t\t\t<td rowspan=\"2\" align=\"center\">";
		print "<form id=\"guest-".$thisGuest->guest_id."\" name=\"guest-".$thisGuest->guest_id."\">";
		print "<input type=\"hidden\" id=\"guesttype-".$thisGuest->guest_id."\" value=\"".$thisGuest->DelegateCategory."\" /> ";
		print "<input type=\"hidden\" id=\"status-".$thisGuest->guest_id."\" value=\"".$thisGuest->DelegateStatus."\" /> ";
		print "<input type=\"hidden\" id=\"fee-".$thisGuest->guest_id."\" value=\"".$thisGuest->Fee."\" /> ";
		print "<input type=\"hidden\" id=\"company-".$thisGuest->guest_id."\" value=\"".$thisGuest->FilmOrCompany."\" /> ";
		print "<input type=\"hidden\" id=\"film-".$thisGuest->guest_id."\" value=\"".$thisGuest->Film."\" /> ";
		print "<input type=\"hidden\" id=\"email-".$thisGuest->guest_id."\" value=\"".$thisGuest->EmailAddress."\" /> ";
		print "<input type=\"hidden\" id=\"guestname-".$thisGuest->guest_id."\" value=\"".$thisGuest->FirstName." ".$thisGuest->LastName."\" /> ";
		print "<button id=\"guest-review-".$thisGuest->guest_id."\" data-id=\"".$thisGuest->guest_id."\" class=\"import\" value=\"".htmlspecialchars($thisGuest->FirstName." ".$thisGuest->LastName)."\">Import</button><br />";
		print "<button id=\"guest-view-".$thisGuest->guest_id."\" data-id=\"".$thisGuest->guest_id."\" class=\"view\">View</button>";
		print "<input type=\"hidden\" id=\"rowtype-".$thisGuest->guest_id."\" name=\"rowtype-".$thisGuest->guest_id."\" value=\"";
		if ($thisGuest->Imported == 1) {
			if (($x % 2) == 1) { print "oddrow-imported"; }
			if (($x % 2) == 0) { print "evenrow-imported"; }
		} else {
			if (($x % 2) == 1) { print "oddrow"; }
			if (($x % 2) == 0) { print "evenrow"; }
		}
		print "\" />";
		print "</form>";		
		print "</td>\n";

		$importedUsername = $emailedUsername = "";
		if ($thisGuest->ImportedBy != 0 || $thisGuest->EmailedBy != 0) {
			foreach ($user_list as $thisUser) {
				if ($thisGuest->ImportedBy == $thisUser->id) { $importedUsername = $thisUser->Username; }
				if ($thisGuest->EmailedBy == $thisUser->id) { $emailedUsername = $thisUser->Username; }
			}
		}

		print "\t\t\t\t<td><strong>".$thisGuest->LastName.", ".$thisGuest->FirstName."</strong></td>\n";
		if ($thisGuest->Imported == 1) {
			print "\t\t\t\t<td colspan=\"6\" align=\"center\">Imported by <strong>".$importedUsername."</strong> to <strong>".$festivals_array[$thisGuest->ImportedFestival]."</strong> on <strong>".date("m-d-Y",strtotime($thisGuest->ImportedDate))."</strong>.</td>\n";
			print "\t\t\t\t<td><form id=\"guest-status-".$thisGuest->guest_id."\"><input type=\"hidden\" name=\"guest-id\" value=\"".$thisGuest->guest_id."\" />";
			print form_dropdown('guest-status', $status_array, $thisGuest->DelegateStatus," data-formname=\"#guest-status-".$thisGuest->guest_id."\" class='status select ui-widget-content ui-corner-all'");
			print "</form>\n";
			if ($emailedUsername != "") {
				print "<div id=\"guest-emailed-".$thisGuest->guest_id."\"><img src=\"/assets/images/icons/email.png\" width=\"16\" height=\"16\" /> ".$thisGuest->EmailedStatus." by ".$emailedUsername." on: ".date("m-d-Y h:ia",strtotime($thisGuest->EmailedDateTime))."</div>";
			} else {
				print "<div id=\"guest-emailed-".$thisGuest->guest_id."\"></div>";
			}
			print "</td>\n";
		} else {
			print "\t\t\t\t<td>".$thisGuest->Film."</td>\n";
			print "\t\t\t\t<td>".$thisGuest->FilmOrCompany."</td>\n";
			print "\t\t\t\t<td>".$thisGuest->TravelMethod."</td>\n";
			print "\t\t\t\t<td>".$thisGuest->DelegateType."</td>\n";
			print "\t\t\t\t<td>$".$thisGuest->Fee."</td>\n";
			print "\t\t\t\t<td>".date("m-d-Y",strtotime($thisGuest->SubmittedDate))."</td>\n";
			print "\t\t\t\t<td><form id=\"guest-status-".$thisGuest->guest_id."\"><input type=\"hidden\" name=\"guest-id\" value=\"".$thisGuest->guest_id."\" />";
			print form_dropdown('guest-status', $status_array, $thisGuest->DelegateStatus," data-formname=\"#guest-status-".$thisGuest->guest_id."\" class='status select ui-widget-content ui-corner-all'");
			print "</form>\n";
			if ($emailedUsername != "") {
				print "<div id=\"guest-emailed-".$thisGuest->guest_id."\"><img src=\"/assets/images/icons/email.png\" width=\"16\" height=\"16\" /> ".$thisGuest->EmailedStatus." by ".$emailedUsername." on: ".date("m-d-Y h:ia",strtotime($thisGuest->EmailedDateTime))."</div>";
			} else {
				print "<div id=\"guest-emailed-".$thisGuest->guest_id."\"></div>";
			}
			print "</td>\n";
		}

		print "\t\t\t</tr>\n";

		if ($thisGuest->Imported == 1) {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n"; }
		} else {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		}

		print "\t\t\t\t<td colspan=\"8\"><div id=\"guest-info-".$thisGuest->guest_id."\" class=\"hidden\">";
?>
				<fieldset class="ui-corner-all">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0;">
                	<tbody>
                	<tr>
						<td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Contact Information</b></td>
						<td rowspan="11" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="34%"><b>Flight Information</b></td>
                        <td rowspan="14" width="1"></td>
                        <td colspan="2" align="center" class="row_title bottom_border" width="33%"><b>Biography</b></td>
                    </tr><tr>
                        <td><label>Guest Name:</label></td><td><?php
                        	print $thisGuest->LastName.", ".$thisGuest->FirstName;
							if ($thisGuest->LegalName != "") { print "<br />(".$thisGuest->LegalName.")"; }
						?></td>
                        <td><label>Travel Method:</label></td><td><?php print $thisGuest->TravelMethod; ?></td>
                        <td colspan="2" rowspan="10"><?php print $thisGuest->Biography; ?></td>
                    </tr><tr>
                        <td><label>Film:</label></td><td><?php print $thisGuest->Film; ?></td>
                        <td><label>Arrival Date / Time:</label></td><td><?php
                        if ($thisGuest->ArrivalDate != "0000-00-00") { print date("m-d-Y",strtotime($thisGuest->ArrivalDate)); }
                        if ($thisGuest->ArrivalTime != "00:00:00") { print " ".date("g:i A",strtotime($thisGuest->ArrivalTime)); }
						?></td>

                    </tr><tr>
                        <td><label>Company:</label></td><td><?php print $thisGuest->FilmOrCompany; ?></td>
                        <td><label>Airline / Flight #:</label></td><td><?php
                        if ($thisGuest->ArrivalAirline != "--") { print $thisGuest->ArrivalAirline; }
						print " ".$thisGuest->ArrivalFlightNum;
						?></td>
                    </tr><tr>
                        <td><label>Address:</label></td><td><?php
                        	print $thisGuest->GuestAddress."<br />";
                        	print $thisGuest->GuestCity.", ";
                        	print $thisGuest->GuestState." ";
                        	print $thisGuest->GuestPostal."<br />";
                        	print $country_string;
						?></td>
                        <td><label>City:</label></td><td><?php
							print $thisGuest->ArrivalCity;
							if ($thisGuest->ArrivalCity != "" && $thisGuest->ArrivalCity2 != "") { print " - "; }
							print $thisGuest->ArrivalCity2;
						?></td>
                    </tr><tr>
                        <td><label>Phone(s):</label></td><td><?php
						if ($thisGuest->DayPhone != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/telephone.png\" alt=\"Primary Phone\" title=\"Primary Phone\"> ".$thisGuest->DayPhone."<br />";
						}
						if ($thisGuest->MobilePhone != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/phone.png\" alt=\"Mobile Phone\" title=\"Mobile Phone\"> ".$thisGuest->MobilePhone."<br />";
						}
						if ($thisGuest->FaxPhone != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/printer.png\" alt=\"Fax\" title=\"Fax\"> ".$thisGuest->FaxPhone;
						}
						?></td>
                        <td><label>Departure Date / Time:</label></td><td><?php
                        if ($thisGuest->DepartureDate != "0000-00-00") { print date("m-d-Y",strtotime($thisGuest->DepartureDate)); }
                        if ($thisGuest->DepartureTime != "00:00:00") { print " ".date("g:i A",strtotime($thisGuest->DepartureTime)); }
						?></td>
                    </tr><tr>
                        <td><label>Email:</label></td><td><?php
						if ($thisGuest->EmailAddress != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/email.png\" alt=\"Email\" title=\"Email\"> <a href=\"mailto:".$thisGuest->EmailAddress."\" target=\"_blank\">".$thisGuest->EmailAddress."</a>";
						}
						?></td>
                        <td><label>Airline / Flight #:</label></td><td><?php
                        if ($thisGuest->DepartureAirline != "--") { print $thisGuest->DepartureAirline; }
						print " ".$thisGuest->DepartureFlightNum;
						?></td>
                    </tr><tr>
                    	<td><label>Website:</label></td><td><a href="<?php
						$prefix = "http://";
						if (substr($thisGuest->WebsiteURL,0,4) == "www.") { print $prefix.$thisGuest->WebsiteURL; } else { print $thisGuest->WebsiteURL; }
						?>">[link]</a></td>
                        <td><label>City:</label></td><td><?php
							print $thisGuest->DepartureCity;
							if ($thisGuest->DepartureCity != "" && $thisGuest->DepartureCity2 != "") { print " - "; }
							print $thisGuest->DepartureCity2;
						 ?></td>
                    </tr><tr>
                        <td><label>Twitter Name:</label></td><td><?php
						if ($thisGuest->TwitterName != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/twitter.png\" alt=\"Twitter\" title=\"Twitter\"> <a href=\"http://twitter.com/".$thisGuest->TwitterName."\" target=\"_blank\">@".$thisGuest->TwitterName."</a><br />";
						}
						?></td>
                        <td>&nbsp;</td><td>&nbsp;</td>
                    </tr><tr>
                        <td><label>Facebook Page:</label></td><td><?php
						if ($thisGuest->FacebookPage != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/facebook.png\" alt=\"Facebook Page\" title=\"Facebook Page\"> <a href=\"".$thisGuest->FacebookPage."\" target=\"_blank\">".$thisGuest->FacebookPage."</a><br />";
						}
						?></td>
                        <td>&nbsp;</td><td>&nbsp;</td>
                    </tr><tr>
                        <td><label>LinkedIn Profile:</label></td><td><?php
						if ($thisGuest->LinkedinProfile != "") {
							print "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/linkedin.png\" alt=\"LinkedIn Profile\" title=\"LinkedIn Profile\"> <a href=\"".$thisGuest->LinkedinProfile."\" target=\"_blank\">".$thisGuest->LinkedinProfile."</a><br />";
						}
						?></td>
                        <td><label>Translator Required:</label></td><td><?php
                        if ($thisGuest->TranslatorLang != 0) {
                        	print $language_array[$thisGuest->TranslatorLang];
                        } else {
                        	print "None";
                        }
                        ?></td>
                    </tr><tr>
                        <td colspan="5" align="center" class="row_title bottom_border"><b>Miscellaneous Information</b></td>
                        <td colspan="2" align="center" class="row_title bottom_border"><b>Photo</b></td>
                    </tr><tr>
                        <td><label>Guest Type:</label></td><td><?php print $guesttypes_array[$thisGuest->DelegateCategory]; ?></td>
						<td>&nbsp;</td>
                        <td><label>Submission Date:</label></td><td><?php print date("m-d-Y",strtotime($thisGuest->SubmittedDate)); ?></td>
                        <td rowspan="2" colspan="2"><?php 
                        	if ($thisGuest->GuestPhotoUrl == "") {
                        		print "No Photo Uploaded";
                        	} else {
                        		print "<a href=\"".$thisGuest->GuestPhotoUrl."\" target=\"_blank\">Download Photo</a>";
                        	}
                        ?></td>
                    </tr><tr>
                        <td><label>Guest Status:</label></td><td><span id="guest-status-view-<?php print $thisGuest->guest_id; ?>"><?php print $thisGuest->DelegateStatus; ?></span></td>
						<td>&nbsp;</td>
                        <td><label>Badge Fee:</label></td><td>$<?php print $thisGuest->Fee; ?></td>
                    </tr>

                    <!--
                    <tr>
                        <td><label>Attending Opening?</label></td><td><?php //if ($thisGuest->AttendOpening == 0) { print "NO"; } else { print "YES"; } ?></td>
						<td>&nbsp;</td>
                        <td><label>Attending Closing?</label></td><td><?php //if ($thisGuest->AttendClosing == 0) { print "NO"; } else { print "YES"; } ?></td>
                    </tr>
	                -->
                	</tbody>
                </table>
                </fieldset>
<?php
		print "</div></td>\n";
		print "\t\t\t</tr>\n";

		//$Guest_Buttons .= "\t\t$('#guest-status-".$thisGuest->guest_id." SELECT').on('change', function() { update_guest_review_status('#guest-status-".$thisGuest->guest_id."'); return false; } );\n";

		$x++;
	}
?>
			</tbody>
		</table>
</fieldset>

<div id="guestImport-dialog" title="Import Guest">
<form name="guestImportForm" id="guestImportForm">
<input type="hidden" name="guest-import-id" id="guest-import-id" value="0" />
<input type="hidden" name="festival-import-id" id="festival-import-id" value="<?php print $festival[0]->id; ?>" />
<input type="hidden" name="user-import-id" id="user-import-id" value="<?php print $current_user[0]->id; ?>" />
	<p>You're trying to import <span id="guest_name">[guest name]</span> as a guest of the <b><?php print $festival[0]->year." ".$festival[0]->name; ?></b>.</p>
    <table class="ImportDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td>
				<label for="status-import">Guest Status:</label> <span class="req">*</span><br />
                <?php print form_dropdown('status-import', $status_array, 0,"id='status-import' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td>
				<label for="guesttype-import">Choose a Guest Type:</label> <span class="req">*</span><br />
                <?php print form_dropdown('guesttype-import', $guesttypes_array, 0,"id='guesttype-import' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
        </tr>
        <tr valign="top">
            <td>
	            <label for="company-import">Company</label><br />
    	        <input type="text" name="company-import" id="company-import" class="text ui-widget-content ui-corner-all" /><br />
            </td>
            <td>
	            <label for="film-import">Film</label><br />
    	        <input type="text" name="film-import" id="film-import" class="text ui-widget-content ui-corner-all" /><br />
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="fee-import">Fee:</label> <span class="req">*</span><br />
                $<?php print form_input(array('name'=>'fee-import', 'id'=>'fee-import', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
            </td>
            <td>
            	<label for="filmaff-import">Film Affiliation:</label><br />
            	<select name="filmaff-import" id="filmaff-import" class="select ui-widget-content ui-corner-all"><option value="0">Select a Film</option>
				<?php foreach ($films as $rec) {	print "\t\t<option value=\"".$rec->movie_id."\">".switch_title($rec->title_en)."</option>\n"; } ?>
            	</select>
            	<div id="replaceMeImport"></div>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<div id="guestEmail-dialog" title="Email Guest">
<form name="guestEmailForm" id="guestEmailForm">
<input type="hidden" name="guest-email-id" id="guest-email-id" value="0" />
<input type="hidden" name="guest-email-status" id="guest-email-status" value="" />
<input type="hidden" name="user-email-id" id="user-email-id" value="<?php print $current_user[0]->id; ?>" />
<input type="hidden" name="emailto-email" id="emailto-email" value="" />
<input type="hidden" id="default-from-email" value="guestservices@hiff.org" />
<input type="hidden" id="approved-subject" value="Congratulations! Your accreditation request for the 2017 Hawaii International Film Festival has been approved." />
<input type="hidden" id="declined-subject" value="Your accreditation request for the 2017 Hawaii International Film Festival has been declined." />
<textarea id="approved-message" style="display:none;">You are officially an accredited guest of​ ​HIFF 2017.​ ​We will soon be sending you an email with more details ​about your festival accreditation, where to check in at the festival, pick up your badge and its various benefits. I​n the meantime, i​f you have any questions,​ ​please contact us at anytime.
 
Thank you for​ ​participating in Hawaii International Film Festival 2017.​ ​We look forward to seeing you at the festival!

HIFF Guest Services
37th Annual Hawaii International Film Festival
Presented by Halekulani | November 2 - 12, 2017
For more info, visit www.hiff.org
www.facebook.com/hawaiiiff​ | ​Twitter @HIFF | Instagram @hiffhawaii</textarea>
<textarea id="declined-message" style="display:none;">Your delegate application has not been approved at this time, as ​you didn't meet the qualifications. In order to be considered an accredited delegate, you must be at least one of the following:

- A filmmaker with a film in this year’s festival​ (1 badge per short film, 2 per feature length film)​
- A participant in any of our ​panels or jury
​- An approved m​ember of the press
- An invited ​industry ​guest 

We apologize for the inconvenience.​ ​If you feel that there has been an error, or have any questions regarding this, please respond to this email as soon as possible. ​

If you would​ ​​like to purchase tickets online, you can do so here: http://www.hiff.org/films-schedule/

HIFF Guest Services
37th Annual Hawaii International Film Festival
Presented by Halekulani | November 2 - 12, 2017
For more info, visit www.hiff.org
www.facebook.com/hawaiiiff​ | ​Twitter @HIFF | Instagram @hiffhawaii</textarea>
	<p>You just set the status for <span id="email_guest_name">[guest name]</span> to <span id="email_guest_status">[guest status]</span>. Would you like to email a notification to them?</p>
    <table class="ImportDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td>
				<label for="emailto-email">To:</label> <span class="req">*</span><br />
				<span id="email_guest_email">[guest email]</span>
            </td>
            <td>
				<label for="emailfrom-email">From:</label> <span class="req">*</span><br />
    	        <input type="text" name="emailfrom-email" id="emailfrom-email" class="text ui-widget-content ui-corner-all" /><br />
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
	            <label for="subject-email">Subject:</label> <span class="req">*</span><br />
    	        <input type="text" name="subject-email" id="subject-email" class="text-large ui-widget-content ui-corner-all" /><br />
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
	            <label for="message-email">Message:</label> <span class="req">*</span><br />
    	        <textarea name="message-email" id="message-email" class="text ui-widget-content ui-corner-all"></textarea>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<script type="text/javascript">
	$(function() {
		$("input:submit").button();
		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});		

		$("#guestImportForm").validate({
			rules: { "status-import": "required", "guesttype-import": { required:true, min:1 }, "fee-import": "required" },
			messages: { "status-import": "Please select a status.", "guesttype-import": "Please enter a guest type.", "fee-import": "Please enter a fee.", Email: "Please enter a valid email address.", WebsiteURL: "Please enter a valid URL (including http://)." }
		});
		$("#guestImport-dialog").dialog({
			autoOpen: false,
			height: 430,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
<?php if ($this->session->userdata('limited') == 0) { ?>
				'DELETE': function() {
					var decision = confirm("Are you sure you really want to delete this Accreditation Form? This cannot be undone.");
					if (decision == true) {
						var toUpdate = $('#guest-import-id').val();

						$.ajax({
							success: function(msg, text, xhr) {
								var button = "#guest-review-"+toUpdate;
								$(button).closest("tr").next("tr").remove();
								$(button).closest("tr").remove();
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#guestImportForm').serialize(),
							url: '/admin/guest_review/delete_accred/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
					}
				},
<?php } ?>
				'Import Guest': function() {
					if ($("#guestImportForm").validate().form() == true) {
						var toUpdate = $('#guest-import-id').val();

						$.ajax({
							success: function(msg, text, xhr) {								
								$('#guest-review-'+toUpdate).closest('tr').replaceWith(msg);

								if ($('#rowtype-'+toUpdate) == "oddrow") {
									$('#guest-review-'+toUpdate).closest('tr').next('tr').removeClass('oddrow').addClass('oddrow-imported');
								} else if ($('#rowtype-'+toUpdate) == "evenrow") {
									$('#guest-review-'+toUpdate).closest('tr').next('tr').removeClass('evenrow').addClass('evenrow-imported');
								}
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#guestImportForm').serialize(),
							url: '/admin/guest_review/import_guest/',
							type: 'POST',
							dataType: 'html'
						});
						$(this).dialog('close');
		            }
				}
			},
			close: function() {
				$("#guestImportForm INPUT[type='text']").each(function() { $(this).val(""); });
				$("#guestImportForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
				$("#guestImportForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
				$('#guest-import-id').val(0);
			}
		});

		$("#guestEmailForm").validate({
			rules: { "emailto-email": { required:true, email:true }, "emailfrom-email": { required:true, email:true }, "subject-email": "required", "message-email": "required" },
			messages: { "emailto-email": "Please provide a valid 'to' email address.", "emailfrom-email": "Please provide a valid 'from' email address.", "subject-email": "Subject cannot be blank.", "message-email": "Message cannot be blank." }
		});
		$("#guestEmail-dialog").dialog({
			autoOpen: false,
			height: 430,
			width: 600,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Send Message': function() {
					if ($("#guestEmailForm").validate().form() == true) {
						var toUpdate = $('#guest-email-id').val();
						$.ajax({
							success: function(msg, text, xhr) {
								$("#guest-emailed-"+toUpdate).html(msg);
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#guestEmailForm').serialize(),
							url: '/admin/guest_review/send_email_confirmation/',
							type: 'POST',
							dataType: 'html'
						});
						$(this).dialog('close');
		            }
				}
			},
			close: function() {
				$("#guestEmailForm INPUT[type='text']").each(function() { $(this).val(""); });
				//$("#guestEmailForm TEXTAREA").each(function() { $(this).val(""); });
				$('#guest-email-id').val(0);
			}
		});

		$('.GuestReviewTab').on('click','button.import', function(e) {
			e.preventDefault();
			open_guest_import_dialog($(this).data('id'),$(this).val().toString());
		});
		$('.GuestReviewTab').on('click','button.view', function(e) {
			e.preventDefault();
			show_guest_detail($(this).data('id'));
		});
		$('.GuestReviewTab').on('change','select.status', function(e) {
			e.preventDefault();
			update_guest_review_status($(this).data('formname').toString());
		});

		jQuery(".GuestReviewTab").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
		$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });
	});
</script>