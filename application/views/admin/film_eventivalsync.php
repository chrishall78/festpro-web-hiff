<style>
	#output {
		width: 100%; height: 600px; margin-top: 10px; padding: 0 10px; overflow: auto;
		background-color: #fafafa; border-radius: 5px; border: 1px solid black; box-sizing: border-box;
	}
	#output p { line-height: 120%; }
	.error { color: red; }
	.success { color: green; }
</style>

<h2>FestPro / Eventival Sync</h2>
<div id="sync_wrapper">
	<p>This page will let you retrieve data from Eventival's Web Services for the project and edition listed below. It will then be inserted into FestPro for the current festival. Please be careful, since any existing films and screenings for the current festival will be automatically removed before the new ones are added.</p>
	<p><label>Eventival Project:</label> <?php print $project; ?></p>
	<p><label>Eventival Edition:</label> <?php print $edition; ?></p>
	<p><label>Eventival API URL:</label> <?php print $api_url; ?></p>
	<p><label>Date/time last synced:</label> <?php print $last_sync; ?></p>
	<p><label>Will films &amp; screenings be automatically published?</label> <?php print "Films: ".$film_publish." Screenings: ".$screening_publish; ?></p>
	<p><button id="start_sync">Start Eventival Sync</button> &nbsp; <button id="clear_output">Clear Sync Output</button></p>
	<label>Sync Output:</label><br>
	<div id="output">

	</div>
</div>

<script type="text/javascript">
	$(function() {
		$("input:submit").button();
		$("button").button();

		$("#start_sync").on("click", function(event) {
			syncButton = $(this);
			syncButton.button("disable");
			$("#output").append("<p>Starting sync. This may take a few minutes to finish.</p>")
			$("#output").append("<p id=\"loading1\"><img src=\"/assets/images/loading.gif\" border=\"0\"></p>")

			jqXHR1 = jQuery.ajax({
				url: '/admin/film_eventivalsync/get_xml/',
				type: 'GET',
				dataType: 'html'
			}).done(function(data, textStatus, jqXHR) {
				$("#loading1").slideUp("slow");
				$("#output").append(data);
				$("#output").append("<p class=\"success\">Successfully retrieved film and screening information from Eventival.</p>")
				$("#output").append("<p>Processing information and inserting into the database...</p>")
				$("#output").append("<p id=\"loading2\"><img src=\"/assets/images/loading.gif\" border=\"0\"></p>")

				jqXHR2 = jQuery.ajax({
					url: '/admin/film_eventivalsync/sync/',
					type: 'GET',
					dataType: 'html'
				}).done(function(data2, textStatus2, jqXHR2) {
					$("#loading2").slideUp("slow");
					$("#output").append("<p class=\"success\">Film and screening information processed and inserted into FestPro. See details below.</p>")
					$("#output").append(data2);
					syncButton.button("enable");
				}).fail(function(jqXHR2, textStatus2, errorThrown2){
					//console.log( "Failure! " + jqXHR2 + textStatus2 + errorThrown2);
					$("#loading2").slideUp("slow");
					$("#output").append("<p class=\"error\">The request to sync() failed! Aborting sync.</p>");
					syncButton.button("enable");
				});
			}).fail(function(jqXHR, textStatus, errorThrown){
				//console.log( "Failure! " + jqXHR + textStatus + errorThrown);
				$("#loading1").slideUp("slow");
				$("#output").append("<p class=\"error\">The request to get_xml() failed! Aborting sync.</p>");
				syncButton.button("enable");
			});
		});

		$("#clear_output").on("click", function(event) {
			var decision = confirm("Are you sure you want to clear the sync output? This will not stop a sync that is currently in progress.");
			if (decision == true) {
				$("#output").html("");
			}
		});

	});
</script>
