<?php

if (isset($updated)) {
	print "<p align=\"center\"><strong>The program updates page for ".$festival[0]->year." ".$festival[0]->name." has been successfully updated.</strong></p>";
}
?>

<fieldset class="ui-corner-all">
<form action="/admin/film_updates/save" method="post">
<input type="hidden" name="festival_to_update" id="festival_to_update" value="<?php print $festival[0]->id; ?>" />
<div style="margin:5px 10px 5px 0; float:left">
<?php print form_submit(array('name'=>'Save', 'id'=>'Save', 'value'=>'SAVE')); ?>
</div>
<p><label for="program_updates">Program Updates</label></p>
<p>The contents of this text editor will show up under the 'Program Updates' page visible to the audience for this festival. Use it to announce schedule changes, new films, etc. It's recommended to have the updates listed most recent first, from the end of the festival working backwards.</p>
<?php print form_textarea(array('name'=>'program_updates', 'id'=>'program_updates', 'value'=>$festival[0]->program_updates, 'rows'=>'6', 'cols'=>'55', 'class'=>'ckeditor')); ?>
<p><label for="pdf_flyer_html">PDF Flyer Text</label></p>
<p>The contents of this text editor will show up on the right side of the generated PDF film/event flyers.</p>
<?php print form_textarea(array('name'=>'pdf_flyer_html', 'id'=>'pdf_flyer_html', 'value'=>$festival[0]->pdf_flyer_html, 'rows'=>'6', 'cols'=>'55', 'class'=>'ckeditor')); ?>
</form>
</fieldset>

<script type="text/javascript">
	$(function() {
		$("input:submit").button();
		$("button").button();
	});
</script>
