<style>
.indent { margin-left: 25px; }
h3.dashboard-title { text-align: left; color: #000000; }
.dashboard-fourth { width:21.9%; margin: 12px 0.5% 0; padding: 10px; border: 1px solid black; border-radius: 5px; background-color: #EEEEEE; }
.dashboard-fourth p { font-weight: bold; color: #666666; margin-bottom: 0; margin-top: 5px; }
.dashboard-fourth p.info { font-weight: normal; font-style: italic; }
.dashboard-fourth p span { display: inline-block; width: 22px; font-weight: bold; color: #000000; }
.dashboard-fourth p span.wide { width: 42px; }
.dashboard-fourth div { border: 1px solid black; height: 4px; }
.dashboard-fourth div span.percentage { display: block; background-color: #F7941D; height: 4px; margin: 0; }
#sections > p, #premieres > p, #event-types > p, #locations > p, #video-formats > p,
#couriers > p, #screening-dates > p, #countries > p, #languages > p, #genres > p { margin-top: 0; }
<?php if ( count($countries) > 20) { ?>
	#countries > p { float:left; width: 50%; }
	#countries > p.info { float:left; width: 100%; }
<?php } if ( count($languages) > 20) { ?>
	#languages > p { float:left; width: 50%; }
	#languages > p.info { float:left; width: 100%; }
<?php } if ( count($genres) > 20) { ?>
	#genres > p { float:left; width: 50%; }
	#genres > p.info { float:left; width: 100%; }
<?php } ?>
</style>

<h2>Festival Dashboard - <?php print $festival[0]->year." ".$festival[0]->name; ?></h2>
<div id="dashboard_wrapper">
	<div class="dashboard-fourth" id="films-events">
		<h3 class="dashboard-title">Films/Events</h3>
	<?php
		$film_total = $dashboard["total-films"];
		$f01 = $f02 = $f03 = $f04 = $f05 = $f06 = $f07 = $f08 = 0;
		if ($film_total > 0) {
			//$f01 = round(($dashboard["total-synopses"]/$film_total) * 100,1);
			$f02 = round(($dashboard["long-synopsis"]/$film_total) * 100,1);
			$f03 = round(($dashboard["short-synopsis"]/$film_total) * 100,1);
			$f04 = round(($dashboard["photos"]/$film_total) * 100,1);
			$f05 = round(($dashboard["videos"]/$film_total) * 100,1);
			$f06 = round(($dashboard["scheduled"]/$film_total) * 100,1);
			$f07 = round(($dashboard["confirmed"]/$film_total) * 100,1);
			$f08 = round(($dashboard["published"]/$film_total) * 100,1);
		}

		$screening_total = $dashboard["total-screenings"];
		$s01 = $s02 = $s03 = $s04 = $s05 = $s06 = $s07 = $s08 = $s09 = 0;
		if ($screening_total > 0) {
			$s01 = round(($dashboard["public-screenings"]/$screening_total) * 100,1);
			$s02 = round(($dashboard["private-screenings"]/$screening_total) * 100,1);
			$s03 = round(($dashboard["standby-screenings"]/$screening_total) * 100,1);
			$s04 = round(($dashboard["free-screenings"]/$screening_total) * 100,1);
			$s05 = round(($dashboard["published-screenings"]/$screening_total) * 100,1);
		}
		$scheduled_total = $dashboard["scheduled"];
		if ($scheduled_total > 0) {
			$s06 = round(($dashboard["screened-1"]/$scheduled_total) * 100,1);
			$s07 = round(($dashboard["screened-2"]/$scheduled_total) * 100,1);
			$s08 = round(($dashboard["screened-3"]/$scheduled_total) * 100,1);
			$s09 = round(($dashboard["screened-4plus"]/$scheduled_total) * 100,1);
		}

		$inbound = $dashboard["inbound-prints"];
		$outbound = $dashboard["outbound-prints"];
		$p01 = $p02 = $p03 = $p04 = 0;
		if ($inbound > 0) {
			$p01 = round(($dashboard["inbound-received"]/$inbound) * 100,1);
			$p02 = round(($dashboard["inbound-not-received"]/$inbound) * 100,1);
		}
		if ($outbound > 0) {
			$p03 = round(($dashboard["outbound-sent"]/$outbound) * 100,1);
			$p04 = round(($dashboard["outbound-not-sent"]/$outbound) * 100,1);
		}

		print "<p><span>".$dashboard["total-films"]."</span> Total Films/Events</p>\n";
			//print "<p class=\"indent\"><span>".$dashboard["total-synopses"]."</span> (".$f01."%) Total Synopses (Long or Short)</p>\n";
			//print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f01."%;\" title=\"".$f01."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["long-synopsis"]."</span> Long Synopses</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f02."%;\" title=\"".$f02."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["short-synopsis"]."</span> Short Synopses</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f03."%;\" title=\"".$f03."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["photos"]."</span> Photos (1 or more - ".count($photos)." total)</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f04."%;\" title=\"".$f04."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["videos"]."</span> Videos (1 or more - ".count($videos)." total)</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f05."%;\" title=\"".$f05."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["scheduled"]."</span> Scheduled</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f06."%;\" title=\"".$f06."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["confirmed"]."</span> Confirmed</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f07."%;\" title=\"".$f07."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["published"]."</span> Published</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$f08."%;\" title=\"".$f08."%\"></span></div>";
		print "<p><span>".$dashboard["screened-1"]."</span> Films Screened 1 Time</p>\n";
		print "<div><span class=\"percentage\" style=\"width:".$s06."%;\" title=\"".$s06."%\"></span></div>";
		print "<p><span>".$dashboard["screened-2"]."</span> Films Screened 2 Times</p>\n";
		print "<div><span class=\"percentage\" style=\"width:".$s07."%; margin-left:".intval($s06)."%\" title=\"".$s07."%\"></span></div>";
		print "<p><span>".$dashboard["screened-3"]."</span> Films Screened 3 Times</p>\n";
		print "<div><span class=\"percentage\" style=\"width:".$s08."%; margin-left:".intval($s06+$s07)."%\" title=\"".$s08."%\"></span></div>";
		print "<p><span>".$dashboard["screened-4plus"]."</span> Films Screened 4+ Times</p>\n";
		print "<div><span class=\"percentage\" style=\"width:".$s09."%; margin-left:".intval($s06+$s07+$s08)."%\" title=\"".$s09."%\"></span></div>";
	?>
	</div>

	<div class="dashboard-fourth" id="screenings">
		<h3 class="dashboard-title">Screenings</h3>
	<?php
		print "<p><span>".$dashboard["total-screenings"]."</span> Total Screenings</p>\n";
			print "<p class=\"indent\"><span>".$dashboard["public-screenings"]."</span> Public</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$s01."%;\" title=\"".$s01."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["private-screenings"]."</span> Private</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$s02."%;\" title=\"".$s02."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["standby-screenings"]."</span> Standby</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$s03."%;\" title=\"".$s03."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["free-screenings"]."</span> Free</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$s04."%;\" title=\"".$s04."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["published-screenings"]."</span> Published</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$s05."%;\" title=\"".$s05."%\"></span></div>";
		print "<p><span>".$dashboard["unique-programs"]."</span> Unique Programs</p>\n";
		print "<p><span>".$dashboard["shorts-programs"]."</span> Shorts Programs (4+ Films)</p>\n";
		print "<p><span class=\"wide\">".number_format($dashboard["audience-attendance"])."</span> Total Audience Attendance</p>\n";
		if ($dashboard["avg-audience-attendance"] > 0) {
			$average_attendance = floatval($dashboard["audience-attendance"] / $dashboard["avg-audience-attendance"]);
			print "<p><span class=\"wide\">".number_format($average_attendance)."</span> Average Audience (".$dashboard["avg-audience-attendance"].")</p>\n";
		} else {
			print "<p><span class=\"wide\">0</span> Average Audience (No data)</p>\n";
		}
		print "<p><span class=\"wide\">".number_format($dashboard["qa-counts"])."</span> Total Q&amp;A Attendance</p>\n";
		if ($dashboard["avg-qa-counts"] > 0) {
			$average_qa = floatval($dashboard["qa-counts"] / $dashboard["avg-qa-counts"]);
			print "<p><span class=\"wide\">".number_format($average_qa)."</span> Average Q&amp;A (".$dashboard["avg-qa-counts"].")</p>\n";
		} else {
			print "<p><span class=\"wide\">0</span> Average Q&amp;A (No data)</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="print-traffic">
		<h3 class="dashboard-title">Print Traffic</h3>
	<?php
		print "<p><span>".$dashboard["inbound-prints"]."</span> Inbound Shipments</p>\n";
			print "<p class=\"indent\"><span>".$dashboard["inbound-received"]."</span> Recieved</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$p01."%;\" title=\"".$p01."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["inbound-not-received"]."</span> Not Received</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$p02."%;\" title=\"".$p01."%\"></span></div>";
		print "<p><span>".$dashboard["outbound-prints"]."</span> Outbound Shipments</p>\n";
			print "<p class=\"indent\"><span>".$dashboard["outbound-sent"]."</span> Sent</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$p03."%;\" title=\"".$p01."%\"></span></div>";
			print "<p class=\"indent\"><span>".$dashboard["outbound-not-sent"]."</span> Not Sent</p>\n";
			print "<div class=\"indent\"><span class=\"percentage\" style=\"width:".$p04."%;\" title=\"".$p01."%\"></span></div>";
		print "<p>&nbsp;</p>\n";
	?>
		<section id="couriers">
		<h3 class="dashboard-title">Package Couriers Used (<?php print count($couriers); ?>)</h3>
		<p class="info">Shipments per Courier</p>
	<?php
		if (count($couriers) == 0) { print "<p>No Courier data available</p>"; }
		foreach ($couriers as $name => $number) {
			print "<p><span>".$number."</span> ".$name."</p>\n";
		}
	?>
		</section>
	</div>

	<div class="dashboard-fourth" id="sections">
		<h3 class="dashboard-title">Film Sections (<?php print count($sections); ?>)</h3>
		<p class="info">Films per section</p>
	<?php
		if (count($sections) == 0) { print "<p>None</p>"; }
		foreach ($sections as $thisType) {
			print "<p><span>".$thisType["count"]."</span> ".$thisType["name"]."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="locations">
		<h3 class="dashboard-title">Screening Locations (<?php print count($locations); ?>)</h3>
		<p class="info">Screenings per location</p>
	<?php
		if (count($locations) == 0) { print "<p>None</p>"; }
		foreach ($locations as $name => $number) {
			print "<p><span>".$number."</span> ".$name."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="screening-dates">
		<h3 class="dashboard-title">Daily Festival Screenings (<?php print count($screeningDays); ?>)</h3>
	<?php
		if (count($screeningDays) == 0) { print "<p>None</p>"; }
		foreach ($screeningDays as $date => $number) {
			print "<p><span>".$number."</span> ".date("D, F j, Y", strtotime($date))."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="countries">
		<h3 class="dashboard-title">Countries Represented (<?php print count($countries); ?>)</h3>
		<p class="info">Films per country</p>
	<?php
		if (count($countries) == 0) { print "<p>None</p>"; }
		foreach ($countries as $name => $number) {
			print "<p><span>".$number."</span> ".$name."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="premieres">
		<h3 class="dashboard-title">Premieres (<?php print count($premieres); ?>)</h3>
		<p class="info">Films per premiere type</p>
	<?php
		if (count($premieres) == 0) { print "<p>None</p>"; }
		foreach ($premieres as $thisType) {
			print "<p><span>".$thisType["count"]."</span> ".$thisType["name"]."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="event-types">
		<h3 class="dashboard-title">Event Types (<?php print count($eventtypes); ?>)</h3>
		<p class="info">Films per event type</p>
	<?php
		if (count($eventtypes) == 0) { print "<p>None</p>"; }
		foreach ($eventtypes as $thisType) {
			print "<p><span>".$thisType["count"]."</span> ".$thisType["name"]."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="video-formats">
		<h3 class="dashboard-title">Video Formats Used (<?php print count($videoformats); ?>)</h3>
		<p class="info">Films per video format</p>
	<?php
		if (count($videoformats) == 0) { print "<p>None</p>"; }
		foreach ($videoformats as $thisType) {
			print "<p><span>".$thisType["count"]."</span> ".$thisType["name"]."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="languages">
		<h3 class="dashboard-title">Languages Represented (<?php print count($languages); ?>)</h3>
		<p class="info">Films per language</p>
	<?php
		if (count($languages) == 0) { print "<p>None</p>"; }
		foreach ($languages as $name => $number) {
			print "<p><span>".$number."</span> ".$name."</p>\n";
		}
	?>
	</div>

	<div class="dashboard-fourth" id="genres">
		<h3 class="dashboard-title">Genres Represented (<?php print count($genres); ?>)</h3>
		<p class="info">Films per genre</p>
	<?php
		if (count($genres) == 0) { print "<p>None</p>"; }
		foreach ($genres as $name => $number) {
			print "<p><span>".$number."</span> ".$name."</p>\n";
		}
	?>
	</div>

</div>

<script type="text/javascript">
	$(function() {
		$("input:submit").button();
		$("button").button();

		$('#dashboard_wrapper').isotope({ itemSelector: '.dashboard-fourth', 'layoutMode': 'masonry' });
	});
</script>
