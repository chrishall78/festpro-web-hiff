<?php
$section_array = convert_to_array2($sections);
?>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Theater Ops</a></li>
		<li><a href="#tabs-2">Print Traffic Shipments</a></li>
		<li><a href="#tabs-3">Guides Export</a></li>
		<li><a href="#tabs-4">Ballot Counts</a></li>
		<li><a href="#tabs-5">Film Views</a></li>
		<li><a href="#tabs-6">Festival Profit/Loss</a></li>
		<li><a href="#tabs-7">Competitions</a></li>
		<li><a href="#tabs-8">Misc</a></li>
	</ul>


	<div id="tabs-1">
		<fieldset class="ReportsTab ui-corner-all">

		<h3>Theater Operations - Daily Reports</h3>
		<p align="center">Please choose a specific festival date or "All Dates" to get a list of daily report previews and export options.</p>
		<table width="100%" cellpadding="5" cellspacing="0">
			<tbody style="border:none;">
				<tr valign="top" align="center">
		<?php
			$cols = count($schedule); $x = 1;

			foreach ($schedule as $thisDate) {
				if ($thisDate == "All Dates") {
					$dateID = "all-dates";
					print "\t\t\t<td><button id=\"".$dateID."\">".$thisDate."</button></td>\n";
				} else {
					$dateID = $thisDate;
					print "\t\t\t<td><button id=\"".$dateID."\">".date("m/d",strtotime($thisDate))."</button></td>\n";
				}
				if ($x % 12 == 0) { print "</tr><tr valign=\"top\" align=\"center\">"; }
				$x++;
			}
		?>
				</tr>
			</tbody>
		</table><br />
		
		<div id="reportsTheaterOps">
		</div>

		<form id="file_download" name="file_download" action="/admin/film_reports/file_download/" method="post">
			<input type="hidden" id="file_name" name="file_name" value="theater-ops-export.csv" />
			<textarea id="file_content" name="file_content" style="display:none"></textarea>
		</form>

		</fieldset>
	</div>

	<div id="tabs-2">
		<fieldset class="ReportsTab ui-corner-all">
		<h3>Print Traffic Shipments</h3>
		<p align="center">Please choose a report to run. A preview will appear below, with an option to export to a CSV file.</p>
		<table width="100%" cellpadding="5" cellspacing="0">
			<tbody style="border:none;">
				<tr valign="top" align="center">
					<td><button id="inboundShipments">Inbound Shipments</button></td>
					<td><button id="outboundShipments">Outbound Shipments</button></td>
				</tr>
			</tbody>
		</table><br />
		
		<div id="reportsPT">
		</div>

		</fieldset>
	</div>

	<div id="tabs-3">
		<fieldset class="ReportsTab ui-corner-all">
		<h3>Preview and Export to Guides</h3>
		<p align="center">Please choose a report to run. A preview will appear below, with an option to export to a CSV file.<br />Note: These reports only include films marked as <strong>CONFIRMED</strong>. Schedule does not include private screenings.</p>
		<table width="100%" cellpadding="5" cellspacing="0">
			<tbody style="border:none;">
				<tr valign="top" align="center">
					<td><button id="allFilms">All Films</button></td>
					<td><button id="tableOfContents">Table of Contents</button></td>
					<td><button id="filmSchedule">Film Schedule</button></td>
					<td><button id="filmTrailers">Film Trailers</button></td>
				</tr>
				<tr valign="top" align="center">
					<td><button id="countryIndex">Country Index</button></td>
					<td><button id="directorIndex">Director Index</button></td>
					<td><button id="filmIndex">Film Index</button></td>
					<td><button id="printSourceIndex">Print Source Index</button></td>
				</tr>
			</tbody>
		</table><br />
		
		<div id="reportsGuideExp">
		</div>

		</fieldset>
	</div>

	<div id="tabs-4">
		<fieldset class="ReportsTab ui-corner-all">
			<p><b>Tabulating methodology:</b> Since we are collecting votes on a 1 to 5 scale, a vote for 1 will cancel out a vote for 5, and the same with 2 and 4, while 3 remains neutral.<br />
			<b>Adjusting the total:</b> Since not all the screenings happen in the same size theater, the totals need to be normalized to the same number of seats. Then a bonus of +1 is given for every vote that exceeds the average number of theater seats.</p>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="BallotCounts">
				<thead>
					<tr valign="top">
						<th width="220" style="text-align:left;">Film Name (<?php print count($films); ?>) 
						<form action="/admin/film_reports/export_ballot_counts/" method="post" id="ballot-count-export" style="display:inline;"> <button id="ballot-count-btn">Export</button><br></form>
						</th>
						<th width="115" style="text-align:left;">Film Type</th>
						<th width="60">House Seats</th>
						<th width="60"># of Votes</th>
						<th width="65">1 Votes<br />(# x -3)</th>
						<th width="65">2 Votes<br />(# x -2)</th>
						<th width="65">3 Votes<br />(# x 1)</th>
						<th width="65">4 Votes<br />(# x 2)</th>
						<th width="65">5 Votes<br />(# x 3)</th>
						<th width="60">Vote Total</th>
						<th width="60">Adjusted Total</th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		$novotes = 0; $prev_name = ""; $seat_array = array();
		$avg_num_votes = $avg_rating = $temp_votes = $temp_rating = $vote_count = $seats = $seat_total = $avg_seats = 0;
		$doc_count = $nar_count = $sho_count = 0;
		$doc_win = $nar_win = $sho_win = "";

		foreach ($films as $thisFilm) {
			$seatcount = 0;
			foreach ($full_schedule as $thisScreening) {
				$movie_ids = explode(",", $thisScreening->program_movie_ids);
				foreach ($movie_ids as $thisMovieID) { if ($thisFilm->movie_id == $thisMovieID) { $seatcount = $thisScreening->seats; } }
			}
		
			if ($thisFilm->votes_1 == 0 && $thisFilm->votes_2 == 0 && $thisFilm->votes_3 == 0 && $thisFilm->votes_4 == 0 && $thisFilm->votes_5 == 0) {
				// No votes
			} else {
				$temp_votes = $temp_votes + ($thisFilm->votes_1 + $thisFilm->votes_2 + $thisFilm->votes_3 + $thisFilm->votes_4 + $thisFilm->votes_5);
				$temp_rating = $temp_rating + $thisFilm->votes_total;
				if ($seatcount != 0) {
					$seat_array[] = $seatcount;
				}
				$vote_count++;
			}
		}
		
		if ($vote_count != 0) {
			$avg_num_votes = round($temp_votes / $vote_count);
			$avg_rating = round($temp_rating / $vote_count);

			$unique_seats = array_unique($seat_array);
			foreach ($unique_seats as $useats) { $seat_total = $seat_total + $useats; }
			$avg_seats = round($seat_total / count($unique_seats));

			foreach ($films as $thisFilm) {
				if ($prev_name == $thisFilm->title_en) {
					// skip duplicate row
				} else {
					if ($thisFilm->votes_1 == 0 && $thisFilm->votes_2 == 0 && $thisFilm->votes_3 == 0 && $thisFilm->votes_4 == 0 && $thisFilm->votes_5 == 0) {
						// Do not display, no votes
					} else {
						$seatcount = 0;
						$location_id2 = 0;
						foreach ($full_schedule as $thisScreening) {
							$movie_ids = explode(",", $thisScreening->program_movie_ids);
							foreach ($movie_ids as $thisMovieID) { if ($thisFilm->movie_id == $thisMovieID && $thisScreening->Private != 1 && $thisScreening->location_id != 15) { $seatcount = $thisScreening->seats; $location_id2 = $thisScreening->location_id2; break(2); } }
						}
	
						if ($location_id2 == 0) { $seats = $seatcount; } else { $seats = $seatcount * 2; }

						$calc_1 = $thisFilm->votes_1 * -3;
						$calc_2 = $thisFilm->votes_2 * -2;
						$calc_3 = $thisFilm->votes_3;
						$calc_4 = $thisFilm->votes_4 * 2;
						$calc_5 = $thisFilm->votes_5 * 3;
						$total = $thisFilm->votes_1 + $thisFilm->votes_2 + $thisFilm->votes_3 + $thisFilm->votes_4 + $thisFilm->votes_5;
	
						$adjusted_total = round(($thisFilm->votes_total * ($avg_seats / $seats)));
						if ($total > $avg_seats) { $adjusted_total = $adjusted_total + ($total - $avg_seats); }
						
						//$bayesian_rating = round((($avg_num_votes * $avg_rating) + ($total * $adjusted_total)) / ($avg_num_votes + $adjusted_total),2);
						print "\t\t\t\t\t<tr valign=\"top\">\n";
						print "\t\t\t\t\t\t<td><a href=\"/admin/film_edit/update/".$thisFilm->slug."\">".switch_title($thisFilm->title_en)."</a></td>";
						print "\t\t\t\t\t\t<td>".$thisFilm->event_name."</td>";
						if ($total > $seats) {
							print "\t\t\t\t\t\t<td class=\"cent\"><span style=\"color:red;\">".$seats."</span></td>";
							print "\t\t\t\t\t\t<td class=\"cent\"><span style=\"color:red;\">".$total."</span></td>";
						} else {
							print "\t\t\t\t\t\t<td class=\"cent\">".$seats."</td>";
							print "\t\t\t\t\t\t<td class=\"cent\">".$total."</td>";
						}
						print "\t\t\t\t\t\t<td class=\"cent lborder\">".$calc_1."</td>";
						print "\t\t\t\t\t\t<td class=\"cent\">".$calc_2."</td>";
						print "\t\t\t\t\t\t<td class=\"cent\">".$calc_3."</td>";
						print "\t\t\t\t\t\t<td class=\"cent\">".$calc_4."</td>";
						print "\t\t\t\t\t\t<td class=\"cent\">".$calc_5."</td>";
						print "\t\t\t\t\t\t<td class=\"cent lborder\">".$thisFilm->votes_total."</td>";
						print "\t\t\t\t\t\t<td class=\"cent\">".$adjusted_total."</td>";
						print "\t\t\t\t\t</tr>\n";
						$novotes = 1;
						
						if ($adjusted_total > $doc_count && $thisFilm->event_name == "Documentary Film") { $doc_count = $adjusted_total; $doc_win = switch_title($thisFilm->title_en); }
						if ($adjusted_total > $nar_count && $thisFilm->event_name == "Narrative Film") { $nar_count = $adjusted_total; $nar_win = switch_title($thisFilm->title_en); }
						if ($adjusted_total > $sho_count && $thisFilm->event_name == "Short Film") { $sho_count = $adjusted_total; $sho_win = switch_title($thisFilm->title_en); }
					}
					$prev_name = $thisFilm->title_en;
				}
			}
		}
		if ($novotes == 0) {
				print "\t\t\t\t\t<tr valign=\"top\">\n";
				print "\t\t\t\t\t\t<td colspan=\"11\" align=\"center\">No films from ".$festival[0]->year." ".$festival[0]->name." have votes yet.</td>";
				print "\t\t\t\t\t</tr>\n";
		}
?>        
				</tbody>
		   </table>

<?php 
		if ($novotes != 0) {
?>           
		   <p><b>Winners:</b> (<?php print $temp_votes; ?> total ballots collected)</p>
		   <p>
			 <b>Documentary Film:</b> <?php print $doc_win." (".$doc_count.")"; ?><br />
			 <b>Narrative Film:</b> <?php print $nar_win." (".$nar_count.")"; ?><br />
			 <b>Short Film:</b> <?php print $sho_win." (".$sho_count.")"; ?>
		   </p>
<?php } ?>
		   
		</fieldset>
	</div>

	<div id="tabs-5">
		<fieldset class="ReportsTab ui-corner-all">

			<h3>Film/Event Views Grouped by Section</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ViewsBySection">
				<thead>
					<tr valign="top">
						<th width="125">Section Name 
							<!-- <form action="/admin/film_reports/export_section_views/" method="post" id="views-section-export" style="display:inline;"> <button id="views-section-btn">Export</button><br></form> -->
						</th>
						<th width="250">Films in Section</th>
						<th width=“60">Web Views</th>
						<th width=“60">Web Views (Average)</th>
						<th width=“60">Mobile Views</th>
						<th width=“60">Mobile Views (Average)</th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		$all_sections = array();
		foreach ($film_ratings as $thisFilmRating) {
			if ($thisFilmRating->category_id != 0) {
				$all_sections[] = $section_array[$thisFilmRating->category_id];	
			}
		}
		$all_sections = array_unique($all_sections);
		sort($all_sections);

		foreach ($all_sections as $thisSection) {
			$section_films = ""; $section_web_views = 0; $section_mobile_views = 0; $film_count = 0;
			foreach ($film_ratings as $thisFilmRating) {
				if ($thisFilmRating->category_id != 0) {
					if ($section_array[$thisFilmRating->category_id] == $thisSection) {
						$section_films .= switch_title($thisFilmRating->title_en).", ";
						$section_web_views = $section_web_views + $thisFilmRating->web;
						$section_mobile_views = $section_mobile_views + $thisFilmRating->mobile;
						$film_count++;
					}
				}
			} $section_films = trim($section_films, ", ");

			if ($thisSection == "") { $thisSection = "**No Section Assigned**"; }

			print "\t\t\t\t\t<tr valign=\"top\">\n";
			print "\t\t\t\t\t\t<td>".$thisSection."</td>";
			print "\t\t\t\t\t\t<td>".$section_films."</td>";
			print "\t\t\t\t\t\t<td>".number_format($section_web_views)."</td>";
			print "\t\t\t\t\t\t<td>";
			if ($film_count > 0) { print number_format( (int) $section_web_views / (int) $film_count ); }
			print "</td>";
			print "\t\t\t\t\t\t<td>".number_format($section_mobile_views)."</td>";
			print "\t\t\t\t\t\t<td>";
			if ($film_count > 0) { print number_format( (int) $section_mobile_views / (int) $film_count ); }
			print "</td>";
			print "\t\t\t\t\t</tr>\n";
		}
?>
				</tbody>
		   </table><br />

			<h3>Individual Film/Event Views</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ViewsByFilm">
				<thead>
					<tr valign="top">
						<th width="225">Film Name 
							<form action="/admin/film_reports/export_ratings_views/" method="post" id="views-ratings-export" style="display:inline;"> <button id="views-ratings-btn">Export</button><br></form>
						</th>
						<th width="125">Section</th>
						<th width="125">Film Type</th>
						<th width="125">Total Votes /<br />Total Value</th>
						<th width="70">Film Rating</th>
						<th width="70">Web Views</th>
						<th width="70">Mobile Views</th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		$novotes = $votes_total = $web_total = $mobile_total = 0; $prev_name = "";
		foreach ($film_ratings as $thisFilmRating) {
				$ip_text = "";

				if ($thisFilmRating->used_ips != "") {
					$ip_array = unserialize($thisFilmRating->used_ips);
					foreach ($ip_array as $thisIP) { $ip_text .= $thisIP."<br>"; }
				} else {
					$ip_text .= "No Votes Yet";
				}
		
				print "\t\t\t\t\t<tr valign=\"top\">\n";
				print "\t\t\t\t\t\t<td><a href=\"/admin/film_edit/update/".$thisFilmRating->slug."\">".switch_title($thisFilmRating->title_en)."</a></td>";
				if ($thisFilmRating->category_id != 0) {
					print "\t\t\t\t\t\t<td>".$section_array[$thisFilmRating->category_id]."</td>";
				} else {
					print "\t\t\t\t\t\t<td>&nbsp;</td>";
				}
				print "\t\t\t\t\t\t<td>".$thisFilmRating->event_name."</td>";
				print "\t\t\t\t\t\t<td>".$thisFilmRating->total_votes." / ".$thisFilmRating->total_value."</td>";
				print "\t\t\t\t\t\t<td>".$thisFilmRating->rating."</td>";
				print "\t\t\t\t\t\t<td>".number_format($thisFilmRating->web)."</td>";
				print "\t\t\t\t\t\t<td>".number_format($thisFilmRating->mobile)."</td>";
				print "\t\t\t\t\t</tr>\n";
				
				$votes_total = $votes_total + intval($thisFilmRating->total_votes);
				$web_total = $web_total + intval($thisFilmRating->web);
				$mobile_total = $mobile_total + intval($thisFilmRating->mobile);

		}
		if (count($film_ratings) == 0) {
				print "\t\t\t\t\t<tr valign=\"top\">\n";
				print "\t\t\t\t\t\t<td colspan=\"8\" align=\"center\">No films from ".$festival[0]->year." ".$festival[0]->name." have views or ratings yet.</td>";
				print "\t\t\t\t\t</tr>\n";
		} else {
?>
					<tr style="border-top:3px solid #CCCCCC;">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><b>Totals:</b></td>
						<td><?php print number_format($votes_total); ?></td>
						<td><?php print ""; ?></td>
						<td><?php print number_format($web_total); ?></td>
						<td><?php print number_format($mobile_total); ?></td>
					</tr>
<?php } ?>
				</tbody>
		   </table>

		</fieldset>
	</div>

	<div id="tabs-6">
		<fieldset class="ReportsTab ui-corner-all">
			<h3>Profit/Loss by Section</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ProfitLossSection">
				<thead>
					<tr valign="top">
						<th width="259">Section</th>
						<th width="141">Number of Films/Events</th>
						<th width="141">Film Revenue</th>
						<th width="141">Film Expenses</th>
						<th width="141">Total Profit/Loss for Section</th>
						<th width="141">Average Profit/Loss per Film</th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		$total_revenue = 0; $total_expenses = 0;

		foreach ($all_sections as $thisSection) {
			$section_revenue = 0; $section_expenses = 0; $section_count = 0;

			foreach ($film_profit_loss as $thisFilm) {
				$ShippingFee = 0;

				foreach ($film_profit_loss_ship as $thisShip) {
					if ($thisShip->ShippingFee != 0) {
						if (strstr($thisFilm->program_movie_ids, $thisShip->movie_id) != FALSE) { $ShippingFee += $thisShip->ShippingFee; }
					}
				}

				$film_array = explode(",", $thisFilm->program_movie_ids);

				if ($thisFilm->category_id != 0) {
					if ($thisSection == $section_array[$thisFilm->category_id]) {
						$section_revenue = $section_revenue + $thisFilm->FilmRevenue;
						$section_expenses = $section_expenses + $thisFilm->RentalFee + $thisFilm->ScreeningFee + $thisFilm->TransferFee + $thisFilm->GuestFee + $ShippingFee;
						$section_count = $section_count + count($film_array);
					}
				}
			}
			if ($thisSection == "") { $thisSection = "**No Section Assigned**"; }

			print "\t\t\t\t\t<tr valign=\"top\">\n";
			print "\t\t\t\t\t\t<td>".$thisSection."</td>";
			print "\t\t\t\t\t\t<td>".$section_count."</td>";
			print "\t\t\t\t\t\t<td class=\"lborder\">$ ".number_format($section_revenue, 0)."</td>";
			print "\t\t\t\t\t\t<td>$ ".number_format($section_expenses, 0)."</td>";

			$section_total = $section_revenue - $section_expenses;
			if ($section_total >= 0) {
				print "\t\t\t\t\t\t<td class=\"lborder\">";
			} else {
				print "\t\t\t\t\t\t<td class=\"lborder\" style=\"color:red;\">";
			}
			print "<b>$ ".number_format($section_total, 0)."</b></td>";

			if ($section_count != 0) {
				$average_total = $section_total / $section_count;	
			} else {
				$average_total = 0;
			}
			if ($average_total >= 0) {
				print "\t\t\t\t\t\t<td class=\"lborder\">";
			} else {
				print "\t\t\t\t\t\t<td class=\"lborder\" style=\"color:red;\">";
			}
			print "<b>$ ". number_format($average_total, 0)."</b></td>";
			print "\t\t\t\t\t</tr>\n";

			$total_revenue += $section_revenue;
			$total_expenses	+= $section_expenses;
		}
?>
					<tr style="border-top:3px solid #CCCCCC;">
						<td>&nbsp;</td>
						<td><b>Totals:</b></td>
						<td>$ <?php print number_format($total_revenue, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_expenses, 0); ?></td>
<?php
	$final_total = $total_revenue - $total_expenses;
	if ($final_total > 0) {
		print "\t\t\t\t\t\t<td>";
	} else {
		print "\t\t\t\t\t\t<td style=\"color:red;\">";
	}
	print "<b>$ ".number_format($final_total, 0)."</b></td>";

?>
					</tr>
				</tbody>
			</table><br />

			<h3>Profit/Loss by Individual Films/Events</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="ProfitLossFilms">
				<thead>
					<tr valign="top">
						<th width="225">Film Name
							<form action="/admin/film_reports/export_profit_loss/" method="post" id="profit-loss-export" style="display:inline;"> <button id="profit-loss-btn">Export</button><br></form>
						</th>
						<th width="150">Section</th>
						<th width="150">Genre(s)</th>
						<th width="85">Film Revenue</th>
						<th width="85">Rental Fees</th>
						<th width="75">Screening Fees</th>
						<th width="75">Dubbing Fees</th>
						<th width="75">Guest Fees</th>
						<th width="75">Shipping Fees</th>
						<th width="100">Profit/Loss</th>
					</tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		$total_revenue = 0; $total_rental = 0; $total_screening = 0; $total_transfer = 0; $total_guest = 0; $total_shipping = 0;

		foreach ($film_profit_loss as $thisFilm) {
			$newtitle = "";
			$film_array = explode(",", $thisFilm->program_movie_ids);
			if (count($film_array) > 1) {
				if ($thisFilm->program_name == "") {
					foreach ($film_array as $thisFilmId) {
						foreach ($filmids as $filmIdList) {
							if ($filmIdList->movie_id == $thisFilmId) {
								$newtitle .= switch_title($filmIdList->label)." + ";
							}
						}
					} $newtitle = trim($newtitle," + ");
					$thisFilm->title_en = $newtitle;
				}
			}

			if ($thisFilm->program_name != "") {
				$thisFilm->title_en = $thisFilm->program_name;
			}
		}
		usort($film_profit_loss,"filmtitlecmp");

		foreach ($film_profit_loss as $thisFilm) {
				$ShippingFee = 0;

				$genre_list = "";
				foreach ($genre_films as $thisMovieGenre) {
					if ($thisMovieGenre->movie_id == $thisFilm->movie_id) {
						$genre_list .= $thisMovieGenre->name.", ";
					}
				} $genre_list = trim($genre_list,", ");

				foreach ($film_profit_loss_ship as $thisShip) {
					if ($thisShip->ShippingFee != 0) {
						if (strstr($thisFilm->program_movie_ids, $thisShip->movie_id) != FALSE) { $ShippingFee += $thisShip->ShippingFee; }
					}
				}
		
				print "\t\t\t\t\t<tr valign=\"top\">\n";
				print "\t\t\t\t\t\t<td><a href=\"/admin/film_edit/update/".$thisFilm->slug."\">".switch_title($thisFilm->title_en)."</a></td>";

				if ($thisFilm->category_id != 0) {
					print "\t\t\t\t\t\t<td>".$section_array[$thisFilm->category_id]."</td>";
				} else {
					print "\t\t\t\t\t\t<td></td>";
				}
				print "\t\t\t\t\t\t<td>".$genre_list."</td>";
				print "\t\t\t\t\t\t<td class=\"lborder\">$ ".number_format($thisFilm->FilmRevenue, 0)."</td>";
				print "\t\t\t\t\t\t<td>$ ".number_format($thisFilm->RentalFee, 0)."</td>";
				print "\t\t\t\t\t\t<td>$ ".number_format($thisFilm->ScreeningFee, 0)."</td>";
				print "\t\t\t\t\t\t<td>$ ".number_format($thisFilm->TransferFee, 0)."</td>";
				print "\t\t\t\t\t\t<td>$ ".number_format($thisFilm->GuestFee, 0)."</td>";
				print "\t\t\t\t\t\t<td>$ ".number_format($ShippingFee, 0)."</td>";
				$film_total = $thisFilm->FilmRevenue - ($thisFilm->RentalFee + $thisFilm->ScreeningFee + $thisFilm->TransferFee + $thisFilm->GuestFee + $ShippingFee);
				if ($film_total >= 0) {
					print "\t\t\t\t\t\t<td class=\"lborder\">";
				} else {
					print "\t\t\t\t\t\t<td class=\"lborder\" style=\"color:red;\">";
				}
				print "<b>$ ".number_format($film_total, 0)."</b></td>";

				print "\t\t\t\t\t</tr>\n";
				
				$total_revenue += $thisFilm->FilmRevenue;
				$total_rental += $thisFilm->RentalFee;
				$total_screening += $thisFilm->ScreeningFee;
				$total_transfer += $thisFilm->TransferFee;
				$total_guest += $thisFilm->GuestFee;
				$total_shipping += $ShippingFee;
		}
		if (count($film_profit_loss) == 0) {
				print "\t\t\t\t\t<tr valign=\"top\">\n";
				print "\t\t\t\t\t\t<td colspan=\"8\" align=\"center\">No films have been added to ".$festival[0]->year." ".$festival[0]->name." yet.</td>";
				print "\t\t\t\t\t</tr>\n";
		} else { // don't show totals if there's no films to display yet
?>        
					<tr style="border-top:3px solid #CCCCCC;">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><b>Totals:</b></td>
						<td>$ <?php print number_format($total_revenue, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_rental, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_screening, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_transfer, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_guest, 0); ?></td>
						<td style="color:red;">-$ <?php print number_format($total_shipping, 0); ?></td>
<?php
	$final_total = $total_revenue - ($total_rental + $total_screening + $total_transfer + $total_guest + $total_shipping);
	if ($final_total > 0) {
		print "\t\t\t\t\t\t<td>";
	} else {
		print "\t\t\t\t\t\t<td style=\"color:red;\">";
	}
	print "<b>$ ".number_format($final_total, 0)."</b></td>";

?>
					</tr>
<?php } ?>
				</tbody>
		   </table>

		</fieldset>
	</div>

	<div id="tabs-7">
		<fieldset class="ReportsTab ui-corner-all">
		<h3>Competitions - Juror Votes</h3>

		<table border="0" cellpadding="0" cellspacing="0">
			<!--
			<thead>
				<tr valign="top">
					<th>Competition</th>
					<th>Juror Name</th>
					<th>Film Ranking</th>
					<th>Film Votes</th>
				</tr>
			</thead>
			-->
			<tbody style="border-top:none;">
		<?php
			$vote_array = $total_vote_array = array();
			foreach ($competition_votes as $thisVote) {
				if (!isset($vote_array[$thisVote->competition_id])) { $vote_array[$thisVote->competition_id] = 1; }
				else { $vote_array[$thisVote->competition_id] = $vote_array[$thisVote->competition_id] + 1; }
				$ranking_array = explode(",", $thisVote->ranking);
				foreach ($ranking_array as $thisFilmId) {
					$total_vote_array[$thisVote->competition_id][$thisFilmId] = 0;
				}
			}

			foreach ($competitions as $thisCompetition) {
				if (isset($vote_array[$thisCompetition->id])) {
					if ($vote_array[$thisCompetition->id] > 0) {
						$cols = $vote_array[$thisCompetition->id]+1;
						print "<tr valign=\"top\"><td colspan=\"".$cols."\" class=\"competition\"><strong>".$thisCompetition->festival_year." ".$thisCompetition->festival_name." ".$thisCompetition->name."</strong></td></tr>";
						print "<tr valign=\"top\">";
						foreach ($competition_votes as $thisVote) {
							$ranking_array = explode(",", $thisVote->ranking);
							$ranking_total = count($ranking_array);
							$filmnames = "";

							print "<td>".$thisVote->name."<br/><br/>";
							foreach ($ranking_array as $thisFilmId) {
								foreach ($filmids as $thisFilmInfo) {
									if ($thisFilmInfo->movie_id == $thisFilmId) {
										$filmnames .= $ranking_total.") ".switch_title($thisFilmInfo->label)."<br/>";
										$total_vote_array[$thisVote->competition_id][$thisFilmId] = $total_vote_array[$thisVote->competition_id][$thisFilmId] + $ranking_total;
										$ranking_total--;
									}
								}
							}
							print $filmnames;
							if ($thisVote->notes != "") { print "<p>".$thisVote->notes."</p>"; }
							print "</td>";
						}
						print "<td><strong>Vote Totals</strong><br /><br />";
						foreach ($total_vote_array as $thisCompId => $voteArray) {
							if ($thisCompId == $thisCompetition->id) {
								arsort($voteArray);
								foreach ($voteArray as $filmId => $vote) {
									foreach ($filmids as $thisFilmInfo) {
										if ($thisFilmInfo->movie_id == $filmId) {
											print $vote.") ".switch_title($thisFilmInfo->label)."<br />";
										}
									}
								}
							}
						}
						print "</td>";
						print "</tr>";
					}
				}
			}

			//print_r($total_vote_array);
			//print_r($competitions);
			//print_r($competition_votes);
		?>
		</tbody></table>
		</fieldset>
	</div>

	<div id="tabs-8">
		<fieldset class="ReportsTab ui-corner-all">
			<style>
				.half_column { float: left; width: 48%; margin-right: 2%; }
				.typeValue { font-weight: bold; }
				.typeCount { font-weight: bold; }
				.typeTotal { border-top: 1px solid black; }
				.filmList { font-size: small; color: #AAAAAA; display: none; }
				a.filmToggle:link, a.filmToggle:visited { font-size: small; color: #F7941D; }
			</style>

			<div class="half_column">
				<h3>Film Section Summary</h3>
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody style="border-top:none;">
				<?php
					$total = 0;
					foreach ($sectionsummary as $thisType) {
						print "<tr><td class=\"typeValue\">".$thisType["name"]." <a href=\"#\" class=\"filmToggle\">Show</a></td><td class=\"typeCount\">".$thisType["count"]."</td></tr>\n";
						print "<tr><td colspan=\"2\"><span class=\"filmList\">".$thisType["films"]."</span></td></tr>\n";
						$total += $thisType["count"];
					}
					print "<tr class=\"typeTotal\"><td class=\"typeValue\">Total</td><td class=\"typeCount\">".$total."</td></tr>\n";
				?>
					</tbody>
				</table>
			</div>

			<div class="half_column">
				<h3>Premiere Status Summary</h3>
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody style="border-top:none;">
				<?php
					$total = 0;
					foreach ($premieres as $thisType) {
						print "<tr><td class=\"typeValue\">".$thisType["name"]." <a href=\"#\" class=\"filmToggle\">Show</a></td><td class=\"typeCount\">".$thisType["count"]."</td></tr>\n";
						print "<tr><td colspan=\"2\"><span class=\"filmList\">".$thisType["films"]."</span></td></tr>\n";
						$total += $thisType["count"];
					}
					print "<tr class=\"typeTotal\"><td class=\"typeValue\">Total</td><td class=\"typeCount\">".$total."</td></tr>\n";
				?>
					</tbody>
				</table>
			</div>

			<div class="half_column">
				<h3>Event Type Summary</h3>
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody style="border-top:none;">
				<?php
					$total = 0;
					foreach ($eventtypes as $thisType) {
						print "<tr><td class=\"typeValue\">".$thisType["name"]." <a href=\"#\" class=\"filmToggle\">Show</a></td><td class=\"typeCount\">".$thisType["count"]."</td></tr>\n";
						print "<tr><td colspan=\"2\"><span class=\"filmList\">".$thisType["films"]."</span></td></tr>\n";
						$total += $thisType["count"];
					}
					print "<tr class=\"typeTotal\"><td class=\"typeValue\">Total</td><td class=\"typeCount\">".$total."</td></tr>\n";
				?>
					</tbody>
				</table>
			</div>

		</fieldset>
	</div>


</div>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();
				
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});		

<?php
foreach ($schedule as $thisDate) {
	if ($thisDate == "All Dates") {
		print "\t\t$(\"#all-dates\").on('click', function() {\n"; 
		print "\t\t\t$(\"#reportsTheaterOps\").load(\"/admin/film_reports/preview_theater_ops2/all/all/preview/\");\n";
		print "\t\t\t$(this).removeClass(\"ui-state-focus\");\n";
		print "\t\t});\n";
	} else {
		print "\t\t$(\"#".$thisDate."\").on('click', function() {\n"; 
		print "\t\t\t$(\"#reportsTheaterOps\").load(\"/admin/film_reports/preview_theater_ops2/\"+\"".$thisDate."/all/preview/\");\n";
		print "\t\t\t$(this).removeClass(\"ui-state-focus\");\n";
		print "\t\t});\n";
	}
}
?>

		$("#inboundShipments").on('click', function() {
			$("#reportsPT").load("/admin/film_reports/preview_shipments/inbound/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#outboundShipments").on('click', function() {
			$("#reportsPT").load("/admin/film_reports/preview_shipments/outbound/preview/"); $(this).removeClass("ui-state-focus");
		});


		$("#allFilms").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_all_films/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#tableOfContents").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_toc/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#filmSchedule").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_film_sched/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#filmTrailers").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_film_trailers/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#countryIndex").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_country_idx/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#directorIndex").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_director_idx/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#filmIndex").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_film_idx/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#printSourceIndex").on('click', function() {
			$("#reportsGuideExp").load("/admin/film_reports/preview_print_idx/preview/"); $(this).removeClass("ui-state-focus");
		});

		jQuery("#BallotCounts").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
		jQuery("#ViewsBySection").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
		jQuery("#ViewsByFilm").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
		jQuery("#ProfitLossSection").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
		jQuery("#ProfitLossFilms").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});


		$('#tabs-8').on('click', 'a.filmToggle', function(e) {
			e.preventDefault();
			if ($(this).parents("tr").next().find("span").css('display') == "none") {
				$(this).text("Hide");
				$(this).parents("tr").next().find("span").css('display', 'inline');
			} else {
				$(this).text("Show");
				$(this).parents("tr").next().find("span").css('display', 'none');
			}
		});

	});
</script>
