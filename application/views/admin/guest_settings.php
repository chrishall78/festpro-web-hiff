<?php
	$festivals_array = convert_festival_to_array($festivals);
	$airlines_array = convert_to_array($airlines);
	$airports_array = convert_to_array($airports);
	$guesttypes_array = convert_to_array($guesttypes);
	$yesno_array = array('No','Yes');
?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Guest Details</a></li>
		<li><a href="#tabs-2">Flights</a></li>
		<li><a href="#tabs-3">Hotels</a></li>
	</ul>
	<div id="tabs-1">
	<!-- Guest Details Tab -->
	<fieldset class="ui-corner-all">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
				<tr valign="top">
					<td width="50%">

		<form id="addGuestType" name="addGuestType">
		<table class="GuestDetailsSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="4"><h3>Add A Guest Type</h3></td>
			</tr>
			<tr valign="top">
				<td width="40%"><label for="guesttype-new">Guest Type Name</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'guesttype-new', 'id'=>'guesttype-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="20%"><label for="getsbadge-new">Badge?</label><span class="req"> *</span><br />
				<?php print form_dropdown('getsbadge-new', $yesno_array, 0, "id='getsbadge-new' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="getsbag-new">Gift Bag?</label><span class="req"> *</span><br />
				<?php print form_dropdown('getsbag-new', $yesno_array, 0, "id='getsbag-new' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><br />
				<?php print form_submit(array('name'=>'guesttype-add', 'id'=>'guesttype-add', 'value'=>'Add')); ?>
				</td>
			</tr>
			</tbody>
		</table>
		</form>

		</td><td width="50%">
	
		<form id="updateGuestType" name="updateGuestType">
		<table class="GuestDetailsSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="4"><h3>Edit/Delete A Guest Type</h3></td>
			</tr>
			<tr valign="top">
				<td colspan="4"><label for="guesttype">Current Guest Types</label><span class="req"> *</span><br />
				<?php print form_dropdown('guesttype', $guesttypes_array, 0, "id='guesttype' class='select ui-widget-content ui-corner-all'"); ?>
				</td>
			</tr>
			<tr valign="top">
				<td width="40%"><label for="guesttype-current">Guest Type Name</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'guesttype-current', 'id'=>'guesttype-current', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="20%"><label for="getsbadge-current">Badge</label><span class="req"> *</span><br />
				<?php print form_dropdown('getsbadge-current', $yesno_array, 0, "id='getsbadge-current' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="getsbag-current">Gift Bag</label><span class="req"> *</span><br />
				<?php print form_dropdown('getsbag-current', $yesno_array, 0, "id='getsbag-current' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%" rowspan="2"><br />
				<?php print form_submit(array('name'=>'guesttype-update', 'id'=>'guesttype-update', 'value'=>'Update')); ?>
				</td>
			</tr>
			</tbody>
		</table>
		</form>

					</td>
				</tr>
			</tbody>
		</table>

		<!-- Deadlines &amp; Fees -->
		<form id="addDeadlinesFees" name="addDeadlinesFees">
		<table class="GuestDetailsSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="6"><h3>Add Deadlines/Fees for Accreditation</h3></td>
			</tr>
			<tr valign="top">
				<td width="20%"><label for="accred-festival-new">Festival</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-festival-new', $festivals_array, 0, "id='accred-festival-new' class='select ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="accred-guesttype-new">Guest Type</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-guesttype-new', $guesttypes_array, 0, "id='accred-guesttype-new' class='select ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="15%"><label for="early-deadline-new">Early Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'early-deadline-new', 'id'=>'early-deadline-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
				<label for="early-fee-new">Early Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'early-fee-new', 'id'=>'early-fee-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><label for="late-deadline-new">Late Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'late-deadline-new', 'id'=>'late-deadline-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
				<label for="late-fee-new">Late Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'late-fee-new', 'id'=>'late-fee-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><label for="final-deadline-new">Final Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'final-deadline-new', 'id'=>'final-deadline-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
				<label for="final-fee-new">Final Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'final-fee-new', 'id'=>'final-fee-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><br />
				<?php print form_submit(array('name'=>'deadlinefees-add', 'id'=>'deadlinefees-add', 'value'=>'Add')); ?>
				</td>
			</tr>
			<tr>
				<td colspan="6"><br /><h3>Edit Deadlines/Fees for Accreditation</h3></td>
			</tr>
			</tbody>
		</table>
		</form>

		<table id="EditDeadlinesFees" class="GuestDetailsSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="10%">&nbsp;</th>
					<th width="18%">Festival</th>
					<th width="18%">Guest Type</th>
					<th width="11%">Early Deadline</th>
					<th width="7%">Fee</th>
					<th width="11%">Late Deadline</th>
					<th width="7%">Fee</th>
					<th width="11%">Final Deadline</th>
					<th width="7%">Fee</th>
				</tr>
			</thead>
			<tbody style="border-top:none;">
<?php
	$x = 1; $df_array = array(); $prev = "";
	if (count($deadlinesfees) == 0) {
		print "\t<tr valign=\"top\" class=\"oddrow\">\n";
		print "\t\t<td colspan=\"9\" align=\"center\">There are no deadlines & fees defined yet.</td>\n";
		print "\t</tr>\n";
	} else {
		foreach ($deadlinesfees as $thisDeadline) {
			$festival_name = "";
			foreach ($festivals as $thisFestival) {
				if ($thisFestival->id == $thisDeadline->festival_id) {
					$festival_name = $thisFestival->year." ".$thisFestival->name;
				}
			}

			if (($x % 2) == 1) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
				}
			}
			if (($x % 2) == 0) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
				}
			}
		
			print "\t\t<td><button id=\"df-".$thisDeadline->id."\" data-id=\"".$thisDeadline->id."\" class=\"edit\">Edit</button></td>\n";
			print "\t\t<td>".$festival_name."</td>\n";
			print "\t\t<td>".$guesttypes_array[$thisDeadline->guesttype_id]."</td>\n";
			print "\t\t<td>".date("m-d-Y",strtotime($thisDeadline->early_deadline))."</td>\n";
			print "\t\t<td>$".$thisDeadline->early_fee."</td>\n";
			print "\t\t<td>".date("m-d-Y",strtotime($thisDeadline->late_deadline))."</td>\n";
			print "\t\t<td>$".$thisDeadline->late_fee."</td>\n";
			print "\t\t<td>".date("m-d-Y",strtotime($thisDeadline->final_deadline))."</td>\n";
			print "\t\t<td>$".$thisDeadline->final_fee."</td>\n";
			print "\t</tr>\n";
		
			$x++; $df_array[] = $thisDeadline->id; $prev = $festival_name;
		}
	}
?>
				<tr id="deadlines_replaceme">
					<td colspan="9"><input type="hidden" name="deadlines_new_id" id="deadlines_new_id" value="0" /></td>
				</tr>
			</tbody>
		</table>

	</fieldset>    
	
	</div>

	<div id="tabs-2">
	<!-- Flights Tab -->
	<fieldset class="ui-corner-all">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
				<tr valign="top">
					<td width="50%">

		<form id="addAirport" name="addAirport">
		<table class="FlightSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="4"><h3>Add an Airport</h3></td>
			</tr>
			<tr valign="top">
				<td width="40%"><label for="airportname-new">Airport Name</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'airportname-new', 'id'=>'airportname-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="40%"><label for="airportcode-new">Code</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'airportcode-new', 'id'=>'airportcode-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="20%"><label for="domestic-new">Domestic</label><span class="req"> *</span><br />
				<?php print form_dropdown('domestic-new', $yesno_array, 0, "id='domestic-new' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="priority-new">Priority</label><span class="req"> *</span><br />
				<?php print form_dropdown('priority-new', $yesno_array, 0, "id='priority-new' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><br />
				<?php print form_submit(array('name'=>'airports-add', 'id'=>'airports-add', 'value'=>'Add')); ?>
				</td>
			</tr>
			</tbody>
		</table>
		</form>

		</td><td width="50%">
	
		<form id="updateAirport" name="updateAirport">
		<table class="FlightSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="4"><h3>Edit/Delete an Airport</h3></td>
			</tr>
			<tr valign="top">
				<td colspan="4"><label for="airports">Current Airports</label><span class="req"> *</span><br />
				<?php print form_dropdown('airports', $airports_array, 0, "id='airports' class='select ui-widget-content ui-corner-all'"); ?>
				</td>
			</tr>
			<tr valign="top">
				<td width="40%"><label for="airportname-current">Airport Name</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'airportname-current', 'id'=>'airportname-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="40%"><label for="airportcode-current">Code</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'airportcode-current', 'id'=>'airportcode-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="20%"><label for="domestic-current">Domestic</label><span class="req"> *</span><br />
				<?php print form_dropdown('domestic-current', $yesno_array, 0, "id='domestic-current' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="priority-current">Priority</label><span class="req"> *</span><br />
				<?php print form_dropdown('priority-current', $yesno_array, 0, "id='priority-current' class='select-small ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%" rowspan="2"><br />
				<?php print form_submit(array('name'=>'airports-update', 'id'=>'airports-update', 'value'=>'Update')); ?>
				</td>
			</tr>
			</tbody>
		</table>
		</form>

					</td>
				</tr>
			</tbody>
		</table>
		
		<div style="width:305px; padding:3px;" class="FlightSettingsTab">
		<?php print_type_AUD($airlines_array, "Airlines", "airlines"); ?>
		</div><br />

		<!--
		<h3>Flight Comp Limits</h3>
		<p>Festival ID, Airline, Cash/Mileage/Flight Limit</p>

		<h3>Flight Comp Options</h3>
		<p>Festival ID, Option Name, Airline, Destination, Cash Value, Mileage, Deal ID</p>
		-->
	</fieldset>    
	</div>

	<div id="tabs-3">
	<!-- Hotels Tab -->
	<fieldset class="ui-corner-all HotelSettingsTab">

		<!-- Hotel Comp Options -->
		<form id="addHotelComps" name="addHotelComps">
		<table class="HotelSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr>
				<td colspan="6"><h3>Add Hotel Comp Options/Limits</h3></td>
			</tr>
			<tr valign="top">
				<td width="20%"><label for="hotelc-festival-new">Festival</label><span class="req"> *</span><br />
				<?php print form_dropdown('hotelc-festival-new', $festivals_array, 0, "id='hotelc-festival-new' class='select ui-widget-content ui-corner-all'"); ?>
				</td>
				<td width="20%"><label for="hotelc-name-new">Option Name</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'hotelc-name-new', 'id'=>'hotelc-name-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><label for="hotelc-hotel-new">Hotel</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'hotelc-hotel-new', 'id'=>'hotelc-hotel-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><label for="hotelc-roomtype-new">Room Type</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'hotelc-roomtype-new', 'id'=>'hotelc-roomtype-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><label for="hotelc-limit-new">Room Limit</label><span class="req"> *</span><br />
				<?php print form_input(array('name'=>'hotelc-limit-new', 'id'=>'hotelc-limit-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				</td>
				<td width="15%"><br />
				<?php print form_submit(array('name'=>'hotelcomps-add', 'id'=>'hotelcomps-add', 'value'=>'Add')); ?>
				</td>
			</tr>
			<tr>
				<td colspan="6"><br /><h3>Edit Hotel Comp Options/Limits</h3></td>
			</tr>
			</tbody>
		</table>
		</form>

		<table class="HotelSettingsTab" width="100%" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="10%">&nbsp;</th>
					<th width="20%">Festival</th>
					<th width="20%">Option Name</th>
					<th width="20%">Hotel</th>
					<th width="15%">Room Type</th>
					<th width="15%">Room Limit</th>
				</tr>
			</thead>
			<tbody style="border-top:none;">
<?php
	$x = 1; $hc_array = array(); $prev = "";
	if (count($hotelcomps) == 0) {
		print "\t<tr valign=\"top\" class=\"oddrow\">\n";
		print "\t\t<td colspan=\"6\" align=\"center\">There are no hotel comps defined yet.</td>\n";
		print "\t</tr>\n";
	} else {
		foreach ($hotelcomps as $thisHotelComp) {
			$festival_name = "";
			foreach ($festivals as $thisFestival) {
				if ($thisFestival->id == $thisHotelComp->festival_id) {
					$festival_name = $thisFestival->year." ".$thisFestival->name;
				}
			}
			
			if (($x % 2) == 1) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
				}
			}
			if (($x % 2) == 0) {
				if ($prev != $festival_name && $prev != "") {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\" style=\"border-top: 1px solid #CCCCCC;\">\n";
				} else {
					print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
				}
			}
		
			print "\t\t<td><button id=\"hc-".$thisHotelComp->id."\" data-id=\"".$thisHotelComp->id."\" class=\"edit\">Edit</button></td>\n";
			print "\t\t<td>".$festival_name."</td>\n";
			print "\t\t<td>".$thisHotelComp->name."</td>\n";
			print "\t\t<td>".$thisHotelComp->hotel."</td>\n";
			print "\t\t<td>".$thisHotelComp->roomtype."</td>\n";
			print "\t\t<td>".$thisHotelComp->limit."</td>\n";
			print "\t</tr>\n";
		
			$x++; $hc_array[] = $thisHotelComp->id; $prev = $festival_name;
		}
	}
?>
				<tr id="hotelcomps_replaceme">
					<td colspan="6"><input type="hidden" name="hotelcomps_new_id" id="hotelcomps_new_id" value="0" /></td>
				</tr>
			</tbody>
		</table>
		</form>

	</fieldset>    
	</div>
</div>

<div id="editDeadlines-dialog" title="Edit Deadlines/Fees">
	<form name="editDeadlinesForm" id="editDeadlinesForm">
	<input type="hidden" value="" name="deadlines_id_update" id="deadlines_id_update" />
	<table width="100%">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="50%" colspan="3">
				<label for="accred-festival-current">Festival</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-festival-current', $festivals_array, 0, "id='accred-festival-current' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="50%" colspan="3">
				<label for="accred-guesttype-current">Guest Type</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-guesttype-current', $guesttypes_array, 0, "id='accred-guesttype-current' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="early-deadline-current">Early Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'early-deadline-current', 'id'=>'early-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
			<td colspan="2">
				<label for="late-deadline-current">Late Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'late-deadline-current', 'id'=>'late-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
			<td colspan="2">
				<label for="final-deadline-current">Final Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'final-deadline-current', 'id'=>'final-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="early-fee-current">Early Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'early-fee-current', 'id'=>'early-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="late-fee-current">Late Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'late-fee-current', 'id'=>'late-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="final-fee-current">Final Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'final-fee-current', 'id'=>'final-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>    
	</form>
</div>

<div id="editHotelcomps-dialog" title="Edit Hotel Comp Options/Limits">
	<form name="editHotelcompsForm" id="editHotelcompsForm">
	<input type="hidden" value="" name="hotelcomps_id_update" id="hotelcomps_id_update" />
	<table width="100%">
		<tbody style="border-top:none;">
		<tr valign="top">
			<td width="50%" colspan="3">
				<label for="accred-festival-current">Festival</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-festival-current', $festivals_array, 0, "id='accred-festival-current' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
			<td width="50%" colspan="3">
				<label for="accred-guesttype-current">Guest Type</label><span class="req"> *</span><br />
				<?php print form_dropdown('accred-guesttype-current', $guesttypes_array, 0, "id='accred-guesttype-current' class='select ui-widget-content ui-corner-all'"); ?>
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="early-deadline-current">Early Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'early-deadline-current', 'id'=>'early-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
			<td colspan="2">
				<label for="late-deadline-current">Late Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'late-deadline-current', 'id'=>'late-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
			<td colspan="2">
				<label for="final-deadline-current">Final Deadline</label><span class="req"> *</span><br />
				<?php print "&nbsp;&nbsp;".form_input(array('name'=>'final-deadline-current', 'id'=>'final-deadline-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?><br />
			</td>
		</tr>
		<tr valign="top">
			<td colspan="2">
				<label for="early-fee-current">Early Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'early-fee-current', 'id'=>'early-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="late-fee-current">Late Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'late-fee-current', 'id'=>'late-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
			<td colspan="2">
				<label for="final-fee-current">Final Fee</label><span class="req"> *</span><br />
				<?php print "$".form_input(array('name'=>'final-fee-current', 'id'=>'final-fee-current', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
			</td>
		</tr>
		</tbody>
	</table>    
	</form>
</div>

<script type="text/javascript">
$(function() {
	$("#tabs").tabs();
	$("input:submit").button();
	$("button").button();				
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});		

	var guestTypeJSON = <?php print json_encode($guesttypes); ?>;
	$('#guesttype').on('change', function() { load_value_guesttype($("option:selected", this).val(), guestTypeJSON); });
	$('#guesttype-add').on('click', function() { if ($("#addGuestType").validate().form() == true) { add_guest_setting("#addGuestType", "#guesttype"); } return false; });
	$('#guesttype-update').on('click', function() { if ($("#updateGuestType").validate().form() == true) { update_guest_setting("#updateGuestType", "#guesttype"); } return false; });
	$("#addGuesttype").validate({ rules: { "guesttype-new": "required", "getsbadge-new": { required:true, min:1 }, "getsbag-new": { required:true, min:1 } }, messages: { "guesttype-new": "Please enter a name.", "getsbadge-new": "Please select Yes or No.", "getsbag-new": "Please select Yes or No." } });
	$("#updateGuesttype").validate({ rules: { "guesttype": { required:true, min:1 }, "guesttype-current": "required", "getsbadge-current": { required:true, min:1 }, "getsbag-current": { required:true, min:1 } }, messages: { "guesttype": "Please select a guest type to edit.", "guesttype-current": "Please enter a name.", "getsbadge-current": "Please select Yes or No.", "getsbag-current": "Please select Yes or No." } });

	var airportJSON = <?php print json_encode($airports); ?>;
	$('#airports').on('change', function() { load_value_airport($("option:selected", this).val(), airportJSON); });
	$('#airports-add').on('click', function() { if ($("#addAirport").validate().form() == true) { add_guest_setting("#addAirport", "#airports"); } return false; });
	$('#airports-update').on('click', function() { if ($("#updateAirport").validate().form() == true) { update_guest_setting("#updateAirport", "#airports"); } return false; });
	$("#addAirport").validate({ rules: { "airportname-new": "required", "airportcode-new": "required", "domestic-new": "required", "priority-new": "required"}, messages: { "airportname-new": "Please enter a value.", "airportcode-new": "Please enter a value.", "domestic-new": "Please select a value.", "priority-new": "Please select a value." } });
	$("#updateAirport").validate({ rules: { "airports": "required", "airportname-current": "required", "airportcode-current": "required", "domestic-current": "required", "priority-current": "required" }, messages: { "airports": "Please select an airline to edit.", "airportname-current": "Please enter a value.", "airportcode-current": "Please enter a value.", "domestic-current": "Please select a value.", "priority-current": "Please select a value." } });

	$('.select').on('change', load_value_guest);
	$('#airlines-add').on('click', function() { if ($("#add-airlines").validate().form() == true) { add_guest_type("airlines", $("#airlines-new").val(), 0); } return false; });
	$('#airlines-update').on('click', function() { if ($("#update-airlines").validate().form() == true) { update_guest_type("airlines", $("#airlines-current").val(), 0, $("#airlines").val()); } return false; });
	$("#add-airlines").validate({ rules: { "airlines-new": "required" }, messages: { "airlines-new": "Please enter a value." } });
	$("#update-airlines").validate({ rules: { "airlines": "required", "airlines-current": "required" }, messages: { "airlines": "Please select an airline to edit.", "airlines": "Please enter a value." } });

	// Deadlines & Fees
	$('#early-deadline-new, #late-deadline-new, #final-deadline-new, #early-deadline-current, #late-deadline-current, #final-deadline-current').each(function() {
			$(this).datepicker();
	})
	$('#deadlinefees-add').on('click', function() {
		if ($("#addDeadlinesFees").validate().form() == true) {
			add_guest_setting("#addDeadlinesFees", "#deadlines_replaceme");
			$.ajax({
				success: function(msg, text, xhr) {
					deadlinesfeesJSON = msg;
				},
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				url: '/admin/guest_settings/return_deadlines_json/',
				type: 'POST',
				dataType: 'json'
			}); 							
		} return false;
	});
	$("#addDeadlinesFees").validate({
		rules: { "accred-festival-new": { required:true, min:1 }, "accred-guesttype-new": { required:true, min:1 }, "early-deadline-new": "required", "early-fee-new": "required", "late-deadline-new": "required", "late-fee-new": "required", "final-deadline-new": "required", "final-fee-new": "required" },
		messages: { "accred-festival-new": "Please select a festival.", "accred-guesttype-new": "Please select a guest type.", "early-deadline-new": "Please enter a deadline.", "early-fee-new": "Please enter a fee.", "late-deadline-new": "Please enter a deadline.", "late-fee-new": "Please enter a fee.", "final-deadline-new": "Please enter a deadline.", "final-fee-new": "Please enter a fee." }
	});

	var deadlinesfeesJSON = <?php print json_encode($deadlinesfees); ?>;

	$('#EditDeadlinesFees').on('click','button.edit', function(e) {
		e.preventDefault();
		open_edit_deadline_dialog($(this).data('id').toString(), deadlinesfeesJSON);
		$(this).removeClass('ui-state-focus');
	});
	$("#editDeadlinesForm").validate({
		rules: { "accred-festival-current": { required:true, min:1 }, "accred-guesttype-current": { required:true, min:1 }, "early-deadline-current": "required", "early-fee-current": "required", "late-deadline-current": "required", "late-fee-current": "required", "final-deadline-current": "required", "final-fee-current": "required" },
		messages: { "accred-festival-current": "Please select a festival.", "accred-guesttype-current": "Please select a guest type.", "early-deadline-current": "Please enter a deadline.", "early-fee-current": "Please enter a fee.", "late-deadline-current": "Please enter a deadline.", "late-fee-current": "Please enter a fee.", "final-deadline-current": "Please enter a deadline.", "final-fee-current": "Please enter a fee." }
	});
	$("#editDeadlines-dialog").dialog({
		autoOpen: false,
		height: 300,
		width: 500,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Update': function() {
				var deadlines_id = $('#deadlines_id_update').val();
				if ($("#editDeadlinesForm").validate().form() == true) {

					$.ajax({
						success: function(msg, text, xhr) {
							$('#df-'+deadlines_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									deadlinesfeesJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_settings/return_deadlines_json/',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editDeadlinesForm').serialize(),
						url: '/admin/guest_settings/update_deadlines_fees/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
				}
			}
		}
	});

	// Hotel Comp Options/Limits
	$('#hotelcomps-add').on('click', function() {
		if ($("#addHotelComps").validate().form() == true) {
			add_guest_setting("#addHotelComps", "#hotelcomps_replaceme");
			$.ajax({
				success: function(msg, text, xhr) {
					hotelcompsJSON = msg;
				},
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				url: '/admin/guest_settings/return_hotelcomps_json/',
				type: 'POST',
				dataType: 'json'
			}); 							
		} return false;
	});
	$("#addHotelComps").validate({
		rules: { "hotelc-festival-new": { required:true, min:1 }, "hotelc-name-new": "required", "hotelc-hotel-new": "required", "hotelc-roomtype-new": "required", "hotelc-limit-new": { required:true, digits:true } },
		messages: { "hotelc-festival-new": "Please select a festival.", "hotelc-name-new": "Please enter an option name.", "hotelc-hotel-new": "Please enter a hotel name.", "hotelc-roomtype-new": "Please enter a room type.", "hotelc-limit-new": "Please enter a room limit (whole numbers only)." }
	});

	var hotelcompsJSON = <?php print json_encode($hotelcomps); ?>;

	$('#HotelSettingsTab').on('click','button.edit', function(e) {
		e.preventDefault();
		open_edit_hotelcomps_dialog($(this).data('id').toString(), hotelcompsJSON);
		$(this).removeClass('ui-state-focus');
	});
	$("#editHotelcompsForm").validate({
		rules: { "hotelc-festival-current": { required:true, min:1 }, "hotelc-name-current": "required", "hotelc-hotel-current": "required", "hotelc-roomtype-current": "required", "hotelc-limit-current": { required:true, digits:true } },
		messages: { "hotelc-festival-current": "Please select a festival.", "hotelc-name-current": "Please enter an option name.", "hotelc-hotel-current": "Please enter a hotel name.", "hotelc-roomtype-current": "Please enter a room type.", "hotelc-limit-current": "Please enter a room limit (whole numbers only)." }
	});
	$("#editHotelcomps-dialog").dialog({
		autoOpen: false,
		height: 300,
		width: 500,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Update': function() {
				var hotelcomps_id = $('#deadlines_id_update').val();
				if ($("#editHotelcompsForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#hc-'+hotelcomps_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									hotelcompsJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/guest_settings/return_hotelcomps_json/',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editHotelcompsForm').serialize(),
						url: '/admin/guest_settings/update_hotelcomps/',
						type: 'POST',
						dataType: 'html'
					}); 
					$(this).dialog('close');
				}
			}
		}
	});
});
</script>