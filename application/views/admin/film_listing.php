<?php
	print "<div>\n";
	if ($this->session->userdata('limited') == 0) {
		print "<form id=\"unpublishAllForm\"><input type=\"hidden\" value=\"".$festival[0]->id."\" name=\"festival_id\"><button id=\"unpublish-all\">Unpublish All</button></form>\n";
		print "<form id=\"publishAllForm\"><input type=\"hidden\" value=\"".$festival[0]->id."\" name=\"festival_id\"><button id=\"publish-all\">Publish All</button></form>\n";
	}
	print "<button id=\"add-film\">Add Film/Event</button><h2>".$festival[0]->year." ".$festival[0]->name." Film Listing (".date("n/d",strtotime($festival[0]->startdate))." - ".date("n/d/Y",strtotime($festival[0]->enddate)).")</h2></div><br clear=\"all\">";
	
	print "<fieldset class=\"ui-corner-all\" style=\"padding:0px\">";
	print "<table id=\"film_listing_table\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">\n";
	print "\t<thead>\n";	
	print "\t<tr>\n";
	print "\t\t<th width=\"1\" style=\"text-align:center;\"> &nbsp; </th>\n";
	print "\t\t<th width=\"200\">Film/Event Name <span><a href=\"/admin/film_listing/sort/nameasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Film/Event Name\" /></a> <a href=\"/admin/film_listing/sort/namedesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Film/Event Name\" /></a></span></th>\n";
	print "\t\t<th width=\"115\">Section <span><a href=\"/admin/film_listing/sort/sectionasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Section\" /></a> <a href=\"/admin/film_listing/sort/sectiondesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Section\" /></a></span></th>\n";
	print "\t\t<th width=\"120\">Video Format <span><a href=\"/admin/film_listing/sort/videoasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Video Format\" /></a> <a href=\"/admin/film_listing/sort/videodesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Video Format\" /></a></span></th>\n";
	print "\t\t<th width=\"100\">Event Type <span><a href=\"/admin/film_listing/sort/eventasc/\"><img src=\"/assets/images/icons/sortasc.png\" width=\"10\" height=\"5\" alt=\"Sort Ascending by Event Type\" /></a> <a href=\"/admin/film_listing/sort/eventdesc/\"><img src=\"/assets/images/icons/sortdesc.png\" width=\"10\" height=\"5\" alt=\"Sort Descending by Event Type\" /></a></span></th>\n";
	print "\t\t<th width=\"90\">Country</th>\n";
	print "\t\t<th width=\"55\">Synopsis</th>\n";
	print "\t\t<th width=\"60\">Photos /<br /> Videos</th>\n";
	print "\t\t<th width=\"60\">Generate PDF</th>\n";
	print "\t\t<th width=\"60\">Scheduled</th>\n";
	print "\t\t<th width=\"60\">Confirmed</th>\n";
	print "\t\t<th width=\"60\">Published</th>\n";
	print "\t</tr>\n";
	print "\t</thead>\n\n";
	print "\t<tbody>\n";
	
	$x = 1;
	if (count($films) == 0) {
		$film_total = 0;

		print "\t<tr valign=\"top\" class=\"oddrow\">\n";
		print "\t\t<td colspan=\"12\" align=\"center\">There are no films/events listed for this festival.</td>\n";
		print "\t</tr>\n";
	} else {
		$film_total = count($films);
	
		foreach ($films as $thisFilm) {
			$movie_id = $thisFilm->movie_id;
			$filmCountries = $this->country->get_movie_countries($movie_id);
			$country_string = "";

			$film_photos = array();
			foreach ($photos as $thisPhoto) {
				if ($thisPhoto->movie_id == $movie_id) { $film_photos[] = $thisPhoto; }
			}
			$film_videos = array();
			foreach ($videos as $thisVideo) {
				if ($thisVideo->movie_id == $movie_id) { $film_videos[] = $thisVideo; }
			}		
		
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		
			print "\t<td><div class=\"button ui-state-default\"><a href=\"/admin/film_edit/update/".$thisFilm->slug."\">Edit</a></div></td>\n";
			print "\t<td><a href=\"/films/detail/".$thisFilm->slug."\" target=\"_blank\">".switch_title($thisFilm->title_en)."</a>";
			if ($thisFilm->title != "") { print "<br>".$thisFilm->title; }
			print "</td>\n";
			print "\t<td>".$thisFilm->category_name."</td>\n";
			print "\t<td>".$thisFilm->format_name."</td>\n";
			print "\t<td>".$thisFilm->event_name."</td>\n";
			print "\t<td>";
	
			foreach ($filmCountries as $thisCountry) {
				$country_string .= $thisCountry->name.", ";
			}
			$country_string = trim($country_string, ", ");
			if ($country_string == "") { $country_string = "None"; }
			print "\t".$country_string."</td>\n";
			
			$long_syn = $short_syn = false;
			switch ($thisFilm->synopsis) {
				case "":
				case "<br />":
				case "<br />\n":
				case "<p>&nbsp;</p>": $long_syn = false; break;
				default: $long_syn = true; break;
			}
			switch ($thisFilm->synopsis_short) {
				case "":
				case "<br />":
				case "<br />\n":
				case "<p>&nbsp;</p>": $short_syn = false; break;
				default: $short_syn = true; break;
			}
	
			if ( $long_syn == false && $short_syn == false ) {
				print "\t<td class=\"cent lborder icon_cross\"></td>\n";
			} elseif ( $long_syn == true && $short_syn == true ) {
				print "\t<td class=\"cent lborder icon_tick\"></td>\n";
			} elseif ( $long_syn == true || $short_syn == true ) {
				print "\t<td class=\"cent lborder icon_ticross\"></td>\n";
			}

			print "\t<td class=\"cent media\">".count($film_photos)." / ".count($film_videos)."</td>\n";

			if ($thisFilm->Confirmed != "0") {
				print "\t<td class=\"cent lborder\">";
				print "<a class=\"choose_photo\" href=\"/admin/film_listing/generate_pdf/".$thisFilm->slug."/english/\" title=\"English\">English</a>";
				if ($thisFilm->title != "") {
					print "<br /><a class=\"choose_photo\" href=\"/admin/film_listing/generate_pdf/".$thisFilm->slug."/original/\" title=\"Original Language\">Original</a>\n";
				}
				print "<div class=\"hiddenphoto\">";
				$urls = "";
				foreach ($film_photos as $thisPhoto) { $urls .= $thisPhoto->url_cropsmall."|"; }
				$urls = trim($urls,"|");
				print $urls."</div>";
				print "</td>\n";
			} else {
				print "\t<td class=\"cent lborder\"></td>\n";
			}
			$screeningsExist = 0;
			foreach ($schedule as $thisScreening) { 
				$film_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($film_array as $screeningFilm) {
					if ($screeningFilm == $movie_id) { $screeningsExist++; break 2; }
				}
			}
			if ($screeningsExist != "0") {
				print "\t<td class=\"cent icon_tick\"></td>\n";
			} else {
				print "\t<td class=\"cent icon_cross\"></td>\n";
			}

			if ($thisFilm->Confirmed != "0") {
				print "\t<td class=\"cent\"><a id=\"Confirmed-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"Confirmed\" class=\"icon_tick\"></a></td>\n";
			} else {
				print "\t<td class=\"cent\"><a id=\"Confirmed-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"Confirmed\" class=\"icon_cross\"></a></td>\n";
			}
			if ($thisFilm->Published != "0") {
				print "\t<td class=\"cent\"><a id=\"Published-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
			} else {
				print "\t<td class=\"cent\"><a id=\"Published-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
			}
			print "\t</tr>\n";		
			$x++;
		}
	}
	print "\t</tbody>\n";
	print "</table>\n";
	print "</fieldset>\n";
	print "<button id=\"add-film2\">Add Film/Event</button><br>";
?>

<div id="dialog-form" title="Add A Film/Event">
	<form name="addFilmForm" id="addFilmForm">
    <?php print form_hidden("festival_id",$festival[0]->id); ?>
    <table width="100%">
    	<tbody style="border-top:none;">
    	<tr valign="top">
        	<td width="50%"><div style="float:right; margin-right:50px; display:none;"><span style="font-size:11px;">Uppercase?</span> <input type="checkbox" name="uppercase" id="uppercase" value="" /></div>
            <label for="title_en">English Title</label><span class="req"> *</span><br /><input type="text" name="title_en" id="title_en" class="text ui-widget-content ui-corner-all" /></td>
            <td width="50%"><label for="runtime">Runtime (in minutes, like: 90)</label><span class="req"> *</span><br /><input type="text" name="runtime" id="runtime" value="0" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
        <tr valign="top">
            <td><label for="eventtype">Event Type</label><br /><select name="eventtype" id="eventtype" class="select ui-widget-content ui-corner-all"><option value="">Select an Event Type</option>
<?php foreach ($eventtypes as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
            <td><label for="format">Video Format</label><br /><select name="videoformat" id="videoformat" class="select ui-widget-content ui-corner-all"><option value="">Select a Video Format</option>
<?php foreach ($videoformat as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
        </tr>
    	<tr valign="top">
        	<td><label for="section">Section</label><br /><select name="section" id="section" class="select ui-widget-content ui-corner-all"><option value="">Select a Section</option>
<?php foreach ($sections as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
        	<td><label for="year">Year Completed</label><br /><?php print form_dropdown('year', $yearcompletion, date("Y"),"id='year' class='select ui-widget-content ui-corner-all'"); ?></td>
        </tr>
    	<tr valign="top">
            <td><label for="premieretype">Premiere Type</label><br /><select name="premieretype" id="premieretype" class="select ui-widget-content ui-corner-all"><option value="">Select a Premiere Type</option>
<?php foreach ($premieres as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
        	<td><label for="country">Country</label><br /><select name="country" id="country" class="select ui-widget-content ui-corner-all"><option value="0">Select a Country</option>
<?php foreach ($countries as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
        </tr>
    	<tr valign="top">
			<td><label for="language">Language</label><br /><select name="language" id="language" class="select ui-widget-content ui-corner-all"><option value="0">Select a Language</option>
<?php foreach ($languages as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
			<td><label for="genre">Genre</label><br /><select name="genre" id="genre" class="select ui-widget-content ui-corner-all"><option value="0">Select a Genre</option>
<?php foreach ($genres as $rec) {	print "\t\t<option value=\"".$rec->id."\">".$rec->name."</option>\n"; } ?>
            </select></td>
        </tr>
        </tbody>
    </table>
	</form>
	<p class="validateTips">Additional Countries, Languages and Genres can be added when you edit this film.</p>
</div>

<div id="pdfPhoto-dialog" title="Generate PDF - Choose a Photo">
</div>

<script type="text/javascript">
$(function() {
	$("input:submit").button();
	$("button").button();
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

	$('#uppercase').on('click', function() {
		var title = $("#title_en"); if( $(this).is(':checked') ) { $(this).val(title.val()); title.val(title.val().toUpperCase()); } else { title.val($(this).val()); }
	});

	var title = $("#title"),
		title_en = $("#title_en"),
		textFields = $([]).add(title).add(title_en);
	var videoformat = $("#videoformat"),
		eventtype = $("#eventtype"),
		section = $("#section"),
		country = $("#country"),
		language = $("#language"),
		genre = $("#genre"),
		premiere = $("#premieretype"),
		selectFields = $([]).add(videoformat).add(eventtype).add(section).add(country).add(language).add(genre).add(premiere);

	// validate signup form on keyup and submit
	$("#addFilmForm").validate({
		rules: { title_en: "required", runtime: { required: true, digits: true } },
		messages: { title_en: "Please enter an English Film Title", runtime: "Please enter a runtime in whole numbers, like: 120" }
	});

	$("#dialog-form").dialog({
		autoOpen: false,
		height: 440,
		width: 600,
		modal: true,
		buttons: {
			'Cancel': function() { $(this).dialog('close'); },
			'Add Film/Event': function() {
				textFields.removeClass('ui-state-error');
				if ($('#addFilmForm').validate().form() == true) {
            		$.ajax({
						success: function(msg) {
							window.location="/admin/film_edit/update/"+msg+"/added/"; },
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addFilmForm').serialize(),
						url: '/admin/film_listing/add/',
						type: 'POST',
						dataType: 'html'
					}); 
					$(this).dialog('close');
	            }
			}
		},
		close: function() {
			textFields.val('').removeClass('ui-state-error');
			selectFields.val('0').removeClass('ui-state-error');
			$('#runtime').val('0');
			$('#year').val('<?php print date("Y"); ?>');
		}
	});
	
	$('#film_listing_table').on('click', 'a.icon_cross', function(e) {
		e.preventDefault();
		toggle_value($(this).data('type'), $(this).data('id'));
	});
	$('#film_listing_table').on('click', 'a.icon_tick', function(e) {
		e.preventDefault();
		toggle_value($(this).data('type'), $(this).data('id'));
	});

	$('#pdfPhoto-dialog').dialog({
		autoOpen: false,
		width: 475,
		height: "auto",
		modal: false,
		close: function() { }
	});

	$('#pdfPhoto-dialog').on('click', '.photo', function() {
		$('#pdfPhoto-dialog').dialog('close');
	})

	$('#film_listing_table').on('click', '.choose_photo', function(e) {
		var film_urls = $(this).nextAll('.hiddenphoto').html();
		if (film_urls != "") {
			e.preventDefault();
			
			var film_name = $(this).closest("tr").children().first().next().find("a").text();
			var film_href = $(this).attr("href");
			var url_array = film_urls.split("|");
			var photo_html = "<p>Choose a photo to be added to the PDF.</p>";
			var count = 0;
	
			jQuery.each(url_array, function(index, value) {
				var plusone = parseInt(count + 1);
				photo_html = photo_html + '<a href="'+film_href+count+'/" class="photo"><img src="'+value+'" alt="Photo #'+plusone+'" title="Photo #'+plusone+'" width="210" height="120"></a>';
				count++;
			});
	
			$('#pdfPhoto-dialog').html(photo_html);
			$('#pdfPhoto-dialog').dialog('option','title','Generate PDF - '+film_name+' - '+$(this).attr('title'));		
			$('#pdfPhoto-dialog').dialog('open');
		}
	});
	
	$('#add-film, #add-film2').each(function() {
		$(this).on('click', function() {
			$('#dialog-form').dialog('open');
			$(this).removeClass("ui-state-focus");
		});
	});

<?php 	if ($this->session->userdata('limited') == 0) { ?>
	$('#publish-all').button().on('click', function() {
		var decision = confirm("Publish all CONFIRMED films/events for this festival?");
		if (decision == true) {
			$.ajax({
				success: function(msg) {
					window.location="/admin/film_listing/"; },
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#publishAllForm').serialize(),
				url: '/admin/film_listing/publish_all/',
				type: 'POST',
				dataType: 'html'
			}); 
		}
		$(this).removeClass("ui-state-focus");
		return false;
	});
	$('#unpublish-all').button().on('click', function() {
		var decision = confirm("Unpublish all films/events for this festival?");
		if (decision == true) {
			$.ajax({
				success: function(msg) {
					window.location="/admin/film_listing/"; },
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#unpublishAllForm').serialize(),
				url: '/admin/film_listing/unpublish_all/',
				type: 'POST',
				dataType: 'html'
			}); 
		}
		$(this).removeClass("ui-state-focus");
		return false;
	});
<?php } ?>
	jQuery("#film_listing_table").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
});
</script>