<button class="add-user">Add New User</button><br>
	
<fieldset class="ui-corner-all" style="padding:0px">
<table id="manage_users_table" width="100%" cellpadding="5" cellspacing="0" border="0">
	<thead>
		<tr>
			<th width="100">&nbsp;</th>
			<th width="150">First/Last Name</th>
			<th width="150">Username</th>
			<th width="200">Email</th>
			<th width="100">Last Login</th>
			<th width="100">Enabled</th>
			<th width="100">Limited Access</th>
			<th width="100">Admin Access</th>
		</tr>
	</thead>
	<tbody>

<?php
	$x = 1;
	foreach ($user_list as $thisUser) {
		if ($thisUser->id != 1) {
			if (($x % 2) == 1) { print "\t\t\t<tr valign=\"top\" class=\"oddrow\">\n"; }
			if (($x % 2) == 0) { print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n"; }
		
			print "\t<td class=\"cent\"><button class=\"edit\" data-id=\"".$thisUser->id."\">Edit</button></td>\n";
			print "\t<td>".$thisUser->FirstName." ".$thisUser->LastName."</td>\n";
			print "\t<td>".$thisUser->Username."</td>\n";
			print "\t<td>".$thisUser->Email."</td>\n";
			if ($thisUser->LastLogin == "") {
				print "\t<td>&nbsp;</td>\n";
			} else {
				print "\t<td>".date("M j Y",strtotime($thisUser->LastLogin))."</td>\n";
			}
	
			if ($thisUser->Enabled != "0") {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			if ($thisUser->LimitedAccess != "0") {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			if ($thisUser->Admin != "0") {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t<td class=\"cent\"><img src=\"/assets/images/icons/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			print "\t</tr>\n";
			$x++;
		}
	}
?>
	</tbody>
</table>
</fieldset>	
<button class="add-user">Add New User</button><br>


<div class="AddUsersDialog" title="Add A New User">
	<form name="addUserForm" id="addUserForm">
    <table width="100%">
    	<tbody style="border-top:none;">
    	<tr valign="top">
        	<td width="50%"><label for="Username">Username</label><span class="req"> *</span><br /><input type="text" name="Username" id="Username" class="text ui-widget-content ui-corner-all" /><br /><button id="CheckUsername">Check Username</button> <div id="UsernameResults"></div></td>
            <td width="5%" rowspan="6">&nbsp;</td>
            <td width="45%" rowspan="6">
            	<h4>User Permissions</h4>
                
                <p>
					<label for="addAccessLevel">Access Level:</label>
					<input type="text" id="addAccessLevel" name="addAccessLevel" />
				</p>
                
                <div id="addSlider"></div>

				<p class="validateTips">Giving someone admin access will allow them to access this Manage Users page. Limited Access means they can add and update items, but deleting is not allowed.</p>                
                
			    <table width="100%">
			    	<tbody style="border-top:none;">
   				 	<tr valign="top">
        				<td colspan="2">&nbsp;</td>
                    </tr>
   				 	<tr valign="top">
        				<td><label for="ProgSuite">Programming Suite</label></td>
        				<td><input type="checkbox" name="ProgSuite" id="ProgSuite" value="1" checked="checked" disabled="disabled" /></td>
                    </tr>
   				 	<tr valign="top">
        				<td><label for="GuestSuite">Guests Suite</label></td>
        				<td><input type="checkbox" name="GuestSuite" id="GuestSuite" value="1" checked="checked" disabled="disabled" /></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    	<tr valign="top">
        	<td><label for="FirstName">First Name</label><span class="req"> *</span><br /><input type="text" name="FirstName" id="FirstName" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
        <tr valign="top">
            <td><label for="LastName">Last Name</label><span class="req"> *</span><br /><input type="text" name="LastName" id="LastName" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td><label for="Email">Email Address</label><span class="req"> *</span><br /><input type="text" name="Email" id="Email" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td><label for="Password1">Password</label><span class="req"> *</span><br /><input type="password" name="Password1" id="Password1" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td><label for="Password2">Confirm Password</label><span class="req"> *</span><br /><input type="password" name="Password2" id="Password2" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
        </tbody>
    </table>
	</form>
</div>

<div class="EditUsersDialog" title="Edit User">
	<form name="editUserForm" id="editUserForm">
    <input type="hidden" name="user-id-update" id="user-id-update" value="" />
    <input type="hidden" name="last-login" id="last-login" value="" />
    <table width="100%">
    	<tbody style="border-top:none;">
    	<tr valign="top">
        	<td width="50%"><label for="Username-update">Username</label><span class="req"> *</span><br /><input type="text" name="Username-update" id="Username-update" class="text ui-widget-content ui-corner-all" /></td>
            <td width="5%" rowspan="6">&nbsp;</td>
            <td width="45%" rowspan="6">
            	<h4>User Permissions</h4>
                <p>
					<label for="updateAccessLevel">Access Level:</label>
					<input type="text" id="updateAccessLevel" name="updateAccessLevel" />
				</p>
                
                <div id="updateSlider"></div>
				<p class="validateTips">Giving someone admin access will allow them to access this Manage Users page. Limited Access means they can add and update items, but deleting is not allowed.</p>                

			    <table width="100%">
			    	<tbody style="border-top:none;">
   				 	<tr valign="top">
        				<td colspan="2">&nbsp;</td>
                    </tr>
   				 	<tr valign="top">
        				<td><label for="ProgSuite">Programming Suite</label></td>
        				<td><input type="checkbox" name="ProgSuite" id="ProgSuite" value="1" checked="checked" disabled="disabled" /></td>
                    </tr>
   				 	<tr valign="top">
        				<td><label for="GuestSuite">Guests Suite</label></td>
        				<td><input type="checkbox" name="GuestSuite" id="GuestSuite" value="1" checked="checked" disabled="disabled" /></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    	<tr valign="top">
        	<td><label for="FirstName-update">First Name</label><span class="req"> *</span><br /><input type="text" name="FirstName-update" id="FirstName-update" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
        <tr valign="top">
            <td><label for="LastName-update">Last Name</label><span class="req"> *</span><br /><input type="text" name="LastName-update" id="LastName-update" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td><label for="Email-update">Email Address</label><span class="req"> *</span><br /><input type="text" name="Email-update" id="Email-update" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td><label for="Password1-update">Password</label><br /><input type="password" name="Password1-update" id="Password1-update" class="text ui-widget-content ui-corner-all" /></td>
        </tr>
    	<tr valign="top">
			<td>
            	<label for="Password2-update">Confirm Password</label><br /><input type="password" name="Password2-update" id="Password2-update" class="text ui-widget-content ui-corner-all" />
                <p class="validateTips">Leave both password fields blank to keep the user's password as it is.</p>
            </td>
        </tr>
        </tbody>
    </table>
	</form>
</div>

<script type="text/javascript">
$(function() {
	$("input:submit").button();
	$("button").button();

	// validate add user form on keyup and submit
	$("#addUserForm").validate({
		rules: { "Username": "required", "FirstName": "required", "LastName": "required", "Email": { required: true, email:true }, "Password1": "required", "Password2": { equalTo: "#Password1" } },
		messages: { "Username": "Please enter a username.", "FirstName": "Please enter a first name.", "LastName": "Please enter a last name.", "Email": "Please enter an email address.", "Password1": "Please enter a password.", "Password2": "Passwords do not match." }
	});

	$(".AddUsersDialog").dialog({
		autoOpen: false,
		height: 520,
		width: 500,
		modal: true,
		buttons: {
			'Cancel': function() { $(this).dialog('close'); },
			'Add User': function() {
				if ($('#addUserForm').validate().form() == true) {
            		$.ajax({
						success: function(msg) {
							$('#manage_users_table TBODY').prepend(msg);
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2);
						},
						data: $('#addUserForm').serialize(),
						url: '/admin/user_manage/add_user/',
						type: 'POST',
						dataType: 'html'
					}); 
					$(this).dialog('close');
	            }
			}
		},
		close: function() {
			$("#addUserForm INPUT[type=text]").each(function() { $(this).val(""); });
			$("#addUserForm INPUT[type=password]").each(function() { $(this).val(""); });
			$("#addAccessLevel").val("Normal");
			$("#addAccessLevel").css("color","#F6931F");
			$("#addSlider").slider("value", 50);
		}
	});

	// User Access Level Slider - Add User
	$("#addSlider").slider({
		value:50,
		min: 0,
		max: 75,
		step: 25,
		slide: function( event, ui ) {
			switch ( ui.value ) {
				case 0: $("#addAccessLevel").val("Disabled");
						 $("#addAccessLevel").css("color","#FF0000");
						   break;
				case 25: $("#addAccessLevel").val("Limited");
						 $("#addAccessLevel").css("color","#F6931F");
						   break;
				case 50: $("#addAccessLevel").val("Normal");
						 $("#addAccessLevel").css("color","#F6931F");
						   break;
				case 75: $("#addAccessLevel").val("Administrator");
						 $("#addAccessLevel").css("color","#00FF00");
						   break;
			}
		}
	});

	// validate edit user form on keyup and submit
	$("#editUserForm").validate({
		rules: { "Username-update": "required", "FirstName-update": "required", "LastName-update": "required", "Email-update": { required: true, email:true }, "Password2-update": { equalTo: "#Password1-update" } },
		messages: { "Username-update": "Please enter a username.", "FirstName-update": "Please enter your first name.", "LastName-update": "Please enter your last name.", "Email-update": "Please enter your email address.", "Password2-update": "Passwords do not match." }
	});

	$(".EditUsersDialog").dialog({
		autoOpen: false,
		height: 520,
		width: 500,
		modal: true,
		buttons: {
			'Cancel': function() { $(this).dialog('close'); },
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var decision = confirm("Are you sure you really want to delete this user?");
				if (decision == true) {
					var user_id = $('#user-id-update').val();
					$.ajax({
						success: function(msg, text, xhr) {
							$('#edituser-'+user_id).closest('tr').slideUp();
		    				var timeoutID = window.setTimeout(function () { $('#edituser-'+user_id).closest('tr').remove() }, 2000);
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editUserForm').serialize(),
						url: '/admin/user_manage/delete_user/',
						type: 'POST',
						dataType: 'html'
					}); 

					$(this).dialog('close');
				}
			},
<?php } ?>
			'Update User': function() {
				if ($('#editUserForm').validate().form() == true) {
					var user_id = $('#user-id-update').val();
            		$.ajax({
						success: function(msg) {
							$('#edituser-'+user_id).closest('tr').replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									//alert(msg);
									userJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/user_manage/return_user_json/',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editUserForm').serialize(),
						url: '/admin/user_manage/update_user/',
						type: 'POST',
						dataType: 'html'
					}); 
					$(this).dialog('close');
	            }
			}
		},
		close: function() {
		}
	});

	// User Access Level Slider - Edit User
	$("#updateSlider").slider({
		value:50,
		min: 0,
		max: 75,
		step: 25,
		slide: function( event, ui ) {
			switch ( ui.value ) {
				case 0: $("#updateAccessLevel").val("Disabled");
						 $("#updateAccessLevel").css("color","#FF0000");
						   break;
				case 25: $("#updateAccessLevel").val("Limited");
						 $("#updateAccessLevel").css("color","#F6931F");
						   break;
				case 50: $("#updateAccessLevel").val("Normal");
						 $("#updateAccessLevel").css("color","#F6931F");
						   break;
				case 75: $("#updateAccessLevel").val("Administrator");
						 $("#updateAccessLevel").css("color","#00FF00");
						   break;
			}
		}
	});
	$("#addAccessLevel,#updateAccessLevel").each(function() {
		$(this).val("Normal");
	});

	var userJSON = <?php print $user_json; ?>;

	$('#manage_users_table').on('click','button.edit', function() {
		var user_id = $(this).data('id').toString();
		open_edit_user_dialog(user_id,userJSON);
		$(this).removeClass("ui-state-focus");
		return false;
	})

	$('#CheckUsername').button().on('click', function() {
		$.ajax({
			success: function(msg) {
				$('#UsernameResults').html(msg);
			},
			error: function(xhr, msg1, msg2){
				alert( "Failure! " + xhr + msg1 + msg2); },
			data: $('#addUserForm').serialize(),
			url: '/admin/user_manage/check_username/',
			type: 'POST',
			dataType: 'html'
		}); 
		$(this).removeClass("ui-state-focus");
		return false;
	});

	jQuery("#film_listing_table").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
	$('.add-user').on('click', function() { $('.AddUsersDialog').dialog('open'); $(this).removeClass("ui-state-focus"); });
	$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });
});
</script>