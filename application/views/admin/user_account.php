<?php
	if (isset($_GET['update'])) {
		print "<div align=\"center\" style=\"font-weight:bold; color:#CC0000;\">".$_GET['update']." has been successfully updated.</div>";
	}
?>
	
<fieldset class="ui-corner-all" style="padding:0px">
	<div class="EditUsersDialog" title="Edit User">
		<form name="editUserForm" id="editUserForm">
		<h3 style="margin:10px 10px 0 10px;">My Account Details</h3>
	    <input type="hidden" name="user-id-update" id="user-id-update" value="<?php print $user_list[0]->id; ?>" />
	    <input type="hidden" name="original-username" id="original-username" value="<?php print $user_list[0]->Username; ?>" />
	    <table style="margin: 10px; width:260px;">
	    	<tbody style="border-top:none;">
	    	<tr valign="top">
	        	<td width="50%"><label for="Username-update">Username</label><span class="req"> *</span><br /><input type="text" value="<?php print $user_list[0]->Username; ?>" name="Username-update" id="Username-update" class="text ui-widget-content ui-corner-all" /></td>
	        </tr>
	    	<tr valign="top">
	        	<td><label for="FirstName-update">First Name</label><span class="req"> *</span><br /><input type="text" value="<?php print $user_list[0]->FirstName; ?>" name="FirstName-update" id="FirstName-update" class="text ui-widget-content ui-corner-all" /></td>
	        </tr>
	        <tr valign="top">
	            <td><label for="LastName-update">Last Name</label><span class="req"> *</span><br /><input type="text" value="<?php print $user_list[0]->LastName; ?>" name="LastName-update" id="LastName-update" class="text ui-widget-content ui-corner-all" /></td>
	        </tr>
	    	<tr valign="top">
				<td><label for="Email-update">Email Address</label><span class="req"> *</span><br /><input type="text" value="<?php print $user_list[0]->Email; ?>" name="Email-update" id="Email-update" class="text ui-widget-content ui-corner-all" /></td>
	        </tr>
	    	<tr valign="top">
				<td><label for="Password1-update">Password</label><br /><input type="password" name="Password1-update" id="Password1-update" class="text ui-widget-content ui-corner-all" /></td>
	        </tr>
	    	<tr valign="top">
				<td>
	            	<label for="Password2-update">Confirm Password</label><br /><input type="password" name="Password2-update" id="Password2-update" class="text ui-widget-content ui-corner-all" />
	                <p class="validateTips" style="margin:0;">Leave both password fields blank to keep your password as it is.</p>
	            </td>
	        </tr>
	    	<tr valign="top">
				<td align="center">
	            	<button id="updateAccount">Update Account</button>
	            </td>
	        </tr>
	        </tbody>
	    </table>
		</form>
	</div>
</fieldset>	

<script type="text/javascript">
$(function() {
	$("button").button();

	// validate edit user form on keyup and submit
	$("#editUserForm").validate({
		rules: { "Username-update": "required", "FirstName-update": "required", "LastName-update": "required", "Email-update": { required: true, email:true }, "Password2-update": { equalTo: "#Password1-update" } },
		messages: { "Username-update": "Please enter a username.", "FirstName-update": "Please enter your first name.", "LastName-update": "Please enter your last name.", "Email-update": "Please enter your email address.", "Password2-update": "Passwords do not match." }
	});

	$('#updateAccount').button().on('click', function() {
		if ($('#editUserForm').validate().form() == true) {
			var decision = true; var logout = false;
			if ( $('#original-username').val() != $('#Username-update').val() ) {
				decision = confirm("Are you sure you want to change your username? If you do so, you will be logged out right now, and will have to log back in.");
				if (decision == true) { logout = true; }
			}
			if (decision == true) {
				$.ajax({
					success: function(msg) {
						if (logout == false) {
							window.location="/admin/user_account/?update="+msg;
						} else {
							window.location="/login/logout/";
						}
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#editUserForm').serialize(),
					url: '/admin/user_account/update_user/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
		}
		$(this).removeClass("ui-state-focus");
		return false;
	});
});
</script>