<?php
$location_array = convert_to_array($locations);
$location_array2 = convert_to_array($locations,"slug","name","All Locations");
$film_screening_array = convert_film_screenings_to_array($films);

print "<div>\n";
print "<h2>".$festival[0]->year." ".$festival[0]->name." Schedule (".date("n/d",strtotime($festival[0]->startdate))." - ".date("n/d/Y",strtotime($festival[0]->enddate)).")</h2>\n";

if ($this->session->userdata('limited') == 0) {
	print "<form id=\"unpublishAllForm\"><input type=\"hidden\" value=\"".$festival[0]->id."\" name=\"festival_id\"><button id=\"unpublish-all\">Unpublish All</button></form>\n";
	print "<form id=\"publishAllForm\"><input type=\"hidden\" value=\"".$festival[0]->id."\" name=\"festival_id\"><button id=\"publish-all\">Publish All</button></form>\n";
}
print "<button id=\"add-screening\">Add Screening</button>";
print "<div style=\"float:left; padding: 6px 12px;\"><label for=\"filter-location\">Filter by Location:</label>&nbsp;";
print form_dropdown("filter-location", $location_array2, $location_sel, "id='filter-location' class='select ui-widget-content ui-corner-all'")."</div>";

print "<div id=\"screening-legend\">\n";
print "\t<div class=\"legend-entry\">\n";
print "\t\t<span style=\"color:#000000;\"><b>Screening Status:</b></span>";
print "\t</div>\n";
print "\t<div class=\"legend-entry\">\n";
print "\t\t<div class=\"venue_format_box\" style=\"background-color:#CCCCCC;\"></div> <span>Private</span>";
print "\t</div>\n";
print "\t<div class=\"legend-entry\">\n";
print "\t\t<div class=\"venue_format_box\" style=\"background-color:#FFCCCC;\"></div> <span>Standby</span>";
print "\t</div>\n";
print "</div>\n";
print "</div>\n<br clear=\"all\">\n";

print "<fieldset class=\"ui-corner-all\" style=\"padding:0px;\">\n";
print "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"schedule_list\">\n";
print "<thead>\n";
?>
        <tr valign="top">
            <th width="14%"><div align="center">(<?php print count($schedule); ?> Screenings)</div></th>
            <th width="12%">Time</th>
            <th width="35%">Program Name/Details (<?php print $program_num; ?> Unique Programs)</th>
            <th width="12%">Location</th>
            <th width="5%">Private</th>
            <th width="5%">Standby</th>
            <th width="4%">Free</th>
            <th width="6%">Ticketed</th>
            <th width="8%">Published</th>
        </tr>
<?php
print "</thead>\n";
print "<tbody style=\"border-top:none;\">\n";
?>
        <tr id="addScreeningHere">
        	<td colspan="9"><input type="hidden" id="screening-id-new" name="screening-id-new" value="0" /></td>
        </tr>
<?php
if (count($schedule) == 0) {
	print "<tr><td align=\"center\" colspan=\"9\">No screenings have been added for this festival yet.</td></tr>";
}

$previousDate = ""; $counter = 0;
$prevScrnDate = ""; $prevScrnTime = ""; $prevScrnMovie = ""; $prevScrnProgramName = "";
foreach ($schedule as $thisScreening) {
	// Check to see that Screening information does not match exactly the last entry (eliminate interlocked screenings from showing twice)
	if (($counter != 0) && ($thisScreening->date == $prevScrnDate) && ($thisScreening->time == $prevScrnTime) && ($thisScreening->movie_id == $prevScrnMovie) && ($thisScreening->program_name == $prevScrnProgramName) ) {
		// Display nothing	
	} else {
		$title = ""; $title_orig = ""; $year = ""; $runtime = ""; $film_id = "";
		foreach ($films as $thisFilm) {
			if ($thisFilm->movie_id == $thisScreening->movie_id) {
				$title = switch_title($thisFilm->title_en);
				$title_orig = $thisFilm->title;
				$year = $thisFilm->year;
				$runtime = $thisFilm->runtime_int;
				$film_id = $thisFilm->movie_id;
				$slug = $thisFilm->slug;
				break;
			}
		}
		$startTime = strtotime($thisScreening->date." ".$thisScreening->time);
		$length = 0;
		$film_ids = explode(",",$thisScreening->program_movie_ids);
		foreach ($film_ids as $thisFilmID) {
			foreach ($films as $thisFilm) {
				if ($thisFilmID == $thisFilm->movie_id) { $length = $length + $thisFilm->runtime_int; }
			}
		}
		if ($thisScreening->intro == 1) { $length += $thisScreening->intro_length; }
		if ($thisScreening->fest_trailer == 1) { $length += $thisScreening->fest_trailer_length; }
		if ($thisScreening->sponsor_trailer1 == 1) { $length += $thisScreening->sponsor_trailer1_length; }
		if ($thisScreening->sponsor_trailer2 == 1) { $length += $thisScreening->sponsor_trailer2_length; }
		if ($thisScreening->q_and_a == 1) { $length += $thisScreening->q_and_a_length; }
		$endTime = $startTime + intval($length*60);


		if ($previousDate != $thisScreening->date) {
			print "\t<tr valign=\"top\">\n";
				if ($counter == 0) {
					print "\t\t<td colspan=\"9\"><h3 style=\"border-bottom:1px solid black;\">".date("l, F jS",strtotime($thisScreening->date))."</h3></td>\n";
				} else {
					print "\t\t<td colspan=\"9\"><br /><h3 style=\"border-bottom:1px solid black;\">".date("l, F jS",strtotime($thisScreening->date))."</h3></td>\n";
				}
			print "\t</tr>\n";
		}	$previousDate = $thisScreening->date;
	
		$location = $thisScreening->name;

		// Start table output
		print "\t<tr id=\"row-".$thisScreening->id."\" valign=\"top\" style=\"";
		if ($thisScreening->Private != "0") {
			print " background-color:#CCCCCC;";
		}
		if ($thisScreening->Rush != "0") {
			print " background-color:#FFCCCC;";
		}
		print "\">\n";
		print "\t\t<td><button id=\"scrn-".$thisScreening->id."\" class=\"edit\" data-id=\"".$thisScreening->id."\">Edit Screening</button></td>\n";
		print "\t\t<td><span class=\"schedule_time\">".date("g:i A",$startTime)." - ".date("g:i A",$endTime)."</span></td>\n";
		print "\t\t<td>";

		$film_array = explode(",",$thisScreening->program_movie_ids);		
		if (count($film_array) == 1) { // Single Film
			print "<a href=\"/admin/film_edit/update/".$slug."#tabs-4\" class=\"schedule_title\">".$title."</a>";
			if ($title_orig != "") { print "<br>".$title_orig; }
			if ($thisScreening->q_and_a == 1) { print " <img src=\"/assets/images/icons/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }
		} else if (count($film_array) > 1) { // Multiple Films
			if ($thisScreening->program_name != "") {
				print "<span style=\"color:#000000; font-weight:bold;\">".$thisScreening->program_name."</span>";
			} else {
				print "<a href=\"/admin/film_edit/update/".$slug."#tabs-4\" class=\"schedule_title\">".$title."</a>";
				if ($title_orig != "") { print "<br>".$title_orig; }
			}
			if ($thisScreening->q_and_a == 1) { print " <img src=\"/assets/images/icons/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }

			$filmlist = "";
			print "\t\t<br /><span class=\"shorts_title\">Plays with:</span> ";
			foreach ($film_array as $thisFilmID2) {
				foreach ($filmids as $thisFilm2) {
					if ($thisFilmID2 == $thisFilm2->movie_id) {
						if ($thisScreening->program_name != "") {
							// Include first film in program if program name is defined
							$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
						} else {
							// Exclude first film in program if program name is not defined
							if ($thisScreening->movie_id != $thisFilmID2) {
								$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
							}
						}
					}
				}
			}
			$filmlist = trim($filmlist,", ");
			print $filmlist;
		}

		print "</td>\n";
		print "\t\t<td>".$location."</td>\n";

		if ($thisScreening->Private != "0") {
			print "\t<td class=\"cent\"><a id=\"Private-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Private-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisScreening->Rush != "0") {
			print "\t<td class=\"cent\"><a id=\"Rush-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Rush-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($thisScreening->Free != "0") {
			print "\t<td class=\"cent\"><a id=\"Free-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Free-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}

		if ($thisScreening->url != "") {
			print "\t<td class=\"cent icon_tick\"></td>\n";
		} else {
			if ($thisScreening->Private == "1") {
				print "\t<td>Private</td>\n";
			} else if ($thisScreening->Free == "1") {
				print "\t<td>Free</td>\n";
			} else {
				print "\t<td class=\"cent icon_cross\"></td>\n";
			}
		}

		if ($thisScreening->Published != "0") {
			print "\t<td class=\"cent\"><a id=\"Published-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Published-".$thisScreening->id."\" href=\"#\" data-id=\"".$thisScreening->id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t</tr>\n";	
	}
	
	$prevScrnDate = $thisScreening->date;
	$prevScrnTime = $thisScreening->time;
	$prevScrnMovie = $thisScreening->movie_id;
	$prevScrnProgramName = $thisScreening->program_name;
	$counter++;
}
print "</tbody></table>";
print "</fieldset>";
print "<button id=\"add-screening2\">Add Screening</button><br>";
?>

<div id="addScreening-dialog" title="Add Screening">
<form name="addScreeningForm" id="addScreeningForm">
	<input type="hidden" name="total_films" id="total_films" value="1" />
	<input type="hidden" name="total-runtime" id="total-runtime" value="0" />
    <table class="ScreeningDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td colspan="2">
				<h3>Screening Details</h3>
            </td>
            <td>
				<h3>Program / Notes</h3>
            </td>
            <td>
				<h3>Screening Breakdown</h3>
            </td>
        </tr>
        <tr valign="top">
            <td width="17%">
				<label for="scrn-published-new">Published:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-published-new', 'id'=>'scrn-published-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-rush-new">Standby:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-rush-new', 'id'=>'scrn-rush-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;
            </td>
            <td width="17%">
				<label for="scrn-private-new">Private:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-private-new', 'id'=>'scrn-private-new', 'value'=>'1', 'checked'=>false)); ?>&nbsp;&nbsp;

				<br /><label for="scrn-free-new">Free:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-free-new', 'id'=>'scrn-free-new', 'value'=>'1', 'checked'=>false)); ?>
            </td>
            <td width="33%">
				<label for="scrn-program-name-new">Program Name:</label><br />
				<?php print form_input(array('name'=>'scrn-program-name-new', 'id'=>'scrn-program-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td width="33%" rowspan="7">
				<fieldset class="ui-corner-all">
			    <table class="addScreeningBreakdown" width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tbody style="border-top:none;">
			        <tr valign="top">
                    	<td width="65%"><label>Film Introduction</label></td>
                    	<td width="25%"><input name="intro-length" id="intro-length" type="text" size="1" value="5" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td width="10%"><input name="intro" id="intro" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Festival Trailer</label></td>
                    	<td><input name="fest-trailer-length" id="fest-trailer-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="fest-trailer" id="fest-trailer" type="checkbox" value="1" checked="checked" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer1-name" id="sponsor-trailer1-name" type="text" size="1" value="Sponsor Trailer 1" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer1-length" id="sponsor-trailer1-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer1" id="sponsor-trailer1" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer2-name" id="sponsor-trailer2-name" type="text" size="1" value="Sponsor Trailer 2" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer2-length" id="sponsor-trailer2-length" type="text" size="1" value="3" class="time-int ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer2" id="sponsor-trailer2" type="checkbox" value="1" class="time-int"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Film 1</label> <span class="req">*</span></td>
                    	<td colspan="2"><div id="scrn-film-01-length"><?php print "0" ?></span> min</td>
                    </tr>
			        <tr valign="top">
                    	<td colspan="3"><?php print form_dropdown_film('scrn-film-01', $film_screening_array, 0, "id='scrn-film-01' class='select-film ui-widget-content ui-corner-all'"); ?></td>
                    </tr>
                    <tr valign="top">
                    	<td colspan="3">                       
                        <div id="addNewFilmsHere"></div>
                        
                        <div align="center"><button id="addFilmScreeningDialog">Add Another Film</button></div>
                        </td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Q&amp;A Session</label></td>
                    	<td><input name="q-and-a-length" id="q-and-a-length" type="text" size="2" value="15" class="time-int ui-widget-content ui-corner-all"> min</td>
                        <td><input name="q-and-a" id="q-and-a" type="checkbox" value="1" class="time-int" /></td>
                    </tr>
                    </tbody>
                </table>
                <div id="scrn-runtime-1">Program Runtime:</div><div id="scrn-runtime-2"><?php print calculate_runtime(0); ?></div>
				</fieldset>				
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="scrn-date-new">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'scrn-date-new', 'id'=>'scrn-date-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="scrn-time-new">Time:</label> <span class="req">*</span> like: '7:00 PM'<br />
				<?php print form_input(array('name'=>'scrn-time-new', 'id'=>'scrn-time-new', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td rowspan="2">
                <label for="scrn-program-desc-new">Program Description:</label><br />
                <?php print form_textarea(array('name'=>'scrn-program-desc-new', 'id'=>'scrn-program-desc-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
            <td colspan="2">
				<label for="scrn-location-new">Screening Location:</label> <span class="req">*</span><br />
                <?php print form_dropdown('scrn-location-new', $location_array, 0,"id='scrn-location-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
		<tr valign="top">
            <td colspan="2">
				<label for="scrn-location2-new">Interlock? (2nd Location):</label><br />
                <?php print form_dropdown('scrn-location2-new', $location_array, 0,"id='scrn-location2-new' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td rowspan="4">
                <label for="scrn-notes-new">Screening Notes (private):</label><br />
                <?php print form_textarea(array('name'=>'scrn-notes-new', 'id'=>'scrn-notes-new', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-ticket-new">Ticket Link:</label> <a href="#" id="addTicketLink" style="float:right; padding-right:5px;">Add tix.com link</a><br />
				<?php print form_input(array('name'=>'scrn-ticket-new', 'id'=>'scrn-ticket-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="scrn-host-new">Host / Introducer:</label><br />
				<?php print form_input(array('name'=>'scrn-host-new', 'id'=>'scrn-host-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td>
				<label for="audience-count-new">Audience Count:</label><br />
				<?php print form_input(array('name'=>'audience-count-new', 'id'=>'audience-count-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td>
				<label for="screening-revenue-new">Screening Revenue:</label><br />
				$ <?php print form_input(array('name'=>'screening-revenue-new', 'id'=>'screening-revenue-new', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<div id="editScreening-dialog" title="Edit Screening">
<form name="editScreeningForm" id="editScreeningForm">
	<input type="hidden" name="screening_id_update" id="screening_id_update" value="0" />
	<input type="hidden" name="program_movie_ids_update" id="program_movie_ids_update" value="0" />
	<input type="hidden" name="total_films_update" id="total_films_update" value="0" />
	<input type="hidden" name="total-runtime-update" id="total-runtime-update" value="0" />
    <table class="ScreeningDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody style="border-top:none;">
        <tr valign="top">
            <td colspan="6">
				<h3>Screening Details</h3>
            </td>
            <td>
				<h3>Program / Notes</h3>
            </td>
            <td>
				<h3>Screening Breakdown</h3>
            </td>
        </tr>
        <tr valign="top">
        	<td width="17%" colspan="3">
				<label for="scrn-published-update">Published:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-published-update', 'id'=>'scrn-published-update', 'value'=>'1', 'checked'=>false)); ?>

				<br /><label for="scrn-private-update">Private:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-private-update', 'id'=>'scrn-private-update', 'value'=>'1', 'checked'=>false)); ?>
        	</td>
        	<td width="17%" colspan="3">
				<label for="scrn-rush-update">Standby:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-rush-update', 'id'=>'scrn-rush-update', 'value'=>'1', 'checked'=>false)); ?>

				<br /><label for="scrn-free-update">Free:</label>&nbsp;&nbsp;
				<?php print form_checkbox(array('name'=>'scrn-free-update', 'id'=>'scrn-free-update', 'value'=>'1', 'checked'=>false)); ?>
        	</td>
            <td width="33%">
				<label for="scrn-program-name-update">Program Name:</label><br />
				<?php print form_input(array('name'=>'scrn-program-name-update', 'id'=>'scrn-program-name-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
            <td width="33%" rowspan="7">
				<fieldset class="ui-corner-all">
			    <table class="addScreeningBreakdown" width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tbody style="border-top:none;">
			        <tr valign="top">
                    	<td width="65%"><label>Film Introduction</label></td>
                    	<td width="25%"><input name="intro-length-update" id="intro-length-update" type="text" size="1" value="5" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td width="10%"><input name="intro-update" id="intro-update" type="checkbox" value="1" checked="checked" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><label>Festival Trailer</label></td>
                    	<td><input name="fest-trailer-length-update" id="fest-trailer-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="fest-trailer-update" id="fest-trailer-update" type="checkbox" value="1" checked="checked" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer1-name-update" id="sponsor-trailer1-name-update" type="text" size="1" value="" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer1-length-update" id="sponsor-trailer1-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer1-update" id="sponsor-trailer1-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
			        <tr valign="top">
                    	<td><input name="sponsor-trailer2-name-update" id="sponsor-trailer2-name-update" type="text" size="1" value="" class="text-small ui-widget-content ui-corner-all"></td>
                    	<td><input name="sponsor-trailer2-length-update" id="sponsor-trailer2-length-update" type="text" size="1" value="3" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                    	<td><input name="sponsor-trailer2-update" id="sponsor-trailer2-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
                    <tr valign="top">
                    	<td colspan="3">
                        <div id="addNewFilmsHere-update"></div>
                        
                        <div align="center"><button id="addFilmScreeningDialog-update">Add Another Film</button></div>
                        </td>
                    </tr>
			        <tr valign="top">
                    	<td>
                        	<label>Q&amp;A Session</label><br />
                        	<span id="q-and-a-personnel"></span>
                        </td>
                    	<td><input name="q-and-a-length-update" id="q-and-a-length-update" type="text" size="2" value="15" class="time-int2 ui-widget-content ui-corner-all"> min</td>
                        <td><input name="q-and-a-update" id="q-and-a-update" type="checkbox" value="1" class="time-int2"></td>
                    </tr>
                    </tbody>
                </table>
                <div id="scrn-runtime-1-update">Program Runtime:</div><div id="scrn-runtime-2-update"><?php print calculate_runtime(0); ?></div>
				</fieldset>				
            </td>
        </tr>
		<tr valign="top">
            <td colspan="3">
				<label for="scrn-date-update">Date:</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'scrn-date-update', 'id'=>'scrn-date-update', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="3">
				<label for="scrn-time-update">Time:</label> <span class="req">*</span> like: '7:00 PM'<br />
				<?php print form_input(array('name'=>'scrn-time-update', 'id'=>'scrn-time-update', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
            </td>
            <td rowspan="2">
				<label for="scrn-program-desc-update">Program Description:</label><br />
				<?php print form_textarea(array('name'=>'scrn-program-desc-update', 'id'=>'scrn-program-desc-update', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-location-update">Screening Location:</label> <span class="req">*</span><br />
                <?php print form_dropdown('scrn-location-update', $location_array, 0,"id='scrn-location-update' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-location2-update">Interlock? (2nd Location):</label><br />
                <?php print form_dropdown('scrn-location2-update', $location_array, 0,"id='scrn-location2-update' class='select ui-widget-content ui-corner-all'"); ?>
            </td>
            <td rowspan="4">
                <label for="scrn-notes-update">Screening Notes:</label><br />
                <?php print form_textarea(array('name'=>'scrn-notes-update', 'id'=>'scrn-notes-update', 'value'=>'', 'rows'=>'5','cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-ticket-update">Ticket Link:</label> <a href="#" id="updateTicketLink" style="float:right; padding-right:5px;">Add tix.com link</a><br />
				<?php print form_input(array('name'=>'scrn-ticket-update', 'id'=>'scrn-ticket-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="6">
				<label for="scrn-host-update">Host / Introducer:</label><br />
				<?php print form_input(array('name'=>'scrn-host-update', 'id'=>'scrn-host-update', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
				<label for="audience-count-update">Audience #:</label><br />
				<?php print form_input(array('name'=>'audience-count-update', 'id'=>'audience-count-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="2">
				<label for="screening-revenue-update">Screening $:</label><br />
				$ <?php print form_input(array('name'=>'screening-revenue-update', 'id'=>'screening-revenue-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
            <td colspan="2">
				<label for="q-and-a-count-update">Q&amp;A Count:</label><br />
				<?php print form_input(array('name'=>'q-and-a-count-update', 'id'=>'q-and-a-count-update', 'value'=>'', 'class'=>'text-smaller ui-widget-content ui-corner-all')); ?>
            </td>
        </tr>
       	</tbody>
    </table>
</form>
</div>

<script type="text/javascript">
$(function() {
	$("button").button();
	$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

	$('#addTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-new')); return false; });
	$('#updateTicketLink').on('click', function() { add_tixdotcom_link($('#scrn-ticket-update')); return false; });

	$("#addScreeningForm").validate({
		rules: { "scrn-date-new": { required:true, daterange:['<?php print date("Y-m-d",strtotime($festival[0]->startdate)); ?>','<?php print date("Y-m-d",strtotime($festival[0]->enddate)); ?>'] }, "scrn-time-new": "required", "scrn-location-new": { required:true, min: 1 }, "scrn-film-01": "required" },
		messages: { "scrn-date-new":{ required: "Please enter a date." }, "scrn-time-new": "Please enter a time.", "scrn-location-new": "Please select a screening location.", "scrn-film-01": "Please select at least one film." }
	});

	$("#addScreening-dialog").dialog({
		autoOpen: false,
		height: 515,
		width: 855,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
			'Add Screening': function() {
				if ($("#addScreeningForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#addScreeningHere').replaceWith(msg);
							var new_screening_id = $('#screening-id-new').val();
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									screeningJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/film_schedule_list/return_screening_json/<?php print $festival[0]->startdate."/".$festival[0]->enddate; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#addScreeningForm').serialize(),
						url: '/admin/film_schedule_list/add_screening/',
						type: 'POST',
						dataType: 'html'
					});
					$(this).dialog('close');
	            }
			}
		},
		close: function() {
			$("#addScreeningForm INPUT[type='text']").each(function() { $(this).val(''); });
			$("#addScreeningForm INPUT[type='checkbox']").each(function() { $(this).prop('checked', false); });
			$("#addScreeningForm SELECT OPTION").each(function() { $(this).prop('selected', false); });
			$('#intro').prop('checked', true);
			$('#fest-trailer').prop('checked', true);
			$('#intro-length').val(5);
			$('#fest-trailer-length').val(3);
			$('#sponsor-trailer1-length').val(3);
			$('#sponsor-trailer2-length').val(3);
			$('#q-and-a-length').val(15);
			$('#total_films').val(1);
			$('#scrn-film-01').val(0);
			$('#scrn-film-01-length').html('0 min');
			$('#total-runtime').val( parseInt($('#intro-length').val()) + parseInt($('#fest-trailer-length').val()) );
			$('DIV.newFilmEntry').each(function() { $(this).remove(); });
			recalculate_total_runtime();
		}
	});

	$('#add-screening, #add-screening2').each(function() {
		$(this).on('click', function() {
			$('#addScreening-dialog').dialog('open');
			$(this).removeClass("ui-state-focus");
			return false;
		});
	})

	$('#addFilmScreeningDialog').on('click', function() { add_screening_film(); $(this).removeClass("ui-state-focus"); return false; });
	$('#addFilmScreeningDialog-update').on('click', function() { add_screening_film_update(0); $(this).removeClass("ui-state-focus"); return false; });

	$('.time-int').on('change', recalculate_total_runtime);
	$('.time-int2').on('change', recalculate_total_runtime_update);
	$('.select-film, .select-film2').each(function() {
		$(this).on('change', load_runtime);
	});

	var screeningJSON = <?php print $screening_json; ?>;

	$('#schedule_list').on('click','button.edit', function(e) {
		e.preventDefault();
		var screening_id = $(this).data('id').toString();
		open_edit_scrn_dialog(screening_id, screeningJSON);
		var timeoutID = window.setTimeout(function () { recalculate_total_runtime_update(); }, 2000);
		$(this).removeClass("ui-state-focus");
	});
	$('#schedule_list').on('click','a.icon_cross', function(e) {
		e.preventDefault();
		toggle_value_screening($(this).data('type'), $(this).data('id'));
	});
	$('#schedule_list').on('click','a.icon_tick', function(e) {
		e.preventDefault();
		toggle_value_screening($(this).data('type'), $(this).data('id'));
	});

	// validate edit screening form on keyup and submit
	$("#editScreeningForm").validate({
		rules: { "scrn-date-update": { required:true, daterange:['<?php print date("Y-m-d",strtotime($festival[0]->startdate)); ?>','<?php print date("Y-m-d",strtotime($festival[0]->enddate)); ?>'] }, "scrn-time-update": "required", "scrn-location-update": { required:true, min: 1 } },
		messages: { "scrn-date-update":{ required: "Please enter a date." }, "scrn-time-update": "Please enter a time.", "scrn-location-update": "Please select a screening location." }
	});

	$("#editScreening-dialog").dialog({
		autoOpen: false,
		height: 515,
		width: 855,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');
			},
<?php if ($this->session->userdata('limited') == 0) { ?>
			'DELETE': function() {
				var screening_id = $('#screening_id_update').val();
				var decision = confirm("Are you sure you really want to delete this Screening?");
				if (decision == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#scrn-'+screening_id).closest("tr").remove();
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editScreeningForm').serialize(),
						url: '/admin/film_schedule_list/delete_screening/',
						type: 'POST',
						dataType: 'html'
					}); 
				
					$(this).dialog('close');
				}
			},
<?php } ?>
			'Update Screening': function() {
				var screening_id = $('#screening_id_update').val();
				if ($("#editScreeningForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$('#scrn-'+screening_id).closest("tr").replaceWith(msg);
						},
						complete: function(xhr, text) {
							$.ajax({
								success: function(msg, text, xhr) {
									screeningJSON = msg;
								},
								error: function(xhr, msg1, msg2){
									alert( "Failure! " + xhr + msg1 + msg2); },
								url: '/admin/film_schedule_list/return_screening_json/<?php print $festival[0]->startdate."/".$festival[0]->enddate; ?>',
								type: 'POST',
								dataType: 'json'
							}); 							
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#editScreeningForm').serialize(),
						url: '/admin/film_schedule_list/update_screening/',
						type: 'POST',
						dataType: 'html'
					}); 

					$(this).dialog('close');
	            }
			}
		},
		close: function() {
			$("#editScreeningForm INPUT[type='text']").each(function() { $(this).val(''); });
			$("#editScreeningForm INPUT[type='checkbox']").each(function() { $(this).prop('checked',false); });
			$("#editScreeningForm SELECT OPTION").each(function() { $(this).prop('selected',false); });

			$('DIV.newFilmEntry').each(function() { $(this).remove(); });
			$('#total_films_update').val(0);
		}
	});

	$('#scrn-date-new').datepicker();
	$('#scrn-date-update').datepicker();
	$('.ui-dialog-buttonpane :button').each(function(){ if($(this).text() == 'DELETE') { $(this).addClass('ui-state-error'); } });

	$('#filter-location').on('change', function(e) {
		list_schedule_redirect("location", $("option:selected", this).val());
	});

<?php if ($this->session->userdata('limited') == 0) { ?>
	$('#publish-all').button().on('click', function() {
		var decision = confirm("Publish all PUBLIC screenings for this festival?");
		if (decision == true) {
			$.ajax({
				success: function(msg) {
					window.location="/admin/film_schedule_list/"; },
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#publishAllForm').serialize(),
				url: '/admin/film_schedule_list/publish_all/',
				type: 'POST',
				dataType: 'html'
			}); 
		}
		$(this).removeClass("ui-state-focus");
		return false;
	});
	$('#unpublish-all').button().on('click', function() {
		var decision = confirm("Unpublish all screenings for this festival?");
		if (decision == true) {
			$.ajax({
				success: function(msg) {
					window.location="/admin/film_schedule_list/"; },
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#unpublishAllForm').serialize(),
				url: '/admin/film_schedule_list/unpublish_all/',
				type: 'POST',
				dataType: 'html'
			}); 
		}
		$(this).removeClass("ui-state-focus");
		return false;
	});
<?php } ?>
	jQuery("#schedule_list").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
});
</script>
