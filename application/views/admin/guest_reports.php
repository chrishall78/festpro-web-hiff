<?php
	$guest_films_array = convert_films_to_array($guest_films);
?>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Guests</a></li>
		<li><a href="#tabs-2">Flights</a></li>
		<li><a href="#tabs-3">Hotels</a></li>
		<!--
		<li><a href="#tabs-4">Transportation</a></li>
		-->
	</ul>

	<div id="tabs-1">
		<fieldset class="GuestReportsTab ui-corner-all">

			<h3>Preview and Export Guest Information</h3>
			<p align="center">Please choose a report to run. A preview will appear below, with an option to export to a CSV file.</p>
			<table width="100%" cellpadding="5" cellspacing="0">
				<tbody style="border:none;">
					<tr valign="top" align="center">
						<td><button id="guestAllInfo">Guest Info, Flights, Hotels</button></td>
						<td><button id="guestInfo">Guest Info</button></td>
						<td>
							<button id="guestByProgram">Guests by Program</button><br />
							<select id="programTitle" class="select ui-widget-content ui-corner-all">
								<option value="0">--</option>								
<?php
	foreach ($guest_programs as $thisProgram) {
		$programDate = date("m/d/Y g:ia",strtotime($thisProgram->date." ".$thisProgram->time));
		$programIds = str_replace(",", "-", $thisProgram->program_movie_ids);
		print "\t\t\t\t\t\t\t\t<option value=\"".$programIds."\">".$thisProgram->program_name." (".$programDate.")</option>\n";
	}
?>
							</select>
						</td>
						<td>
							<button id="guestByFilm">Guests by Film</button><br />
							<select id="filmTitle" class="select ui-widget-content ui-corner-all">
<?php
	foreach ($guest_films_array as $key => $value) {
		print "\t\t\t\t\t\t\t\t<option value=\"".$key."\">".$value."</option>\n";
	}
?>
							</select>
						</td>
					</tr>
				</tbody>
			</table><br />

			<div id="guestExp"></div>
		</fieldset>
	</div>

	<div id="tabs-2">
		<fieldset class="GuestReportsTab ui-corner-all">
		<h3>Airfare Summary</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				  <tr valign="top">
					<th width="10%">&nbsp;</th>
					<th width="30%">Airline Miles</th>
					<th width="30%">Cash</th>
					<th width="30%">Flights</th>
				  </tr>
				</thead>
				<tbody style="border-top:none;">
<?php
	$cash_provided = $cash_used = $cash_remaining = 0;
	$miles_provided = $miles_used = $miles_remaining = 0;
	$flights_provided = $flights_used = $flights_remaining = 0;

	foreach ($air_deals as $thisDeal) {
		$cash_provided += $thisDeal->cashlimit;
		$miles_provided += $thisDeal->mileagelimit;
		$flights_provided += $thisDeal->flightlimit;
	}

	foreach ($air_summary as $thisSummary) {
		$type = "";
		foreach ($air_deals as $thisDeal) {
			if ($thisSummary->deal_id == $thisDeal->id) {
				if ($thisDeal->flightlimit > 0) { $type = "flights"; }
				elseif ($thisDeal->mileagelimit > 0) { $type = "miles"; }
				elseif ($thisDeal->cashlimit > 0) { $type = "cash"; }
			}
		}
		
		if ($type == "flights") {
			foreach ($air_used as $thisUsed) {
				if ($thisSummary->id == $thisUsed->flightcomp_id) { $flights_used++; }
			}
		} elseif ($type == "miles") {
			foreach ($air_used as $thisUsed) {
				if ($thisSummary->id == $thisUsed->flightcomp_id) { $miles_used += $thisUsed->mileage; }
			}

		} elseif ($type == "cash") {
			foreach ($air_used as $thisUsed) {
				if ($thisSummary->id == $thisUsed->flightcomp_id) { $cash_used += $thisUsed->cost; }
			}
		}		
	}

	$cash_remaining = $cash_provided - $cash_used;
	$miles_remaining = $miles_provided - $miles_used;
	$flights_remaining = $flights_provided - $flights_used;
	
	setlocale(LC_MONETARY, 'en_US');

	print "\t\t\t\t<tr>\n";
	print "\t\t\t\t\t<td><b>Provided:</b></td>\n";
	print "\t\t\t\t\t<td>".number_format($miles_provided)."</td>\n";
	//print "\t\t\t\t\t<td>".$cash_provided."</td>\n";
	print "\t\t\t\t\t<td>".money_format('%.0n', $cash_provided)."</td>\n";
	print "\t\t\t\t\t<td>".number_format($flights_provided)."</td>\n";
	print "\t\t\t\t</tr>\n";

	print "\t\t\t\t<tr>\n";
	print "\t\t\t\t\t<td><b>Used:</b></td>\n";
	print "\t\t\t\t\t<td>".number_format($miles_used)."</td>\n";
	//print "\t\t\t\t\t<td>".$cash_used."</td>\n";
	print "\t\t\t\t\t<td>".money_format('%.0n', $cash_used)."</td>\n";
	print "\t\t\t\t\t<td>".number_format($flights_used)."</td>\n";
	print "\t\t\t\t</tr>\n";

	print "\t\t\t\t<tr>\n";
	print "\t\t\t\t\t<td><b>Remaining:</b></td>\n";
	print "\t\t\t\t\t<td>".number_format($miles_remaining)."</td>\n";
	//print "\t\t\t\t\t<td>".$cash_remaining."</td>\n";
	print "\t\t\t\t\t<td>".money_format('%.0n', $cash_remaining)."</td>\n";
	print "\t\t\t\t\t<td>".number_format($flights_remaining)."</td>\n";
	print "\t\t\t\t</tr>\n";
?>
				</tbody>
		   </table>
		   <!--<p style="text-align:center; margin-top:0;">The 'Provided' values above can be changed by going to the <a href="/admin/guest_settings/">Guest Settings</a> page.</p> -->

		<!-- <h3>Airfare Comps by Type</h3> -->

		<p>&nbsp;</p>

		<div id="flightDatesExp"></div>

		</fieldset>
	</div>

	<div id="tabs-3">
		<fieldset class="GuestReportsTab ui-corner-all">
		<h3>Hotel Summary</h3>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
				  <tr valign="top">
					<th width="10%">&nbsp;</th>
<?php
	$h_provided = $h_used = $h_remaining = 0;
	$provided_array = $used_array = $remaining_array = array();

	foreach ($hotel_summary as $thisSummary) {
		$provided_array[$thisSummary->id] = $thisSummary->limit;
		$h_provided = $h_provided + $thisSummary->limit;
		$used_array[$thisSummary->id] = 0;
		foreach ($hotel_used as $thisUsed) {
			if ($thisUsed->hotelcomp_id == $thisSummary->id) {
				$used_array[$thisSummary->id] += $thisUsed->compnights;
				$h_used += $thisUsed->compnights;
			}
		}
		$remaining_array[$thisSummary->id] = $thisSummary->limit - $used_array[$thisSummary->id];

		print "\t\t\t\t\t<th width=\"10%\">".$thisSummary->name."</th>\n";
	}

	$h_remaining = $h_provided - $h_used;
?>
					<th>All Hotels</th>
				  </tr>
				</thead>
				<tbody style="border-top:none;">
<?php
		
	print "\t\t\t\t\t<tr valign=\"top\">\n";
	print "\t\t\t\t\t\t<td><b>Provided:</b></td>";
	foreach ($provided_array as $provided_rooms) { print "\t\t\t\t\t\t<td>".$provided_rooms."</td>\n"; }
	print "\t\t\t\t\t\t<td>".$h_provided."</td>\n";
	print "\t\t\t\t\t</tr>\n";

	print "\t\t\t\t\t<tr valign=\"top\">\n";
	print "\t\t\t\t\t\t<td><b>Used:</b></td>";
	foreach ($used_array as $used_rooms) { print "\t\t\t\t\t\t<td>".$used_rooms."</td>\n"; }
	print "\t\t\t\t\t\t<td>".$h_used."</td>\n";
	print "\t\t\t\t\t</tr>\n";

	print "\t\t\t\t\t<tr valign=\"top\">\n";
	print "\t\t\t\t\t\t<td><b>Remaining:</b></td>";
	foreach ($remaining_array as $remaining_rooms) { print "\t\t\t\t\t\t<td>".$remaining_rooms."</td>\n"; }
	print "\t\t\t\t\t\t<td>".$h_remaining."</td>\n";
	print "\t\t\t\t\t</tr>\n";
?>        
				</tbody>
		   </table>
		   <!-- <p style="text-align:center; margin-top:0;">The 'Provided' values above can be changed by going to the <a href="/admin/guest_settings/">Guest Settings</a> page.</p> -->

		<p>&nbsp;</p>

		<div id="hotelDatesExp"></div>

		</fieldset>
	</div>
	
	<!--

	<div id="tabs-4">
		<fieldset class="GuestReportsTab ui-corner-all">
		</fieldset>
	</div>
	
	-->
	
</div>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs();
		$("input:submit").button();
		$("button").button();				
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		$("#guestAllInfo").on('click', function() {
			$("#guestExp").load("/admin/guest_reports/preview_guest_allinfo_export/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#guestInfo").on('click', function() {
			$("#guestExp").load("/admin/guest_reports/preview_guest_export/preview/"); $(this).removeClass("ui-state-focus");
		});

		$("#guestByProgram").on('click', function() {
			var screeningId = $('#programTitle').val();
			$("#guestExp").load("/admin/guest_reports/preview_guest_program_export/preview/"+screeningId+"/"); $(this).removeClass("ui-state-focus");
		});

		$("#guestByFilm").on('click', function() {
			var filmId = $('#filmTitle').val();
			$("#guestExp").load("/admin/guest_reports/preview_guest_film_export/preview/"+filmId+"/"); $(this).removeClass("ui-state-focus");
		});

		$("#flightDatesExp").load("/admin/guest_reports/preview_flights_export/preview/");
		$("#hotelDatesExp").load("/admin/guest_reports/preview_hotels_export/preview/");
	});
</script>
