<!doctype html>
<html lang="en">
  <head>
    <title><?php print $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php // Load these stylesheets on all pages ?>
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/admin-style.css" />
	<link type="text/css" rel="stylesheet" media="print" href="<?php echo $path;?>assets/css/admin/admin-print.css" />
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/jquery-ui-theme/jquery-ui-1.12.1.custom.min.css" />
<?php if ($selected_page == "film_settings") { // Load these stylesheets only on the "Film Settings" page ?>
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/colorpicker/colorpicker.css" />
<?php } if ($selected_page == "film_edit") { // Load these stylesheets only on the "Edit a Film" page ?>
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/imgAreaSelect/imgareaselect-default.css" />
<?php } ?>

<?php // Load these scripts on all pages ?>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery-ui-1.12.1.custom.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.validate.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.validate.daterange.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.floatheader.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.form.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/jquery.isotope.min.js"></script>
<?php if ($selected_page == "film_settings") { // Load these scripts only on the "Film Settings" page ?>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/colorpicker/colorpicker.js"></script>
<?php } if ($selected_page == "film_edit" || $selected_page == "film_updates") { // Load these scripts on the "Edit a Film" & "Film Updates" pages ?>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.frameready.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $path;?>assets/scripts/admin/ckeditor/config.js"></script>
<?php }
	// Load misc functions based on which part of the app we're currently in
	if (substr($selected_page, 0, 4) == "film") { print "\t<script type=\"text/javascript\" language=\"javascript\" src=\"".$path."assets/scripts/admin/functions_film.min.js\"></script>\n"; }
	if (substr($selected_page, 0, 5) == "guest") { print "\t<script type=\"text/javascript\" language=\"javascript\" src=\"".$path."assets/scripts/admin/functions_guest.min.js\"></script>\n"; }
	if (substr($selected_page, 0, 4) == "user") { print "\t<script type=\"text/javascript\" language=\"javascript\" src=\"".$path."assets/scripts/admin/functions_user.min.js\"></script>\n"; }
?>	
  </head>

  <body class="layout-first-main">
    <div id="page" class="clearfix">
      <div id="main-wrapper">
        <div id="main" class="clearfix">
        	<div id="ajaxFeedback"></div>