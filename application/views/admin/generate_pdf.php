<?php
	$premiere_array = convert_to_array($premiere);
	$sections_array = convert_to_array($sections);
	$countries_array = convert_to_array($countries);
	$languages_array = convert_to_array($languages);
	$genres_array = convert_to_array($genres);
	$sponsors_array = convert_to_array($sponsors);

	$countries_string = "";
	$languages_string = "";
	$genres_string = "";
	$director_string = "";
	$cast_string = "";

	if ($screening_type == "feature" || $screening_type == "feature_shorts" ) {
		foreach ($film_countries as $thisCountry) {
			$countries_string .= trim($thisCountry->name).", ";
		} $countries_string = trim($countries_string,", ");

		foreach ($film_languages as $thisLanguage) {
			$languages_string .= trim($thisLanguage->name).", ";
		} $languages_string = trim($languages_string,", ");

		foreach ($film_genres as $thisGenre) {
			$genres_string .= trim($thisGenre->name).", ";
		} $genres_string = trim($genres_string,", ");

		foreach ($film_personnel as $thisPerson) {
			$roles = explode(",",$thisPerson->personnel_roles);
			foreach ($roles as $thisRole) {
				if ($thisRole == 1) {
					$director_string .= trim($thisPerson->name)." ".trim($thisPerson->lastname).", ";
				} else if ($thisRole == 6) {
					$cast_string .= trim($thisPerson->name)." ".trim($thisPerson->lastname).", ";
				}
			}
		}
		$director_string = trim($director_string,", ");
		$cast_string = trim($cast_string,", ");
	}

	// Determine what language this film's text is in for PDF font purposes
	$specified_language = "";
	$japanese = $korean = $chinese = 0;
	if ($language != "english") {
		foreach ($film_languages as $thisLanguage) {
			switch ($thisLanguage->name) {
				case 'Chinese':
				case 'Min Nan Chinese':
				case 'Mandarin Chinese':
				case 'Mandarin':
				case 'Cantonese': $chinese++; break;
				case 'Japanese': $japanese++; break;
				case 'Korean': $korean++; break;
			}
		}

		// Simple cases where only one asian language is present
		if ($chinese > $korean && $chinese > $japanese) { $specified_language = "ch"; }
		if ($japanese > $korean && $japanese > $chinese) { $specified_language = "jp"; }
		if ($korean > $japanese && $korean > $chinese) { $specified_language = "kr"; }

		// Multiple asian languages - add country value to try to narrow it down furthers
		if ($specified_language == "") {
			if ($chinese == $japanese || $chinese == $korean || $japanese == $korean) {
				foreach ($film_countries as $thisCountry) {
					switch ($thisCountry->name) {
						case 'China':
						case 'Hong Kong SAR China':
						case 'Taiwan':
						case 'Hong Kong': $chinese++; break;
						case 'Japan': $japanese++; break;
						case 'Korea':
						case 'North Korea':
						case 'South Korea': $korean++; break;
					}
				}
				if ($chinese > $korean && $chinese > $japanese) { $specified_language = "ch"; }
				if ($japanese > $korean && $japanese > $chinese) { $specified_language = "jp"; }
				if ($korean > $japanese && $korean > $chinese) { $specified_language = "kr"; }
			}
		}
	}

?>
<!doctype html>
<html>
	<head>
		<title><?php print $film[0]->title_en ?></title>
		<meta content="text/html; charset=UTF-8" http-equiv="content-type" />

		<style type="text/css">
			/* --- Reset Margins and Padding --- */
			html, body, div, span, p, blockquote, pre, code, ul, ol, li, h1, h2, h3, h4, h5, h6,
			form, fieldset, legend, label, input, textarea, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; }
			img { border: 0; }

			/* --- Typography --- */
			body {
			  font-family: Helvetica, Arial, sans-serif;
			  font-size: 15px;
			  font-weight: normal;
			  line-height: 125%;
			}
			h1 { font-size: 2.5em; font-weight: normal; margin: 8px 0; }
			h2 { font-size: 1.75em; font-weight: normal; margin: 8px 0; }
			h3 { font-size: 1.25em; font-weight: normal; margin: 8px 0; }
			h4 { font-size: 1.1em; font-weight: normal; margin: 8px 0; }
			h5 { font-size: 1em; font-weight: bold; margin: 8px 0; }
			h6 { font-size: 0.8em; font-weight: bold; margin: 8px 0; }

			blockquote { margin: 0 0 0 20px; }
			p { margin: 10px 0; }

			.section { text-align: center;  }
			.title_english { text-align: center; font-weight: bold; text-transform: uppercase; }
			.sponsored_by { text-align: center; font-style: italic; }
			.shorts_title { margin-bottom: 0; }
			.shorts_film_details { font-weight: bold; margin-top: 0; margin-bottom: 15px; }
			.film_details { font-weight: bold; }
			.shorts_title, .synopsis, .shorts_film_details, .film_details, .film_director, .film_cast, .film_shorts { margin-left: 5px; }
			.right_column { margin-right: 5px; }
			.title_original { text-align: center; text-transform: uppercase; }
			.synopsis_original { margin-left: 5px; }

			.ch { font-family: 'SimHei'; word-wrap: break-word; }
			.jp { font-family: 'MS Gothic'; word-wrap: break-word; }
			.kr { font-family: 'Gulim'; word-wrap: break-word; }
		</style>
	</head>
	<body>
		<img src="<?php print $_SERVER['DOCUMENT_ROOT']; ?>/assets/images/2016_spring_showcase/002-PDF-Header.jpg" width="790" height="169" style="margin-left:10px; margin-top:10px;" />
		<table width="612" cellpadding="0" cellspacing="0" border="0">
			<tbody style="border-top:none;">
			<tr valign="top">
				<td>&nbsp;</td>
			</tr>
			<tr valign="top">
				<td width="6">&nbsp;</td>
				<td width="368">
				<?php
				// Single Feature or Feature + Shorts
				if ($screening_type == "feature" || $screening_type == "feature_shorts") {
					$photo_count = count($film_photos);
					if ($photo_count > 0 && intval($selected_photo) < $photo_count) {
						/*
						if ($film_photos[$selected_photo]->url_cropxlarge != "") {
							print "<img src=\"".$_SERVER['DOCUMENT_ROOT'].$film_photos[$selected_photo]->url_cropxlarge."\" width=\"475\" height=\"267\" />";
						} else {
							print "<img src=\"".$_SERVER['DOCUMENT_ROOT'].$film_photos[$selected_photo]->url_croplarge."\" width=\"475\" height=\"267" />";
						}
						*/
						if ($file_path != "") {
							print "<img src=\"".$file_path."\" width=\"475\" height=\"267\" />";
						}
					} else {
						$default_photo = $_SERVER['DOCUMENT_ROOT']."/".$upload_dir."/001-default-photo.jpg";
						print "<img src=\"".$default_photo."\" width=\"475\" height=\"267\" />";
					}

					// Change layout if we are printing an english or original language flyer
					if ($language == "english") {
						print "<h2 class=\"title_english\">".switch_title(convert_specialchars($film[0]->title_en))."</h2>";
						if ($film[0]->title != "") {
							//print "<h4 class=\"title_original\">".convert_specialchars($film[0]->title)."</h4>";
						}
					} else {
						print "<h2 class=\"title_original $specified_language\">".$film[0]->title."</h2>";
					}

					if (count($sponsor_logos) > 0) {
						print "<p class=\"sponsored_by\">Sponsored by: ";
						$x = 0;
						foreach ($sponsor_logos as $thisSponsor) {
							if ($x > 0) { print ", "; }
							print $thisSponsor->name;
							$x++;
						}
						print "</p>";
					}

					if ($language == "english") {
						print "<div class=\"synopsis\">\n";
						print convert_specialchars($film[0]->synopsis_short);
						print "</div>\n";

						print "<p class=\"film_details\">";
						print $countries_string." ".$film[0]->year." | ".$languages_string." | ".$film[0]->runtime_int."M | ".$sections_array[$film[0]->category_id]." | ".$genres_string;
						print "</p>\n";

						if ($director_string != "") {
							print "<p class=\"film_director\"><strong>DIRECTOR:</strong> ";
							print $director_string;
							print "</p>\n";
						}
						if ($cast_string != "") {
							print "<p class=\"film_cast\"><strong>CAST:</strong> ";
							print $cast_string;
							print "</p>\n";
						}

						if (count($film_array) > 0) {
							print "<p class=\"film_shorts\">";
							print "<strong>PRECEDED BY:</strong>";
							foreach ($film_array as $thisFilmArray) {
								foreach ($thisFilmArray as $thisFilm) {
									print "<br/>".switch_title(convert_specialchars($thisFilm->title_en))." | ".$thisFilm->country_name." ".$thisFilm->year." | ".$thisFilm->language_name." | ".$thisFilm->runtime_int."M | Director: ".$thisFilm->director_name;
								}
							}
							print "</p>";
						}
					} else {
						print "<div class=\"synopsis_original $specified_language\">\n";
						print convert_specialchars($film[0]->synopsis_original);
						print "</div>\n";
					}

				// Shorts Program
				} else {
					$photo_count = count($film_photos);
					if ($photo_count > 0 && intval($selected_photo) < $photo_count) {
						if ($file_path != "") {
							print "<img src=\"".$file_path."\" width=\"475\" height=\"267\" />";
						}
					} else {
						$default_photo = $_SERVER['DOCUMENT_ROOT']."/".$upload_dir."/001-default-photo.jpg";
						print "<img src=\"".$default_photo."\" width=\"475\" height=\"267\" />";
					}

					print "<h2 class=\"title_english\">".convert_specialchars($film_screenings[0]->program_name)."</h2>";
					if (count($sponsor_logos) > 0) {
						print "<p class=\"sponsored_by\">Presented by ";
						$x = 0;
						foreach ($sponsor_logos as $thisSponsor) {
							if ($x > 0) { print ", "; }
							print $thisSponsor->name;
							$x++;
						}
						print "</p>\n";
					}

					foreach ($film_array as $thisFilmArray) {
						foreach ($thisFilmArray as $thisFilm) {
							print "<h4 class=\"shorts_title\">".switch_title(convert_specialchars($thisFilm->title_en))."</h4>";
							print "<p class=\"shorts_film_details\">".$thisFilm->country_name." ".$thisFilm->year." | ".$thisFilm->language_name." | ".$thisFilm->runtime_int."M | Director: ".$thisFilm->director_name."</p>";
							//print "<div class=\"synopsis\">".$thisFilm->synopsis_short."</div>";
						}
					}
				}
				?>
				</td>
				<td width="12">&nbsp;</td>
				<td width="220" class="right_column">
					<p class="showtimes" style="margin-top:0;">
						<?php
							if ($language == "english") {
								if (count($film_screenings) > 0) {
									print "<h3 style=\"margin:0; font-weight:bold;\">SCREENINGS:</h3>\n";
								}
								$x = 0;
								foreach ($film_screenings as $thisScreening) {
									if ($x > 0) { print "<br />"; }
									print date("D, M j",strtotime($thisScreening->date))." at ".date("g:iA",strtotime($thisScreening->time))." - ".$thisScreening->displayname;
									$x++;
								}
								print "<br />";
							} else {
								if (count($film_screenings) > 0) {
									print "<h3 style=\"margin:0; font-weight:bold;\">SCREENINGS:</h3>\n";
								}
								$x = 0;
								foreach ($film_screenings as $thisScreening) {
									$dstamp = strtotime($thisScreening->date);
									$tstamp = strtotime($thisScreening->time);
									$y = date("Y", $dstamp);
									$m1 = date("n", $dstamp);
									$m2 = date("m", $dstamp);
									$d1 = date("j", $dstamp);
									$d2 = date("d", $dstamp);
									$h = date("H", $tstamp);
									$m = date("i", $tstamp);

									if ($x > 0) { print "<br />"; }
									print "<div class=\"$specified_language\">";
									if ($specified_language == "ch") {
										print $y."年".$m1."月".$d1."日 ".$h.":".$m." - ".$thisScreening->displayname;
									} elseif ($specified_language == "jp") {
										print $y."年".$m2."月".$d2."日 ".$h.":".$m." - ".$thisScreening->displayname;
									} elseif ($specified_language == "kr") {
										print $y."년 ".$m2."월 ".$d2."일 ".$h."시 ".$m."분 - ".$thisScreening->displayname;
									}
									print "</div>";
									$x++;
								}
								print "<br />";
							}
						?>
					</p>
<?php
	//if ($language == "english") {
		if ($festival[0]->pdf_flyer_html != "") { print $festival[0]->pdf_flyer_html; }
	//}
?>				</td>
				<td width="6">&nbsp;</td>
			</tr>
			</tbody>
		</table>
	</body>
</html>