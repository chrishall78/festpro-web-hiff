<?php
if (count($filmids) == 0) {
	print "<div style=\"margin-top:2px;\">&nbsp;</div>\n";
	print "<fieldset class=\"ui-corner-all\">\n";
	print "<p align=\"center\">The full program &amp; schedule for the ".$festival[0]->year." ".$festival[0]->name." has not been released yet.</p>";
	print "</fieldset>\n";
} else {
	$wsiw_sections   = convert_to_array_all($sections);
	$wsiw_countries  = convert_to_array_all($countries);
	$wsiw_languages  = convert_to_array_all($languages);
	$wsiw_genres     = convert_to_array_all($genres);
	$wsiw_eventtypes = convert_to_array_all($eventtypes);
?>

<div style="margin-top:2px;">&nbsp;</div>
<fieldset class="ui-corner-all">
<?php print "<h1 class=\"title\">".$title."</h1>"; ?>
<p>Select your preferred film language, country, genre, type or section and we'll highlight any films that match.</p>

<table id="wsiw_header" cellpadding="5" cellspacing="0" border="0">
	<thead>
    	<th>Section</th>
    	<th>Country</th>
    	<th>Language</th>
    	<th>Genre</th>
    	<th>Film/Event Type</th>
    </thead>
	<tbody style="border-top:none;">
    	<tr valign="top">
<?php print "<td>".form_dropdown('wsiw_section', $wsiw_sections, "0", "id='wsiw_section' class='select ui-widget-content ui-corner-all'")."</td>"; ?>
<?php print "<td>".form_dropdown('wsiw_country', $wsiw_countries, "0", "id='wsiw_country' class='select ui-widget-content ui-corner-all'")."</td>"; ?>
<?php print "<td>".form_dropdown('wsiw_language', $wsiw_languages, "0", "id='wsiw_language' class='select ui-widget-content ui-corner-all'")."</td>"; ?>
<?php print "<td>".form_dropdown('wsiw_genre', $wsiw_genres, "0", "id='wsiw_genre' class='select ui-widget-content ui-corner-all'")."</td>"; ?>
<?php print "<td>".form_dropdown('wsiw_eventtypes', $wsiw_eventtypes, "0", "id='wsiw_eventtype' class='select ui-widget-content ui-corner-all'")."</td>"; ?>
			<td><button id="wsiw_reset">Reset</button></td>
		</tr>
    </tbody>
</table>

<div id="wsiw">
<?php
$default_image = "/".$upload_dir."/001-default-photo.jpg";

foreach ($film_grid as $thisFilm) {
	$title = switch_title($thisFilm->title_en);

	$no_image = "yes";
	$trimmed = 0;

	foreach ($all_photos as $thisPhoto) {
		if ($thisPhoto->movie_id == $thisFilm->movie_id) {
			$no_image = $thisPhoto->url_cropsmall; break;
		}
	}
	$country_list = $thisFilm->country_name;
	$genre_list = $thisFilm->genre_name;

	print "<div class=\"film ";
	print $thisFilm->section_slug." ";
	print $thisFilm->country_slug;
	print $thisFilm->language_slug;
	print $thisFilm->genre_slug;
	print $thisFilm->event_slug;
	print "\">";

	print "<a href=\"/films/detail/".$thisFilm->slug."\"><img src=\"";
	if ($no_image == "yes") { print $default_image; } else { print $no_image; }
	print "\" width=\"210\" border=\"0\"></a>\n";

 	print "<p class=\"film_list_title\"><a href=\"/films/detail/".$thisFilm->slug."\">".$title."</a>";
	print "</div>";
}
?>
</div>
</fieldset>

<script type="text/javascript" language="javascript">
	$(function() {
		$("button").button();

		$('#wsiw').isotope({ itemSelector: '.film', 'layoutMode': 'fitRows' });
		$('#wsiw').imagesLoaded( function() {
			// images have loaded
			$('#wsiw').isotope({ itemSelector: '.film', 'layoutMode': 'fitRows' });
		});
		$('#wsiw_reset').on('click', function() { wsiw_reset(); });
		$('#wsiw_section, #wsiw_country, #wsiw_language, #wsiw_genre, #wsiw_eventtype').on('change', function() { wsiw_filter(); });
	});
</script>
<?php } ?>