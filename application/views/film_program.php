<?php
// Current path of images
$default_image = "/".$upload_dir."/001-default-photo.jpg";

$preview = false;
if (isset($logged_in)) {
	if ($logged_in == true) { $preview = true; }
}

// check to see screening is published before displaying | preview if logged in
if ($program[0]->Published == 0 && $preview == false) {
	header('Location: /films/');
} else {
	$premiere_array = convert_to_array2($premiere_type);
	$premiere_name = "";

	$currentdate = time();
	if ($program[0]->program_name != "") {
		$newtitle = $program[0]->program_name;
	} else {
		$newtitle = "Shorts Program";
	}
	
	print "<div style=\"margin-top:2px;\">&nbsp;</div>";
	print "<fieldset class=\"ui-corner-all";
	if ($program[0]->Published == 0) { print " unpublished"; }
	print "\">";
	print "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"film_detail\"><tbody>";
	print "\t\t<tr valign=\"top\"><td colspan=\"4\">\n";
	print "\t\t\t<h1 class=\"title\">".$newtitle."</h1>\n";
	if ($program[0]->program_desc != "") {
		print "<p>".$program[0]->program_desc."</p>";
	}
	print "\t\t\t<span>Various Directors | ".$program_runtime." mins.</span>\n";
	print "\t\t</td></tr>\n";

	// Screenings
	print "\t\t<tr valign=\"top\"><td colspan=\"4\"><a name=\"screenings\"></a><h3>Screenings</h3></td></tr>\n";
	foreach ($program as $thisScreening) {
		$tickets_img = find_screening_status($thisScreening->date,$thisScreening->url,$thisScreening->Rush,$thisScreening->Free);
		$datetime = $thisScreening->date." ".$thisScreening->time;
		$date = date("l, F d", strtotime($datetime));
		$time = date("g:i A", strtotime($datetime));
		$location = $thisScreening->displayname;

		print "\t<tr valign=\"top\">\n";
		print "\t\t<td width=\"25%\">".$date."</td>\n";
		print "\t\t<td width=\"25%\">".$time."</td>\n";
		print "\t\t<td width=\"25%\">".$location."</td>\n";
		print "\t\t<td width=\"25%\" align=\"center\">".$tickets_img."</td>\n";  
		print "\t</tr>\n";
	}
	print "\t\t<tr valign=\"top\"><td colspan=\"4\">&nbsp;</td></tr>\n";

	// Films
	print "\t\t<tr valign=\"top\"><td colspan=\"4\"><h3>Films</h3><div id=\"program_wrapper\">\n";
	foreach ($films as $thisFilm) {
		$photoFound = false;
		$director_name = trim($director[$thisFilm->movie_id],", ");
		print "<div class=\"program_film\">";
		foreach ($all_photos as $thisPhoto) {
			if ($thisPhoto->movie_id == $thisFilm->movie_id) {
				if ($thisPhoto->url_cropxlarge != "") {
					$photo_url = $thisPhoto->url_cropxlarge;
				} else {
					$photo_url = $thisPhoto->url_croplarge;
				}
				print "<a href=\"/films/detail/".$thisFilm->slug."\"><img width=\"330\" height=\"189\" border=\"0\" src=\"".$photo_url."\" alt=\"".switch_title($thisFilm->title_en)."\" /></a>\n";
				$photoFound = true;
			}
		}
		if ($photoFound == false) { print "<img width=\"330\" height=\"189\" border=\"0\" src=\"".$default_image."\" alt=\"".switch_title($thisFilm->title_en)."\" />\n"; }
		print "<h4><a href=\"/films/detail/".$thisFilm->slug."\">".switch_title($thisFilm->title_en)."</a></h4>";
		print "<p><strong>Directed by:</strong> ".$director_name."<br/>".$thisFilm->year." | ".$thisFilm->runtime_int." min.";
		if ($thisFilm->premiere_id != 0) { $premiere_name = $premiere_array[$thisFilm->premiere_id]; }
		if ($premiere_name != "") { print " | Premiere: <span>".$premiere_name."</span>"; }
		foreach ($all_videos as $movieId => $thisVideoUrl) {
			if ($movieId == $thisFilm->movie_id && $thisVideoUrl != "") {
				print "<br /><a href=\"".$thisVideoUrl."\" target=\"_blank\">View Film Trailer</a>";
			}
		}
		print "</p>";
		if ($thisFilm->synopsis_short != "") {
			$syn_short = str_replace("&#13;&#10;", "<br>", stripslashes($thisFilm->synopsis_short));
			print "<p>".$syn_short."</p>";
		}
		if ($thisFilm->synopsis_original != "") {
			$syn_original = str_replace("&#13;&#10;", "<br>", stripslashes($thisFilm->synopsis_original));
			print "<p>".$syn_original."</p>";
		}
		print "</div>\n";
	}
	print "\t\t</div></td></tr>\n";

	print "</tbody></table>";	
	?>
	
	<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#program_wrapper').isotope({ itemSelector: '.program_film', layoutMode: 'fitRows' });
		$('#program_wrapper').imagesLoaded( function() {
			// images have loaded
			$('#program_wrapper').isotope({ itemSelector: '.program_film', layoutMode: 'fitRows' });
		});
	});
	</script>
<?php
}
?>