<?php
if (count($competition) == 0 || count($competition_films) == 0) {
	print "<p>Sorry, that competition doesn't exist or has no films assigned.</p>";
} else {
	print "<h1>".$competition[0]->festival_year." ".$competition[0]->festival_name." - ".$competition[0]->name."</h1>\n";

	print "<div id=\"competition_left\">\n";
	$x = 1;
	foreach ($competition_films as $thisFilm) {
		print "<h3>".$x.". ".switch_title($thisFilm->title_en)."</h3>\n";
		print "<p>".$thisFilm->year." | ".$thisFilm->runtime_int." mins.</p>\n";
		if ($thisFilm->video_password != "") {
			print "<p><strong>Video Password:</strong> ".$thisFilm->video_password."</p>\n";
		}

		foreach ($video_array as $key => $thisVideo) {
			if ($key == $thisFilm->movie_id) {
				print $thisVideo->html."\n";
			}
		}
		print "<p>&nbsp;</p>\n";
		$x++;
	}
	print "</div>\n";

	print "<div id=\"competition_right\">\n";
	print "<h3>VOTING</h3>\n";
	if ($competition[0]->instructions != "") {
		print "<p><strong>Instructions:</strong> ".$competition[0]->instructions."</p>\n";
	}
	print "<p>Please rank the films in this competition from your most favorite to least favorite. To do this, drag and drop the films from the bottom section and arrange them in the top section.</p>\n";
	print "<form id=\"competitionVotingForm\">\n";
		print "<input type=\"hidden\" id=\"competition-id\" name=\"competition-id\" value=\"".$competition[0]->id."\" />\n";
		print "<input type=\"hidden\" id=\"film-ranking\" name=\"film-ranking\" value=\"\" />\n";

		print "<p><label for=\"juror-name\">Juror Name</label><span class=\"req\"> *</span><br />\n";
		print form_input(array('name'=>'juror-name', 'id'=>'juror-name', 'class'=>'text ui-widget-content ui-corner-all'))."</p>";

		print "<label for=\"film-ranking\">Your Film Ranking</label><span class=\"req\"> *</span><br />\n";
		print "<div class=\"favorite\">Most Favorite</div>\n";
		print "<ul id=\"ranking_drop_zone\" class=\"connectedSortable\">\n";
		print "</ul>\n";
		print "<div class=\"favorite\">Least Favorite</div>\n";
		print "<br />\n";

		print "<label for=\"film-list\">List of Films</label><span class=\"req\"> *</span> (must be empty to submit form)<br />\n";
		print "<ul id=\"ranking_holding_zone\" class=\"connectedSortable\">\n";
		foreach ($competition_films as $thisFilm) {
			print "<li data-id=\"".$thisFilm->movie_id."\" class=\"ui-state-default\"><span class=\"ui-icon ui-icon-arrowthick-2-n-s\"></span>".switch_title($thisFilm->title_en)."</li>\n";
		}
		print "</ul>\n";
		print "<label id=\"ranking_holding_zone_error\" style=\"display: none;\">Please drag all films to the upper box and order them before submitting.</label>\n";
		print "<br />\n";

		print "<p><label for=\"juror-notes\">Juror Notes</label><br />\n";
		print form_textarea(array('name'=>'juror-notes', 'id'=>'juror-notes', 'value'=>'', 'rows'=>'4', 'cols'=>'30', 'class'=>'text ui-widget-content ui-corner-all'))."</p>";

		print form_submit('ranking-submit', 'Submit Your Film Ranking');
	print "</form>\n";
	print "</div>\n";
}
?>

<style>
	#competition_left { float:left; margin-right: 25px; width:612px; }
	#competition_left h3 { margin: 10px 0; }
	#competition_left p { margin: 5px 0; }
	#competition_right { float:left; width:300px; overflow: auto; }
	#competition_right h3 { margin: 10px 0; }
	#competition_right .favorite { text-align: center; font-weight: bold; }
	#ranking_drop_zone, #ranking_holding_zone { list-style-type: none; margin: 0; padding: 5px 0; width: 298px; min-height: 80px; background-color: #CCCCCC; border: 1px solid #666666; border-radius: 6px 6px 6px 6px; }
	#ranking_drop_zone li, #ranking_holding_zone li { margin: 0px 5px 3px; padding: 0.4em 0.4em 0.4em 1.5em; font-size: 1em; min-height: 30px; cursor:move; background-color: #FFFFFF; border-radius: 6px 6px 6px 6px; }
	#ranking_drop_zone li span, #ranking_holding_zone li span { position: absolute; margin-left: -1.3em; }
	#ranking_holding_zone_error { color: red; font-weight: bold; display: block; }
	#competitionVotingForm label { font-weight: bold; }
	#competitionVotingForm input.text { width: 300px; }
	#competitionVotingForm textarea { width: 300px; min-height: 100px; }
</style>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {

		$("#ranking_drop_zone").sortable({
			connectWith: ".connectedSortable",
			placeholder: "ui-state-highlight",
			dropOnEmpty: false,
			update: function(event, ui) {
				var toUpdate = $('#film-ranking');
				var rankingString = "";
				$("#ranking_drop_zone li").each(function() {
					rankingString += $(this).data("id").toString() + ",";
				});
				if (rankingString.length > 0) {
					toUpdate.val(rankingString.slice(0,-1));	
				}
				if ($("#ranking_holding_zone li").length == 0) { $("#ranking_holding_zone_error").css("display", "none"); }
			}
		});
		$("#ranking_holding_zone").sortable({
			connectWith: ".connectedSortable",
			placeholder: "ui-state-highlight"
		})
		$("#ranking_drop_zone, #ranking_holding_zone").disableSelection();

		$("#competitionVotingForm").submit(function() {
			if ($("#ranking_holding_zone li").length == 0) {
				$("#ranking_holding_zone_error").css("display", "none");
				if ($("#competitionVotingForm").validate().form() == true) {
					$.ajax({
						success: function(msg, text, xhr) {
							$("#competitionVotingForm").replaceWith(msg);
						},
						error: function(xhr, msg1, msg2){
							alert( "Failure! " + xhr + msg1 + msg2); },
						data: $('#competitionVotingForm').serialize(),
						url: '/competitions/submit_vote/',
						type: 'POST',
						dataType: 'html'
					});
					return false;
				}
			} else {
				$("#ranking_holding_zone_error").css("display", "block");
				return false;
			}
		});
		$("#competitionVotingForm").validate({
			rules: { "juror-name": "required" },
			messages: { "juror-name": "Please enter your name." }
		});
	});
</script>