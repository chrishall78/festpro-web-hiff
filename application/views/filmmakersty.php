<style type="text/css">
.numbers { float:left; padding: 0px 10px; }
.numbers h1 { font-size:3em; line-height:100%; margin:0px; }
.detail { margin-left: 45px; }
.detail ol, .detail ul { margin: 0px; }
</style>

<div style="margin-top:2px;">&nbsp;</div>
<fieldset class="ui-corner-all">

      <h1 class="title"><?php print $title; ?></h1>
      
      <p>Thank you for submitting your film information to the <?php print $upcoming[0]->year." ".$upcoming[0]->name; ?>. A festival representative will contact you shortly regarding final scheduling and a request for publicity materials.</p>
      
      <div class="detail">
        <p>Second, we would like you to send us digital images of film stills, posters, and other promotional shots for your film, as well as film trailers or film clips in Quicktime .mov format (broadcast quality, 720x480), and a secure online link to a screener of your film. Please email this content to our Programming Coordinator Nancy McDonald at <a href="mailto:nancy@hiff.org">nancy@hiff.org</a>.</p>
      </div>
      
      <div class="detail">
        <p>Third, if you have these available, please send the following materials via postal mail to the festival office:</p>
        <ul>
          <li>2 posters</li>
          <li>one sheets or postcards</li>
        </ul>
        <p>Our mailing address is:<br /><br />
        	Programming Department<br />
			Hawaii International Film Festival<br />
            680 Iwilei Rd, Suite 100<br />
			Honolulu, HI 96813</p>
      </div>

      <div class="numbers"><h1>4</h1></div>
      <div class="detail">
        <p>If you are planning to attend our festival here in Hawaii, please <a href="http://festpro.hiff.org/accreditation">submit an accreditation form</a> on our site. Our general policy for delegates that have films in our festival is to waive festival entrance fees for 2 persons for a feature film and 1 person for a short film. However, in most cases we cannot cover your travel or hotel costs. We have partnered with several leading airlines and hotels serving the island of O'ahu to provide travel deals.</p>
      </div>

</fieldset>

<script type="text/javascript">
	$(function() {	
		$("input:submit").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});
	});
</script>