<?php
// Current path of images
$default_image = "/".$upload_dir."/001-default-photo.jpg";

$currentdate = time();
//print "<div class=\"film_list_stats_left\">".$start_row."-".$end_row." of ".$total_rows." films/events</div>";
print "<div class=\"film_list_stats_left\">".$total_rows." films/events</div>";
print "<div class=\"film_list_stats_right\">".$this->pagination->create_links()." <a href=\"/films/index/a/9999\">View All Films</a></div><br clear=\"all\">";
print "<fieldset class=\"ui-corner-all\">";
print "<div id=\"film_list\">";

if (isset($current_section)) { if ($current_section != "All Sections") { print "<div class=\"film_list_col full\"><h3>".$current_section."</h3></div>"; } }
if (isset($current_country)) { if ($current_country != "All Countries") { print "<div class=\"film_list_col full\"><h3>".$current_country."</h3></div>"; } }
if (isset($current_genre)) { if ($current_genre != "All Genres") { print "<div class=\"film_list_col full\"><h3>".$current_genre."</h3></div>"; } }
if (isset($current_eventtype)) { if ($current_eventtype != "All Event Types") { print "<div class=\"film_list_col full\"><h3>".$current_eventtype."</h3></div>"; } }

if (count($films) == 0 && strtotime($festival[0]->startdate) > $currentdate) {
	print "<div class=\"film_list_col fullc\">The full program &amp; schedule for the ".$festival[0]->year." ".$festival[0]->name." has not been released yet.</div>";
} else if (count($films) == 0) {
	print "<div class=\"film_list_col fullc\">No films were found with that search criteria. Please widen your search or <a href=\"/films/\">visit our full film listing page</a>.</div>";
}

$record = 1;
$film_program_array = array();
foreach ($films as $thisFilm) {
	$title = switch_title($thisFilm->title_en);
	$trimmed = 0;

	// Cut synopsis down to 200 characters, strip out any last word fragments.
	$synopsis = strip_tags(stripslashes($thisFilm->synopsis_short));
	$synopsis_temp = substr(strip_tags(stripslashes($thisFilm->synopsis_short)),0,200);
	if (strlen($synopsis) <= 15) {
		$synopsis = strip_tags(stripslashes($thisFilm->synopsis));
		$synopsis_temp = substr(strip_tags(stripslashes($thisFilm->synopsis)),0,200);
	}
	if ($synopsis != $synopsis_temp) {
		$synopsis_array = explode(" ", $synopsis_temp);
		$synopsis = "";
		$count = count($synopsis_array)-1;
		for ($x=0; $x < $count; $x++) {
			$synopsis = $synopsis.$synopsis_array[$x]." ";
		}
		$trimmed = 1;
	}

	$country_list = "";
	foreach ($countries as $thisCountry) {
		if ($thisCountry->movie_id == $thisFilm->movie_id ) {
			$country_list .= $thisCountry->name.", ";
		}
	} $country_list = trim($country_list,", ");

	$genre_list = "";
	foreach ($genres as $thisGenre) {
		if ($thisGenre->movie_id == $thisFilm->movie_id ) {
			$genre_list .= $thisGenre->name.", ";
		}
	} $genre_list = trim($genre_list,", ");
	
	$no_image = "yes"; 
	foreach ($all_photos as $thisPhoto) {
		if ($thisPhoto->movie_id == $thisFilm->movie_id) {
			$no_image = $thisPhoto->url_cropsmall;
			break;
		}
	}
	
	if ($thisFilm->id == 0) {
		// Print record for film program
		print "\t\t<div class=\"film_list_col";
		if ($thisFilm->Published == 0) { print " unpublished"; }
		print "\">\n";
		print "\t\t\t<div class=\"film_list_image\"><a href=\"/films/program/".$thisFilm->slug."\">\n";
		print "\t\t\t<div id=\"".$thisFilm->slug."\" class=\"nivoSlider\">\n";
		foreach ($film_program_photos as $program_slug => $url_array ) {
			if ($program_slug == $thisFilm->slug) {
				if (count($url_array) > 0) {
					$film_program_array[] = $program_slug;
					foreach ($url_array as $film_photo_url) {
						print "\t\t\t\t<img src=\"".$film_photo_url."\" width=\"210\" height=\"118\" border=\"0\">\n";
					}
				} else {
					print "\t\t\t\t<img src=\"".$default_image."\" width=\"210\" height=\"118\" border=\"0\">\n";
				}
			}
		}
		print "\t\t\t</div>\n";
		print "\t\t\t</a></div>\n";

	 	print "\t\t<p class=\"film_list_title\"><a href=\"/films/program/".$thisFilm->slug."\">".strtoupper($title)."</a><br />";
		print "<span class=\"film_list_info\">Various Directors | ".$thisFilm->runtime_int." min. | Plays with:</span></p>";
	 	print "\t\t<p>".$thisFilm->title."</p>\n";

	 	if ($synopsis != '') {
			print "\t\t<p>".trim($synopsis);
			$trimmed == 1 ? print "..." : print "";
			print "</p>\n";
	 	}

	 	if ($thisFilm->event_name != '') {
	 		print $thisFilm->event_name;
	 	}
		print "\t</div>\n";
	} else {
		$skip_short_films = false;
		if ($selected_page == "film_list") {
			$skip_short_films = true;
		}
		// add filter to not show shorts programs
		if ($thisFilm->runtime_int >= 45 || $skip_short_films == false) {
			// Print normal film record
			print "\t\t<div class=\"film_list_col";
			if ($thisFilm->Published == 0) { print " unpublished"; }
			print "\">\n";
			print "\t\t\t<div class=\"film_list_image\"><a href=\"/films/detail/".$thisFilm->slug."\"><img src=\"";
			if ($no_image == "yes") { print $default_image; } else { print $no_image; }
			print "\" height=\"118\" border=\"0\"></a></div>\n";
		 	print "\t\t<p class=\"film_list_title\"><a href=\"/films/detail/".$thisFilm->slug."\">".$title."</a><br />";
			print "<span class=\"film_list_info\">";
			if ($thisFilm->title != "") { print $thisFilm->title."<br />"; }
			print $country_list." ".$thisFilm->year." | ";
			if ($thisFilm->runtime_int != 0) {
				print $thisFilm->runtime_int;
			} else {
				$runtime_total = 0;
				foreach ($schedule as $thisScreening) {
					if ($thisScreening->movie_id == $thisFilm->movie_id) {
						$runtime_start = 0;
						$film_array = explode(",",$thisScreening->program_movie_ids);
						foreach ($film_array as $thisFilmID) {
							foreach ($filmids as $thisFilm2) {
								if ($thisFilmID == $thisFilm2->movie_id) {
									$runtime_start += $thisFilm2->runtime_int;
								}
							}
						}
						$runtime_total = $runtime_start;
					}
				}
				print $runtime_total;
			}
			print " min.<br />";
			if ($thisFilm->category_id != 0) { print $thisFilm->category_name. " | "; }
			print $genre_list;
			print "</span></p>";

			print "\t\t<p>".trim($synopsis);
			$trimmed == 1 ? print "..." : print "";
			print "</p>\n";

			$counter = 0; $prevScrnDate = ""; $prevScrnTime = ""; $prevScrnMovie = "";
			foreach ($schedule as $thisScreening) {
				if (($counter != 0) && ($thisScreening->date == $prevScrnDate) && ($thisScreening->time == $prevScrnTime) && ($thisScreening->movie_id == $prevScrnMovie) ) {
					// Display nothing	
				} else {
					$film_array = explode(",",$thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilmID) {
						if ($thisFilm->movie_id == $thisFilmID) {
							$datetime = $thisScreening->date." ".$thisScreening->time;
							$date = date("D, M d", strtotime($datetime));
							$time = date("g:iA", strtotime($datetime));
				
							$location = $thisScreening->displayname;
							
							if ($thisScreening->url != "") {
								if (FP_FRONT_ENABLE_FACEBOX) {
									print "<p class=\"film_list_screening\"><a href=\"#!".$thisScreening->url."\" rel=\"facebox\">".$date." - ".$time."&nbsp;&nbsp;".$location."</a></p>";
								} else {
									print "<p class=\"film_list_screening\"><a href=\"".$thisScreening->url."\" target=\"_blank\">".$date." - ".$time."&nbsp;&nbsp;".$location."</a></p>";
								}
							} else {
								print "<p class=\"film_list_screening\">".$date." - ".$time."&nbsp;&nbsp;".$location."</p>";
							}
						}
					}
				}
				$prevScrnDate = $thisScreening->date;
				$prevScrnTime = $thisScreening->time;
				$prevScrnMovie = $thisScreening->movie_id;
				$counter++;
			}
			print "\t</div>\n";	
		}
	}	
	$record++;
}
print "</div>";
print "</fieldset>";
//print "<div class=\"film_list_stats_left\">".$start_row."-".$end_row." of ".$total_rows." films/events</div>";
print "<div class=\"film_list_stats_left\">".$total_rows." films/events</div>";
print "<div class=\"film_list_stats_right\">".$this->pagination->create_links()." <a href=\"/films/index/a/9999\">View All Films</a></div>";
?>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#film_list').isotope({ itemSelector: '.film_list_col', 'layoutMode': 'fitRows' });
		$('#film_list').imagesLoaded( function() {
			// images have loaded
			$('#film_list').isotope({ itemSelector: '.film_list_col', 'layoutMode': 'fitRows' });
		});

<?php
	foreach ($film_program_array as $program_slug) {
		print "\t\t\t$('#".$program_slug."').nivoSlider({\n";
		print "\t\t\t\teffect: 'fade',\n";
		print "\t\t\t\tcontrolNav: false,\n";
		print "\t\t\t\tdirectionNav: false,\n";
		print "\t\t\t\tpauseOnHover: true,\n";
		print "\t\t\t\tanimSpeed: 500,\n";
		print "\t\t\t\tpauseTime: 3000\n";
		print "\t\t\t});\n\n";
	}
?>
	});
</script>