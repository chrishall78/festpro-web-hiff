<?php
printScheduleByDay($full_schedule, $activeDay, "grid", "top");

if (count($schedule) == 0) {
	print "<br clear=\"all\" /><fieldset class=\"ui-corner-right\" style=\"position:relative;\">";
	print "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
	print "<tr><td align=\"center\">No schedule data was found at this time.</td></tr>";
	print "</table></fieldset>";
} else {
	print "<br clear=\"all\" />";


	$filmcode_string = ""; $locations = array(); $width = 100;
	foreach ($schedule as $thisScreening) {	$locations[] = $thisScreening->displayname; }
	$locations = array_unique($locations);
	sort($locations);	
	$count = count($locations);	

	print "<a class=\"prev browse left disabled\"></a>";
	print "<a class=\"next browse right disabled\"></a>";
	
	$previousDate = ""; $counter = 0;
	$prevScrnDate = ""; $prevScrnTime = ""; $prevScrnMovie = "";
	foreach ($schedule as $thisScreening) {
			$eventType = $thisScreening->event_slug;		
			$tickets_img = find_screening_status($thisScreening->date,$thisScreening->url,$thisScreening->Rush,$thisScreening->Free);
	
			$title = ""; $year = ""; $runtime = ""; $film_id = "";
			$length = 0;
			$film_ids = explode(",",$thisScreening->program_movie_ids);
			foreach ($film_ids as $thisFilmID) {
				foreach ($films as $thisFilm) {
					if ($thisFilmID == $thisFilm->movie_id) {
						$length = $length + $thisFilm->runtime_int;
					}				
					if ($thisFilm->movie_id == $thisScreening->movie_id) {
						if ($thisScreening->program_name != "") {
							$title = switch_title($thisScreening->program_name);
							$slug = $thisScreening->program_slug;
						} else {
							$title = switch_title($thisFilm->title_en);
							$slug = $thisFilm->slug;
						}
						$year = $thisFilm->year;
						$runtime = $thisFilm->runtime_int;
						$film_id = $thisFilm->movie_id;
					}
				}
			}
	
			$offsetTime = strtotime($thisScreening->date." 09:00:00"); 
			$startTime = strtotime($thisScreening->date." ".$thisScreening->time);
			$endTime = $startTime + intval($length*60);
			
			$extraTime = 0;
			if ($thisScreening->intro == 1) { $endTime = $endTime + intval($thisScreening->intro_length*60); $extraTime = $extraTime + $thisScreening->intro_length; }
			if ($thisScreening->fest_trailer == 1) { $endTime = $endTime + intval($thisScreening->fest_trailer_length*60); $extraTime = $extraTime + $thisScreening->fest_trailer_length; }
			if ($thisScreening->sponsor_trailer1 == 1) { $endTime = $endTime + intval($thisScreening->sponsor_trailer1_length*60); $extraTime = $extraTime + $thisScreening->sponsor_trailer1_length; }
			if ($thisScreening->sponsor_trailer2 == 1) { $endTime = $endTime + intval($thisScreening->sponsor_trailer2_length*60); $extraTime = $extraTime + $thisScreening->sponsor_trailer2_length; }
			if ($thisScreening->q_and_a == 1) { $endTime = $endTime + intval($thisScreening->q_and_a_length*60); $extraTime = $extraTime + $thisScreening->q_and_a_length; }
			
			// Add 40 pixels to the height to allow for a header
			$offsetHeight = 40 + (($startTime - $offsetTime) / 60);
	
			// Add 41 pixels to the width to allow for a time column
			$offsetWidth = 0;
			foreach ($locations as $thisLocation) {
				if ($thisLocation == $thisScreening->displayname) {
					break;
				} else {
					$offsetWidth = $offsetWidth + $width + 8;
				}
			}
	
			foreach ($locations as $thisLocation) {
				if ($thisLocation == $thisScreening->displayname) {
					// Start table output
					$filmcode_string .= "<div class=\"floatingFilm ui-corner-all";
					if ($eventType != "") { $filmcode_string .= " ".$eventType; }
					$filmcode_string .= "\" style=\"width:".$width."px; height:".($length + $extraTime - 8)."px; top:".$offsetHeight."px; left:".$offsetWidth."px; \">\n";
			
					// Insert hover box code here
			
					if ($thisScreening->program_name != "") {
						$filmcode_string .= "<p class=\"film_title\"><a href=\"/films/program/".$slug."\" class=\"schedule_grid_title\">";
					} else {
						$filmcode_string .= "<p class=\"film_title\"><a href=\"/films/detail/".$slug."\" class=\"schedule_grid_title\">";
					}
					$filmcode_string .= substr($title,0,20);
					if (strlen($title) > 20 ) { $filmcode_string .= "..."; }
					$filmcode_string .= "</a></p>\n";
					$filmcode_string .= "<p>".date("g:i",$startTime)." - ".date("g:ia",$endTime)."</p>\n";
					$filmcode_string .= $tickets_img;
					$filmcode_string .= "</div>\n";
				}
			}
	}

	// Print 'Time' Column - non-moving
	print "\t\t<div class=\"grid_scrollable_time\">\n";
	print "\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"52\">\n";
	print "<tr><th class=\"time\">Time</th></tr>";
	$starttime = "9"; $endtime = "24"; $counter = $starttime;
	while ($counter <= $endtime) {
		if ($counter > 12) { $printCounter = $counter-12; } else { $printCounter = $counter; }
		if ($printCounter < 10) { $z = "0"; } else { $z = ""; }
		print "<tr><td class=\"time onhour\">".$z.$printCounter.":00</td></tr>\n";
		print "<tr><td class=\"time halfhour\">".$z.$printCounter.":30</td></tr>\n";
		$counter++;
	}
	print "\t\t\t</table>\n";
	print "\t\t</div>\n";

	print "<div id=\"grid_scrollable\" class=\"grid_scrollable ui-corner-right\">\n";
	print "\t<div class=\"items\">\n";
	
	$x = 0;
	foreach ($locations as $thisLocation) {
		print "\t\t<div>\n";
		print "\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"108\" class=\"grid_visible\"><thead>\n";
		print "\t\t\t\t<tr><th>".$thisLocation."</th></tr>\n";
		print "\t\t\t</thead><tbody>\n";
		$starttime = "9"; $endtime = "24"; $counter = $starttime;
		while ($counter <= $endtime) {
			if ($counter > 12) { $printCounter = $counter-12; } else { $printCounter = $counter; }
			if ($printCounter < 10) { $z = "0"; } else { $z = ""; }
			print "\t\t\t\t<tr><td class=\"onhour\">&nbsp;</td></tr>\n";
			print "\t\t\t\t<tr><td class=\"halfhour\">&nbsp;</td></tr>\n";
			$counter++;
		}
		print "\t\t\t</tbody></table>\n";
		if ($x == 0) {
			print $filmcode_string;
		}
		print "\t\t</div>\n";
		$x++;
	}
	while ($count < 6) {
		print "\t\t<div>\n";
		print "\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"108\" class=\"grid_visible\"><thead>\n";
		print "\t\t\t\t<tr><th>&nbsp;</th></tr>\n";
		print "\t\t\t</thead><tbody>\n";
		$starttime = "9"; $endtime = "24"; $counter = $starttime;
		while ($counter <= $endtime) {
			if ($counter > 12) { $printCounter = $counter-12; } else { $printCounter = $counter; }
			if ($printCounter < 10) { $z = "0"; } else { $z = ""; }
			print "\t\t\t\t<tr><td class=\"onhour\">&nbsp;</td></tr>\n";
			print "\t\t\t\t<tr><td class=\"halfhour\">&nbsp;</td></tr>\n";
			$counter++;
		}
		print "\t\t\t</tbody></table>\n";
		print "\t\t</div>\n";
		$count++;
	}

	print "\t</div>\n";
	print "</div><br clear=\"all\">\n";	

	$colorArray = array();
	foreach ($full_schedule as $thisScreening) {
		if (!isset($colorArray[$thisScreening->event_slug])) {
			$colorArray[$thisScreening->event_slug] = $thisScreening->event_color;
		}
	}
	print "<style type=\"text/css\">\n";
	foreach ($colorArray as $slug => $color) {
		print ".".$slug." { background-color: #".$color."; }\n";
	}
	print "</style>\n";
}

printScheduleByDay($full_schedule, $activeDay, "grid", "bottom");
?>

<br clear="all">

<script type="text/javascript" language="javascript">
	$(document).ready(function() { 
		// Temporary fix since scrollable is not working properly.
		$("#grid_scrollable .items").draggable({ axis: "x", cursor: "move", grid: [108, 0]});

<?php if (count($locations) > 0) { ?>
		// 11/10/2011 - Need to find a way to only show floating headers for "visible" columns. cbFadeIn callback seems to be the way to go.
		//jQuery(".items TABLE").floatHeader({fadeOut:250, fadeIn:250, floatClass:'floatHeader1'});
<?php } ?>

	});
</script>