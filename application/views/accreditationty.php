<div style="margin-top:2px;">&nbsp;</div>
<fieldset class="ui-corner-all">

    <h1 class="title"><?php print $title; ?></h1>
	<p>Thank you for submitting your delegate application. A festival representative will review your application and reply to you by phone or email with an update on your status as a delegate.</p>

	<p><strong>Industry delegates who do not qualify for complimentary accreditation:</strong> Please submit your $150 payment by clicking the button below after submitting this form.</a></p>

	<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="732c8c70-c81e-4036-a827-5555eeb5d46f" /> <input type = "image" src ="//content.authorize.net/images/buy-now-blue.gif" /> </form>

	<p><b>Please Note:</b> This is simply an application to become a delegate, it is not a confirmation of delegate status.</p>      
	<p align="center"><a href="/accreditation">Back to the Guest Accreditation Page</a></p>

</fieldset>