<?php
// Current path of images
$default_image = "/".$upload_dir."/001-default-photo.jpg";

$currentdate = time();
print "<fieldset class=\"ui-corner-all\">\n";
print "<div id=\"section_list\">\n";

$record = 1;
foreach ($section_photos as $thisSection) {
	print "\t<div class=\"film_list_col\">\n";
	print "\t\t<div class=\"section_image\">";
	print "<a href=\"/films/section/".$thisSection->slug."\"><img src=\"".$thisSection->url_cropsmall."\" height=\"120\"><br />".$thisSection->program_name."</a>";
	print "</div>";
	print "\t</div>\n";
	$record++;
}

print "</div>";
print "</fieldset>";
?>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#section_list').isotope({ itemSelector: '.film_list_col', 'layoutMode': 'fitRows' });
		$('#section_list').imagesLoaded( function() {
			// images have loaded
			$('#section_list').isotope({ itemSelector: '.film_list_col', 'layoutMode': 'fitRows' });
		});
	});
</script>