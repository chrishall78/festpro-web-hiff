<?php $sample_fest = "/2014_hiff_fall_festival"; $timestamp = "1325376000";

include_once('pretty-print.php');

if ($error == "") {
	// requires included file above for formatted output
	print indent($output);
} else {
	print $output;
?>
<h1>FestPro API Documentation - Version <?php print $api_version; ?></h1>
<p>This API will return information about the festival, films, screenings or related information in a JSON format. A slug for the festival is required. This is generally composed of the year and name of the festival, in all lowercase, with any spaces converted to underscores.</p>
<p>Sample JSON format: <code>{"api_version":"<?php print $api_version; ?>","api_call":"films","api_data":[collection_of_objects]}</code></p>
<p>&nbsp;</p>

<h2>Festival - 'festival'</h2>
<p><code><?php print $site_url.$sample_fest."/festival"; ?></code></p>
<p>This returns information about the festival, including start and end dates, name, year and any program updates.</p>
<p>&nbsp;</p>

<h2>Films - 'films' or 'films_since'</h2>
<p><code><?php print $site_url.$sample_fest."/films"; ?></code></p>
<p>This returns information about all of the confirmed films in the festival, and includes a photo url if there is one for the film.</p>
<p><code><?php print $site_url.$sample_fest."/films_since/".$timestamp; ?></code></p>
<p>By providing a timestamp, you can filter the results to be only films updated after that date and time.</p>
<p>&nbsp;</p>

<h2>Schedule - 'schedule' or 'schedule_since'</h2>
<p><code><?php print $site_url.$sample_fest."/schedule"; ?></code></p>
<p>This returns a chronologically ordered schedule of film screenings in the festival.</p>
<p><code><?php print $site_url.$sample_fest."/schedule_since/".$timestamp; ?></code></p>
<p>By providing a timestamp, you can filter the results to be only screenings updated after that date and time.</p>
<p>&nbsp;</p>

<h2>Personnel - 'personnel' or 'personnel_since'</h2>
<p><code><?php print $site_url.$sample_fest."/personnel"; ?></code></p>
<p>This returns records of all the cast &amp; crew for each film in the festival.</p>
<p><code><?php print $site_url.$sample_fest."/personnel_since/".$timestamp; ?></code></p>
<p>By providing a timestamp, you can filter the results to be only personnel updated after that date and time.</p>
<p>&nbsp;</p>

<h2>Photos - 'photos'</h2>
<p><code><?php print $site_url.$sample_fest."/photos"; ?></code></p>
<p>This returns links to small and large thumbnails of for each film in the festival which has them.</p>
<p>&nbsp;</p>

<h2>Videos - 'videos'</h2>
<p><code><?php print $site_url.$sample_fest."/videos"; ?></code></p>
<p>This returns links to videos on external sites for each film in the festival which has them.</p>
<p>&nbsp;</p>

<h2>Updates - 'updates_since'</h2>
<p><code><?php print $site_url.$sample_fest."/updates_since/".$timestamp; ?></code></p>
<p><em>Requires a timestamp as the last parameter.</em> This returns an array listing the number of films, screenings, personnel, photos and videos that have been updated after that date and time.</p>
<p>&nbsp;</p>

<?php } ?>