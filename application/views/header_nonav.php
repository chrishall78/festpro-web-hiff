<!doctype html>
<html class="no-js boxed-layout skin-black std-selector" lang="en" prefix="og: http://ogp.me/ns#">
  <head>

	<meta name="description" content="2017 HIFF Fall Showcase presented by Halekulani, Nov 2 - 12, 2017"/>
	<link rel="canonical" href="https://festpro.hiff.org/" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Hawaii International Film Festival" />
	<meta property="og:description" content="2017 HIFF Fall Showcase presented by Halekulani, Nov 2 - 12, 2017" />
	<meta property="og:url" content="http://festpro.hiff.org/" />
	<meta property="og:site_name" content="Hawaii International Film Festival" />
	<meta property="og:image" content="http://www.hiff.org/wp-content/uploads/2016-sponsors/fb-banner.png" />
	<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/www.hiff.org\/","name":"Hawaii International Film Festival","potentialAction":{"@type":"SearchAction","target":"http:\/\/www.hiff.org\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
	<link rel="stylesheet" href="/themes/hiff_2017_fall/css/960.css">
	<link rel="stylesheet" href="/themes/hiff_2017_fall/css/hiff-style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,300' rel='stylesheet' type='text/css'>
	<script src="/themes/hiff_2017_fall/js/modernizr-2.0.6.min.js"></script>

	<!-- Begin FestPro CSS and Scripts -->
	<title><?php print $title; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content="text/html; charset=UTF-8" http-equiv="content-type" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/program-style.css" />
	<link type="text/css" rel="stylesheet" media="print" href="<?php echo $path;?>assets/css/program-print.css" />
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/jquery-ui-theme/jquery-ui-1.10.3.custom.min.css" />
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/css/admin/imgAreaSelect/imgareaselect-default.css" />
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo $path;?>assets/scripts/facebox/facebox.css" />

	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.validate.daterange.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/admin/jquery.frameready.js"></script>
	<script type="text/javascript" src="<?php echo $path;?>assets/scripts/facebox/facebox.js"></script>
	<!-- End FestPro CSS and Scripts -->

    <style type="text/css" media="screen">
            /* Header Logo */

                    header div.logo > a, .ie8 header div.logo > a {
            width: 768px;
            height: 272px;
            background: transparent url(http://www.hiff.org/transparent.png) center center no-repeat;
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 1.5) {
            header div.logo > a, .ie8 header div.logo > a {
                background-image: url(http://www.hiff.org/transparent.png);
                background-size: 768px 272px;
            }
        }

            
        @media screen and (max-width: 767px) {
            header div.logo > a {
                width: 320px;
                height: 220px;
                background-image: url(http://www.hiff.org/transparent.png);
            }
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 1.5) and (max-width: 767px) {
            header div.logo > a {
                background-image: url(http://www.hiff.org/transparent.png);
                background-size: 320px 220px;
            }
        }

    </style>
        <style type="text/css" media="screen">
        a, a:visited, .widget_rss h3 a:hover, body a:hover, body a:visited:hover, .main .tabs > li > a, .tabs > li > a, blockquote, span.pullquote, div.video-embed-shortcode:hover, div#map_canvas:hover, div.recent-posts-carousel h5 a:hover, div.recent-posts-carousel article.item div.description a, div.recent-posts-carousel  article.item div.description a:visited, ul#filters li div:hover, article.item div.description a:hover, .comment a, .comment a:visited, .comment a:hover, .comment .message a.reply:hover, .paginators ul.page-numbers li a.prev:hover, .paginators ul.page-numbers li a.next:hover {
            /*  color: ; */
        }

        html.no-touch span.image-overlay, div.video-embed-shortcode:hover, div#map_canvas:hover {
            /*  border-color: ; */
        }

        a.image-link span.overlay-thumbnail {
            /*  background : ; */
        }

        input:focus, textarea:focus, .search-widget > form.search-form > fieldset.has-focus {
            /*  border-color: rgba(, , , 0.8);*/
            /*  -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1), 0 0 8px rgba(, , , 0.6);*/
            /*  -moz-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1), 0 0 8px rgba(, , , 0.6);*/
            /*   box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1), 0 0 8px rgba(, , , 0.6);*/
        }

        .search-widget > form.search-form > fieldset.has-focus button {
            /*     border-color: rgba(, , , 0.8);*/
        }
    </style>
    <style type="text/css"></style><style type="text/css">header { padding-top: 0px; }div.menu-wrapper { margin-top: 0px; }div.logo { padding-top: 0px; }div.tagline { margin-top: 0px; }div.header-content { margin-top: 0px; }html body div.social-links > a.facebook-link.btn { background-color: #00aeef;}html body div.social-links > a.twitter-link.btn { background-color: #00aeef;}html body div.social-links > a.linkedin-link.btn { background-color: #00aeef;}html.std-selector body span.call-us-button a.btn { background-color: #00aeef;}div.menu-wrapper ul.topmenu > li:hover > a strong, div.menu-wrapper ul.topmenu > li.sfHover > a strong { color:#ffdc17;}html.std-selector body a.mobile-menu-btn.btn { background-color: #00aeef;}div#nav > div.nav-controls > a#next, .flex-control-nav li a.icon-play, html.msie .flex-control-nav li a.icon-play { color:#00aeef;}div#nav > div.nav-controls > a#pause, .flex-control-nav li a.icon-pause, html.msie .flex-control-nav li a.icon-pause { color:#00aeef;}div.prime-page a, div.prime-page a:hover, div.prime-page a:visited { color:#f7941e;}html.std-selector div.plan-action > a.btn, html.std-selector div.plan-action > a.btn:hover, html.std-selector div.plan-action > a.btn:visited { background-color: #00aeef;}html.std-selector body div.recent-posts article.item div.description p.post-meta a,
html.std-selector body div.recent-posts article.item div.description p.post-meta a:visited,
html.std-selector body div.recent-posts-shortcode div.preview-content p.post-meta a,
html.std-selector body div.recent-posts-shortcode div.preview-content p.post-meta a:visited,
html.std-selector body span.categories a, html.std-selector body span.categories a:hover, html.std-selector body span.categories a:visited { background-color: #f7941e;}html.std-selector body div#sidebar a, html.std-selector body div#sidebar p a, html.std-selector body div#sidebar .widget p a, html.std-selector body div#sidebar a:hover, html.std-selector body div#sidebar p a:hover, html.std-selector body div#sidebar .widget p a:hover, html.std-selector body div#sidebar a:active, html.std-selector body div#sidebar p a:active, html.std-selector body div#sidebar .widget p a:active, html.std-selector body div#sidebar a:visited, html.std-selector body div#sidebar p a:visited, html.std-selector body div#sidebar .widget p a:visited { color:#f7941e;}</style>        <style type="text/css">
            div[role="document"] {
                font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            }
        </style>
                <style type="text/css">
            div[role="document"] {
                font-size: 13px;
            }
        </style>
        <style type="text/css" media="screen">#wpadminbar { top: 28px; }
@media screen and ( max-width: 782px ) {
    #wpadminbar { top: 0; position: fixed; }
}
.home .prime-page a img.alignnone { margin: 5px 20px 5px 0; }
.home .prime-page h4 { margin: -20px 0 10px; }
.hp-main-link { display:block; margin-bottom: 10px; font-size: x-large; }
div.period { color: #666666; }</style>
    <style>
        div.cpslider-wrapper, div.content-slider-wrapper { width: 100%; position: relative; margin: 0 auto; }
        div.cpslider > div.slide, div.content-slider > div.slide { margin: 0 auto; width: 100%; }
        .cpslider-inner-wrap, .content-slider-inner-wrap { position: relative; margin: 0 auto; height: 100%; overflow: hidden; }
        .cpslider > .slide, .content-slider > .slide { display: none; }
        .cpslider > .slide > .slide-content, .content-slider > .slide > .slide-content { margin: 0 auto; padding: 0; width: auto; position: relative; height: 100%; }
        div.cpslider > div.slide .cp-anim-image, div.content-slider > div.slide .cp-anim-image { bottom: 0; display: block; vertical-align: bottom; }
        .long-anim { -webkit-animation-duration: 3s; -webkit-animation-delay: .5s; }
    </style>

</head>

<body>

<div id="wrap" class="container document-container" role="document">

		<div class="header-bg">
		<div class="menu-wrapper-upper">
			<div class="uppermenu">
				<div class="uppersearch">
					<form action="http://www.hiff.org/" method="get" class="searchform">
						<input type="text" name="s" value="" class="searchtext" />
						<input type="submit" value="Search" class="searchsubmit" />
					</form>
				</div>
				<ul id="upper_menu" class="sf-menu topmenu"><li id="menu-home" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="http://www.hiff.org/"><span class="menu-link-wrap"><strong>Home</strong></span></a></li>
<li id="menu-photo-gallery" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/photo-gallery/"><span class="menu-link-wrap"><strong>Photo Gallery</strong></span></a></li>
<li id="menu-blog" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/blog/"><span class="menu-link-wrap"><strong>Blog</strong></span></a></li>
<li id="menu-news-press" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.hiff.org/content/pressreleases/"><span class="menu-link-wrap"><strong>News / Press</strong></span></a></li>
<li id="menu-donate" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/donate/"><span class="menu-link-wrap"><strong>Donate</strong></span></a>
<ul class="sub-menu">
	<li id="menu-hiff-ohana-legacy" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/donate/ohana-legacy/"><span class="menu-link-wrap"><strong>HIFF Ohana Legacy</strong></span></a></li>
</ul>
</li>
<li id="menu-contact-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/contact-hiff/"><span class="menu-link-wrap"><strong>Contact HIFF</strong></span></a></li>
</ul>            </div>
		</div>
		<div class="header-bg-fill"></div>
	</div>
	<header>
		
		<nav>
				<div class="logo">
					<a name="header-logo" href="http://www.hiff.org">
					</a>
				</div>
				<div class="tagline">
									</div>

				<div class="mobile-uppersearch">
					<form action="http://www.hiff.org/" method="get" class="searchform">
						<input type="text" name="s" value="" class="searchtext" />
						<input type="submit" value="Search" class="searchsubmit" />
					</form>
				</div>


								<a class="mobile-menu-btn btn" href="javascript:void(0)" data-toggle="collapse"
				   data-target="#mobile-menu-wrapper">
										<span class="list-icon-row"></span>
					<span class="list-icon-row"></span>
					<span class="list-icon-row"></span>
									</a>
			<div class="header-content standard-header-content">


				<div class="social-links">
																			</div>

				
			
			</div>

			<div class="clear"></div>
			<div class="menu-wrapper">

			   <ul id="topmenu" class="sf-menu desktop-menu topmenu"><li id="menu-films-events" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Films &#038; Events</strong></span></a>
<ul class="sub-menu">
	<li id="menu-browse-films-events" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Browse Films &#038; Events</strong></span></a></li>
	<li id="menu-view-schedule-buy-tickets" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/schedule/"><span class="menu-link-wrap"><strong>View Schedule / Buy Tickets</strong></span></a></li>
	<li id="menu-program-updates" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/updates/"><span class="menu-link-wrap"><strong>Program Updates</strong></span></a></li>
	<li id="menu-program-guide-mobile-apps" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/program-guide-mobile-apps/"><span class="menu-link-wrap"><strong>Program Guide &#038; Mobile Apps</strong></span></a></li>
	<li id="menu-box-office-ticket-information" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-tickets/"><span class="menu-link-wrap"><strong>Box Office &#038; Ticket Information</strong></span></a></li>
	<li id="menu-how-to-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/howtohiff/"><span class="menu-link-wrap"><strong>How to HIFF</strong></span></a></li>
	<li id="menu-membership-flash-passes" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li id="menu-box-office-membership-faqs" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-faqs/"><span class="menu-link-wrap"><strong>Box Office &#038; Membership FAQs</strong></span></a></li>
</ul>
</li>
<li id="menu-filmmakers" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Filmmakers</strong></span></a>
<ul class="sub-menu">
	<li id="menu-call-for-entries" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Call for Entries</strong></span></a></li>
	<li id="menu-delegate-press-accreditation" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accreditation/"><span class="menu-link-wrap"><strong>Delegate &#038; Press Accreditation</strong></span></a></li>
	<li id="menu-accredited-guest-delegate-on-the-ground-info" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accredited-guest-delegate-on-the-ground-info/"><span class="menu-link-wrap"><strong>Accredited Guest / Delegate On-the-Ground Info</strong></span></a></li>
	<li id="menu-awards-and-jurors" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/awards-jurors/"><span class="menu-link-wrap"><strong>Awards and Jurors</strong></span></a></li>
	<li id="menu-logos-laurels-film-images" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/logos-film-images/"><span class="menu-link-wrap"><strong>Logos, Laurels &#038; Film Images</strong></span></a></li>
	<li id="menu-key-visual" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/key-visual/"><span class="menu-link-wrap"><strong>Key Visual</strong></span></a></li>
	<li id="menu-travel-hotel-information" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/travel-hotels/"><span class="menu-link-wrap"><strong>Travel &#038; Hotel Information</strong></span></a></li>
</ul>
</li>
<li id="menu-support" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/sponsorship/"><span class="menu-link-wrap"><strong>Support</strong></span></a>
<ul class="sub-menu">
	<li id="menu-hiff-sponsors" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/sponsorship/hiff-sponsors-spring-2017/"><span class="menu-link-wrap"><strong>HIFF Sponsors</strong></span></a></li>
	<li id="menu-membership-flash-passes" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li id="menu-volunteer-at-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/volunteer/"><span class="menu-link-wrap"><strong>Volunteer at HIFF</strong></span></a></li>
	<li id="menu-donate-individual-giving" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/donate/"><span class="menu-link-wrap"><strong>Donate – Individual Giving</strong></span></a></li>
	<li id="menu-job-opportunities-internships" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/job-opportunities/"><span class="menu-link-wrap"><strong>Job Opportunities &#038; Internships</strong></span></a></li>
	<li id="menu-cinema-partners" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cinema-partners/"><span class="menu-link-wrap"><strong>Cinema Partners</strong></span></a></li>
</ul>
</li>
<li id="menu-creative-lab-at-hiff" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/creative-lab/"><span class="menu-link-wrap"><strong>Creative Lab at HIFF</strong></span></a>
<ul class="sub-menu">
	<li id="menu-creative-lab-hiff-ebert-young-writers-program-for-the-arts" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/roger-ebert-foundation-young-film-critics-program/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF: Ebert Young Writers Program for the Arts</strong></span></a></li>
	<li id="menu-creative-lab-hiff-doc-lab-anatomy-of-oscar-winning-documentaries" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/hiff-doc-lab-anatomy-of-oscar-winning-documentaries/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF Doc Lab: Anatomy of Oscar Winning Documentaries</strong></span></a></li>
</ul>
</li>
<li id="menu-education-programs" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/"><span class="menu-link-wrap"><strong>Education Programs</strong></span></a>
<ul class="sub-menu">
	<li id="menu-cultural-visual-literacy-program" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program</strong></span></a>
	<ul class="sub-menu">
		<li id="menu-cultural-visual-literacy-program-signup-form-oahu" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/cvlp-signup-form-oahu/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Oahu</strong></span></a></li>
		<li id="menu-cultural-visual-literacy-program-signup-form-big-island" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cvlp-signup-form-big-island/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Big Island</strong></span></a></li>
	</ul>
</li>
	<li id="menu-guest-filmmaker-program" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/guest-filmmaker-program/"><span class="menu-link-wrap"><strong>Guest Filmmaker Program</strong></span></a>
	<ul class="sub-menu">
		<li id="menu-gfp-signup-form-oahu" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form/"><span class="menu-link-wrap"><strong>GFP Signup Form (Oahu)</strong></span></a></li>
		<li id="menu-gfp-signup-form-kauai" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form-kauai/"><span class="menu-link-wrap"><strong>GFP Signup Form (Kauai)</strong></span></a></li>
	</ul>
</li>
	<li id="menu-student-showcase" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/student-showcase/"><span class="menu-link-wrap"><strong>Student Showcase</strong></span></a></li>
	<li id="menu-kupuna-lens-film-program" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/kupuna-lens/"><span class="menu-link-wrap"><strong>Kupuna Lens Film Program</strong></span></a></li>
</ul>
</li>
<li id="menu-about-hiff" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About HIFF</strong></span></a>
<ul class="sub-menu">
	<li id="menu-about-the-organization" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About the Organization</strong></span></a></li>
	<li id="menu-contact-hiff-staff-board-of-directors" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/contact-hiff/"><span class="menu-link-wrap"><strong>Contact HIFF – Staff &#038; Board of Directors</strong></span></a></li>
	<li id="menu-festival-history" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/history/"><span class="menu-link-wrap"><strong>Festival History</strong></span></a></li>
	<li id="menu-join-our-e-news-list" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/enewslist/"><span class="menu-link-wrap"><strong>Join Our E-News List</strong></span></a></li>
</ul>
</li>
</ul><ul id="topmenu" class="sf-menu tablet-menu tablet-menu-portrait topmenu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Films &#038; Events</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Browse Films &#038; Events</strong></span></a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/schedule/"><span class="menu-link-wrap"><strong>View Schedule / Buy Tickets</strong></span></a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/updates/"><span class="menu-link-wrap"><strong>Program Updates</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/program-guide-mobile-apps/"><span class="menu-link-wrap"><strong>Program Guide &#038; Mobile Apps</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-tickets/"><span class="menu-link-wrap"><strong>Box Office &#038; Ticket Information</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/howtohiff/"><span class="menu-link-wrap"><strong>How to HIFF</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-faqs/"><span class="menu-link-wrap"><strong>Box Office &#038; Membership FAQs</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Filmmakers</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Call for Entries</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accreditation/"><span class="menu-link-wrap"><strong>Delegate &#038; Press Accreditation</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accredited-guest-delegate-on-the-ground-info/"><span class="menu-link-wrap"><strong>Accredited Guest / Delegate On-the-Ground Info</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/awards-jurors/"><span class="menu-link-wrap"><strong>Awards and Jurors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/logos-film-images/"><span class="menu-link-wrap"><strong>Logos, Laurels &#038; Film Images</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/key-visual/"><span class="menu-link-wrap"><strong>Key Visual</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/travel-hotels/"><span class="menu-link-wrap"><strong>Travel &#038; Hotel Information</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/sponsorship/"><span class="menu-link-wrap"><strong>Support</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/sponsorship/hiff-sponsors-spring-2017/"><span class="menu-link-wrap"><strong>HIFF Sponsors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/volunteer/"><span class="menu-link-wrap"><strong>Volunteer at HIFF</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/donate/"><span class="menu-link-wrap"><strong>Donate – Individual Giving</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/job-opportunities/"><span class="menu-link-wrap"><strong>Job Opportunities &#038; Internships</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cinema-partners/"><span class="menu-link-wrap"><strong>Cinema Partners</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/creative-lab/"><span class="menu-link-wrap"><strong>Creative Lab at HIFF</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/roger-ebert-foundation-young-film-critics-program/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF: Ebert Young Writers Program for the Arts</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/hiff-doc-lab-anatomy-of-oscar-winning-documentaries/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF Doc Lab: Anatomy of Oscar Winning Documentaries</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/"><span class="menu-link-wrap"><strong>Education Programs</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program</strong></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/cvlp-signup-form-oahu/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Oahu</strong></span></a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cvlp-signup-form-big-island/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Big Island</strong></span></a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/guest-filmmaker-program/"><span class="menu-link-wrap"><strong>Guest Filmmaker Program</strong></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form/"><span class="menu-link-wrap"><strong>GFP Signup Form (Oahu)</strong></span></a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form-kauai/"><span class="menu-link-wrap"><strong>GFP Signup Form (Kauai)</strong></span></a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/student-showcase/"><span class="menu-link-wrap"><strong>Student Showcase</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/kupuna-lens/"><span class="menu-link-wrap"><strong>Kupuna Lens Film Program</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About HIFF</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About the Organization</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/contact-hiff/"><span class="menu-link-wrap"><strong>Contact HIFF – Staff &#038; Board of Directors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/history/"><span class="menu-link-wrap"><strong>Festival History</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/enewslist/"><span class="menu-link-wrap"><strong>Join Our E-News List</strong></span></a></li>
</ul>
</li>
</ul><ul id="topmenu" class="sf-menu tablet-menu tablet-menu-landscape topmenu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Films &#038; Events</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Browse Films &#038; Events</strong></span></a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/schedule/"><span class="menu-link-wrap"><strong>View Schedule / Buy Tickets</strong></span></a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/updates/"><span class="menu-link-wrap"><strong>Program Updates</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/program-guide-mobile-apps/"><span class="menu-link-wrap"><strong>Program Guide &#038; Mobile Apps</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-tickets/"><span class="menu-link-wrap"><strong>Box Office &#038; Ticket Information</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/howtohiff/"><span class="menu-link-wrap"><strong>How to HIFF</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-faqs/"><span class="menu-link-wrap"><strong>Box Office &#038; Membership FAQs</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Filmmakers</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/industry/"><span class="menu-link-wrap"><strong>Call for Entries</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accreditation/"><span class="menu-link-wrap"><strong>Delegate &#038; Press Accreditation</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/accredited-guest-delegate-on-the-ground-info/"><span class="menu-link-wrap"><strong>Accredited Guest / Delegate On-the-Ground Info</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/awards-jurors/"><span class="menu-link-wrap"><strong>Awards and Jurors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/logos-film-images/"><span class="menu-link-wrap"><strong>Logos, Laurels &#038; Film Images</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/key-visual/"><span class="menu-link-wrap"><strong>Key Visual</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/travel-hotels/"><span class="menu-link-wrap"><strong>Travel &#038; Hotel Information</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/sponsorship/"><span class="menu-link-wrap"><strong>Support</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/sponsorship/hiff-sponsors-spring-2017/"><span class="menu-link-wrap"><strong>HIFF Sponsors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/volunteer/"><span class="menu-link-wrap"><strong>Volunteer at HIFF</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/donate/"><span class="menu-link-wrap"><strong>Donate – Individual Giving</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/job-opportunities/"><span class="menu-link-wrap"><strong>Job Opportunities &#038; Internships</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cinema-partners/"><span class="menu-link-wrap"><strong>Cinema Partners</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/creative-lab/"><span class="menu-link-wrap"><strong>Creative Lab at HIFF</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/roger-ebert-foundation-young-film-critics-program/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF: Ebert Young Writers Program for the Arts</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/hiff-doc-lab-anatomy-of-oscar-winning-documentaries/"><span class="menu-link-wrap"><strong>Creative Lab @ HIFF Doc Lab: Anatomy of Oscar Winning Documentaries</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/"><span class="menu-link-wrap"><strong>Education Programs</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program</strong></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/cultural-visual-literacy-program/cvlp-signup-form-oahu/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Oahu</strong></span></a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/cvlp-signup-form-big-island/"><span class="menu-link-wrap"><strong>Cultural &#038; Visual Literacy Program Signup Form – Big Island</strong></span></a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/education/guest-filmmaker-program/"><span class="menu-link-wrap"><strong>Guest Filmmaker Program</strong></span></a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form/"><span class="menu-link-wrap"><strong>GFP Signup Form (Oahu)</strong></span></a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/guest-filmmaker-program/gfp-signup-form-kauai/"><span class="menu-link-wrap"><strong>GFP Signup Form (Kauai)</strong></span></a></li>
	</ul>
</li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/student-showcase/"><span class="menu-link-wrap"><strong>Student Showcase</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/kupuna-lens/"><span class="menu-link-wrap"><strong>Kupuna Lens Film Program</strong></span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-parent-item"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About HIFF</strong></span></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About the Organization</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/contact-hiff/"><span class="menu-link-wrap"><strong>Contact HIFF – Staff &#038; Board of Directors</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/history/"><span class="menu-link-wrap"><strong>Festival History</strong></span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/enewslist/"><span class="menu-link-wrap"><strong>Join Our E-News List</strong></span></a></li>
</ul>
</li>
</ul>
					<div class="clear"></div>
					</div>
				</nav>

			<!-- </div>
			<div class="clear"></div>
		</div> -->
		<div id="mobile-menu-wrapper" class="mobile-menu-wrapper collapse">
				<div class="mobile-menu-tip"></div>
			   <ul id="topmenu" class="mobile-menu topmenu"><li id="menu-home" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="http://www.hiff.org/"><span class="menu-link-wrap"><strong>Home</strong></span></a></li>
<li id="menu-browse-films-events" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/films/"><span class="menu-link-wrap"><strong>Browse Films &#038; Events</strong></span></a></li>
<li id="menu-view-schedule-buy-tickets" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://program.hiff.org/schedule/"><span class="menu-link-wrap"><strong>View Schedule / Buy Tickets</strong></span></a></li>
<li id="menu-membership-flash-passes" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/membership/"><span class="menu-link-wrap"><strong>Membership &#038; Flash Passes</strong></span></a></li>
<li id="menu-box-office-ticket-information" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/filmsevents/box-office-tickets/"><span class="menu-link-wrap"><strong>Box Office &#038; Ticket Information</strong></span></a></li>
<li id="menu-hiff-sponsors-spring-2017" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/sponsorship/hiff-sponsors-spring-2017/"><span class="menu-link-wrap"><strong>HIFF Sponsors &#8211; Spring 2017</strong></span></a></li>
<li id="menu-creative-lab-at-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/creative-lab/"><span class="menu-link-wrap"><strong>Creative Lab at HIFF</strong></span></a></li>
<li id="menu-education" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/education/"><span class="menu-link-wrap"><strong>Education</strong></span></a></li>
<li id="menu-about-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/"><span class="menu-link-wrap"><strong>About HIFF</strong></span></a></li>
<li id="menu-photo-gallery" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/photo-gallery/"><span class="menu-link-wrap"><strong>Photo Gallery</strong></span></a></li>
<li id="menu-blog" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/blog/"><span class="menu-link-wrap"><strong>Blog</strong></span></a></li>
<li id="menu-news-press" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.hiff.org/content/pressreleases/"><span class="menu-link-wrap"><strong>News / Press</strong></span></a></li>
<li id="menu-donate" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/donate/"><span class="menu-link-wrap"><strong>Donate</strong></span></a></li>
<li id="menu-contact-hiff" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.hiff.org/about-hiff/contact-hiff/"><span class="menu-link-wrap"><strong>Contact HIFF</strong></span></a></li>
</ul>                  <div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="header-content mobile-header-content">
			
									</div>
	</header>

	
<div class="main " role="main">
	<div class="row-fluid clearfix">
		<div class="intro">
					</div>
	</div>

	<div class="content-wrapper">
		<div class="overlay-divider"></div>
		<div class="row-fluid clearfix page-container">
			<div class="span12">
				<!--PAGE CONTENT-->
				<div class="prime-page prime-full-width">

	  <!-- Start FestPro Content -->
	  <div id="main-wrapper">
		<div id="main" class="clearfix">
			<?php if ($admin != "LOGIN") { print "<div id=\"content-wrapper\">\n"; } ?>
