<?php
	date_default_timezone_set(FP_BASE_TIMEZONE);
	$currentDate = date("Y-m-d"); $currentDateTime = date("Y-m-d H:i:s");
	$press_kit_deadline = strtotime($upcoming[0]->startdate) - (55 * 86400);
	$film_print_deadline = strtotime($upcoming[0]->startdate) - (13 * 86400);

	$film_only1 = array('Feet'=>'Feet','Meters'=>'Meters');
	$film_only2 = array('N/A'=>'N/A','Reels'=>'Reels','Cores'=>'Cores');
	$yes_no_more = array(1=>'Yes',0=>'No',2=>'Tell Me More');
	
?>
<style>
.film_data_table { border: none; width: 100%; background-color:transparent; border-collapse:separate; }
.film_data_table tr { vertical-align: top; }

.film_data_table INPUT.text { margin-bottom:5px; width:318px; padding: .2em; }
.film_data_table INPUT.text-small { margin-bottom:5px; width:135px; padding: .2em; }
.film_data_table SELECT.select { margin-bottom:5px; width:143px; padding: .2em; background:none #FFF !important; }
.film_data_table SELECT.multiselect { margin-bottom:5px; width:140px; height:165px; padding: .2em; background:none #FFF !important; }
.film_data_table H3 { text-align:center; display:block; padding-bottom:3px;}

#addPersonnelForm label { width:auto; margin:0; }
#addPersonnelForm input { height:20px; margin:5px 0; }
#addPersonnelForm span { line-height:20px; height:auto;}
#addPersonnelForm th, #addPersonnelForm td { padding: 0; }
</style>

<div style="margin-top:2px;">&nbsp;</div>
<fieldset class="ui-corner-all">

			<h1 class="title">Film Data Sheet - <?php print $upcoming[0]->year." ".$upcoming[0]->name; ?></h1>
			<!--
			<div style="border: 1px dashed rgb(102, 102, 102); margin-left: 10px; float: right; width: 280px; padding: 5px; text-align:center" class="ui-corner-all">
			<h3>Important Deadlines</h3>
			<p><u>Press Kits Received</u><br /><?php print date("F j, Y", $press_kit_deadline); ?> <span class="basetext_sm">(8 weeks before start)</span></p>
			<p><u>Film Prints Received</u><br /><?php print date("F j, Y", $film_print_deadline); ?> <span class="basetext_sm">(2 weeks before start)</span></p>
			</div>
			-->
			<p align="justify">Welcome, filmmakers! If you have reached this page, your film entry has been accepted or your film has been invited for our film festival.</p>
			<p align="justify">Please fill out this film data sheet as completely as you can, as we cannot schedule your film to be shown without this information.
			Our <?php print $upcoming[0]->name; ?> will be held from <?php print date("F",strtotime($upcoming[0]->startdate))." ".date("d",strtotime($upcoming[0]->startdate))."-".date("d",strtotime($upcoming[0]->enddate)).", ".date("Y",strtotime($upcoming[0]->enddate)); ?>.<br clear="all"></p>

			<form id="filmEntryForm" name="film_entry" action="/filmmakers/thank_you" method="post">
			<?php
			print form_hidden('Imported','0');
		print form_hidden('festival_id',$upcoming[0]->id);
		print form_hidden('SubmittingDate',$currentDate);
		?>
		<input type="hidden" value="0" id="PersNumToAdd" name="PersNumToAdd">
			<h3 align="center">Primary Contact</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
					<td width="50%">Contact Name<span class="req"> *</span><br /><?php print form_input(array('name'=>'PressContact', 'id'=>'PressContact', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td width="50%">Telephone<br /><?php print form_input(array('name'=>'PressPhone', 'id'=>'PressPhone', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>Fax<br /><?php print form_input(array('name'=>'PressFax', 'id'=>'PressFax', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>Email<span class="req"> *</span><br /><?php print form_input(array('name'=>'PressEmail', 'id'=>'PressEmail', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td colspan="2">Website <span style="font-size:10pt;">(URL to an online press kit or images for your film)</span><br /><?php print form_input(array('name'=>'PressWebsite', 'id'=>'PressWebsite', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Technical Data</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
			 <td colspan="2">English Title of Film<span class="req"> *</span><br /><?php print form_input(array('name'=>'EnglishTitle', 'id'=>'EnglishTitle', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			 <td>Original Language Title<br /><?php print form_input(array('name'=>'OriginalLanguageTitle', 'id'=>'OriginalLanguageTitle', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			 <td>Run Time (in minutes)<span class="req"> *</span><br /><?php print form_input(array('name'=>'RunTime', 'id'=>'RunTime', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
			 <td width="26%" rowspan="3">Countries of Origin<br /><?php print form_multiselect('Country[]', $countries, '',"class='multiselect ui-widget-content ui-corner-all'"); ?></td>
			 <td width="25%" rowspan="3">Original Language(s)<br /><?php print form_multiselect('Language[]', $languages, '',"class='multiselect ui-widget-content ui-corner-all'"); ?></td>
			 <td width="27%" rowspan="3">Genre(s)<br /><?php print form_multiselect('Genre[]', $genres, '',"class='multiselect ui-widget-content ui-corner-all'"); ?></td>
			 <td width="22%">Year Completed<br /><?php print form_input(array('name'=>'Year', 'id'=>'Year', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
			 <td>Premiere Status<br /><?php print form_dropdown('PremiereStatus', $premiere, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
				</tr>
				<tr>
			 <td>Exhibition Format<br /><?php print form_dropdown('ExhibitionFormat', $format, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
				</tr>
				<tr>
			 <td colspan="3"><div style="font-size:10pt; text-align:center; font-weight:bold;">Hold Ctrl (PC) or Command (Mac) to select multiple countries, languages or genres.</div></td>
			 <td>Aspect Ratio<br /><?php print form_dropdown('AspectRatio', $aspectratio, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
				</tr>
				<tr>
			 <td>Sound<br /><?php print form_dropdown('Sound', $sound, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
			 <td>Color<br /><?php print form_dropdown('Color', $color, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
			 <td>Total Footage (Film)<br /><?php print form_input(array('name'=>'FootageLength', 'id'=>'FootageLength', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>&nbsp;<?php print form_dropdown('FootageMeasure', $film_only1, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
			 <td>Reels/Cores (Film)<br /><?php print form_dropdown('FootageType', $film_only2, '',"class='select ui-widget-content ui-corner-all'"); ?></td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Film Cast &amp; Crew</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="0">
			 <tbody style="border: none;">
				<tr>
				<td colspan="2">
				 <table cellspacing="0" cellpadding="0">
					<tbody style="border: none;">
					<tr><th>&nbsp;</th><th>First Name</th><th>Last Name</th><th>Role(s)</th></tr>
					<tr id="add-personnel-message">
						<td colspan="4" align="center"><b>Please add the cast &amp; crew of this film by clicking the 'Add Cast / Crew Member' button below.</b></td>
					</tr>
					<tr><td colspan="4" align="center"><button id="add-personnel">Add Cast / Crew Member</button></td></tr>
						</tbody>
				 </table>
			 </td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">General Information</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="0">
			 <tbody style="border: none;">
				<tr>
			 <td>Previous Screenings<br /><?php print form_input(array('name'=>'PreviousScreenings', 'id'=>'PreviousScreenings', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			 <td>Official Film or Distributor Website<br /><?php print form_input(array('name'=>'Website', 'id'=>'Website', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
			 <td colspan="2">
				 Film Synopsis<span class="req"> *</span> <span style="font-size:10pt;">(Please limit this to 75-100 words. Content may be edited for publication.)</span><br />
				 <?php print form_textarea(array('name'=>'Synopsis', 'id'=>'Synopsis', 'value'=>'', 'rows'=>'4', 'cols'=>'73', 'class'=>'text ui-widget-content ui-corner-all')); ?>
			 </td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Distribution / Print Source</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
			 <td width="50%">Current Distributor (Company Name)<br /><?php print form_input(array('name'=>'distributorName', 'id'=>'distributorName', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			 <td width="50%">Distributor Contact Name<br /><?php print form_input(array('name'=>'distributorType', 'id'=>'distributorType', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
			 <td>Address<br /><?php print form_input(array('name'=>'distributorAddress', 'id'=>'distributorAddress', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			 <td>Phone<br /><?php print form_input(array('name'=>'distributorPhone', 'id'=>'distributorPhone', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
			 <td>Fax<br /><?php print form_input(array('name'=>'distributorFax', 'id'=>'distributorFax', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			 <td>Email<br /><?php print form_input(array('name'=>'distributorEmail', 'id'=>'distributorEmail', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Insurance</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
			 <td>Value of Print for Insurance Purposes<span class="req"> *</span><br />$<?php print form_input(array('name'=>'InsuranceValue', 'id'=>'InsuranceValue', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?> <span style="font-size:10pt;">(U.S. Dollars, please no commas.)</span></td>
			 <td width="48%" class="text_justify" style="font-size: 10pt;">If the festival does not receive this Film Data Sheet with the value of the print indicated in U.S. dollars, then we will insure your film for $1,000 or the actual cash value, whichever amount is lower. The festival will not be responsible for any claim more than the stated amount.</td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Shipping</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr align="center">
			 <td width="50%"><b>Inbound (To Festival)</b></td>
			 <td width="50%"><b>Outbound (From Festival)</b></td>
				</tr>
				<tr>
					<td>Shipper Name<br /><?php print form_input(array('name'=>'Shipping_In_Name', 'id'=>'Shipping_In_Name', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>Shipper Name<br /><?php print form_input(array('name'=>'Shipping_Out_Name', 'id'=>'Shipping_Out_Name', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>Address<br /><?php print form_textarea(array('name'=>'Shipping_In_Address', 'id'=>'Shipping_In_Address', 'value'=>'', 'rows'=>'3', 'cols'=>'35', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>Address<br /><?php print form_textarea(array('name'=>'Shipping_Out_Address', 'id'=>'Shipping_Out_Address', 'value'=>'', 'rows'=>'3', 'cols'=>'35', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>Telephone<br /><?php print form_input(array('name'=>'Shipping_In_Phone', 'id'=>'Shipping_In_Phone', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>Telephone<br /><?php print form_input(array('name'=>'Shipping_Out_Phone', 'id'=>'Shipping_Out_Phone', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>Fax<br /><?php print form_input(array('name'=>'Shipping_In_Fax', 'id'=>'Shipping_In_Fax', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>Fax<br /><?php print form_input(array('name'=>'Shipping_Out_Fax', 'id'=>'Shipping_Out_Fax', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>E-mail<br /><?php print form_input(array('name'=>'Shipping_In_Email', 'id'=>'Shipping_In_Email', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
					<td>E-mail<br /><?php print form_input(array('name'=>'Shipping_Out_Email', 'id'=>'Shipping_Out_Email', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
				</tr>
				<tr>
					<td>Estimated Date of Arrival<br /><?php print form_input(array('name'=>'Shipping_In_Date', 'id'=>'Shipping_In_Date', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
					<td>Estimated Date of Arrival<br /><?php print form_input(array('name'=>'Shipping_Out_Date', 'id'=>'Shipping_Out_Date', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
				</tr>
			 </tbody>
			</table>

			<h3 align="center">Publicity Information</h3>
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
					<td width="50%">Hawaii Connections or Filming<br /><?php print form_textarea(array('name'=>'HawaiiConnections', 'id'=>'HawaiiConnections', 'value'=>'', 'rows'=>'4', 'cols'=>'35', 'class'=>'text ui-widget-content ui-corner-all')); ?></td><td width="50%"><span style="font-size: 10pt;">Does any person on the cast and crew have a connection to Hawaii, e.g., hometown, school, family?  Was any part of the movie filmed in Hawaii?  If so, please list in detail.</span><br /></td>
				</tr>
				<tr>
					<td>Festival Television Commercial<br /><?php print form_dropdown('TV_OK_ID', $yes_no_more, '',"class='select ui-widget-content ui-corner-all'"); ?></td><td><span style="font-size: 10pt;">Can the festival use clips from your film in the festival television public service announcements or commercials, which will be shown on Hawaii's local cable television stations?</span><br /></td>
				</tr>
				<tr>
					<td>Festival Radio Commercial<br /><?php print form_dropdown('Radio_OK_ID', $yes_no_more, '',"class='select ui-widget-content ui-corner-all'"); ?></td><td><span style="font-size: 10pt;">Can the festival use audio clips from your film in radio spots or commercials, which will be broadcast on local radio stations?</span><br /></td>
				</tr>
				<tr>
					<td>Festival VOD Channel<br /><?php print form_dropdown('VOD_OK_ID', $yes_no_more, '',"class='select ui-widget-content ui-corner-all'"); ?></td><td><span style="font-size: 10pt;">Can the festival use clips from your film on its Video-On-Demand Channel (Ch. 680) on the local Time Warner Cable system?</span><br /></td>
				</tr>
				<tr>
					<td>Festival Website<br /><?php print form_dropdown('Web_OK_ID', $yes_no_more, '',"class='select ui-widget-content ui-corner-all'"); ?></td><td><span style="font-size: 10pt;">Can the festival use clips from your film on its website, or a partner website?  You will retain all rights: The festival footage use will be non-exclusive, and streamed video only.</span><br /></td>
				</tr>
			 </tbody>
			</table><br />
			
			<table class="film_data_table" cellspacing="0" cellpadding="5">
			 <tbody style="border: none;">
				<tr>
					<td colspan="2">
						<div style="font-size: 10pt; text-align:justify;"><b>I acknowledge and agree as follows:</b> I have read, understood and complied with all eligibility requirements. To the best of my knowledge, all of the statements in this document are true. This film is not subject to litigation and is not threatened by any litigators.  I hold the Hawaii International Film Festival harmless from damage to or loss of the print en route to the Festival. I am duly authorized to submit this film to the Festival.</div>
					</td>
				</tr>
				<tr>
					<td>Date<br /><?php print date("m/d/Y",strtotime($currentDate)); ?></td>
					<td><!-- Name of Person Completing Form<span class="req"> *</span><br /><?php //print form_input(array('name'=>'SubmittingName', 'id'=>'SubmittingName', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?> --></td>
				</tr>
				<!--
			<tr>
			<td>Are You a Human?<br /><span style="font-size: 12px;">We are using a <a href="http://en.wikipedia.org/wiki/Captcha" target="_blank" title="Completely Automated Public Turing test to tell Computers and Humans Apart">CAPTCHA</a> here to reduce fake / spam entries of this form. It verifies that you're not a script or bot.</span></td>
			<td>
			<?php //print $recaptcha; ?>
			</td>
		</tr>
				-->
				<tr>
					<td colspan="2">
						<div align="center"><input type="Submit" name="Submit" value="Submit Film Data Sheet"></div>
					</td>
				</tr>
			 </tbody>
			</table>
			</form>
</fieldset>

<div id="addPersonnel-dialog" title="Add Cast &amp; Crew">
<form name="addPersonnelForm" id="addPersonnelForm">
	<input type="hidden" value="0" id="PersonnelNum" name="PersonnelNum">
		<table class="PersonnelDialog" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tbody style="border-top:none;">
				<tr valign="top">
						<td width="50%">
				<label for="pers-first-name-new">First Name</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'pers-first-name-new', 'id'=>'pers-first-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
						</td>
						<td width="50%" rowspan="4">
				<label for="pers-role-new">Role(s)</label> <span class="req">*</span><br />
				<?php print form_multiselect('pers-role-new[]', $personnel, '', "id='pers-role-new' class='multiselect ui-widget-content ui-corner-all'"); ?>
								<br /><span>Hold CTRL / COMMAND to select multiple roles</span>
						</td>
				</tr>
				<tr valign="top">
						<td>
				<label for="pers-last-name-new">Last Name</label> <span class="req">*</span><br />
				<?php print form_input(array('name'=>'pers-last-name-new', 'id'=>'pers-last-name-new', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?>
						</td>
				</tr>
				<tr valign="top">
						<td>
				<label for="pers-last-name-first-new">Last Name First?</label><br />
				<?php print form_checkbox('pers-last-name-first-new','1',false,' id="pers-last-name-first-new"'); ?>
						</td>
				</tr>
				<tr valign="top">
			<td><br /><br /><br /><br /></td>
				</tr>
				</tbody>
		</table>
</form>
</div>

<script type="text/javascript">
	$(function() {	
		<!-- Whole page -->
		$("input:submit").button();
		//$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		// validate film data sheet on keyup and submit
		$("#filmEntryForm").validate({
			rules: {
				EnglishTitle: "required",
				RunTime: { required:true, digits:true },
				Synopsis: "required",
				InsuranceValue: { required:true, digits:true },
				//SubmittingName: "required",
				Year: { digits:true, range:[1900,<?php print date("Y"); ?>] },
				Website: "url",
				distributorEmail: "email",
				Shipping_In_Email: "email",
				Shipping_Out_Email: "email",
				PressContact: "required",
				PressEmail: { required:true, email:true },
				PressWebsite: "url"
			},
			messages: {
				EnglishTitle: "Please enter an english title for your film.",
				RunTime: "Please enter the run time for your film. (Whole numbers only, round up any seconds)",
				Synopsis: "Please enter a synopsis for your film or a url to an online synopsis.",
				InsuranceValue: "Please enter a value for your film print.",
				//SubmittingName: "Please enter your name.",
				Year: "Please enter a 4 digit year.",
				Website: "Please enter a valid URL (including http://).",
				distributorEmail: "Please enter a valid email address.",
				Shipping_In_Email: "Please enter a valid email address.",
				Shipping_Out_Email: "Please enter a valid email address.",
				PressContact: "Please enter a primary contact name.",
				PressEmail: "Please enter a valid email address.",
				PressWebsite: "Please enter a valid URL (including http://)."
			}
		});

		// validate add personnel on keyup and submit
		$("#addPersonnelForm").validate({
			rules: {
				"pers-first-name-new": "required",
				"pers-last-name-new": "required",
				"pers-role-new[]": "required"
			},
			messages: {
				"pers-first-name-new": "Please enter a first name.",
				"pers-last-name-new": "Please enter a last name.",
				"pers-role-new[]": "Please select at least one role."
			}
		});

		$("#addPersonnel-dialog").dialog({
			autoOpen: false,
			height: 375,
			width: 350,
			modal: true,
			buttons: {
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Add Cast / Crew Member': function() {
					if ($("#addPersonnelForm").validate().form() == true) {
						// Add new personnel and update page
										$.ajax({
							success: function(msg, text, xhr) {
								$('#add-personnel').closest("tr").before(msg);
								$('#add-personnel-message').css('display','none');

								var total_num = $('#PersonnelNum').val();
								total_num++;

								$('#PersonnelNum').val(total_num);
								$('#PersNumToAdd').val(total_num);
								
								$('#pers-del-'+total_num).button();
								$('#pers-del-'+total_num).on('click', function() {
									// find current number to add and subtract 1
									var currentNum = $('#PersNumToAdd').val();
									currentNum--;
									$('#PersNumToAdd').val(currentNum);
									
									// remove the row
									$('#pers-del-'+total_num).closest("tr").remove();
									
									$(this).removeClass("ui-state-focus");
									return false;
								});
							},
							error: function(xhr, msg1, msg2){
								alert( "Failure! " + xhr + msg1 + msg2); },
							data: $('#addPersonnelForm').serialize(),
							url: '/filmmakers/add_personnel/',
							type: 'POST',
							dataType: 'html'
						}); 

						$(this).dialog('close');
								}
				}
			},
			close: function() {
				$("#addPersonnelForm INPUT[type='text']").each(function() { $(this).val(""); });
				$("#addPersonnelForm INPUT[type='checkbox']").each(function() { $(this).val(0); $(this).prop('checked',false); });
				$("#addPersonnelForm SELECT OPTION").each(function() { $(this).prop('selected',false); });
			}
		});

		$('#add-personnel').button().on('click', function() {
			$('#addPersonnel-dialog').dialog('open');
			$(this).removeClass("ui-state-focus");
			return false;
		});
		
		$('#Shipping_In_Date').datepicker();
		$('#Shipping_Out_Date').datepicker();

	});
</script>