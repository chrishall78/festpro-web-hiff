<?php

if (isset($activeType) && $activeType == "week") {
	printScheduleByWeek($full_schedule, $activeDay, "top");
} else if (isset($activeType) && $activeType == "day") {
	printScheduleByDay($full_schedule, $activeDay, "list", "top");
}

print "<br clear=\"all\" /><fieldset class=\"ui-corner-right\">";
print "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"schedule_list\"><tbody>";

$currentdate = time();
if (count($schedule) == 0 && strtotime($festival[0]->startdate) > $currentdate) {
	print "<tr><td align=\"center\">The full program &amp; schedule for the ".$festival[0]->year." ".$festival[0]->name." has not been released yet.</td></tr>";
} else if (count($schedule) == 0) {
	print "<tr><td align=\"center\">No schedule data was found at this time.</td></tr>";
}

// Print venue name for venue specific pages
if (isset($venue_name)) { print "<h2>".$venue_name."</h2>\n"; }

$previousDate = ""; $counter = 0;
$prevScrnDate = ""; $prevScrnTime = ""; $prevScrnMovie = ""; $prevScrnProgramName = "";
foreach ($schedule as $thisScreening) {
	
	// Check to see that Screening information does not match exactly the last entry (eliminate interlocked screenings from showing twice)
	if (($counter != 0) && ($thisScreening->date == $prevScrnDate) && ($thisScreening->time == $prevScrnTime) && ($thisScreening->movie_id == $prevScrnMovie) && ($thisScreening->program_name == $prevScrnProgramName) ) {
		// Display nothing	
	} else {
		$slug = "";
		$tickets_img = find_screening_status($thisScreening->date,$thisScreening->url,$thisScreening->Rush,$thisScreening->Free);

		$title = ""; $title_orig = ""; $year = ""; $runtime = ""; $film_id = "";
		foreach ($films as $thisFilm) {
			if ($thisFilm->movie_id == $thisScreening->movie_id) {
				if ($thisScreening->program_name != "") {
					$title = switch_title($thisScreening->program_name);
				} else {
					$title = switch_title($thisFilm->title_en);
					$title_orig =  $thisFilm->title;
				}
				$year = $thisFilm->year;
				$runtime = $thisFilm->runtime_int;
				$film_id = $thisFilm->movie_id;
				$slug = $thisFilm->slug;
				break;
			}
		}
		$country = "";
		foreach ($countries as $thisCountry) {
			if ($thisCountry->movie_id == $thisScreening->movie_id ) {
				$country .= $thisCountry->name.", ";
			}
		}
		$director = "";
		foreach ($personnel as $thisDirector) {
			$director .= find_personnel_name2($thisDirector, $thisScreening->movie_id, "1");
		} $director = rtrim($director,", ");

		$datetime = $thisScreening->date." ".$thisScreening->time;
		$time = date("g:i A", strtotime($datetime));
		if ($previousDate != $thisScreening->date) {
			print "\t<tr valign=\"top\">\n";
				if ($counter == 0) {
					print "\t\t<td colspan=\"4\"><h3>".date("l, F jS",strtotime($thisScreening->date))."</h3></td>\n";
				} else {
					print "\t\t<td colspan=\"4\"><br /><h3>".date("l, F jS",strtotime($thisScreening->date))."</h3></td>\n";
				}
			print "\t</tr>\n";
		}	$previousDate = $thisScreening->date;
	
		$location = $thisScreening->displayname;

		// Start table output
		print "\t<tr valign=\"top\"";
		if ($thisScreening->Published == 0) { print " class=\"unpublished\"";  }
		print ">\n";
		print "\t\t<td width=\"12%\"><span class=\"schedule_time\">".$time."</span></td>\n";
		print "\t\t<td width=\"64%\">";
		if ($thisScreening->program_name != "") {
			print "<a href=\"/films/program/".$thisScreening->program_slug."\" class=\"schedule_title\">".$thisScreening->program_name."</a>";
			print "<br />";
			print "Various Directors | ";
		} else {
			print "<a href=\"/films/detail/".$slug."\" class=\"schedule_title\">".$title."</a>";
			if ($title_orig != "") { print " (".$title_orig.")"; }
			print "<br />";
			print $director." ".$year." | ".trim($country,", ")." | ";
		}

		$runtime_total = 0;
		$film_array = explode(",",$thisScreening->program_movie_ids);
		foreach ($film_array as $thisFilmID) {
			foreach ($filmids as $thisFilm2) {
				if ($thisFilmID == $thisFilm2->movie_id) {
					$runtime_total += $thisFilm2->runtime_int;
				}
			}
		}
		print $runtime_total;

		print " min.</td>\n";
		print "\t\t<td width=\"12%\">".$location."</td>\n";
		print "\t\t<td width=\"12%\">".$tickets_img."</td>\n";  
		print "\t</tr>\n";	

		$film_array = explode(",",$thisScreening->program_movie_ids);
		if (count($film_array) > 1) {
			$filmlist = "";
			print "\t<tr valign=\"top\">\n";
			print "\t\t<td align=\"right\" style=\"line-height: 120%;\"></td>\n";
			print "\t\t<td colspan=\"3\" style=\"line-height: 120%;\">Plays with: ";
			foreach ($film_array as $thisFilmID2) {
				foreach ($filmids as $thisFilm2) {
					if ($thisFilmID2 == $thisFilm2->movie_id) {
						if ($thisScreening->program_name != "") {
							// Include first film in program if program name is defined
							$filmlist .= "<a href=\"/films/detail/".$thisFilm2->value."\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
						} else {
							// Exclude first film in program if program name is not defined
							if ($thisScreening->movie_id != $thisFilmID2) {
								$filmlist .= "<a href=\"/films/detail/".$thisFilm2->value."\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
							}
						}
					}
				}
			}
			$filmlist = trim($filmlist,", ");
			print $filmlist;
			print "\t\t</td>\n";
			print "\t</tr>\n";
		}
	}
	
	$prevScrnDate = $thisScreening->date;
	$prevScrnTime = $thisScreening->time;
	$prevScrnMovie = $thisScreening->movie_id;
	$prevScrnProgramName = $thisScreening->program_name;
	$counter++;
}

print "</tbody></table>";
print "</fieldset>";

if (isset($activeType) && $activeType == "week") {
	printScheduleByWeek($full_schedule, $activeDay, "bottom");
} else if (isset($activeType) && $activeType == "day") {
	printScheduleByDay($full_schedule, $activeDay, "list", "bottom");
}
?>