<?php print "<h1 class=\"title\">".$title."</h1>"; ?>

<style>
	.half { width:48%; margin: 0 0.5%; min-height: 125px; }
</style>

<div style="margin-top:2px;">&nbsp;</div>
<fieldset id="venue_list" class="ui-corner-all">
<?php
	foreach ($venues as $thisVenue) {
		$address = urlencode($thisVenue->address.", ".$thisVenue->city.", ".$thisVenue->state." ".$thisVenue->postal_code);
		print "<div class=\"half\">";
		print "<h3>".$thisVenue->name."</h3>";
		print "<p><a href=\"http://www.google.com/maps/preview#!q=".$address."\" target=\"_blank\">".$thisVenue->address."<br/>".$thisVenue->city.", ".$thisVenue->state." ".$thisVenue->postal_code."</a></p>";
		print "<p>".$thisVenue->phone."</p>";
		print "</div>";
	}
?>
</fieldset>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#venue_list').isotope({ itemSelector: '.half', 'layoutMode': 'fitRows' });
	});
</script>