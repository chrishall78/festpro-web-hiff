
        <?php if ($admin != "LOGIN") { print "</div><!-- /#content-wrapper -->\n"; } ?>
      </div><!-- /#main -->
    </div><!-- /#main-wrapper -->
    <!-- End FestPro Content -->

				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div><!-- /#wrap -->

</div><!-- /.main -->
	<div class="subfooter">Content and Design Copyright © 2009-2015 Hawaii International Film Festival</div>
	<script type="text/javascript">
		jQuery(window).resize(function() {
			if(jQuery('div.main.has-sidebar').length > 0 && jQuery(window).width() > 767 ) {
				var pageLength = jQuery('div.prime-page').height();
				var sidebarLength = jQuery('div#sidebar').height();
				if(pageLength < sidebarLength) {
					jQuery('div.prime-page').css('min-height', sidebarLength + 'px');
				}
			}
		});
	</script>


<!-- Start FestPro Scripts -->
<script type="text/javascript" language="javascript">
	$(document).ready(function() {

		$('a[rel*=facebox]').facebox();
<?php $active_url = explode("/",$_SERVER['REQUEST_URI']);
	if ($active_url[1] == "schedule") {
		print "\t\t$('#sched_list').on('change', function() { window.location = '/schedule/'; return false; });\n";
		print "\t\t$('#sched_grid').on('change', function() { window.location = '/schedule/grid/".$festival[0]->startdate."'; return false; });\n";
	}
?>

		$('#filter_schedule').on('change', function() { schedule_filter_redirect("filter", $("option:selected", this).val()); });
		$('#filter_schedule_grid').on('change', function() { schedule_filter_redirect("grid", $("option:selected", this).val()); });
		$('#filter_schedule_venue').on('change', function() { schedule_filter_redirect("venue", $("option:selected", this).val()); });

		$('#filter_section').on('change', function() { film_filter_redirect("section", $("option:selected", this).val()); });
		$('#filter_country').on('change', function() { film_filter_redirect("country", $("option:selected", this).val()); });
		$('#filter_genre').on('change', function() { film_filter_redirect("genre", $("option:selected", this).val()); });
		$('#filter_event').on('change', function() { film_filter_redirect("event", $("option:selected", this).val()); });
		//$('#filter_venue').on('change', function() { film_filter_redirect("venue", $("option:selected", this).val()); });
		
<?php if (count($allfestivals) > 1) { ?>
		$('#change_festival').on('change', function() { change_festival_redirect($("option:selected", this).val()); });
<?php } ?>

		$('#filmSearchButton').button();
		$('#filmSearchForm').on('submit', function() {
			if ($('#filmSearchURL').val() != "") { window.location = $('#filmSearchURL').val(); return false; }
			else { return true; }
		});

<?php if ($filmJSON != "[]") { ?>
		var filmJSON = <?php print $filmJSON; ?>;

		$('#filmSearch').autocomplete({
			minLength: 2,
			delay: 0,
			source: filmJSON,
			dataType: "json",
			cache: false,
			focus: function(event, ui) {
				$('#filmSearch').val(ui.item.label);
				return false;
			},
			select: function(event, ui) {
				$('#filmSearch').val(ui.item.label);
				if (ui.item.movie_id != 0) {
					$('#filmSearchURL').val('/films/detail/' + ui.item.value);
				} else {
					$('#filmSearchURL').val('/films/program/' + ui.item.value);					
				}
				$('#filmSearchForm').submit();
				return false;
			}
		})
		.data("ui-autocomplete")._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.label + "</a>" )
				.appendTo( ul );
		};
<?php } ?>
	});
</script>
<?php
	if (isset($logged_in)) {
		if ($logged_in == true) { include_once('nav_bar.php'); }		
	}
?>
<!-- End FestPro Scripts -->

</body>
</html>