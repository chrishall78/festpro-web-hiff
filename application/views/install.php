<?php
	require_once("admin/install_filmtypes.php");

	$filmtype_array = array("1" => "Add List Below", "2" => "Skip For Now");

	$country_array = convert_to_simple_array($wf_type_country);
	$language_array = convert_to_simple_array($wf_type_language);
	$genre_array = convert_to_simple_array($wf_type_genre);
	
	$aspectratio_array = convert_to_simple_array($wf_type_aspectratio);
	$color_array = convert_to_simple_array($wf_type_color);
	$courier_array = convert_to_simple_array($wf_type_courier);

	$distribution_array = convert_to_simple_array($wf_type_distribution);
	$event_array = convert_to_simple_array($wf_type_event);
	$premiere_array = convert_to_simple_array($wf_type_premiere);

	$videoformat_array = convert_to_simple_array($wf_type_format);
	$soundformat_array = convert_to_simple_array($wf_type_sound);

	$progress = 0;
	if ($step1 == true) { $progress = $progress + 15; }
	if ($step2 == true) { $progress = $progress + 15; }
	if ($step3 == true) { $progress = $progress + 15; }
	if ($step4 == true) { $progress = $progress + 15; }
	if ($step5 == true) { $progress = $progress + 26; }
	if ($step6 == true) { $progress = 100; }

?>

	<style type="text/css">
		.ui-progressbar { height:22px; }
		.ui-progressbar-value { background: url('/assets/css/admin/jquery-ui-theme/images/pbar-ani.gif'); }
	</style>
<script type="text/javascript" language="javascript">
function preview(img, selection) {
	if (!selection.width || !selection.height)
		return;

	var scaleX = 100 / selection.width;
	var scaleY = 100 / selection.height;

	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
}
</script>


<div style="width:702px; text-align:center;"><h2>FestPro Installation</h2></div><br />

<fieldset class="flexible ui-corner-all">

    <h3 align="center">Installation Progress</h3>
	<div id="progressbar"></div>
    <p>&nbsp;</p>


<?php
	if ($step7 == true) {
	    print "\t<div class=\"step7_results\">\n";
	} else {
	    print "\t<div class=\"step7_results\" style=\"display:none;\">\n";
	}
	print "\t\t<h3 align=\"center\">Installation is Complete</h3>\n";
	print "\t\t<p align=\"center\"><a href=\"/login/\">Please click here to go to the FestPro login form.</a></p>\n";
	print "\t</div>\n";


	if ($step6 == true) {
	    print "\t<div class=\"step6_results\">\n";
		print "\t\t<div align=\"center\">The film types have been added.</div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step6_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step6_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
?>
	<div class="step6_form" style="display:none;">
      <h3 align="center">Step 6: Define Film Settings</h3>
      <p align="center">You can choose to add pre-determined lists for these film settings, or skip it and add your own values later.</p>

        <form id="addFilmtypeForm" name="addFilmtypeForm">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td width="33%"><label for="country-new">Countries</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('country-new', $filmtype_array, 0, "id='country-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('country-list', $country_array, 0, "size='6' id='country-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="language-new">Languages</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('language-new', $filmtype_array, 0, "id='language-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('language-list', $language_array, 0, "size='6' id='language-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="genre-new">Genres</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('genre-new', $filmtype_array, 0, "id='genre-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('genre-list', $genre_array, 0, "size='6' id='genre-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
            </tr>
            <tr valign="top">
                <td width="33%"><label for="aspectratio-new">Aspect Ratio</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('aspectratio-new', $filmtype_array, 0, "id='aspectratio-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('aspectratio-list', $aspectratio_array, 0, "size='3' id='aspectratio-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="color-new">Color</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('color-new', $filmtype_array, 0, "id='color-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('color-list', $color_array, 0, "size='3' id='color-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="distribution-new">Distribution Types</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('distribution-new', $filmtype_array, 0, "id='distribution-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('distribution-list', $distribution_array, 0, "size='3' id='distribution-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
            </tr>
            <tr valign="top">
                <td width="33%"><label for="event-new">Event Types</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('event-new', $filmtype_array, 0, "id='event-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('event-list', $event_array, 0, "size='3' id='event-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="courier-new">Package Couriers</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('courier-new', $filmtype_array, 0, "id='courier-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('courier-list', $courier_array, 0, "size='3' id='courier-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="premiere-new">Premiere Status</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('premiere-new', $filmtype_array, 0, "id='premiere-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('premiere-list', $premiere_array, 0, "size='3' id='premiere-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
            </tr>
            <tr valign="top">
                <td width="33%"><label for="soundformat-new">Sound Format</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('soundformat-new', $filmtype_array, 0, "id='soundformat-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('soundformat-list', $soundformat_array, 0, "size='3' id='soundformat-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%"><label for="videoformat-new">Video Format</label><span class="req"> *</span><br />
                <?php
                	print form_dropdown('videoformat-new', $filmtype_array, 0, "id='videoformat-new' class='select-large ui-widget-content ui-corner-all'")."<br />";
					print form_dropdown('videoformat-list', $videoformat_array, 0, "size='3' id='videoformat-list' class='select-large ui-widget-content ui-corner-all'");
				?>
                </td>
                <td width="33%" align="center"><br /><br />
                <?php print form_submit('filmtypes-add', 'Add Film Types'); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
	</div>

<?php
	}

	if ($step5 == true) {
	    print "\t<div class=\"step5b_results\">\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step5_results\">\n";
		print "\t\t<div align=\"center\">Here is your default photo:<br /><img src=\"/".$photo_path."\" width=\"525\" height=\"300\" border=\"0\" /></div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step5_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step5b_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step5_results\" style=\"display:none;\">\n";
?>
      <h3 align="center">Step 5b: Crop Photo</h3>
      <p align="center">Choose how you would like your photo to be cropped.</p>
        	<form name="addPhotoForm2" id="addPhotoForm2">
            	<div id="photo-crop-preview" class="ui-corner-all">
					<img id="photo-crop-image" src="/assets/images/transparent.png" width="525" border="0" style="float:left; margin-right:10px;">
                	<!-- Sample Data, this should get replaced by the uploaded photo -->
                    <div id="photo-crop-data">                    
					<input type='hidden' name='x1' value='0' id='x1' />
					<input type='hidden' name='y1' value='0' id='y1' />
					<input type='hidden' name='w' value='525' id='w' />
					<input type='hidden' name='h' value='300' id='h' />
					<input type='hidden' name='orig-filename1' id='orig-filename1' value='' />
					<input type='hidden' name='photo-new-id' value='0' />
					<input type='hidden' name='photo-new-path' value='0' />
                    </div>
                    <!-- End Sample Data -->
                    <?php print form_submit('photo-crop', 'Crop Photo'); ?>
                    <br clear="all" /><br />
                </div>
        	</form>
<?php
	    print "\t</div>\n";
?>
	<div class="step5_form" style="display:none;">
      <h3 align="center">Step 5: Upload a Default Photo</h3>      
      <p align="center">This photo will be used if there is no photo provided for a film/event. Consider adding your organization's logo or festival artwork. The photo will be resized and cropped to 525 x 300 pixels.</p>
      
        <form name="addPhotoForm" id="addPhotoForm" enctype="multipart/form-data" target="uploadPhotoFrame" method="post" action="/install/upload_photo/">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td width="65%">
                    <label for="photo-upload-new">Attach a Photo</label> <span class="req">*</span><br />
                    <?php print form_upload(array('name'=>'photo-upload-new', 'id'=>'photo-upload-new', 'value'=>'', 'class'=>'text-upload', 'size'=>'55')); ?>
                </td><td><br />
                	<?php print form_submit('photo-upload', 'Upload Photo', ' id="photo-upload"'); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>

		<div id="photoIframe">
			<iframe width="1" height="1" id="uploadPhotoFrame" name="uploadPhotoFrame" src="about:blank" frameborder="0"></iframe>
		</div>
	</div>

<?php
	}

	if ($step4 == true) {
	    print "\t<div class=\"step4_results\">\n";
		print "\t\t<div align=\"center\">The <b>".$sections[0]->name."</b> section has been created. More can be added once installation is completed.</div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step4_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step4_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
?>
	<div class="step4_form" style="display:none;">
        <h3 align="center">Step 4: Add a Section (Category)</h3>
        <p align="center">More sections can be added once you have completed installation.</p>
          
        <form id="addSectionForm" name="addSectionForm">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td width="25%"><label for="section-new">Section Name</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'section-new', 'id'=>'section-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="25%"><label for="sec-festival-new">Applies to</label><span class="req"> *</span><br />
                <?php print form_dropdown('sec-festival-new', $festivals_array, 0, "id='sec-festival-new' class='select ui-widget-content ui-corner-all'"); ?>
                </td>
                <td width="40%" rowspan="2"><label for="description-new">Description</label><br />
                <?php print form_textarea(array('name'=>'description-new', 'id'=>'description-new', 'value'=>'', 'rows'=>'4', 'cols'=>'40', 'class'=>'text ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="10%" rowspan="2"><br />
                <?php print form_submit('section-add', 'Add'); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

<?php
	}

	if ($step3 == true) {
	    print "\t<div class=\"step3_results\">\n";
		print "\t\t<div align=\"center\">The <b>".$locations[0]->name."</b> location has been created and added to the previously created festival.</div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step3_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step3_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
?>
	<div class="step3_form" style="display:none;">
        <h3 align="center">Step 3: Add a Screening Location</h3>
        <p align="center">More screening locations can be added once you have completed installation.</p>  

        <form id="addLocationForm" name="addLocationForm">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td width="25%"><label for="screeninglocation-new">Location Name</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'screeninglocation-new', 'id'=>'screeninglocation-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="25%"><label for="displayname-new">Audience Display Name</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'displayname-new', 'id'=>'displayname-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="25%"><label for="seats-new"># of Seats</label><br />
                <?php print form_input(array('name'=>'seats-new', 'id'=>'seats-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="10%"><br />
                <?php print form_submit(array('name'=>'screeninglocation-add', 'id'=>'screeninglocation-add', 'value'=>'Add')); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

<?php
	}

	if ($step2 == true) {
	    print "\t<div class=\"step2_results\">\n";
		print "\t\t<div align=\"center\">A festival for <b>".$festivals[0]->year." ".$festivals[0]->name."</b> has been created, running from <b>".date("m/d/Y", strtotime($festivals[0]->startdate))." to ".date("m/d/Y", strtotime($festivals[0]->enddate))."</b>.</div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step2_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step2_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
?>
	<div class="step2_form" style="display:none;">
        <h3 align="center">Step 2: Create a Festival</h3>

        <form id="addFestivalForm" name="addFestivalForm">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td width="20%"><label for="festival-new">Festival Name</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'festival-new', 'id'=>'festival-new', 'class'=>'text ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="20%"><label for="year-new">Year</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'year-new', 'id'=>'year-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="20%"><label for="startdate-new">Start Date</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'startdate-new', 'id'=>'startdate-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="20%"><label for="enddate-new">End Date</label><span class="req"> *</span><br />
                <?php print form_input(array('name'=>'enddate-new', 'id'=>'enddate-new', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
                </td>
                <td width="10%"><br />
                <?php print form_submit(array('name'=>'festival-add', 'id'=>'festival-add', 'value'=>'Add')); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>
    </div>

<?php
	}

	if ($step1 == true) {
	    print "\t<div class=\"step1_results\">\n";
		print "\t\t<div align=\"center\">Welcome to FestPro, <b>".$users[1]->FirstName." ".$users[1]->LastName."</b>. Your username is <b>".$users[1]->Username."</b>, email address is <b>".$users[1]->Email."</b>.</div>\n";
	    print "\t</div>\n";
	    print "\t<div class=\"step1_form\">\n";
	    print "\t</div>\n";
	} else {
	    print "\t<div class=\"step1_results\" style=\"display:none;\">\n";
	    print "\t</div>\n";
?>
	<div class="step1_form">
        <h3 align="center">Step 1: Create a User</h3>

        <form name="addUserForm" id="addUserForm">
        <table class="installTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody style="border-top:none;">
            <tr valign="top">
                <td><label for="FirstName">First Name</label><span class="req"> *</span><br /><input type="text" name="FirstName" id="FirstName" class="text ui-widget-content ui-corner-all" /></td>
                <td><label for="LastName">Last Name</label><span class="req"> *</span><br /><input type="text" name="LastName" id="LastName" class="text ui-widget-content ui-corner-all" /></td>
                <td><label for="Email">Email Address</label><span class="req"> *</span><br /><input type="text" name="Email" id="Email" class="text ui-widget-content ui-corner-all" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr valign="top">
                <td><label for="Username">Username</label><span class="req"> *</span><br /><input type="text" name="Username" id="Username" class="text ui-widget-content ui-corner-all" /></td>
                <td><label for="Password1">Password</label><span class="req"> *</span><br /><input type="password" name="Password1" id="Password1" class="text ui-widget-content ui-corner-all" /></td>
                <td><label for="Password2">Confirm Password</label><span class="req"> *</span><br /><input type="password" name="Password2" id="Password2" class="text ui-widget-content ui-corner-all" /></td>
                <td><br />
                <?php print form_submit(array('name'=>'user-add', 'id'=>'user-add', 'value'=>'Add')); ?>
                </td>
            </tr>
            </tbody>
        </table>
        </form>    
    </div>
<?php
	}
?>

</fieldset>



<script type="text/javascript">
	$(function() {	
		$("input:submit").button();
		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		$("#progressbar").progressbar({ value: 1 });

<?php	
	if ($progress != 0) { print "\t\t$(\"#progressbar\").progressbar({ value: ".$progress." });\n"; }
	if ($step1 == true) { print "\t\t$('.step2_form').css('display','block');\n"; }
	if ($step2 == true) { print "\t\t$('.step3_form').css('display','block');\n"; }
	if ($step3 == true) { print "\t\t$('.step4_form').css('display','block');\n"; }
	if ($step4 == true) { print "\t\t$('.step5_form').css('display','block');\n"; }
	if ($step5 == true) { print "\t\t$('.step6_form').css('display','block');\n"; }
	if ($step6 == true) { print "\t\t$('.step7_results').css('display','block');\n"; }


	if ($step1 == false) {
?>
		$("#addUserForm").submit(function() {
			if ($("#addUserForm").validate().form() == true) {
				$.ajax({
					success: function(msg, text, xhr) {
						//alert(msg);
						$('.step1_results').html(msg);
						$('.step1_form').slideUp();
						$("#addUserForm INPUT[type=text]").each(function() { $(this).val(""); });
						$("#addUserForm INPUT[type=password]").each(function() { $(this).val(""); });
						$('.step1_results').slideDown();
						$('.step2_form').slideDown();
						$("#progressbar").progressbar({ value: 15 });

					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#addUserForm').serialize(),
					url: '/install/add_user/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
			$(this).removeClass("ui-state-focus"); return false;
		});

		// validate add user form on keyup and submit
		$("#addUserForm").validate({
			rules: { "FirstName": "required", "LastName": "required", "Email": { required:true, email:true }, "Username": "required", "Password1": { required:true, minlength:8 }, "Password2": { required:true, equalTo:"#Password1" } },
			messages: { "FirstName": "Please enter a first name.", "LastName": "Please enter a last name.", "Email": "Please enter a valid email address.", "Username": "Please enter a username.", "Password1": { required:"Please enter a password.", minlength:"Your password must be at least 8 characters long." }, "Password2": { required:"Please verify your password.", equalTo:"The two passwords do not match." } }
		});

<?php
	}
	
	if ($step2 == false) {
?>
		$("#startdate-new").datepicker();
		$("#enddate-new").datepicker();

		$("#addFestivalForm").submit(function() {
			if ($("#addFestivalForm").validate().form() == true) {
				$.ajax({
					success: function(msg, text, xhr) {
						//alert(msg);
						$('.step2_results').html(msg);
						$('.step2_form').slideUp();
						$("#addFestivalForm INPUT[type=text]").each(function() { $(this).val(""); });
						$('.step2_results').slideDown();
						$('.step3_form').slideDown();
						$("#progressbar").progressbar({ value: 30 });
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#addFestivalForm').serialize(),
					url: '/install/add_festival/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
			$(this).removeClass("ui-state-focus"); return false;
		});

		// validate add festival form on keyup and submit
		$("#addFestivalForm").validate({
			rules: { "festival-new": "required", "year-new": { required:true, minlength:4, digits:true }, "startdate-new": { required:true, date:true }, "enddate-new": { required:true, date:true, greaterThan: "#startdate-new" }, "Password1": { required:true, minlength:8 }, "Password2": { required:true, equalTo:"#Password1" } },
			messages: { "festival-new": "Please enter a festival name.", "year-new": { required:"Please enter a year.", minlength:"Year must be at least 4 digits.", digits:"Only numbers are allowed in this field." }, "startdate-new": { required:"Please enter a start date.", date:"Please enter a valid date." }, "enddate-new": { required:"Please enter an end date.", date:"Please enter a valid date." } }
		});

<?php
	}
	
	if ($step3 == false) {
?>
		$("#addLocationForm").submit(function() {
			if ($("#addLocationForm").validate().form() == true) {
				$.ajax({
					success: function(msg, text, xhr) {
						//alert(msg);
						$('.step3_results').html(msg);
						$('.step3_form').slideUp();
						$("#addLocationForm INPUT[type=text]").each(function() { $(this).val(""); });
						$('.step3_results').slideDown();
						$('.step4_form').slideDown();
						$("#progressbar").progressbar({ value: 45 });
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#addLocationForm').serialize(),
					url: '/install/add_location/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
			$(this).removeClass("ui-state-focus"); return false;
		});

		// validate add screening location form on keyup and submit
		$("#addLocationForm").validate({
			rules: { "screeninglocation-new": "required", "displayname-new": "required", "seats-new": "digits" },
			messages: { "screeninglocation-new": "Please enter a location name.", "displayname-new": "Please enter a display name.", "seats-new": "Whole numbers only." }
		});

<?php
	}
	
	if ($step4 == false) {
?>
		$("#addSectionForm").submit(function() {
			if ($("#addSectionForm").validate().form() == true) {
				$.ajax({
					success: function(msg, text, xhr) {
						//alert(msg);
						$('.step4_results').html(msg);
						$('.step4_form').slideUp();
						$("#addSectionForm INPUT[type=text]").each(function() { $(this).val(""); });
						$("#addSectionForm TEXTAREA").each(function() { $(this).val(""); });
						$('.step4_results').slideDown();
						$('.step5_form').slideDown();
						$("#progressbar").progressbar({ value: 60 });
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#addSectionForm').serialize(),
					url: '/install/add_section/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
			$(this).removeClass("ui-state-focus"); return false;
		});


		// validate add section form on keyup and submit
		$("#addSectionForm").validate({
			rules: { "section-new": "required", "sec-festival-new": "required" },
			messages: { "section-new": "Please enter a section name.", "sec-festival-new": "Please select a festival." }
		});

<?php
	}
	
	if ($step5 == false) {
?>

		$("#photo-upload").on('click', function() {
			if ($("#addPhotoForm").validate().form() == true) {
				$("#addPhotoForm").submit();
				$('#photo-crop-image').attr('width','525').attr('src','/assets/images/loadingAnimation.gif');
				
				$.frameReady(function() {
					$('#photo-crop-data', window.parent.document).html( $("#photo-crop-data").html() );
	
					if ($("#photo-crop-data").html() == "The filetype you are attempting to upload is not allowed. Please press the 'Cancel' button and try again.") {
						$('#photo-crop-image', window.parent.document).css('display','none');
					} else {
						$('#photo-crop-image', window.parent.document).attr('src',$('#photo-new-path').val());
					}
	
				}, "top.uploadPhotoFrame");

				$('.step5_form').slideUp();
				$('.step5_results').slideDown();
				$("#progressbar").progressbar({ value: 73 });

				$('#photo-crop-image').imgAreaSelect({ hide:false, zIndex:1100, minWidth:525, minHeight:300, aspectRatio: '21:12', x1:0, y1:0, x2:525, y2:300, handles: true, onInit: preview, onSelectChange: preview });	
			}
		});

		$("#addPhotoForm2").submit(function() {
			$.ajax({
				success: function(msg, text, xhr) {
					//alert(msg);
					$('#photo-crop-image').imgAreaSelect({ hide:true });
					$('.step5b_results').html(msg);
					$('.step5b_results').slideDown();
					$('.step5_results').slideUp();
					$('.step6_form').slideDown();
					$("#progressbar").progressbar({ value: 86 });
				},
				error: function(xhr, msg1, msg2){
					alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#addFilmtypeForm').serialize(),
				url: '/install/upload_photo_crop/',
				type: 'POST',
				dataType: 'html'
			}); 
			$(this).removeClass("ui-state-focus"); return false;
		});

		// validate add filmtypes form on keyup and submit
		$("#addPhotoForm").validate({
			rules: { "photo-upload-new": {required:true, accept:"png|jp?g|gif" } },
			messages: { "photo-upload-new": {required:"Please select a photo to upload.", accept:"Only JPG, PNG or GIF images are allowed." } }
		});

<?php
	}
	
	if ($step6 == false) {
?>

		$("#addFilmtypeForm").submit(function() {
			if ($("#addFilmtypeForm").validate().form() == true) {
				$.ajax({
					success: function(msg, text, xhr) {
						//alert(msg);
						$('.step6_results').html(msg);
						$('.step6_form').slideUp();
						$('.step6_results').slideDown();
						$('.step7_results').slideDown();
						$("#progressbar").progressbar({ value: 100 });
					},
					error: function(xhr, msg1, msg2){
						alert( "Failure! " + xhr + msg1 + msg2); },
					data: $('#addFilmtypeForm').serialize(),
					url: '/install/add_filmtypes/',
					type: 'POST',
					dataType: 'html'
				}); 
			}
			$(this).removeClass("ui-state-focus"); return false;
		});

		// validate add filmtypes form on keyup and submit
		$("#addFilmtypeForm").validate({
			rules: { "country-new": "required", "language-new": "required", "genre-new": "required", "aspectratio-new": "required", "color-new": "required", "distribution-new": "required", "event-new": "required", "courier-new": "required", "premiere-new": "required", "soundformat-new": "required", "videoformat-new": "required" },
			messages: { "country-new": "Please select an option.", "language-new": "Please select an option.", "genre-new": "Please select an option.", "aspectratio-new": "Please select an option.", "color-new": "Please select an option.", "distribution-new": "Please select an option.", "event-new": "Please select an option.", "courier-new": "Please select an option.", "premiere-new": "Please select an option.", "soundformat-new": "Please select an option.", "videoformat-new": "Please select an option." }
		});

<?php
	}
?>

		/*
		var pGress = setInterval(function() {
			var pVal = $('#progressbar').progressbar('option', 'value');
			var pCnt = !isNaN(pVal) ? (pVal + 1) : 1;
			if (pCnt > 100) {
				clearInterval(pGress);
			} else {
				$('#progressbar').progressbar({value: pCnt , easing: 'easeInOutQuint'});
			}
		},100);
		*/
	});
</script>
