<?php
	date_default_timezone_set(FP_BASE_TIMEZONE);
	$currentDate = date("Y-m-d"); $currentDateTime = date("Y-m-d H:i:s");
	$delegate_type = array('0'=>'--');
	$travel_method = array(''=>'--','Local'=>'Local (No Travel)','Flying from USA'=>'Flying from USA','Flying from International'=>'Flying from International');
	$yes_no = array(0=>'No',1=>'Yes');
	$airlines_array = convert_to_array($airlines);
	$translator_lang = convert_to_array($all_languages);
	$festival_dates = date("F",strtotime($upcoming[0]->startdate))." ".date("d",strtotime($upcoming[0]->startdate))."-".date("d",strtotime($upcoming[0]->enddate)).", ".date("Y",strtotime($upcoming[0]->enddate));

	if( count($deadlines_fees) > 0) {
		$early_deadline = strtotime($deadlines_fees[0]->early_deadline);
		$late_deadline = strtotime($deadlines_fees[0]->late_deadline);
		$final_deadline = strtotime($deadlines_fees[0]->final_deadline);
	} else {
		$early_deadline = $late_deadline = $final_deadline = 0;
	}

?>

	<style type="text/css">
		.accred_data_table { border: none; width: 100%; background-color:transparent; border-collapse:separate; }
		.accred_data_table tr { vertical-align: top; }
		.accred_data_table FIELDSET { border: 1px solid rgb(102, 102, 102); background-color:#FFFFFF; min-height:120px; min-width:670px; width:670px; }

		.accred_data_table INPUT.text { margin-bottom:5px; width:322px; padding: .2em; }
		.accred_data_table INPUT.text-small { margin-bottom:5px; width:150px; padding: .2em; }
		.accred_data_table INPUT.text-smaller { margin-bottom:5px; width:100px; padding: .2em; }
		.accred_data_table TEXTAREA.text-full { width:670px; height:100px; }
		.accred_data_table SELECT.select { margin-bottom:5px; width:330px; padding: .2em; background:none #FFF; }
		.accred_data_table SELECT.select-small { margin-bottom:5px; width:155px; padding: .2em; background:none #FFF; }
		.accred_data_table SELECT.multiselect { margin-bottom:5px; width:140px; height:165px; padding: .2em; background:none #FFF; }
		.accred_data_table H3 { text-align:center; display:block; padding-bottom:3px;}
		.accred_data_table LABEL { font-weight: bold; }

		.fee_table { border: none; background-color:#FFFFFF; width:100%; color:#000000; font-size:10pt; margin:0; }
		.fee_table tbody { border:none; }
		.fee_table tr { vertical-align: top; text-align:left; }
		.fee_table td { padding: 3px; border: none; }
		.fee_table th { padding: 3px; border-bottom: 1px solid rgb(102, 102, 102);}
		.fee_table td.before { color:#000000; text-decoration:none; }
		.fee_table td.after { color:#FF0000; text-decoration:line-through; }
		
		#deadline_box { border: 1px dashed rgb(102, 102, 102); background-color:#FFFFFF; margin: 0 5px 0 10px; float: right; width: 200px; padding: 5px; }
		#deadline_box P { margin: 6px 0; }
		.deadline_label { display:inline-block; width:40px; margin:0 0 0 13%; text-decoration:underline; }
		
		#airport_info { border: 1px dashed #521F00; background-color:#FFFFFF; padding: 10px; position: absolute; z-index: 1000; left: 536px; }
		#airport_info h2 { font-size: 1em; text-transform: none; margin: 0px 5px; color: rgb(204, 0, 0); }
		#airport_info p { font-size: 10pt; }

		FIELDSET P { margin: 0 5px 20px; }
	</style>

<?php

if( count($deadlines_fees) == 0) {
	print "<div style=\"margin-top:2px;\">&nbsp;</div>\n";
	print "<fieldset class=\"ui-corner-all\">\n";
	print "<h1 class=\"title\">".$title."</h1>";
	print "<p>There is no delegate accreditation for ".$upcoming[0]->year." ".$upcoming[0]->name.".</p>";
	print "</fieldset>\n";	
} else {

?>

<div style="margin-top:2px;">&nbsp;</div>
<fieldset class="ui-corner-all">

	  <h1 class="title"><?php print $title; ?></h1>
	  <!--
	  <div id="deadline_box" class="ui-corner-all">
		<h3 style="text-align:center;">Important Deadlines</h3>
		<p><span class="deadline_label">Early</span><?php print date("F j, Y", $early_deadline); ?></p>
		<p><span class="deadline_label">Late</span><?php print date("F j, Y", $late_deadline); ?></p>
		<p><span class="deadline_label">Final</span><?php print date("F j, Y", $final_deadline); ?></p>
	  </div>
	  -->
	<p>Welcome, delegates! Please fill out this application as completely as you can (fields with an asterisk are required). You will then receive either a confirmation, a request for payment, or be declined.</p>
	<p>In order to be considered an accredited delegate, you must be at least one of the following:</p>
	<ol style="padding: 0 0 1.5em 2em;">
		<li>A filmmaker with a film in this year’s festival</li>
		<li>A participant in any of our seminars or jury</li>
		<li>Member of the press</li>
		<li>An invited guest</li>
	</ol>
	<p><strong>For feature-length films in the festival, we can extend two complimentary accreditations per film, and one complimentary accreditation for short films.</strong> All others can gain accreditation at the below rates, subject to approval of delegate status. Our HIFF Fall Festival will be held from November 2 – 12, 2017.</p>
	<p><strong>Industry delegates who do not qualify for complimentary accreditation:</strong> Please submit your $150 payment by clicking the button below after submitting this form.</a></p>

	<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="732c8c70-c81e-4036-a827-5555eeb5d46f" /> <input type = "image" src ="//content.authorize.net/images/buy-now-blue.gif" /> </form>

	<!-- <p>Our <?php print $upcoming[0]->name; ?> will be held from <?php print $festival_dates; ?>.</p> -->


<?php 
	if(strtotime($currentDate) > $final_deadline) {
		print "<p align=\"justify\" style=\"color:red;\"><b>Please note:</b> The final deadline for accreditation has now passed.</p>";
	} else {
?>

	  <form name="guest_entry" id="guestEntryForm" action="/accreditation/thank_you" method="post" enctype="multipart/form-data">
	  <?php
		print form_hidden('festival_id',$upcoming[0]->id);
		print "<input type=\"hidden\" name=\"Fee\" id=\"Fee\" value=\"0\" />\n";
	  ?>

	  <h3 align="center">Delegate Types &amp; Fees</h3>
	  <table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
		  <td colspan="2" align="center">
			<fieldset class="ui-corner-all">
			<table class="fee_table">
			   <tr>
				 <th width="20%"><b>Delegate Type</b></th>
				 <!-- <th width="20%"><b>Early Deadline</b><br /><?php print date("F j, Y", $early_deadline); ?></th> -->
				 <!-- <th width="20%"><b>Regular Deadline</b><br /><?php print date("F j, Y", $late_deadline); ?></th> -->
				 <th width="80%"><b>Final Deadline</b><br /><?php print date("F j, Y", $final_deadline); ?></th>
			   </tr>
<?php
		foreach ($deadlines_fees as $thisGuestType) {
			($currentDate <= $early_deadline) ? $early_class = "before" : $early_class = "after"; 
			($currentDate <= $late_deadline) ? $late_class = "before" : $late_class = "after"; 
			($currentDate <= $final_deadline) ? $final_class = "before" : $final_class = "after"; 

			if (strtotime($currentDate) <= $early_deadline) {
				$delegate_type[$thisGuestType->guesttype_id] = $thisGuestType->guesttype_name; //." - $".$thisGuestType->early_fee;
			} elseif (strtotime($currentDate) <= $late_deadline) {
				$delegate_type[$thisGuestType->guesttype_id] = $thisGuestType->guesttype_name; //." - $".$thisGuestType->late_fee;
			} elseif (strtotime($currentDate) <= $final_deadline) {
				$delegate_type[$thisGuestType->guesttype_id] = $thisGuestType->guesttype_name; //." - $".$thisGuestType->final_fee;
			} elseif (strtotime($currentDate) > $final_deadline) {
				$delegate_type[$thisGuestType->guesttype_id] = $thisGuestType->guesttype_name; //." - $".$thisGuestType->final_fee;
			}

			print "\t\t\t<tr>\n";
			print "\t\t\t\t<td align=\"left\"><b>".$thisGuestType->guesttype_name."</b></td>\n";
			//print "\t\t\t\t<td class=\"".$early_class."\">$".$thisGuestType->early_fee."</td>\n";
			//print "\t\t\t\t<td class=\"".$late_class."\">$".$thisGuestType->late_fee."</td>\n";
			print "\t\t\t\t<td class=\"".$final_class."\">$".$thisGuestType->final_fee."</td>\n";
			print "\t\t\t</tr>\n";
		}
?>
			</table>
			</fieldset>
		  </td>
		</tr>
		<tr>
		  <td width="50%"><label>What type of Delegate are you?</label><span class="req"> *</span><br /><?php print form_dropdown('DelegateCategory', $delegate_type, '',"id='DelegateCategory' class='select ui-widget-content ui-corner-all'"); ?></td>
		  <td width="50%"><label>Are you local or flying in?</label><span class="req"> *</span><br /><?php print form_dropdown('TravelMethod', $travel_method, '',"id='TravelMethod' class='select ui-widget-content ui-corner-all'"); ?></td>
		</tr>
		<tr>
			<td align="right">If you would like a translator for your screening appearances, enter language required: </td>
			<td><label>Translator Language</label><br /><?php print form_dropdown('TranslatorLang', $translator_lang, '',"id='TranslatorLang' class='select ui-widget-content ui-corner-all'"); ?></td>
		</tr>
	  </table>


	<h3 align="center">Contact Information</h3>
	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
			<td><label>First Name</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'FirstName', 'id'=>'FirstName', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Last (Family) Name</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'LastName', 'id'=>'LastName', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Film Name</label><br /><?php print form_input(array('name'=>'Film', 'id'=>'Film', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				<!-- <p><span class="req">*</span> Required for Filmmaker in Festival</p> -->
			</td>
			<td><label>Company Name</label><br /><?php print form_input(array('name'=>'FilmOrCompany', 'id'=>'FilmOrCompany', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?>
				<!-- <p><span class="req">*</span> Required for Industry / Press</p> -->
			</td>
		</tr>
		<tr>
			<td width="25%"><label>Address / Street</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'GuestAddress', 'id'=>'GuestAddress', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td width="25%"><label>City</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'GuestCity', 'id'=>'GuestCity', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td width="25%"><label>State / Province</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'GuestState', 'id'=>'GuestState', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td width="25%"><label>Postal Code</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'GuestPostal', 'id'=>'GuestPostal', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr>
			<td><label>Email Address</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'EmailAddress', 'id'=>'EmailAddress', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Mobile Phone</label><span class="req"> *</span><br /><?php print form_input(array('name'=>'MobilePhone', 'id'=>'MobilePhone', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Other Phone</label><br /><?php print form_input(array('name'=>'DayPhone', 'id'=>'DayPhone', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Website or Blog</label><br /><?php print form_input(array('name'=>'WebsiteURL', 'id'=>'WebsiteURL', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>
		<tr>
			<td colspan="2"><label>Film/Company Facebook Page</label><br /><?php print form_input(array('name'=>'FacebookPage', 'id'=>'FacebookPage', 'value'=>'', 'class'=>'text ui-widget-content ui-corner-all')); ?></td>
			<td><label>Twitter Name</label><br />@<?php print form_input(array('name'=>'TwitterName', 'id'=>'TwitterName', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Linkedin Profile</label><br /><?php print form_input(array('name'=>'LinkedinProfile', 'id'=>'LinkedinProfile', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>
	</table>

	<h3 align="center">Travel Information</h3>
	<p class="travel_info_hide" style="display:none; text-align:center;">Local - no travel information required.</p>
	<table class="accred_data_table travel_info" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="6"><b>Arriving Flight</b></td>
		</tr>
		<tr>
			<td colspan="2"><label>From City/Airport</label><br /><?php print_airport_select($domestic, $international, "ArrivalCity","","select ui-widget-content ui-corner-all"); ?></td>
			<td colspan="2"><label>To City/Airport</label><br /><?php print_airport_select($domestic, $international, "ArrivalCity2","HNL","select ui-widget-content ui-corner-all"); ?></td>
		</tr>
		<tr>
			<td><label>Arrival Date</label><br /><?php print form_input(array('name'=>'ArrivalDate', 'id'=>'ArrivalDate', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Arrival Time (like 7:00 PM)</label><br /><?php print form_input(array('name'=>'ArrivalTime', 'id'=>'ArrivalTime', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Airline</label><br /><?php print form_dropdown('ArrivalAirline', $airlines_array, '',"class='select-small ui-widget-content ui-corner-all'"); ?></td>
			<td><label>Flight No.</label><br /><?php print form_input(array('name'=>'ArrivalFlightNum', 'id'=>'ArrivalFlightNum', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>        
		<tr>
			<td colspan="6"><br /><b>Departing Flight</b></td>
		</tr>
		<tr>
			<td colspan="2"><label>From City/Airport</label><br /><?php print_airport_select($domestic, $international, "DepartureCity","HNL","select ui-widget-content ui-corner-all"); ?></td>
			<td colspan="2"><label>To City/Airport</label><br /><?php print_airport_select($domestic, $international, "DepartureCity2","","select ui-widget-content ui-corner-all"); ?></td>
		</tr>
		<tr>
			<td><label>Departure Date</label><br /><?php print form_input(array('name'=>'DepartureDate', 'id'=>'DepartureDate', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Departure Time (like 7:00 PM)</label><br /><?php print form_input(array('name'=>'DepartureTime', 'id'=>'DepartureTime', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
			<td><label>Airline</label><br /><?php print form_dropdown('DepartureAirline', $airlines_array, '',"class='select-small ui-widget-content ui-corner-all'"); ?></td>
			<td><label>Flight No.</label><br /><?php print form_input(array('name'=>'DepartureFlightNum', 'id'=>'DepartureFlightNum', 'value'=>'', 'class'=>'text-small ui-widget-content ui-corner-all')); ?></td>
		</tr>        
	</table>

	<!--
	<h3 align="center">Screening Attendance Information</h3>
	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
		  <td width="50%">Will you attend the <strong>Opening Night</strong> screening?<span class="req"> *</span></td><td width="50%"><?php print form_dropdown('AttendOpening', $yes_no, '',"class='select-small ui-widget-content ui-corner-all'"); ?></td>
		</tr>
		<tr>
		  <td>Will you attend the <strong>Closing Night</strong> screening?<span class="req"> *</span></td><td><?php print form_dropdown('AttendClosing', $yes_no, '',"class='select-small ui-widget-content ui-corner-all'"); ?></td>
		</tr>
	</table>
-->

	<h3 align="center">Professional Biography<span class="req"> *</span></h3>
	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
			<td><p style="margin-bottom:0;">You can provide a link to an online biography, or enter one below (limit ~ 100 words).</p></td>
		</tr>
		<tr>
			<td><?php print form_textarea(array('name'=>'Biography', 'id'=>'Biography', 'value'=>'', 'rows'=>'4', 'cols'=>'75', 'class'=>'text-full ui-widget-content ui-corner-all')); ?></td>
		</tr>
	</table>

	<h3 align="center">Upload a Photo</h3>
	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
			<td><p style="margin-bottom:0;">Please provide a head shot of the delegate applying to attend this festival. This will be used for your delegate badge. <strong>Please limit files to a maximum of 2MB. Only .GIF .JPG or .PNG formats accepted.</strong></p></td>
		</tr>
		<tr>
			<td><?php print form_upload(array('name'=>'GuestPhoto', 'id'=>'GuestPhoto', 'value'=>'', 'size'=>'70', 'class'=>'text-full ui-widget-content ui-corner-all')); ?></td>
		</tr>
	</table>

<!--
	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tr>
			<td><b>Are You a Human?</b><br /><span style="font-size: 12px;">We are using a <a href="http://en.wikipedia.org/wiki/Captcha" target="_blank" title="Completely Automated Public Turing test to tell Computers and Humans Apart">CAPTCHA</a> here to reduce fake / spam entries of this form. It verifies that you're not a script or bot.</span></td>
			<td>
			<?php //require_once('intranet/lib/recaptcha/recaptcha_html.php'); ?>
			</td>
		</tr>
	</table>
-->

	<table class="accred_data_table" cellspacing="0" cellpadding="5">
		<tbody>
			<tr>
				<td colspan="2">
					<div align="center"><input type="Submit" name="Submit" value="Submit Accreditation Form"></div>
				</td>
			</tr>
		</tbody>
	</table>

	  </form>
<?php
	} // end - remove form if after deadline
?>
</fieldset>
<?php
}

if( count($deadlines_fees) > 0) {
?>

<script type="text/javascript">
	$(function() {	
		<!-- Whole page -->
		$("input:submit").button();
		$("button").button();
		$.datepicker.setDefaults({dateFormat: 'yy-mm-dd'});

		$('#ArrivalDate').datepicker();
		$('#DepartureDate').datepicker();

		// validate accreditation form on keyup and submit
		$("#guestEntryForm").validate({
			rules: { FirstName: "required", LastName: "required", GuestAddress: "required", GuestCity: "required", GuestState: "required", GuestPostal: "required", EmailAddress: { required:true, email:true }, MobilePhone: "required", DelegateCategory: { required:true, min:1 }, TravelMethod: "required", Biography: "required", FacebookPage: "url", LinkedinProfile: "url", WebsiteURL: "url", GuestPhoto: { accept: "png|jpe?g|gif" }  },
			messages: { FirstName: "Please enter your first name.", LastName: "Please enter your last/family name.", GuestAddress: "Please enter an address.", GuestCity: "Please enter a city.", GuestState: "Please enter a state.", GuestPostal: "Please enter a postal code.", EmailAddress: "Please enter your email address.", MobilePhone: "Please enter your mobile phone.", DelegateCategory: "Please choose a delegate type.", TravelMethod: "Please choose a travel method.", Biography: "Please enter a biography.", FacebookPage: "Please enter a valid URL (including http://).", LinkedinProfile: "Please enter a valid URL (including http://).", WebsiteURL: "Please enter a valid URL (including http://).", GuestPhoto: "Please select an image file (.JPG .GIF .PNG)." }
		});

		// update fee when delegate category changes.
		$('#DelegateCategory').on('change', function() {
			var feeinput = $('#Fee');
<?php
	foreach ($deadlines_fees as $thisFee) {
		if (strtotime($currentDate) <= $early_deadline) {
			print "\t\t\tif ( $(this).val() == ".$thisFee->guesttype_id.") { feeinput.val(".$thisFee->early_fee."); }\n";
		} elseif (strtotime($currentDate) <= $late_deadline) {
			print "\t\t\tif ( $(this).val() == ".$thisFee->guesttype_id.") { feeinput.val(".$thisFee->late_fee."); }\n";
		} elseif (strtotime($currentDate) <= $late_deadline) {
			print "\t\t\tif ( $(this).val() == ".$thisFee->guesttype_id.") { feeinput.val(".$thisFee->final_fee."); }\n";
		} elseif (strtotime($currentDate) > $late_deadline) {
			print "\t\t\tif ( $(this).val() == ".$thisFee->guesttype_id.") { feeinput.val(".$thisFee->final_fee."); }\n";
		}
	}
?>
		});

		$('#TravelMethod').on('change', function() {
			if ( $(this).val() == "Local") {
				$('.travel_info INPUT[type=text]').each(function() { $(this).prop('disabled',true).css('background','#CCCCCC'); });
				$('.travel_info SELECT').each(function() { $(this).prop('disabled',true).css('background','#CCCCCC'); });
				$('.travel_info').hide(2000);
				$('.travel_info_hide').show(2000);
			} else {
				$('.travel_info INPUT[type=text]').each(function() { $(this).prop('disabled',false).css('background','#FFFFFF'); });
				$('.travel_info SELECT').each(function() { $(this).prop('disabled',false).css('background','#FFFFFF'); });
				$('.travel_info').show(2000);
				$('.travel_info_hide').hide(2000);
			}
		});
	});
</script>

<?php
}
?>