<div align="center">
    <p style="font-weight:bold; color:#000000; font-size:24px">Generate App Icons</p>
</div>

<p><b>Select a file to create iOS &amp; Android icons from</b> - Recommended size: 1024 x 1024 pixels, PNG file format</p>

<form method="post" action="/generateappicons/process/" enctype="multipart/form-data">
	<label for="source-file">Select a File</label><br />
	<?php print form_upload(array('name'=>'source-file', 'id'=>'source-file', 'value'=>'', 'class'=>'', 'size'=>'55')); ?>

	<?php print form_submit(array('name'=>'submit', 'id'=>'submit', 'value'=>'Process Files')); ?>
</form>