<style type="text/css">
	H2 { font-family:Verdana, Arial, Helvetica, sans-serif;}
	LABEL { font-weight:bold; color:#000;}
	INPUT.text { margin-bottom:5px; width:320px; height: 40px; padding: 1em 0.5em; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; border:1px solid #CCC; }
	TBODY { border:none;}
	#delete_message { width:100%; margin:0 auto; color:#900; text-align:center; }
	#addedit_message { width:100%; margin:0 auto; color:#090; text-align:center; }
	#login_bkg * { box-sizing: border-box; }
	#login_bkg { width: 435px; height: 300px; padding: 30px 20px; margin: 0 auto; background: url('/assets/images/login-background.png') no-repeat; box-sizing: border-box; }
	#login_bkg .titles { border-bottom: 1px solid #CCCCCC; height: 30px; margin: 0 0 20px; }
	#login_bkg .titles h4 { color: #000; font-size: 20px; }
	#login_bkg .titles .right { float: right; }
	#login_bkg input[type="submit"] { margin: 0 0 13px 0 !important; }
</style>

<div id="login_bkg">
	<div class="titles">
		<a class="right" href="/films/">Back to Film Listing</a>
		<h4>HIFF/FestPro Login</h4>
	</div>
<?php
if ($error_logging_in != FALSE) {
  if ($account_disabled == TRUE) {
    print "\t<div id=\"delete_message\">Your account has been disabled, please contact the site administrator.</div>\n";
  } else {
    print "\t<div id=\"delete_message\">There has been an error logging you in. Please try again.</div>\n";
  }
} elseif ($logged_out != FALSE) {
  print "\t<div id=\"addedit_message\"><b>".$logged_out."</b> has been logged out.</div>\n";
}

print form_open('login/verify');
print "\n\t<table cellspacing=\"0\" cellpadding=\"3\" border=\"0\" align=\"center\">\n";
print "\t\t<tr valign=\"top\"><td>".form_label('Username: ', 'username')."</td><td>".form_input(array('name'=>'username','id'=>'username','size'=>'40','class'=>'text'))."</td></tr>\n";
print "\t\t<tr valign=\"top\"><td>".form_label('Password: ', 'password')."</td><td>".form_password(array('name'=>'password','id'=>'password','size'=>'40','class'=>'text'))."</td></tr>\n";
print "\t\t<tr valign=\"top\"><td align=\"right\" colspan=\"2\">";
//print form_checkbox(array('name'=>'save_login','id'=>'save_login','checked'=>FALSE,'value'=>1))."&nbsp; &nbsp;".form_label('Stay logged in?', 'save_login')."\n";
print form_submit('loginsubmit','Sign In')."</td></tr>\n";
print "\t</table>\n";
print form_close();
?>
</div>

<script type="text/javascript">
	$(function() {	
		$('INPUT[name=loginsubmit]').button();
	});
</script>
