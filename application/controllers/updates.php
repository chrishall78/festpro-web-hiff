<?php
class Updates extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");	
		require_once("admin/helper_functions.php");
	}
	
	function index($type="normal")
	{
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$film_id_array = convert_to_array3($data['filmids']);

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);

		if (count($film_id_array) == 0) {
			$vars['sections'] = $vars['countries'] = $vars['languages'] = $vars['genres'] = $vars['eventtypes'] = array();
		} else {
			$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
			$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
			//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
			$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
			$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");
		}

		$vars['title'] = "Program Updates - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		//$vars['path'] = "http://www.hiff.org/";
		$vars['selected_page'] = "updates";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('updates', $data);
		$this->load->view('footer', $vars);
	}
}

/* End of file updates.php */
/* Location: ./system/application/controllers/updates.php */
?>