<?php
class Accreditation extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");	
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->helper('file');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Locationmodel','location');

		$data['upcoming'] = $this->festival->get_current_festival();
		$upcoming_fest = $data['upcoming'][0]->id;
		$upcoming_name = $data['upcoming'][0]->name;
		$upcoming_year = $data['upcoming'][0]->year;
		
		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}
		
		$data['deadlines_fees'] = $this->guest->get_deadlines_fees($upcoming_fest);
		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		$data['domestic'] = $this->guesttype->get_type_airports("domestic");
		$data['international'] = $this->guesttype->get_type_airports("international");
		$data['airlines'] = $this->guesttype->get_all_type("airlines","asc","no");
		$data['all_languages'] = $this->filmtype->get_all_type("language","asc", "no");

		//$data['recaptcha'] = read_file('./assets/scripts/recaptcha/recaptcha_html.php');
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		if(count($film_id_array) == 0) {
			$vars['sections'] = $vars['countries'] = $vars['languages'] = $vars['genres'] = $vars['eventtypes'] = array("--");
		} else {
			$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
			$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
			//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
			$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
			$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");
		}
		$vars['title'] = "Delegate Accreditation - ".$upcoming_year." ".$upcoming_name;
		$vars['path'] = "/";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('accreditation', $data);
		$this->load->view('footer', $vars);
	}

	function thank_you()
	{
		// This function will check the captcha, and either reload the form or submit the data and display a thank you page.
		$this->load->helper('form');
		$this->load->library('email');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Guestreviewmodel','guestreview');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Locationmodel','location');

		$data['upcoming'] = $this->festival->get_current_festival();
		$upcoming_fest = $data['upcoming'][0]->id;
		$upcoming_name = $data['upcoming'][0]->name;
		$upcoming_year = $data['upcoming'][0]->year;
		
		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$film_id_array = convert_to_array3($data['filmids']);
		
		$data['guest_types'] = $this->guesttype->get_all_type("guests","asc","no");
		$guest_type_array = convert_to_array2($data['guest_types']);

		// Handle photo file upload and retrieve photo url & file system path
		$photoUrl = null;
		$filePath = null;
		$config['upload_path'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year)."/delegates";
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= '2048';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['remove_spaces']  = true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload("GuestPhoto")) {
			$error = array('error' => $this->upload->display_errors('', ''));
			// Handle an error better here - reload the form with an error message?
			//print $error['error'];
		} else 	{
			$data['upload_data'] = $this->upload->data();
			$filePath = $data['upload_data']['full_path'];
			$photoUrl = "/".$config['upload_path']."/".$data['upload_data']['file_name'];
		}
		
		// Insert data to film review table here.
		$new_id = $this->guestreview->insert_guest_review_entry($this->input->post(), $photoUrl, $filePath);
		
		// 1. Send notification email to Hospitality or PR staff
		if ($new_id != 0) {
			$dc = $this->input->post('DelegateCategory');
			if ($guest_type_array[$dc] == "Press") {
				// Press email address(es) - defined in films_functions.php
				$to = explode(",", FP_ACCRED_PRESS_EMAIL);
				foreach ($to as &$email) { $email = trim($email); }
			} else {
				// Hospitality email address(es) - defined in films_functions.php
				$to = explode(",", FP_ACCRED_INDUSTRY_EMAIL);
				foreach ($to as &$email) { $email = trim($email); }
			}

			$subject = "New Accreditation Application - ".$this->input->post('FirstName')." ".$this->input->post('LastName');
			$message  = "A new guest has applied through the Accreditation Form for the ".$upcoming_year." ".$upcoming_name.".\n\n";
			$message .= "First/Last Name: ".$this->input->post('FirstName')." ".$this->input->post('LastName')."\n";
			$message .= "Email Address: ".$this->input->post('EmailAddress')."\n";
			$message .= "Mobile Phone: ".$this->input->post('MobilePhone')."\n";
			if ($this->input->post('DelegateCategory') == 0) {
				$message .= "Delegate Type: Not Specified\n";
			} else {
				$message .= "Delegate Type: ".$guest_type_array[$this->input->post('DelegateCategory')]."\n";
			}
			$message .= "Badge Fee: $".$this->input->post('Fee')."\n";
			$message .= "Film: ".$this->input->post('Film')."\n";
			$message .= "Company: ".$this->input->post('FilmOrCompany')."\n";
			$message .= "Travel Method: ".$this->input->post('TravelMethod')."\n";

			if ($photoUrl == null) {
				$message .= "Guest Photo: None Uploaded";
				if ($error['error'] != "") {
					$message .= " (".$error['error'].")";
				}
				$message .= "\n";
			} else {
				$message .= "Guest Photo: ".FP_BASE_URL.$photoUrl."\n";
			}
			/*
			$message .= "Attending Opening: ";
			if ($this->input->post('AttendOpening') == 0) { $message .= "NO\n"; } else { $message .= "YES\n"; }
			$message .= "Attending Closing: ";
			if ($this->input->post('AttendClosing') == 0) { $message .= "NO\n"; } else { $message .= "YES\n"; }
			*/
			$message .= "Submission Date: ".date("m-d-Y")."\n\n";
			$message .= "FestPro Notifications\n";
			
			$this->email->from('info@hiff.org', 'FestPro Notifications');
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			if ( ! $this->email->send()) {
				// Generate error
			}

			// 2. Send confirmation email to form submitter
			$message2 = "Hello ".$this->input->post('FirstName').",\n";
			$message2 .= "Thank you for submitting your delegate application. A festival representative will review your application and reply to you via email with an update on your status as a delegate.\n\n";
			$message2 .= "Industry delegates who do not qualify for complimentary accreditation: Please submit your $150 payment by clicking the \"Buy Now\" button on the accreditation form page: https://festpro.hiff.org/accreditation\n\n";
			$message2 .= "Please Note: This is simply an application to become a delegate, it is not a confirmation of delegate status.\n\n";
			$message2 .= "Hawaii International Film Festival Guest Services\n";

			$this->email->clear();
			$this->email->from('guestservices@hiff.org', 'HIFF Guest Services');
			$this->email->to($this->input->post('EmailAddress'));
			$this->email->subject("Your guest accreditation request for the 2017 Hawaii International Film Festival was received");
			$this->email->message($message2);
			if ( ! $this->email->send()) {
				// Generate error
			}
		}

		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		if(count($film_id_array) == 0) {
			$vars['filmJSON'] = null;
			$vars['sections'] = $vars['countries'] = $vars['languages'] = $vars['genres'] = $vars['eventtypes'] = array("--");
			$vars['schedule'] = array("--");
		} else {
			$vars['filmJSON'] = json_encode($data['filmids_search']);
			$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
			$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
			$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
			//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
			$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
			$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");
		}
		$vars['title'] = "Thank You";
		$vars['path'] = "/";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('accreditationty', $data);
		$this->load->view('footer', $vars);
	}

}

/* End of file accreditation.php */
/* Location: ./system/application/controllers/accreditation.php */
?>