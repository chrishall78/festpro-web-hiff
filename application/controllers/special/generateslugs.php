<?php
class Generateslugs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");	
	}
	
	function index()
	{
	
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		
		$data['output'] = "";
		$data['festivals'] = $this->festival->get_all_festivals();		
		
		foreach ($data['festivals'] as $thisFestival) {
			$festival_id = $thisFestival->id;
			$festival_year = $thisFestival->year;
			$data['output'] .= $festival_year." ".$thisFestival->name."<br><br>";

			//$festival_id = 15;
			//$festival_year = 2010;
			//$data['output'] .= $festival_year."<br><br>";
			
			$film_list = $this->films->get_all_internal_films($festival_id);
			foreach ($film_list as $thisFilm) {
				if ($thisFilm->slug == "" || $thisFilm->slug == NULL) {
					$updated_id = 0;
					// Create new slug when it detects an empty slug field
					$new_slug = create_slug($thisFilm->title_en, $festival_year);
					$data['output'] .= "Empty slug for: <b>".$thisFilm->title_en."</b> Generated slug: <b>".$new_slug."</b>";
					
					// Update film record with new slug
					$values = array("slug" => $new_slug);
					$updated_id = $this->films->update_value($values, "wf_movie", $thisFilm->movie_id);
					if ($updated_id != 0) {
						$data['output'] .= " Slug Updated!<br>";
					} else {
						$data['output'] .= " <b style=\"color:red;\">Slug Updated!</b><br>";
					}
				}
			}
		}

		$vars['path'] = "/";
		$vars['title'] = "Generate Slugs Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('generateslugs',$data);
		$this->load->view('footer',$vars);

	}

}

/* End of file scrntimeupdate.php */
/* Location: ./system/application/controllers/scrntimeupdate.php */
?>