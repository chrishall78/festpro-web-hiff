<?php
class Encryptcheck extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->load->model('Usersmodel','users');
		$this->load->library('encrypt');

		$data['error_logging_in'] = FALSE;
		$data['logged_out'] = FALSE;
		
		$userdata = $this->users->get_all_users();
		$data['output'] = "";
		
		foreach ($userdata as $thisUser) {
			$oldPassword = $thisUser->Password;
			
			$newPassword = $this->encrypt->encode_from_legacy($oldPassword, $legacy_mode = MCRYPT_MODE_ECB, $key = '2imD7a9dNjnE8d7sX3q8c6C52bP2As7d');
			
			$data['output'] .= $thisUser->Username.":<br>";
			$data['output'] .= $oldPassword."<br>";
			$data['output'] .= $newPassword."<br><br>";
			
			// Add section to update database here
			//$values = array("Password" => $newPassword);			
			//$this->users->update_value($values, "users",$thisUser->id);
		}

		$vars['path'] = "/";
		$vars['title'] = "HIFF Hallway - Encrypt Update";
		$vars['admin'] = "NO";
		$this->load->helper('form');
		$this->load->view('header_nonav',$vars);
		$this->load->view('encryptcheck',$data);
		$this->load->view('footer',$vars);
	}
}

/* End of file login.php */
/* Location: ./system/application/controllers/encryptcheck.php */
?>