<?php
class Fileimport extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");	

		function cleanQuotes($text) {
			$newText = trim($text);
			$newText = ltrim($newText, "\"'");
			$newText = rtrim($newText, "\"'");
			return $newText;
		}

		function unique($array) {
			$newArray = array_unique($array);
			$newArray = array_filter($newArray);
			return $newArray;
		}

		function processPersonnel($director_temp, $directorList) {
			$new_director_string = "";
			foreach ($director_temp as $thisDirector) {
				$director_name = explode(" ",$thisDirector);
				if (count($director_name) == 1) { $count = 1; }
				elseif (count($director_name) == 2) { $count = 2; }
				elseif (count($director_name) > 2) { $count = 99; }
				foreach($directorList as $dList) {
					if ($count == 1) {
						if ($director_name[0] == $dList->name && $dList->lastname == "") {
							$new_director_string .= $dList->id.",";
						}
					} elseif ($count == 2) {
						if ($director_name[0] == $dList->name && $director_name[1] == $dList->lastname) {
							$new_director_string .= $dList->id.",";
						}
					} elseif ($count == 99) {
						$firstmiddle = ""; $last = "";
						$realcount = count($director_name)-1;
						for ($x=0; $x < $realcount; $x++) {
							$firstmiddle .= $director_name[$x]." ";						
						}
						$firstmiddle = trim($firstmiddle); $last = $director_name[$realcount];

						if ($firstmiddle == $dList->name && $last == $dList->lastname) {
							$new_director_string .= $dList->id.",";
						}						
					}
				}
			} $new_director_string = trim($new_director_string, ",");
			return $new_director_string;
		}

		function processSynopsis($synopsis) {
			// For #1
			// â€“ = &ndash;
			// â€œ = &quot;
			// â€™ = '
			// Ã±  = &ntilde; 

			// For #2
			// â€  = &quot;

			$search1 = array('â€“', 'â€œ', 'â€™', 'Ã±');
			$replace1 = array('&ndash;', '&quot;', '\'', '&ntilde');
			$search2 = array('â€');
			$replace2 = array('&quot;');

			$new_synopsis = strip_tags($synopsis);
			$new_synopsis = str_replace($search1, $replace1, $new_synopsis);
			$new_synopsis = str_replace($search2, $replace2, $new_synopsis);
			$new_synopsis = trim($new_synopsis);

			return $new_synopsis;
		}
	}
	
	function index()
	{
	
		$this->load->helper('file');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		
		$tabfile = read_file('./HRFF25ImportDRAFT1.txt');
		$festival_id = "7";
		$tabrecords = explode("\n", $tabfile);
		//$tabrecords = explode("\r", $tabfile);

		$importfile = array(); $outputrecord = ""; $importcount = 0; $simportcount = 0;
		foreach ($tabrecords as $thisRecord) {
			$importfile[] = explode("\t", $thisRecord);
		}

		// Spreadsheet Import Columns - Updated April 2014
		//  0.  1. English Film Title - trim to remove any extra line returns.
		//  1.  2. Original Language Film Title - trim to remove any extra line returns.
		//  2.  3. Synopsis
		//  3.  4. Country *
		//  4.  5. Language *
		//  5.  6. Genre *
		//  6.  7. Section
		//  7.  8. Event Type
		//  8.  9. Year
		//  9. 10. Runtime (total number of minutes, like: 120)
		// 10. 11. Video Format
		// 11. 12. Director *
		// 12. 13. Executive Producer *
		// 13. 14. Producer *
		// 14. 15. Writer *
		// 15. 16. Cinematographer *
		// 16. 17. Production Designer *
		// 17. 18. Art Director *
		// 18. 19. Sound *
		// 19. 20. Music *
		// 20. 21. Editor *
		// 21. 22. Cast *
		// 22. 23. Trailer Url
		// 23. 24. Screening Date and Time (such as: 3/1/2014 7:00 PM)
		// 24. 25. Screening Location
	    // 25. 26. Preceded By
		// * These fields can have multiple values, please separate them with a comma.

		// Before running for real:
		// 1. Normalize country, language, genre columns with commas
		// 2. make sure filmtype, films & personnel model are updated
		// 3. make sure personnel ids match up to database
		// 4. get actual festival id
		// 5. update file name and upload tab delimited file to root directory


		// 1. Make unique list of fields to enter
		$country = $language = $genre = $section = $event = $format = array();
		$director = $execprod = $producer = $writer = $cinematographer = $proddesigner = $artdirector = $sound = $music = $editor = $cast = array();
		$x = 0; $e = 0;

		foreach ($importfile as $thisRecord) {
			$fields = count($thisRecord);
			for ($z = 0; $z < $fields; $z++) {
				if (!isset($thisRecord[$z])) {
					print $z." - ".($x+1)."<br />"; $e++;
				} else {
					$thisRecord[$z] = cleanQuotes($thisRecord[$z]);
					$thisRecord[$z] = str_replace('""', '"', $thisRecord[$z]);
				}
			}

			$tempArray = explode(",", $thisRecord[3]);
			foreach ($tempArray as $value) { $country[] = trim($value); }
			$tempArray = explode(",", $thisRecord[4]);
			foreach ($tempArray as $value) { $language[] = trim($value); }
			$tempArray = explode(",", $thisRecord[5]);
			foreach ($tempArray as $value) { $genre[] = trim($value); }
			$tempArray = explode(",", $thisRecord[6]);
			foreach ($tempArray as $value) { $section[] = trim($value); }
			$event[] = $thisRecord[7];
			$format[] = $thisRecord[10];

			$tempArray = explode(",", $thisRecord[11]);
			foreach ($tempArray as $value) { $director[] = trim($value); }
			$execprod[] = $thisRecord[12];
			$producer[] = $thisRecord[13];
			$writer[] = $thisRecord[14];
			$cinematographer[] = $thisRecord[15];
			$proddesigner[] = $thisRecord[16];
			$artdirector[] = $thisRecord[17];
			$sound[] = $thisRecord[18];
			$music[] = $thisRecord[19];
			$editor[] = $thisRecord[20];
			$tempArray = explode(",", $thisRecord[21]);
			foreach ($tempArray as $value) { $cast[] = trim($value); }

			$x++;
		}
		if ($e != 0) { print "<br><br>".$e." formatting errors encountered.<br>"; exit; }
		
		$country = unique($country);
		$language = unique($language);
		$genre = unique($genre);
		$section = unique($section);
		$event = unique($event);
		$format = unique($format);

		$director = unique($director);
		$execprod = unique($execprod);
		$producer = unique($producer);
		$writer = unique($writer);
		$cinematographer = unique($cinematographer);
		$proddesigner = unique($proddesigner);
		$artdirector = unique($artdirector);
		$sound = unique($sound);
		$music = unique($music);
		$editor = unique($editor);
		$cast = unique($cast);


		// 2. Check to see if these values already exist in the database, if not, add them.
		$countryList = $this->filmtype->check_add_film_type("country", $country);
		$languageList = $this->filmtype->check_add_film_type("language", $language);
		$genreList = $this->filmtype->check_add_film_type("genre", $genre);
		$sectionList = $this->section->check_add_section($section);
		$eventList = $this->filmtype->check_add_film_type("event", $event);
		$formatList = $this->filmtype->check_add_film_type("format", $format);

		$directorList = $this->personnel->check_add_personnel("Director", $director);
		$execprodList = $this->personnel->check_add_personnel("Executive Producer", $execprod);
		$producerList = $this->personnel->check_add_personnel("Producer", $producer);
		$writerList = $this->personnel->check_add_personnel("Writer", $writer);
		$cinematographerList = $this->personnel->check_add_personnel("Cinematographer", $cinematographer);
		$proddesignerList = $this->personnel->check_add_personnel("Production Designer", $proddesigner);
		$artdirectorList = $this->personnel->check_add_personnel("Art Director", $artdirector);
		$soundList = $this->personnel->check_add_personnel("Sound", $sound);
		$musicList = $this->personnel->check_add_personnel("Music Director", $music);
		$editorList = $this->personnel->check_add_personnel("Editor", $editor);
		$castList = $this->personnel->check_add_personnel("Cast", $cast);

		$imported_titles = array();
		$imported_films = array();


		// 3. Loop through array and insert into database (films only).
		foreach ($importfile as $thisRecord) {
			$fields = count($thisRecord);
			for ($z = 0; $z < $fields; $z++) {
				$thisRecord[$z] = cleanQuotes($thisRecord[$z]);
				$thisRecord[$z] = str_replace('""', '"', $thisRecord[$z]);
			}

			// Check to see that this record is not imported more than once (for lists which include screenings)
			if (in_array($thisRecord[0], $imported_titles) == false) {

				// Replace Country names with wf_type_country id numbers
				$country_temp = explode(", ",$thisRecord[3]);
				$new_country_string = "";
				foreach ($country_temp as $thisCountry) {
					foreach($countryList as $cList) {
						if ($cList->name == $thisCountry) {
							$new_country_string .= $cList->id.",";
						}
					}
				} $new_country_string = trim($new_country_string, ",");

				// Replace Language names with wf_type_language id numbers
				$language_temp = explode(", ",$thisRecord[4]);
				$new_language_string = "";
				foreach ($language_temp as $thisLanguage) {
					foreach($languageList as $lList) {
						if ($lList->name == $thisLanguage) {
							$new_language_string .= $lList->id.",";
						}
					}
				} $new_language_string = trim($new_language_string, ",");

				// Replace Genre names with wf_type_genre id numbers
				$genre_temp = explode(", ",$thisRecord[5]);
				$new_genre_string = "";
				foreach ($genre_temp as $thisGenre) {
					foreach($genreList as $gList) {
						if ($gList->name == $thisGenre) {
							$new_genre_string .= $gList->id.",";
						}
					}
				} $new_genre_string = trim($new_genre_string, ",");
				
				// Replace Personnel names with wf_personnel id numbers
				$new_director_string = processPersonnel(explode(", ",$thisRecord[11]), $directorList);
				$new_execprod_string = processPersonnel(explode(", ",$thisRecord[12]), $execprodList);
				$new_producer_string = processPersonnel(explode(", ",$thisRecord[13]), $producerList);
				$new_writer_string = processPersonnel(explode(", ",$thisRecord[14]), $writerList);
				$new_cinematographer_string = processPersonnel(explode(", ",$thisRecord[15]), $cinematographerList);
				$new_proddesigner_string = processPersonnel(explode(", ",$thisRecord[16]), $proddesignerList);
				$new_artdirector_string = processPersonnel(explode(", ",$thisRecord[17]), $artdirectorList);
				$new_sound_string = processPersonnel(explode(", ",$thisRecord[18]), $soundList);
				$new_music_string = processPersonnel(explode(", ",$thisRecord[19]), $musicList);
				$new_editor_string = processPersonnel(explode(", ",$thisRecord[20]), $editorList);
				$new_cast_string = processPersonnel(explode(", ",$thisRecord[21]), $castList);

				// Replace section names with ids
				$new_section_string = "1";
				foreach($sectionList as $thisSection) { if ($thisSection->name == $thisRecord[6]) { $new_section_string = $thisSection->id; } }

				// Replace event names with ids
				$new_event_string = "1";
				foreach($eventList as $thisEvent) { if ($thisEvent->name == $thisRecord[7]) { $new_event_string = $thisEvent->id; } }
				
				// Replace format names with ids
				$new_format_string = "25"; // Unknown / No Film
				foreach($formatList as $thisFormat) { if ($thisFormat->name == $thisRecord[10]) { $new_format_string = $thisFormat->id; } }
				
				$synopsis_temp = processSynopsis($thisRecord[2]);

				$values = array(
					"title_en" => adjust_title(trim($thisRecord[0])),
					"title" => trim($thisRecord[1]),
					"synopsis" => $synopsis_temp,
					"synopsis_short" => $synopsis_temp,
					"synopsis_original" => "",
					"year" => $thisRecord[8],
					"runtime_int" => $thisRecord[9],
					"country" => $new_country_string,
					"language" => $new_language_string,
					"genre" => $new_genre_string,
					"format_id" => $new_format_string,
					"event_id" => $new_event_string,
					"category_id" => $new_section_string,
					"color_id" => NULL,
					"director" => $new_director_string,
					"execprod" => $new_execprod_string,
					"producer" => $new_producer_string,
					"writer" => $new_writer_string,
					"cinematographer" => $new_cinematographer_string,
					"proddesigner" => $new_proddesigner_string,
					"artdirector" => $new_artdirector_string,
					"sound" => $new_sound_string,
					"music" => $new_music_string,
					"editor" => $new_editor_string,
					"cast" => $new_cast_string,
					"festival_id" => $festival_id
				);

				// Sort of hacky, depends on previously known personnel_type_id
				$personnel_to_add = array(
					"director" => 1,
					"execprod" => 10,
					"producer" => 2,
					"writer" => 22,
					"cinematographer" => 5,
					"proddesigner" => 14,
					"artdirector" => 12,
					"sound" => 15,
					"music" => 8,
					"editor" => 7,
					"cast" => 6);
				
				// Assign a slug for the film
				$slug = create_slug(trim($thisRecord[0]));
				$counter = 2; $empty = true; $base_slug = $slug;
				while ($empty) {
					$empty = $this->films->check_existing_slug($slug);
					if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
				}
				$new_id = $this->films->import_film_from_tabfile($values, $slug, $personnel_to_add);
				
				if ($new_id != 0) {
					$outputrecord .= "<li><b>".$values["title_en"]."</b> has been successfully imported to FestPro.</li>\n";
					$imported_titles[] = $thisRecord[0];
					$imported_films[$new_id] = $thisRecord[0];
					$importcount++;
				}
			} else {
				$outputrecord .= "<li><b>".$thisRecord[0]."</b> was skipped as a duplicate.</li>\n";
			}
		}

		// 4. Loop through array again and insert into database (screenings).
		foreach ($importfile as $thisRecord) {
			// find id number of previously added film
			$film_id = "0";
			$film_name = "";
			foreach($imported_films as $key => $value) {
				if ($value == $thisRecord[0]) {
					$film_id = $key;
					$film_name = $value;
				}
			}

			$screeningDateTime = $thisRecord[23];
			if ($screeningDateTime != "") {
				$datetime = strtotime($screeningDateTime);
				$screening_values = array(
					"movie_id" => $film_id,
					"program_movie_ids" => $film_id,
					"program_name" => "",
					"date" => date("Y-m-d",$datetime),
					"time" => date("H:i:s",$datetime),
					"url" => "",
					"Rush" => 0,
					"Free" => 0,
					"Private" => 0,
					"location_id" => 1, // default, location is not specified in the import
					"intro" => 0,
					"fest_trailer" => 0,
					"sponsor_trailer1" => 0,
					"sponsor_trailer2" => 0,
					"q_and_a" => 0,
					"intro_length" => 5,
					"fest_trailer_length" => 3,
					"sponsor_trailer1_length" => 3,
					"sponsor_trailer2_length" => 3,
					"q_and_a_length" => 15
				);
				$new_screening_id = $this->schedule->add_value($screening_values, "wf_screening");

				if ($new_screening_id != 0) {
					$outputrecord .= "<li>A screening for <b>".$film_name."</b> at ".date("m-d-Y g:i A",$datetime)." has been successfully imported to FestPro.</li>\n";
					$simportcount++;
				}

			}
		}
		
		$data['tabfile'] = $importfile;
		$data['outputrecord'] = $outputrecord;
		$data['importcount'] = $importcount;
		$data['simportcount'] = $simportcount;
	
		$vars['path'] = "/";
		$vars['title'] = "Tab Delimited File Import Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('fileimport',$data);
		$this->load->view('footer',$vars);

	}
}

/* End of file fileimport.php */
/* Location: ./system/application/controllers/fileimport.php */
?>