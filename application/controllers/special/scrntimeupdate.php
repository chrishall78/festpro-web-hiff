<?php
class Scrntimeupdate extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");	
	}
	
	function index()
	{
	
		$this->load->helper('file');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');

		$data['all_screenings'] = $this->schedule->get_internal_schedule('2000-01-01', '2011-12-31');
		$data['output'] = "";
		
		foreach($data['all_screenings'] as $thisScreening) {
			// Only update this if the total runtime is 0.
			if ($thisScreening->total_runtime == "00:00:00") {
				$runtime = array();

				// Determine if this program has multiple films
				if ($thisScreening->program_movie_ids == "") {
					$film = $this->films->get_film($thisScreening->movie_id);
					$runtime[] = $film[0]->runtime_int;
				} else {
					$film_array = explode(",",$thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilmId) {
						$film = $this->films->get_film($thisFilmId);
						$runtime[] = $film[0]->runtime_int;
					}
				}

				// Add any intros, trailers, Q&A's etc
				if ($thisScreening->intro == 1) { $runtime[] = $thisScreening->intro_length; }
				if ($thisScreening->fest_trailer == 1) { $runtime[] = $thisScreening->fest_trailer_length; }
				if ($thisScreening->sponsor_trailer1 == 1) { $runtime[] = $thisScreening->sponsor_trailer1_length; }
				if ($thisScreening->sponsor_trailer2 == 1) { $runtime[] = $thisScreening->sponsor_trailer2_length; }
				if ($thisScreening->q_and_a == 1) { $runtime[] = $thisScreening->q_and_a_length; }
				
				$trt = 0;
				foreach ($runtime as $thisRuntime) { $trt += $thisRuntime; }
				
				$total_runtime = convert_min_to_time($trt);
				
				if ($total_runtime != "00:00:00") {
					$data['output'] .= $thisScreening->date." ".$thisScreening->time." ".$total_runtime. " [updated]<br />";

					$values = array("total_runtime" => $total_runtime);		
					$this->schedule->update_value($values, "wf_screening", $thisScreening->id);
				} else {
					$data['output'] .= $thisScreening->date." ".$thisScreening->time." no film length found<br />";
				}
			}
		}

		$vars['path'] = "/";
		$vars['title'] = "Screening Time Update Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('scrntimeupdate',$data);
		$this->load->view('footer',$vars);

	}

}

/* End of file scrntimeupdate.php */
/* Location: ./system/application/controllers/scrntimeupdate.php */
?>