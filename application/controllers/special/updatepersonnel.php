<?php
class Updatepersonnel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");	
	}
	
	function index()
	{
	
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Personnelmodel','personnel');
		
		$data['output'] = "";
		$data['festivals'] = $this->festival->get_all_festivals();		
				
		foreach ($data['festivals'] as $thisFestival) {
			$festival_id = $thisFestival->id;
			$data['output'] .= $thisFestival->year." ".$thisFestival->name."<br><br>";

			$film_list = $this->films->get_all_internal_film_ids($festival_id);
			$film_id_array = convert_to_array3($film_list);

			if ( count($film_id_array) > 0 ) {
			
				$personnel = $this->personnel->get_all_personnel($film_id_array, "");
				
				foreach ($personnel as $thisPerson) {
					if ($thisPerson->personnel_roles == "") {
						if ($thisPerson->name != "" && $thisPerson->lastname != "") {
							$updated_id = 0;
							$data['output'] .= "Updating: <b>".$thisPerson->name." ".$thisPerson->lastname."</b> role to ".$thisPerson->role." ";
		
							$values = array("personnel_roles" => $thisPerson->id);
							$updated_id = $this->films->update_value($values, "wf_movie_personnel", $thisPerson->personnel_id);
							if ($updated_id != 0) {
								$data['output'] .= " Personnel Updated!<br>";
							} else {
								$data['output'] .= " <b style=\"color:red;\">Personnel NOT Updated!</b><br>";
							}
						}
					}
				}
				
				//print_r($personnel);
				//print "<br><br>";
			}
		}

		$vars['path'] = "/";
		$vars['title'] = "Update Personnel Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('updatepersonnel',$data);
		$this->load->view('footer',$vars);

	}

}

/* End of file scrntimeupdate.php */
/* Location: ./system/application/controllers/scrntimeupdate.php */
?>