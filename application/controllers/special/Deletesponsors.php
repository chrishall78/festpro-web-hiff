<?php
class Deletesponsors extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
	
		$this->load->model('Sponsormodel','sponsor');
		
		$data['output'] = "";
		$duplicates = [];
		$duplicate_sponsor_ids = [];

		$sponsors = $this->sponsor->get_all_sponsors();
		//print_r($sponsors);

		foreach ($sponsors as $id => $value) {
			$name_lowercase = trim(strtolower($value->name));
			$duplicates[$name_lowercase][] = $value->id;
		}
		//print_r($duplicates);

		foreach($duplicates as $name => $idarray) {
			if (count($idarray) > 1) {
				$x = 0;
				$first_id = 0;
				$replacements = [];

				foreach ($idarray as $sponsorid) {
					$sponsorlogos = $this->sponsor->get_sponsor_logo_simple($sponsorid);
					if ($x == 0) { $first_id = $sponsorid; }
					if ($x > 0) {
						foreach ($sponsorlogos as $value) {
							$replacements[] = $value->id;
						}	
					}
					//print $first_id;
					//print_r($replacements);

					foreach ($replacements as $value) {
						$data['output'] .= "Updating sponsor ID ".$sponsorid." to ".$first_id."<br>";
						$this->sponsor->update_value(array("sponsor_id" => $first_id),"wf_sponsor_logos", $value);
					}


					if ( count($sponsorlogos) == 0) {
						$duplicate_sponsor_ids[] = $sponsorid;
						$data['output'] .= "No sponsor logos for ".$sponsorid."<br>";
					}
					$x++;
					//print_r($sponsorlogos);
				}
				//print_r($idarray);
			}
		}

		foreach ($duplicate_sponsor_ids as $id) {
			$this->sponsor->delete_value($id,"wf_sponsor");
		}

		$vars['path'] = "/";
		$vars['title'] = "Update Photo / Sponsor Logo Path Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('deletesponsors',$data);
		$this->load->view('footer',$vars);

	}

}

/* End of file deletesponsors.php */
/* Location: ./system/application/controllers/deletesponsors.php */
?>