<?php
class Generateappicons extends CI_Controller {
	private $iconSizeArray = null;

	function __construct()
	{
		parent::__construct();
		require_once("films_functions.php");
		require_once("admin/helper_functions.php");

		$this->iconSizeArray = array(
			// To support iOS 6.1 and prior
			//"iPhoneIconSettings.png"		=> 29,
			//"iPhoneIcon.png"				=> 57,
			//"iPhoneIcon@2x.png"			=> 114,
			//"iPadIconSpotlight.png"		=> 50,
			//"iPadIconSpotlight@2x.png"	=> 100,
			//"iPadIcon.png"				=> 72,
			//"iPadIcon@2x.png"				=> 144,

			// iOS 7.0 and later
			"iPhoneIconSettings@2x.png"		=> 58,
			"iPhoneIconSettings@3x.png"		=> 87,
			"iPhoneIcon7@2x.png"			=> 120,
			"iPhoneIcon7@3x.png"			=> 180,
			"iPhoneIconSpotlight7@2x.png"	=> 80,
			"iPhoneIconSpotlight7@3x.png"	=> 120,

			"iPadIconSettings.png"		=> 29,
			"iPadIconSettings@2x.png"	=> 58,
			"iPadIconSpotlight7.png"	=> 40,
			"iPadIconSpotlight7@2x.png"	=> 80,
			"iPadIcon7.png"				=> 76,
			"iPadIcon7@2x.png"			=> 152,
			"iPadProIcon@2x.png"		=> 167,

			// Android
			//"ldpi_ic_launcher.png"	=> 36,
			"mdpi_ic_launcher.png"		=> 48,
			"hdpi_ic_launcher.png"		=> 72,
			"xhdpi_ic_launcher.png"		=> 96,
			"xxhdpi_ic_launcher.png" 	=> 144,
			"xxxhdpi_ic_launcher.png" 	=> 192
		);
	}
	
	function index()
	{	
		$this->load->helper('form');

		$vars['path'] = "/";
		$vars['title'] = "Generate App Icons";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('generateappicons');
		$this->load->view('footer',$vars);
	}


	function process()
	{
		$this->load->helper('form');

		$img_dir = "themes/icons/";
		if (!is_dir($img_dir)) {
			if (!mkdir($img_dir,0755)) { die('Failed to create themes/icons directory: '.$img_dir); }
		}

		$config['upload_path'] = $img_dir;
		$config['allowed_types'] = 'png';
		$config['max_size']	= '4096';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['remove_spaces']  = true;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload("source-file")) {
			echo $this->upload->display_errors('', '');
			$data = array();
		} else 	{
			// Create Thumbnails
			$thumbArray = array();
			$data = array('upload_data' => $this->upload->data());
			$this->load->library('image_lib'); 

			$config2['image_library'] = 'gd2';
			$config2['source_image'] = $data['upload_data']['full_path'];
			$config2['maintain_ratio'] = TRUE;

			foreach ($this->iconSizeArray as $filename => $size) {
				$config2['new_image'] = $data['upload_data']['file_path'].$filename;
				$thumbArray[] = $config2['new_image'];
				$config2['width'] = $size;
				$config2['height'] = $size;
				$this->image_lib->clear();			
				$this->image_lib->initialize($config2);
				if (!$this->image_lib->resize()) { echo $this->image_lib->display_errors(); }
			}

			// Zip thumbnails
			$zip = new ZipArchive;
			$res = $zip->open($data['upload_data']['file_path'].'icons.zip', ZipArchive::CREATE);
			foreach ($thumbArray as $filepath) {
				$zip->addFile($filepath);
			}
			$zip->close();
		}

		$vars['path'] = "/";
		$vars['title'] = "Generate App Icons";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('generateappicons',$data);
		$this->load->view('footer',$vars);
	}

}

/* End of file generateappicons.php */
/* Location: ./system/application/controllers/generateappicons.php */
?>