<?php
class Updatephotos extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");	
	}
	
	function index()
	{
	
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Sponsormodel','sponsor');
		
		$data['output'] = "";
		
		// Update Film Photo paths
		$data['photos'] = $this->photovideo->load_all_photos();		
		
		foreach ($data['photos'] as $thisPhoto) {
			$id = $thisPhoto->id;
			$original = str_replace("/program/","/",$thisPhoto->url_original);
			$large = str_replace("/program/","/",$thisPhoto->url_croplarge);
			$small = str_replace("/program/","/",$thisPhoto->url_cropsmall);
			$file_dir = str_replace("/hiff.org/html/program/","/program.hiff.org/html/",$thisPhoto->file_dir);
			
			$values = array(
				"url_original" => $original,
				"url_croplarge" => $large,
				"url_cropsmall" => $small,
				"file_dir" => $file_dir
			);
			
			$update = $this->photovideo->update_value($values, "wf_movie_photos", $id);

			$data['output'] .= "Updating: ".$original."<br>";
		}

		// Update Sponsor Logo paths
		$data['sponsorlogos'] = $this->sponsor->load_all_sponsor_logos();
		
		foreach ($data['sponsorlogos'] as $thisSponsorLogo) {
			$id = $thisSponsorLogo->id;
			$url_logo = str_replace("/program/","/",$thisSponsorLogo->url_logo);
			
			$values = array(
				"url_logo" => $url_logo
			);
			
			$update = $this->sponsor->update_value($values, "wf_sponsor_logos", $id);

			$data['output'] .= "Updating: ".$url_logo."<br>";
		}
		

		$vars['path'] = "/";
		$vars['title'] = "Update Photo / Sponsor Logo Path Page";
		$vars['admin'] = "NO";
				
		$this->load->view('header_nonav',$vars);
		$this->load->view('updatephotos',$data);
		$this->load->view('footer',$vars);

	}

}

/* End of file scrntimeupdate.php */
/* Location: ./system/application/controllers/scrntimeupdate.php */
?>