<?php
class Film_Listing extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Countrymodel','country');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$data['schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate,$data['festival'][0]->enddate);
		$data['photos'] = $this->photovideo->get_all_photos($film_id_array);
		$data['videos'] = $this->photovideo->get_all_videos($film_id_array);

		$data['sections'] = $this->section->get_all_sections($current_fest);
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
		$data['premieres'] = $this->filmtype->get_all_type("premiere","asc");
		$data['yearcompletion'] = generate_year_array();
		
		// Lists for the 'Add Film' popup
		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['genres'] = $this->filmtype->get_all_type("genre","asc");

		$vars['title'] = "Film Listing";
		$vars['admin'] = "YES";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_listing";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_listing', $data);
		$this->load->view('admin/footer', $vars);
	}

	function search()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Countrymodel','country');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_search($current_fest,"wf_movie.title_en", "asc",0,9999,$this->input->post('filmSearch'));
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$data['schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate,$data['festival'][0]->enddate);
		$data['photos'] = $this->photovideo->get_all_photos($film_id_array);
		$data['videos'] = $this->photovideo->get_all_videos($film_id_array);

		$data['sections'] = $this->section->get_all_sections($current_fest);
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
		$data['premieres'] = $this->filmtype->get_all_type("premiere","asc");
		$data['yearcompletion'] = generate_year_array();
		
		// Lists for the 'Add Film' popup
		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['genres'] = $this->filmtype->get_all_type("genre","asc");

		$vars['title'] = "Film Listing - Search Results";
		$vars['admin'] = "YES";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_listing";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_listing', $data);
		$this->load->view('admin/footer', $vars);
	}

	function sort($sorttoken)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Countrymodel','country');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		switch ($sorttoken) {
			case "namedesc": $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "desc",0,9999); break;
			case "nameasc":  $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);  break;

			case "sectiondesc": $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"category_name", "desc",0,9999); break;
			case "sectionasc":  $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"category_name", "asc",0,9999);  break;

			case "videodesc": $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"format_name", "desc",0,9999); break;
			case "videoasc":  $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"format_name", "asc",0,9999);  break;

			case "eventdesc": $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"event_name", "desc",0,9999); break;
			case "eventasc":  $data['films'] = $this->films->get_all_internal_films_sort($current_fest,"event_name", "asc",0,9999);  break;
			
			// Add sort by country - slightly more difficult.
		}
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$data['schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate,$data['festival'][0]->enddate);
		$data['photos'] = $this->photovideo->get_all_photos($film_id_array);
		$data['videos'] = $this->photovideo->get_all_videos($film_id_array);

		// Lists for the 'Add Film' popup
		$data['sections'] = $this->section->get_all_sections($current_fest);
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
		$data['premieres'] = $this->filmtype->get_all_type("premiere","asc");
		$data['yearcompletion'] = generate_year_array();

		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['genres'] = $this->filmtype->get_all_type("genre","asc");

		$vars['title'] = "Film Listing";
		$vars['admin'] = "YES";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_listing";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_listing', $data);
		$this->load->view('admin/footer', $vars);
			
	}
	
	function add()
	{
		// This function will add the film, manipulate the title to move "THE " or "A " to the end
		// of the title, like: "TITLE, THE" or "TITLE, A". This is for better search and sorting.
		// It will also create the slug from the english title, since this is how the film will be
		// referenced in the URL. It will then redirect to the edit film page for this newly added film.
		$this->load->model('Filmsmodel','films');
		$values = array(
			"title_en" => adjust_title(trim($this->input->post('title_en'))),
			"runtime" => $this->input->post('runtime'),
			"year" => $this->input->post('year'),
			"format_id" => $this->input->post('videoformat'),
			"event_id" => $this->input->post('eventtype'),
			"premiere_id" => $this->input->post('premieretype'),
			"category_id" => $this->input->post('section'),
			"country" => $this->input->post('country'),
			"language" => $this->input->post('language'),
			"genre" => $this->input->post('genre'),
			//"user_id" => $this->input->post('user_id'),
			"festival_id" => $this->input->post('festival_id')
		);
		$slug = create_slug($values["title_en"]);
		
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->films->check_existing_slug($slug);
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}
		
		$new_id = $this->films->add_film($values, $slug);
		print $slug;
	}
	
	function toggle($field_name, $movie_id)
	{
		$this->load->model('Filmsmodel','films');
		
		// Get the info for the film value to be toggled.
		$data['film'] = $this->films->get_film_toggle($movie_id);
		
		// Don't do anything if we couldn't find the film id.
		if ($data['film']) {
			$field_value = 0; $table_name = "wf_festivalmovie";
			
			switch ($field_name) {
				case "Confirmed": $field_value = $data['film'][0]->Confirmed;
								break;
				case "Published": $field_value = $data['film'][0]->Published;
								break;							 
			}

			// Toggle field value
			if ($field_value == 0) {
				$this->films->val_on($movie_id, $table_name, $field_name);
				print "\t<a id=\"".$field_name."-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"".$field_name."\" class=\"icon_tick\"></a></td>\n";
			} elseif ($field_value == 1) {
				$this->films->val_off($movie_id, $table_name, $field_name);
				print "\t<a id=\"".$field_name."-".$movie_id."\" href=\"#\" data-id=\"".$movie_id."\" data-type=\"".$field_name."\" class=\"icon_cross\"></a></td>\n";
			}

			// Set 'Updated Date' field for this film - only for "Published"
			if ($field_value == 0 && $field_name == "Published") {
				$values = array("updated_date" => date("Y-m-d H:i:s"));
				$this->films->update_value($values, "wf_movie", $movie_id);
			}			
		}	
	}

	function publish_all()
	{
		$this->load->model('Filmsmodel','films');

		$film_ids = $this->films->get_all_film_ids2($this->input->post('festival_id'));
		$film_id_array = convert_to_array3($film_ids);
		if (count($film_id_array) > 0) {
			$this->films->publish_all_confirmed_films($film_id_array);
		}
	}

	function unpublish_all()
	{
		$this->load->model('Filmsmodel','films');
		$this->films->unpublish_all_films($this->input->post('festival_id'));
	}

	function generate_pdf($slug="novalue", $language="english", $photo="0")
	{
		if ($slug != "novalue") {
			$this->load->helper('dompdf');
			$this->load->helper('file');

			$this->load->model('Countrymodel','country');
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Filmtypesmodel','filmtype');
			$this->load->model('Genremodel','genre');
			$this->load->model('Languagemodel','language');
			$this->load->model('Personnelmodel','personnel');
			$this->load->model('Photovideomodel','photovideo');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Sectionmodel','section');
			$this->load->model('Sponsormodel','sponsor');
			$this->load->model('Locationmodel','location');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}
			$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);
			$program_movie_ids = array();
			$data['film_array'] = array();

			// Get film info
			$data['film'] = $this->films->get_film_by_slug($slug);
			$data['language'] = $language;
			$movie_id = $data['film'][0]->movie_id;
			$movie_runtime = $data['film'][0]->runtime_int;

			// Get schedule for this film
			$data['film_screenings'] = $this->schedule->get_movie_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate, $data['film'][0]->movie_id);
			if (count($data['film_screenings']) > 0) {
				$program_movie_ids = explode(",", $data['film_screenings'][0]->program_movie_ids);
			}

			// Shorts Program
			if (count($program_movie_ids) > 3) {
				$data['screening_type'] = "shorts_program";
				foreach ($program_movie_ids as $thisMovieId) {
					$shorts_temp = $this->films->get_film($thisMovieId);
					foreach ($shorts_temp as $thisFilm) {
						$ctemp = $ltemp = $gtemp = $dtemp = "";
						$countries = $this->country->get_movie_countries($thisFilm->movie_id);
						$languages = $this->language->get_movie_languages($thisFilm->movie_id);
						//$genres = $this->genre->get_movie_genres($thisFilm->movie_id);
						$personnel = $this->personnel->get_movie_personnel($thisFilm->movie_id, "");

						foreach ($countries as $thisMovieCountry) { $ctemp .= $thisMovieCountry->name.", "; }
						foreach ($languages as $thisMovieLanguage) { $ltemp .= $thisMovieLanguage->name.", "; }
						//foreach ($genres as $thisMovieGenre) { $gtemp .= $thisMovieGenre->name.", "; }
						foreach ($personnel as $thisMovieDirector) {
							$roles = explode(",", $thisMovieDirector->personnel_roles);
							foreach ($roles as $thisRoleId) {
								if ($thisRoleId == "1") {
									$dtemp .= $thisMovieDirector->name." ".$thisMovieDirector->lastname.", ";
								}
							}
						}
						
						$thisFilm->country_name = trim($ctemp,", ");
						$thisFilm->language_name = trim($ltemp,", ");
						//$thisFilm->genre_name = trim($gtemp,", ");
						$thisFilm->director_name = trim($dtemp,", ");
					}
					$data['film_array'][] = $shorts_temp;
				}

				// Get sponsor names/logos for this film
				$data['sponsors'] = $this->sponsor->get_all_sponsors();
				$data['sponsor_logos'] = $this->sponsor->get_all_sponsor_logos($movie_id, $current_fest);

			// Single Film/Event
			} else if (count($program_movie_ids) == 0 || count($program_movie_ids) == 1) {
				$data['screening_type'] = "feature";

			// Feature + 1 or 2 shorts
			} else {
				$data['screening_type'] = "feature_shorts";
				$short_selected = false;
				foreach ($program_movie_ids as $thisMovieId) {
					if ($movie_id != $thisMovieId) {
						$shorts_temp = $this->films->get_film($thisMovieId);

						// check to see if we have selected the short
						foreach ($shorts_temp as $thisFilm) {
							if ($thisFilm->runtime_int > $movie_runtime) {
								$short_selected = true;
							} else {
								$short_selected = false;
							}
						}
						// if so, swap the feature and short.
						if ($short_selected == true) {
							$feature_temp = $data['film'];
							$data['film'] = $shorts_temp;
							$shorts_temp = $feature_temp;
						}

						foreach ($shorts_temp as $thisFilm) {
							$ctemp = $ltemp = $gtemp = $dtemp = "";
							$countries = $this->country->get_movie_countries($thisFilm->movie_id);
							$languages = $this->language->get_movie_languages($thisFilm->movie_id);
							//$genres = $this->genre->get_movie_genres($thisFilm->movie_id);
							$personnel = $this->personnel->get_movie_personnel($thisFilm->movie_id, "");

							foreach ($countries as $thisMovieCountry) { $ctemp .= $thisMovieCountry->name.", "; }
							foreach ($languages as $thisMovieLanguage) { $ltemp .= $thisMovieLanguage->name.", "; }
							//foreach ($genres as $thisMovieGenre) { $gtemp .= $thisMovieGenre->name.", "; }
							foreach ($personnel as $thisMovieDirector) {
								$roles = explode(",", $thisMovieDirector->personnel_roles);
								foreach ($roles as $thisRoleId) {
									if ($thisRoleId == "1") {
										$dtemp .= $thisMovieDirector->name." ".$thisMovieDirector->lastname.", ";
									}
								}
							}
							
							$thisFilm->country_name = trim($ctemp,", ");
							$thisFilm->language_name = trim($ltemp,", ");
							//$thisFilm->genre_name = trim($gtemp,", ");
							$thisFilm->director_name = trim($dtemp,", ");
						}
						$data['film_array'][] = $shorts_temp;
					}
				}
				if ($short_selected == true) {
					$movie_id = $data['film'][0]->movie_id;
				}
			}

			// Only do this if we have a "feature" film
			if ($data['screening_type'] == "feature" || $data['screening_type'] == "feature_shorts" ) {
				// Get film info, plus associated countries, languages and genres
				$data['film_countries'] = $this->country->get_movie_countries($movie_id);
				$data['film_languages'] = $this->language->get_movie_languages($movie_id);
				$data['film_genres'] = $this->genre->get_movie_genres($movie_id);
				
				// Get sponsor names/logos for this film
				$data['sponsors'] = $this->sponsor->get_all_sponsors();
				$data['sponsor_logos'] = $this->sponsor->get_all_sponsor_logos($movie_id, $current_fest);

				// Get existing Film Personnel data
				$data['film_personnel'] = $this->personnel->get_movie_personnel($movie_id, "");				
			}

			// Get existing photos for this film
			$data['film_photos'] = $this->photovideo->get_all_movie_photos($movie_id);
			$data['selected_photo'] = $photo;

			$photo_count = count($data['film_photos']);
			if ($photo_count > 0 && intval($data['selected_photo']) < $photo_count) {
				if ($data['film_photos'][$data['selected_photo']]->url_cropxlarge != "") {
					$photoUrl = $data['film_photos'][$data['selected_photo']]->url_cropxlarge;
				} else {
					$photoUrl = $data['film_photos'][$data['selected_photo']]->url_croplarge;
				}

				$ch = curl_init();
				// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_URL, $photoUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				curl_setopt($ch, CURLOPT_HEADER, 0);
				// grab URL and pass it to the browser
				$image = curl_exec($ch);
				// close cURL resource, and free up system resources
				curl_close($ch);

				$data['file_path'] = $data['upload_dir']."/003-PDF-Photo.jpg";

				if (! write_file($data['file_path'], $image)) {
					// writing file failed
				}
			}


			// Getting Film Types for dropdowns
			$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
			$data['sections'] = $this->section->get_all_sections($current_fest);
			$data['countries'] = $this->filmtype->get_all_type("country","asc");
			$data['languages'] = $this->filmtype->get_all_type("language","asc");
			$data['genres'] = $this->filmtype->get_all_type("genre","asc");
			$data['personnel'] = $this->filmtype->get_all_type("personnel","asc");
			$data['locations'] = $this->location->get_internal_festival_locations(explode(",",$data['festival'][0]->locations));		

			// for testing purposes, load it in the browser
			//$this->load->view('admin/generate_pdf', $data);

			// if you want to generate the file and send it to the browser
			$html = $this->load->view('admin/generate_pdf', $data, true);
			if ($data['screening_type'] == "feature" || $data['screening_type'] == "feature_shorts" ) {
				pdf_create($html, create_simple_slug(switch_title($data['film'][0]->title_en)));
			} else {
				pdf_create($html, create_simple_slug($data['film_screenings'][0]->program_name));
			}

			// if you want to write it to disk and/or send it as an attachment
			// $this->load->helper('file');
			// $data = pdf_create($html, '', false);
			// write_file('name', $data);
		}
	}
}

/* End of file film_listing.php */
/* Location: ./system/application/controllers/admin/film_listing.php */
?>