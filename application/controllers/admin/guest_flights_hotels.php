<?php
class Guest_Flights_Hotels extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$data['days'] = array();
		$startdate = strtotime($data['festival'][0]->startdate) - (3 * (24 * 60 * 60));
		$enddate = strtotime($data['festival'][0]->enddate) + (3 * (24 * 60 * 60));
		for ($x = $startdate; $x <= $enddate; $x = $x + (24 * 60 * 60) ) {
			$data['days'][] = date("Y-m-d", $x);
		}

		$data['guests'] = $this->guest->get_all_guests($current_fest);
		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$data['guestlist'] = array(); foreach ($data['guestids'] as $guests) { $data['guestlist'][] = $guests->guest_id; }
		$data['airlines'] = $this->guesttype->get_all_type("airlines", "asc", "no");

		if (count($data['guestlist']) > 0) {
			$data['flights'] =  $this->flight->get_all_flights($data['guestlist'], "asc");
			$data['hotels'] =  $this->hotel->get_all_hotels($data['guestlist'], "asc");
		} else {
			$data['flights'] = array();
			$data['hotels'] = array();
		}

		foreach ($data['guests'] as $guests) {
			$guests->DepartureDate = $guests->ArrivalDate = $guests->checkin = $guests->checkout = "";
			foreach ($data['flights'] as $flights) {
				if ($flights->festivalguest_id == $guests->guest_id) {
					$guests->DepartureDate .= date("n/j/Y",strtotime($flights->DepartureDate));
					$guests->ArrivalDate .= date("n/j/Y",strtotime($flights->ArrivalDate));
				}
			}
			foreach ($data['hotels'] as $hotels) {
				if ($hotels->festivalguest_id == $guests->guest_id) {
					$guests->checkin .= date("n/j/Y",strtotime($hotels->checkin));
					$guests->checkout .= date("n/j/Y",strtotime($hotels->checkout));
				}
			}
		}

		$vars['title'] = "List of Guest Flights/Hotels";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_flights_hotels";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_flights_hotels', $data);
		$this->load->view('admin/footer', $vars);
	}

}

/* End of file guest_flights_hotels.php */
/* Location: ./system/application/controllers/admin/guest_flights_hotels.php */
?>