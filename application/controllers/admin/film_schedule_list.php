<?php
class Film_Schedule_List extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['screening_json'] = json_encode($data['full_schedule']);
		$data['program_num'] = findNumberOfPrograms($data['schedule']);

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['films']);

		$data['locations'] = $this->location->get_internal_festival_locations(explode(",",$data['festival'][0]->locations));
		$data['location_sel'] = "0";

		$vars['title'] = "Film Schedule (List)";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_schedule_list";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_schedule_list', $data);
		$this->load->view('admin/footer', $vars);
	}

	function location($location_slug)
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $this->schedule->get_schedule_by_venue($data['festival'][0]->startdate, $data['festival'][0]->enddate, $location_slug);
		$data['screening_json'] = json_encode($data['full_schedule']);
		$data['program_num'] = findNumberOfPrograms($data['schedule']);

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['films']);

		$data['locations'] = $this->location->get_internal_festival_locations(explode(",",$data['festival'][0]->locations));
		$data['location_sel'] = $location_slug;

		$vars['title'] = "Film Schedule (List)";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_schedule_list";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_schedule_list', $data);
		$this->load->view('admin/footer', $vars);
	}

	function add_screening() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		
		$film_title_01 = ""; $film_slug_01 = "";
		foreach ($data['filmids'] as $thisFilm) {
			if ($thisFilm->movie_id == $this->input->post('scrn-film-01')) {
				$film_title_01 = $thisFilm->label;
				$film_slug_01 = $thisFilm->value;
			}
		}
		
		$locations = $this->location->get_all_locations();
		$location_id1 = 0; $location_id2 = 0;
		foreach ($locations as $thisLocation) {
			if ($thisLocation->id == $this->input->post('scrn-location-new')) { $location_id1 = $thisLocation->name; }
			if ($thisLocation->id == $this->input->post('scrn-location2-new')) { $location_id2 = $thisLocation->name; }
		}
		
		$total_films = $this->input->post('total_films');
		$film_ids = array();
		for ($x=1; $x<=$total_films; $x++) {
			if ($x <= 9) { $z = "0";} else { $z = ""; }
			$film_ids[] = $this->input->post('scrn-film-'.$z.$x);
		}
		$film_ids_string = convert_multiselect_to_string($film_ids);

		// Runtime in minutes converted to 00:00:00 time type
		$total_runtime = convert_min_to_time($this->input->post('total-runtime'));

		$values = array(
			"movie_id" => $this->input->post('scrn-film-01'),
			"program_movie_ids" => $film_ids_string,
			"program_name" => $this->input->post('scrn-program-name-new'),
			"program_desc" => $this->input->post('scrn-program-desc-new'),
			"date" => date("Y-m-d",strtotime($this->input->post('scrn-date-new'))),
			"time" => date("H:i:s",strtotime($this->input->post('scrn-time-new'))),
			"location_id" => $this->input->post('scrn-location-new'),
			"location_id2" => $this->input->post('scrn-location2-new'),
			"url" => $this->input->post('scrn-ticket-new'),
			"host" => $this->input->post('scrn-host-new'),
			"notes" => $this->input->post('scrn-notes-new'),
			"Published" => $this->input->post('scrn-published-new'),
			"Private" => $this->input->post('scrn-private-new'),
			"Rush" => $this->input->post('scrn-rush-new'),
			"Free" => $this->input->post('scrn-free-new'),
			"AudienceCount" => $this->input->post('audience-count-new'),
			"ScreeningRevenue" => $this->input->post('screening-revenue-new'),

			"intro_length" => $this->input->post('intro-length'),
			"intro" => $this->input->post('intro'),
			"fest_trailer_length" => $this->input->post('fest-trailer-length'),
			"fest_trailer" => $this->input->post('fest-trailer'),
			"sponsor_trailer1_name" => $this->input->post('sponsor-trailer1-name'),
			"sponsor_trailer1_length" => $this->input->post('sponsor-trailer1-length'),
			"sponsor_trailer1" => $this->input->post('sponsor-trailer1'),
			"sponsor_trailer2_name" => $this->input->post('sponsor-trailer2-name'),
			"sponsor_trailer2_length" => $this->input->post('sponsor-trailer2-length'),
			"sponsor_trailer2" => $this->input->post('sponsor-trailer2'),
			"q_and_a_length" => $this->input->post('q-and-a-length'),
			"q_and_a" => $this->input->post('q-and-a'),
			"total_runtime" => $total_runtime,
			"updated_date" => date("Y-m-d H:i:s")
		);
		if ($this->input->post('scrn-program-name-new') != "") { $values["program_slug"] = create_slug($this->input->post('scrn-program-name-new')); } else { $values["program_slug"] = ""; }
		if ($this->input->post('sponsor-trailer1-name') == "Sponsor Trailer 1") { $values["sponsor_trailer1_name"] = ""; }
		if ($this->input->post('sponsor-trailer2-name') == "Sponsor Trailer 2") { $values["sponsor_trailer2_name"] = ""; }
		if ($this->input->post('scrn-published-new')) { $values["Published"] = 1; } else { $values["Published"] = 0; }
		if ($this->input->post('scrn-private-new')) { $values["Private"] = 1; } else { $values["Private"] = 0; }
		if ($this->input->post('scrn-rush-new')) { $values["Rush"] = 1; } else { $values["Rush"] = 0; }
		if ($this->input->post('scrn-free-new')) { $values["Free"] = 1; } else { $values["Free"] = 0; }
		if ($this->input->post('intro')) { $values["intro"] = 1; } else { $values["intro"] = 0; }
		if ($this->input->post('fest-trailer')) { $values["fest_trailer"] = 1; } else { $values["fest_trailer"] = 0; }
		if ($this->input->post('sponsor-trailer1')) { $values["sponsor_trailer1"] = 1; } else { $values["sponsor_trailer1"] = 0; }
		if ($this->input->post('sponsor-trailer2')) { $values["sponsor_trailer2"] = 1; } else { $values["sponsor_trailer2"] = 0; }
		if ($this->input->post('q-and-a')) { $values["q_and_a"] = 1; } else { $values["q_and_a"] = 0; }
		$screening_id = $this->schedule->add_value($values,"wf_screening");

		$startTime = strtotime($values["date"]." ".$values["time"]);
		$length = 0;
		foreach ($film_ids as $thisFilmID) {
			foreach ($data['films'] as $thisFilm) {
				if ($thisFilmID == $thisFilm->movie_id) { $length = $length + $thisFilm->runtime_int; }
			}
		}
		if ($values["intro"] == 1) { $length += $values["intro_length"]; }
		if ($values["fest_trailer"] == 1) { $length += $values["fest_trailer_length"]; }
		if ($values["sponsor_trailer1"] == 1) { $length += $values["sponsor_trailer1_length"]; }
		if ($values["sponsor_trailer2"] == 1) { $length += $values["sponsor_trailer2_length"]; }
		if ($values["q_and_a"] == 1) { $length += $values["q_and_a_length"]; }
		$endTime = $startTime + intval($length*60);

		
		// Print out replacement row and new row
        print "<tr id=\"addScreeningHere\">";
        print "<td colspan=\"8\"><input type=\"hidden\" id=\"screening-id-new\" name=\"screening-id-new\" value=\"".$screening_id."\" /></td>";
        print "</tr>";

		print "\t<tr id=\"row-".$screening_id."\" valign=\"top\"";
		if ($values["Private"] == "1") {
			print " style=\"background-color:#DDDDDD;\"";
		}
		else if ($values["Rush"] == "1") {
			print " style=\"background-color:#FFCCCC;\"";
		}
		print ">\n";
		print "\t\t<td><button id=\"scrn-".$screening_id."\" class=\"edit\" data-id=\"".$screening_id."\">Edit Screening</button></td>\n";
		print "\t\t<td><span class=\"schedule_time\">".date("g:i A",$startTime)." - ".date("g:i A",$endTime)."</span></td>\n";
		print "\t\t<td>";

		if (count($film_ids) == 1) { // Single Film
			print "<a href=\"/admin/film_edit/update/".$film_slug_01."#tabs-4\" class=\"schedule_title\">";
			print switch_title($film_title_01);
			print "</a>";
			if ($values["q_and_a"] == 1) { print " <img src=\"/assets/images/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }
		} else if (count($film_ids) > 1) { // Multiple Films
			if ($values["program_name"] != "") {
				print "<span style=\"color:#000000;\">".$values["program_name"]."</span>";
			} else {
				print "<a href=\"/admin/film_edit/update/".$film_slug_01."#tabs-4\" class=\"schedule_title\">";
				print switch_title($film_title_01);
				print "</a>";
			}
			if ($values["q_and_a"] == 1) { print " <img src=\"/assets/images/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }

			$filmlist = "";
			print "\t\t<br /><span class=\"shorts_title\">Plays with:</span> ";
			foreach ($film_ids as $thisFilmID2) {
				foreach ($data['filmids'] as $thisFilm2) {
					if ($thisFilmID2 == $thisFilm2->movie_id) {
						if ($values["program_name"] != "") {
							// Include first film in program if program name is defined
							$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
						} else {
							// Exclude first film in program if program name is not defined
							if ($values["movie_id"] != $thisFilmID2) {
								$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
							}
						}
					}
				}
			}
			$filmlist = trim($filmlist,", ");
			print $filmlist;
		}

		print "</td>\n";
		print "\t\t<td>".$location_id1."</td>\n";
		if ($values["Private"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Rush"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Free"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["url"] != "") {
			print "\t<td class=\"cent icon_tick\"></td>\n";
		} else {
			if ($values["Private"] == "1") {
				print "\t<td>Private</td>\n";
			} else if ($values["Free"] == "1") {
				print "\t<td>Free</td>\n";
			} else {
				print "\t<td class=\"cent icon_cross\"></td>\n";
			}
		}
		if ($values["Published"] != "0") {
			print "\t<td class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t</tr>\n";	
	}

	function update_screening() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$film_title_01 = ""; $film_slug_01 = "";
		foreach ($data['filmids'] as $thisFilm) {
			if ($thisFilm->movie_id == $this->input->post('scrn-film-update-01')) {
				$film_title_01 = $thisFilm->label;
				$film_slug_01 = $thisFilm->value;
			}
		}
		
		$locations = $this->location->get_all_locations();
		$location_id1 = 0; $location_id2 = 0;
		foreach ($locations as $thisLocation) {
			if ($thisLocation->id == $this->input->post('scrn-location-update')) { $location_id1 = $thisLocation->name; }
			if ($thisLocation->id == $this->input->post('scrn-location2-update')) { $location_id2 = $thisLocation->name; }
		}
		
		$total_films = $this->input->post('total_films_update');
		$film_ids = array();
		for ($x=1; $x<=$total_films; $x++) {
			if ($x <= 9) { $z = "0";} else { $z = ""; }
			$film_ids[] = $this->input->post('scrn-film-update-'.$z.$x);
		}
		$film_ids_string = convert_multiselect_to_string($film_ids);		

		// Runtime in minutes converted to 00:00:00 time type
		$total_runtime = convert_min_to_time($this->input->post('total-runtime-update'));

		$values = array(
			"movie_id" => $this->input->post('scrn-film-update-01'),
			"program_movie_ids" => $film_ids_string,
			"program_name" => $this->input->post('scrn-program-name-update'),
			"program_desc" => $this->input->post('scrn-program-desc-update'),
			"date" => date("Y-m-d",strtotime($this->input->post('scrn-date-update'))),
			"time" => date("H:i:s",strtotime($this->input->post('scrn-time-update'))),
			"location_id" => $this->input->post('scrn-location-update'),
			"location_id2" => $this->input->post('scrn-location2-update'),
			"url" => $this->input->post('scrn-ticket-update'),
			"host" => $this->input->post('scrn-host-update'),
			"notes" => $this->input->post('scrn-notes-update'),
			"Published" => $this->input->post('scrn-published-update'),
			"Private" => $this->input->post('scrn-private-update'),
			"Rush" => $this->input->post('scrn-rush-update'),
			"Free" => $this->input->post('scrn-free-update'),
			"AudienceCount" => $this->input->post('audience-count-update'),
			"ScreeningRevenue" => $this->input->post('screening-revenue-update'),
			"QandACount" => $this->input->post('q-and-a-count-update'),

			"intro_length" => $this->input->post('intro-length-update'),
			"intro" => $this->input->post('intro-update'),
			"fest_trailer_length" => $this->input->post('fest-trailer-length-update'),
			"fest_trailer" => $this->input->post('fest-trailer-update'),
			"sponsor_trailer1_name" => $this->input->post('sponsor-trailer1-name-update'),
			"sponsor_trailer1_length" => $this->input->post('sponsor-trailer1-length-update'),
			"sponsor_trailer1" => $this->input->post('sponsor-trailer1-update'),
			"sponsor_trailer2_name" => $this->input->post('sponsor-trailer2-name-update'),
			"sponsor_trailer2_length" => $this->input->post('sponsor-trailer2-length-update'),
			"sponsor_trailer2" => $this->input->post('sponsor-trailer2-update'),
			"q_and_a_length" => $this->input->post('q-and-a-length-update'),
			"q_and_a" => $this->input->post('q-and-a-update'),
			"total_runtime" => $total_runtime,
			"updated_date" => date("Y-m-d H:i:s")
		);
		$screening_id = $this->input->post('screening_id_update');
		if ($this->input->post('scrn-program-name-update') != "") { $values["program_slug"] = create_slug($this->input->post('scrn-program-name-update')); } else { $values["program_slug"] = ""; }
		if ($this->input->post('sponsor-trailer1-name-update') == "Sponsor Trailer 1") { $values["sponsor_trailer1_name"] = ""; }
		if ($this->input->post('sponsor-trailer2-name-update') == "Sponsor Trailer 2") { $values["sponsor_trailer2_name"] = ""; }
		if ($this->input->post('scrn-published-update')) { $values["Published"] = 1; } else { $values["Published"] = 0; }
		if ($this->input->post('scrn-private-update')) { $values["Private"] = 1; } else { $values["Private"] = 0; }
		if ($this->input->post('scrn-rush-update')) { $values["Rush"] = 1; } else { $values["Rush"] = 0; }
		if ($this->input->post('scrn-free-update')) { $values["Free"] = 1; } else { $values["Free"] = 0; }
		if ($this->input->post('intro-update')) { $values["intro"] = 1; } else { $values["intro"] = 0; }
		if ($this->input->post('fest-trailer-update')) { $values["fest_trailer"] = 1; } else { $values["fest_trailer"] = 0; }
		if ($this->input->post('sponsor-trailer1-update')) { $values["sponsor_trailer1"] = 1; } else { $values["sponsor_trailer1"] = 0; }
		if ($this->input->post('sponsor-trailer2-update')) { $values["sponsor_trailer2"] = 1; } else { $values["sponsor_trailer2"] = 0; }
		if ($this->input->post('q-and-a-update')) { $values["q_and_a"] = 1; } else { $values["q_and_a"] = 0; }
		$this->schedule->update_value($values,"wf_screening",$screening_id);

		$startTime = strtotime($values["date"]." ".$values["time"]);
		$length = 0;
		foreach ($film_ids as $thisFilmID) {
			foreach ($data['films'] as $thisFilm) {
				if ($thisFilmID == $thisFilm->movie_id) { $length = $length + $thisFilm->runtime_int; }
			}
		}
		if ($values["intro"] == 1) { $length += $values["intro_length"]; }
		if ($values["fest_trailer"] == 1) { $length += $values["fest_trailer_length"]; }
		if ($values["sponsor_trailer1"] == 1) { $length += $values["sponsor_trailer1_length"]; }
		if ($values["sponsor_trailer2"] == 1) { $length += $values["sponsor_trailer2_length"]; }
		if ($values["q_and_a"] == 1) { $length += $values["q_and_a_length"]; }
		$endTime = $startTime + intval($length*60);
		

		// Print out replacement row
		print "\t<tr id=\"row-".$screening_id."\" valign=\"top\"";
		if ($values["Private"] == "1") {
			print " style=\"background-color:#DDDDDD;\"";
		}
		if ($values["Rush"] == "1") {
			print " style=\"background-color:#FFCCCC;\"";
		}
		print ">\n";
		print "\t\t<td><button class=\"edit\" data-id=\"".$screening_id."\">Edit Screening</button></td>\n";
		print "\t\t<td><span class=\"schedule_time\">".date("g:i A",$startTime)." - ".date("g:i A",$endTime)."</span></td>\n";
		print "\t\t<td>";

		if (count($film_ids) == 1) { // Single Film
			print "<a href=\"/admin/film_edit/update/".$film_slug_01."#tabs-4\" class=\"schedule_title\">";
			print switch_title($film_title_01);
			print "</a>";
			if ($values["q_and_a"] == 1) { print " <img src=\"/assets/images/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }
		} else if (count($film_ids) > 1) { // Multiple Films
			if ($values["program_name"] != "") {
				print "<span style=\"color:#000000;\">".$values["program_name"]."</span>";
			} else {
				print "<a href=\"/admin/film_edit/update/".$film_slug_01."#tabs-4\" class=\"schedule_title\">";
				print switch_title($film_title_01);
				print "</a>";
			}
			if ($values["q_and_a"] == 1) { print " <img src=\"/assets/images/qa2.png\" width=\"36\" height=\"18\" class=\"qa2\" />"; }

			$filmlist = "";
			print "\t\t<br /><span class=\"shorts_title\">Plays with:</span> ";
			foreach ($film_ids as $thisFilmID2) {
				foreach ($data['filmids'] as $thisFilm2) {
					if ($thisFilmID2 == $thisFilm2->movie_id) {
						if ($values["program_name"] != "") {
							// Include first film in program if program name is defined
							$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
						} else {
							// Exclude first film in program if program name is not defined
							if ($values["movie_id"] != $thisFilmID2) {
								$filmlist .= "<a href=\"/admin/film_edit/update/".$thisFilm2->value."#tabs-4\" class=\"shorts_title\">".switch_title($thisFilm2->label)."</a>, ";
							}
						}
					}
				}
			}
			$filmlist = trim($filmlist,", ");
			print $filmlist;
		}
		print "</td>\n";
		print "\t\t<td>".$location_id1."</td>\n";
		if ($values["Private"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Rush"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Free"] == "1") {
			print "\t<td class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["url"] != "") {
			print "\t<td class=\"cent icon_tick\"></td>\n";
		} else {
			if ($values["Private"] == "1") {
				print "\t<td>Private</td>\n";
			} else if ($values["Free"] == "1") {
				print "\t<td>Free</td>\n";
			} else {
				print "\t<td class=\"cent icon_cross\"></td>\n";
			}
		}
		if ($values["Published"] != "0") {
			print "\t<td class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t</tr>\n";
	}

	function return_screening_json($start_date, $end_date) {
		$this->load->model('Schedulemodel','schedule');
		print json_encode($this->schedule->get_internal_schedule($start_date, $end_date));
	}

	function delete_screening() {
		$this->load->model('Schedulemodel','schedule');
		$screening_id = $this->input->post('screening_id_update');		
		$this->schedule->delete_value($screening_id, "wf_screening");
	}

	function add_screening_film_update_multiple($new_total) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
	
		$film_screening_array = convert_film_screenings_to_array($data['films']);		
		$program_movie_ids = explode(",",$this->input->post('program_movie_ids_update'));
		
		$x = 1;
		foreach ($program_movie_ids as $thisScreening) {
			$film_runtime = 0;
			foreach ($film_screening_array as $thisFilm) {
				if ($thisFilm['movie_id'] == $thisScreening) {
					$film_runtime = $thisFilm['runtime'];
				}
			}

			if ($x <= 9) {$z = "0";} else {$z = "";}		
			print "\t\t\t\t\t\t<div class=\"newFilmEntry\">\n";
			print "\t\t\t\t\t\t\t<label>Film ".$x."</label> <div id=\"scrn-film-update-".$z.$x."-length\" style=\"float:right; margin-right:37px\">".$film_runtime." min</div>\n";
			print "\t\t\t\t\t\t\t<div>";
			print form_dropdown_film('scrn-film-update-'.$z.$x, $film_screening_array, $thisScreening, " id='scrn-film-update-".$z.$x."' class='select-film2 ui-widget-content ui-corner-all'");
			if ($x != 1) { // Don't show a delete button for the first film.
				print "&nbsp;&nbsp;<a href=\"#\" id=\"scrn-film-update-del-".$z.$x."\"><img src=\"/assets/images/cancel.png\" alt=\"Remove Film\" title=\"Remove Film\" width=\"16\" height=\"16\" border=\"0\"></a>\n";
			}
			print "\t\t\t\t\t\t\t</div>\n";
			print "\t\t\t\t\t\t</div>\n\n";
			$x++;
		}
		print "\t\t\t\t\t\t<div id=\"addNewFilmsHere-update\"></div>\n";
	}

	function publish_all()
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');

		$film_ids = $this->films->get_all_film_ids2($this->input->post('festival_id'));
		$film_id_array = convert_to_array3($film_ids);

		$screening_ids = $this->schedule->get_internal_screening_ids($film_id_array);
		$screening_id_array = convert_to_array3($screening_ids);

		if (count($screening_id_array) > 0) {
			$this->schedule->publish_all_public_screenings($screening_id_array, $film_id_array);
		}
	}

	function unpublish_all()
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');

		$data['filmids'] = $this->films->get_all_internal_film_ids($this->input->post('festival_id'));
		$film_id_array = convert_to_array3($data['filmids']);

		$screeningids = $this->schedule->get_internal_screening_ids($film_id_array);
		$screening_id_array = convert_to_array3($screeningids);

		$this->schedule->unpublish_all_screenings($screening_id_array);
	}

}

/* End of file film_schedule_list.php */
/* Location: ./system/application/controllers/admin/film_schedule_list.php */
?>