<?php
class Film_EventivalSync extends CI_Controller {

	private $eventival_api_url = "https://eventival.eu/HIFF/hiff2016/en/ws/";
	private $eventival_api_key = "CBYlNCyW5QCn79OXbiF0nY6iUVSw1B";
	private $xml_file_dir_path = "./assets/xml/";
	private $eventival_import_festival_id = "29";
	private $film_confirmed = "1";
	private $film_published = "0";
	private $screening_published = "0";
	private $disable_sync = false;
	private $material_photos = "Photo Stills";
	private $material_sponsor_logo = "Sponsor Logo";
	private $material_sponsor_link = "Sponsor Link";

	function __construct()
	{
		parent::__construct();
		//check_login();
		require_once("helper_functions.php");
	}


	function index()
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Locationmodel', 'location');
		$this->load->model('Sponsormodel', 'sponsor');
		$this->load->model('Syncmodel','sync');

		// Gathering data from the database
		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['filmids']); // uses 'festivalmovie.movie_id'
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);

		// all processing moved to get_xml() and sync() functions
		$eventival = explode("/", $this->eventival_api_url);
		$data['project'] = $eventival[3];
		$data['edition'] = $eventival[4];
		$data['api_url'] = $this->eventival_api_url.$this->eventival_api_key."/";
		if ($this->film_published == 1) { $data['film_publish'] = "YES"; } else { $data['film_publish'] = "NO"; }
		if ($this->screening_published == 1) { $data['screening_publish'] = "YES"; } else { $data['screening_publish'] = "NO"; }
		$filename = "./assets/xml/films.xml";
		date_default_timezone_set('Pacific/Honolulu');
		if (file_exists($filename)) {
    		$data['last_sync'] = date ("F d, Y - H:i:s", filemtime($filename));
		} else {
			$data['last_sync'] = "Has not been synced yet.";
		}

		$vars['schedule'] = convert_to_array_schedule2($data['full_schedule']);
		$vars['title'] = "Eventival Sync";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_eventivalsync";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_eventivalsync', $data);
		$this->load->view('admin/footer', $vars);
	}


	function get_xml()
	{
		$this->load->helper('file');

		$data['output'] = "";
		$results = array();
		$error = false;
		$base_url = $this->eventival_api_url.$this->eventival_api_key;
		$filmid_array = $films_array = $packageid_array = array();
		$packages_array = $sections_array = $cinemas_array = $venues_array = array();

		$filmlist_url = 	$base_url."/films.xml";
		$screenings_url = 	$base_url."/films/screenings.xml";
		$filmpackages_url = $base_url."/film-packages.xml";
		$sections_url = 	$base_url."/films/sections.xml";
		$cinemas_url = 		$base_url."/films/cinemas.xml";
		$venues_url = 		$base_url."/venues.xml";

		/* --------- Get XML from Web Services ----------- */

		if ($this->disable_sync != true) {
			// Sections
			// $sections_xml = get_xml_from_api($sections_url);
			// if (! write_file($this->xml_file_dir_path."films/sections.xml", $sections_xml)) {
			// 	$error = true;
			// 	$data['output'] .= "<p class='error'>Failed to retrieve section list XML</p>\n";
			// }

			// Cinemas
			// $cinemas_xml = get_xml_from_api($cinemas_url);
			// if (! write_file($this->xml_file_dir_path."films/cinemas.xml", $cinemas_xml)) {
			// 	$error = true;
			// 	$data['output'] .= "<p class='error'>Failed to retrieve cinema list XML</p>\n";
			// }

			// Venues
			$venues_xml = get_xml_from_api($venues_url);
			if (! write_file($this->xml_file_dir_path."venues.xml", $venues_xml)) {
				$error = true;
				$data['output'] .= "<p class='error'>Failed to retrieve venue list XML</p>\n";
			}

			// Film ID List
			$films_xml = get_xml_from_api($filmlist_url);
			if (! write_file($this->xml_file_dir_path."films.xml", $films_xml)) {
				$error = true;
				$data['output'] .= "<p class='error'>Failed to retrieve film list XML</p>\n";
			}

			// Screenings
			$screenings_xml = get_xml_from_api($screenings_url);
			if (! write_file($this->xml_file_dir_path."films/screenings.xml", $screenings_xml)) {
				$error = true;
				$data['output'] .= "<p class='error'>Failed to retrieve screenings XML</p>\n";
			}

			// Film Packages - not needed unless package descriptions are added
			$packages_xml = get_xml_from_api($filmpackages_url);
			if (! write_file($this->xml_file_dir_path."film-packages.xml", $packages_xml)) {
				$error = true;
				$data['output'] .= "<p class='error'>Failed to retrieve film packages XML</p>\n";
			}

			// Retrieve Film Details (multiple)
			$ev_film_urls = array();
			if ($films_xml != false) {
				$film_ids = new SimpleXMLElement($films_xml);
				foreach ($film_ids->item as $thisFilm) { $filmid_array[(string)$thisFilm->id] = (string)$thisFilm->title_english; }
			}
			if ( count($filmid_array) > 0) {
				$count = 0;
				foreach ($filmid_array as $thisFilmId => $thisFilmName) {
					$ev_film_urls[$count] = $this->eventival_api_url.$this->eventival_api_key."/films/".$thisFilmId.".xml";
					$count++;
					// Artificial limit for testing purposes - REMOVE IN PRODUCTION
					//if ($count >= 5) { break; }
				}

				$results = get_xml_from_api_multi($ev_film_urls);
				foreach ($results as $key => $filmdetail_xml) {
					$filmData = new SimpleXMLElement($filmdetail_xml);
					$thisFilmId = (string)$filmData->ids->system_id;
					$thisFilmName = (string)$filmData->titles->title_english;

					if (! write_file($this->xml_file_dir_path."films/".$thisFilmId.".xml", $filmdetail_xml)) {
						$error = true;
						$data['output'] .= "<p class='error'>Failed to retrieve film detail XML (#".$thisFilmId." - ".$thisFilmName.")</p>\n";
					}
				}
			}

			// Retrieve Film Package Details (multiple)
			$ev_package_urls = array();
			if ($packages_xml != false) {
				$film_packages = new SimpleXMLElement($packages_xml);
				foreach ($film_packages->item as $thisPackage) { $packageid_array[(string)$thisPackage->id] = (string)$thisPackage->title_english; }
			}
			if ( count($packageid_array) > 0) {
				$count = 0;
				foreach ($packageid_array as $thisPackageId => $thisPackageName) {
					$ev_package_urls[$count] = $this->eventival_api_url.$this->eventival_api_key."/film-packages/".$thisPackageId.".xml";
					$count++;
					// Artificial limit for testing purposes - REMOVE IN PRODUCTION
					// if ($count >= 5) { break; }
				}

				$results = get_xml_from_api_multi($ev_package_urls);
				foreach ($results as $key => $packagedetail_xml) {
					$packageData = new SimpleXMLElement($packagedetail_xml);
					$thisPackageId = (string)$packageData->ids->system_id;
					$thisPackageName = (string)$packageData->titles->title_english;

					if (! write_file($this->xml_file_dir_path."film-packages/".$thisPackageId.".xml", $packagedetail_xml)) {
						$error = true;
						$data['output'] .= "<p class='error'>Failed to retrieve package detail XML (#".$thisPackageId." - ".$thisPackageName.")</p>\n";
					}
				}
			}

			print $data["output"];
		}
	}


	function sync()
	{

		$this->load->helper('file');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Locationmodel', 'location');
		$this->load->model('Sponsormodel', 'sponsor');
		$this->load->model('Syncmodel','sync');

		$data['output'] = "";
		$error = false;
		$filmid_array = $films_array = $packageid_array = array();
		$packages_array = $sections_array = $locations_array = $venues_array = array();

		/* --------- Read XML files from disk ----------- */
		// if ($sections_xml != false) {
		// 	$sections = new SimpleXMLElement($sections_xml);
		// } else {
		// 	$error = true;
		// 	$data['output'] .= "<p class='error'>Failed to read section list XML file</p>\n";
		// }

		// $cinemas_xml = read_file($this->xml_file_dir_path."films/cinemas.xml");
		// if ($cinemas_xml != false) {
		// 	$cinemas = new SimpleXMLElement($cinemas_xml);
		// } else {
		// 	$error = true;
		// 	$data['output'] .= "<p class='error'>Failed to read cinema list XML file</p>\n";
		// }

		$venues_xml = read_file($this->xml_file_dir_path."venues.xml");
		if ($venues_xml != false) {
			$venues = new SimpleXMLElement($venues_xml);
		} else {
			$error = true;
			$data['output'] .= "<p class='error'>Failed to read venue list XML file</p>\n";
		}

		$films_xml = read_file($this->xml_file_dir_path."films.xml");
		if ($films_xml != false) {
			$film_ids = new SimpleXMLElement($films_xml);
			foreach ($film_ids->item as $thisFilm) {
				$status = (string)$thisFilm->selection->selection_status;
				if ($status == "Failed" || $status == "On Hold" || $status == "Postponed" || $status == "Withdrawn" || $status == "Not Selected") {
					// skip this film
				} else {
					$filmid_array[(string)$thisFilm->id] = (string)$thisFilm->title_english;
				}
			}
		} else {
			$error = true;
			$data['output'] .= "<p class='error'>Failed to read film list XML file</p>\n";
		}

		// Retrieve Film Details
		if ( count($filmid_array) > 0) {
			$count = 0;
			foreach ($filmid_array as $thisFilmId => $thisFilmName) {
				$filmdetail_xml = read_file($this->xml_file_dir_path."films/".$thisFilmId.".xml");
				if ($filmdetail_xml != false) {
					$films_array[] = new SimpleXMLElement($filmdetail_xml);
				} else {
					$error = true;
					$data['output'] .= "<p class='error'>Failed to read film detail XML file (#".$thisFilmId." - ".$thisFilmName.")</p>\n";
				}
				$count++;
				// Artificial limit for testing purposes - REMOVE IN PRODUCTION
				//if ($count >= 3) { break; }
			}
		}

		$screenings_xml = read_file($this->xml_file_dir_path."films/screenings.xml");
		if ($screenings_xml != false) {
			$screenings = new SimpleXMLElement($screenings_xml);
		} else {
			$error = true;
			$data['output'] .= "<p class='error'>Failed to read screenings XML file</p>\n";
		}

		$packages_xml = read_file($this->xml_file_dir_path."film-packages.xml");
		if ($packages_xml != false) {
			$film_packages = new SimpleXMLElement($packages_xml);
			foreach ($film_packages->item as $thisPackage) { $packageid_array[(string)$thisPackage->id] = (string)$thisPackage->title_english; }
		} else {
			$error = true;
			$data['output'] .= "<p class='error'>Failed to read film packages XML file</p>\n";
		}

		// Retrieve Film Package Details
		if ( count($packageid_array) > 0) {
			foreach ($packageid_array as $thisPackageId => $thisPackageName) {
				$packagedetail_xml = read_file($this->xml_file_dir_path."film-packages/".$thisPackageId.".xml");
				if ($packagedetail_xml != false) {
					$packages_array[] = new SimpleXMLElement($packagedetail_xml);
				} else {
					$error = true;
					$data['output'] .= "<p class='error'>Failed to read package detail XML file (#".$thisPackageId." - ".$thisPackageName.")</p>\n";
				}
			}
		}

		/* --------- Process XML and pull out relevant information ----------- */
		// When retreiving a final value, add a cast for (string). Otherwise you will get a SimpleXML object.

		// Process Sections XML
		// foreach ($sections->section as $thisSection) {
		// 	$sections_array[(string)$thisSection->id] = (string)$thisSection->name;
		// }

		// Process Cinemas XML
		// foreach ($cinemas->cinema as $thisCinema) {
		// 	$cinema_halls = $thisCinema->cinema_halls;
		// 	$cinema_halls_array = array();
		// 	foreach ($cinema_halls->cinema_hall as $thisHall) {
		// 		$cinema_halls_array[] = array("id" => (string)$thisHall->id, "name" => (string)$thisHall->name, "abbreviation" => (string)$thisHall->abbreviation, "capacity" => (string)$thisHall->capacity); 
		// 	}
		// 	$cinemas_array[] = array("cinema_id" => (string)$thisCinema->id, "cinema_name" => (string)$thisCinema->name, "cinema_halls" => $cinema_halls_array);
		// }

		// $venueList = $this->location->get_all_venues();
		// $locationList = $this->location->get_all_locations();
		// foreach ($cinemas_array as $key => $thisVenue) {
		// 	$locationIdArray = array();
		// 	foreach ($thisVenue["cinema_halls"] as $subkey => $thisLocation) {
		// 		$locationId	= $this->sync->check_add_location($thisLocation, $locationList);
		// 		$locationIdArray[] = $locationId;
		// 		$cinemas_array[$key]["cinema_halls"][$subkey]["location_id"] = $locationId;
		// 	}

		// 	$venueId = $this->sync->check_add_venue($thisVenue["cinema_name"], $venueList, $locationIdArray);
		// 	$cinemas_array[$key]["venue_id"] = $venueId;
		// }

		// Process Venues/Locations XML
		foreach ($venues->venue as $thisVenue) {
			$venues_array[] = array("venue_id" => (string)$thisVenue->company_id, "venue_name" => (string)$thisVenue->company);
			$locations_array[] = array("id" => (string)$thisVenue->id, "name" => (string)$thisVenue->name, "abbreviation" => (string)$thisVenue->abbreviation, "capacity" => (string)$thisVenue->capacity, "venue_id" => (string)$thisVenue->company_id); 
		}
		$venues_array = array_unique($venues_array);

		$locationList = $this->location->get_all_locations();
		foreach ($locations_array as $key => $thisLocation) {
			$locationId	= $this->sync->check_add_location($thisLocation, $locationList);
			$locations_array[$key]["location_id"] = $locationId;
		}

		$venueList = $this->location->get_all_venues();
		foreach ($venues_array as $key => $thisVenue) {
			$locationIdArray = array();
			foreach ($locations_array as $subkey => $thisLocation) {
				if ($thisVenue["venue_id"] == $thisLocation["venue_id"]) {
					$locationIdArray[] = $thisLocation["location_id"];
				}
			}
			$venueId = $this->sync->check_add_venue($thisVenue["venue_name"], $venueList, $locationIdArray);
			$venues_array[$key]["venue_id"] = $venueId;
		}

		// Process Film Details XML
		$currentPersList = $this->personnel->get_personnel_list();
		$currentSponsorList = $this->sponsor->get_all_sponsors();
		$films_container = array();
		foreach ($films_array as $thisFilm) {
			$film = array();

			// Film Info
			$film["eventival_film_id"] = (string)$thisFilm->ids->system_id;
			$film["festpro_film_id"] = "";
			$film["festival_id"] = $this->eventival_import_festival_id;
			$film["Confirmed"] = $this->film_confirmed;
			$film["Published"] = $this->film_published;
			$film["title_en"] = (string)$thisFilm->titles->title_english;
			$film["title"] = (string)$thisFilm->titles->title_original;
			$film["slug"] = create_slug($film["title_en"]);
			$film["year"] = (string)$thisFilm->film_info->completion_date->year;
			$runtimeSeconds = intval((string)$thisFilm->film_info->runtime->seconds);
			$film["runtime_int"] = floor($runtimeSeconds/60);
			$film["synopsis"] = (string)$thisFilm->film_info->texts->long_synopsis;
			$film["synopsis_short"] = (string)$thisFilm->film_info->texts->short_synopsis;
			$film["synopsis_original"] = (string)$thisFilm->film_info->texts->directors_statement;
			$film["websiteurl"] = (string)$thisFilm->film_info->website_official_url;

			// Section, Video Format, Premiere Type, Event Type
			$filmSection = "";
			$sections = $thisFilm->eventival_categorization->sections;
			foreach ($sections as $thisSection) {
				$filmSection = (string)$thisSection->section->name; break;
			}
			$film["category_id"] = $this->sync->check_add_section(explode("~",$filmSection));

			$filmFormat = "";
			$video_formats = $thisFilm->film_info->formats->exhibition_formats;
			foreach ($video_formats as $thisFormat) {
				$filmFormat = (string)$thisFormat->item->medium; break;
			}
			$film["format_id"] = $this->sync->check_add_film_type("format", explode("~",$filmFormat));

			$premiereType = (string)$thisFilm->film_info->premiere_type;
			$film["premiere_id"] = $this->sync->check_add_film_type("premiere", explode("~",$premiereType));

			$filmEventType = "";
			$eventTypes = $thisFilm->film_info->types->type;
			foreach ($eventTypes as $thisType) {
				if ((string)$thisType != "") { $filmEventType = (string)$thisType; break; }
			}
			$film["event_id"] = $this->sync->check_add_film_type("event", explode("~",$filmEventType));

			// Languages, Countries, Genres - using genres for spring, categories for fall?
			$film_languages = $film_subtitles = $film_countries = $film_genres = array();
			$languages = $thisFilm->film_info->languages;
			foreach ($languages->language as $thisItem) { $film_languages[] = (string)$thisItem->name; }
			$subtitles = $thisFilm->film_info->subtitle_languages;
			foreach ($subtitles->subtitle_language as $thisItem) { $film_subtitles[] = (string)$thisItem->name; }
			$countries = $thisFilm->film_info->countries;
			foreach ($countries->country as $thisItem) { $film_countries[] = (string)$thisItem->name; }
			//$genres = $thisFilm->film_info->genres->genre;
			//foreach ($genres as $thisItem) { $film_genres[] = (string)$thisItem; }
			$categories = $thisFilm->eventival_categorization->categories->category;
			foreach ($categories as $thisItem) { $film_genres[] = (string)$thisItem; }

			$film["film_languages"] = $this->sync->check_add_film_type("language", $film_languages);
			$film["film_subtitles"] = $this->sync->check_add_film_type("language", $film_subtitles);
			$film["film_countries"] = $this->sync->check_add_film_type("country", $film_countries);
			$film["film_genres"] = $this->sync->check_add_film_type("genre", $film_genres);

			// Film Personnel
			$film_personnel = array();
			$directors = $thisFilm->film_info->relationships->directors;
			foreach ($directors->director as $thisPerson) {
				$film_personnel[] = array("role" => "Director", "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);
			}
			$producers = $thisFilm->film_info->relationships->producers;
			foreach ($producers->producer as $thisPerson) {
				$film_personnel[] = array("role" => "Producer", "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);
			}
			$writers = $thisFilm->film_info->relationships->writers;
			foreach ($writers->writer as $thisPerson) {
				$film_personnel[] = array("role" => "Writer", "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);
			}
			$cast = $thisFilm->film_info->relationships->cast;
			foreach ($cast->cast as $thisPerson) {
				$film_personnel[] = array("role" => "Cast", "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);
			}
			$crew = $thisFilm->film_info->relationships->crew;
			foreach ($crew->crew as $thisPerson) {
				$otherCrew = "Other Crew";
				if ( (string)$thisPerson->activity == "") {
					$film_personnel[] = array("role" => $otherCrew, "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);
				} else {
					$film_personnel[] = array("role" => (string)$thisPerson->activity, "name" => (string)$thisPerson->name, "lastname" => (string)$thisPerson->surname);					
				}
			}
			$film["film_personnel"] = $this->sync->check_add_personnel($film_personnel, $currentPersList);

			// Multimedia + Sponsor Logos & Links
			$film["trailer"] = (string)$thisFilm->film_info->online_trailer_url;
			$film_photos = $sponsor_logos = $sponsor_links = array();
			$materials = $thisFilm->film_info->materials;
			foreach ($materials->material as $thisMaterial) {
				if ((string)$thisMaterial["user_type"] == $this->material_photos) {
					$film_photos[] = array("description" => (string)$thisMaterial["description"], "url" => (string)$thisMaterial["file"]);
				}
				if ((string)$thisMaterial["user_type"] == $this->material_sponsor_logo) {
					$sponsor_id = "0";
					$sponsor_name = (string)$thisMaterial["description"];
					if ($sponsor_name != "") {
						$sponsor_id = $this->sync->check_add_sponsor($sponsor_name, $currentSponsorList);
					}
					$sponsor_logos[] = array("description" => (string)$thisMaterial["description"], "url" => (string)$thisMaterial["file"], "sponsor_id" => $sponsor_id);
				}
				if ((string)$thisMaterial["user_type"] == $this->material_sponsor_link) {
					$sponsor_links[] = array("description" => (string)$thisMaterial["description"], "url" => (string)$thisMaterial["url"]);
				}
			}
			$film["film_photos"] = $film_photos;
			$film["sponsor_logos"] = $sponsor_logos;
			$film["sponsor_links"] = $sponsor_links;

			// Related Films - a.k.a. "My HIFF"
			$related_films = $thisFilm->film_info->relationships->films;
			$film["myhiff_1"] = $film["myhiff_2"] = $film["myhiff_3"] = $film["myhiff_4"] = "0";
			$counter = 1;
			foreach ($related_films->film as $thisFilm) {
				if ($counter <= 4) {
					$e_film_id = (string)$thisFilm->film_id;
					//$e_film_name = (string)$thisFilm->film_title_english;
					switch ($counter) {
						case 1:
							$film["myhiff_1"] = $e_film_id;
							break;
						case 2:
							$film["myhiff_2"] = $e_film_id;
							break;
						case 3:
							$film["myhiff_3"] = $e_film_id;
							break;
						case 4:
							$film["myhiff_4"] = $e_film_id;
							break;
					}
				}
				$counter++;
			}

			// Add to films container 
			$films_container[] = $film;
		}

		// Process Screenings XML - single films and film packages
		$film_screening_container = array();
		$location_ids = array();
		foreach ($screenings->screening as $thisScreening) {
			$film_screening = array();

			// Screening Status
			$private = false;
			$screening_code = strtolower((string)$thisScreening->code);
			if (strpos($screening_code, "free") !== false) {
				$film_screening["Free"] = "1";
			} else {
				$film_screening["Free"] = "0";
			}
			if (strpos($screening_code, "rush") !== false) {
				$film_screening["Rush"] = "1";
			} else {
				$film_screening["Rush"] = "0";
			}
			if (strpos($screening_code, "private") !== false) {
				$film_screening["Private"] = "1";
				$private = true;
			} else {
				$film_screening["Private"] = "0";
			}

			// Screening Info
			$locationId = "";
			$venue_id = (string)$thisScreening->venue_id;
			foreach ($locations_array as $thisLocation) {
				if ($thisLocation["id"] == $venue_id) {
					$locationId = $thisLocation["location_id"];
					$location_ids[] = $locationId;
					break 1;
				}
			}
			$datetime = strtotime((string)$thisScreening->start);
			$film_screening["title_en"] = "";
			$film_screening["date"] = date("Y-m-d", $datetime);
			$film_screening["time"] = date("H:i:s", $datetime);
			$film_screening["location_id"] = $locationId;
			$film_screening["url"] = (string)$thisScreening->print_note;
			if ($private == true) {
				$film_screening["Published"] = "0";
			} else {
				$film_screening["Published"] = $this->screening_published;	
			}

			// Single Screening or Film Package?
			$singleFilm = $thisScreening->film;
			if ($singleFilm->count() == 0) {
				$package_id = "";
				$package_name = (string)$thisScreening->film_package["title_english"];
				$package_films = $thisScreening->film_package;
				$package_description = "";
				foreach ($package_films->film as $thisFilm) {
					$package_id .= (string)$thisFilm->id;
					$package_id .= ",";
				}
				if (strlen($package_id) > 1) {
					$package_id = substr($package_id, 0, -1);
				}
				$film_screening["eventival_program_ids"] = $package_id;
				$film_screening["program_name"] = $package_name;
				$film_screening["program_slug"] = create_slug($package_name);
				$film_screening["program_desc"] = "";

				// Add program descriptions from film packages
				foreach ($packages_array as $thisPackage) {
					$package_id2 = "";
					foreach ($thisPackage->film_ids->id as $packageFilmId) {
						$package_id2 .= (string)$packageFilmId;
						$package_id2 .= ",";
					}
					if (strlen($package_id2) > 1) {
						$package_id2 = substr($package_id2, 0, -1);
					}
					if ($package_id2 == $package_id) {
						$package_description = (string)$thisPackage->texts->description;
						$film_screening["program_desc"] = $package_description;
					}
				}
			} else {
				$film_screening["eventival_program_ids"] = (string)$thisScreening->film->id;
				$film_screening["program_name"] = "";
				$film_screening["program_slug"] = "";
				$film_screening["program_desc"] = "";
			}
			$film_screening["movie_id"] = "0";
			$film_screening["program_movie_ids"] = "";

			// Intro / Q&A
			$intro = $qa = false;
			if ((string)$thisScreening->presentation->available == "1") {
				$film_screening["intro"] = "1"; $intro = true;
			} else {
				$film_screening["intro"] = "0";
			}
			$film_screening["intro_length"] = (string)$thisScreening->presentation->duration;				

			if ((string)$thisScreening->qa->available == "1") {
				$film_screening["q_and_a"] = "1"; $qa = true;
			} else {
				$film_screening["q_and_a"] = "0";
			}
			$film_screening["q_and_a_length"] = (string)$thisScreening->qa->duration;

			// Runtime calculation
			$film_length = intval((string)$thisScreening->complete_duration_minutes);
			$total_length_minutes = $film_length;
			if ($intro == true) { $total_length_minutes += intval($film_screening["intro_length"]); }
			if ($qa == true) { $total_length_minutes += intval($film_screening["q_and_a_length"]); }
			$film_screening["total_runtime"] = convert_min_to_time($total_length_minutes);

			// Add to screenings container 
			$film_screening_container[] = $film_screening;
		}
		$location_ids = array_unique($location_ids);
		//print_r($film_screening_container);


		/* --------- Remove existing data and add new data to database ----------- */

		// for testing script without updating database
		if ($this->disable_sync == true) {
			$error = true;
		}
		// Cancel operation if we have any errors retrieving XML from Eventival
		if (!$error) {

			// Remove Existing Films/Screenings from Database
			$film_ids = $this->films->get_all_internal_film_ids($this->eventival_import_festival_id);
			$data["output"] .= "<p>Removing existing films and their associated screenings, personnel, photos and videos: <b>";
			$count = 0;
			foreach ($film_ids as $key => $thisFilm) {
				$values = array("movie_id" => $thisFilm->movie_id, "festivalmovie_id" => $thisFilm->id);
				$this->sync->delete_film($values);

				if ($count != 0) { $data["output"] .= ", "; }
				$data["output"] .= switch_title($thisFilm->label);
				$count++;
			}
			if ( count($film_ids) == 0) { $data["output"] .= "NONE"; }
			$data["output"] .= "</b></p>\n";

			// Add films & related info: Personnel, Languages, Countries, Genres, Photos, Videos, Sponsors, Sponsor Logos and Links
			$data["output"] .= "<p>------ ADDING FILMS (".count($films_container).") ------</p>";
			foreach ($films_container as $key => $filmToAdd) {
				$movie_id = $this->sync->add_film($filmToAdd);

				// Add FestPro movie ids to films container so they can be cross referenced later for screenings
				$films_container[$key]["festpro_film_id"] = $movie_id;

				$data["output"] .= "<p class=\"success\">Added film: ".switch_title($filmToAdd["title_en"])."</p>\n";
			}

			// Update Eventival "My HIFF" ids to FestPro ids
			$new_films = $this->sync->get_all_new_films($this->eventival_import_festival_id);
			foreach ($new_films as $thisFilm) {
				$this_film_id = $thisFilm->id;
				$myhiff_1 = $thisFilm->myhiff_1;
				$myhiff_2 = $thisFilm->myhiff_2;
				$myhiff_3 = $thisFilm->myhiff_3;
				$myhiff_4 = $thisFilm->myhiff_4;

				foreach ($new_films as $thisFilm2) {
					if ($myhiff_1 == $thisFilm2->eventival_film_id) {
						$myhiff_1 = $thisFilm2->movie_id;
					}
					if ($myhiff_2 == $thisFilm2->eventival_film_id) {
						$myhiff_2 = $thisFilm2->movie_id;
					}
					if ($myhiff_3 == $thisFilm2->eventival_film_id) {
						$myhiff_3 = $thisFilm2->movie_id;
					}
					if ($myhiff_4 == $thisFilm2->eventival_film_id) {
						$myhiff_4 = $thisFilm2->movie_id;
					}
				}

				$values = array(
					"myhiff_1" => $myhiff_1,
					"myhiff_2" => $myhiff_2,
					"myhiff_3" => $myhiff_3,
					"myhiff_4" => $myhiff_4
				);
				$this->sync->update_value($values, "wf_festivalmovie", $this_film_id);
			}

			// Set "program_movie_ids" for all screenings
			foreach ($film_screening_container as $key => $thisScreening) {
				$fp_movie_id_array = array();
				$film_title = "";
				$program_ids = explode(",", $thisScreening["eventival_program_ids"]);
				foreach ($program_ids as $event_id) {
					foreach ($films_container as $thisFilm) {
						if ($event_id == $thisFilm["eventival_film_id"]) {
							$fp_movie_id_array[] = $thisFilm["festpro_film_id"];
							$film_title = $thisFilm["title_en"];
						}
					}
				}
				$film_screening_container[$key]["program_movie_ids"] = implode(",", $fp_movie_id_array);
				$film_screening_container[$key]["eventival_program_movie_ids"] = $thisScreening["eventival_program_ids"];
				if (count($fp_movie_id_array) > 0) { $film_screening_container[$key]["movie_id"] = $fp_movie_id_array[0]; }
				if ($film_title != "") { $film_screening_container[$key]["title_en"] = $film_title; }
			}
			$data["output"] .= "<p>------ ADDING SCREENINGS (".count($film_screening_container).") ------</p>";

			// Add screenings
			foreach ($film_screening_container as $key => $screeningToAdd) {
				$screening_id = $this->sync->add_screening($screeningToAdd);
				$datetime = date("m/d/Y H:i",strtotime($screeningToAdd["date"]." ".$screeningToAdd["time"]));
				if ($screeningToAdd["program_name"] == "") {
					$data["output"] .= "<p class=\"success\">Added screening at ".$datetime." for film: ".switch_title($screeningToAdd["title_en"])."</p>\n";	
				} else {
					$data["output"] .= "<p class=\"success\">Added screening at ".$datetime." for film program: ".$screeningToAdd["program_name"]."</p>\n";
				}
			}

			// Update festival locations with all unique locations in screening list.
			$values = array("locations" => implode(",",$location_ids));
			$this->sync->update_value($values, "wf_festival",$this->eventival_import_festival_id);

		} else {
			$data["output"] .= "<p class='error'>Errors encountered! Skipping database operations. Please try running the sync again.</p>\n";
		}

		print $data["output"];
	}

}

/* End of file film_eventivalsync.php */
/* Location: ./system/application/controllers/admin/film_eventivalsync.php */
?>