<?php
class Guest_Settings extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Hotelmodel','hotel');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);

		$data['airlines'] = $this->guesttype->get_all_type("airlines","asc");
		$data['airports'] = $this->guesttype->get_all_type("airports","asc");
		$data['guesttypes'] = $this->guesttype->get_all_type("guests","asc");
		$data['deadlinesfees'] = $this->guesttype->get_type_deadlines_fees_all();
		$data['hotelcomps'] = $this->hotel->get_hotel_comps_all();


		$vars['title'] = "Guest Settings";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_settings";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_settings', $data);
		$this->load->view('admin/footer', $vars);
	}

	// Generic Guest Types Add/Update (ones that only have ID & Name)
	function add_type($type)
	{
		$value = $this->input->post($type.'-new');
		if ($this->input->post($type.'-color') != 0) {
			$color = $this->input->post($type.'-color');
		} else {
			$color = 0;		
		}
		
		$this->load->model('Guesttypesmodel','guesttype');
		$new_id = $this->guesttype->add_type_value($type, $value, $color);
		print "<option value=\"".$new_id."\">".$value."</option>";
	}

	function update_type($type)
	{
		$value = $this->input->post($type.'-current');		
		if ($this->input->post($type.'-color-update') != 0) {
			$color = $this->input->post($type.'-color-update');
		} else {
			$color = 0;		
		}

		$id = $this->input->post($type);
		$this->load->model('Guesttypesmodel','guesttype');
		$this->guesttype->update_type_value($type, $value, $color, $id);
	}

	// Guest Type
	function add_guesttype()
	{
		$values = array(
			"name" => $this->input->post('guesttype-new'),
			"badge" => $this->input->post('getsbadge-new'),
			"giftbag" => $this->input->post('getsbag-new')
		);
		$this->load->model('Guestmodel','guest');
		$new_id = $this->guest->add_value($values,"fg_type_guests");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
	}

	function update_guesttype()
	{
		$id = $this->input->post('guesttype');
		$values = array(
			"name" => $this->input->post('guesttype-current'),
			"badge" => $this->input->post('getsbadge-current'),
			"giftbag" => $this->input->post('getsbag-current')
		);
		$this->load->model('Guestmodel','guest');
		$new_id = $this->guest->update_value($values,"fg_type_guests",$id);
	}

	// Airports
	function add_airport()
	{
		$values = array(
			"name" => $this->input->post('airportname-new'),
			"code" => $this->input->post('airportcode-new'),
			"domestic" => $this->input->post('domestic-new'),
			"priority" => $this->input->post('priority-new')
		);
		$this->load->model('Guestmodel','guest');
		$new_id = $this->guest->add_value($values,"fg_type_airports");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
	}

	function update_airport()
	{
		$id = $this->input->post('airports');
		$values = array(
			"name" => $this->input->post('airportname-current'),
			"code" => $this->input->post('airportcode-current'),
			"domestic" => $this->input->post('domestic-current'),
			"priority" => $this->input->post('priority-current')
		);
		$this->load->model('Guestmodel','guest');
		$new_id = $this->guest->update_value($values,"fg_type_airports",$id);
	}

	// Deadlines & Fees
	function add_deadlines_fees()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');

		$festivals_array = convert_festival_to_array($this->festival->get_all_festivals());
		$guesttypes_array = convert_to_array($this->guesttype->get_all_type("guests","asc"));
		$values = array(
			"festival_id" => $this->input->post('accred-festival-new'),
			"guesttype_id" => $this->input->post('accred-guesttype-new'),
			"early_deadline" => $this->input->post('early-deadline-new'),
			"late_deadline" => $this->input->post('late-deadline-new'),
			"final_deadline" => $this->input->post('final-deadline-new'),
			"early_fee" => $this->input->post('early-fee-new'),
			"late_fee" => $this->input->post('late-fee-new'),
			"final_fee" => $this->input->post('final-fee-new')
		);
		$new_id = $this->guest->add_value($values,"fg_deadlines_fees");
		
		// Print new table row here
		print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"df-".$new_id."\" data-id=\"".$new_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t<td>".$festivals_array[$values["festival_id"]]."</td>\n";
		print "\t\t<td>".$guesttypes_array[$values["guesttype_id"]]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["early_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["early_fee"]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["late_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["late_fee"]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["final_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["final_fee"]."</td>\n";
		print "\t</tr>\n";
		print "\t\t\t\t<tr id=\"deadlines_replaceme\">\n";
		print "\t\t\t\t\t<td colspan=\"9\"><input type=\"hidden\" name=\"deadlines_new_id\" id=\"deadlines_new_id\" value=\"".$new_id."\" /></td>\n";
		print "\t\t\t\t</tr>\n";
	}

	function update_deadlines_fees()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');

		$festivals_array = convert_festival_to_array($this->festival->get_all_festivals());
		$guesttypes_array = convert_to_array($this->guesttype->get_all_type("guests","asc"));
		$values = array(
			"festival_id" => $this->input->post('accred-festival-current'),
			"guesttype_id" => $this->input->post('accred-guesttype-current'),
			"early_deadline" => $this->input->post('early-deadline-current'),
			"late_deadline" => $this->input->post('late-deadline-current'),
			"final_deadline" => $this->input->post('final-deadline-current'),
			"early_fee" => $this->input->post('early-fee-current'),
			"late_fee" => $this->input->post('late-fee-current'),
			"final_fee" => $this->input->post('final-fee-current')
		);
		$id = $this->input->post('deadlines_id_update');
		$new_id = $this->guest->update_value($values,"fg_deadlines_fees",$id);

		// Print update of current table row here
		print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"df-".$id."\" data-id=\"".$id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t<td>".$festivals_array[$values["festival_id"]]."</td>\n";
		print "\t\t<td>".$guesttypes_array[$values["guesttype_id"]]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["early_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["early_fee"]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["late_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["late_fee"]."</td>\n";
		print "\t\t<td>".date("m-d-Y",strtotime($values["final_deadline"]))."</td>\n";
		print "\t\t<td>$".$values["final_fee"]."</td>\n";
		print "\t</tr>\n";
	}

	function return_deadlines_json() {
		$this->load->model('Guesttypesmodel','guesttype');
		print json_encode( $this->guesttype->get_type_deadlines_fees_all() );
	}

	// Hotel Comp Options
	function add_hotelcomps()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');

		$festivals_array = convert_festival_to_array($this->festival->get_all_festivals());
		$values = array(
			"festival_id" => $this->input->post('hotelc-festival-new'),
			"name" => $this->input->post('hotelc-name-new'),
			"hotel" => $this->input->post('hotelc-hotel-new'),
			"roomtype" => $this->input->post('hotelc-roomtype-new'),
			"limit" => $this->input->post('hotelc-limit-new')
		);
		$new_id = $this->guest->add_value($values,"fg_hotelcompdeals");
		
		// Print new table row here
		print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"hc-".$new_id."\" data-id=\"".$new_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t<td>".$festivals_array[$values["festival_id"]]."</td>\n";
		print "\t\t<td>".$values["name"]."</td>\n";
		print "\t\t<td>".$values["hotel"]."</td>\n";
		print "\t\t<td>".$values["roomtype"]."</td>\n";
		print "\t\t<td>".$values["limit"]."</td>\n";
		print "\t</tr>\n";
		print "\t\t\t\t<tr id=\"hotelcomps_replaceme\">\n";
		print "\t\t\t\t\t<td colspan=\"6\"><input type=\"hidden\" name=\"hotelcomps_new_id\" id=\"hotelcomps_new_id\" value=\"".$new_id."\" /></td>\n";
		print "\t\t\t\t</tr>\n";
	}

	function update_hotelcomps()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');

		$festivals_array = convert_festival_to_array($this->festival->get_all_festivals());
		$values = array(
			"festival_id" => $this->input->post('hotelc-festival-current'),
			"name" => $this->input->post('hotelc-name-current'),
			"hotel" => $this->input->post('hotelc-hotel-current'),
			"roomtype" => $this->input->post('hotelc-roomtype-current'),
			"limit" => $this->input->post('hotelc-limit-current')
		);
		$id = $this->input->post('deadlines_id_update');
		$new_id = $this->guest->update_value($values,"fg_hotelcompdeals",$id);

		// Print update of current table row here
		print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"hc-".$id."\" data-id=\"".$id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t<td>".$festivals_array[$values["festival_id"]]."</td>\n";
		print "\t\t<td>".$values["name"]."</td>\n";
		print "\t\t<td>".$values["hotel"]."</td>\n";
		print "\t\t<td>".$values["roomtype"]."</td>\n";
		print "\t\t<td>".$values["limit"]."</td>\n";
		print "\t</tr>\n";
	}

	function return_hotelcomps_json() {
		$this->load->model('Hotelmodel','hotel');
		print json_encode( $this->hotel->get_hotel_comps_all() );
	}
}

/* End of file guest_settings.php */
/* Location: ./system/application/controllers/admin/guest_settings.php */
?>