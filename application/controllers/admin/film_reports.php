<?php
class Film_Reports extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Genremodel','genre');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Competitionmodel','competition');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->reports->get_ballot_counts($current_fest);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$data['sections'] = $this->section->get_all_sections($current_fest);

		$premieres = $this->filmtype->get_all_type("premiere", "asc", "no");
		$data['premieres'] = calculateTypeSummary($premieres, $data['films'], "premiere_id");

		$eventtypes = $this->filmtype->get_all_type("event", "asc", "no");
		$data['eventtypes'] = calculateTypeSummary($eventtypes, $data['films'], "event_id");

		$data['sectionsummary'] = calculateTypeSummary($data['sections'], $data['films'], "category_id");

		$movie_id_array = array();
		foreach ($data['filmids'] as $thisId) {
			$movie_id_array[] = $thisId->movie_id;
		}
		if (count($movie_id_array) == 0) {
			$movie_id_array[] = 0;
		}
		$data['genre_films'] = $this->genre->get_all_genres_in($movie_id_array);

		$data['film_ratings'] = $this->reports->get_rating_counts($current_fest);
		$data['film_profit_loss'] = $this->reports->get_profit_loss($current_fest);
		$data['film_profit_loss_ship'] = $this->reports->get_profit_loss_shipping($current_fest);
		$data['film_profit_loss_shorts'] = $this->reports->get_profit_loss_shorts($current_fest, $movie_id_array);
		foreach ($data['film_profit_loss_shorts'] as $thisRecord) {
			$film_id_array = explode(",", $thisRecord->program_movie_ids);
			$RentalFee = 0; $ScreeningFee = 0; $TransferFee = 0;

			$program_results = $this->reports->get_profit_loss_expenses($film_id_array);
			foreach ($program_results as $thisMovie) {
				$RentalFee += $thisMovie->RentalFee;
				$ScreeningFee += $thisMovie->ScreeningFee;
				$TransferFee += $thisMovie->TransferFee;
			}

			// Update original report with all fees for shorts programs, etc
			foreach ($data['film_profit_loss'] as $thisProfitLoss) {
				if ($thisRecord->program_movie_ids == $thisProfitLoss->program_movie_ids) {
					$thisProfitLoss->RentalFee = $RentalFee;
					$thisProfitLoss->ScreeningFee = $ScreeningFee;
					$thisProfitLoss->TransferFee = $TransferFee;
				}
			}
		}
		$data['competitions'] = $this->competition->get_all_competitions();
		$data['competition_votes'] = $this->competition->get_all_competition_votes();

		$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$vars['schedule'] = convert_to_array_schedule2($data['full_schedule']);

		$vars['title'] = "Reports";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_reports";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_reports', $data);
		$this->load->view('admin/footer', $vars);
	}

	function preview_theater_ops2($date, $venue, $download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Locationmodel','location');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sponsormodel','sponsor');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$full_schedule = $this->schedule->get_internal_schedule_minimal($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		if ($date == "all") {
			$report = $this->reports->get_internal_schedule_export($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		} else {
			$report = $this->reports->get_internal_schedule_export($date, $date);
		}
		$films_array = $venue_array = array();
		foreach ($report as $thisRec) {
			$films_array[$thisRec->movie_id] = switch_title($thisRec->title_en);
			$venue_array[] = $thisRec->location;
		}
		$venue_array = array_unique($venue_array); sort($venue_array);
		$locations = convert_to_array2($this->location->get_all_locations());
		date_default_timezone_set(FP_BASE_TIMEZONE);
		$currentdate = date("m-d-Y"); $runtime_no_qanda = 0;

		foreach ($report as $thisRec) {
			if ($thisRec->program_name != "") { $thisRec->title_en = switch_title($thisRec->program_name); }

			$movie_array = explode(",",$thisRec->program_movie_ids);
			if (count($movie_array) > 1) {
				$current_runtime = 0;
				// Pull runtimes for all films in this program
				foreach ($movie_array as $movie_id) {
					$currentTitle = $thisRec->title_en;
					$shorts_program = $this->reports->get_film_schedule_program_export($movie_id);

					// Short with Feature, add short name to English Title
					if ($thisRec->program_name == "") {
						foreach ($shorts_program as $shortwithfeature) {
							if ($shortwithfeature->title_en != $currentTitle) {
								$thisRec->title_en = switch_title($currentTitle)." + ".switch_title($shortwithfeature->title_en);
							}
							$current_runtime = $current_runtime + $shortwithfeature->runtime_int;
						}
					} else {
						foreach ($shorts_program as $shorts) {
							$thisRec->title_en = switch_title($currentTitle).", ".switch_title($shorts->title_en);
							$current_runtime = $current_runtime + $shorts->runtime_int;
						}
					}
				}
				$thisRec->runtime_int = $current_runtime;
			}
			
			if ($thisRec->intro == 1) { $thisRec->runtime_int = $thisRec->runtime_int + $thisRec->intro_length; }
			if ($thisRec->fest_trailer == 1) { $thisRec->runtime_int = $thisRec->runtime_int + $thisRec->fest_trailer_length; }
			if ($thisRec->sponsor_trailer1 == 1) { $thisRec->runtime_int = $thisRec->runtime_int + $thisRec->sponsor_trailer1_length; }
			if ($thisRec->sponsor_trailer2 == 1) { $thisRec->runtime_int = $thisRec->runtime_int + $thisRec->sponsor_trailer2_length; }
			$runtime_no_qanda = $thisRec->runtime_int;
			if ($thisRec->q_and_a == 1) { $thisRec->runtime_int = $thisRec->runtime_int + $thisRec->q_and_a_length; }

			// Pull in Sponsor Information
			$sponsortext = "";
			$sponsors = $this->sponsor->get_all_sponsor_logos($thisRec->movie_id, $current_fest);
			if (count($sponsors > 0)) {
				foreach ($sponsors as $thisSponsor) { $sponsortext .= $thisSponsor->name.", "; }
				$sponsortext = trim($sponsortext,", ");
			} 

			// Determine if this is a first public screening for ballot purposes
			$firstScreening = ""; $publicScreeningCount = 0; $x = 0;
			$firstDate = ""; $firstTime = "";
			$recMovieArray = explode(",",$thisRec->program_movie_ids);
			sort($recMovieArray);
			$recMovieString = implode(",",$recMovieArray);
			foreach ($full_schedule as $thisScreening) {
				$thisScrnMovieArray = explode(",",$thisScreening->program_movie_ids);
				sort($thisScrnMovieArray);
				$thisScrnMovieString = implode(",",$thisScrnMovieArray);

				if ($recMovieString == $thisScrnMovieString) {
					if ($thisRec->Private == 0) {
						$publicScreeningCount++;
						if ($x == 0) {
							$firstDate = $thisScreening->date;
							$firstTime = $thisScreening->time;
						}
						$x++;
					}
				}
			}
			if ($publicScreeningCount == 1) {
				$firstScreening = "Ballot ";
			} else {
				if ($firstDate == $thisRec->date && $firstTime == $thisRec->time) {
					$firstScreening = "Ballot ";
				}
			}

			// Output new fields for export
			$thisRec->Date = date("m-d-Y", strtotime($thisRec->date));
			if ($thisRec->location_id2 != 0) {
				$location_name = $locations[$thisRec->location_id2];
				$thisRec->Location = $thisRec->location.", ".$location_name;
			} else {
				$thisRec->Location = $thisRec->location;
			}
			$thisRec->Format = $thisRec->format_name;
			$thisRec->Title = switch_title($thisRec->title_en);
			$thisRec->Start = date("g:i A", strtotime($thisRec->time));
			$thisRec->Film_End = date("g:i A", strtotime($thisRec->time)+($runtime_no_qanda * 60));
			$thisRec->End = date("g:i A", strtotime($thisRec->time)+($thisRec->runtime_int * 60));
			$thisRec->TRT = $thisRec->runtime_int;
			$thisRec->Intro = $thisRec->host;
			$thisRec->Sponsor = $sponsortext;
			$thisRec->Program = ""; // . $firstScreening;
			if ($thisRec->sponsor_trailer1 == 1) {
				if ($thisRec->sponsor_trailer1_name == "") {
					$thisRec->Program .= "Sponsor Trailer 1 ";
				} else {
					$thisRec->Program .= $thisRec->sponsor_trailer1_name." ";
				}
			}
			if ($thisRec->sponsor_trailer2 == 1) {
				if ($thisRec->sponsor_trailer2_name == "") {
					$thisRec->Program .= "Sponsor Trailer 2 ";
				} else {
					$thisRec->Program .= $thisRec->sponsor_trailer2_name." ";
				}
			}
			if ($thisRec->q_and_a == 1) { $thisRec->Program .= "Q&A (".$thisRec->q_and_a_length.") "; }
			if ($thisRec->Private == 1) { $thisRec->Program .= "Private "; }
			if ($thisRec->Rush == 1) { $thisRec->Program .= "Standby "; }
			if ($thisRec->Free == 1) { $thisRec->Program .= "Free "; }
			if ($thisRec->notes != "") { $thisRec->Program .= $thisRec->notes." "; }

			// Tech Notes
			$thisRec->Colorbar_End = $thisRec->colorbar_end_time;
			$thisRec->TitleID_End = $thisRec->titleid_end_time;
			$thisRec->In_Time = $thisRec->in_time;
			$thisRec->End_Credits = $thisRec->end_credits_time;
			$thisRec->Out_Time = $thisRec->out_time;

			// Remove unneeded values from report
			unset($thisRec->id, $thisRec->movie_id, $thisRec->program_movie_ids, $thisRec->program_name, $thisRec->location_id2, $thisRec->Private, $thisRec->Rush, $thisRec->Free, $thisRec->date, $thisRec->time, $thisRec->host, $thisRec->location_id, $thisRec->location, $thisRec->intro, $thisRec->fest_trailer, $thisRec->sponsor_trailer1, $thisRec->sponsor_trailer2, $thisRec->q_and_a, $thisRec->intro_length, $thisRec->fest_trailer_length, $thisRec->sponsor_trailer1_length, $thisRec->sponsor_trailer2_length, $thisRec->q_and_a_length, $thisRec->total_runtime, $thisRec->title_en, $thisRec->runtime_int, $thisRec->format_name, $thisRec->notes, $thisRec->sponsor_trailer1_name, $thisRec->sponsor_trailer2_name);
			unset($thisRec->colorbar_end_time, $thisRec->titleid_end_time, $thisRec->in_time, $thisRec->end_credits_time, $thisRec->out_time);
		}

		if ($download == "download") {
			if ($date == "all") {
				$this->reports->csv_download($report,"theater-ops-".$data['festival'][0]->year."-".strtolower(str_replace(array(" ","_",":","/","\\"),"-",$data['festival'][0]->name)).".csv");
			} else {
				$this->reports->csv_download($report,"theater-ops-".$date.".csv");			
			}
		} else {
			if ($date == "all") {
				print "<h3>Daily Reports for ".$data['festival'][0]->year." ".$data['festival'][0]->name."</h3>";
				print "<form id=\"export-thops-all\" method=\"post\" action=\"/admin/film_reports/preview_theater_ops2/all/all/download/\">";
				print "<b>Theater Operations Schedule (all locations - ".count($report)." records)</b> <button id=\"export-thops-all-btn\">Export</button><br />\n";
				print "</form>";
				print_report_preview($report, "export-thops-all-btn", "export-thops-all");
			} else {
				print "<h3>Daily Reports for ".date("m/d/Y",strtotime($date))."</h3>";
				print "<form id=\"export-thops-".$date."-all\" method=\"post\" action=\"/admin/film_reports/preview_theater_ops2/".$date."/all/download/\">";
				print "<b>Theater Operations Schedule (all locations - ".count($report)." records)</b> <button id=\"export-thops-".$date."-all-btn\">Export</button><br />\n";
				print "</form>";
				print_report_preview($report, "export-thops-".$date."-all-btn", "export-thops-".$date."-all");

			}	
		}
	}

	function file_download() {
		$this->load->helper('download');
		
		$csv_file = $this->input->post("file_content");
		$file_name = $this->input->post("file_name");
    	force_download($file_name,$csv_file);
	}

	function preview_shipments($type, $download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Schedulemodel','schedule');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$data['courier'] = $this->filmtype->get_all_type("courier","asc");
		

		$report = $this->reports->get_pt_shipments($type, $current_fest);
		
		foreach ($report as $thisRec) {
			
			$screeningsList = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRec->movie_id);

			// Get all screenings for this film
			$scrnString = "";
			foreach ($screeningsList as $thisScreening) {
				$scrnString .= date("n/j g:i A",strtotime($thisScreening->date." ".$thisScreening->time)).", ";
			} $scrnString = trim($scrnString,", ");
			
			$courierString = "";
			foreach ($data['courier'] as $thisCourier) {
				if ($thisCourier->id == $thisRec->Courier) { $courierString = $thisCourier->name; }
			}

			// Update record values
			$thisRec->Outbound == "1" ? $thisRec->Outbound = "yes" : $thisRec->Outbound = "no";			
			$thisRec->Courier = $courierString;
			$thisRec->Screenings = $scrnString;

			// Remove unneeded values from report
			unset($thisRec->movie_id);
		}
		
		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"pt-".$type."-shipments-".$currentdate.".csv");
		} else {
			print "<h3>";
			$type == "inbound" ? print "Inbound" : print "Outbound";
			print " Shipments (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"pt-shipments-export\" method=\"post\" action=\"/admin/film_reports/preview_shipments/".$type."/download/\">";
			print "<b>";
			$type == "inbound" ? print "Inbound" : print "Outbound";
			print " Shipments (".count($report)." records)</b> <button id=\"pt-shipments-btn\">Export</button><br />\n";
			print "</form>";
			print_report_preview($report, "pt-shipments-btn", "pt-shipments-export");
		}
	}

	function preview_all_films($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Schedulemodel','schedule');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_all_films_export($current_fest);
		$films_array = array();
		foreach ($report as $thisRec) { $films_array[$thisRec->movie_id] = switch_title($thisRec->EnglishTitle); }

		foreach ($report as $thisRec) {
			$countryList = $this->country->get_movie_countries($thisRec->movie_id);
			$languageList = $this->language->get_movie_languages($thisRec->movie_id);
			$genreList = $this->genre->get_movie_genres($thisRec->movie_id);
			$personnelList = $this->personnel->get_movie_personnel($thisRec->movie_id, "");
			$screeningsList = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRec->movie_id);

			// Get all countries, languages, genres for this film
			$countryString = $languageString = $genreString = $scrnString = "";
			if (count($countryList) > 0) { foreach ($countryList as $rec) { $countryString .= $rec->name.", "; } $countryString = trim($countryString,", "); }
			if (count($languageList) > 0) { foreach ($languageList as $rec) { $languageString .= $rec->name.", "; } $languageString = trim($languageString,", "); }
			if (count($genreList) > 0) { foreach ($genreList as $rec) { $genreString .= $rec->name.", "; } $genreString = trim($genreString,", "); }
			
			// Get all personnel for this film
			$dirString = $prodString = $cinString = $scriptString = $castString = "";
			foreach ($personnelList as $thisPerson) {
				$roles = explode(",",$thisPerson->personnel_roles);
				if (count($roles) > 0) {
					foreach ($roles as $thisRole) {
						if ($thisRole == 1) { $dirString .= $thisPerson->name." ".$thisPerson->lastname.", "; }
						if ($thisRole == 2) { $prodString .= $thisPerson->name." ".$thisPerson->lastname.", "; }
						if ($thisRole == 3) { $scriptString .= $thisPerson->name." ".$thisPerson->lastname.", "; }
						if ($thisRole == 5) { $cinString .= $thisPerson->name." ".$thisPerson->lastname.", "; }
						if ($thisRole == 6) { $castString .= $thisPerson->name." ".$thisPerson->lastname.", "; }
					}
				}
			} $dirString = trim($dirString,", "); $prodString = trim($prodString,", "); $scriptString = trim($scriptString,", "); $cinString = trim($cinString,", "); $castString = trim($castString,", ");

			// Get all screenings for this film
			foreach ($screeningsList as $thisScreening) {
				if ($thisScreening->Private != 1) {
					$scrnString .= date("n/j g:i A",strtotime($thisScreening->date." ".$thisScreening->time)).", ";
				}
			} $scrnString = trim($scrnString,", ");

			// Process Synopses - processing done in reports->csv_download function

			// Update record values
			$thisRec->Countries = $countryString;
			$thisRec->Languages = $languageString;
			$thisRec->Genres = $genreString;
			$thisRec->Director = $dirString;
			$thisRec->Producer = $prodString;
			$thisRec->Cinematographer = $cinString;
			$thisRec->Scriptwriter = $scriptString;
			$thisRec->Cast = $castString;
			$thisRec->Screenings = $scrnString;

			if ($thisRec->SimilarFilms1 != 0 && isset($films_array[$thisRec->SimilarFilms1])) { $thisRec->SimilarFilms1 = $films_array[$thisRec->SimilarFilms1]; } else { $thisRec->SimilarFilms1 = ""; }
			if ($thisRec->SimilarFilms2 != 0 && isset($films_array[$thisRec->SimilarFilms2])) { $thisRec->SimilarFilms2 = $films_array[$thisRec->SimilarFilms2]; } else { $thisRec->SimilarFilms2 = ""; }
			if ($thisRec->SimilarFilms3 != 0 && isset($films_array[$thisRec->SimilarFilms3])) { $thisRec->SimilarFilms3 = $films_array[$thisRec->SimilarFilms3]; } else { $thisRec->SimilarFilms3 = ""; }
			if ($thisRec->SimilarFilms4 != 0 && isset($films_array[$thisRec->SimilarFilms4])) { $thisRec->SimilarFilms4 = $films_array[$thisRec->SimilarFilms4]; } else { $thisRec->SimilarFilms4 = ""; }

			// Remove unneeded values from report
			unset($thisRec->movie_id);
		}
		
		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"all-films-".$currentdate.".csv");
		} else {
			print "<h3>All Films (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"all-films-export\" method=\"post\" action=\"/admin/film_reports/preview_all_films/download/\">";	
			print "<b>All Films (".count($report)." records)</b> <button id=\"all-films-btn\">Export</button><br />\n";
			print "</form>";
			print_report_preview($report, "all-films-btn", "all-films-export");
		}
	}

	function preview_toc($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_table_of_contents_export($current_fest);
		
		foreach ($report as $thisRec) {
			$filmsInCategory = $this->reports->get_table_of_contents_films($current_fest, $thisRec->category_id);
			$filmString = $PBString = $UGString = "";
			if (count($filmsInCategory) > 0) {
				foreach ($filmsInCategory as $filmName) {
					$filmString .= switch_title($filmName->title_en)."; ";
					$PBString .= $filmName->PBPage.", ";
					$UGString .= $filmName->UGPage.", ";
				}
				$filmString = trim($filmString,"; ");
				$PBString = trim($PBString,", ");
				$UGString = trim($UGString,", ");
			}
			$thisRec->Films = $filmString;
			$thisRec->ProgramBookPages = $PBString;
			$thisRec->UserGuidePages = $UGString;
			unset($thisRec->category_id);
		}

		if ($download == "download") {
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"table-contents-".$currentdate.".csv");
		} else {
			print "<h3>Table of Contents (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"toc-export\" method=\"post\" action=\"/admin/film_reports/preview_toc/download/\">";	
			print "<b>Table of Contents (".count($report)." records)</b> <button id=\"table-contents-btn\">Export</button><br />\n";
			print "</form>";
			
			print_report_preview($report, "table-contents-btn", "toc-export");
		}
	}

	function preview_print_idx($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_printsource_index_export($current_fest);
		foreach ($report as $thisRec) { $thisRec->EnglishTitle = switch_title($thisRec->EnglishTitle); }

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"print-idx-".$currentdate.".csv");
		} else {
			print "<h3>Print Source Index (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"print-idx-export\" method=\"post\" action=\"/admin/film_reports/preview_print_idx/download/\">";	
			print "<b>Print Source Index (".count($report)." records)</b> <button id=\"print-idx-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "print-idx-btn", "print-idx-export");
		}
	}

	function preview_country_idx($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_country_index_export($current_fest);
		foreach ($report as $thisRec) { $thisRec->EnglishTitle = switch_title($thisRec->EnglishTitle); }

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"country-idx-".$currentdate.".csv");
		} else {
			print "<h3>Country Index (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"country-idx-export\" method=\"post\" action=\"/admin/film_reports/preview_country_idx/download/\">";	
			print "<b>Country Index (".count($report)." records)</b> <button id=\"country-idx-btn\">Export</button><br />\n";
			print "</form>";		
	
			print_report_preview($report, "country-idx-btn", "country-idx-export");
		}
	}

	function preview_director_idx($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_director_index_export($current_fest);
		foreach ($report as $thisRec) { $thisRec->EnglishTitle = switch_title($thisRec->EnglishTitle); }

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"director-idx-".$currentdate.".csv");
		} else {
			print "<h3>Director Index (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"director-idx-export\" method=\"post\" action=\"/admin/film_reports/preview_director_idx/download/\">";	
			print "<b>Director Index (".count($report)." records)</b> <button id=\"director-idx-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "director-idx-btn", "director-idx-export");
		}
	}

	function preview_film_idx($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_film_index_export($current_fest);
		foreach ($report as $thisRec) { $thisRec->EnglishTitle = switch_title($thisRec->EnglishTitle); }

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"film-idx-".$currentdate.".csv");
		} else {
			print "<h3>Film Index (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"film-idx-export\" method=\"post\" action=\"/admin/film_reports/preview_film_idx/download/\">";	
			print "<b>Film Index (".count($report)." records)</b> <button id=\"film-idx-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "film-idx-btn", "film-idx-export");
		}
	}

	function preview_film_sched($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Locationmodel','location');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_film_schedule_export($current_fest);
		$countries_report = $this->reports->get_country_index_export($current_fest);

		$films_array = array();
		foreach ($report as $thisRec) { $films_array[$thisRec->movie_id] = switch_title($thisRec->Title); }
		$locations = convert_to_array2($this->location->get_all_locations());

		foreach ($report as $thisRec) {
			$country_string = "";
			foreach ($countries_report as $thisCountry) {
				if ($thisCountry->EnglishTitle == $thisRec->Title) {
					$country_string .= $thisCountry->Country.", ";
				}
			} $country_string = trim($country_string, ", ");

			$starttime_int = $thisRec->StartTime;
			$movie_array = explode(",",$thisRec->program_movie_ids);
			if (count($movie_array) > 1) {
				$current_runtime = 0;
				// Pull runtimes for all films in this program
				foreach ($movie_array as $movie_id) {
					$currentTitle = $thisRec->Title;
					$shorts_program = $this->reports->get_film_schedule_program_export($movie_id);
					foreach ($shorts_program as $shorts) { $current_runtime = $current_runtime + $shorts->runtime_int; }
					
					// Short with Feature, add short name to English Title
					if ($thisRec->program_name == "") {
						foreach ($shorts_program as $shortwithfeature) {
							if ($shortwithfeature->title_en != $currentTitle) {
								$thisRec->Title = switch_title($currentTitle)." + ".switch_title($shortwithfeature->title_en);
							}
						}
					}
				}
				$thisRec->Runtime = $current_runtime;
			}

			if ($thisRec->intro == 1) { $thisRec->Runtime = $thisRec->Runtime + $thisRec->intro_length; }
			if ($thisRec->fest_trailer == 1) { $thisRec->Runtime = $thisRec->Runtime + $thisRec->fest_trailer_length; }
			if ($thisRec->sponsor_trailer1 == 1) { $thisRec->Runtime = $thisRec->Runtime + $thisRec->sponsor_trailer1_length; }
			if ($thisRec->sponsor_trailer2 == 1) { $thisRec->Runtime = $thisRec->Runtime + $thisRec->sponsor_trailer2_length; }
			$runtime_no_qanda = $thisRec->Runtime;
			if ($thisRec->q_and_a == 1) { $thisRec->Runtime = $thisRec->Runtime + $thisRec->q_and_a_length; }

			if ($thisRec->program_name != "") {
				$thisRec->Title = switch_title($thisRec->program_name);
			}
			if ($thisRec->location_id2 != 0) {
				$location_name = $locations[$thisRec->location_id2];
				$thisRec->Venue = $thisRec->Venue.", ".$location_name;
			}



			$thisRec->Countries = $country_string;
			$thisRec->Date = date("m-d-Y", strtotime($thisRec->Date));
			$thisRec->StartTime = date("g:i A", strtotime($thisRec->StartTime));
			$thisRec->EndTime = date("g:i A", strtotime($starttime_int)+($runtime_no_qanda * 60));
			$thisRec->Title = switch_title($thisRec->Title);

			if ($thisRec->SimilarFilms1 != 0 && isset($films_array[$thisRec->SimilarFilms1])) { $thisRec->SimilarFilms1 = $films_array[$thisRec->SimilarFilms1]; } else { $thisRec->SimilarFilms1 = ""; }
			if ($thisRec->SimilarFilms2 != 0 && isset($films_array[$thisRec->SimilarFilms2])) { $thisRec->SimilarFilms2 = $films_array[$thisRec->SimilarFilms2]; } else { $thisRec->SimilarFilms2 = ""; }
			if ($thisRec->SimilarFilms3 != 0 && isset($films_array[$thisRec->SimilarFilms3])) { $thisRec->SimilarFilms3 = $films_array[$thisRec->SimilarFilms3]; } else { $thisRec->SimilarFilms3 = ""; }
			if ($thisRec->SimilarFilms4 != 0 && isset($films_array[$thisRec->SimilarFilms4])) { $thisRec->SimilarFilms4 = $films_array[$thisRec->SimilarFilms4]; } else { $thisRec->SimilarFilms4 = ""; }

			// Remove unneeded values from report
			unset($thisRec->Runtime);
			unset($thisRec->movie_id);
			unset($thisRec->program_movie_ids);
			unset($thisRec->program_name);
			unset($thisRec->location_id2);
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"film-schedule-".$currentdate.".csv");
		} else {
			print "<h3>Film Schedule (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"film-sched-export\" method=\"post\" action=\"/admin/film_reports/preview_film_sched/download/\">";
			print "<b>Film Schedule (".count($report)." records)</b> <button id=\"film-schedule-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "film-schedule-btn", "film-sched-export");
		}
	}

	function preview_film_trailers($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Locationmodel','location');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_film_trailers_export($current_fest);
		foreach ($report as $thisRec) { $thisRec->EnglishTitle = switch_title($thisRec->EnglishTitle); }

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"film-trailers-".$currentdate.".csv");
		} else {
			print "<h3>Film Trailers (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"film-trailers-export\" method=\"post\" action=\"/admin/film_reports/preview_film_trailers/download/\">";
			print "<b>Film Trailers (".count($report)." records)</b> <button id=\"film-trailers-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "film-trailers-btn", "film-trailers-export");
		}
	}

	function preview_film_review($download) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_film_review_export($current_fest);

		$country_array = convert_to_array2($this->country->get_all_countries());
		$language_array = convert_to_array2($this->language->get_all_languages());
		$genre_array = convert_to_array2($this->genre->get_all_genres());
		$format_array = convert_to_array2($this->filmtype->get_all_type("format", "asc", "no"));
		$aspectratio_array = convert_to_array2($this->filmtype->get_all_type("aspectratio", "asc", "no"));
		$sound_array = convert_to_array2($this->filmtype->get_all_type("sound", "asc", "no"));
		$color_array = convert_to_array2($this->filmtype->get_all_type("color", "asc", "no"));
		$personnel_array = convert_to_array2($this->filmtype->get_all_type("personnel", "asc", "no"));

		// Process report to replace film settings id numbers with actual values
		foreach ($report as $thisRec) {
			$country_string = $language_string = $genre_string = $personnel_string = $roles_string = "";

			$countries = explode(",",$thisRec->Country);
			foreach ($countries as $thisCountry) { if ($thisCountry != "") { $country_string = $country_array[$thisCountry].", "; } }
			$country_string = trim($country_string, ", ");

			$languages = explode(",",$thisRec->Language);
			foreach ($languages as $thisLanguage) { if ($thisLanguage != "") { $language_string = $language_array[$thisLanguage].", "; } }
			$language_string = trim($language_string, ", ");

			$genres = explode(",",$thisRec->Genre);
			foreach ($genres as $thisGenre) { if ($thisGenre != "") { $genre_string = $genre_array[$thisGenre].", "; } }
			$genre_string = trim($genre_string, ", ");

			if ($thisRec->Personnel != "") {
				$personnel = preg_split("/}/",$thisRec->Personnel);
				foreach ($personnel as $thisPerson) {
					if ($thisPerson != "") {
						$thisPerson = ltrim($thisPerson,","); $thisPerson = ltrim($thisPerson,"{");
						$fields = preg_split("/\|/",$thisPerson);
						$firstname = $fields[0]; $lastname = $fields[1]; $roles = $fields[2]; $lnf = $fields[3];
						if ($lnf == "" || $lnf == " ") { $lnf = 0; } else { $lnf = 1; }
						
						$roles_array = explode(",", $roles);
						foreach ($roles_array as $thisRole) { if ($thisRole != "") { $roles_string = $personnel_array[$thisRole].", "; } }
						$roles_string = rtrim($roles_string, ", ");
						
						if ($lnf == 0) {
							$personnel_string .= $firstname." ".$lastname.": ".$roles_string." ";
						} else {
							$personnel_string .= $lastname." ".$firstname.": ".$roles_string." ";
						}
					}
				}
			}
			
			
			$thisRec->Country = $country_string;
			$thisRec->Language = $language_string;
			$thisRec->Genre = $genre_string;
			if ($thisRec->ExhibitionFormat != "" && $thisRec->ExhibitionFormat != 0) { $thisRec->ExhibitionFormat = $format_array[$thisRec->ExhibitionFormat]; }
			if ($thisRec->AspectRatio != "" && $thisRec->AspectRatio != 0) { $thisRec->AspectRatio = $aspectratio_array[$thisRec->AspectRatio]; }
			if ($thisRec->Sound != "" && $thisRec->Sound != 0) { $thisRec->Sound = $sound_array[$thisRec->Sound]; }
			if ($thisRec->Color != "" && $thisRec->Color != 0) { $thisRec->Color = $color_array[$thisRec->Color]; }
			$thisRec->Personnel = $personnel_string;
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"film-review-".$currentdate.".csv");
		} else {
			print "<h3>Film Review (".$data['festival'][0]->year." ".$data['festival'][0]->name.")</h3>";
			print "<form id=\"film-review-export\" method=\"post\" action=\"/admin/film_reports/preview_film_review/download/\">";	
			print "<b>Film Index (".count($report)." records)</b> <button id=\"film-review-btn\">Export</button><br />\n";
			print "</form>";
	
			print_report_preview($report, "film-review-btn", "film-review-export");
		}
	}


	function export_ballot_counts() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Reportsmodel','reports');
		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$full_schedule = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$report = $this->reports->get_ballot_counts($current_fest);

		// Process films to get array of seat counts and assign specific seat counts for report
		$novotes = 0; $seat_array = array();
		$avg_num_votes = $avg_rating = $temp_votes = $temp_rating = $vote_count = $seats = $seat_total = $avg_seats= 0;

		foreach ($report as $thisFilm) {
			$seatcount = 0;
			foreach ($full_schedule as $thisScreening) {
				$movie_ids = explode(",", $thisScreening->program_movie_ids);
				foreach ($movie_ids as $thisMovieID) { if ($thisFilm->movie_id == $thisMovieID) { $seatcount = $thisScreening->seats; } }
			}
		
			if ($thisFilm->votes_1 == 0 && $thisFilm->votes_2 == 0 && $thisFilm->votes_3 == 0 && $thisFilm->votes_4 == 0 && $thisFilm->votes_5 == 0) {
				// No votes
			} else {
				$temp_votes = $temp_votes + ($thisFilm->votes_1 + $thisFilm->votes_2 + $thisFilm->votes_3 + $thisFilm->votes_4 + $thisFilm->votes_5);
				$temp_rating = $temp_rating + $thisFilm->votes_total;
				if ($seatcount != 0) { $seat_array[] = $seatcount; }
				$vote_count++;
			}
		}
		
		
		$avg_num_votes = round($temp_votes / $vote_count);
		$avg_rating = round($temp_rating / $vote_count);
		$unique_seats = array_unique($seat_array);
		foreach ($unique_seats as $useats) { $seat_total = $seat_total + $useats; }
		$avg_seats = round($seat_total / count($unique_seats));

		// Process report to determine adjusted total
		foreach ($report as $thisRec) {
			$seatcount = $location_id2 = 0;
			foreach ($full_schedule as $thisScreening) {
				$movie_ids = explode(",", $thisScreening->program_movie_ids);
				foreach ($movie_ids as $thisMovieID) { if ($thisRec->movie_id == $thisMovieID && $thisScreening->Private != 1) { $seatcount = $thisScreening->seats; $location_id2 = $thisScreening->location_id2; break(2); } }
			}		
			if ($location_id2 == 0) { $seats = $seatcount; } else { $seats = $seatcount * 2; }
		
			$adjusted_total = round($thisRec->votes_total * ($avg_seats / $seats));
			if ($thisRec->votes_num > $avg_seats) { $adjusted_total = $adjusted_total + ($thisRec->votes_num - $avg_seats); }

			$thisRec->house_seats = $seats;
			$thisRec->adjusted_total = $adjusted_total;				

			// Remove unneeded values from report
			unset($thisRec->id);
			unset($thisRec->movie_id);
			unset($thisRec->premiere_id);
			unset($thisRec->title);
			unset($thisRec->slug);
		}

		date_default_timezone_set(FP_BASE_TIMEZONE);
		$currentdate = date("m-d-Y");
		$this->reports->csv_download($report,"ballot-counts-".$currentdate.".csv");
	}

	function export_ratings_views() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Reportsmodel','reports');
		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_rating_counts($current_fest);
		$section_array = convert_to_array2($this->section->get_all_sections($current_fest));

		// Process report to replace category ids with the section names
		foreach ($report as $thisRec) {
			$thisRec->section = $section_array[$thisRec->category_id];

			// Remove unneeded values from report
			unset($thisRec->id);
			unset($thisRec->movie_id);
			unset($thisRec->category_id);
			unset($thisRec->used_ips);
			unset($thisRec->slug);
		}

		date_default_timezone_set(FP_BASE_TIMEZONE);
		$currentdate = date("m-d-Y");
		$this->reports->csv_download($report,"views-ratings-".$currentdate.".csv");
	}

	function export_profit_loss() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Genremodel','genre');
		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$section_array = convert_to_array2($this->section->get_all_sections($current_fest));
		$filmids = $this->films->get_all_internal_film_ids($current_fest);

		$movie_id_array = array();
		foreach ($filmids as $thisId) {
			$movie_id_array[] = $thisId->movie_id;
		}
		if (count($movie_id_array) == 0) {
			$movie_id_array[] = 0;
		}
		$genre_films = $this->genre->get_all_genres_in($movie_id_array);

		$report = $this->reports->get_profit_loss($current_fest);
		$report_ship = $this->reports->get_profit_loss_shipping($current_fest);
		$report_shorts = $this->reports->get_profit_loss_shorts($current_fest, $movie_id_array);
		foreach ($report_shorts as $thisRecord) {
			$film_id_array = explode(",", $thisRecord->program_movie_ids);
			$RentalFee = 0; $ScreeningFee = 0; $TransferFee = 0; $GuestFee = 0;

			$program_results = $this->reports->get_profit_loss_expenses($film_id_array);
			foreach ($program_results as $thisMovie) {
				$RentalFee += $thisMovie->RentalFee;
				$ScreeningFee += $thisMovie->ScreeningFee;
				$TransferFee += $thisMovie->TransferFee;
				$GuestFee += $thisMovie->GuestFee;
			}

			// Update original report with all fees for shorts programs, etc
			foreach ($report as $thisProfitLoss) {
				if ($thisRecord->program_movie_ids == $thisProfitLoss->program_movie_ids) {
					$thisProfitLoss->RentalFee = $RentalFee;
					$thisProfitLoss->ScreeningFee = $ScreeningFee;
					$thisProfitLoss->TransferFee = $TransferFee;
					$thisProfitLoss->GuestFee = $GuestFee;
				}
			}
		}

		// Process report to replace category ids with the section names
		$revenue = $rental = $screening = $dubbing = $shipping = $guestfees = 0;
		foreach ($report as $thisRec) {
			$newtitle = "";
			$film_array = explode(",", $thisRec->program_movie_ids);
			if (count($film_array) > 1) {
				if ($thisRec->program_name == "") {
					foreach ($film_array as $thisFilmId) {
						foreach ($filmids as $filmIdList) {
							if ($filmIdList->movie_id == $thisFilmId) {
								$newtitle .= switch_title($filmIdList->label)." + ";
							}
						}
					} $newtitle = trim($newtitle," + ");
					$thisRec->title_en = $newtitle;
				}
			}

			if ($thisRec->program_name != "") {
				$thisRec->title_en = $thisRec->program_name;
			}
		}
		usort($report,"filmtitlecmp");

		foreach ($report as $thisRec) {
			$ShippingFee = 0;

			$genre_list = "";
			foreach ($genre_films as $thisMovieGenre) {
				if ($thisMovieGenre->movie_id == $thisRec->movie_id) {
					$genre_list .= $thisMovieGenre->name.", ";
				}
			} $thisRec->genre = trim($genre_list,", ");

			foreach ($report_ship as $thisShip) {
				if ($thisShip->ShippingFee != 0) {
					if (strstr($thisRec->program_movie_ids, $thisShip->movie_id) != FALSE) { $ShippingFee += $thisShip->ShippingFee; }
				}
			}
			$thisRec->section = $section_array[$thisRec->category_id];
			$thisRec->ShippingFee = $ShippingFee;
			$thisRec->Profit_Loss = round($thisRec->FilmRevenue - ($thisRec->RentalFee + $thisRec->ScreeningFee + $thisRec->TransferFee + $thisRec->GuestFee + $ShippingFee),2);
			
			// Total up revenue and fee values
			$revenue += $thisRec->FilmRevenue;
			$rental += $thisRec->RentalFee;
			$screening += $thisRec->ScreeningFee;
			$dubbing += $thisRec->TransferFee;
			$guestfees += $thisRec->GuestFee;
			$shipping += $ShippingFee;

			// Remove unneeded values from report
			unset($thisRec->id);
			unset($thisRec->movie_id);
			unset($thisRec->category_id);
			unset($thisRec->program_movie_ids);
			unset($thisRec->program_name);
			unset($thisRec->slug);
		}

		// Add a new row to the report with totals of the various figures
		$newRow = new stdClass();
		$newRow->title_en = "";
		$newRow->section = "";
		$newRow->event_name = "Totals: ";
		$newRow->FilmRevenue = round($revenue,2);
		$newRow->RentalFee = round($rental,2);
		$newRow->ScreeningFee = round($screening,2);
		$newRow->TransferFee = round($dubbing,2);
		$newRow->GuestFee = round($guestfees,2);
		$newRow->ShippingFee = round($shipping,2);
		$newRow->Profit_Loss = round($revenue - ($rental + $screening + $dubbing + $guestfees + $shipping),2);
		$report[] = $newRow;

		date_default_timezone_set(FP_BASE_TIMEZONE);
		$currentdate = date("m-d-Y");
		$this->reports->csv_download($report,"profit-loss-".$currentdate.".csv");
	}

}

/* End of file film_reports.php */
/* Location: ./system/application/controllers/admin/film_reports.php */
?>