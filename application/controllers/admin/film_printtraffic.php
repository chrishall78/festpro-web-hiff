<?php
class Film_Printtraffic extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Filmsmodel','films');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$film_array = array();
		$fest_films = $this->films->get_all_film_ids2($current_fest);
		foreach ($fest_films as $film) { $film_array[] = $film->id; }
		if (count($fest_films) == 0) { $film_array[] = 0; }

		$data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "asc");
		$data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "asc");		
		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['courier'] = $this->filmtype->get_all_type("courier","asc");

		$vars['title'] = "Print Traffic";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_printtraffic";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_printtraffic', $data);
		$this->load->view('admin/footer', $vars);
	}

	function sort($traffictype, $sorttoken)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Filmsmodel','films');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$film_array = array();
		$fest_films = $this->films->get_all_film_ids2($current_fest);
		foreach ($fest_films as $film) { $film_array[] = $film->id; }

		switch ($traffictype) {
			case "shipping":
				$tablename = "wf_printtraffic_shipments.";
				switch ($sorttoken) {
					case "namedesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "desc"); break;
					case "nameasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "asc");  break;

					case "formatdesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"VideoFormat", "desc"); break;
					case "formatasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"VideoFormat", "asc");  break;

					case "eventdesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"EventType", "desc"); break;
					case "eventasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"EventType", "asc");  break;
		
					case "courierdesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"Courier", "desc"); break;
					case "courierasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"Courier", "asc");  break;

					case "accountdesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."AccountNum", "desc"); break;
					case "accountasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."AccountNum", "asc");  break;

					case "trackingdesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."TrackingNum", "desc"); break;
					case "trackingasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."TrackingNum", "asc");  break;

					case "shippeddesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."OutShipped", "desc"); break;
					case "shippedasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."OutShipped", "asc");  break;

					case "arrivedesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."OutArriveBy", "desc"); break;
					case "arriveasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."OutArriveBy", "asc");  break;
		
					case "feedesc": $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."ShippingFee", "desc"); break;
					case "feeasc":  $data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,$tablename."ShippingFee", "asc");  break;					
				}
		
				$data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "asc");
				break;

			case "receiving":
				$tablename = "wf_printtraffic_shipments.";
				switch ($sorttoken) {
					case "namedesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "desc"); break;
					case "nameasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "asc");  break;

					case "formatdesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"VideoFormat", "desc"); break;
					case "formatasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"VideoFormat", "asc");  break;

					case "eventdesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"EventType", "desc"); break;
					case "eventasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"EventType", "asc");  break;
		
					case "courierdesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"Courier", "desc"); break;
					case "courierasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"Courier", "asc");  break;

					case "accountdesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."AccountNum", "desc"); break;
					case "accountasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."AccountNum", "asc");  break;

					case "trackingdesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."TrackingNum", "desc"); break;
					case "trackingasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."TrackingNum", "asc");  break;
		
					case "receivedesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."InReceived", "desc"); break;
					case "receiveasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."InReceived", "asc");  break;

					case "arrivaldesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."InExpectedArrival", "desc"); break;
					case "arrivalasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."InExpectedArrival", "asc");  break;
		
					case "feedesc": $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."ShippingFee", "desc"); break;
					case "feeasc":  $data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,$tablename."ShippingFee", "asc");  break;
				}

				$data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "asc");
				break;
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['courier'] = $this->filmtype->get_all_type("courier","asc");

		$vars['title'] = "Print Traffic";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_printtraffic";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_printtraffic', $data);
		$this->load->view('admin/footer', $vars);
	}

	// Outbound Shipments
	function ship_outbound_shipment($shipment_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$todaysDate = date("Y-m-d");
		$values = array("confirmed" => 1, "OutShipped" => $todaysDate);
		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$shipment_id);
	}
	function unship_outbound_shipment($shipment_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$values = array("confirmed" => 0);
		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$shipment_id);
	}

	// Inbound Shipments
	function receive_inbound_shipment($shipment_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$todaysDate = date("Y-m-d");
		$values = array("confirmed" => 1, "InReceived" => $todaysDate);
		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$shipment_id);
	}
	function unreceive_inbound_shipment($shipment_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$values = array("confirmed" => 0);
		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$shipment_id);
	}


	function add_shipment() {
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Filmsmodel','films');

		$this->load->helper('form');
		
		$film_details = $this->films->get_film2($this->input->post('festivalmovie_id-new'));
		$videoformat_array = convert_to_array2($this->filmtype->get_all_type("format","asc"));
		$courier_array = convert_to_array2($this->filmtype->get_all_type("courier","asc"));
		$event_array = convert_to_array2($this->filmtype->get_all_type("event","asc"));
		
		$shipment_type = $this->input->post('outbound-new');

		if ($shipment_type == 1) {
			// Inbound Shipment
			$values = array(
				"festivalmovie_id" => $this->input->post('festivalmovie_id-new'),
				"courier_id" => $this->input->post('courier_id-new'),
				"InExpectedArrival" => $this->input->post('InExpectedArrival-new'),
				"InReceived" => $this->input->post('InReceived-new'),
				"AccountNum" => $this->input->post('AccountNum-new'),
				"TrackingNum" => $this->input->post('TrackingNum-new'),
				"ShippingFee" => $this->input->post('ShippingFee-new'),
				"wePay" => $this->input->post('wePay-new'),
				"notes" => $this->input->post('notes-new'),
				"confirmed" => 0,
				"outbound" => 0
			);

			$new_id = $this->printtraffic->add_value($values,"wf_printtraffic_shipments");

		} elseif ($shipment_type == 2) {
			// Outbound Shipment
			$values = array(
				"festivalmovie_id" => $this->input->post('festivalmovie_id-new'),
				"courier_id" => $this->input->post('courier_id-new'),
				"OutShipped" => $this->input->post('OutShipped-new'),
				"OutArriveBy" => $this->input->post('OutArriveBy-new'),
				"AccountNum" => $this->input->post('AccountNum-new'),
				"TrackingNum" => $this->input->post('TrackingNum-new'),
				"ShippingFee" => $this->input->post('ShippingFee-new'),
				"wePay" => $this->input->post('wePay-new'),
				"notes" => $this->input->post('notes-new'),
				"confirmed" => 0,
				"outbound" => 1
			);
			
			$new_id = $this->printtraffic->add_value($values,"wf_printtraffic_shipments");
		}
			$id = "-".$new_id;

		if ($shipment_type == 1) {
			print "<div id=\"Inbound-ReplaceMe\"><input type=\"hidden\" value=\"".$new_id."\" id=\"new-inbound-id\" name=\"new-inbound-id\"></div>";
		} elseif ($shipment_type == 2) {
			print "<div id=\"Outbound-ReplaceMe\"><input type=\"hidden\" value=\"".$new_id."\" id=\"new-outbound-id\" name=\"new-outbound-id\"></div>";
		}

		if ($shipment_type == 1) {
			print "\t\t<form name=\"filmReceivingForm".$id."\" id=\"filmReceivingForm".$id."\">\n";
			print "\t\t<input type=\"hidden\" name=\"film-name".$id."\" id=\"film-name".$id."\" value=\"".switch_title($film_details[0]->title_en)."\" />\n";
			print "\t\t<input type=\"hidden\" name=\"movie_id".$id."\" id=\"movie_id".$id."\" value=\"".$film_details[0]->movie_id."\" />\n";
			print "\t\t<table class=\"FilmReceivingTab\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t\t\t<tbody>\n";
		} elseif ($shipment_type == 2) {
			print "\t\t<form name=\"filmShippingForm".$id."\" id=\"filmShippingForm".$id."\">\n";
			print "\t\t<input type=\"hidden\" name=\"film-name".$id."\" id=\"film-name".$id."\" value=\"".switch_title($film_details[0]->title_en)."\" />\n";
			print "\t\t<input type=\"hidden\" name=\"movie_id".$id."\" id=\"movie_id".$id."\" value=\"".$film_details[0]->movie_id."\" />\n";
			print "\t\t<table class=\"FilmShippingTab\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t\t\t<tbody>\n";
		}
	
			// First Row
			print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		if ($shipment_type == 1) {
			print "\t\t\t\t<td width=\"70\" rowspan=\"2\" align=\"center\"><div class=\"button ui-state-default\"><a href=\"/admin/film_edit/update/".$film_details[0]->slug."/#tabs-5\">Update</a></div><br />";
			if ($values['confirmed'] == 0) {
				print "<button id=\"receiving-receive".$id."\" data-id=\"".$new_id."\" class=\"receive\">Receive</button>";
			}
		} elseif ($shipment_type == 2) {
			print "\t\t\t\t<td width=\"70\" rowspan=\"2\" align=\"center\"><div class=\"button ui-state-default\"><a href=\"/admin/film_edit/update/".$film_details[0]->slug."/#tabs-5\">Update</a></div>";
			if ($values['confirmed'] == 0) {
				print "<button id=\"shipping-ship".$id."\" data-id=\"".$new_id."\" class=\"ship\">Ship</button>";
			}

		}
			print "</td>\n";
			print "\t\t\t\t<td width=\"150\"><strong>".switch_title($film_details[0]->title_en)."</strong></td>\n";
			print "\t\t\t\t<td width=\"150\">".$event_array[$film_details[0]->event_id] ."</td>\n";
			print "\t\t\t\t<td width=\"150\">".$courier_array[$values['courier_id']]."</td>\n";
			print "\t\t\t\t<td width=\"150\">".$values['TrackingNum']."</td>\n";
			print "\t\t\t\t<td width=\"120\">$".$values['ShippingFee']."</td>\n";
			print "\t\t\t\t<td width=\"175\" rowspan=\"2\">".$values['notes']."</td>\n";
			print "\t\t\t</tr>\n";
	
			// Second Row
			print "\t\t\t<tr valign=\"top\" class=\"evenrow2\">\n";
			print "\t\t\t\t<td><strong>".$videoformat_array[$film_details[0]->format_id]."</strong></td>\n";

		if ($shipment_type == 1) {
			$values['InExpectedArrival'] == "" ? print "\t\t\t\t<td>&nbsp;</td>\n" : print "\t\t\t\t<td>".date("m-d-Y",strtotime($values['InExpectedArrival']))."</td>\n";
			$values['InReceived'] == "" ? print "\t\t\t\t<td>&nbsp;</td>\n" : print "\t\t\t\t<td>".date("m-d-Y",strtotime($values['InReceived']))."</td>\n";
		} elseif ($shipment_type == 2) {
			$values['OutShipped'] == "" ? print "\t\t\t\t<td>&nbsp;</td>\n" : print "\t\t\t\t<td>".date("m-d-Y",strtotime($values['OutShipped']))."</td>\n";
			$values['OutArriveBy'] == "" ? print "\t\t\t\t<td>&nbsp;</td>\n" : print "\t\t\t\t<td>".date("m-d-Y",strtotime($values['OutArriveBy']))."</td>\n";
		}
			print "\t\t\t\t<td>".$values['AccountNum']."</td>\n";
			print "\t\t\t\t<td><label>We Pay:</label> ";
			if (intval($values['wePay']) == 1) {
				print "Yes";
			} else {
				print "No";
			}
			print "</td>\n";
			print "\t\t\t</tr>\n";
	
			print "\t\t\t</tbody>\n";
			print "\t\t</table>\n";
			print "\t\t</form>\n";
	}
}

/* End of file film_printtraffic.php */
/* Location: ./system/application/controllers/admin/film_printtraffic.php */
?>