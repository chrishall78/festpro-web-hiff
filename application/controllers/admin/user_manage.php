<?php
class User_Manage extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		if ($this->session->userdata('admin') == 1) {
			$this->load->helper('form');
	
			$this->load->model('Usersmodel','users');
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}
			
			$data['user_list'] = $this->users->get_all_users();
			$data['user_json'] = json_encode($data['user_list']);
			$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
	
			$vars['title'] = "Manage Users";
			$vars['path'] = "/";
			$vars['selected_page'] = "user_manage";
			$vars['filmJSON'] = json_encode($data['filmids']);
			$vars['festivals'] = $this->festival->get_all_festivals();
	
			$this->load->view('admin/header', $vars);
			$this->load->view('admin/nav_bar', $vars);
			$this->load->view('admin/user_manage', $data);
			$this->load->view('admin/footer', $vars);
		} else {
			$this->load->helper('url');
			redirect('/admin/film_listing', 'refresh');
		}
	}

	function add_user()
	{
		$this->load->model('Usersmodel','users');
		$this->load->library('encrypt');
		
		// Encrypt password
		$Password1 = $this->input->post('Password1');
		$Password2 = $this->input->post('Password2');
		$EncryptedPassword = "";
		
		if ($Password1 == $Password2 && $Password1 != "" && $Password2 != "") {
			$EncryptedPassword = $this->encrypt->encode($Password1);
		} else {
			$EncryptedPassword = "xXxPasswordMismatchxXx";
		}
		
		// Check on user access levels
		$enabled = 1; $limited = $adminaccess = 0;
		switch ( $this->input->post('addAccessLevel') ) {
			case "Disabled": $enabled = 0; $limited = 0; $adminaccess = 0;
							break;
			case "Limited": $enabled = 1; $limited = 1; $adminaccess = 0;
							break;
			case "Normal": $enabled = 1; $limited = 0; $adminaccess = 0;
							break;
			case "Administrator": $enabled = 1; $limited = 0; $adminaccess = 1;
							break;
			default:	$enabled = 1; $limited = 0; $adminaccess = 0;
						break;
		}
		
		// Assemble values for user insert
		$values = array(
			"Username" => $this->input->post('Username'),
			"FirstName" => $this->input->post('FirstName'),
			"LastName" => $this->input->post('LastName'),
			"Email" => $this->input->post('Email'),
			"Password" => $EncryptedPassword,
			"Regdate" => date("Y-m-d"),
			"LastLogin" => NULL,
			"Enabled" => $enabled,
			"Admin" => $adminaccess,
			"LimitedAccess" => $limited,
			"Programming" => "1",
			"Guest" => 1
		);
		$UsernameCheck = $this->users->get_username($values['Username']);
		
		if (count($UsernameCheck) >= 1) {
			// Username already exists, abort user add
			print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
			print "\t\t\t\t<td class=\"cent\" colspan=\"8\"><b>Sorry, this username is already taken. Please try again.</b></td>\n";
			print "\t\t\t</tr>\n";
		} else {
			// Username does not exist, go ahead with user add
			$new_id = $this->users->add_value($values, "users");

			print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
			print "\t\t\t\t<td class=\"cent\"><button class=\"edit\" data-id=\"".$new_id."\">Edit</button></td>\n";
			print "\t\t\t\t<td>".$values["FirstName"]." ".$values["LastName"]."</td>\n";
			print "\t\t\t\t<td>".$values["Username"]."</td>\n";
			print "\t\t\t\t<td>".$values["Email"]."</td>\n";
			print "\t\t\t\t<td>&nbsp;</td>\n";
			if ($values["Enabled"] == "1") {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			if ($values["LimitedAccess"] == "1") {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			if ($values["Admin"] == "1") {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			} else {
				print "\t\t\t\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
			}
			print "\t\t\t</tr>\n";
		}
	}

	function update_user()
	{
		$this->load->model('Usersmodel','users');
		$this->load->library('encrypt');

		// Check on user access levels
		$enabled = 1; $limited = $adminaccess = 0;
		switch ( $this->input->post('updateAccessLevel') ) {
			case "Disabled": $enabled = 0; $limited = 0; $adminaccess = 0;
							break;
			case "Limited": $enabled = 1; $limited = 1; $adminaccess = 0;
							break;
			case "Normal": $enabled = 1; $limited = 0; $adminaccess = 0;
							break;
			case "Administrator": $enabled = 1; $limited = 0; $adminaccess = 1;
							break;
			default:	$enabled = 1; $limited = 0; $adminaccess = 0;
						break;
		}
				
		// Assemble values for user update
		$values = array(
			"Username" => $this->input->post('Username-update'),
			"FirstName" => $this->input->post('FirstName-update'),
			"LastName" => $this->input->post('LastName-update'),
			"Email" => $this->input->post('Email-update'),
			"Enabled" => $enabled,
			"Admin" => $adminaccess,
			"LimitedAccess" => $limited
		);

		// Encrypt password
		$Password1 = $this->input->post('Password1-update');
		$Password2 = $this->input->post('Password2-update');
		
		if ($Password1 == "" && $Password2 == "") {
			// password fields are blank - don't change password
		} else if ($Password1 == $Password2 && $Password1 != "" && $Password2 != "") {
			// both passwords match - change password
			$values["Password"] = $this->encrypt->encode($Password1);
		}

		$updated_id = $this->users->update_value($values, "users", $this->input->post('user-id-update'));

		print "\t\t\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t<td class=\"cent\"><button class=\"edit\" data-id=\"".$updated_id."\">Edit</button></td>\n";
		print "\t<td>".$values["FirstName"]." ".$values["LastName"]."</td>\n";
		print "\t<td>".$values["Username"]."</td>\n";
		print "\t<td>".$values["Email"]."</td>\n";
		if ($this->input->post('last-login') != "" || $this->input->post('last-login') != NULL) {
			print "\t<td>".date("M j Y",strtotime($this->input->post('last-login')))."</td>\n";
		} else {
			print "\t<td>&nbsp;</td>\n";
		}
		if ($values["Enabled"] == "1") {
			print "\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		} else {
			print "\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		}
		if ($values["LimitedAccess"] == "1") {
			print "\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		} else {
			print "\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		}
		if ($values["Admin"] == "1") {
			print "\t<td class=\"cent\"><img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		} else {
			print "\t<td class=\"cent\"><img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /></td>\n";
		}
		print "\t</tr>\n";
	}

	function delete_user()
	{
		$this->load->model('Usersmodel','users');
		$user_id = $this->input->post('user-id-update');

		$UserCheck = $this->users->get_user($user_id);
		
		if (count($UserCheck) == 1) {
			// User exists, go ahead with delete
			$this->users->delete_value($user_id, "users");
		} else {
			// User does not exist, don't do anything
		}
		

	}

	function return_user_json() {
		$this->load->model('Usersmodel','users');
		print json_encode($this->users->get_all_users());
	}


	function check_username()
	{
		$this->load->model('Usersmodel','users');

		if ($this->input->post('Username') == "") {
			print "\t<img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /> Please enter a Username.\n";
		} else {
			$username = $this->users->get_username($this->input->post('Username'));
		
			if (count($username) >= 1) {
				print "\t<img src=\"/assets/images/cross.png\" height=\"16\" width=\"16\" border=\"0\" /> This Username is taken.\n";
			} else {
				print "\t<img src=\"/assets/images/tick.png\" height=\"16\" width=\"16\" border=\"0\" /> This Username is available.\n";
			}
		}
	}

}

/* End of file user_manage.php */
/* Location: ./system/application/controllers/admin/user_manage.php */
?>