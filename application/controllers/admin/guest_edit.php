<?php
class Guest_Edit extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}

	function index()
	{
	}
	
	function update($slug="novalue", $saved_guest_title="none")
	{
		if ($slug == "novalue") {
			header('Location: /admin/guest_listing/');
		} else {
			$this->load->helper('form');	
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Filmtypesmodel','filmtype');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Guestmodel','guest');
			$this->load->model('Guesttypesmodel','guesttype');
			$this->load->model('Flightmodel','flight');
			$this->load->model('Hotelmodel','hotel');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			// Add data calls here
			$data['guest'] = $this->guest->get_guest_by_slug($slug);
			$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
			$data['guestlist'] = array(); foreach ($data['guestids'] as $guests) { $data['guestlist'][] = $guests->guest_id; }
			$data['film_affiliations'] = $this->guest->get_guest_film_affiliation($data['guest'][0]->id);
			$data['flights'] = $this->flight->get_all_guest_flights($data['guest'][0]->id);
			$data['flight_comps'] = $this->flight->get_flight_comps($current_fest);
			$data['hotels'] = $this->hotel->get_all_guest_hotels($data['guest'][0]->id);
			$data['hotel_comps'] = $this->hotel->get_hotel_comps($current_fest);
			$data['hotel_used'] = $this->hotel->get_used_hotel_comps($data['guestlist']);
			$data['hotel_shares'] = $this->guest->get_all_guests_except($current_fest, $data['guest'][0]->id);
			$data['event_appearances'] = $this->guest->get_guest_special_appearances($data['guest'][0]->id);
			$data['existing_events'] = $this->guest->get_existing_events($data['guestlist']);

			$data['films'] = $this->films->get_all_internal_films($current_fest);
			$data['guesttypes'] = $this->guesttype->get_all_type("guests","asc");
			$data['languages'] = $this->filmtype->get_all_type("language","asc");
			$data['air_domestic'] = $this->guesttype->get_type_airports("domestic");
			$data['air_intl'] = $this->guesttype->get_type_airports("international");
			$data['airlines'] = $this->guesttype->get_all_type("airlines", "asc", "no");

			foreach ($data['film_affiliations'] as $thisFilmAff) {
				if ($thisFilmAff->movie_id == 0) {
					$this->guest->delete_value($thisFilmAff->aff_id, "fg_filmaffiliations");
				}
			}

			if ($saved_guest_title != "none") {
				$data['saved_guest_title'] = $data['guest'][0]->Firstname." ".$data['guest'][0]->Lastname;
			} else {
				$data['saved_guest_title'] = "none";
			}
	
			$vars['title'] = "Now editing: ".$data['guest'][0]->Firstname." ".$data['guest'][0]->Lastname;
			$vars['path'] = "/";
			$vars['selected_page'] = "guest_edit";
			$vars['guestJSON'] = json_encode($data['guestids']);
			$vars['festivals'] = $this->festival->get_all_festivals();
	
			$this->load->view('admin/header', $vars);
			$this->load->view('admin/nav_bar', $vars);
			$this->load->view('admin/guest_edit', $data);
			$this->load->view('admin/footer', $vars);
		}
	}

	function save() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Schedulemodel','schedule');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$festivalguest_id = $this->input->post('festivalguest_id');
		$film_aff_movie_temp = trim($this->input->post('film_aff_movie_ids'),",");
		$film_aff_movie_ids = explode(",", $film_aff_movie_temp);
		$film_aff_ids = explode(",", $this->input->post('film_aff_ids'));

		// Update film affiliation values
		foreach ($film_aff_ids as $thisFilmAff) {
			// If we're saving and find a value for this film aff, update it. If no value exists, delete the record.
			$movie_value = $this->input->post("film_affiliation_".$thisFilmAff);
			if ($movie_value != 0) {
				$values = array("movie_id" => $this->input->post("film_affiliation_".$thisFilmAff));
				$this->guest->update_value($values, "fg_filmaffiliations", $thisFilmAff);
			} else {
				// Get the movie id for this film, to get screening ids.
				$filmaff = $this->guest->get_film_affiliation($thisFilmAff);
				
				// Don't do any of this if there are no film affiliations defined
				if ( count($filmaff) > 0) {
					$movie_id = $filmaff[0]->movie_id;

					$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $movie_id);
					$screening_array = array();
					foreach ($screenings as $thisScreening) { $screening_array[] = $thisScreening->id;}
				
					// Check for any screening appearances for this film+guest & delete them
					foreach ($screening_array as $screening_id) {
						$screening_app = $this->guest->get_screening_appearance($screening_id, $festivalguest_id);
						$toDelete = array();
						foreach ($screening_app as $thisScreeningApp) { $toDelete[] = $thisScreeningApp->id; }
						foreach ($toDelete as $toDeleteId) { $this->guest->delete_value($toDeleteId, "fg_screeningappearances"); }
					}
				
					// Delete the actual film affiliation record
					$this->guest->delete_value($thisFilmAff, "fg_filmaffiliations");
				}
			}
		}

		// Update fg_festivalguests values
		$values1 = array(
			"Firstname" => $this->input->post('Firstname'),
			"MI" => $this->input->post('MI'),
			"Lastname" => $this->input->post('Lastname'),
			"guesttype_id" => $this->input->post('guesttype_id'),
			"Address" => $this->input->post('Address'),
			"City" => $this->input->post('City'),
			"State" => $this->input->post('State'),
			"PostalCode" => $this->input->post('PostalCode'),
			"Phone" => $this->input->post('Phone'),
			"Cell" => $this->input->post('Cell'),
			"Fax" => $this->input->post('Fax'),
			"language_id" => $this->input->post('language_id'),
			"Email" => $this->input->post('Email'),
			"TwitterName" => $this->input->post('TwitterName'),
			"FacebookPage" => $this->input->post('FacebookPage'),
			"LinkedinProfile" => $this->input->post('LinkedinProfile'),
			"WebsiteURL" => $this->input->post('WebsiteURL'),
			"AffiliationOptional" => $this->input->post('AffiliationOptional'),
			"notes" => $this->input->post('notes'),
			"Bio" => $this->input->post('Bio'),
			"gb_pickedup" => $this->input->post('gb_pickedup'),
			"badge_pickedup" => $this->input->post('badge_pickedup'),
			"updated_date" => date("Y-m-d H:i:s")
		);
		if ($this->input->post('gb_pickedup')) { $values1["gb_pickedup"] = 1; } else { $values1["gb_pickedup"] = 0; }
		if ($this->input->post('badge_pickedup')) { $values1["badge_pickedup"] = 1; } else { $values1["badge_pickedup"] = 0; }
		$this->guest->update_value($values1, "fg_festivalguests", $festivalguest_id);
		header('Location: /admin/guest_edit/update/'.$this->input->post('guest_slug').'/saved/');
	}

	function add_filmaff() {
		$this->load->helper('form');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		$films_array = convert_films_to_array($this->films->get_all_internal_films($current_fest));
		
		$filmaff_id = $this->guest->add_value(array('festivalguest_id' => $this->input->post('guest_id'), 'movie_id' => 0), "fg_filmaffiliations");
		print form_dropdown('film_affiliation_'.$filmaff_id, $films_array, 0,"class='select-film ui-widget-content ui-corner-all'");
		print " <a href=\"#\" id=\"film_aff_remove_".$filmaff_id."\" class=\"remove\"><img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/cancel.png\" alt=\"Remove Film Affiliation\" title=\"Remove Film Affiliation\"></a><br />";
		print "<div id=\"addFilmAffHere\"><input type=\"hidden\" name=\"addFilmAffId\" id=\"addFilmAffId\" value=\"".$filmaff_id."\" /></div>";
	}

	function add_flight() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Flightmodel','flight');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$airlines = $this->guesttype->get_all_type("airlines", "asc", "no");
		$airlines_array = convert_to_array($airlines);
		$flight_comp_array = convert_to_array($this->flight->get_flight_comps($current_fest));
		$arrival_time = date("H:i:s",strtotime($this->input->post('arr-time-add')));
		$departure_time = date("H:i:s",strtotime($this->input->post('dep-time-add')));
		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"ArrivalDate" => $this->input->post('arr-date-add'),
			"ArrivalTime" => $arrival_time,
			"ArrivalCity" => $this->input->post('arr-city1-add'),
			"ArrivalCity2" => $this->input->post('arr-city2-add'),
			"Airline" => $this->input->post('arr-airline-add'),
			"FlightNumber" => $this->input->post('arr-fltnum-add'),
			"DepartureDate" => $this->input->post('dep-date-add'),
			"DepartureTime" => $departure_time,
			"DepartureCity" => $this->input->post('dep-city1-add'),
			"DepartureCity2" => $this->input->post('dep-city2-add'),
			"DepartureAirline" => $this->input->post('dep-airline-add'),
			"DepartureFlightNum" => $this->input->post('dep-fltnum-add'),
			"cost" => $this->input->post('flight-cost-add'),
			"mileage" => $this->input->post('flight-mileage-add'),
			"flightcomp_id" => $this->input->post('flight-comp-add'),
			"Notes" => $this->input->post('flight-notes-add')
		);
		if ($this->input->post('flight-pickup-add')) { $values["pickup"] = 1; } else { $values["pickup"] = 0; }
		if ($this->input->post('flight-translator-add')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		$flight_id = $this->guest->add_value($values,"fg_flights");

		// Print new row for this flight
		print "\t<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"FlightsTab\">\n";
		print "\t\t<tbody style=\"border-top: medium none;\">\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\" width=\"55\"><button id=\"flgt-".$flight_id."\" name=\"flgt-".$flight_id."\" data-id=\"".$flight_id."\" class=\"edit\">Edit</button></td>\n";

		print "\t\t\t<td width=\"195\">";
		if ($values['ArrivalDate'] == "0000-00-00" || $values['ArrivalDate'] == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($values['ArrivalDate']))." "; }
		if ($values['ArrivalTime'] != "00:00:00") { print date("g:iA",strtotime($values['ArrivalTime']))."<br />"; } else { print "<br />"; }
		if ($values['ArrivalCity'] != "" && $values['ArrivalCity2'] != "") { print $values['ArrivalCity']."-".$values['ArrivalCity2']." / "; }
		elseif ($values['ArrivalCity'] == "" && $values['ArrivalCity2'] != "") { print $values['ArrivalCity2']." / "; }
		elseif ($values['ArrivalCity'] != "" && $values['ArrivalCity2'] == "") { print  $values['ArrivalCity']." / "; }
		if ($values['Airline'] != 0) { print $airlines_array[$values['Airline']]; }
		if ($values['FlightNumber'] != "") { print " #".$values['FlightNumber']; }
		print "</td>\n";

		print "\t\t\t<td width=\"195\">";
		if ($values['DepartureDate'] == "0000-00-00" || $values['DepartureDate'] == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($values['DepartureDate']))." "; }
		if ($values['DepartureTime'] != "00:00:00") { print date("g:iA",strtotime($values['DepartureTime']))."<br />"; } else { print "<br />"; }
		if ($values['DepartureCity'] != "" && $values['DepartureCity2'] != "") { print $values['DepartureCity']."-".$values['DepartureCity2']." / "; }
		elseif ($values['DepartureCity'] == "" && $values['DepartureCity2'] != "") { print $values['DepartureCity2']." / "; }
		elseif ($values['DepartureCity'] != "" && $values['DepartureCity2'] == "") { print  $values['DepartureCity']." / "; }
		if ($values['DepartureAirline'] != 0) { print $airlines_array[$values['DepartureAirline']]; }
		if ($values['DepartureFlightNum'] != "") { print " #".$values['DepartureFlightNum']; }
		print "</td>\n";

		print "\t\t\t<td width=\"35\">$".$values['cost']."</td>\n";
		print "\t\t\t<td width=\"46\">".$values['mileage']."</td>\n";
		print "\t\t\t<td width=\"175\">".$flight_comp_array[$values['flightcomp_id']]."</td>\n";
		if ($values['pickup'] != "0") {
			print "\t<td width=\"101\"><a id=\"pickupf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"pickupf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"101\"><a id=\"pickupf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"pickupf\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] != "0") {
			print "\t<td width=\"64\"><a id=\"translatorf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"translatorf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"64\"><a id=\"translatorf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"translatorf\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		if ($values['Notes'] != "") {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\"><label>Notes:</label> ".$values['Notes']."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\">&nbsp;</td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t</tbody>\n";
		print "\t</table>\n";
		print "\t<div id=\"addFlightHere\"><input type=\"hidden\" id=\"flight-id-new\" name=\"flight-id-new\" value=\"".$flight_id."\" /></div>";
	}

	function update_flight() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Flightmodel','flight');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$airlines = $this->guesttype->get_all_type("airlines", "asc", "no");
		$airlines_array = convert_to_array($airlines);
		$flight_comp_array = convert_to_array($this->flight->get_flight_comps($current_fest));
		$arrival_time = date("H:i:s",strtotime($this->input->post('arr-time-edit')));
		$departure_time = date("H:i:s",strtotime($this->input->post('dep-time-edit')));
		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"ArrivalDate" => $this->input->post('arr-date-edit'),
			"ArrivalTime" => $arrival_time,
			"ArrivalCity" => $this->input->post('arr-city1-edit'),
			"ArrivalCity2" => $this->input->post('arr-city2-edit'),
			"Airline" => $this->input->post('arr-airline-edit'),
			"FlightNumber" => $this->input->post('arr-fltnum-edit'),
			"DepartureDate" => $this->input->post('dep-date-edit'),
			"DepartureTime" => $departure_time,
			"DepartureCity" => $this->input->post('dep-city1-edit'),
			"DepartureCity2" => $this->input->post('dep-city2-edit'),
			"DepartureAirline" => $this->input->post('dep-airline-edit'),
			"DepartureFlightNum" => $this->input->post('dep-fltnum-edit'),
			"cost" => $this->input->post('flight-cost-edit'),
			"mileage" => $this->input->post('flight-mileage-edit'),
			"flightcomp_id" => $this->input->post('flight-comp-edit'),
			"Notes" => $this->input->post('flight-notes-edit')
		);
		if ($this->input->post('flight-pickup-edit')) { $values["pickup"] = 1; } else { $values["pickup"] = 0; }
		if ($this->input->post('flight-translator-edit')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		$flight_id = $this->input->post('flight_id_update');
		$this->guest->update_value($values, "fg_flights", $flight_id);

		// Update row for this flight
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\"><button id=\"flight-".$flight_id."\" name=\"flight-".$flight_id."\" data-id=\"".$flight_id."\" class=\"edit\">Edit</button></td>\n";

		print "\t\t\t<td>";
		if ($values['ArrivalDate'] == "0000-00-00" || $values['ArrivalDate'] == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($values['ArrivalDate']))." "; }
		if ($values['ArrivalTime'] != "00:00:00") { print date("g:iA",strtotime($values['ArrivalTime']))."<br />"; } else { print "<br />"; }
		if ($values['ArrivalCity'] != "" && $values['ArrivalCity2'] != "") { print $values['ArrivalCity']."-".$values['ArrivalCity2']." / "; }
		elseif ($values['ArrivalCity'] == "" && $values['ArrivalCity2'] != "") { print $values['ArrivalCity2']." / "; }
		elseif ($values['ArrivalCity'] != "" && $values['ArrivalCity2'] == "") { print  $values['ArrivalCity']." / "; }
		if ($values['Airline'] != 0) { print $airlines_array[$values['Airline']]; }
		if ($values['FlightNumber'] != "") { print " #".$values['FlightNumber']; }
		print "</td>\n";

		print "\t\t\t<td>";
		if ($values['DepartureDate'] == "0000-00-00" || $values['DepartureDate'] == "0") { print "Not Specified "; } else { print date("F j, Y",strtotime($values['DepartureDate']))." "; }
		if ($values['DepartureTime'] != "00:00:00") { print date("g:iA",strtotime($values['DepartureTime']))."<br />"; } else { print "<br />"; }
		if ($values['DepartureCity'] != "" && $values['DepartureCity2'] != "") { print $values['DepartureCity']."-".$values['DepartureCity2']." / "; }
		elseif ($values['DepartureCity'] == "" && $values['DepartureCity2'] != "") { print $values['DepartureCity2']." / "; }
		elseif ($values['DepartureCity'] != "" && $values['DepartureCity2'] == "") { print  $values['DepartureCity']." / "; }
		if ($values['DepartureAirline'] != 0) { print $airlines_array[$values['DepartureAirline']]; }
		if ($values['DepartureFlightNum'] != "") { print " #".$values['DepartureFlightNum']; }
		print "</td>\n";

		print "\t\t\t<td>$".$values['cost']."</td>\n";
		print "\t\t\t<td>".$values['mileage']."</td>\n";
		print "\t\t\t<td>".$flight_comp_array[$values['flightcomp_id']]."</td>\n";
		if ($values['pickup'] != "0") {
			print "\t<td><a id=\"pickupf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"pickupf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"pickupf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"pickupf\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] != "0") {
			print "\t<td><a id=\"translatorf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"translatorf\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorf-".$flight_id."\" href=\"#\" data-id=\"".$flight_id."\" data-type=\"translatorf\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		if ($values['Notes'] != "") {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\"><label>Notes:</label> ".$values['Notes']."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"7\" class=\"lighter-text\">&nbsp;</td>\n";
		}
		print "\t\t</tr>\n";
	}

	function delete_flight() {
		$this->load->model('Guestmodel','guest');
		$this->guest->delete_value($this->input->post('flight_id_update'), "fg_flights");
	}

	function return_flight_json($guest_id) {
		$this->load->model('Flightmodel','flight');
		print json_encode( $this->flight->get_all_guest_flights($guest_id) );
	}

	function add_hotel() {
		$this->load->model('Guestmodel','guest');

		$check_out_time = date("H:i:s",strtotime($this->input->post('checkouttime-add')));		
		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"hotel" => $this->input->post('hotel-add'),
			"checkin" => $this->input->post('checkin-add'),
			"checkout" => $this->input->post('checkout-add'),
			"checkouttime" => $check_out_time,
			"compnights" => $this->input->post('comp-nights-add'),
			"paidnights" => $this->input->post('paid-nights-add'),
			"hotelcomp_id" => $this->input->post('hotel-comp-add'),
			"share" => $this->input->post('hotel-share-add'),
			"notes" => $this->input->post('hotel-notes-add')
		);
		if ($this->input->post('hotel-pickup-add')) { $values["pickup"] = 1; } else { $values["pickup"] = 0; }
		if ($this->input->post('hotel-translator-add')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		$hotel_id = $this->guest->add_value($values,"fg_accomodations");

		// Print new row for this hotel
		print "\t<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"HotelsTab\">\n";
		print "\t\t<tbody style=\"border-top: medium none;\">\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\" width=\"49\"><button id=\"hotel-".$hotel_id."\" name=\"hotel-".$hotel_id."\" data-id=\"".$hotel_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td width=\"88\">".$values['hotel']."</td>\n";
		print "\t\t\t<td width=\"128\">".date("F j, Y",strtotime($values['checkin']))."</td>\n";
		print "\t\t\t<td width=\"128\">".date("F j, Y",strtotime($values['checkout']))." ".date("g:iA",strtotime($values['checkouttime']))."</td>\n";
		print "\t\t\t<td width=\"48\">".$values['compnights']."</td>\n";
		print "\t\t\t<td width=\"48\">".$values['paidnights']."</td>\n";
		$totalnights = strtotime($values['checkout']) - strtotime($values['checkin']);
		print "\t\t\t<td width=\"48\">".($totalnights / 86400)."</td>\n";

		if ($values['share'] == 0) {
			print "\t\t\t<td width=\"108\">None</td>\n";
		} else {
			print "\t\t\t<td width=\"108\">".$values['share']."</td>\n";
		}
		if ($values['pickup'] != "0") {
			print "\t<td width=\"108\"><a id=\"pickuph-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"pickuph\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"108\"><a id=\"pickuph-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"pickuph\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] != "0") {
			print "\t<td width=\"88\"><a id=\"translatorh-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"translatorh\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"88\"><a id=\"translatorh-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"translatorh\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";		
		print "\t\t<tr valign=\"top\">\n";
		if ($values['notes'] != "") {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><label>Notes:</label> ".$values['notes']."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\">&nbsp;</td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t</tbody>\n";
		print "\t</table>\n";
		print "\t<div id=\"addHotelHere\"><input type=\"hidden\" id=\"hotel-id-new\" name=\"hotel-id-new\" value=\"".$hotel_id."\" /></div>";
	}

	function update_hotel() {
		$this->load->model('Guestmodel','guest');

		$check_out_time = date("H:i:s",strtotime($this->input->post('checkouttime-edit')));
		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"hotel" => $this->input->post('hotel-edit'),
			"checkin" => $this->input->post('checkin-edit'),
			"checkout" => $this->input->post('checkout-edit'),
			"checkouttime" => $check_out_time,
			"compnights" => $this->input->post('comp-nights-edit'),
			"paidnights" => $this->input->post('paid-nights-edit'),
			"hotelcomp_id" => $this->input->post('hotel-comp-edit'),
			"share" => $this->input->post('hotel-share-edit'),
			"notes" => $this->input->post('hotel-notes-edit')
		);
		if ($this->input->post('hotel-pickup-edit')) { $values["pickup"] = 1; } else { $values["pickup"] = 0; }
		if ($this->input->post('hotel-translator-edit')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		$hotel_id = $this->input->post('hotel_id_update');
		$this->guest->update_value($values, "fg_accomodations", $hotel_id);

		// Update row for this hotel
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td rowspan=\"2\"><button id=\"hotel-".$hotel_id."\" name=\"hotel-".$hotel_id."\" data-id=\"".$hotel_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td>".$values['hotel']."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($values['checkin']))."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($values['checkout']))." ".date("g:iA",strtotime($values['checkouttime']))."</td>\n";
		print "\t\t\t<td>".$values['compnights']."</td>\n";
		print "\t\t\t<td>".$values['paidnights']."</td>\n";
		$totalnights = strtotime($values['checkout']) - strtotime($values['checkin']);
		print "\t\t\t<td>".($totalnights / 86400)."</td>\n";

		if ($values['share'] == 0) {
			print "\t\t\t<td>None</td>\n";
		} else {
			print "\t\t\t<td>".$values['share']."</td>\n";
		}
		if ($values['pickup'] != "0") {
			print "\t<td><a id=\"pickuph-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"pickuph\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"pickuph-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"pickuph\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] != "0") {
			print "\t<td><a id=\"translatorh-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"translatorh\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorh-".$hotel_id."\" href=\"#\" data-id=\"".$hotel_id."\" data-type=\"translatorh\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t<tr valign=\"top\">\n";
		if ($values['notes'] != "") {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><label>Notes:</label> ".$values['notes']."</td>\n";
		} else {
			print "\t\t\t<td colspan=\"9\" class=\"lighter-text\">&nbsp;</td>\n";
		}
		print "\t\t</tr>\n";
	}

	function delete_hotel() {
		$this->load->model('Guestmodel','guest');
		$this->guest->delete_value($this->input->post('hotel_id_update'), "fg_accomodations");
	}

	function return_hotel_json($guest_id) {
		$this->load->model('Hotelmodel','hotel');
		print json_encode( $this->hotel->get_all_guest_hotels($guest_id) );
	}

	function add_screening_appearance() {
		$this->load->helper('form');
		$this->load->model('Guestmodel','guest');
		
		$guest_id = $this->input->post('guest_id');
		$screening_id = $this->input->post('screening_id');
		$scr_runtime = $this->input->post('runtime');
		$scr_date = $this->input->post('date');
		$scr_time = $this->input->post('time');
		$scr_loc = $this->input->post('location');

		$values = array(
			"festivalguest_id" => $guest_id,
			"screening_id" => $screening_id,
			"translator" => 0,
			"transport" => 0
		);
		$screenapp_id = $this->guest->add_value($values, "fg_screeningappearances");

		// Print a new row
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td>";
		print "<button id=\"attend-".$screening_id."\" name=\"attend-".$screening_id."\" data-formname=\"#screening_app_".$screening_id."\" class=\"remove ui-state-error\">Remove</button>";
		print "</td>\n";
		print "\t\t\t<td>".$scr_runtime."</td>\n";
		print "\t\t\t<td>".$scr_date."</td>\n";
		print "\t\t\t<td>".$scr_time."</td>\n";
		print "\t\t\t<td>".$scr_loc."</td>\n";
		print "\t\t\t<td><a id=\"transportsc-".$screenapp_id."\" href=\"#\" data-id=\"".$screenapp_id."\" data-type=\"transportsc\" class=\"icon_cross\"></a></td>\n";
		print "\t\t\t<td><a id=\"translatorsc-".$screenapp_id."\" href=\"#\" data-id=\"".$screenapp_id."\" data-type=\"translatorsc\" class=\"icon_cross\"></a></td>\n";
		print "\t\t</tr>\n";
	}

	function delete_screening_appearance() {
		$this->load->helper('form');
		$this->load->model('Guestmodel','guest');
		$this->guest->delete_value($this->input->post('attend_id'), "fg_screeningappearances");

		$guest_id = $this->input->post('guest_id');
		$screening_id = $this->input->post('screening_id');
		$scr_runtime = $this->input->post('runtime');
		$scr_date = $this->input->post('date');
		$scr_time = $this->input->post('time');
		$scr_loc = $this->input->post('location');

		// Print a new row
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td>";
		print "<button id=\"attend-".$screening_id."\" name=\"attend-".$screening_id."\" data-id=\"".$screening_id."\" class=\"add\">Add</button>";
		print "</td>\n";
		print "\t\t\t<td>".$scr_runtime."</td>\n";
		print "\t\t\t<td>".$scr_date."</td>\n";
		print "\t\t\t<td>".$scr_time."</td>\n";
		print "\t\t\t<td>".$scr_loc."</td>\n";
		print "\t\t\t<td>&nbsp;</td>\n";
		print "\t\t\t<td>&nbsp;</td>\n";
		print "\t\t</tr>\n";
	}

	function add_event_appearance() {
		$this->load->model('Guestmodel','guest');
		$event_time = date("H:i:s",strtotime($this->input->post('event-time-add')));

		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"location" => $this->input->post('event-location-add'),
			"date" => $this->input->post('event-date-add'),
			"time" => $event_time,
		);
		if ($this->input->post('event-translator-add')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		if ($this->input->post('event-transport-add')) { $values["transport"] = 1; } else { $values["transport"] = 0; }
		$appearance_id = $this->guest->add_value($values,"fg_specialappearances");

		// Print new row for this event appearance
		print "\t<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"SpecialAppTab\">\n";
		print "\t\t<tbody style=\"border-top: medium none;\">\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td width=\"84\"><button id=\"appear-".$appearance_id."\" name=\"appear-".$appearance_id."\" data-id=\"".$appearance_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td width=\"201\">".$values['location']."</td>\n";
		print "\t\t\t<td width=\"152\">".date("F j, Y",strtotime($values['date']))."</td>\n";
		print "\t\t\t<td width=\"133\">".date("g:iA",strtotime($values['time']))."</td>\n";
		if ($values['transport'] == "1") {
			print "\t<td width=\"162\"><a id=\"transportsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"transportsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"162\"><a id=\"transportsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"transportsp\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] == "1") {
			print "\t<td width=\"162\"><a id=\"translatorsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"translatorsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"162\"><a id=\"translatorsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"translatorsp\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
		print "\t\t</tbody>\n";
		print "\t</table>\n";
		print "\t<div id=\"addEventAppHere\"><input type=\"hidden\" id=\"appearance-id-new\" name=\"appearance-id-new\" value=\"".$appearance_id."\" /></div>";
	}

	function update_event_appearance() {
		$this->load->model('Guestmodel','guest');
		$event_time = date("H:i:s",strtotime($this->input->post('event-time-edit')));

		$values = array(
			"festivalguest_id" => $this->input->post('guest_id'),
			"location" => $this->input->post('event-location-edit'),
			"date" => $this->input->post('event-date-edit'),
			"time" => $event_time,
		);
		if ($this->input->post('event-translator-edit')) { $values["translator"] = 1; } else { $values["translator"] = 0; }
		if ($this->input->post('event-transport-edit')) { $values["transport"] = 1; } else { $values["transport"] = 0; }
		$appearance_id = $this->input->post('eventapp_id_update');
		$this->guest->update_value($values, "fg_specialappearances", $appearance_id);

		// Update row for this event appearance
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td><button id=\"appear-".$appearance_id."\" name=\"appear-".$appearance_id."\" data-id=\"".$appearance_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td>".$values['location']."</td>\n";
		print "\t\t\t<td>".date("F j, Y",strtotime($values['date']))."</td>\n";
		print "\t\t\t<td>".date("g:iA",strtotime($values['time']))."</td>\n";
		if ($values['transport'] != "0") {
			print "\t<td><a id=\"transportsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"transportsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"transportsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"transportsp\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values['translator'] != "0") {
			print "\t<td><a id=\"translatorsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"translatorsp\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td><a id=\"translatorsp-".$appearance_id."\" href=\"#\" data-id=\"".$appearance_id."\" data-type=\"translatorsp\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";
	}

	function delete_event_appearance() {
		$this->load->model('Guestmodel','guest');
		$this->guest->delete_value($this->input->post('eventapp_id_update'), "fg_specialappearances");
	}

	function return_eventapp_json($guest_id) {
		$this->load->model('Guestmodel','guest');
		print json_encode( $this->guest->get_guest_special_appearances($guest_id) );
	}

	function delete() {
		// This function will delete the guest from fg_festivalguests, any associated hotels, flights,
		// film affiliations, screening appearances, special appearances. Quite a lot to check for and delete.
		$this->load->model('Guestmodel','guest');
		
		$values = array(
			"festivalguest_id" => $this->input->post('festivalguest_id')
		);
				
		$deleted = $this->guest->delete_guest($values);
		if ($deleted == true) {
			print "This guest has been successfully deleted. You will be redirected to the guest listing page in 5 seconds.";
		}
	}
}

/* End of file guest_edit.php */
/* Location: ./system/application/controllers/admin/guest_edit.php */
?>