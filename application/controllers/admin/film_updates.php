<?php
class Film_Updates extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$vars['title'] = "Program Updates";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_updates";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_updates', $data);
		$this->load->view('admin/footer', $vars);
	}
	
	function save() {
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');		
		$this->load->model('Filmsmodel','films');

		$this->festival->update_value(array("program_updates" => $this->input->post('program_updates'), "pdf_flyer_html" => $this->input->post('pdf_flyer_html'), "updated_date" => date("Y-m-d H:i:s")), "wf_festival", $this->input->post('festival_to_update'));

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$data['updated'] = "Yes";

		$vars['title'] = "Program Updates";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_updates";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_updates', $data);
		$this->load->view('admin/footer', $vars);
		
	}

}

/* End of file film_updates.php */
/* Location: ./system/application/controllers/admin/film_updates.php */
?>