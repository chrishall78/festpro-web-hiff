<?php
class Guest_Reports extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttypes');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$data['guestlist'] = array(); foreach ($data['guestids'] as $guests) { $data['guestlist'][] = $guests->guest_id; }

		$data['air_summary'] = $this->flight->get_flight_comps($current_fest);
		$data['air_deals'] = $this->flight->get_flight_deals($current_fest);
		$data['hotel_summary'] = $this->hotel->get_hotel_comps($current_fest);

		if (count($data['guestlist']) > 0) {
			$data['air_used'] = $this->flight->get_used_flight_comps($data['guestlist']);
			$data['hotel_used'] = $this->hotel->get_used_hotel_comps($data['guestlist']);
			$data['all_flights'] = $this->flight->get_all_flights($data['guestlist'], "asc");
			$data['all_hotels'] = $this->hotel->get_all_hotels($data['guestlist'], "asc");
			$data['guest_films'] = $this->guest->get_all_guest_film_affiliations($data['guestlist']);
		} else {
			$data['air_used'] = array();
			$data['hotel_used'] = array();
			$data['all_flights'] = array();
			$data['all_hotels'] = array();
			$data['guest_films'] = array();
		}

		$data['screenings'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['guest_programs'] = array();
		foreach ($data['screenings'] as $thisScreening) {
			$match = FALSE;
			if ($thisScreening->program_name != "") {
				$program_id_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($data['guest_films'] as $thisGuestFilm) {
					foreach ($program_id_array as $thisProgramId) {
						if ($thisGuestFilm->movie_id == $thisProgramId) {
							$match = TRUE;
							break(2);
						}
					}
				}
				if ($match == TRUE) { $data['guest_programs'][] = $thisScreening; }
			}
		}
		usort($data['guest_programs'],"programnamecmp");

		$vars['title'] = "Guest Reports";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_reports";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_reports', $data);
		$this->load->view('admin/footer', $vars);
	}

	function preview_guest_allinfo_export($download)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Guesttypesmodel','guesttypes');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$airlines = $this->guesttypes->get_all_type("airlines", "asc", "no");
		$report = $this->reports->get_guest_allinfo_export($current_fest);

		// Process records before export
		foreach ($report as $thisRecord) {
			// Check to see if guest is affiliated with a film
			if ($thisRecord->movie_id != "") {
				$filmTitle = $this->films->get_film_title($thisRecord->movie_id);
				if (count($filmTitle) > 0) {
					$thisRecord->Film_Title = switch_title($filmTitle[0]->title_en);
				} else {
					$thisRecord->Film_Title = "";
				}

				$match = FALSE;
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRecord->movie_id);
				foreach ($screenings as $thisScreening) {
					$film_array = explode(",", $thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilm) {
						if ($thisFilm == $thisRecord->movie_id) { $match = TRUE; }
					}
					if ($match == TRUE) {
						$thisRecord->Program_Title = $thisScreening->program_name;
					} else {
						$thisRecord->Program_Title = "";
					}
				}
				if (count($screenings) == 0) {
					$thisRecord->Program_Title = "";
				}
			} else {
				$thisRecord->Program_Title = "";
				$thisRecord->Film_Title = "";
			}

			// Flight related columns
			if ($thisRecord->Arrival_Date == "0000-00-00" || $thisRecord->Arrival_Date == "1969-12-31") { $thisRecord->Arrival_Date = ""; }
			if ($thisRecord->Arrival_Time == "00:00:00") { $thisRecord->Arrival_Time = ""; }
			if ($thisRecord->Arrival_Airline == "0" || $thisRecord->Arrival_Airline == "--") { $thisRecord->Arrival_Airline = ""; }
			if ($thisRecord->Departure_Date == "0000-00-00" || $thisRecord->Departure_Date == "1969-12-31") { $thisRecord->Departure_Date = ""; }
			if ($thisRecord->Departure_Time == "00:00:00") { $thisRecord->Departure_Time = ""; }
			if ($thisRecord->Departure_Airline == "0" || $thisRecord->Departure_Airline == "--") { $thisRecord->Departure_Airline = ""; }
			if ($thisRecord->Flight_Pickup == "0") { $thisRecord->Flight_Pickup = ""; } else { $thisRecord->Flight_Pickup = "Yes"; }
			if ($thisRecord->Flight_Translator == "0") { $thisRecord->Flight_Translator = ""; } else { $thisRecord->Flight_Translator = "Yes"; }

			foreach ($airlines as $thisAirline) {
				if ($thisAirline->id == $thisRecord->Arrival_Airline) { $thisRecord->Arrival_Airline = $thisAirline->name; }
				if ($thisAirline->id == $thisRecord->Departure_Airline) { $thisRecord->Departure_Airline = $thisAirline->name; }
			}

			// Hotel related columns
			if ($thisRecord->Check_In_Date == "0000-00-00" || $thisRecord->Check_In_Date == "1969-12-31") { $thisRecord->Check_In_Date = ""; }
			if ($thisRecord->Check_Out_Date == "0000-00-00" || $thisRecord->Check_Out_Date == "1969-12-31") { $thisRecord->Check_Out_Date = ""; }
			if ($thisRecord->Check_Out_Time == "00:00:00") { $thisRecord->Check_Out_Time = ""; }
			if ($thisRecord->Hotel_Pickup == "0") { $thisRecord->Hotel_Pickup = ""; } else { $thisRecord->Hotel_Pickup = "Yes"; }
			if ($thisRecord->Hotel_Translator == "0") { $thisRecord->Hotel_Translator = ""; } else { $thisRecord->Hotel_Translator = "Yes"; }

			// Remove these columns
			unset($thisRecord->movie_id);
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"guest-allinfo-export-".$currentdate.".csv");
		} else {
			print "<h3>Guests Export</h3>";
			print "<form id=\"guest-allinfo-export\" method=\"post\" action=\"/admin/guest_reports/preview_guest_allinfo_export/download/\">";	
			print "<b>Guest Info, Flights, Hotels Export (".count($report)." records)</b> <button id=\"guest-allinfo-export-btn\">Export</button><br />\n";
			print "</form>";

			print_report_preview($report, "guest-allinfo-export-btn", "guest-allinfo-export");
		}
	}

	function preview_guest_export($download)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_guest_export($current_fest);

		// Process records before export
		foreach ($report as $thisRecord) {
			// Check to see if guest is affiliated with a film
			if ($thisRecord->movie_id != "") {
				$filmTitle = $this->films->get_film_title($thisRecord->movie_id);
				if (count($filmTitle) > 0) {
					$thisRecord->Film_Title = switch_title($filmTitle[0]->title_en);
				} else {
					$thisRecord->Film_Title = "";
				}

				$match = FALSE;
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRecord->movie_id);
				foreach ($screenings as $thisScreening) {
					$film_array = explode(",", $thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilm) {
						if ($thisFilm == $thisRecord->movie_id) { $match = TRUE; }
					}
					if ($match == TRUE) {
						$thisRecord->Program_Title = $thisScreening->program_name;
					} else {
						$thisRecord->Program_Title = "";
					}
				}
				if (count($screenings) == 0) {
					$thisRecord->Program_Title = "";
				}
			} else {
				$thisRecord->Program_Title = "";
				$thisRecord->Film_Title = "";
			}
			unset($thisRecord->movie_id);
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"guest-export-".$currentdate.".csv");
		} else {
			print "<h3>Guests Export</h3>";
			print "<form id=\"guest-export\" method=\"post\" action=\"/admin/guest_reports/preview_guest_export/download/\">";	
			print "<b>Guest Export (".count($report)." records)</b> <button id=\"guest-export-btn\">Export</button><br />\n";
			print "</form>";

			print_report_preview($report, "guest-export-btn", "guest-export");
		}
	}

	function preview_guest_program_export($download, $movie_ids)
	{
		if ($movie_ids == 0 || !$movie_ids) {
			print "<h3>Error Encountered</h3>\n";
			print "<p align=\"center\">Please select a program for the 'Guests By Program' report and try again.</p>\n";
		} else {
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Reportsmodel','reports');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Guesttypesmodel','guesttypes');

			$this->load->dbutil();
			$this->load->helper('download');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			$movie_id_array = explode("-",$movie_ids);
			$airlines = $this->guesttypes->get_all_type("airlines", "asc", "no");
			$report = $this->reports->get_guest_byprogram_export($current_fest, $movie_id_array);

			// Process records before export
			foreach ($report as $thisRecord) {
				$thisRecord->Film_Title = switch_title($thisRecord->Film_Title);
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRecord->movie_id);
				foreach ($screenings as $thisScreening) {
						$thisRecord->Program_Title = $thisScreening->program_name;
						break;
				}

				// Flight related columns
				if ($thisRecord->Arrival_Date == "0000-00-00" || $thisRecord->Arrival_Date == "1969-12-31") { $thisRecord->Arrival_Date = ""; }
				if ($thisRecord->Arrival_Time == "00:00:00") { $thisRecord->Arrival_Time = ""; }
				if ($thisRecord->Arrival_Airline == "0" || $thisRecord->Arrival_Airline == "--") { $thisRecord->Arrival_Airline = ""; }
				if ($thisRecord->Departure_Date == "0000-00-00" || $thisRecord->Departure_Date == "1969-12-31") { $thisRecord->Departure_Date = ""; }
				if ($thisRecord->Departure_Time == "00:00:00") { $thisRecord->Departure_Time = ""; }
				if ($thisRecord->Departure_Airline == "0" || $thisRecord->Departure_Airline == "--") { $thisRecord->Departure_Airline = ""; }
				if ($thisRecord->Flight_Pickup == "0") { $thisRecord->Flight_Pickup = ""; } else { $thisRecord->Flight_Pickup = "Yes"; }
				if ($thisRecord->Flight_Translator == "0") { $thisRecord->Flight_Translator = ""; } else { $thisRecord->Flight_Translator = "Yes"; }

				foreach ($airlines as $thisAirline) {
					if ($thisAirline->id == $thisRecord->Arrival_Airline) { $thisRecord->Arrival_Airline = $thisAirline->name; }
					if ($thisAirline->id == $thisRecord->Departure_Airline) { $thisRecord->Departure_Airline = $thisAirline->name; }
				}

				// Hotel related columns
				if ($thisRecord->Check_In_Date == "0000-00-00" || $thisRecord->Check_In_Date == "1969-12-31") { $thisRecord->Check_In_Date = ""; }
				if ($thisRecord->Check_Out_Date == "0000-00-00" || $thisRecord->Check_Out_Date == "1969-12-31") { $thisRecord->Check_Out_Date = ""; }
				if ($thisRecord->Check_Out_Time == "00:00:00") { $thisRecord->Check_Out_Time = ""; }
				if ($thisRecord->Hotel_Pickup == "0") { $thisRecord->Hotel_Pickup = ""; } else { $thisRecord->Hotel_Pickup = "Yes"; }
				if ($thisRecord->Hotel_Translator == "0") { $thisRecord->Hotel_Translator = ""; } else { $thisRecord->Hotel_Translator = "Yes"; }

				// Remove these columns
				unset($thisRecord->movie_id);
			}

			if ($download == "download") {
				date_default_timezone_set(FP_BASE_TIMEZONE);
				$currentdate = date("m-d-Y");
				$this->reports->csv_download($report,"guest-program-export-".$currentdate.".csv");
			} else {
				print "<h3>Guests Export</h3>";
				print "<form id=\"guest-program-export\" method=\"post\" action=\"/admin/guest_reports/preview_guest_program_export/download/".$movie_ids."/\">";	
				print "<b>Guests By Program Export (".count($report)." records)</b> <button id=\"guest-program-export-btn\">Export</button><br />\n";
				print "</form>";

				print_report_preview($report, "guest-program-export-btn", "guest-program-export");
			}
		}
	}

	function preview_guest_film_export($download, $film_id)
	{
		if ($film_id == 0 || !$film_id) {
			print "<h3>Error Encountered</h3>\n";
			print "<p align=\"center\">Please select a film for the 'Guests By Film' report and try again.</p>\n";
		} else {
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Reportsmodel','reports');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Guesttypesmodel','guesttypes');

			$this->load->dbutil();
			$this->load->helper('download');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			$airlines = $this->guesttypes->get_all_type("airlines", "asc", "no");
			$report = $this->reports->get_guest_byfilm_export($current_fest, $film_id);

			// Process records before export
			foreach ($report as $thisRecord) {
				$thisRecord->Film_Title = switch_title($thisRecord->Film_Title);
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $thisRecord->movie_id);
				$singleFilm = TRUE;
				foreach ($screenings as $thisScreening) {
					if ($thisScreening->movie_id == $thisScreening->program_movie_ids) {
						$singleFilm = TRUE;
						$thisRecord->Program_Title = $thisRecord->Film_Title;
					} else {
						$singleFilm = FALSE;
						$screening_id_array = explode($thisScreening->program_movie_ids, ",");
						if ($thisScreening->program_name != "") {
							$thisRecord->Program_Title = $thisScreening->program_name;
						} else {
							$thisRecord->Program_Title = $thisRecord->Film_Title." + ".(count($screening_id_array)-1)." films";
						}
					}
				}

				// Flight related columns
				if ($thisRecord->Arrival_Date == "0000-00-00" || $thisRecord->Arrival_Date == "1969-12-31") { $thisRecord->Arrival_Date = ""; }
				if ($thisRecord->Arrival_Time == "00:00:00") { $thisRecord->Arrival_Time = ""; }
				if ($thisRecord->Arrival_Airline == "0" || $thisRecord->Arrival_Airline == "--") { $thisRecord->Arrival_Airline = ""; }
				if ($thisRecord->Departure_Date == "0000-00-00" || $thisRecord->Departure_Date == "1969-12-31") { $thisRecord->Departure_Date = ""; }
				if ($thisRecord->Departure_Time == "00:00:00") { $thisRecord->Departure_Time = ""; }
				if ($thisRecord->Departure_Airline == "0" || $thisRecord->Departure_Airline == "--") { $thisRecord->Departure_Airline = ""; }
				if ($thisRecord->Flight_Pickup == "0") { $thisRecord->Flight_Pickup = ""; } else { $thisRecord->Flight_Pickup = "Yes"; }
				if ($thisRecord->Flight_Translator == "0") { $thisRecord->Flight_Translator = ""; } else { $thisRecord->Flight_Translator = "Yes"; }

				foreach ($airlines as $thisAirline) {
					if ($thisAirline->id == $thisRecord->Arrival_Airline) { $thisRecord->Arrival_Airline = $thisAirline->name; }
					if ($thisAirline->id == $thisRecord->Departure_Airline) { $thisRecord->Departure_Airline = $thisAirline->name; }
				}

				// Hotel related columns
				if ($thisRecord->Check_In_Date == "0000-00-00" || $thisRecord->Check_In_Date == "1969-12-31") { $thisRecord->Check_In_Date = ""; }
				if ($thisRecord->Check_Out_Date == "0000-00-00" || $thisRecord->Check_Out_Date == "1969-12-31") { $thisRecord->Check_Out_Date = ""; }
				if ($thisRecord->Check_Out_Time == "00:00:00") { $thisRecord->Check_Out_Time = ""; }
				if ($thisRecord->Hotel_Pickup == "0") { $thisRecord->Hotel_Pickup = ""; } else { $thisRecord->Hotel_Pickup = "Yes"; }
				if ($thisRecord->Hotel_Translator == "0") { $thisRecord->Hotel_Translator = ""; } else { $thisRecord->Hotel_Translator = "Yes"; }

				// Remove these columns
				unset($thisRecord->movie_id);
			}

			if ($download == "download") {
				date_default_timezone_set(FP_BASE_TIMEZONE);
				$currentdate = date("m-d-Y");
				$this->reports->csv_download($report,"guest-film-export-".$currentdate.".csv");
			} else {
				print "<h3>Guests Export</h3>";
				print "<form id=\"guest-film-export\" method=\"post\" action=\"/admin/guest_reports/preview_guest_film_export/download/".$film_id."/\">";	
				print "<b>Guests By Film Export (".count($report)." records)</b> <button id=\"guest-film-export-btn\">Export</button><br />\n";
				print "</form>";

				print_report_preview($report, "guest-film-export-btn", "guest-film-export");
			}
		}
	}

	function preview_flights_export($download)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');
		$this->load->model('Guesttypesmodel','guesttypes');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$airlines = $this->guesttypes->get_all_type("airlines", "asc", "no");
		$report = $this->reports->get_flights_export($current_fest);

		// Process records before export
		foreach ($report as $thisRecord) {
			if ($thisRecord->Arrival_Date == "0000-00-00" || $thisRecord->Arrival_Date == "1969-12-31") { $thisRecord->Arrival_Date = ""; }
			if ($thisRecord->Arrival_Time == "00:00:00") { $thisRecord->Arrival_Time = ""; }
			if ($thisRecord->Arrival_Airline == "0" || $thisRecord->Arrival_Airline == "--") { $thisRecord->Arrival_Airline = ""; }
			if ($thisRecord->Departure_Date == "0000-00-00" || $thisRecord->Departure_Date == "1969-12-31") { $thisRecord->Departure_Date = ""; }
			if ($thisRecord->Departure_Time == "00:00:00") { $thisRecord->Departure_Time = ""; }
			if ($thisRecord->Departure_Airline == "0" || $thisRecord->Departure_Airline == "--") { $thisRecord->Departure_Airline = ""; }
			if ($thisRecord->Needs_Pickup == "0") { $thisRecord->Needs_Pickup = ""; } else { $thisRecord->Needs_Pickup = "Yes"; }
			if ($thisRecord->Needs_Translator == "0") { $thisRecord->Needs_Translator = ""; } else { $thisRecord->Needs_Translator = "Yes"; }

			foreach ($airlines as $thisAirline) {
				if ($thisAirline->id == $thisRecord->Arrival_Airline) { $thisRecord->Arrival_Airline = $thisAirline->name; }
				if ($thisAirline->id == $thisRecord->Departure_Airline) { $thisRecord->Departure_Airline = $thisAirline->name; }
			}
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"flights-export-".$currentdate.".csv");
		} else {
			print "<h3>Flight Dates Export</h3>";
			print "<form id=\"flights-export\" method=\"post\" action=\"/admin/guest_reports/preview_flights_export/download/\">";	
			print "<b>Flight Dates Export (".count($report)." records)</b> <button id=\"flights-export-btn\">Export</button><br />\n";
			print "</form>";

			print_report_preview($report, "flights-export-btn", "flights-export");
		}
	}

	function preview_hotels_export($download)
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Reportsmodel','reports');

		$this->load->dbutil();
		$this->load->helper('download');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$report = $this->reports->get_hotels_export($current_fest);

		// Process records before export
		foreach ($report as $thisRecord) {
			if ($thisRecord->Check_In_Date == "0000-00-00" || $thisRecord->Check_In_Date == "1969-12-31") { $thisRecord->Check_In_Date = ""; }
			if ($thisRecord->Check_Out_Date == "0000-00-00" || $thisRecord->Check_Out_Date == "1969-12-31") { $thisRecord->Check_Out_Date = ""; }
			if ($thisRecord->Check_Out_Time == "00:00:00") { $thisRecord->Check_Out_Time = ""; }
			if ($thisRecord->Needs_Pickup == "0") { $thisRecord->Needs_Pickup = ""; } else { $thisRecord->Needs_Pickup = "Yes"; }
			if ($thisRecord->Needs_Translator == "0") { $thisRecord->Needs_Translator = ""; } else { $thisRecord->Needs_Translator = "Yes"; }
		}

		if ($download == "download") {
			date_default_timezone_set(FP_BASE_TIMEZONE);
			$currentdate = date("m-d-Y");
			$this->reports->csv_download($report,"hotels-export-".$currentdate.".csv");
		} else {
			print "<h3>Hotel Dates Export</h3>";
			print "<form id=\"hotels-export\" method=\"post\" action=\"/admin/guest_reports/preview_hotels_export/download/\">";	
			print "<b>Hotel Dates Export (".count($report)." records)</b> <button id=\"hotels-export-btn\">Export</button><br />\n";
			print "</form>";

			print_report_preview($report, "hotels-export-btn", "hotels-export");
		}
	}

}

/* End of file guest_reports.php */
/* Location: ./system/application/controllers/admin/guest_reports.php */
?>