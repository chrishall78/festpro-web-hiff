<?php
class Film_Review extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmreviewmodel','filmreview');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Usersmodel','users');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['festivals'] = $this->festival->get_all_festivals();
		$data['user_list'] = $this->users->get_all_users();
		$data['current_user'] = $this->users->get_username($this->session->userdata('username'));

		$data['film_items'] = $this->filmreview->get_film_review_items($current_fest,"wf_movie_staging.SubmittingDate", "desc");
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['genres'] = $this->filmtype->get_all_type("genre","asc");
		$data['sections'] = $this->section->get_all_festival_sections($current_fest);

		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['soundformat'] = $this->filmtype->get_all_type("sound","asc");
		$data['color'] = $this->filmtype->get_all_type("color","asc");
		$data['aspectratio'] = $this->filmtype->get_all_type("aspectratio","asc");
		$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
		$data['distribution'] = $this->filmtype->get_all_type("distribution","asc");
		$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
		$data['personnelroles'] = $this->filmtype->get_all_type("personnel","asc");
		
		$vars['title'] = "Review Film Data Entries";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_review";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $data['festivals'];

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_review', $data);
		$this->load->view('admin/footer', $vars);
	}

	function sort($filmorcfe, $sorttoken)
	{
		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmreviewmodel','filmreview');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Usersmodel','users');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['user_list'] = $this->users->get_all_users();
		$data['current_user'] = $this->users->get_username($this->session->userdata('username'));

		switch ($filmorcfe) {
			case "film":
				$tablename = "wf_movie_staging.";
				switch ($sorttoken) {
					case "namedesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."EnglishTitle", "desc"); break;
					case "nameasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."EnglishTitle", "asc");  break;
		
					case "datedesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."SubmittingDate", "desc"); break;
					case "dateasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."SubmittingDate", "asc");  break;
		
					case "contactdesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."SubmittingName", "desc"); break;
					case "contactasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."SubmittingName", "asc");  break;
		
					case "distrdesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."distributorName", "desc"); break;
					case "distrasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."distributorName", "asc");  break;

					case "countrydesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."Country", "desc"); break;
					case "countryasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."Country", "asc");  break;

					case "languagedesc": $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."Language", "desc"); break;
					case "languageasc":  $data['film_items'] = $this->filmreview->get_film_review_items($current_fest,$tablename."Language", "asc");  break;
				}
				break;
		}
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");

		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['soundformat'] = $this->filmtype->get_all_type("sound","asc");
		$data['color'] = $this->filmtype->get_all_type("color","asc");
		$data['aspectratio'] = $this->filmtype->get_all_type("aspectratio","asc");
		$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
		$data['distribution'] = $this->filmtype->get_all_type("distribution","asc");

		$vars['title'] = "Review Film Data Entries";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_review";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_review', $data);
		$this->load->view('admin/footer', $vars);
	}
	
	function import_film() {
		// This function imports the film values from this temporary spot into the Film Listing page.
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmreviewmodel','filmreview');
		$this->load->model('Usersmodel','users');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$toImport = $this->input->post('film-import-id');
		$thisFilm = $this->filmreview->get_film_review($toImport);
		
		// Process runtime value to remove any spaces or colons, to get just an integer (# of minutes).
		if ($thisFilm[0]->RunTime != "") {
			$temp = explode(" ",$thisFilm[0]->RunTime);
			$temp2 = explode(":",$temp[0]);
			$processRuntime = intval($temp2[0]);
		} else {
			$processRuntime = 0;
		}
		
		$generalNotes = "";
		
		// Keep these miscellaneous fields		
		if ($thisFilm[0]->PreviousScreenings != "") { $generalNotes .= "Previous Screenings: ".$thisFilm[0]->PreviousScreenings."\n"; }
		if ($thisFilm[0]->FootageLength != "") { $generalNotes .= "Film Length: ".$thisFilm[0]->FootageLength." ".$thisFilm[0]->FootageMeasure."\n"; }
		if ($thisFilm[0]->FootageType != "N/A") { $generalNotes .= "Film Type: ".$thisFilm[0]->FootageType."\n"; }
		if ($thisFilm[0]->InsuranceValue != "") { $generalNotes .= "Insurance Value: $".$thisFilm[0]->InsuranceValue."\n"; }

		$slug = create_slug(trim($this->input->post('title_en')));
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->films->check_existing_slug($slug);
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}

		// 1. add wf_movie table
		$movie_values = array(
			"slug" => $slug,
			"title_en" => trim($this->input->post('title_en')),
			"title" => trim($thisFilm[0]->OriginalLanguageTitle),
			"format_id" => $thisFilm[0]->ExhibitionFormat,
			"runtime" => $thisFilm[0]->RunTime,
			"runtime_int" => $processRuntime,
			"year" => $thisFilm[0]->Year,
			"synopsis" => $thisFilm[0]->Synopsis,
			"user_id" => $this->input->post('user-import-id'),
			"aspectratio_id" => $thisFilm[0]->AspectRatio,
			"color_id" => $thisFilm[0]->Color,
			"sound_id" => $thisFilm[0]->Sound,
			"distributor" => $thisFilm[0]->distributorName,
			"distributorContact" => $thisFilm[0]->distributorType,
			"distributorAddress" => $thisFilm[0]->distributorAddress,
			"distributorPhone" => $thisFilm[0]->distributorPhone,
			"distributorFax" => $thisFilm[0]->distributorFax,
			"distributorEmail" => $thisFilm[0]->distributorEmail,
			"PressContact" => $thisFilm[0]->PressContact,
			"PressEmail" => $thisFilm[0]->PressEmail,
			"PressFax" => $thisFilm[0]->PressFax,
			"PressPhone" => $thisFilm[0]->PressPhone,
			"PressWebsite" => $thisFilm[0]->PressWebsite,
			"HawaiiConnections" => $thisFilm[0]->HawaiiConnections,
			"updated_date" => date("Y-m-d H:i:s")
		);
		$wf_movie_id = $this->films->add_value($movie_values,"wf_movie");

		// 2. add wf_festivalmovie table
		$fmovie_values = array(
			"festival_id" => $thisFilm[0]->festival_id,
			"category_id" => $this->input->post('section-import'),
			"event_id" => $this->input->post('eventtype-import'),
			"premiere_id" => $thisFilm[0]->PremiereStatus,
			"movie_id" => $wf_movie_id,
			"printtraffic_id" => 0, // we don't know the printtraffic id yet
			"generalNotes" => $generalNotes,
			"PBPage" => 0,
			"UGPage" => 0
		);
		$wf_festivalmovie_id = $this->films->add_value($fmovie_values,"wf_festivalmovie");

		// 3. add wf_printtraffic table	
	  	$pt_values = array(
		  "festivalmovie_id" => $wf_festivalmovie_id,
		  "user_id" => $this->input->post('user-import-id'),
		  "ChangeDate" => date("Y-m-d"),
		  "InboundContact" => $thisFilm[0]->Shipping_In_Name,
		  "InboundPhone" => $thisFilm[0]->Shipping_In_Phone,
		  "InboundEmail" => $thisFilm[0]->Shipping_In_Email,
		  "InboundNotes" => "Fax: ".$thisFilm[0]->Shipping_In_Fax."\nAddress:\n".$thisFilm[0]->Shipping_In_Address,
		  "OutboundContact" => $thisFilm[0]->Shipping_Out_Name,
		  "OutboundShippingAddress" => $thisFilm[0]->Shipping_Out_Address,
		  "OutboundPhone" => $thisFilm[0]->Shipping_Out_Phone,
		  "OutboundEmail" => $thisFilm[0]->Shipping_Out_Email,
		  "OutboundNotes" => "Fax: ".$thisFilm[0]->Shipping_Out_Fax,
		);
	  	$wf_printtraffic_id = $this->films->add_value($pt_values,"wf_printtraffic");
				
		// 4. add wf_printtraffic_shipments table - outbound
		$pt_outbound_values = array(
			"festivalmovie_id" => $wf_festivalmovie_id,
			"OutScheduleBy" => "0000-00-00",
			"OutShipBy" => "0000-00-00",
			"OutShipped" => "0000-00-00",
			"OutArriveBy" => $thisFilm[0]->Shipping_Out_Date,
			"AccountNum" => "",
			"TrackingNum" => "",
			"ShippingFee" => 0,
			"notes" => "",
			"wePay" => 1,
			"arranged" => 0,
			"confirmed" => 0,
			"outbound" => 1,
			"courier_id" => 7 // this is "Other (See Notes)"
		);
		$wf_pt_outbound_id = $this->films->add_value($pt_outbound_values,"wf_printtraffic_shipments");

		// 5. add wf_printtraffic_shipments table - inbound
		$pt_inbound_values = array(
			"festivalmovie_id" => $wf_festivalmovie_id,
			"InShipped" => "0000-00-00",
			"InExpectedArrival" => $thisFilm[0]->Shipping_In_Date,
			"InReceived" => "0000-00-00",
			"AccountNum" => "",
			"TrackingNum" => "",
			"ShippingFee" => 0,
			"notes" => "",
			"wePay" => 0,
			"confirmed" => 0,
			"outbound" => 0,
			"courier_id" => 7 // this is "Other (See Notes)"
		);
		$wf_pt_inbound_id = $this->films->add_value($pt_inbound_values,"wf_printtraffic_shipments");

		// 6a. Add Movie Country (if any)
		if ($thisFilm[0]->Country != "") {
			$wf_country_id = array();
			foreach(explode(",",$thisFilm[0]->Country) as $thisCountry) {
				$wf_country_id[] = $this->films->add_value(array("movie_id"=>$wf_movie_id, "country_id"=>$thisCountry, "updated_date" => date("Y-m-d H:i:s")),"wf_movie_country");
			}
		}

		// 6b. Add Movie Language (if any)
		if ($thisFilm[0]->Language != "") {
			$wf_language_id = array();
			foreach(explode(",",$thisFilm[0]->Language) as $thisLanguage) {
				$wf_language_id[] = $this->films->add_value(array("movie_id"=>$wf_movie_id,"language_id"=>$thisLanguage, "updated_date" => date("Y-m-d H:i:s")),"wf_movie_language");
			}
		}
				
		// 6c. Add Movie Genre (if any)
		if ($thisFilm[0]->Genre != "") {
			$wf_genre_id = array();
			foreach(explode(",",$thisFilm[0]->Genre) as $thisGenre) {
				$wf_genre_id[] = $this->films->add_value(array("movie_id"=>$wf_movie_id,"genre_id"=>$thisGenre, "updated_date" => date("Y-m-d H:i:s")),"wf_movie_genre");
			}
		}
		
		// 7. Add new people to the wf_personnel table and the wf_movie_personnel table
		if ($thisFilm[0]->Personnel != "") {

		  $personnel = preg_split("/}/",$thisFilm[0]->Personnel);
		  foreach ($personnel as $thisPerson) {
			  if ($thisPerson != "") {
				  $thisPerson = ltrim($thisPerson,","); $thisPerson = ltrim($thisPerson,"{");
				  $fields = preg_split("/\|/",$thisPerson);
				  $firstname = $fields[0]; $lastname = $fields[1]; $roles = $fields[2]; $lnf = $fields[3];
				  if ($lnf == "" || $lnf == " ") { $lnf = 0; } else { $lnf = 1; }
			  
				  $pers_values = array(
					  "name" => $firstname,
					  "lastname" => $lastname,
					  "lastnamefirst" => $lnf
				  );
				  $new_pers_id = $this->films->add_value($pers_values,"wf_personnel");
				  
				  $movie_pers_values = array(
					  "type_id" => 1,
					  "movie_id" => $wf_movie_id,
					  "personnel_id" => $new_pers_id,
					  "personnel_roles" => $roles,
		  			  "updated_date" => date("Y-m-d H:i:s")
				  );
				  $new_movie_pers_id = $this->films->add_value($movie_pers_values,"wf_movie_personnel");
			  }
		  }
		}

		// 8. update wf_festivalmovie table with actual wf_printtraffic id number
		if ($wf_printtraffic_id != 0) {
			$fmovie_values2 = array(
				"printtraffic_id" => $wf_printtraffic_id
			);
			$this->films->update_value($fmovie_values2,"wf_festivalmovie",$wf_festivalmovie_id);
		}
		
		// Update the film review entry to mark it as imported.
		$values = array(
			"Imported" => 1,
			"ImportedBy" => $this->input->post('user-import-id'),
			"ImportedDate" => date("Y-m-d"),
			"ImportedFestival" => $this->input->post('festival-import-id')
		);
		$this->filmreview->update_value($values,"wf_movie_staging",$toImport);
		
		if ($this->input->post('rowtype-'.$toImport) == "oddrow" || $this->input->post('rowtype-'.$toImport) == "oddrow-imported") {
			print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n";
		} else {
			print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n";
		}
		print "\t\t\t\t<td rowspan=\"2\" align=\"center\">";
		print "<button id=\"film-review-".$toImport."\" value=\"".$thisFilm[0]->EnglishTitle."\" data-id=\"".$toImport."\" class=\"import\">Import</button><br />";
		print "<button id=\"film-view-".$toImport."\" data-id=\"".$toImport."\" class=\"view\">View</button>";
		print "</td>\n";
		print "\t\t\t\t<td><strong>".$thisFilm[0]->EnglishTitle."</strong></td>\n";
		print "\t\t\t\t<td>".date("m-d-Y",strtotime($thisFilm[0]->SubmittingDate))."</td>\n";
		print "\t\t\t\t<td colspan=\"5\" align=\"center\">Imported by <strong>".$this->session->userdata('username')."</strong> to <strong>".$data['festival'][0]->year." ".$data['festival'][0]->name."</strong> on <strong>".date("m-d-Y",strtotime($values['ImportedDate']))."</strong>.</td>\n";
		print "\t\t\t</tr>\n";
	}
	
	function delete_film() {
		// This function deletes the film data form submission, normally only used for duplicates.
		$this->load->model('Filmreviewmodel','filmreview');

		$toDelete = $this->input->post('film-import-id');
		$this->filmreview->delete_value($toDelete,"wf_movie_staging");		
	}
}

/* End of file film_review.php */
/* Location: ./system/application/controllers/admin/film_review.php */
?>