<?php
class Film_Edit extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		parse_str($_SERVER['QUERY_STRING'],$_GET);
		require_once("helper_functions.php");
	}

	function index()
	{
	}
	
	// Display page for film, and just an error page if no slug parameter is provided
	function update($slug="novalue", $saved_film_title="none")
	{
		if ($slug == "novalue") {
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
			$data['saved_film_title'] = "none";
			$saved_film_title == "added" ? $data['film_added'] = "yes" : $data['film_added'] = "no";
	
			$vars['title'] = "No Film Specified";
			$vars['path'] = "/";
			$vars['selected_page'] = "film_edit";
			$vars['filmJSON'] = json_encode($data['filmids']);
			$vars['festivals'] = $this->festival->get_all_festivals();
			
			$this->load->view('admin/header', $vars);
			$this->load->view('admin/nav_bar', $vars);
			$this->load->view('admin/film_edit');
			$this->load->view('admin/footer', $vars);
		} else {
			// load oEmbed with JSON (for videos only)
			$this->load->library('oembed');
			$this->load->helper('form');
			
			$this->load->model('Countrymodel','country');
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Filmtypesmodel','filmtype');
			$this->load->model('Genremodel','genre');
			$this->load->model('Languagemodel','language');
			$this->load->model('Personnelmodel','personnel');
			$this->load->model('Photovideomodel','photovideo');
			$this->load->model('Printtrafficmodel','printtraffic');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Sectionmodel','section');
			$this->load->model('Sponsormodel','sponsor');
			$this->load->model('Locationmodel','location');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			// Get film info, plus associated countries, languages and genres
			$data['film'] = $this->films->get_film_by_slug($slug);
			$data['film_countries'] = $this->country->get_movie_countries($data['film'][0]->movie_id);
			$data['film_languages'] = $this->language->get_movie_languages($data['film'][0]->movie_id);
			$data['film_genres'] = $this->genre->get_movie_genres($data['film'][0]->movie_id);
			$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
			$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
			$data['country_ids'] = ""; $data['language_ids'] = ""; $data['genre_ids'] = "";
			foreach ($data['film_countries'] as $thisCountry) { $data['country_ids'] .= $thisCountry->id.","; }
			foreach ($data['film_languages'] as $thisLanguage) { $data['language_ids'] .= $thisLanguage->id.","; }
			foreach ($data['film_genres'] as $thisGenre) { $data['genre_ids'] .= $thisGenre->id.","; }
			$data['country_ids'] = trim($data['country_ids'],",");
			$data['language_ids'] = trim($data['language_ids'],",");
			$data['genre_ids'] = trim($data['genre_ids'],",");
			
			// Get sponsor names/logos for this film
			$data['sponsors'] = $this->sponsor->get_all_sponsors();
			$data['sponsor_logos'] = $this->sponsor->get_all_sponsor_logos($data['film'][0]->movie_id, $current_fest);

			// Get existing Film Personnel data
			$data['film_personnel'] = $this->personnel->get_movie_personnel($data['film'][0]->movie_id, "");
			
			// Get existing photos and videos for this film
			$data['film_photos'] = $this->photovideo->get_all_movie_photos($data['film'][0]->movie_id);
			$data['film_videos'] = $this->photovideo->get_all_movie_videos($data['film'][0]->movie_id);

			// Get schedule for this film
			$data['film_screenings'] = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $data['film'][0]->movie_id);
			$data['screening_revenue'] = 0;
			foreach ($data['film_screenings'] as $thisScreening) {
				$data['screening_revenue'] = $data['screening_revenue'] + $thisScreening->ScreeningRevenue;
			}

			// Get print traffic details and shipments for this film
			$data['pt_details'] = $this->printtraffic->get_pt_details($data['film'][0]->id);
			$data['pt_shipments'] = $this->printtraffic->get_pt_shipments($data['film'][0]->id);
			$data['pt_courier'] = $this->filmtype->get_all_type("courier","asc");
			$data['shipping_fees'] = 0;
			$data['pt_shipment_ids'] = "";
			foreach ($data['pt_shipments'] as $thisShipment) {
				$data['shipping_fees'] = $data['shipping_fees'] + $thisShipment->ShippingFee;
				$data['pt_shipment_ids'] .= $thisShipment->id.",";
			}
			$data['pt_shipment_ids'] = trim($data['pt_shipment_ids'],",");

			// Getting Film Types for dropdowns
			$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
			$data['soundformat'] = $this->filmtype->get_all_type("sound","asc");
			$data['framerate'] = $this->filmtype->get_all_type("framerate","asc");
			$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
			$data['aspectratio'] = $this->filmtype->get_all_type("aspectratio","asc");
			$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
			$data['yearcompletion'] = generate_year_array();
			$data['sections'] = $this->section->get_all_festival_sections($current_fest);
			$data['countries'] = $this->filmtype->get_all_type("country","asc");
			$data['languages'] = $this->filmtype->get_all_type("language","asc");
			$data['genres'] = $this->filmtype->get_all_type("genre","asc");

			$data['distribution'] = $this->filmtype->get_all_type("distribution","asc");
			$data['personnel'] = $this->filmtype->get_all_type("personnel","asc");
			$data['locations'] = $this->location->get_internal_festival_locations(explode(",",$data['festival'][0]->locations));		

			$saved_film_title != "none" ? $data['saved_film_title'] = $data['film'][0]->title_en : $data['saved_film_title'] = "none";
			$saved_film_title == "added" ? $data['film_added'] = "yes" : $data['film_added'] = "no";

			$vars['title'] = "Now editing: ".$data['film'][0]->title_en;
			$vars['path'] = "/";
			$vars['selected_page'] = "film_edit";
			$vars['filmJSON'] = json_encode($data['filmids']);
			$vars['festivals'] = $this->festival->get_all_festivals();
			
			$this->load->view('admin/header', $vars);
			$this->load->view('admin/nav_bar', $vars);
			$this->load->view('admin/film_edit', $data);
			$this->load->view('admin/footer', $vars);
		}
	}
	
	// Save changes to the film info, print traffic, etc. After changes have been committed, reload the page and display a message.
	function save() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Printtrafficmodel','printtraffic');
		
		$festivalmovie_id = $this->input->post('festivalmovie_id');
		$movie_id = $this->input->post('movie_id');
		$printtraffic_id = $this->input->post('printtraffic_id');
		$printtraffic_shipment_ids = explode(",", $this->input->post('pt_shipment_ids'));
		
		// Update wf_movie values
		$values1 = array(
			"title" => trim($this->input->post('title')),
			"title_en" => trim($this->input->post('title_en')),
			"year" => $this->input->post('year'),
			"format_id" => $this->input->post('format'),
			"websiteurl" => $this->input->post('websiteurl'),
			"synopsis" => $this->input->post('synopsis'),
			"synopsis_short" => $this->input->post('synopsis_short'),
			"synopsis_original" => $this->input->post('synopsis_original'),
			"writerLong" => $this->input->post('writerLong'),
			"writerShort" => $this->input->post('writerShort'),
			"writerOrigLang" => $this->input->post('writerOrigLang'),
			"runtime_int" => $this->input->post('runtime_int'),
			"user_id" => $this->session->userdata('user_id'),
			"aspectratio_id" => $this->input->post('aspectratio'),
			"sound_id" => $this->input->post('sound'),
			"framerate_id" => $this->input->post('framerate'),
			"distribution_id" => $this->input->post('distribution'),
			"distributor" => $this->input->post('distributor'),
			"distributorContact" => $this->input->post('distributorContact'),
			"distributorAddress" => $this->input->post('distributorAddress'),
			"distributorPhone" => $this->input->post('distributorPhone'),
			"distributorFax" => $this->input->post('distributorFax'),
			"distributorEmail" => $this->input->post('distributorEmail'),
			"PressContact" => $this->input->post('PressContact'),
			"PressPhone" => $this->input->post('PressPhone'),
			"PressFax" => $this->input->post('PressFax'),
			"PressEmail" => $this->input->post('PressEmail'),
			"PressWebsite" => $this->input->post('PressWebsite'),
			"HawaiiConnections" => $this->input->post('HawaiiConnections'),
			"updated_date" => date("Y-m-d H:i:s")
		);
		$this->films->update_value($values1, "wf_movie", $movie_id);

		// Update wf_festivalmovie values
		$values2 = array(
			"category_id" => $this->input->post('sections'),
			"premiere_id" => $this->input->post('premiere'),
			"event_id" => $this->input->post('eventtype'),
			"generalNotes" => $this->input->post('generalNotes'),
			"RentalFee" => $this->input->post('RentalFee'),
			"ScreeningFee" => $this->input->post('ScreeningFee'),
			"TransferFee" => $this->input->post('TransferFee'),
			"GuestFee" => $this->input->post('GuestFee'),
			"PBPage" => $this->input->post('PBPage'),
			"UGPage" => $this->input->post('UGPage'),
			"myhiff_1" => $this->input->post('myhiff_1'),
			"myhiff_2" => $this->input->post('myhiff_2'),
			"myhiff_3" => $this->input->post('myhiff_3'),
			"myhiff_4" => $this->input->post('myhiff_4'),
			"votes_1" => $this->input->post('votes_1'),
			"votes_2" => $this->input->post('votes_2'),
			"votes_3" => $this->input->post('votes_3'),
			"votes_4" => $this->input->post('votes_4'),
			"votes_5" => $this->input->post('votes_5'),
			"dcp_encryption" => $this->input->post('dcp_encryption'),
			"colorbar_end_time" => $this->input->post('colorbar_end_time'),
			"titleid_end_time" => $this->input->post('titleid_end_time'),
			"in_time" => $this->input->post('in_time'),
			"end_credits_time" => $this->input->post('end_credits_time'),
			"out_time" => $this->input->post('out_time')
		);
		if ($this->input->post('key_received')) { $values2["key_received"] = 1; } else { $values2["key_received"] = 0; }
		if ($this->input->post('key_tested')) { $values2["key_tested"] = 1; } else { $values2["key_tested"] = 0; }		
		if ($this->input->post('Confirmed')) { $values2["Confirmed"] = 1; } else { $values2["Confirmed"] = 0; }
		if ($this->input->post('Published')) { $values2["Published"] = 1; } else { $values2["Published"] = 0; }		

		$this->films->update_value($values2, "wf_festivalmovie", $festivalmovie_id);

		// Update wf_movie_countries values
		$country_update = explode(",",$this->input->post('country_ids'));
		foreach ($country_update as $thisMovieCountry) {
			$values = array(
				"country_id" =>$this->input->post('countries-'.$thisMovieCountry),
				"updated_date" => date("Y-m-d H:i:s")
			);
			if ($values['country_id'] != 0) {
				$this->films->update_value($values, "wf_movie_country", $thisMovieCountry);
			} elseif ($values['country_id'] == 0) {
				$this->films->delete_value($thisMovieCountry,"wf_movie_country");
			}
		}

		// Update wf_movie_languages values
		$language_update = explode(",",$this->input->post('language_ids'));
		foreach ($language_update as $thisMovieLanguage) {
			$values = array(
				"language_id" =>$this->input->post('languages-'.$thisMovieLanguage),
				"updated_date" => date("Y-m-d H:i:s")
			);
			if ($values['language_id'] != 0) {
				$this->films->update_value($values, "wf_movie_language", $thisMovieLanguage);
			} elseif ($values['language_id'] == 0) {
				$this->films->delete_value($thisMovieLanguage,"wf_movie_language");
			}
		}

		// Update wf_movie_genres values
		$genre_update = explode(",",$this->input->post('genre_ids'));
		foreach ($genre_update as $thisMovieGenre) {
			$values = array(
				"genre_id" =>$this->input->post('genres-'.$thisMovieGenre),
				"updated_date" => date("Y-m-d H:i:s")
			);
			if ($values['genre_id'] != 0) {
				$this->films->update_value($values, "wf_movie_genre", $thisMovieGenre);
			} elseif ($values['genre_id'] == 0) {
				$this->films->delete_value($thisMovieGenre,"wf_movie_genre");
			}
		}

		// Update Print Traffic values
		$values3 = array(
			"InboundContact" => $this->input->post('InboundContact'),
			"InboundEmail" => $this->input->post('InboundEmail'),
			"InboundPhone" => $this->input->post('InboundPhone'),
			"InboundNotes" => $this->input->post('InboundNotes'),
			"OutboundContact" => $this->input->post('OutboundContact'),
			"OutboundEmail" => $this->input->post('OutboundEmail'),
			"OutboundPhone" => $this->input->post('OutboundPhone'),
			"OutboundNotes" => $this->input->post('OutboundNotes'),
			"OutboundShippingAddress" => $this->input->post('OutboundShippingAddress'),
			"user_id" => $this->session->userdata('user_id'),
			"ChangeDate" => date("Y-m-d H:i:s")
		);
		$this->films->update_value($values3, "wf_printtraffic", $printtraffic_id);

		header('Location: /admin/film_edit/update/'.$this->input->post('film_slug').'/saved/');
	}
	
	// This deletes the film from wf_movie, wf_festivalmovie, any associated countries, languages,
	// genres, personnel, any screenings, any print traffic entries. Everything is gone.
	function delete() {
		$this->load->model('Filmsmodel','films');
		
		$values = array(
			"festivalmovie_id" => $this->input->post('festivalmovie_id'),
			"movie_id" => $this->input->post('movie_id')
		);				
		$deleted = $this->films->delete_film($values);
		if ($deleted == true) {
			print "This film has been successfully deleted. You will be redirected to the film listing page in 5 seconds.";
		}
	}

/* -------------------
	Screening Functions
---------------------- */

	// Adds a new screening - includes information and one or more films
	function add_screening() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		
		$locations = $this->location->get_all_locations();
		$location_id1 = 0; $location_id2 = 0;
		foreach ($locations as $thisLocation) {
			if ($thisLocation->id == $this->input->post('scrn-location-new')) { $location_id1 = $thisLocation->name; }
			if ($thisLocation->id == $this->input->post('scrn-location2-new')) { $location_id2 = $thisLocation->name; }
		}
		
		$total_films = $this->input->post('total_films');
		$film_ids = array();
		for ($x=1; $x<=$total_films; $x++) {
			if ($x <= 9) { $z = "0";} else { $z = ""; }
			$film_ids[] = $this->input->post('scrn-film-'.$z.$x);
		}
		$film_ids_string = convert_multiselect_to_string($film_ids);
		
		$time_string = date("H:i:s",strtotime($this->input->post('scrn-time-new')));
		
		// Runtime in minutes converted to 00:00:00 time type
		$total_runtime = convert_min_to_time($this->input->post('total-runtime'));

		$values = array(
			"movie_id" => $this->input->post('scrn-film-01'),
			"program_movie_ids" => $film_ids_string,
			"program_name" => $this->input->post('scrn-program-name-new'),
			"program_desc" => $this->input->post('scrn-program-desc-new'),
			"date" => $this->input->post('scrn-date-new'),
			"time" => $time_string,
			"location_id" => $this->input->post('scrn-location-new'),
			"location_id2" => $this->input->post('scrn-location2-new'),
			"url" => $this->input->post('scrn-ticket-new'),
			"host" => $this->input->post('scrn-host-new'),
			"notes" => $this->input->post('scrn-notes-new'),
			"Published" => $this->input->post('scrn-published-new'),
			"Private" => $this->input->post('scrn-private-new'),
			"Rush" => $this->input->post('scrn-rush-new'),
			"Free" => $this->input->post('scrn-free-new'),
			"AudienceCount" => $this->input->post('audience-count-new'),
			"ScreeningRevenue" => $this->input->post('screening-revenue-new'),

			"intro_length" => $this->input->post('intro-length'),
			"intro" => $this->input->post('intro'),
			"fest_trailer_length" => $this->input->post('fest-trailer-length'),
			"fest_trailer" => $this->input->post('fest-trailer'),
			"sponsor_trailer1_name" => $this->input->post('sponsor-trailer1-name'),
			"sponsor_trailer1_length" => $this->input->post('sponsor-trailer1-length'),
			"sponsor_trailer1" => $this->input->post('sponsor-trailer1'),
			"sponsor_trailer2_name" => $this->input->post('sponsor-trailer2-name'),
			"sponsor_trailer2_length" => $this->input->post('sponsor-trailer2-length'),
			"sponsor_trailer2" => $this->input->post('sponsor-trailer2'),
			"q_and_a_length" => $this->input->post('q-and-a-length'),
			"q_and_a" => $this->input->post('q-and-a'),
			"total_runtime" => $total_runtime,
			"updated_date" => date("Y-m-d H:i:s")
		);
		if ($this->input->post('scrn-program-name-new') != "") { $values["program_slug"] = create_slug($this->input->post('scrn-program-name-new')); } else { $values["program_slug"] = ""; }
		if ($this->input->post('sponsor-trailer1-name') == "Sponsor Trailer 1") { $values["sponsor_trailer1_name"] = ""; }
		if ($this->input->post('sponsor-trailer2-name') == "Sponsor Trailer 2") { $values["sponsor_trailer2_name"] = ""; }
		if ($this->input->post('scrn-published-new')) { $values["Published"] = 1; } else { $values["Published"] = 0; }
		if ($this->input->post('scrn-private-new')) { $values["Private"] = 1; } else { $values["Private"] = 0; }
		if ($this->input->post('scrn-rush-new')) { $values["Rush"] = 1; } else { $values["Rush"] = 0; }
		if ($this->input->post('scrn-free-new')) { $values["Free"] = 1; } else { $values["Free"] = 0; }
		if ($this->input->post('intro')) { $values["intro"] = 1; } else { $values["intro"] = 0; }
		if ($this->input->post('fest-trailer')) { $values["fest_trailer"] = 1; } else { $values["fest_trailer"] = 0; }
		if ($this->input->post('sponsor-trailer1')) { $values["sponsor_trailer1"] = 1; } else { $values["sponsor_trailer1"] = 0; }
		if ($this->input->post('sponsor-trailer2')) { $values["sponsor_trailer2"] = 1; } else { $values["sponsor_trailer2"] = 0; }
		if ($this->input->post('q-and-a')) { $values["q_and_a"] = 1; } else { $values["q_and_a"] = 0; }
		$screening_id = $this->schedule->add_value($values,"wf_screening");
		

		$film_program_ids = explode(",",$film_ids_string);
		$film_breakdown = "";

		if ($values['program_name'] != "") {
			$film_breakdown .= "<strong>".$values['program_name']."</strong> - ";
		}
		if ($values['intro'] == 1) {
			$film_breakdown .= "Intro (".$values['intro_length']."), ";
		}
		if ($values['fest_trailer'] == 1) {
			$film_breakdown .= "Festival Trailer (".$values['fest_trailer_length']."), ";
		}
		if ($values['sponsor_trailer1'] == 1) {
			if ($values['sponsor_trailer1_name'] == "") {
				$film_breakdown .= "Sponsor Trailer 1";
			} else {
				$film_breakdown .= $values['sponsor_trailer1_name'];
			}
			$film_breakdown .= " (".$values['sponsor_trailer1_length']."), ";
		}
		if ($values['sponsor_trailer2'] == 1) {
			if ($values['sponsor_trailer2_name'] == "") {
				$film_breakdown .= "Sponsor Trailer 2";
			} else {
				$film_breakdown .= $values['sponsor_trailer2_name'];
			}
			$film_breakdown .= " (".$values['sponsor_trailer2_length']."), ";
		}
		foreach ($film_program_ids as $scheduledFilm) {
			foreach ($data['films'] as $thisFilm) {
				if ($scheduledFilm == "$thisFilm->movie_id") {
					$film_breakdown .= switch_title($thisFilm->title_en)." (".$thisFilm->runtime_int."), ";
					break;
				}
			}
		}		
		if ($values['q_and_a'] == 1) {
			$film_breakdown .= "Q&amp;A Session (".$values['q_and_a_length'].")";
		}
		$film_breakdown = trim($film_breakdown,", ");


		$runtime_string = calculate_runtime(intval($this->input->post('total-runtime')));		
		print "\t<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"ScreeningsTab\">\n";
		print "\t\t<tbody style=\"border-top: medium none;\">\n";
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td width=\"10%\" rowspan=\"2\"><button id=\"scrn-".$screening_id."\" name=\"scrn-".$screening_id."\" data-id=\"".$screening_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td width=\"15%\">".$runtime_string."</td>\n";
		print "\t\t\t<td width=\"13%\">".date("F j, Y",strtotime($values['date']))."</td>\n";
		print "\t\t\t<td width=\"11%\">".date("g:iA",strtotime($values['time']))."</td>\n";
		print "\t\t\t<td width=\"15%\">".$location_id1;
		if ($values['location_id2'] != 0) { print " / ".$location_id2; }
		print "</td>\n";
		if ($values['host'] != "") {
			print "\t\t\t<td width=\"12%\">".$values['host']."</td>\n";
		} else {
			print "\t\t\t<td width=\"12%\">None</td>\n";
		}

		if ($values["Private"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Rush"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Free"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Published"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";

		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><strong>Screening Breakdown:</strong> ".$film_breakdown."<br><strong>Screening Notes:</strong> ".$values['notes']."</td>\n";
		print "\t\t</tr>\n";
		print "\t\t</tbody>\n";
		print "\t</table>\n";
		print "\t<div id=\"addScreeningHere\"><input type=\"hidden\" id=\"screening-id-new\" name=\"screening-id-new\" value=\"".$screening_id."\" /></div>";			
	}

	// Updates existing screening - includes information and one or more films
	function update_screening() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		
		$locations = $this->location->get_all_locations();
		$location_id1 = 0; $location_id2 = 0;
		foreach ($locations as $thisLocation) {
			if ($thisLocation->id == $this->input->post('scrn-location-update')) { $location_id1 = $thisLocation->name; }
			if ($thisLocation->id == $this->input->post('scrn-location2-update')) { $location_id2 = $thisLocation->name; }
		}
		
		$total_films = $this->input->post('total_films_update');
		$film_ids = array();
		for ($x=1; $x<=$total_films; $x++) {
			if ($x <= 9) { $z = "0";} else { $z = ""; }
			$film_ids[] = $this->input->post('scrn-film-update-'.$z.$x);
		}
		$film_ids_string = convert_multiselect_to_string($film_ids);		
		$time_string = date("H:i:s",strtotime($this->input->post('scrn-time-update')));

		// Runtime in minutes converted to 00:00:00 time type
		$total_runtime = convert_min_to_time($this->input->post('total-runtime-update'));

		$values = array(
			"movie_id" => $this->input->post('scrn-film-update-01'),
			"program_movie_ids" => $film_ids_string,
			"program_name" => $this->input->post('scrn-program-name-update'),
			"program_desc" => $this->input->post('scrn-program-desc-update'),
			"date" => $this->input->post('scrn-date-update'),
			"time" => $time_string,
			"location_id" => $this->input->post('scrn-location-update'),
			"location_id2" => $this->input->post('scrn-location2-update'),
			"url" => $this->input->post('scrn-ticket-update'),
			"host" => $this->input->post('scrn-host-update'),
			"notes" => $this->input->post('scrn-notes-update'),
			"Published" => $this->input->post('scrn-published-update'),
			"Private" => $this->input->post('scrn-private-update'),
			"Rush" => $this->input->post('scrn-rush-update'),
			"Free" => $this->input->post('scrn-free-update'),
			"AudienceCount" => $this->input->post('audience-count-update'),
			"ScreeningRevenue" => $this->input->post('screening-revenue-update'),
			"QandACount" => $this->input->post('q-and-a-count-update'),

			"intro_length" => $this->input->post('intro-length-update'),
			"intro" => $this->input->post('intro-update'),
			"fest_trailer_length" => $this->input->post('fest-trailer-length-update'),
			"fest_trailer" => $this->input->post('fest-trailer-update'),
			"sponsor_trailer1_name" => $this->input->post('sponsor-trailer1-name-update'),
			"sponsor_trailer1_length" => $this->input->post('sponsor-trailer1-length-update'),
			"sponsor_trailer1" => $this->input->post('sponsor-trailer1-update'),
			"sponsor_trailer2_name" => $this->input->post('sponsor-trailer2-name-update'),
			"sponsor_trailer2_length" => $this->input->post('sponsor-trailer2-length-update'),
			"sponsor_trailer2" => $this->input->post('sponsor-trailer2-update'),
			"q_and_a_length" => $this->input->post('q-and-a-length-update'),
			"q_and_a" => $this->input->post('q-and-a-update'),
			"total_runtime" => $total_runtime,
			"updated_date" => date("Y-m-d H:i:s")
		);
		$screening_id = $this->input->post('screening_id_update');
		if ($this->input->post('scrn-program-name-update') != "") { $values["program_slug"] = create_slug($this->input->post('scrn-program-name-update')); } else { $values["program_slug"] = ""; }
		if ($this->input->post('sponsor-trailer1-name-update') == "Sponsor Trailer 1") { $values["sponsor_trailer1_name"] = ""; }
		if ($this->input->post('sponsor-trailer2-name-update') == "Sponsor Trailer 2") { $values["sponsor_trailer2_name"] = ""; }
		if ($this->input->post('scrn-published-update')) { $values["Published"] = 1; } else { $values["Published"] = 0; }
		if ($this->input->post('scrn-private-update')) { $values["Private"] = 1; } else { $values["Private"] = 0; }
		if ($this->input->post('scrn-rush-update')) { $values["Rush"] = 1; } else { $values["Rush"] = 0; }
		if ($this->input->post('scrn-free-update')) { $values["Free"] = 1; } else { $values["Free"] = 0; }
		if ($this->input->post('intro-update')) { $values["intro"] = 1; } else { $values["intro"] = 0; }
		if ($this->input->post('fest-trailer-update')) { $values["fest_trailer"] = 1; } else { $values["fest_trailer"] = 0; }
		if ($this->input->post('sponsor-trailer1-update')) { $values["sponsor_trailer1"] = 1; } else { $values["sponsor_trailer1"] = 0; }
		if ($this->input->post('sponsor-trailer2-update')) { $values["sponsor_trailer2"] = 1; } else { $values["sponsor_trailer2"] = 0; }
		if ($this->input->post('q-and-a-update')) { $values["q_and_a"] = 1; } else { $values["q_and_a"] = 0; }
		$this->schedule->update_value($values,"wf_screening",$screening_id);
		

		$film_program_ids = explode(",",$film_ids_string);
		$film_breakdown = "";

		if ($values['program_name'] != "") {
			$film_breakdown .= "<strong>".$values['program_name']."</strong> - ";
		}
		if ($values['intro'] == 1) {
			$film_breakdown .= "Intro (".$values['intro_length']."), ";
		}
		if ($values['fest_trailer'] == 1) {
			$film_breakdown .= "Festival Trailer (".$values['fest_trailer_length']."), ";
		}
		if ($values['sponsor_trailer1'] == 1) {
			if ($values['sponsor_trailer1_name'] == "") {
				$film_breakdown .= "Sponsor Trailer 1";
			} else {
				$film_breakdown .= $values['sponsor_trailer1_name'];
			}
			$film_breakdown .= " (".$values['sponsor_trailer1_length']."), ";
		}
		if ($values['sponsor_trailer2'] == 1) {
			if ($values['sponsor_trailer2_name'] == "") {
				$film_breakdown .= "Sponsor Trailer 2";
			} else {
				$film_breakdown .= $values['sponsor_trailer2_name'];
			}
			$film_breakdown .= " (".$values['sponsor_trailer2_length']."), ";
		}
		foreach ($film_program_ids as $scheduledFilm) {
			foreach ($data['films'] as $thisFilm) {
				if ($scheduledFilm == "$thisFilm->movie_id") {
					$film_breakdown .= switch_title($thisFilm->title_en)." (".$thisFilm->runtime_int."), ";
					break;
				}
			}
		}
		if ($values['q_and_a'] == 1) {
			$film_breakdown .= "Q&amp;A Session (".$values['q_and_a_length'].")";
		}
		$film_breakdown = trim($film_breakdown,", ");


		$runtime_string = calculate_runtime(intval($this->input->post('total-runtime-update')));		
		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td width=\"10%\" rowspan=\"2\"><button id=\"scrn-".$screening_id."\" name=\"scrn-".$screening_id."\" data-id=\"".$screening_id."\" class=\"edit\">Edit</button></td>\n";
		print "\t\t\t<td width=\"15%\">".$runtime_string."</td>\n";
		print "\t\t\t<td width=\"13%\">".date("F j, Y",strtotime($values['date']))."</td>\n";
		print "\t\t\t<td width=\"11%\">".date("g:iA",strtotime($values['time']))."</td>\n";
		print "\t\t\t<td width=\"15%\">".$location_id1;
		if ($values['location_id2'] != 0) { print " / ".$location_id2; }
		print "</td>\n";
		if ($values['host'] != "") {
			print "\t\t\t<td width=\"12%\">".$values['host']."</td>\n";
		} else {
			print "\t\t\t<td width=\"12%\">None</td>\n";
		}
		if ($values["Private"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Rush"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Free"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_cross\"></a></td>\n";
		}
		if ($values["Published"] == "1") {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_tick\"></a></td>\n";
		} else {
			print "\t<td width=\"7%\" class=\"cent\"><a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_cross\"></a></td>\n";
		}
		print "\t\t</tr>\n";

		print "\t\t<tr valign=\"top\">\n";
		print "\t\t\t<td colspan=\"9\" class=\"lighter-text\"><strong>Screening Breakdown:</strong> ".$film_breakdown."<br><strong>Screening Notes:</strong> ".$values['notes']."</td>\n";
		print "\t\t</tr>\n";
	}

	// Removes a screening
	function delete_screening() {
		$this->load->model('Schedulemodel','schedule');
		$screening_id = $this->input->post('screening_id_update');		
		$this->schedule->delete_value($screening_id, "wf_screening");
	}

	// Adds a dropdown to choose more films in the 'Add Screening' popup box
	function add_screening_film($new_total) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$film_screening_array = convert_film_screenings_to_array($data['films']);

		if ($new_total <= 9) {$z = "0";} else {$z = "";}
		print "\t\t\t\t\t\t<div class=\"newFilmEntry\">\n";
        print "\t\t\t\t\t\t\t<label>Film ".$new_total."</label> <div id=\"scrn-film-".$z.$new_total."-length\" style=\"float:right; margin-right:37px\">0 min</div>\n";
        print "\t\t\t\t\t\t\t<div>";
		print form_dropdown_film('scrn-film-'.$z.$new_total, $film_screening_array, 0, " id='scrn-film-".$z.$new_total."' class='select-film ui-widget-content ui-corner-all'");
		print "&nbsp;&nbsp;<a href=\"#\" id=\"scrn-film-del-".$z.$new_total."\"><img src=\"/assets/images/cancel.png\" alt=\"Remove Film\" title=\"Remove Film\" width=\"16\" height=\"16\" border=\"0\"></a>\n";
        print "\t\t\t\t\t\t\t</div>\n";
		print "\t\t\t\t\t\t</div>\n\n";
		print "\t\t\t\t\t\t<div id=\"addNewFilmsHere\"></div>\n";
	}

	// Adds a dropdown to choose more films in the 'Update Screening' popup box
	function add_screening_film_update($screening_id, $new_total) {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
	
		$film_screening_array = convert_film_screenings_to_array($data['films']);
		$film_runtime = 0;
		foreach ($film_screening_array as $thisFilm) {
			if ($thisFilm['movie_id'] == $screening_id) {
				$film_runtime = $thisFilm['runtime'];
			}
		}

		if ($new_total <= 9) {$z = "0";} else {$z = "";}
		print "\t\t\t\t\t\t<div class=\"newFilmEntry\">\n";
        print "\t\t\t\t\t\t\t<label>Film ".$new_total."</label> <div id=\"scrn-film-update-".$z.$new_total."-length\" style=\"float:right; margin-right:37px\">".$film_runtime." min</div>\n";
        print "\t\t\t\t\t\t\t<div>";
		print form_dropdown_film('scrn-film-update-'.$z.$new_total, $film_screening_array, $screening_id, " id='scrn-film-update-".$z.$new_total."' class='select-film2 ui-widget-content ui-corner-all'");
		print "&nbsp;&nbsp;<a href=\"#\" id=\"scrn-film-update-del-".$z.$new_total."\"><img src=\"/assets/images/cancel.png\" alt=\"Remove Film\" title=\"Remove Film\" width=\"16\" height=\"16\" border=\"0\"></a>\n";
        print "\t\t\t\t\t\t\t</div>\n";
		print "\t\t\t\t\t\t</div>\n\n";
		print "\t\t\t\t\t\t<div id=\"addNewFilmsHere-update\"></div>\n";
	}

	// Gets list of screenings for a film in JSON format
	function return_screening_json($start_date, $end_date, $movie_id) {
		$this->load->model('Schedulemodel','schedule');
		print json_encode($this->schedule->get_internal_movie_screenings($start_date, $end_date, $movie_id));
	}

/* -------------------
	Personnel Functions
---------------------- */

	// Adds one person to a film
	function add_personnel() {
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		
		$personnel = $this->filmtype->get_all_type("personnel","asc");
		$roles_array = $this->input->post('pers-role-new');
		$roles_string = "";
		foreach ($roles_array as $role) {
			foreach ($personnel as $type) {
				if ($role == $type->id) {
					$roles_string .= $type->name."<br />";
					break;
				}
			}
		}

		// Add new Personnel record
		$values1 = array(
			"name" => $this->input->post('pers-first-name-new'),
			"lastname" => $this->input->post('pers-last-name-new'),
			"lastnamefirst" => $this->input->post('pers-last-name-first-new')
		);
		if ($this->input->post('pers-last-name-first-new')) { $values1["lastnamefirst"] = 1; } else { $values1["lastnamefirst"] = 0; }
		$personnel_id = $this->personnel->add_value($values1,"wf_personnel");

		// Add new record associating personnel with film
		$values2 = array(
			"movie_id" => $this->input->post('pers-movie-id'),
			"personnel_id" => $personnel_id,
			"type_id" => $roles_array[0],
			"personnel_roles" => convert_multiselect_to_string($roles_array),
			"updated_date" => date("Y-m-d H:i:s")
		);		
		$movie_personnel_id = $this->personnel->add_value($values2,"wf_movie_personnel");

        print "\t\t<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
        print "\t\t\t<tbody style=\"border-top:none;\">\n";
		print "\t\t\t<tr valign=\"top\">\n";
        print "\t\t\t\t<td width=\"5%\"><button id=\"pers-".$movie_personnel_id."\"data-id=\"".$movie_personnel_id."\" class=\"edit\">Edit</button></th>\n";
        print "\t\t\t\t<td width=\"25%\">".$values1['name']."</td>\n";
        print "\t\t\t\t<td width=\"45%\">".$values1['lastname']."</th>\n";
        print "\t\t\t\t<td width=\"25%\">".$roles_string."</td>\n";
        print "\t\t\t</tr>\n";
        print "\t\t\t</tbody>\n";
        print "\t\t</table><br />\n";
		print "\t\t<div id=\"add_personnel_here\">";
		print "<input type=\"hidden\" id=\"new_personnel_id\" name=\"new_personnel_id\" value=\"".$movie_personnel_id."\" />";
		print "</div>\n";
	}

	// Updates one person's information
	function update_personnel() {
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		
		$personnel = $this->filmtype->get_all_type("personnel","asc");
		$roles_array = $this->input->post('pers-role');
		$roles_string = "";
		foreach ($roles_array as $role) {
			foreach ($personnel as $type) {
				if ($role == $type->id) {
					$roles_string .= $type->name."<br />";
					break;
				}
			}
		}

		$values1 = array(
			"name" => $this->input->post('pers-first-name'),
			"lastname" => $this->input->post('pers-last-name'),
			"lastnamefirst" => $this->input->post('pers-last-name-first')
		);
		if ($this->input->post('pers-last-name-first')) { $values1["lastnamefirst"] = 1; } else { $values1["lastnamefirst"] = 0; }
		$values2 = array(
			"movie_id" => $this->input->post('pers-movie-id'),
			"type_id" => $roles_array[0],
			"personnel_roles" => convert_multiselect_to_string($roles_array),
			"updated_date" => date("Y-m-d H:i:s")
		);
		
		// Update Personnel record
		$movie_personnel_id = $this->input->post('pers-id');
		$personnel_id = $this->personnel->update_value($values2,"wf_movie_personnel",$movie_personnel_id);
		$x = $this->personnel->update_value($values1,"wf_personnel",$personnel_id);

		print "<tr valign=\"top\">\n";
        print "\t<td><button id=\"pers-".$movie_personnel_id."\"data-id=\"".$movie_personnel_id."\" class=\"edit\">Edit</button></td>\n";
        print "\t<td>".$values1['name']."</td>\n";
        print "\t<td>".$values1['lastname']."</td>\n";
        print "\t<td>".$roles_string."</td>\n";
        print "</tr>\n";		
	}

	// Removes one person from the database
	function delete_personnel() {
		$this->load->model('Personnelmodel','personnel');

		$movie_pers_id = $this->input->post('pers-id');
		$pers_id = $this->personnel->del_movie_personnel($movie_pers_id);
		// improve logic here to delete wf_personnel record only if no wf_movie_personnel refers to it?
		// $this->personnel->del_personnel($pers_id);
	}

	// Gets list of personnel for a film in JSON format
	function return_personnel_json($movie_id) {
		$this->load->model('Personnelmodel','personnel');
		print json_encode($this->personnel->get_movie_personnel($movie_id, ""));
	}

/* -------------------
	Photo Functions
---------------------- */

	// Updates the order of a film's photos
	function save_photo_sort() {
		$this->load->model('Photovideomodel','photovideo');
		foreach ($this->input->post('photo') as $position => $item) {
			$values = array('order' => intval($position+1), "updated_date" => date("Y-m-d H:i:s"));
			$this->photovideo->update_value($values, "wf_movie_photos", $item);
		}
		print "Photo order successfully saved!";
	}

	// Uploads photo, resizes it initially, creates 3 thumbnails, updates database and displays image for crop
	function upload_photo() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Photovideomodel','photovideo');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$config['upload_path'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= '4096';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['remove_spaces']  = true;
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload("photo-upload-new")) {
			$error = array('error' => $this->upload->display_errors('', ''));
			print "<div id=\"photo-crop-data\">".$error['error']." Please press the 'Cancel' button and try again.</div>";
			
		} else 	{
			$data = array('upload_data' => $this->upload->data());
			$image_width = $data['upload_data']['image_width'];
			$image_height = $data['upload_data']['image_height'];
			
			if ($image_width == $image_height) {
				$ratiow = 1; $ratioh = 1;
			} else if ($image_width > $image_height) {
				$ratioh = $image_height / $image_width;
				$ratiow = $image_width / $image_height;
			} else if ($image_width < $image_height) {
				$ratiow = $image_width / $image_height;
				$ratioh = $image_height / $image_width;
			}

			$movie_id = $this->input->post('movie_id_photo');
			$film_photos = $this->photovideo->get_all_movie_photos($movie_id);
			if (count($film_photos) == 0) {
				$order = 1;
			} else {
				$order = 0;
				foreach ($film_photos as $thisPhoto) {
					if ($order <= $thisPhoto->order) { $order = $thisPhoto->order; }
				}
				$order++;
			}

			$this->load->library('image_lib'); 

			// Options for Image Resize - no bigger than 1050x1050
			$config2['image_library'] = 'gd2';
			$config2['source_image'] = $data['upload_data']['full_path'];
			$config2['create_thumb'] = FALSE;
			$config2['maintain_ratio'] = TRUE;
			$config2['width'] = 1050;
			$config2['height'] = 1050;
			$this->image_lib->initialize($config2);
			if ( ! $this->image_lib->resize()) { echo $this->image_lib->display_errors(); }
			$original_path = "/".$config['upload_path']."/".$data['upload_data']['raw_name'].$data['upload_data']['file_ext'];


			// Options for Thumb Resize #1 - 525x300
			$lg_width = 525;  $lg_height = 300;
			$config3['image_library'] = 'gd2';
			$config3['source_image'] = $data['upload_data']['full_path'];
			$config3['new_image'] = $data['upload_data']['file_path']."thumbs/".$data['upload_data']['raw_name']."_1.jpg";
			$config3['maintain_ratio'] = TRUE;
			if ($image_width > $image_height || $image_width == $image_height) {
				// image is either square or wider than it is tall
				if ( round($lg_width * $ratioh) >= $lg_height) {
					$config3['width'] = $lg_width;
					$config3['height'] = round($lg_width * $ratioh);
				} else {
					$config3['width'] = round($lg_height * $ratiow);
					$config3['height'] = $lg_height;
				}
			} else {
				$config3['width'] = $lg_width;
				$config3['height'] = round($lg_width * $ratioh);
			}
			$this->image_lib->clear();			
			$this->image_lib->initialize($config3);
			if ( ! $this->image_lib->resize()) { echo $this->image_lib->display_errors(); }
			$croplarge_path = "/".$config['upload_path']."/thumbs/".$data['upload_data']['raw_name']."_1.jpg";


			// Options for Thumb Resize #2 - 210x120
			$sm_width = 210;  $sm_height = 120;
			$config4['image_library'] = 'gd2';
			$config4['source_image'] = $data['upload_data']['full_path'];
			$config4['new_image'] = $data['upload_data']['file_path']."thumbs/".$data['upload_data']['raw_name']."_2.jpg";
			$config4['maintain_ratio'] = TRUE;
			if ($image_width > $image_height || $image_width == $image_height) {
				// image is either square or wider than it is tall
				if ( round($sm_width * $ratioh) >= $sm_height) {
					$config4['width'] = $sm_width;
					$config4['height'] = round($sm_width * $ratioh);
				} else {
					$config4['width'] = round($sm_height * $ratiow);
					$config4['height'] = $sm_height;
				}
			} else {
				// image is taller than it is wide
				$config4['width'] = $sm_width;
				$config4['height'] = round($sm_width * $ratioh);
			}
			$this->image_lib->clear();			
			$this->image_lib->initialize($config4);
			if ( ! $this->image_lib->resize()) { echo $this->image_lib->display_errors(); }
			$cropsmall_path = "/".$config['upload_path']."/thumbs/".$data['upload_data']['raw_name']."_2.jpg";


			// Options for Thumb Resize #3 - 1050x600
			$xl_width = 1050; $xl_height = 600;
			if ($image_width >= $xl_width || $image_height >= $xl_height) {
				$config5['image_library'] = 'gd2';
				$config5['source_image'] = $data['upload_data']['full_path'];
				$config5['new_image'] = $data['upload_data']['file_path']."thumbs/".$data['upload_data']['raw_name']."_3.jpg";
				$config5['maintain_ratio'] = TRUE;
				if ($image_width > $image_height || $image_width == $image_height) {
					// image is either square or wider than it is tall
					if ( round($xl_width * $ratioh) >= $xl_height) {
						$config5['width'] = $xl_width;
						$config5['height'] = round($xl_width * $ratioh);
					} else {
						$config5['width'] = round($xl_height * $ratiow);
						$config5['height'] = $xl_height;
					}
				} else {
					$config5['width'] = $xl_width;
					$config5['height'] = round($xl_width * $ratioh);
				}
				$this->image_lib->clear();			
				$this->image_lib->initialize($config5);
				if ( ! $this->image_lib->resize()) { echo $this->image_lib->display_errors(); }
				$cropxlarge_path = "/".$config['upload_path']."/thumbs/".$data['upload_data']['raw_name']."_3.jpg";
			} else {
				$cropxlarge_path = "";
				$config5['new_image'] = "";
			}

			// Update database with photo info
			$values = array(
				"movie_id" => $movie_id,
				"order" => $order,
				"url_original" => $original_path,
				"url_cropxlarge" => $cropxlarge_path,
				"url_croplarge" => $croplarge_path,
				"url_cropsmall" => $cropsmall_path,
				"file_dir" => $data['upload_data']['file_path'],
				"updated_date" => date("Y-m-d H:i:s")
			);
			$new_id = $this->photovideo->add_value($values, "wf_movie_photos");

            print "<div id=\"photo-crop-data\">\n";
			print "<input type='hidden' name='x1' value='0' id='x1' />\n";
			print "<input type='hidden' name='y1' value='0' id='y1' />\n";
			print "<input type='hidden' name='w' value='525' id='w' />\n";
			print "<input type='hidden' name='h' value='300' id='h' />\n";
			print "<input type='hidden' name='orig-filename0' id='orig-filename0' value='".$config2['source_image']."' />\n";
			print "<input type='hidden' name='orig-filename1' id='orig-filename1' value='".$config3['new_image']."' />\n";
			print "<input type='hidden' name='orig-filename2' id='orig-filename2' value='".$config4['new_image']."' />\n";
			print "<input type='hidden' name='orig-filename3' id='orig-filename3' value='".$config5['new_image']."' />\n";
			print "<input type='hidden' name='photo-new-id' id='photo-new-id' value='".$new_id."' />\n";
			print "<input type='hidden' name='photo-new-path' id='photo-new-path' value='".$croplarge_path."' />";
			//print "<input type='hidden' name='orig-width' id='orig-width'  value='".$data['upload_data']['image_width']."' />\n";
			//print "<input type='hidden' name='orig-height' id='orig-height'  value='".$data['upload_data']['image_height']."' />\n";
			print "</div>\n";
		}
	}

	// Takes crop parameters and crops the 3 thumbnails
	function upload_photo_crop() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Photovideomodel','photovideo');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$img_dir = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$this->load->library('image_lib'); 
		
		$x1 = $this->input->post('x1');
		$y1 = $this->input->post('y1');
		$width = $this->input->post('w');
		$height = $this->input->post('h');
		if ($width < 525) { $width = 525; }
		if ($height < 300) { $height = 300; }
		$orig_filename1 = $this->input->post('orig-filename1');
		$orig_filename2 = $this->input->post('orig-filename2');
		$orig_filename3 = $this->input->post('orig-filename3');
		$new_id = $this->input->post('photo-new-id');
		$new_path = $this->input->post('photo-new-path');

		// Options for Thumb Crop #1 - 525x300
		$config3['image_library'] = 'gd2';
		$config3['source_image'] = $orig_filename1;
		$config3['create_thumb'] = FALSE;
		$config3['maintain_ratio'] = FALSE;
		$config3['width'] = $width;
		$config3['height'] = $height;
		$config3['x_axis'] = $x1;
		$config3['y_axis'] = $y1;
		
		$this->image_lib->initialize($config3);
		if ( ! $this->image_lib->crop()) { echo $this->image_lib->display_errors(); }

		// Options for Thumb Crop #2 - 210x120
		$config4['image_library'] = 'gd2';
		$config4['source_image'] = $orig_filename2;
		$config4['create_thumb'] = FALSE;
		$config4['maintain_ratio'] = FALSE;
		$config4['width'] = round($width * 0.4);
		$config4['height'] = round($height * 0.4);
		$config4['x_axis'] = round($x1 * 0.4);
		$config4['y_axis'] = round($y1 * 0.4);
		
		$this->image_lib->clear();			
		$this->image_lib->initialize($config4);
		if ( ! $this->image_lib->crop()) { echo $this->image_lib->display_errors(); }

		// Options for Thumb Crop #3 - 1050x600
		if ($orig_filename3 != "") {
			$config5['image_library'] = 'gd2';
			$config5['source_image'] = $orig_filename3;
			$config5['create_thumb'] = FALSE;
			$config5['maintain_ratio'] = FALSE;
			$config5['width'] = round($width * 2);
			$config5['height'] = round($height * 2);
			$config5['x_axis'] = round($x1 * 2);
			$config5['y_axis'] = round($y1 * 2);
			
			$this->image_lib->clear();			
			$this->image_lib->initialize($config5);
			if ( ! $this->image_lib->crop()) { echo $this->image_lib->display_errors(); }
		}

		print "<li id=\"photo-".$new_id."\" class=\"ui-state-default ui-corner-all\">";
		print "<div><a href=\"#\"><img src=\"".str_replace("_1.","_2.",$new_path)."\" height=\"120\" border=\"0\" class=\"added_photo\"></a></div>";
		if ($this->session->userdata('limited') == 0) {
			print "<div><a href=\"#\" id=\"photo-del-".$new_id."\" data-id=\"".$new_id."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Photo\" title=\"Delete this Photo\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a></div>";
		}
		print "</li>";
	}

	// Removes photos from the file system and from the database - if upload or crop is cancelled
	function remove_uploaded_photos() {
		$this->load->model('Photovideomodel','photovideo');
		$photo_id = $this->input->post('photo-new-id');
	
		$original = unlink($this->input->post('orig-filename0'));
		$thumb1 = unlink($this->input->post('orig-filename1'));
		$thumb2 = unlink($this->input->post('orig-filename2'));
		$thumb3 = unlink($this->input->post('orig-filename3'));
		if ($original == true && $thumb1 == true && $thumb2 == true && $thumb3 == true) {
			print "Photo and all thumbnails have been deleted!";
		}

		// Actually delete the database entry
		$this->photovideo->delete_value($photo_id, "wf_movie_photos");
	}


	// Removes photos from the file system and from the database - after initial upload and processing
	function delete_photo($photo_id) {
		$this->load->model('Photovideomodel','photovideo');

		// Grab photo information with file urls
		$photo_info = $this->photovideo->get_movie_photo($photo_id);
		$file_dir = $photo_info[0]->file_dir;

		// Delete the original and three thumbnail files
		$p = explode("/",$photo_info[0]->url_original);
		$original = unlink($file_dir."/".$p[count($p)-1]);
		$p = explode("/",$photo_info[0]->url_croplarge);
		$thumb1 = unlink($file_dir."/".$p[count($p)-2]."/".$p[count($p)-1]);
		$p = explode("/",$photo_info[0]->url_cropsmall);
		$thumb2 = unlink($file_dir."/".$p[count($p)-2]."/".$p[count($p)-1]);		
		$p = explode("/",$photo_info[0]->url_cropxlarge);
		$thumb3 = unlink($file_dir."/".$p[count($p)-2]."/".$p[count($p)-1]);		
		if ($original == true && $thumb1 == true && $thumb2 == true && $thumb3 == true) {
			print "Photo and all thumbnails have been deleted!";
		}

		// Actually delete the database entry
		$photo_id = $this->photovideo->delete_value($photo_id, "wf_movie_photos");
	}

/* -------------------
	Video Functions
---------------------- */

	// Updates the order of a film's videos
	function save_video_sort() {
		$this->load->model('Photovideomodel','photovideo');
		foreach ($this->input->post('video') as $position => $item) {
			$values = array('order' => intval($position+1), "updated_date" => date("Y-m-d H:i:s"));
			$this->photovideo->update_value($values, "wf_movie_videos", $item);
		}
		print "Video order successfully saved!";
	}

	// Accepts url input and adds the video if it recongnizes it as a supported service
	function add_video() {
		$this->load->library('oembed');
		$this->load->model('Photovideomodel','photovideo');
		
		$service_name = "";
		$url_array = explode("/",$this->input->post('video-trailerurl'));
		$domain = trim($url_array[2],"www.");
		$service_name = explode(".",$domain);

		$proceed = false;
		switch ($service_name[0]) {
			case "hulu":
			case "revision3":
			case "qik":
			case "viddler":
			case "vimeo":
			case "youtube":
			case "bliptv":
			case "dailymotion":
			case "googlevideo":
			case "metacafe": $proceed = true; break;
			default: break;
		}

		if ($proceed == true) {
			$values = array(
				"movie_id" => $this->input->post('video-movie-id'),
				"url_video" => $this->input->post('video-trailerurl'),
				"service_name" => strtolower($service_name[0]),
				"order" => $this->input->post('highest-id')+1,
				"updated_date" => date("Y-m-d H:i:s")
			);
			
			$video_id = $this->photovideo->add_value($values,"wf_movie_videos");
			switch ($values["service_name"]) {
				case "hulu":
				case "revision3":
				case "qik":
				case "viddler":
				case "vimeo":
				case "youtube": $video_object = $this->oembed->call($values['service_name'], $values['url_video']);
								break;			
				case "bliptv":
				case "dailymotion":
				case "googlevideo":
				case "metacafe": $video_object = $this->oembed->call('oohembed', $values['url_video']);
								 break;
				default: $video_object = "No video service was specified.";
						 break;
			}
	
			print "<li id=\"video-".$video_id."\" class=\"ui-state-default ui-corner-all\">";
			print "<div>".$video_object->html."</div>";
			print "<div><a href=\"#\" id=\"video-edit-".$video_id."\" data-id=\"".$video_id."\" data-url=\"".$values['url_video']."\" data-order=\"".$values['order']."\" class=\"edit\"><img src=\"/assets/images/edit.png\" alt=\"Edit this Video\" title=\"Edit this Video\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a></div>";
			print "</li>";
			print "<li id=\"video_replace_me\" style=\"display:none;\"><input type=\"hidden\" id=\"video-id-new\" value=\"".$video_id."\" /></li>";
		} else {
			print "<li id=\"video-0\" class=\"ui-state-default ui-corner-all\">";
			print "<div>The video you tried to add was<br>not on one of the supported<br>sites, so it was not added.<br>Please try again.</div>";
			print "</li>";
			print "<li id=\"video_replace_me\" style=\"display:none;\"><input type=\"hidden\" id=\"video-id-new\" value=\"0\" /></li>";
		}
	}

	// Updates an existing video
	function update_video() {
		$this->load->library('oembed');
		$this->load->model('Photovideomodel','photovideo');
		$order = $this->input->post('video-current-order');

		$service_name = "";
		$url_array = explode("/",$this->input->post('video-trailerurl-update'));
		$domain = trim($url_array[2],"www.");
		$service_name = explode(".",$domain);

		$values = array(
			"url_video" => $this->input->post('video-trailerurl-update'),
			"service_name" => strtolower($service_name[0]),
			"updated_date" => date("Y-m-d H:i:s")
		);
		$x = $this->photovideo->update_value($values,"wf_movie_videos",$this->input->post('video-id-update'));
		$video_id = $this->input->post('video-id-update');

		print "<li id=\"video-".$video_id."\" class=\"ui-state-default ui-corner-all\">";
		print "<div>".$values['url_video']."</div>";
		print "<div><a href=\"#\" id=\"video-edit-".$video_id."\" data-id=\"".$video_id."\" data-url=\"".$values['url_video']."\" data-order=\"".$order."\" class=\"edit\"><img src=\"/assets/images/edit.png\" alt=\"Edit this Video\" title=\"Edit this Video\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a></div>";
		print "</li>";
	}

	// Removes video entry from database
	function delete_video() {
		$this->load->model('Photovideomodel','photovideo');

		$video_id = $this->photovideo->delete_value($this->input->post('video-id-update'), "wf_movie_videos");
	}


/* -------------------
	Print Traffic / Shipment Functions
---------------------- */

	// Gets list of screenings for a film in JSON format
	function return_ptraffic_json($movie_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		print json_encode($this->printtraffic->get_pt_shipments($movie_id));
	}

	// Adds new print traffic shipment leg
	function add_shipment($festivalmovie_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Filmtypesmodel','filmtype');
		
		$courier_array = convert_to_array($this->filmtype->get_all_type("courier","asc"));

		$outbound = $this->input->post("outbound-new");
        $tracking_url = "";
        $tracking_link = 0;  

		$values = array(
			"festivalmovie_id" => $festivalmovie_id,
			"courier_id" => $this->input->post("courier_id-new"),
			"AccountNum" => $this->input->post("AccountNum-new"),
			"TrackingNum" => $this->input->post("TrackingNum-new"),
			"ShippingFee" => $this->input->post("ShippingFee-new"),
			"wePay" => $this->input->post("wePay-new"),
			"notes" => $this->input->post("notes-new"),
			"confirmed" => 0
		);

		if ($outbound == 1) {
			// Inbound
			$values["outbound"] = 0;
			//$values["InShipped"] = $this->input->post("InShipped-new");
			$values["InExpectedArrival"] = $this->input->post("InExpectedArrival-new");
			$values["InReceived"] = $this->input->post("InReceived-new");
		} else if ($outbound == 2) {
			// Outbound
			$values["outbound"] = 1;
			//$values["OutScheduleBy"] = $this->input->post("OutScheduleBy-new");
			//$values["OutShipBy"] = $this->input->post("OutShipBy-new");
			$values["OutShipped"] = $this->input->post("OutShipped-new");
			$values["OutArriveBy"] = $this->input->post("OutArriveBy-new");
		}

		$plain_id = $this->printtraffic->add_value($values,"wf_printtraffic_shipments");
		$id = "-".$plain_id;

		$courier_id = $values["courier_id"];
        if ($courier_id != 0) {
            if ($values["TrackingNum"] != "" && ($courier_array[$courier_id] == "FedEx" || $courier_array[$courier_id] == "DHL" || $courier_array[$courier_id] == "UPS" || $courier_array[$courier_id] == "USPS")) {
                $tracking_url = get_tracking_url($courier_array[$courier_id],$values["TrackingNum"]);
                $tracking_link = 1;
            }
        }

		// Print double rows of print traffic shipment info
		if ($outbound == 1) {
			// Inbound
			$columns = 8;
            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
            print "\t\t\t\t\t<td><button id=\"ptin-".$plain_id."\" name=\"ptin-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
            if ($this->session->userdata('limited') == 0) {
                print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
            }
            print "</td>\n";
            if ($values["wePay"] == 1) {
                print "\t\t\t\t\t<td>We Pay</td>\n";
            } else {
                print "\t\t\t\t\t<td>Receiver Pays</td>\n";
            }
            if ($courier_id == 0) {
                print "\t\t\t\t\t<td>None Selected</td>\n";
            } else {
                print "\t\t\t\t\t<td>".$courier_array[$courier_id]."</td>\n";
            }
            print "\t\t\t\t\t<td>".$values["AccountNum"]."</td>\n";
            print "\t\t\t\t\t<td>";
            if ($tracking_link == 1) {
                print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
            }
            print $values["TrackingNum"]."</td>\n";
            print "\t\t\t\t\t<td>$".$values["ShippingFee"]."</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["InShipped"] != "0000-00-00" && $values["InShipped"] != "") { print date("m/d/Y",strtotime($values["InShipped"])); }
            //print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["InExpectedArrival"] != "0000-00-00" && $values["InExpectedArrival"] != "") { print date("m/d/Y",strtotime($values["InExpectedArrival"])); }
            print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["InReceived"] != "0000-00-00" && $values["InReceived"] != "") { print date("m/d/Y",strtotime($values["InReceived"])); }
            print "</td>\n";
            print "\t\t\t\t</tr>\n";

            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n";
            print "\t\t\t\t\t<td colspan=\"1\">";
            if ($values["confirmed"] == 1) {
                print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"receive\">Received</button>";
            } else {
                print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"receive\">Not Received</button>";
            }
            print "</td>\n";
            print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$values["notes"]."</td>\n";
            print "\t\t\t\t</tr>\n";

			print "\t\t<tr valign=\"top\" style=\"display:none;\">\n";
    	    print "\t\t\t<td colspan=\"10\"><div id=\"add_inbound_shipment_here\"><input type=\"hidden\" name=\"added_inbound_pt_id\" id=\"added_inbound_pt_id\" value=\"".$plain_id."\" /></div></td>\n";
			print "\t\t</tr>\n";
		} else if ($outbound == 2) {
			// Outbound
			$columns = 9;
            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
            print "\t\t\t\t\t<td><button id=\"ptout-".$plain_id."\" name=\"ptout-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
            if ($this->session->userdata('limited') == 0) {
                print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
            }
            print "</td>\n";
            if ($values["wePay"] == 1) {
                print "\t\t\t\t\t<td>We Pay</td>\n";
            } else {
                print "\t\t\t\t\t<td>Receiver Pays</td>\n";
            }
            if ($courier_id == 0) {
                print "\t\t\t\t\t<td>None Selected</td>\n";
            } else {
                print "\t\t\t\t\t<td>".$courier_array[$courier_id]."</td>\n";
            }
            print "\t\t\t\t\t<td>".$values["AccountNum"]."</td>\n";
            print "\t\t\t\t\t<td>";
            if ($tracking_link == 1) {
                print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
            }
            print $values["TrackingNum"]."</td>\n";
            print "\t\t\t\t\t<td>$".$values["ShippingFee"]."</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["OutScheduleBy"] != "0000-00-00" && $values["OutScheduleBy"] != "") { print date("m/d/Y",strtotime($values["OutScheduleBy"])); }
            //print "</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["OutShipBy"] != "0000-00-00" && $values["OutShipBy"] != "") { print date("m/d/Y",strtotime($values["OutShipBy"])); }
            //print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["OutShipped"] != "0000-00-00" && $values["OutShipped"] != "") { print date("m/d/Y",strtotime($values["OutShipped"])); }
            print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["OutArriveBy"] != "0000-00-00" && $values["OutArriveBy"] != "") { print date("m/d/Y",strtotime($values["OutArriveBy"])); }
            print "</td>\n";
            print "\t\t\t\t</tr>\n";

            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n";
            print "\t\t\t\t\t<td colspan=\"1\">";
            if ($values["confirmed"] == 1) {
                print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"ship\">Shipped</button>";
            } else {
                print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"ship\">Not Shipped</button>";
            }
            print "</td>\n";
            print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$values["notes"]."</td>\n";
            print "\t\t\t\t</tr>\n";

			print "\t\t<tr valign=\"top\" style=\"display:none;\">\n";
    	    print "\t\t\t<td colspan=\"10\"><div id=\"add_outbound_shipment_here\"><input type=\"hidden\" name=\"added_outbound_pt_id\" id=\"added_outbound_pt_id\" value=\"".$plain_id."\" /></div></td>\n";
			print "\t\t</tr>\n";
		}

	}

	function edit_shipment($printtraffic_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Filmtypesmodel','filmtype');
		
		$courier_array = convert_to_array($this->filmtype->get_all_type("courier","asc"));

		$outbound = $this->input->post("outbound-update");
        $tracking_url = "";
        $tracking_link = 0;

		$values = array(
			"confirmed" => $this->input->post("confirmed-update"),
			"courier_id" => $this->input->post("courier_id-update"),
			"AccountNum" => $this->input->post("AccountNum-update"),
			"TrackingNum" => $this->input->post("TrackingNum-update"),
			"ShippingFee" => $this->input->post("ShippingFee-update"),
			"wePay" => $this->input->post("wePay-update"),
			"notes" => $this->input->post("notes-update")
		);

		if ($outbound == 1) {
			// Inbound
			//$values["InShipped"] = $this->input->post("InShipped-update");
			$values["InExpectedArrival"] = $this->input->post("InExpectedArrival-update");
			$values["InReceived"] = $this->input->post("InReceived-update");
		} else if ($outbound == 2) {
			// Outbound
			//$values["OutScheduleBy"] = $this->input->post("OutScheduleBy-update");
			//$values["OutShipBy"] = $this->input->post("OutShipBy-update");
			$values["OutShipped"] = $this->input->post("OutShipped-update");
			$values["OutArriveBy"] = $this->input->post("OutArriveBy-update");
		}

		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$printtraffic_id);
		$plain_id = $printtraffic_id;
		$id = "-".$plain_id;

		$courier_id = $values["courier_id"];
        if ($courier_id != 0) {
            if ($values["TrackingNum"] != "" && ($courier_array[$courier_id] == "FedEx" || $courier_array[$courier_id] == "DHL" || $courier_array[$courier_id] == "UPS" || $courier_array[$courier_id] == "USPS")) {
                $tracking_url = get_tracking_url($courier_array[$courier_id],$values["TrackingNum"]);
                $tracking_link = 1;
            }
        }

		// Print double rows of print traffic shipment info
		if ($outbound == 1) {
			// Inbound
			$columns = 8;
            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
            print "\t\t\t\t\t<td><button id=\"ptin-".$plain_id."\" name=\"ptin-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
            if ($this->session->userdata('limited') == 0) {
                print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
            }
            print "</td>\n";
            if ($values["wePay"] == 1) {
                print "\t\t\t\t\t<td>We Pay</td>\n";
            } else {
                print "\t\t\t\t\t<td>Receiver Pays</td>\n";
            }
            if ($courier_id == 0) {
                print "\t\t\t\t\t<td>None Selected</td>\n";
            } else {
                print "\t\t\t\t\t<td>".$courier_array[$courier_id]."</td>\n";
            }
            print "\t\t\t\t\t<td>".$values["AccountNum"]."</td>\n";
            print "\t\t\t\t\t<td>";
            if ($tracking_link == 1) {
                print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
            }
            print $values["TrackingNum"]."</td>\n";
            print "\t\t\t\t\t<td>$".$values["ShippingFee"]."</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["InShipped"] != "0000-00-00" && $values["InShipped"] != "") { print date("m/d/Y",strtotime($values["InShipped"])); }
            //print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["InExpectedArrival"] != "0000-00-00" && $values["InExpectedArrival"] != "") { print date("m/d/Y",strtotime($values["InExpectedArrival"])); }
            print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["InReceived"] != "0000-00-00" && $values["InReceived"] != "") { print date("m/d/Y",strtotime($values["InReceived"])); }
            print "</td>\n";
            print "\t\t\t\t</tr>\n";

            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n";
            print "\t\t\t\t\t<td colspan=\"1\">";
            if ($values["confirmed"] == 1) {
                print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"receive\">Received</button>";
            } else {
                print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"receive\">Not Received</button>";
            }
            print "</td>\n";
            print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$values["notes"]."</td>\n";
            print "\t\t\t\t</tr>\n";
		} else if ($outbound == 2) {
			// Outbound
			$columns = 9;
            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow\">\n";
            print "\t\t\t\t\t<td><button id=\"ptout-".$plain_id."\" name=\"ptout-".$plain_id."\" data-id=\"".$plain_id."\" class=\"edit\">Edit</button> ";
            if ($this->session->userdata('limited') == 0) {
                print "<a href=\"#\" id=\"shipment-del".$id."\" data-id=\"".$plain_id."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Shipment\" title=\"Delete this Shipment\" width=\"16\" height=\"16\" border=\"0\"></a>";
            }
            print "</td>\n";
            if ($values["wePay"] == 1) {
                print "\t\t\t\t\t<td>We Pay</td>\n";
            } else {
                print "\t\t\t\t\t<td>Receiver Pays</td>\n";
            }
            if ($courier_id == 0) {
                print "\t\t\t\t\t<td>None Selected</td>\n";
            } else {
                print "\t\t\t\t\t<td>".$courier_array[$courier_id]."</td>\n";
            }
            print "\t\t\t\t\t<td>".$values["AccountNum"]."</td>\n";
            print "\t\t\t\t\t<td>";
            if ($tracking_link == 1) {
                print "<a href=\"".$tracking_url."\" target=\"_blank\"><img src=\"/assets/images/package_link.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Track this Shipment\" title=\"Track this Shipment\"></a> ";
            }
            print $values["TrackingNum"]."</td>\n";
            print "\t\t\t\t\t<td>$".$values["ShippingFee"]."</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["OutScheduleBy"] != "0000-00-00" && $values["OutScheduleBy"] != "") { print date("m/d/Y",strtotime($values["OutScheduleBy"])); }
            //print "</td>\n";
            //print "\t\t\t\t\t<td>";
            //if ($values["OutShipBy"] != "0000-00-00" && $values["OutShipBy"] != "") { print date("m/d/Y",strtotime($values["OutShipBy"])); }
            //print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["OutShipped"] != "0000-00-00" && $values["OutShipped"] != "") { print date("m/d/Y",strtotime($values["OutShipped"])); }
            print "</td>\n";
            print "\t\t\t\t\t<td>";
            if ($values["OutArriveBy"] != "0000-00-00" && $values["OutArriveBy"] != "") { print date("m/d/Y",strtotime($values["OutArriveBy"])); }
            print "</td>\n";
            print "\t\t\t\t</tr>\n";

            print "\t\t\t\t<tr valign=\"top\" class=\"oddrow2\">\n";
            print "\t\t\t\t\t<td colspan=\"1\">";
            if ($values["confirmed"] == 1) {
                print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"ship\">Shipped</button>";
            } else {
                print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"ship\">Not Shipped</button>";
            }
            print "</td>\n";
            print "\t\t\t\t\t<td colspan=\"".$columns."\"><label>Notes:</label> ".$values["notes"]."</td>\n";
            print "\t\t\t\t</tr>\n";
		}
    }



	// Set Shipment to Shipped or Not Shipped
	function ship_shipment($shipment_id, $value) {
		$this->load->model('Printtrafficmodel','printtraffic');

		$plain_id = $shipment_id; $id = "-".$shipment_id;
		$set_value = 0;
		if ($value == 1) { $set_value = 1; }
		$values = array("confirmed" => $set_value);

		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$plain_id);

		if ($value == 1) {
            print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"ship\">Shipped</button>";
		} else {
            print "<button id=\"ptout-ship-".$plain_id."\" name=\"ptout-ship-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"ship\">Not Shipped</button>";
		}
	}

	// Set Shipment to Received or Not Received
	function receive_shipment($shipment_id, $value) {
		$this->load->model('Printtrafficmodel','printtraffic');

		$plain_id = $shipment_id; $id = "-".$shipment_id;
		$set_value = 0;
		if ($value == 1) { $set_value = 1; }
		$values = array("confirmed" => $set_value);

		$this->printtraffic->update_value($values,"wf_printtraffic_shipments",$plain_id);

		if ($value == 1) {
            print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"1\" class=\"receive\">Received</button>";
		} else {
            print "<button id=\"ptin-receive-".$plain_id."\" name=\"ptin-receive-".$plain_id."\" data-id=\"".$plain_id."\" data-confirmed=\"0\" class=\"receive\">Not Received</button>";
		}
	}

	// Delete shipment legs
	function delete_shipment($shipment_id) {
		$this->load->model('Printtrafficmodel','printtraffic');
		$id = $this->printtraffic->delete_value($shipment_id,"wf_printtraffic_shipments");
	}

/* -------------------
	Misc Functions
---------------------- */

	function add_clg($type, $movie_id) {
		$this->load->helper('form');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Languagemodel','language');
		
		$add_div = "";

		switch($type) {
			case "Country": $select = $this->filmtype->get_all_type("country","asc");
							$insert_id = $this->country->add_movie_country($movie_id);
							$text = "countries-";
							$add_div = "\t\t\t\t<div id=\"addCountryHere\"><input type=\"hidden\" name=\"addCountryId\" id=\"addCountryId\" value=\"".$insert_id."\" /></div>";
							break;

			case "Language": $select = $this->filmtype->get_all_type("language","asc");
							 $insert_id = $this->language->add_movie_language($movie_id);
							 $text = "languages-";
							$add_div = "\t\t\t\t<div id=\"addLanguageHere\"><input type=\"hidden\" name=\"addLanguageId\" id=\"addLanguageId\" value=\"".$insert_id."\" /></div>";
							 break;

			case "Genre":	$select = $this->filmtype->get_all_type("genre","asc");
							$insert_id = $this->genre->add_movie_genre($movie_id);
							$text = "genres-";
							$add_div = "\t\t\t\t<div id=\"addGenreHere\"><input type=\"hidden\" name=\"addGenreId\" id=\"addGenreId\" value=\"".$insert_id."\" /></div>";
							break;
		}
		$select_array = convert_to_array($select);
		print form_dropdown($text.$insert_id, $select_array, 0," class='select ui-widget-content ui-corner-all'")."<br />";
		print $add_div;
	}

	function toggle($field, $screening_id)
	{
		$this->load->model('Schedulemodel','schedule');
		
		// Get the info for the film value to be toggled.
		$data['screening'] = $this->schedule->get_screening_toggle($screening_id);
		
		// Don't do anything if we couldn't find the film id.
		if ($data['screening']) {
			$table_name = "wf_screening";

			switch ($field) {
				case "Private": if ($data['screening'][0]->Private == 0) {
									$this->schedule->val_on($screening_id, $table_name, $field);
									print "\t<a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_tick\"></a>\n";
								 } elseif ($data['screening'][0]->Private == 1) {
									$this->schedule->val_off($screening_id, $table_name, $field);
									print "\t<a id=\"Private-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Private\" class=\"icon_cross\"></a>\n";
								 } break;

				case "Rush": if ($data['screening'][0]->Rush == 0) {
									$this->schedule->val_on($screening_id, $table_name, $field);
									print "\t<a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_tick\"></a>\n";
								 } elseif ($data['screening'][0]->Rush == 1) {
									$this->schedule->val_off($screening_id, $table_name, $field);
									print "\t<a id=\"Rush-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Rush\" class=\"icon_cross\"></a>\n";
								 } break;

				case "Free": if ($data['screening'][0]->Free == 0) {
									$this->schedule->val_on($screening_id, $table_name, $field);
									print "\t<a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_tick\"></a>\n";
								 } elseif ($data['screening'][0]->Free == 1) {
									$this->schedule->val_off($screening_id, $table_name, $field);
									print "\t<a id=\"Free-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Free\" class=\"icon_cross\"></a>\n";
								 } break;

				case "Published": if ($data['screening'][0]->Published == 0) {
									$this->schedule->val_on($screening_id, $table_name, $field);
									print "\t<a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_tick\"></a>\n";
								 } elseif ($data['screening'][0]->Published == 1) {
									$this->schedule->val_off($screening_id, $table_name, $field);
									print "\t<a id=\"Published-".$screening_id."\" href=\"#\" data-id=\"".$screening_id."\" data-type=\"Published\" class=\"icon_cross\"></a>\n";
								 } break;
			}

			// Set 'Updated Date' field for all films associated with this screening
			// only for "Published"
			if ($data['screening'][0]->Published == 0 && $field == "Published") {
				$program_movie_ids = convert_string_to_array($data['screening'][0]->program_movie_ids);
				foreach ($program_movie_ids as $movie_id) {
					$values = array("updated_date" => date("Y-m-d H:i:s"));
					$this->schedule->update_value($values, "wf_movie", $movie_id);
				}
			}
		}
	}

}

/* End of file film_edit.php */
/* Location: ./system/application/controllers/admin/film_edit.php */
?>