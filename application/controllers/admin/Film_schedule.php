<?php
class Film_Schedule extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Locationmodel','location');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Sectionmodel','section');

		$this->load->helper('form');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		$data['locations'] = $this->location->get_internal_festival_locations(explode(",",$data['festival'][0]->locations));
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		$data['sections'] = $this->section->get_all_sections($current_fest);

		$data['festivaldates'] = createDateRangeArray($data['festival'][0]->startdate,$data['festival'][0]->enddate);

		$film_array = array();
		foreach ($data['filmids'] as $film) { $film_array[] = $film->id; }
		if (count($data['filmids']) == 0) { $film_array[] = 0; }

		$data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "asc");
		$data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "asc");

		$vars['title'] = "Film Schedule";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_schedule";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_schedule', $data);
		$this->load->view('admin/footer', $vars);
	}

	function add_screening() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		
		$locations = $this->location->get_all_locations();
		$location_id1 = 0; $location_id2 = 0;
		foreach ($locations as $thisLocation) {
			if ($thisLocation->id == $this->input->post('scrn-location-new')) { $location_id1 = $thisLocation->name; }
			if ($thisLocation->id == $this->input->post('scrn-location2-new')) { $location_id2 = $thisLocation->name; }
		}
		
		$total_films = $this->input->post('total_films');
		$film_ids = array();
		for ($x=1; $x<=$total_films; $x++) {
			if ($x <= 9) { $z = "0";} else { $z = ""; }
			$film_ids[] = $this->input->post('scrn-film-'.$z.$x);
		}
		$film_ids_string = convert_multiselect_to_string($film_ids);
		$time_string = date("H:i:s",strtotime($this->input->post('scrn-time-new')));

		// Runtime in minutes converted to 00:00:00 time type
		$total_runtime = convert_min_to_time($this->input->post('total-runtime'));

		// By default, intro and fest trailer are enabled, and add ~8 minutes to the runtime.
		$values = array(
			"movie_id" => $this->input->post('scrn-film-01'),
			"program_movie_ids" => $film_ids_string,
			"program_name" => $this->input->post('scrn-program-name-new'),
			"program_desc" => $this->input->post('scrn-program-desc-new'),
			"date" => $this->input->post('scrn-date-new'),
			"time" => $time_string,
			"location_id" => $this->input->post('scrn-location-new'),
			"location_id2" => $this->input->post('scrn-location2-new'),
			"url" => $this->input->post('scrn-ticket-new'),
			"host" => $this->input->post('scrn-host-new'),
			"notes" => $this->input->post('scrn-notes-new'),
			"Published" => $this->input->post('scrn-published-new'),
			"Private" => $this->input->post('scrn-private-new'),
			"Rush" => $this->input->post('scrn-rush-new'),
			"Free" => $this->input->post('scrn-free-new'),
			"AudienceCount" => $this->input->post('audience-count-new'),
			"ScreeningRevenue" => $this->input->post('screening-revenue-new'),

			"intro_length" => $this->input->post('intro-length'),
			"intro" => $this->input->post('intro'),
			"fest_trailer_length" => $this->input->post('fest-trailer-length'),
			"fest_trailer" => $this->input->post('fest-trailer'),
			"sponsor_trailer1_name" => $this->input->post('sponsor-trailer1-name'),
			"sponsor_trailer1_length" => $this->input->post('sponsor-trailer1-length'),
			"sponsor_trailer1" => $this->input->post('sponsor-trailer1'),
			"sponsor_trailer2_name" => $this->input->post('sponsor-trailer2-name'),
			"sponsor_trailer2_length" => $this->input->post('sponsor-trailer2-length'),
			"sponsor_trailer2" => $this->input->post('sponsor-trailer2'),
			"q_and_a_length" => $this->input->post('q-and-a-length'),
			"q_and_a" => $this->input->post('q-and-a'),
			"total_runtime" => $total_runtime,
			"updated_date" => date("Y-m-d H:i:s")
		);
		if ($this->input->post('scrn-program-name-new') != "") { $values["program_slug"] = create_slug($this->input->post('scrn-program-name-new')); } else { $values["program_slug"] = ""; }
		if ($this->input->post('sponsor-trailer1-name') == "Sponsor Trailer 1") { $values["sponsor_trailer1_name"] = ""; }
		if ($this->input->post('sponsor-trailer2-name') == "Sponsor Trailer 2") { $values["sponsor_trailer2_name"] = ""; }
		if ($this->input->post('scrn-published-new')) { $values["Published"] = 1; } else { $values["Published"] = 0; }
		if ($this->input->post('scrn-private-new')) { $values["Private"] = 1; } else { $values["Private"] = 0; }
		if ($this->input->post('scrn-rush-new')) { $values["Rush"] = 1; } else { $values["Rush"] = 0; }
		if ($this->input->post('scrn-free-new')) { $values["Free"] = 1; } else { $values["Free"] = 0; }
		if ($this->input->post('intro')) { $values["intro"] = 1; } else { $values["intro"] = 0; }
		if ($this->input->post('fest-trailer')) { $values["fest_trailer"] = 1; } else { $values["fest_trailer"] = 0; }
		if ($this->input->post('sponsor-trailer1')) { $values["sponsor_trailer1"] = 1; } else { $values["sponsor_trailer1"] = 0; }
		if ($this->input->post('sponsor-trailer2')) { $values["sponsor_trailer2"] = 1; } else { $values["sponsor_trailer2"] = 0; }
		if ($this->input->post('q-and-a')) { $values["q_and_a"] = 1; } else { $values["q_and_a"] = 0; }
		$new_id = $this->schedule->add_value($values, "wf_screening");

		$movie_id = $this->input->post('scrn-film-01');
		$filminfo = $this->films->get_film($movie_id);		
		$toUpdate = $this->input->post('divToUpdate');
		$runtime = $filminfo[0]->runtime_int;
		$starttime = strtotime($this->input->post('scrn-time-new'));
		$endtime = $starttime + ($runtime * 60);		
		$controltime = strtotime("8:00 AM");
		$leftOffset = 97 + abs( ($starttime - $controltime) / 60);

		if ($values['program_name'] != "") {
			$title = switch_title($values['program_name']);
		} else {
			$title = switch_title($filminfo[0]->title_en);
		}

		if (strlen($title) >= 27) {
			$title_trim = substr($title,0,27)."...";
		} else {
			$title_trim = $title;
		}

		if ($runtime == 0) {
			$runtime = 120;
		} else {
			$runtime = $runtime + 8;
		}
		
		print "\t\t\t<div style=\"width: ".$runtime."px; left: ".$leftOffset."px;\" class=\"floating ui-corner-all";
		if ($values['Published'] == 0) { print " ui-state-highlight"; }
		if ($values['Private'] != 0) {	print " private"; }
		if ($values['location_id2'] != 0) {	print " interlock"; }
		print "\" id=\"screening-".$new_id."\">\n";
		print "\t\t\t\t<div style=\"background-color: #".$filminfo[0]->color.";\" class=\"h\"></div>\n";
		print "\t\t\t\t<p class=\"information\"><a href=\"/admin/film_edit/update/".$filminfo[0]->slug."#tabs-4\">".$title_trim."</a><br><span class=\"time\">".date("h:i",$starttime)." - ".date("h:i",$endtime)."</span></p>\n";
		print "\t\t\t</div>\n";
		print "\t\t\t<div id=\"addScreeningHere-".$toUpdate."\"><input type=\"hidden\" value=\"".$new_id."\" name=\"newid-".$toUpdate."\" id=\"newid-".$toUpdate."\"></div>\n";
	}
	
	function update_screening($screening_id, $location_id, $timestamp) {
		$this->load->model('Schedulemodel','schedule');

		date_default_timezone_set('Pacific/Honolulu');
		$values = array(
			"date" => date("Y-m-d",$timestamp),
			"time" => date("H:i:s",$timestamp),
			"location_id" => $location_id,
			"updated_date" => date("Y-m-d H:i:s")
		);
		$this->schedule->update_value($values, "wf_screening", $screening_id);	
		print "Screening Updated!";
	}

	function delete_screening($screening_id) {
		$this->load->model('Schedulemodel','schedule');
		$this->schedule->delete_value($screening_id, "wf_screening");	
		print "Screening Deleted!";
	}
}

/* End of file film_schedule.php */
/* Location: ./system/application/controllers/admin/film_schedule.php */
?>