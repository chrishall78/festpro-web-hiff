<?php
class Film_Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Countrymodel','country');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Genremodel','genre');
		$this->load->model('Languagemodel','language');
		$this->load->model('Locationmodel','location');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Printtrafficmodel','printtraffic');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Sectionmodel','section');

		// Gathering data from the database
		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$film_id_array = convert_to_array3($data['filmids']); // uses 'festivalmovie.movie_id'
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$data['photos'] = $this->photovideo->get_all_photos($film_id_array);
		$data['videos'] = $this->photovideo->get_all_videos($film_id_array);

		$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_array = convert_to_array($data['filmids'], null, "id", false); // uses 'festivalmovie.id'
		if (count($film_array) == 0) { $film_array = array(0); }
		$data['inbound'] = $this->printtraffic->get_inbound_shipments($film_array,"wf_movie.title_en", "asc");
		$data['outbound'] = $this->printtraffic->get_outbound_shipments($film_array,"wf_movie.title_en", "asc");

		$sections = $this->section->get_all_sections($current_fest);
		$premieres = $this->filmtype->get_all_type("premiere", "asc", "no");
		$eventtypes = $this->filmtype->get_all_type("event", "asc", "no");
		$videoformats = $this->filmtype->get_all_type("format", "asc", "no");
		$couriers = $this->filmtype->get_all_type("courier", "asc", "no");

		$countries = $this->country->get_all_countries_in($film_id_array);
		$languages = $this->language->get_all_languages_in($film_id_array);
		$genres = $this->genre->get_all_genres_in($film_id_array);

		$locationsArray = explode(",", $data['festival'][0]->locations);
		$locations = $this->location->get_internal_festival_locations($locationsArray);


		// Counting Statistics
		$syn_short = $syn_long = $photo_num = $video_num = $scheduled = $confirmed = $published = 0;
		foreach ($data['films'] as $thisFilm) {
			// Counting Synopses
			switch ($thisFilm->synopsis) {
				case "":
				case "<br />":
				case "<br />\n":
				case "<p>&nbsp;</p>": break;
				default: $syn_long++; break;
			}
			switch ($thisFilm->synopsis_short) {
				case "":
				case "<br />":
				case "<br />\n":
				case "<p>&nbsp;</p>": $short_syn = false; break;
				default: $syn_short++; break;
			}

			// Counting Photos/Videos
			$film_photos = array();
			foreach ($data['photos'] as $thisPhoto) {
				if ($thisPhoto->movie_id == $thisFilm->movie_id) { $film_photos[] = $thisPhoto; }
			}
			if ( count($film_photos) > 0) { $photo_num++; }
			$film_videos = array();
			foreach ($data['videos'] as $thisVideo) {
				if ($thisVideo->movie_id == $thisFilm->movie_id) { $film_videos[] = $thisVideo; }
			}
			if ( count($film_videos) > 0) { $video_num++; }

			// Counting Scheduled Screenings
			foreach ($data['full_schedule'] as $thisScreening) {
				$film_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($film_array as $screeningFilm) {
					if ($screeningFilm == $thisFilm->movie_id) {
						$scheduled++;
						break 2; // break out of both foreach loops as soon as a match is found
					}
				}
			}

			// Counting Confirmed / Published
			if ($thisFilm->Confirmed != "0") { $confirmed++; }
			if ($thisFilm->Published != "0") { $published++; }
		}

		// Counting Multiple Screenings + Shorts Programs
		$screeningArray = array(); $data["screeningDays"] = array();
		$screenings_1 = $screenings_2 = $screenings_3 = $screenings_4 = $shorts_programs = 0;
		$published_screenings = $public_screenings = $private_screenings = $standby_screenings = $free_screenings = 0;
		$audience_attendance = $qa_counts = $avg_audience_attendance = $avg_qa_counts = 0;
		foreach ($data['full_schedule'] as $thisScreening) {
			if ($thisScreening->Private == "0") { $public_screenings++; }
			else { $private_screenings++; }
			if ($thisScreening->Rush != "0") { $standby_screenings++; }
			if ($thisScreening->Free != "0") { $free_screenings++; }
			if ($thisScreening->Published != "0") { $published_screenings++; }

			if ($thisScreening->AudienceCount > 0) {
				$audience_attendance += $thisScreening->AudienceCount;
				$avg_audience_attendance++;
			}
			if ($thisScreening->QandACount > 0) {
				$qa_counts += $thisScreening->QandACount;
				$avg_qa_counts++;
			}

			$film_array = explode(",",$thisScreening->program_movie_ids);
			if (count($film_array) > 3) { $shorts_programs++; }

			foreach ($film_array as $screeningFilm) {
				if ( !isset($screeningArray[$screeningFilm]) ) {
					$screeningArray[$screeningFilm] = 1;
				} else {
					$screeningArray[$screeningFilm]++;
				}
			}

			$screeningDate = $thisScreening->date;
			if ( !isset($data["screeningDays"][$screeningDate]) ) {
				$data["screeningDays"][$screeningDate] = 1;
			} else {
				$data["screeningDays"][$screeningDate]++;
			}

		}
		foreach ($screeningArray as $number) {
			if ($number > 3) { $screenings_4++; }
			elseif ($number == 3) { $screenings_3++; }
			elseif ($number == 2) { $screenings_2++; }
			elseif ($number == 1) { $screenings_1++; }
		}

		// Counting Inbound/Outbound Shipments
		$in_recd = $in_notrecd = $out_sent = $out_notsent = 0;
		foreach ($data['inbound'] as $thisShipment) {
			if ($thisShipment->confirmed == 0) { $in_notrecd++; }
			else { $in_recd++; }
		}
		foreach ($data['outbound'] as $thisShipment) {
			if ($thisShipment->confirmed == 0) { $out_notsent++; }
			else { $out_sent++; }
		}

		// Counting Sections, Premieres, Event Types, Video Formats
		$data['sections'] = calculateTypeSummary($sections, $data['films'], "category_id");
		$data['premieres'] = calculateTypeSummary($premieres, $data['films'], "premiere_id");
		$data['eventtypes'] = calculateTypeSummary($eventtypes, $data['films'], "event_id");
		$data['videoformats'] = calculateTypeSummary($videoformats, $data['films'], "format_id");

		// Counting Countries, Languages, Genres
		$data['countries'] = countTypeByName($countries);
		$data['languages'] = countTypeByName($languages);
		$data['genres'] = countTypeByName($genres);

		// Counting Locations
		$data['locations'] = array();
		foreach ($data['full_schedule'] as $thisScreening) {
			foreach ($locations as $thisLocation) {
				if ($thisScreening->location_id == $thisLocation->id) {
					if ( !isset($data['locations'][$thisLocation->name]) ) {
						$data['locations'][$thisLocation->name] = 1;
					} else {
						$data['locations'][$thisLocation->name]++;
					}
				}
			}
		}
		ksort($data['locations']);

		// Counting Couriers
		$data['couriers'] = array();
		foreach ($data['inbound'] as $thisShipment) {
			foreach ($couriers as $thisCourier) {
				if ($thisShipment->courier_id == $thisCourier->id) {
					if ( !isset($data['couriers'][$thisCourier->name]) ) {
						$data['couriers'][$thisCourier->name] = 1;
					} else {
						$data['couriers'][$thisCourier->name]++;
					}
				}
			}
		}
		foreach ($data['outbound'] as $thisShipment) {
			foreach ($couriers as $thisCourier) {
				if ($thisShipment->courier_id == $thisCourier->id) {
					if ( !isset($data['couriers'][$thisCourier->name]) ) {
						$data['couriers'][$thisCourier->name] = 1;
					} else {
						$data['couriers'][$thisCourier->name]++;
					}
				}
			}
		}
		ksort($data['couriers']);


		// Assembling array of data to be displayed on the dashboard
		$data['dashboard'] = array();

		// Films Info
		$data['dashboard']['total-films'] = count($data['films']);
		//$data['dashboard']['total-synopses'] = $syn_total;
		$data['dashboard']['long-synopsis'] = $syn_long;
		$data['dashboard']['short-synopsis'] = $syn_short;
		$data['dashboard']['photos'] = $photo_num;
		$data['dashboard']['videos'] = $video_num;
		$data['dashboard']['scheduled'] = $scheduled;
		$data['dashboard']['confirmed'] = $confirmed;
		$data['dashboard']['published'] = $published;

		// Screenings Info
		$data['dashboard']['total-screenings'] = count($data['full_schedule']);
		$data['dashboard']['public-screenings'] = $public_screenings;
		$data['dashboard']['private-screenings'] = $private_screenings;
		$data['dashboard']['standby-screenings'] = $standby_screenings;
		$data['dashboard']['free-screenings'] = $free_screenings;
		$data['dashboard']['published-screenings'] = $published_screenings;
		$data['dashboard']['screened-1'] = $screenings_1;
		$data['dashboard']['screened-2'] = $screenings_2;
		$data['dashboard']['screened-3'] = $screenings_3;
		$data['dashboard']['screened-4plus'] = $screenings_4;
		$data['dashboard']['unique-programs'] = findNumberOfPrograms($data['full_schedule']);
		$data['dashboard']['shorts-programs'] = $shorts_programs;
		$data['dashboard']['audience-attendance'] = $audience_attendance;
		$data['dashboard']['avg-audience-attendance'] = $avg_audience_attendance;
		$data['dashboard']['qa-counts'] = $qa_counts;
		$data['dashboard']['avg-qa-counts'] = $avg_qa_counts;

		// Print Traffic Info
		$data['dashboard']['inbound-prints'] = count($data['inbound']);
		$data['dashboard']['inbound-received'] = $in_recd;
		$data['dashboard']['inbound-not-received'] = $in_notrecd;
		$data['dashboard']['outbound-prints'] = count($data['outbound']);
		$data['dashboard']['outbound-sent'] = $out_sent;
		$data['dashboard']['outbound-not-sent'] = $out_notsent;


		$vars['schedule'] = convert_to_array_schedule2($data['full_schedule']);
		$vars['title'] = "Dashboard";
		$vars['path'] = "/";
		$vars['selected_page'] = "film_dashboard";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/film_dashboard', $data);
		$this->load->view('admin/footer', $vars);
	}
}

/* End of file film_dashboard.php */
/* Location: ./system/application/controllers/admin/film_dashboard.php */
?>