<?php
class User_Account extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Usersmodel','users');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['user_list'] = $this->users->get_username($this->session->userdata('username'));
		$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);

		$vars['title'] = "My Account";
		$vars['path'] = "/";
		$vars['selected_page'] = "user_account";
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/user_account', $data);
		$this->load->view('admin/footer', $vars);
	}

	function update_user()
	{
		$this->load->model('Usersmodel','users');
		$this->load->library('encrypt');
				
		// Assemble values for user update
		$values = array(
			"Username" => $this->input->post('Username-update'),
			"FirstName" => $this->input->post('FirstName-update'),
			"LastName" => $this->input->post('LastName-update'),
			"Email" => $this->input->post('Email-update')
		);

		// Encrypt password
		$Password1 = $this->input->post('Password1-update');
		$Password2 = $this->input->post('Password2-update');
		
		if ($Password1 == "" && $Password2 == "") {
			// password fields are blank - don't change password
		} else if ($Password1 == $Password2 && $Password1 != "" && $Password2 != "") {
			// both passwords match - change password
			$values["Password"] = $this->encrypt->encode($Password1);
		}

		$updated_id = $this->users->update_value($values, "users", $this->input->post('user-id-update'));
		
		print $values['Username'];
	}
	
	function change_festival() {
		$this->load->library('user_agent');
		
		// This modifies the user's session data to specify which festival to load.
		$new_festival_id = $this->input->post("changeFestival");
		if ($new_festival_id != 0) {
			$this->session->set_userdata('festival', $new_festival_id);
		}
		
		// Redirect to either guest or film listing, depending on what page the user is currently on
		if ($this->agent->is_referral()) {
    		//echo $this->agent->referrer();
			$active_url = explode("/",$this->agent->referrer());
			switch (substr($active_url[4],0,5)) {
				case "guest": $suite = "guest"; break;
				case "film_": $suite = "film"; break;
				case "user_": $suite = "film"; break;
				default: $suite = "film"; break;
			}
			header('Location: /admin/'.$suite.'_listing/');			
		}		
	}
}

/* End of file user_account.php */
/* Location: ./system/application/controllers/admin/user_account.php */
?>