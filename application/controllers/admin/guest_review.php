<?php
class Guest_Review extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guestreviewmodel','guestreview');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Usersmodel','users');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['festivals'] = $this->festival->get_all_festivals();
		$data['user_list'] = $this->users->get_all_users();
		$data['current_user'] = $this->users->get_username($this->session->userdata('username'));
		$data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,"fg_guests_staging.SubmittedDate", "desc");
		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['guesttypes'] = $this->guesttype->get_all_type("guests", "asc");

		$vars['title'] = "Review Guest Accreditations";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_review";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $data['festivals'];

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_review', $data);
		$this->load->view('admin/footer', $vars);
	}

	function sort($sorttoken)
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guestreviewmodel','guestreview');
		$this->load->model('Guesttypesmodel','guesttype');
		$this->load->model('Usersmodel','users');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['festivals'] = $this->festival->get_all_festivals();
		$data['user_list'] = $this->users->get_all_users();
		$data['current_user'] = $this->users->get_username($this->session->userdata('username'));

		$tablename = "fg_guests_staging.";
		switch ($sorttoken) {
			case "namedesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."LastName", "desc"); break;
			case "nameasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."LastName", "asc");  break;

			case "filmdesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."Film", "desc"); break;
			case "filmasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."Film", "asc");  break;

			case "companydesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."FilmOrCompany", "desc"); break;
			case "companyasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."FilmOrCompany", "asc");  break;

			case "travelmethoddesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."TravelMethod", "desc"); break;
			case "travelmethodasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."TravelMethod", "asc");  break;

			case "categorydesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,"DelegateType", "desc"); break;
			case "categoryasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,"DelegateType", "asc");  break;

			case "feedesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."Fee", "desc"); break;
			case "feeasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."Fee", "asc");  break;

			case "datedesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."SubmittedDate", "desc"); break;
			case "dateasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."SubmittedDate", "asc");  break;

			case "statusdesc": $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."DelegateStatus", "desc"); break;
			case "statusasc":  $data['guest_items'] = $this->guestreview->get_guest_review_items($current_fest,$tablename."DelegateStatus", "asc");  break;
			
		}
		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['guesttypes'] = $this->guesttype->get_all_type("guests", "asc");

		$vars['title'] = "Review Guest Accreditations";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_review";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $data['festivals'];

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_review', $data);
		$this->load->view('admin/footer', $vars);
	}

	function import_guest() {
		// This function imports the guest information from this temporary spot into the Guest Listing page.
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guestreviewmodel','guestreview');
		$this->load->model('Guesttypesmodel','guesttype');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}
		
		$data['festivals'] = $this->festival->get_all_festivals();
		$data['countries'] = $this->filmtype->get_all_type("country","asc");

		$festivals_array = convert_festival_to_array2($data['festivals']);
		$countries_array = convert_to_array2($data['countries']);		
		$toImport = $this->input->post('guest-import-id');
		$thisGuest = $this->guestreview->get_guest_review($toImport);
		$slug = create_slug($thisGuest[0]->FirstName." ".$thisGuest[0]->LastName);
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->guest->check_existing_slug("fg_festivalguests", $slug);
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}

		$attending = "";
		/*
		if ($thisGuest[0]->AttendOpening == 0) { $attending .= "Attending Opening: NO\n"; } else { $attending .= "Attending Opening: YES\n"; }
		if ($thisGuest[0]->AttendClosing == 0) { $attending .= "Attending Closing: NO\n"; } else { $attending .= "Attending Closing: YES\n"; }
		*/
		if ($this->input->post('film-import') != "") { $attending .= "Film: ".$this->input->post('film-import'); }

		// 1. add fg_festivalguests table
		$guest_values = array(
			"slug" => $slug,
			"festival_id" => $this->input->post('festival-import-id'),
			"guesttype_id" => $thisGuest[0]->DelegateCategory,
			"Firstname" => $thisGuest[0]->FirstName,
			"Lastname" => $thisGuest[0]->LastName,
			"Address" => $thisGuest[0]->GuestAddress,
			"City" => $thisGuest[0]->GuestCity,
			"State" => $thisGuest[0]->GuestState,
			"PostalCode" => $thisGuest[0]->GuestPostal,
			"Phone" => $thisGuest[0]->DayPhone,
			"Cell" => $thisGuest[0]->MobilePhone,
			"Fax" => $thisGuest[0]->FaxPhone,
			"Email" => $thisGuest[0]->EmailAddress,
			"WebsiteURL" => $thisGuest[0]->WebsiteURL,
			"TwitterName" => str_replace("@","",$thisGuest[0]->TwitterName),
			"FacebookPage" => $thisGuest[0]->FacebookPage,
			"LinkedinProfile" => $thisGuest[0]->LinkedinProfile,
			"Bio" => $thisGuest[0]->Biography,
			"PhotoUrl" => $thisGuest[0]->GuestPhotoUrl,
			"PhotoFilePath" => $thisGuest[0]->GuestPhotoFilePath,
			"AffiliationOptional" => $this->input->post('company-import'),
			"notes" => $attending,
			"gb_pickedup" => 0,
			"badge_pickedup" => 0,
			"updated_date" => date("Y-m-d H:i:s")
		);
		if ($thisGuest[0]->TranslatorLang != 0 || $thisGuest[0]->TranslatorLang != NULL) { $guest_values['language_id'] = $thisGuest[0]->TranslatorLang; }
		$fg_guest_id = $this->guest->add_value($guest_values,"fg_festivalguests");

		// 2. add fg_accomodations table (hotels)
		/*
		$hotel_values = array(
			"festivalguest_id" => $fg_guest_id,
			"hotel" => $thisGuest[0]->LocalHotelName,
			"checkin" => "",
			"checkout" => "",
			"checkouttime" => "",
			"compnights" => 0,
			"hotelcomp_id" => 0,
			"share" => 0,
			"pickup" => 0,
			"translator" => 0,
			"notes" => $thisGuest[0]->LocalAddress."\n".$thisGuest[0]->LocalPhone
		);
		$fg_hotel_id = $this->guest->add_value($hotel_values,"fg_accomodations");
		*/

		// 3. add fg_flights table only if Travel Method is not Local
		if ($thisGuest[0]->TravelMethod != "Local") {
			$flight_values = array(
				"festivalguest_id" => $fg_guest_id,
				"Firstname" => $thisGuest[0]->FirstName,
				"Lastname" => $thisGuest[0]->LastName,
				"DepartureCity" => $thisGuest[0]->DepartureCity,
				"DepartureCity2" => $thisGuest[0]->DepartureCity2,
				"DepartureDate" => $thisGuest[0]->DepartureDate,
				"DepartureTime" => $thisGuest[0]->DepartureTime,
				"DepartureAirline" => $thisGuest[0]->DepartureAirline,
				"DepartureFlightNum" => $thisGuest[0]->DepartureFlightNum,
				"ArrivalCity" => $thisGuest[0]->ArrivalCity,
				"ArrivalCity2" => $thisGuest[0]->ArrivalCity2,
				"ArrivalDate" => $thisGuest[0]->ArrivalDate,
				"ArrivalTime" => $thisGuest[0]->ArrivalTime,
				"Airline" => $thisGuest[0]->ArrivalAirline,
				"FlightNumber" => $thisGuest[0]->ArrivalFlightNum
			);
			$fg_flights_id = $this->guest->add_value($flight_values,"fg_flights");
		}

		// 4. add fg_filmaffiliations table if a film has been chosen.
		if ($this->input->post('filmaff-import') != 0) {
			$film_values = array(
				"festivalguest_id" => $fg_guest_id,
				"movie_id" => $this->input->post('filmaff-import')
			);
			$filmaffiliation_id = $this->guest->add_value($film_values, "fg_filmaffiliations");
		}
		
		// Update the guest review entry to mark it as imported.
		$values = array(
			"Fee" => $this->input->post('fee-import'),
			"Imported" => 1,
			"ImportedBy" => $this->input->post('user-import-id'),
			"ImportedDate" => date("Y-m-d"),
			"ImportedFestival" => $this->input->post('festival-import-id')
		);
		$this->guest->update_value($values,"fg_guests_staging",$toImport);
		
		if ($this->input->post('rowtype-'.$toImport) == "oddrow" || $this->input->post('rowtype-'.$toImport) == "oddrow-imported") {
			print "\t\t\t<tr valign=\"top\" class=\"oddrow-imported\">\n";
			$rowtype = "oddrow-imported";
		} else {
			print "\t\t\t<tr valign=\"top\" class=\"evenrow-imported\">\n";
			$rowtype = "evenrow-imported";
		}
		print "\t\t\t\t<td rowspan=\"2\" align=\"center\">";
		print "<form id=\"guest-".$toImport."\" name=\"guest-".$toImport."\">";
		print "<input type=\"hidden\" id=\"guesttype-".$toImport."\" value=\"".$this->input->post('guesttype-import')."\" /> ";
		print "<input type=\"hidden\" id=\"status-".$toImport."\" value=\"".$this->input->post('status-import')."\" /> ";
		print "<input type=\"hidden\" id=\"fee-".$toImport."\" value=\"".$this->input->post('fee-import')."\" />";
		print "<input type=\"hidden\" id=\"filmcompany-".$toImport."\" value=\"".$this->input->post('filmcompany-import')."\" />";
		print "<button id=\"guest-review-".$toImport."\" data-id=\"".$toImport."\" class=\"import\" value=\"".htmlspecialchars($thisGuest[0]->FirstName." ".$thisGuest[0]->LastName)."\">Import</button><br />";
		print "<button id=\"guest-view-".$toImport."\" data-id=\"".$toImport."\" class=\"view\">View</button>";
		print "<input type=\"hidden\" id=\"rowtype-".$toImport."\" name=\"rowtype-".$toImport."\" value=\"".$rowtype."\" />";
		print "</form>";		
		print "</td>\n";
		print "\t\t\t\t<td><strong>".$thisGuest[0]->LastName.", ".$thisGuest[0]->FirstName."</strong></td>\n";
		print "\t\t\t\t<td colspan=\"7\" align=\"center\">Imported by <strong>".$this->session->userdata('username')."</strong> to <strong>".$festivals_array[$values['ImportedFestival']]."</strong> on <strong>".date("m-d-Y",strtotime($values['ImportedDate']))."</strong>.</td>\n";
		print "\t\t\t</tr>\n";
	}

	function send_email_confirmation() {
		// This function sends an email confirmation when a guest is accepted or declined.
		$this->load->model('Guestreviewmodel','guestreview');
		$this->load->model('Usersmodel','users');
		$this->load->library('email');

		$this->email->from($this->input->post('emailfrom-email'));
		$this->email->to($this->input->post('emailto-email'));
		$this->email->subject($this->input->post('subject-email'));
		$this->email->message($this->input->post('message-email'));

		if ( ! $this->email->send()) {
			// Generate error
		} else {
			// Update database with details on message sent
			$emailedBy = $this->input->post("user-email-id");
			$emailedStatus = $this->input->post('guest-email-status');
			$emailedDateTime = date("Y-m-d H:i:s");

			$emailedUsername = "";
			$user_list = $this->users->get_all_users();
			foreach ($user_list as $thisUser) {
				if ($emailedBy == $thisUser->id) { $emailedUsername = $thisUser->Username; }
			}

			$this->guestreview->update_value(
				array("EmailedBy" => $emailedBy,
					"EmailedStatus" => $emailedStatus,
					"EmailedDateTime" => $emailedDateTime
				),
				"fg_guests_staging",
				$this->input->post("guest-email-id")
			);
			print "<img src=\"/assets/images/icons/email.png\" width=\"16\" height=\"16\" /> ".$emailedStatus." by ".$emailedUsername." on: ".date("m-d-Y h:ia",strtotime($emailedDateTime));
		}
	}
	
	function delete_accred() {
		// This function deletes the accreditation form submission, normally only used for duplicates.
		$this->load->model('Guestreviewmodel','guestreview');

		$toDelete = $this->input->post('guest-import-id');
		$this->guestreview->delete_value($toDelete,"fg_guests_staging");
	}

	function update_status() {
		// This function updates the status of this particular accreditation form submission.
		$this->load->model('Guestreviewmodel','guestreview');

		$this->guestreview->update_value(array("DelegateStatus" => $this->input->post("guest-status")),"fg_guests_staging", $this->input->post("guest-id"));		
		print $this->input->post("guest-status");
	}
}

/* End of file guest_review.php */
/* Location: ./system/application/controllers/admin/guest_review.php */
?>