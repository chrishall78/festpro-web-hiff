<?php
if (!defined("FP_BASE_TIMEZONE")) {
	define("FP_BASE_TIMEZONE","Pacific/Honolulu");
}

function convert_specialchars($input) {
	//$output = preg_replace('/\\\u([0-9a-z]{4})/', '&#x$1;', $input);
	$output = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $input);
	$output = str_replace("\r\n", "<br>", $output);
	return $output;
}

function get_xml_from_api_multi($urls, $custom_options = null) {
	// make sure the rolling window isn't greater than the # of urls
	$rolling_window = 5;
	$rolling_window = (sizeof($urls) < $rolling_window) ? sizeof($urls) : $rolling_window;

	// init
	$mh = curl_multi_init();
	$results = array();

	// set options
	$std_options = array(
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_MAXREDIRS => 5);
	$options = ($custom_options) ? ($std_options + $custom_options) : $std_options;

	// loop through $urls and create curl handles then add them to the multi-handle
	for ($i = 0; $i < $rolling_window; $i++) {
		// create a new cURL resource
		$ch = curl_init();

		// set URL and other appropriate options
		$options[CURLOPT_URL] = $urls[$i];
		curl_setopt_array($ch, $options);

		// add to multi handle
		curl_multi_add_handle($mh, $ch);
	}

	$running = null;
	do {
		while (($execrun = curl_multi_exec($mh, $running)) == CURLM_CALL_MULTI_PERFORM);

		if($execrun != CURLM_OK) { break; }

		// a request was just completed -- find out which one
		while($done = curl_multi_info_read($mh)) {
			$info = curl_getinfo($done['handle']);
			if ($info['http_code'] == 200)  {
				$results[] = curl_multi_getcontent($done['handle']);

				if ($i < sizeof($urls)) {
					// start a new request (it's important to do this before removing the old one)
					$ch = curl_init();
					$options[CURLOPT_URL] = $urls[$i++];  // increment i
					curl_setopt_array($ch, $options);
					curl_multi_add_handle($mh, $ch);
				}

				// remove the curl handle that just completed
				curl_multi_remove_handle($mh, $done['handle']);
			} else {
				// request failed.  add error handling.
			}
		}
	} while ($running);

	// close cURL resource, and free up system resources
	curl_multi_close($mh);

	return $results;
}

function get_xml_from_api($url) {
	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	// grab URL and pass it to the browser
	$result = curl_exec($ch);

	// close cURL resource, and free up system resources
	curl_close($ch);

	return $result;
}

function sort_film_titles($a,$b) { return $a['title']>$b['title']; }

function programnamecmp($a, $b) {
	if ($a->program_name == $b->program_name) {
		return 0;
	} else {
		return $a->program_name < $b->program_name ? -1 : 1; // forward order
	}
}

function filmtitlecmp($a, $b) {
	$first = strtoupper($a->title_en);
	$last = strtoupper($b->title_en);
	if ($first == $last) {
		return 0;
	} else {
		return $first < $last ? -1 : 1; // forward order
	}
}

function programcmp($a, $b) {
  if ($a->Program_Title == $b->Program_Title) {
	if ($a->Film_Title == $b->Film_Title) {
		if ($a->Lastname == $b->Lastname) {
			return 0;
		} else {
			return $a->Lastname < $b->Lastname ? 1 : -1;
		}
	} else {
		return $a->Film_Title < $b->Film_Title ? 1 : -1;
	}
  } else {
	return $a->Program_Title < $b->Program_Title ? 1 : -1; // reverse order
  }
}

function programcmp2($a, $b) {
  if ($a->Program_Title == $b->Program_Title) {
	if ($a->Film_Title == $b->Film_Title) {
		if ($a->Lastname == $b->Lastname) {
			return 0;
		} else {
			return $a->Lastname < $b->Lastname ? -1 : 1;
		}
	} else {
		return $a->Film_Title < $b->Film_Title ? -1 : 1;
	}
  } else {
	return $a->Program_Title < $b->Program_Title ? -1 : 1; // forward order
  }
}

function get_tracking_url($courier, $num) {
	$dhl_tracking_url = "http://www.dhl-usa.com/content/us/en/express/tracking.shtml?brand=DHL&AWB=";
	$fedex_tracking_url = "http://fedex.com/Tracking?action=track&cntry_code=us&tracknumber_list=";
	$ups_tracking_url = "http://wwwapps.ups.com/WebTracking/processInputRequest?sort_by=status&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_US&track.x=0&track.y=0&InquiryNumber1=";
	$usps_tracking_url = "http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum=";
	
	switch ($courier) {
		case "DHL": return $dhl_tracking_url.$num; break;
		case "FedEx": return $fedex_tracking_url.$num; break;
		case "UPS": return $ups_tracking_url.$num; break;
		case "USPS": return $usps_tracking_url.$num; break;
	}
}

function findNumberOfPrograms($schedule) {
	$idArray = array(); $program_number = 0;
	foreach ($schedule as $thisScreening) {
		if ($thisScreening->program_name != "") {
			$idArray[] = $thisScreening->program_name;
		} else {
			$title = strtolower($thisScreening->title_en);
			if ($title == "tba" || $title == "to be announced" || $title == "tbd" || $title == "to be determined") {
				$idArray[] = $thisScreening->title_en.rand(0,100000);
			} else {
				$idArray[] = $thisScreening->title_en;
			}
		}
	}
	$uniqueArray = array_unique($idArray);
	$program_number = count($uniqueArray);
	return $program_number;
}

// this checks to see if a certain directory exists, if not it tries to create it.
function return_img_dir($name, $year, $subdirs = array()) {
	// remove these characters
	$strip = array('!','@','#','$','%','^','&','*','(',')','+','=','{','}',':',';','"','\'','<','>',',','.','?','/','|','\\','~','`');
	$festname_temp = str_replace($strip, "", strtolower($name));

	// replace dash or space with underscore
	$search = array('-',' ');
	$festname_temp = str_replace($search, "_", $festname_temp);
	
	$img_dir = "assets/images/".$year."_".$festname_temp;
	$subdirs[] = "thumbs";
	$subdirs[] = "sponsors";
	$subdirs[] = "delegates";
	
	if (!is_dir($img_dir)) {
		if (!mkdir($img_dir,0755)) {
			die('Failed to create image directory: '.$img_dir);
		} else {
			foreach ($subdirs as $thisSubdir) {
				$directory = "assets/images/".$year."_".$festname_temp."/".$thisSubdir;
				if (!is_dir($directory)) {
					if (!mkdir($directory,0755)) {
						die('Failed to create '.$thisSubdir.' directory: '.$directory);
					}
				}
			}
			return $img_dir;
		}
	} else {
		foreach ($subdirs as $thisSubdir) {
			$directory = "assets/images/".$year."_".$festname_temp."/".$thisSubdir;
			if (!is_dir($directory)) {
				if (!mkdir($directory,0755)) {
					die('Failed to create '.$thisSubdir.' directory: '.$directory);
				}
			}
		}
		return $img_dir;
	}
}


function create_slug($title_en, $date = "") {
	if ($date == "") { $date = date("Y"); }

	// replace dash or space with underscore
	$newslug = str_replace(array('-',' '), "_", $title_en);	
	// remove all characters except a-z, A-Z, 0-9, underscore
	$slug = ""; $slug_array = str_split(strtolower(strip_tags($newslug)));
	foreach ($slug_array as $char) { if (preg_match("/[\w]/",$char) == 1) { $slug .= $char; } }
	// limit slug to 40 characters and make lowercase
	$slug = substr($slug,0,39);
	// strip any trailing underscores
	$slug = trim($slug,"_");
	// add the current year
	$slug = $slug."_".$date;
	
	return $slug;
}

function create_simple_slug($name) {
	// replace dash or space with underscore
	$newslug = str_replace(array('-',' '), "_", $name);	
	// remove all characters except a-z, A-Z, 0-9, underscore
	$slug = ""; $slug_array = str_split(strtolower(strip_tags($newslug)));
	foreach ($slug_array as $char) { if (preg_match("/[\w]/",$char) == 1) { $slug .= $char; } }
	// limit slug to 40 characters and make lowercase
	$slug = substr($slug,0,39);
	// strip any trailing underscores
	$slug = trim($slug,"_");
	
	return $slug;
}


function adjust_title($title_en) {
	// rewrite this to take 'THE X' and turn it into 'X, THE'
	$title_en = strtoupper(stripslashes($title_en));
	$temp_title = "";
	if ($title_en != "") {
		if (substr($title_en,0,2) == "A ") {
			$temp_title = substr($title_en,2).", A";
		}
		if (substr($title_en,0,3) == "AN ") {
			$temp_title = substr($title_en,3).", AN";
		}
		if (substr($title_en,0,4) == "THE ") {
			$temp_title = substr($title_en,4).", THE";
		}

		if ($temp_title != "") {
			return $temp_title;
		} else {
			return $title_en;
		}
	}
	return $title_en;
}

function form_dropdown_color($fieldname, $array, $selected, $attributes) {
	print "<select name=\"".$fieldname."\" ".$attributes.">";
	foreach ($array as $key => $value) {
		print "<option value=\"".$value['id']."\" title=\"".$value['color']."\">".$value['name']."</option>";
	}
	print "</select>";
}

function form_dropdown_film($fieldname, $array, $selected, $attributes) {
	print "<select name=\"".$fieldname."\" ".$attributes.">";
	foreach ($array as $key => $value) {
		print "<option value=\"".$value['movie_id']."\" title=\"".$value['runtime']."\"";
		if ($selected == $value['movie_id']) { print " selected=\"selected\" "; }
		print ">".$value['title_en']."</option>";
	}
	print "</select>";
}

function switch_title($title) {
	$title = stripslashes($title);
	$newtitle = "";
	
	$checkArray = array(
		", A" => "A ", ",A" => "A ",
		", AN" => "AN ", ",AN" => "AN ",
		", THE" => "THE ", ",THE" => "THE "
	);

	if ($title != "") {
		foreach ($checkArray as $key => $value) {
			$len = strlen($key);
			$film_len = strlen($title)-$len;
			if ( substr($title,$film_len,$len) == $key ) {
				return $value.substr($title,0,$film_len);
			}			
		}
		
		// If we get through all of the items above and no match
		return $title;
	}
}

function generate_year_array() {
	$year_array = array("0"=>"--");
	$start = 1900;
	$current = date("Y");

	for($x=$current; $x>=$start; $x--) {
		$year_array[$x] = $x;
	}
	return $year_array;
}

function convert_multiselect_to_string($array) {
	$output = "";
	if (count($array) > 0) {
		foreach ($array as $value) {
			$output .= $value.",";
		}
	}
	return trim($output,",");
}

function convert_string_to_array($string) {
	$array = explode(",",$string);
	return $array;
}

function convert_obj_to_array($object_list, $key, $value_array, $sep = " ", $first_entry = null) {
	if ($first_entry == null) {
		$return_array = array();
	} else {
		$return_array = array(0 => $first_entry);
	}
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$array_string = "";
			foreach ($value_array as $thisValue) {
				$array_string .= $thisObject->{"$thisValue"}.$sep;
			}
			$return_array[$thisObject->{"$key"}] = $array_string;
		}
	}
	return $return_array;
}

function convert_to_array($object_list, $key = "id", $value = "name", $first_entry = true) {
	if ($first_entry == true) {
		$return_array = array(0 => "--");
	} else if ($first_entry == false) {
		$return_array = array();
	} else {
		$return_array = array(0 => $first_entry);
	}
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			if ($key != null) {
				$return_array[$thisObject->{"$key"}] = $thisObject->{"$value"};
			} else {
				$return_array[] = $thisObject->{"$value"};
			}
		}
	}
	return $return_array;
}

function convert_to_array2($object_list) {
	return convert_to_array($object_list, "id", "name", false);
}

function convert_to_array3($object_list) {
	return convert_to_array($object_list, null, "movie_id", false);
}

function convert_to_array_all($object_list) {
	return convert_to_array($object_list, "slug", "name", "All");
}

function convert_films_to_array($object_list) {
	return convert_to_array($object_list, "movie_id", "title_en", true);
}

function convert_films_to_array2($object_list) {
	return convert_to_array($object_list, "id", "title_en", true);
}

function convert_films_to_array3($object_list) {
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->movie_id] = switch_title($thisObject->title_en);
		}
	}
	if (count($object_list) == 0) { $return_array[0] = "PLEASE ADD A FILM BEFORE CONTINUING."; }
	return $return_array;
}

function convert_film_screenings_to_array($object_list) {
	$return_array[] = array("movie_id"=>"0","title_en"=>"--","runtime"=>"0");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[] = array("movie_id"=>$thisObject->movie_id,"title_en"=>switch_title($thisObject->title_en),"runtime"=>$thisObject->runtime_int);
		}
	}
	return $return_array;
}

function convert_to_array_slug($object_list, $all_value = "--") {
	if ($all_value != "--") {
		$plural = $all_value;
		if (substr($all_value, -1) == "y") { $plural = substr($all_value, 0, -1)."ie"; }
		$return_array = array(0 => "By ".$all_value, "all" => "-- ALL ".strtoupper($plural)."S --");
	} else {
		$return_array = array(0 => "--");
	}
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->slug] = $thisObject->name;
		}
	}
	return $return_array;
}

function convert_to_array_schedule($object_list, $key = "date", $value = "date", $date = "l, F jS", $first_entry = false) {
	if ($first_entry == false) {
		$return_array = array();
	} else {
		$return_array = array(0 => $first_entry);
	}
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->{"$key"}] = date($date,strtotime($thisObject->{"$value"}));
		}
	}
	return $return_array;
}

function convert_to_array_schedule2($object_list) {
	return convert_to_array_schedule($object_list, "date", "date", "Y-m-d", "All Dates");
}

function convert_to_array_schedule3($object_list) {
	return convert_to_array_schedule($object_list, "date", "date", "Y-m-d", false);
}

function convert_festival_to_array($object_list) {
	$return_array = array(0 => "All Festivals");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->id] = $thisObject->year." ".$thisObject->name;
		}
	}
	return $return_array;
}

function convert_festival_to_array2($object_list) {
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->id] = $thisObject->year." ".$thisObject->name;
		}
	}
	return $return_array;
}

function convert_color_to_array($object_list) {
	$return_array[] = array("id"=>"0","name"=>"--","color"=>"FFFFFF");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[] = array("id"=>$thisObject->id,"name"=>$thisObject->name,"color"=>$thisObject->color);
		}
	}
	return $return_array;
}

// Return array [flightcomp_id] => [airline comp_name (mileage)] 
function convert_flightcomp_to_array($object_list) {
	$return_array = array(0 => "--");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->id] = $thisObject->airline." ".$thisObject->destination." (".number_format($thisObject->mileage).")";
		}
	}
	return $return_array;
}

// Return array [hotelcomp_id] => [hotelroom (remaining)] 
function convert_hotelcomp_to_array($object_list, $used_rooms) {
	$return_array = array(0 => "--");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$limit = $thisObject->limit;
			foreach($used_rooms as $thisRoom) {
				if ($thisObject->id == $thisRoom->hotelcomp_id) { $limit = $limit - $thisRoom->compnights; }
			}
	
			$return_array[$thisObject->id] = $thisObject->name." (".$limit." left)";
		}
	}
	return $return_array;
}

// Return array [event_id] => [name date time] 
function convert_events_to_array($object_list) {
	$return_array = array(0 => "--");
	if (count($object_list) > 0) {
		foreach ($object_list as $thisObject) {
			$return_array[$thisObject->id] = $thisObject->location." (".date("n-j-Y",strtotime($thisObject->date))." - ".date("g:i A",strtotime($thisObject->time)).")";
		}
	}
	return $return_array;
}

function convert_to_simple_array($multi_array) {
	if (count($multi_array) > 0) {
		foreach ($multi_array as $thisArray) {
			$return_array[$thisArray['id']] = $thisArray['name'];
		}
	}
	return $return_array;
}


function convert_number_to_date($number) {
	return substr($number,0,4)."-".substr($number,4,2)."-".substr($number,6,2);
}

// Runtime in minutes converted to 00:00:00 time type
function convert_min_to_time($runtime_temp) {
	$hr=0; $min=0; $z="0";
	while ($runtime_temp >= 60) { $hr++; $runtime_temp = $runtime_temp-60;}
	$min = $runtime_temp;
	$total_runtime = "";
	$hr < 10 ? $total_runtime .= $z.$hr.":" : $total_runtime .= $hr.":";
	$min < 10 ? $total_runtime .= $z.$min.":00" : $total_runtime .= $min.":00";
		
	return $total_runtime;
}

// Runtime in 00:00:00 time type converted to minutes
function convert_time_to_min($hours) {
	$minutes = 0;
	if (strpos($hours, ':') !== false)
	{
		// Split hours and minutes.
		list($hours, $minutes) = explode(':', $hours);
	}
	return $hours * 60 + $minutes;
}


function calculate_runtime($num) {
	$tempstring = "";
	$hourcount = 0;
	while ($num >= 60) { $num = $num - 60; $hourcount++; }
	
	if ($hourcount > 0) { $tempstring .= $hourcount." hours"; }
	if ($hourcount > 0 && $num != 0 && $num < 60) { $tempstring .= " "; }
	if ($num != 0 && $num < 60) { $tempstring .= $num." mins";}
	
	return $tempstring;
}

function print_type_AUD($array, $label, $fieldname) {
	print "<form name=\"add-".$fieldname."\" id=\"add-".$fieldname."\">\n";
	print "<h3>".$label."</h3>\n";
	print form_input(array('name'=>$fieldname.'-new', 'id'=>$fieldname.'-new', 'class'=>'text ui-widget-content ui-corner-all'))."&nbsp;&nbsp;&nbsp;";
	print "<button name=\"".$fieldname."-add\" id=\"".$fieldname."-add\">Add</button>";
	print form_close();"\n";
	print "<br />";

	print "<form name=\"update-".$fieldname."\" id=\"update-".$fieldname."\">\n";
	print "<label for=\"".$fieldname."\">".$label." List</label><br />";
	print form_dropdown($fieldname, $array, 0, "id='".$fieldname."' class='select ui-widget-content ui-corner-all'");
	if ($fieldname == "personnel") {
		print "&nbsp;&nbsp;&nbsp;<button id=\"order_personnel_button\">Re-Order</button>";
	}
	print "&nbsp;&nbsp;&nbsp;<img id=\"".$fieldname."-delete\" class=\"clickable\" src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected ".$label."\" title=\"Delete selected ".$label."\" width=\"16\" height=\"16\" border=\"0\"><br />\n";
	print form_input(array('name'=>$fieldname.'-current', 'id'=>$fieldname.'-current', 'class'=>'text ui-widget-content ui-corner-all'))."&nbsp;&nbsp;&nbsp;";
	print "<button name=\"".$fieldname."-update\" id=\"".$fieldname."-update\">Update</button>";
	print form_close();"\n";
}

function print_type_video_AUD($array, $label, $fieldname) {
	$videoformat_array = convert_color_to_array($array);
	
	print "<form name=\"add-".$fieldname."\" id=\"add-".$fieldname."\">\n";
	print "<h3>".$label."</h3>\n";
	print form_input(array('name'=>$fieldname.'-new', 'id'=>$fieldname.'-new', 'class'=>'text ui-widget-content ui-corner-all'))."&nbsp;&nbsp;";
	print "<div class=\"customWidget addColor\"><div id=\"colorSelector\"><div style=\"background-color: rgb(0, 0, 0);\"></div></div></div>";
	print "<input type=\"hidden\" value=\"000000\" id=\"".$fieldname."-color\" name=\"".$fieldname."-color\">&nbsp;&nbsp;";
	print "<button name=\"".$fieldname."-add\" id=\"".$fieldname."-add\">Add</button>";
	print form_close();"\n";
	print "<br />";

	print "<form name=\"update-".$fieldname."\" id=\"update-".$fieldname."\">\n";
	print "<label for=\"".$fieldname."\">".$label." List</label><br />";
	print form_dropdown_color($fieldname, $videoformat_array, 0, "id='".$fieldname."' class='select-video ui-widget-content ui-corner-all'");	
	print "&nbsp;&nbsp;&nbsp;<img id=\"".$fieldname."-delete\" class=\"clickable\" src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected ".$label."\" title=\"Delete selected ".$label."\" width=\"16\" height=\"16\" border=\"0\"><br />\n";
	print form_input(array('name'=>$fieldname.'-current', 'id'=>$fieldname.'-current', 'class'=>'text ui-widget-content ui-corner-all'))."&nbsp;&nbsp;";
	print "<div class=\"customWidget updateColor\"><div id=\"colorSelector2\"><div style=\"background-color: rgb(0, 0, 0);\"></div></div></div>";
	print "<input type=\"hidden\" value=\"000000\" id=\"".$fieldname."-color-update\" name=\"".$fieldname."-color-update\">&nbsp;&nbsp;";
	print "<button name=\"".$fieldname."-update\" id=\"".$fieldname."-update\">Update</button>";
	print form_close();"\n";
}

function print_type_splogo_AUD($array, $label, $fieldname) {
	$splogo_array = convert_to_array($array);
	$max_order = 0;	foreach ($array as $item) {	if ($item->order >= $max_order) { $max_order = $item->order; } } $max_order++;
	
	print "<h3>Add a ".$label."</h3>\n";
	
	print "<form name=\"add-".$fieldname."\" id=\"add-".$fieldname."\">";
	print "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
	print "<thead><th style=\"border:0;\"><label for=\"".$fieldname."-name-new\">Name</label><span class=\"req\"> *</span></th><th style=\"border:0;\"><label for=\"".$fieldname."-desc-new\">Description</label><span class=\"req\"> *</span></th><th style=\"border:0;\"><label for=\"".$fieldname."-order-new\">Order</label><span class=\"req\"> *</span></th><th style=\"border:0;\">&nbsp;</th></thead>\n";
	print "<tbody style=\"border:none;\"><tr valign=\"top\"><td>\n";	
	print form_input(array('name'=>$fieldname.'-name-new', 'id'=>$fieldname.'-name-new', 'class'=>'text-small ui-widget-content ui-corner-all'))."</td><td>";
	print form_input(array('name'=>$fieldname.'-desc-new', 'id'=>$fieldname.'-desc-new', 'class'=>'text-small ui-widget-content ui-corner-all'))."</td><td>";
	print form_input(array('name'=>$fieldname.'-order-new', 'id'=>$fieldname.'-order-new', 'class'=>'text-smaller ui-widget-content ui-corner-all', 'value'=>$max_order))."</td><td>";
	print "<button name=\"".$fieldname."-add\" id=\"".$fieldname."-add\">Add</button>";
	print "</td></tr>";
	print "</table>";
	print form_close();"\n";

	print "<h3>Edit a ".$label."</h3>\n";

	print "<form name=\"update-".$fieldname."\" id=\"update-".$fieldname."\">";
	print "\n<br /><label for=\"".$fieldname."\">Current ".$label." List</label><span class=\"req\"> *</span><br />";
	print form_dropdown($fieldname, $splogo_array, 0, "id='".$fieldname."' class='select-small ui-widget-content ui-corner-all'");
	//print "&nbsp;&nbsp;&nbsp;<a href=\"#\"><img src=\"/assets/images/icons/cancel.png\" alt=\"Delete selected ".$label."\" title=\"Delete selected ".$label."\" width=\"16\" height=\"16\" border=\"0\"></a>\n";

	print "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
	print "<thead><th style=\"border:0;\"><label for=\"".$fieldname."-name-current\">Name</label><span class=\"req\"> *</span></th><th style=\"border:0;\"><label for=\"".$fieldname."-desc-current\">Description</label><span class=\"req\"> *</span></th><th style=\"border:0;\"><label for=\"".$fieldname."-order-current\">Order</label><span class=\"req\"> *</span></th><th style=\"border:0;\">&nbsp;</th></thead>\n";
	print "<tbody style=\"border:none;\"><tr valign=\"top\"><td>\n";	
	print form_input(array('name'=>$fieldname.'-name-current', 'id'=>$fieldname.'-name-current', 'class'=>'text-small ui-widget-content ui-corner-all'))."</td><td>";
	print form_input(array('name'=>$fieldname.'-desc-current', 'id'=>$fieldname.'-desc-current', 'class'=>'text-small ui-widget-content ui-corner-all'))."</td><td>";
	print form_input(array('name'=>$fieldname.'-order-current', 'id'=>$fieldname.'-order-current', 'class'=>'text-smaller ui-widget-content ui-corner-all'))."</td><td>";
	print "<button name=\"".$fieldname."-update\" id=\"".$fieldname."-update\">Update</button>";
	print "</td></tr>";
	print "</table>";

	print form_close();"\n";
}

// Requires Oembed library to be loaded
function get_video_object($video) {
	switch ($video->service_name) {
		case "hulu":
		case "revision3":
		case "qik":
		case "viddler":
		case "vimeo":
		case "youtube": $video_object = $video->oembed->call($video->service_name, $video->url_video);
						break;
	
		case "bliptv":
		case "dailymotion":
		case "googlevideo":
		case "metacafe": $video_object = $video->oembed->call('oohembed', $video->url_video);
						 break;
	}
	
	return $video_object;
}

function print_report_preview($report, $button, $url, $venue = "", $date = "") {
	$report_keys = array();
	foreach ($report as $thisRecord) {
		$report_vars = get_object_vars($thisRecord);
		$report_keys = array_keys($report_vars);
		break;
	}
	
	if ($venue != "") {
		$name = "export-thops-".create_slug($venue);
		$btnname = $name."-btn";
		$desc = "Theater Operations Schedule (".$venue." - ".count($report)." records)";
		
		print "<form id=\"".$name."\" method=\"post\" action=\"/admin/film_reports/preview_theater_ops2/".$date."/download/\">";
		print "<b>".$desc."</b> <button id=\"".$btnname."\">Export</button><br />\n";
		print "</form>";
	}

	print "<fieldset class=\"ui-corner-all\" style=\"background-color:#FFF;\">\n";
	print "<div style=\"width:1019px; overflow:auto;\">\n";
	print "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0;\">\n";
	print "<thead><tr>\n";
	foreach ($report_keys as $headerName) {
		print "<th>".$headerName."</th>";
	}
	print "</tr></thead>\n";
	print "<tbody style=\"border:none;\">\n";
	
	$recordCount = 0; $totalRecords = count($report);

	if ($totalRecords == 0) {
		print "<tr valign=\"top\">";
		print "<td colspan=\"".count($report_keys)."\"><b>Sorry, there are no results for this report.</b></td>\n";
		print "</tr>\n";
	} else {
		foreach ($report as $thisRecord) {
			if ($recordCount < 10) {
				$report_vars = get_object_vars($thisRecord);
				print "<tr valign=\"top\">";
				foreach ($report_keys as $cell) {
					switch ($cell) {
						case 'ShortSynopsis':
						case 'LongSynopsis':
						case 'Director':
						case 'Producer':
						case 'Cinematographer':
						case 'Scriptwriter':
						case 'Cast':
						case 'Bio':
							print "<td>".substr(trim(strip_tags($report_vars[$cell])), 0, 50)."</td>\n";
							break;
	
						default:
							print "<td>".$report_vars[$cell]."</td>\n";
							break;
					}
				}
				print "</tr>\n";
				$recordCount++;
				
			} else if ($recordCount == $totalRecords || $recordCount == 10) {
				print "<tr valign=\"top\">";
				print "<td colspan=\"".count($report_keys)."\"><b>".($totalRecords - $recordCount)." more records are available when you export the full report. Some fields are shortened for preview purposes.</b></td>\n";
				print "</tr>\n";
				break;
			}		
		}
	}
	print "</tbody></table></div></fieldset><br>\n";

	print "<script type=\"text/javascript\">\n";
	print "\t$(function() {\n";
	print "\t\t$(\"#".$button."\").button().on('click', function() {";
	print "\t\t\t$(\"#".$url."\").submit();\n";
	print "\t\t\t$(this).removeClass(\"ui-state-focus\");\n";
	print "\t\t\treturn false;\n";
	print "\t\t});\n";
	print "\t});\n";
	print "</script>\n";

}

function print_airport_select($domestic, $intl, $name, $selected, $class = "") {
	print "<select name=\"".$name."\" id=\"".$name."\" class=\"".$class."\">\n";
	if ($selected == "") {
		print "\t<option value=\"\" selected=\"selected\">Choose an Airport</option>\n";
	} else {
		print "\t<option value=\"\">Choose an Airport</option>\n";
	}
	print "\t<optgroup label=\"USA/Canada\">\n";
	foreach($domestic as $domesticAir) {
		print "\t\t<option value=\"".$domesticAir->code."\"";
		if ($domesticAir->code == $selected) { print " selected=\"selected\""; }
		print ">".$domesticAir->name."</option>\n";
	}
	print "\t</optgroup>\n";
	print "\t<optgroup label=\"International\">\n";
	foreach($intl as $intlAir) {
		print "\t\t<option value=\"".$intlAir->code."\"";
		if ($intlAir->code == $selected) { print " selected=\"selected\""; }
		print ">".$intlAir->name."</option>\n";
	}
	print "\t</optgroup>\n";
	print "</select>\n";
}

function createDateRangeArray($strDateFrom,$strDateTo) {
  // takes two dates formatted as YYYY-MM-DD and creates an
  // inclusive array of the dates between the from and to dates.

  $aryRange=array();

  $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2), substr($strDateFrom,8,2),substr($strDateFrom,0,4));
  $iDateTo=mktime(1,0,0,substr($strDateTo,5,2), substr($strDateTo,8,2),substr($strDateTo,0,4));

  if ($iDateTo>=$iDateFrom) {
	array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry

	while ($iDateFrom<$iDateTo) {
	  $iDateFrom+=86400; // add 24 hours
	  array_push($aryRange,date('Y-m-d',$iDateFrom));
	}
  }
  return $aryRange;
}

function calculateTypeSummary($festivalType, $festivalFilms, $eventId) {
	$resultsArray = $tempArray = array();
	$typeArray = convert_to_array2($festivalType);
	$total = $arrayCount = 0;

	foreach ($typeArray as $thisType => $name) {
		$count = 0;
		$filmString = "";
		foreach ($festivalFilms as $thisFilm) {
			if ($thisFilm->$eventId == $thisType) {
				$filmString = $filmString.switch_title($thisFilm->title_en)."; ";
				$count++;
			}
		}
		if ($count > 0) {
			if (strlen($filmString) > 2) { $filmString = substr($filmString, 0, -2); }
			$resultsArray[$arrayCount]["name"] = $name;
			$resultsArray[$arrayCount]["count"] = $count;
			$resultsArray[$arrayCount]["films"] = $filmString;
			$arrayCount++;
		}
	}

	return $resultsArray;
}

function countTypeByName($typeArray) {
	$array = array();
	foreach ($typeArray as $thisObject) {
		if ( !isset($array[$thisObject->name]) ) {
			$array[$thisObject->name] = 1;
		} else {
			$array[$thisObject->name]++;
		}
	}
	return $array;
}


/* End of file helper_functions.php */
/* Location: ./system/application/controllers/admin/helper_functions.php */
?>