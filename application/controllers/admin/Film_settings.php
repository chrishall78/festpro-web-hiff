<?php
class Film_Settings extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");
	}
	
	function index()
	{
		if ($this->session->userdata('limited') == 0) {
			$this->load->helper('form');
	
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Filmsmodel','films');
			$this->load->model('Filmtypesmodel','filmtype');
			$this->load->model('Sectionmodel','section');
			$this->load->model('Locationmodel','location');
			$this->load->model('Sponsormodel','sponsor');
			$this->load->model('Competitionmodel','competition');
	
			$data['festivalfront'] = $this->festival->get_current_festival_front();		
			$data['festival'] = $this->festival->get_current_festival();
			$data['festivaladmin'] = $data['festival'];
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}

			$data['countries'] = $this->filmtype->get_all_type("country","asc");
			$data['languages'] = $this->filmtype->get_all_type("language","asc");
			$data['genres'] = $this->filmtype->get_all_type("genre","asc");
	
			$data['aspectratio'] = $this->filmtype->get_all_type("aspectratio","asc");
			$data['color'] = $this->filmtype->get_all_type("color","asc");
			$data['distribution'] = $this->filmtype->get_all_type("distribution","asc");

			$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
			$data['courier'] = $this->filmtype->get_all_type("courier","asc");
			$data['personnel'] = $this->filmtype->get_all_type("personnel","asc");
			$data['personnel_order'] = $this->filmtype->get_all_type_order("personnel","asc");

			$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
			$data['soundformat'] = $this->filmtype->get_all_type("sound","asc");
			$data['sponsorlogo'] = $this->filmtype->get_all_type("sponsorlogo","asc");

			$data['videoformat'] = $this->filmtype->get_all_type("format","asc");	

			$data['films'] = $this->films->get_all_internal_films_sort($current_fest,"wf_movie.title_en", "asc",0,9999);
			$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
	
			$data['sections'] = $this->section->get_all_sections($current_fest);
			$data['sponsors'] = $this->sponsor->get_all_sponsors();
			$data['sponsor_logos'] = $this->sponsor->get_all_sponsor_logos("0", $current_fest);

			$data['locations'] = $this->location->get_all_locations();
			$data['venues'] = $this->location->get_all_venues();
			$data['festivals'] = $this->festival->get_all_festivals();

			$data['competitions'] = $this->competition->get_all_competitions();
			$data['competition_films'] = $this->competition->get_all_competition_films();
	
			$vars['title'] = "Film Settings";
			$vars['path'] = "/";
			$vars['selected_page'] = "film_settings";
			$vars['filmJSON'] = json_encode($data['filmids']);
			$vars['festivals'] = $this->festival->get_all_festivals();
	
			$this->load->view('admin/header', $vars);
			$this->load->view('admin/nav_bar', $vars);
			$this->load->view('admin/film_settings', $data);
			$this->load->view('admin/footer', $vars);
		} else {
			$this->load->helper('url');
			redirect('/admin/film_listing', 'refresh');
		}
	}

	// Film Types
	function add_type($type)
	{
		$this->load->model('Filmtypesmodel','filmtype');

		$value = $this->input->post($type.'-new');

		$slug = ""; $slug = create_simple_slug($value);
		$counter = 2; $empty = true; $base_slug = $slug;

		switch($type) {
			case "event":
			case "language":
			case "genre":
			case "country":
			while ($empty) {
				$empty = $this->filmtype->check_filmtype_slug($slug,"wf_type_".$type);
				if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
			} break;
			default: break;
		}

		if ($this->input->post($type.'-color') != "") {
			$color = $this->input->post($type.'-color');
		} else {
			$color = 0;
		}
		
		$new_id = $this->filmtype->add_type_value($type, $value, $color, $slug);
		print "<option value=\"".$new_id."\">".$value."</option>";
	}

	function update_type($type)
	{
		$this->load->model('Filmtypesmodel','filmtype');

		$id = $this->input->post($type);
		$value = $this->input->post($type.'-current');

		$slug = ""; $slug = create_simple_slug($value);
		$counter = 2; $empty = true; $base_slug = $slug;
		switch($type) {
			case "event":
			case "language":
			case "genre":
			case "country":
			while ($empty) {
				$empty = $this->filmtype->check_filmtype_slug($slug,"wf_type_".$type);
				if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
			} break;
			default: break;
		}

		if ($this->input->post($type.'-color-update') != "") {
			$color = $this->input->post($type.'-color-update');
		} else {
			$color = 0;		
		}

		$this->filmtype->update_type_value($type, $value, $color, $slug, $id);
		print $type." / ".$value." / ".$color." / ".$slug." / ".$id." / ".$this->input->post($type.'-color-update');
	}

	function delete_type($type)
	{
		$this->load->helper('form');
		$this->load->model('Filmtypesmodel','filmtype');

		$res_count = 9999;
		$id = $this->input->post($type);
		switch($type) {
			case "country":
			case "language":
			case "genre":
				$results = $this->filmtype->check_clg($type, $id);
				$res_count = count($results);

				if ($res_count > 0) {
					$type_array = convert_to_array($this->filmtype->get_type_except($type, $id, "asc"));

					print "<p><strong>".$results[0]->name."</strong> is associated with <strong>".$res_count."</strong> films. If you wish to delete it, please select a replacement value below to assign to these films.</p>\n";
					print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
					print "\t<tbody style=\"border-top:none;\">\n";
					print "\t\t<tr valign=\"top\">\n";
					print "\t\t\t<td width=\"50%\"><label for=\"replacement-value\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
					print "\t\t\t<td width=\"50%\">";
					print form_dropdown('replacement-value', $type_array, 0, "id='replacement-value' class='select ui-widget-content ui-corner-all'");
					print "</td>\n";
					print "\t\t</tr>\n";
					print "\t</tbody>\n";
					print "</table>\n";
				} else {
					print "0";
				}
				break;

			case "aspectratio":
			case "color":
			case "distribution":
			case "sound":
			case "event":
			case "premiere":
			case "format":
				$results = $this->filmtype->check_movie_value($type, $id);
				$res_count = count($results);

				if ($type == "format") {
					// TODO - also check wf_screeninglocation 'format' field
				}

				if ($res_count > 0) {
					$type_array = convert_to_array($this->filmtype->get_type_except($type, $id, "asc"));

					print "<p><strong>".$results[0]->name."</strong> is associated with <strong>".$res_count."</strong> films. If you wish to delete it, please select a replacement value below to assign to these films.</p>";
					print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
					print "\t<tbody style=\"border-top:none;\">\n";
					print "\t\t<tr valign=\"top\">\n";
					print "\t\t\t<td width=\"50%\"><label for=\"replacement-value\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
					print "\t\t\t<td width=\"50%\">";
					print form_dropdown('replacement-value', $type_array, 0, "id='replacement-value' class='select ui-widget-content ui-corner-all'");
					print "</td>\n";
					print "\t\t</tr>\n";
					print "\t</tbody>\n";
					print "</table>\n";
				} else {
					print "0";
				}
				break;

			case "courier":
				$results = $this->filmtype->check_shipment_value($type, $id);
				$res_count = count($results);

				if ($res_count > 0) {
					$type_array = convert_to_array($this->filmtype->get_type_except($type, $id, "asc"));

					print "<p><strong>".$results[0]->name."</strong> is associated with <strong>".$res_count."</strong> shipments. If you wish to delete it, please select a replacement value below to assign to these shipments.</p>";
					print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
					print "\t<tbody style=\"border-top:none;\">\n";
					print "\t\t<tr valign=\"top\">\n";
					print "\t\t\t<td width=\"50%\"><label for=\"replacement-value\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
					print "\t\t\t<td width=\"50%\">";
					print form_dropdown('replacement-value', $type_array, 0, "id='replacement-value' class='select ui-widget-content ui-corner-all'");
					print "</td>\n";
					print "\t\t</tr>\n";
					print "\t</tbody>\n";
					print "</table>\n";
				} else {
					print "0";
				}
				break;

			case "personnel":
				$results = $this->filmtype->check_personnel_value($type, $id);
				$res_count = 0;
				// Check for false positive results (i.e '1' matching '10')
				foreach($results as $thisResult) {
					$roles = explode(",",$thisResult->personnel_roles);
					foreach ($roles as $thisRole) {
						if ($thisRole == $id) { $res_count++; }
					}
				}

				if ($res_count > 0) {
					$type_name = convert_to_array($this->filmtype->get_type($type, $id));
					$type_array = convert_to_array($this->filmtype->get_type_except($type, $id, "asc"));

					print "<p><strong>".$type_name[$id]."</strong> is associated with <strong>".$res_count."</strong> people. If you wish to delete it, please select a replacement value below to assign to these people.</p>";
					print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
					print "\t<tbody style=\"border-top:none;\">\n";
					print "\t\t<tr valign=\"top\">\n";
					print "\t\t\t<td width=\"50%\"><label for=\"replacement-value\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
					print "\t\t\t<td width=\"50%\">";
					print form_dropdown('replacement-value', $type_array, 0, "id='replacement-value' class='select ui-widget-content ui-corner-all'");
					print "</td>\n";
					print "\t\t</tr>\n";
					print "\t</tbody>\n";
					print "</table>\n";
				} else {
					print "0";
				}
				break;

			default: break;
		}

		// No results found, so it's okay to delete this value.
		if ($res_count == 0) {
			$this->filmtype->delete_type_value($type, $id);
		}
	}

	function delete_type_replace($type, $id_delete)
	{
		$this->load->model('Filmtypesmodel','filmtype');

		// Replace all instances of old id with new id
		$result = 0;
		$replacement_success = false;
		$id_replace = $this->input->post("replacement-value");

		switch($type) {
			case "country":
			case "language":
			case "genre":
				$result = $this->filmtype->replace_clg_value($type, $id_delete, $id_replace);
				if ($result != 0) { $replacement_success = true; }
				break;

			case "aspectratio":
			case "color":
			case "distribution":
			case "sound":
			case "event":
			case "premiere":
			case "format":
				$result = $this->filmtype->replace_movie_value($type, $id_delete, $id_replace);
				if ($result != 0) { $replacement_success = true; }
				break;

			case "courier":
				$result = $this->filmtype->replace_shipment_value($type, $id_delete, $id_replace);
				if ($result != 0) { $replacement_success = true; }
				break;

			case "personnel":
				$results = $this->filmtype->check_personnel_value($type, $id_delete);
				$res_array = array();
				// Check for false positive results (i.e '1' matching '10')
				foreach($results as $thisResult) {
					$res_count = 0;
					$roles = explode(",",$thisResult->personnel_roles);
					foreach ($roles as $thisRole) {
						if ($thisRole == $id_delete) { $res_count++; }
					}
					if ($res_count > 0) {
						$res_array[$thisResult->id] = $thisResult->personnel_roles;
					}
				}

				$personnel_updates = 0;
				foreach ($res_array as $personnel_id => $old_roles) {
					$new_roles = $id_replace.",";
					$roles = explode(",",$old_roles);
					foreach ($roles as $thisRole) {
						if ($thisRole != $id_delete) {
							$new_roles .= $thisRole.",";
						}
					} $new_roles = trim($new_roles,",");

					$result = 0;
					$result = $this->filmtype->replace_personnel_value($personnel_id, $id_replace, $new_roles);
					if ($result != 0) { $personnel_updates++; }
				}
				if ($personnel_updates != 0) { $replacement_success = true; }
				break;

			default: break;
		}

		// If replacement was successful, remove old id
		if ($replacement_success == true) {
			$this->filmtype->delete_type_value($type, $id_delete);
		}
	}


	function update_personnel_order() {
		$this->load->model('Filmtypesmodel','filmtype');
		$order_array = explode(",",$this->input->post("personnel_order"));	
		for ($x = 0; $x < count($order_array); $x++) {
			$upd_id = $this->filmtype->update_type_order("personnel", $x+1, $order_array[$x]);
		}
		print "Order successfully updated!";
	}

	// Sections
	function add_section()
	{
		$this->load->model('Filmtypesmodel','filmtype');

		$slug = ""; $slug = create_simple_slug($this->input->post('section-new'));
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->filmtype->check_filmtype_slug($slug,"wf_category");
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}

		$values = array(
			"name" => $this->input->post('section-new'),
			"slug" => $slug,
			"festival_id" => $this->input->post('sec-festival-new'),
			//"sponsor_id" => $this->input->post('sec-sponsor-new'),
			"Description" => $this->input->post('description-new'),
			"color" => $this->input->post('section-color')
		);
		$this->load->model('Sectionmodel','section');
		$new_id = $this->section->add_value($values,"wf_category");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
	}

	function update_section()
	{
		$this->load->model('Filmtypesmodel','filmtype');

		$slug = ""; $slug = create_simple_slug($this->input->post('section-current'));
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->filmtype->check_filmtype_slug($slug,"wf_category");
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}

		$id = $this->input->post('section');
		$values = array(
			"name" => $this->input->post('section-current'),
			"slug" => $slug,
			"festival_id" => $this->input->post('sec-festival-current'),
			//"sponsor_id" => $this->input->post('sec-sponsor-current'),
			"Description" => $this->input->post('description-current'),
			"color" => $this->input->post('section-color-update')
		);
		$this->load->model('Sectionmodel','section');
		$new_id = $this->section->update_value($values,"wf_category",$id);
	}

	function delete_section($section_id)
	{
		$this->load->helper('form');
		$this->load->model('Sectionmodel','section');

		$res_count = 9999;
		$id = $section_id;

		$results = $this->section->check_section_value($id);
		$res_count = count($results);

		if ($res_count > 0) {
			$section_array = convert_to_array($this->section->get_section_except($id, "asc"));

			print "<p><strong>".$results[0]->name."</strong> is associated with <strong>".$res_count."</strong> films. If you wish to delete it, please select a replacement value below to assign to these films.</p>\n";
			print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t<tbody style=\"border-top:none;\">\n";
			print "\t\t<tr valign=\"top\">\n";
			print "\t\t\t<td width=\"50%\"><label for=\"replacement-value-section\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
			print "\t\t\t<td width=\"50%\">";
			print form_dropdown('replacement-value-section', $section_array, 0, "id='replacement-value-section' class='select ui-widget-content ui-corner-all'");
			print "</td>\n";
			print "\t\t</tr>\n";
			print "\t</tbody>\n";
			print "</table>\n";
		} else {
			// Tells javascript to remove the entry from the dropdown box
			print "0";
		}

		// No results found, so it's okay to delete this value.
		if ($res_count == 0) {
			$this->section->delete_value($id, "wf_category");
		}
	}

	function delete_section_replace($id_delete)
	{
		$this->load->model('Sectionmodel','section');

		// Replace all instances of old id with new id
		$result = 0;
		$replacement_success = false;
		$id_replace = $this->input->post("replacement-value-section");

		$result = $this->section->replace_section_value($id_delete, $id_replace);
		if ($result != 0) { $replacement_success = true; }

		// If replacement was successful, remove old id
		if ($replacement_success == true) {
			$this->section->delete_value($id_delete, "wf_category");
		}
	}

	// Sponsors
	function add_sponsor()
	{
		$values = array(
			"name" => $this->input->post('sponsor-new')
		);
		$this->load->model('Sponsormodel','sponsor');
		$new_id = $this->sponsor->add_value($values,"wf_sponsor");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
	}

	function update_sponsor()
	{
		$id = $this->input->post('sponsor');
		$values = array(
			"name" => $this->input->post('sponsor-current')
		);
		$this->load->model('Sponsormodel','sponsor');
		$new_id = $this->sponsor->update_value($values,"wf_sponsor",$id);
	}

	function add_sponsor_type()
	{
		$values = array(
			"name" => $this->input->post('sponsortype-name-new'),
			"description" => $this->input->post('sponsortype-desc-new'),
			"order" => $this->input->post('sponsortype-order-new')
		);
		$this->load->model('Sponsormodel','sponsor');
		$new_id = $this->sponsor->add_value($values,"wf_type_sponsorlogo");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
	}

	function update_sponsor_type()
	{
		$id = $this->input->post('sponsortype');
		$values = array(
			"name" => $this->input->post('sponsortype-name-current'),
			"description" => $this->input->post('sponsortype-desc-current'),
			"order" => $this->input->post('sponsortype-order-current')
		);
		$this->load->model('Sponsormodel','sponsor');
		$new_id = $this->sponsor->update_value($values,"wf_type_sponsorlogo",$id);
	}

	function add_sponsor_logo()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->helper('url');

		$logo_url = "";
		// Handle the file upload
		if ($_FILES['logofile-new']['error'] == 0) {
			if ($_FILES['logofile-new']['type'] == "image/gif" ||
				$_FILES['logofile-new']['type'] == "image/jpeg" ||
				$_FILES['logofile-new']['type'] == "image/pjpeg" ||
				$_FILES['logofile-new']['type'] == "image/png") {

				$data['festival'] = $this->festival->get_current_festival();					
				$uploaddir = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year)."/sponsors/";
			
				$rand_value = rand(0,32768)."-";

				$uploadfile = $uploaddir . $rand_value . str_replace(" ","-",basename($_FILES['logofile-new']['name']));
				if (move_uploaded_file($_FILES['logofile-new']['tmp_name'], $uploadfile)) {
					$logo_url = "/".$uploadfile;
				}

				// Options for Image Resize - longer height or width no bigger than 400px
				$this->load->library('image_lib'); 
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadfile;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 400;
				$config['height'] = 400;
				
				$this->image_lib->initialize($config);
				if ( ! $this->image_lib->resize()) {
					echo $this->image_lib->display_errors();
				}
			}
		}
	
		// add sponsor logo entry / entries
		$sponsored_film_list = $this->input->post('sponsored-films-new');

		foreach ($sponsored_film_list as $sponsoredFilm) {
			$values = array(
				"movie_id" => $sponsoredFilm,
				"festival_id" => $this->input->post('festival-id'),
				"sponsor_id" => $this->input->post('sponsor-logo'),
				"sponsorlogo_id" => $this->input->post('sponsorlogo-type-new'),
				"url_website" => $this->input->post('websitelink-new'),
				"url_logo" => $logo_url
				//"file_dir" => $logo_url
			);
			$new_id = $this->films->add_value($values, "wf_sponsor_logos");
		}
		
		redirect('admin/film_settings/#tabs-3', 'refresh');
	}

	function update_sponsor_logo()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sponsormodel','sponsor');
		$this->load->helper('url');
		
		$logo_url = "";
		// Check to see if a new file has been uploaded first
		/*
		if (isset($_FILES['logofile-update']) && $_FILES['logofile-update']['error'] == 0) {
			if ($_FILES['logofile-update']['type'] == "image/gif" ||
				$_FILES['logofile-update']['type'] == "image/jpeg" ||
				$_FILES['logofile-update']['type'] == "image/pjpeg" ||
				$_FILES['logofile-update']['type'] == "image/png") {

				$data['festival'] = $this->festival->get_current_festival();					
				$uploaddir = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year)."/sponsors/";
			
				$rand_value = rand(0,32768)."-";

				$uploadfile = $uploaddir . $rand_value . str_replace(" ","-",basename($_FILES['logofile-new']['name']));
				if (move_uploaded_file($_FILES['logofile-new']['tmp_name'], $uploadfile)) {
					$logo_url = "/".$uploadfile;
				}

				// Options for Image Resize - longer height or width no bigger than 400px
				$this->load->library('image_lib'); 
				$config['image_library'] = 'gd2';
				$config['source_image'] = $uploadfile;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 400;
				$config['height'] = 400;
				
				$this->image_lib->initialize($config);
				if ( ! $this->image_lib->resize()) {
					echo $this->image_lib->display_errors();
				}
			}
		} else {
			$logo_url = $this->input->post('currentlogo-url');
		}
		*/
		$logo_url = $this->input->post('currentlogo-url');
	
		// determine which sponsored films need to be updated, added or deleted
		$logo_ids_to_update = convert_string_to_array($this->input->post('logo-ids-to-update'));
		$original_film_ids = convert_string_to_array($this->input->post('original-film-ids'));
		$new_sponsored_films = $this->input->post('sponsored-films-update');
		
		$update_list = array_intersect($original_film_ids, $new_sponsored_films);
		if (count($update_list) > 0) {
			foreach ($update_list as $sponsoredFilmToUpdate) {
				$sponsor_logo_data = $this->sponsor->get_sponsor_logo($this->input->post('sponsor-logo-update'), $sponsoredFilmToUpdate, $this->input->post('festival-id'));
				$values = array(
					"sponsorlogo_id" => $this->input->post('sponsorlogo-type-update'),
					"url_website" => $this->input->post('websitelink-update'),
					"url_logo" => $logo_url
				);
				$updated_id = $this->films->update_value($values, "wf_sponsor_logos", $sponsor_logo_data[0]->id);
			}
		}

		$delete_list = array_diff($original_film_ids, $new_sponsored_films);
		if (count($delete_list) > 0) {
			foreach ($delete_list as $sponsoredFilmToDelete) {
				$sponsor_logo_data = $this->sponsor->get_sponsor_logo($this->input->post('sponsor-logo-update'), $sponsoredFilmToDelete, $this->input->post('festival-id'));
				$this->films->delete_value($sponsor_logo_data[0]->id, "wf_sponsor_logos");
			}
		}
		
		$add_list = array_diff($new_sponsored_films, $original_film_ids);
		if (count($add_list) > 0) {
			foreach ($add_list as $sponsoredFilmToAdd) {
				$values = array(
					"movie_id" => $sponsoredFilmToAdd,
					"festival_id" => $this->input->post('festival-id'),
					"sponsor_id" => $this->input->post('sponsor-logo-update'),
					"sponsorlogo_id" => $this->input->post('sponsorlogo-type-update'),
					"url_website" => $this->input->post('websitelink-update'),
					"url_logo" => $logo_url
				);
				$new_id = $this->films->add_value($values, "wf_sponsor_logos");
			}
		}


		$sponsor_logos = $this->sponsor->get_all_sponsor_logos("0", $this->input->post('festival-id'));
		$sponsorlogo_types = $this->filmtype->get_all_type("sponsorlogo", "asc");

		$sponsored_films = array();
		foreach ($sponsor_logos as $thisLogo) {
			if (!isset($sponsored_films[$thisLogo->sponsor_id])) { $sponsored_films[$thisLogo->sponsor_id] = "<a href=\"/films/detail/".$thisLogo->slug."\" target=\"_blank\">".$thisLogo->title_en."</a>";}
			else { $sponsored_films[$thisLogo->sponsor_id] .= "<br/><a href=\"/films/detail/".$thisLogo->slug."\" target=\"_blank\">".$thisLogo->title_en."</a>"; }
		}
		$sponsorlogo_name = "";
		foreach($sponsorlogo_types as $thisLogoType) {
			if($thisLogoType->id == $this->input->post('sponsorlogo-type-update')) {
				$sponsorlogo_name = $thisLogoType->name;
			}
		}

		$width = 120;
		print "<tr valign=\"top\">";
		print "<td align=\"center\">";
		print "<button class=\"update\" data-id=\"".$this->input->post('sponsor-logo-update')."\" id=\"logo-".$this->input->post('sponsor-logo-update')."\">Update</button><br />";
		print "</td>";
		print "<td>".$this->input->post('sponsor-name')."<br />".$this->input->post('websitelink-update')."</td>";
		print "<td>".$sponsorlogo_name."<br /></td>";
		if ($sponsored_films[$this->input->post('sponsor-logo-update')] != "") {
			print "<td>".$sponsored_films[$this->input->post('sponsor-logo-update')]."</td>";
		} else {
			print "<td>No Films Sponsored?</td>";				
		}
		if ($logo_url != "") {
			print "<td><img src=\"".$logo_url."\" height=\"\" width=\"".$width."\" /></td>";
		} else {
			print "<td>No Logo Provided</td>";
		}
		print "</tr>";
	}


	function delete_sponsor_logo()
	{
		$this->load->model('Filmsmodel','films');
	
		// remove sponsor logo from disk
		$current_logo_url = $this->input->post('currentlogo-url');
		
		//$file = unlink($current_logo_url);
		//if ($file == true) {
		//	print "all photo files deleted!";
		//}

		// remove entries from database
		$entries_to_delete = convert_string_to_array($this->input->post('logo-ids-to-update'));
		foreach ($entries_to_delete as $thisSponsorLogo) {
			$this->films->delete_value($thisSponsorLogo, "wf_sponsor_logos");
		}
	}

	function return_sponsorlogo_json($festival_id) {
		$this->load->model('Sponsormodel','sponsor');
		print json_encode($this->sponsor->get_all_sponsor_logos("0", $festival_id));
	}


	// Locations / Venues
	function add_location()
	{
		$this->load->model('Locationmodel','location');

		$values = array(
			"name" => $this->input->post('screeninglocation-new'), 
			"displayname" => $this->input->post('displayname-new'),
			"seats" => $this->input->post('seats-new'),
			"slug" => create_simple_slug($this->input->post('displayname-new'))
		);

		if ($this->input->post('screeningformat-new') == false) {
			$values["format"] = "";
		} else {
			$values["format"] = convert_multiselect_to_string($this->input->post('screeningformat-new'));
		}
		
		$new_id = $this->location->add_value($values,"wf_screeninglocation");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";
		
		// Optionally add this location to the current festival
		if ($this->input->post('location-addtofestival') == 1) {
			$this->load->model('Festivalmodel','festival');

			$data['festival'] = $this->festival->get_current_festival();
			if ($this->session->userdata('festival') == $data['festival'][0]->id) {
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				$data['festival'] = $this->festival->get_festival($current_fest);
			}
			
			$festival_ids = explode(",",$data['festival'][0]->locations);
			$festival_ids[] = $new_id;
			
			$values2 = array(
				"locations" => implode(",",$festival_ids)
			);
			$upd_id = $this->festival->update_value($values2, "wf_festival", $current_fest);
		}		
	}

	function update_location()
	{
		$this->load->model('Locationmodel','location');

		$id = $this->input->post('screeninglocation');
		$values = array(
			"name" => $this->input->post('screeninglocation-current'), 
			"displayname" => $this->input->post('displayname-current'),
			"seats" => $this->input->post('seats-current'),
			"slug" => create_simple_slug($this->input->post('displayname-current'))
		);
		
		if ($this->input->post('screeningformat-current') == false) {
			$values["format"] = "";
		} else {
			$values["format"] = convert_multiselect_to_string($this->input->post('screeningformat-current'));
		}
		$new_id = $this->location->update_value($values,"wf_screeninglocation",$id);
	}

	function delete_location($location_id)
	{
		$this->load->helper('form');
		$this->load->model('Locationmodel','location');

		$id = $location_id;
		$location = $this->location->get_location($id);

		// Check if location is being used in any screenings - as location_id -or- location_id2 
		$results1 = $this->location->check_location_screening_value($id);
		$res_count1 = count($results1);

		// Check if location is being used in any festivals
		$results2 = $this->location->check_location_festival_value($id);
		$res_count2 = count($results2);

		if ($res_count1 > 0 || $res_count2 > 0) {
			$location_array = convert_to_array($this->location->get_location_except($id, "asc"));

			print "<p><strong>".$location[0]->name."</strong> is associated with <strong>".$res_count1."</strong> screenings and is assigned to <strong>".$res_count2."</strong> festivals. If you wish to delete it, please select a replacement location below to assign to these screenings/festivals.</p>\n";
			print "<table class=\"FilmSettingDialog\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
			print "\t<tbody style=\"border-top:none;\">\n";
			print "\t\t<tr valign=\"top\">\n";
			print "\t\t\t<td width=\"50%\"><label for=\"replacement-value-location\">Replacement Value</label><span class=\"req\"> *</span></td>\n";
			print "\t\t\t<td width=\"50%\">";
			print form_dropdown('replacement-value-location', $location_array, 0, "id='replacement-value-location' class='select ui-widget-content ui-corner-all'");
			print "</td>\n";
			print "\t\t</tr>\n";
			print "\t</tbody>\n";
			print "</table>\n";
		} else {
			// Tells javascript to remove the entry from the dropdown box
			print "0";
		}

		// No results found, so it's okay to delete this value.
		if ($res_count1 == 0 && $res_count2 == 0) {
			$this->location->delete_value($id, "wf_screeninglocation");
		}
	}

	function delete_location_replace($id_delete)
	{
		$this->load->model('Locationmodel','location');

		// Replace all instances of old id with new id
		$result = 0;
		$screening_replacement_success = false;
		$festival_replacement_success = false;
		$replacement_success = false;
		$id_replace = $this->input->post("replacement-value-location");

		// Check if location is being used in any screenings - as location_id -or- location_id2 
		$check1 = $this->location->check_location_screening_value($id_delete);
		$check_count1 = count($check1);

		// Check if location is being used in any festivals
		$check2 = $this->location->check_location_festival_value($id_delete);
		$check_count2 = count($check2);

		if ($check_count1 > 0) {
			$results1 = $this->location->replace_location_value_screening1($id_delete, $id_replace);
			$results2 = $this->location->replace_location_value_screening2($id_delete, $id_replace);

			if ($results1 != 0 || $results2 != 0) { $screening_replacement_success = true; }
		} else if ($check_count1 == 0) {
			$screening_replacement_success = true;	
		}

		if ($check_count2 > 0) {
			$replacement_count = 0;
			foreach ($check2 as $thisFestival) {
				$locations_array = explode(",", $thisFestival->locations);
					$keys = array_keys($locations_array,$id_delete);
					foreach($keys as $k) { $locations_array[$k] = $id_replace; }
				$locations_string = implode(",", $locations_array); 
				$results3 = $this->location->replace_location_value_festival($thisFestival->id, $locations_string);
				if ($results3 != 0) { $replacement_count++; }
			}
			if ($check_count2 == $replacement_count) { $festival_replacement_success = true; }
		} else if ($check_count2 == 0) {
			$festival_replacement_success = true;	
		}
		
		// If replacement was successful, remove old id
		if ($screening_replacement_success == true && $festival_replacement_success == true) { $replacement_success = true; }
		if ($replacement_success == true) { $this->location->delete_value($id_delete, "wf_screeninglocation"); }
	}

	function add_venue()
	{
		$this->load->model('Locationmodel','location');

		$values = array(
			"name" => $this->input->post('screeningvenue-new'),
			"address" => $this->input->post('venueaddress-new'),
			"city" => $this->input->post('venuecity-new'),
			"state" => $this->input->post('venuestate-new'),
			"postal_code" => $this->input->post('venuezipcode-new'),
			"phone" => $this->input->post('venuephone-new'),
			"slug" => create_simple_slug($this->input->post('screeningvenue-new'))
		);

		if ($this->input->post('venuelocations-new') == false) {
			$values["location_ids"] = "";
		} else {
			$values["location_ids"] = convert_multiselect_to_string($this->input->post('venuelocations-new'));
		}
		$new_id = $this->location->add_value($values,"wf_screeningvenue");
		print "<option value=\"".$new_id."\">".$values['name']."</option>";		
	}

	function update_venue()
	{
		$this->load->model('Locationmodel','location');

		$id = $this->input->post('screeningvenue');
		$values = array(
			"name" => $this->input->post('screeningvenue-current'),
			"address" => $this->input->post('venueaddress-current'),
			"city" => $this->input->post('venuecity-current'),
			"state" => $this->input->post('venuestate-current'),
			"postal_code" => $this->input->post('venuezipcode-current'),
			"phone" => $this->input->post('venuephone-current'),
			"slug" => create_simple_slug($this->input->post('screeningvenue-current'))
		);
		
		if ($this->input->post('venuelocations-current') == false) {
			$values["location_ids"] = "";
		} else {
			$values["location_ids"] = convert_multiselect_to_string($this->input->post('venuelocations-current'));
		}
		$new_id = $this->location->update_value($values,"wf_screeningvenue",$id);
	}

	// Festivals
	function add_festival()
	{
		$this->load->model('Festivalmodel','festival');
		$slug = $this->input->post('year-new')."_".create_simple_slug($this->input->post('festival-new'));
		$values = array(
			"name" => $this->input->post('festival-new'), 
			"year" => $this->input->post('year-new'),
			"slug" => $slug,
			"startdate" => $this->input->post('startdate-new'),
			"enddate" => $this->input->post('enddate-new'),
			"timezone" => date("O"),
			"current" => 0,
			"currentfront" => 0,
			"showonfront" => 0,
			"default_photo_url" => "/assets/images/".$slug."/001-default-photo.jpg",
			"program_updates" => "<p>No Updates Yet</p>",
			"updated_date" => date("Y-m-d H:i:s")
		);

		if ($this->input->post('locations-new') == false) {
			$values["locations"] = "";
		} else {
			$values["locations"] = convert_multiselect_to_string($this->input->post('locations-new'));
		}

		// Create image directories for this festival
		$img_dir = return_img_dir($values['name'], $values['year']);

		$new_id = $this->festival->add_value($values,"wf_festival");
		print "<option value=\"".$new_id."\">".$values['year']." ".$values['name']."</option>";
	}

	function update_festival()
	{
		$id = $this->input->post('festival');
		$values = array(
			"name" => $this->input->post('festival-current'), 
			"year" => $this->input->post('year-current'),
			"startdate" => $this->input->post('startdate-current'),
			"enddate" => $this->input->post('enddate-current')
		);

		if ($this->input->post('locations-current') == false) {
			$values["locations"] = "";
		} else {
			$values["locations"] = convert_multiselect_to_string($this->input->post('locations-current'));
		}

		$this->load->model('Festivalmodel','festival');
		$new_id = $this->festival->update_value($values,"wf_festival",$id);

		$img_dir = return_img_dir($this->input->post('festival-current'), $this->input->post('year-current'));
	}

	function set_current_festival($admin)
	{
		$this->load->model('Festivalmodel','festival');
		if ($admin == 1) {
			$id = $this->input->post('set-current-festival');
			$values = array(
				"current" => 1
			);
			$this->festival->clear_current_festivals();
		} else {
			$id = $this->input->post('set-current-festival2');
			$values = array(
				"currentfront" => 1,
				"showonfront" => 1
			);
			$this->festival->clear_current_festivals_front();
		}
		$new_id = $this->festival->update_value($values,"wf_festival",$id);
	}

	function return_sptype_json() {
		$this->load->model('Filmtypesmodel','filmtype');
		print json_encode($this->filmtype->get_all_type("sponsorlogo","asc"));
	}


	function add_competition() {
		$this->load->model('Competitionmodel','competition');

		$values = array(
			"name" => $this->input->post('comp-name-new'),
			"slug" => create_simple_slug($this->input->post('festival-name-new'))."_".create_simple_slug($this->input->post('comp-name-new')),
			"festival_id" => $this->input->post('comp-festival-new'),
			"instructions" => $this->input->post('comp-instructions-new')
		);
		$new_id = $this->competition->add_value($values,"wf_competition");
		print "<option value=\"".$new_id."\">".$this->input->post('festival-name-new')." ".$values['name']."</option>";
	}

	function update_competition() {
		$this->load->model('Competitionmodel','competition');

		$id = $this->input->post('comp-competition-update');
		$values = array(
			"name" => $this->input->post('comp-name-update'), 
			"slug" => create_simple_slug($this->input->post('festival-name-update'))."_".create_simple_slug($this->input->post('comp-name-update')),
			"festival_id" => $this->input->post('comp-festival-update'),
			"instructions" => $this->input->post('comp-instructions-update')
		);
		$updated_id = $this->competition->update_value($values,"wf_competition",$id);
	}

	function add_competition_film() {
		$this->load->model('Competitionmodel','competition');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$competition = $this->competition->get_competition($this->input->post('compfilm-compname-new'));
		$festivals = $this->festival->get_all_festivals();
		$filmids = $this->films->get_all_internal_film_ids($competition[0]->festival_id);

		$festival_name = $movie_name = "";
		foreach ($festivals as $thisFestival) {
			if ($thisFestival->id == $competition[0]->festival_id) {
				$festival_name = $thisFestival->year." ".$thisFestival->name;
			}
		}
		foreach ($filmids as $thisMovie) {
			if ($thisMovie->movie_id == $this->input->post('compfilm-filmname-new')) {
				$movie_name = $thisMovie->label; break;
			}
		}
		$values = array(
			"competition_id" => $this->input->post('compfilm-compname-new'), 
			"movie_id" => $this->input->post('compfilm-filmname-new'),
			"video_url" => $this->input->post('compfilm-video-url-new'),
			"video_password" => $this->input->post('compfilm-video-password-new')
		);
		$new_id = $this->competition->add_value($values,"wf_competition_films");

		print "\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"cf-".$new_id."\" data-id=\"".$new_id."\" class=\"edit\">Edit</button>";
		if ($this->session->userdata('limited') == 0) {
			print "&nbsp;<a href=\"#\" id=\"cf-del-".$new_id."\" data-id=\"".$new_id."\" data-filmname=\"".switch_title($movie_name)."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Competition Film\" title=\"Delete this Competition Film\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a>";
		}
		print "</td>\n";
		print "\t\t<td>".$festival_name."</td>\n";
		print "\t\t<td>".$competition[0]->name."</td>\n";
		print "\t\t<td>".switch_title($movie_name)."</td>\n";
		print "\t\t<td>".$values['video_url']."</td>\n";
		print "\t\t<td>".$values['video_password']."</td>\n";
		print "\t</tr>\n";
		print "\t<tr id=\"competitions_replaceme\">\n";
		print "\t\t<td colspan=\"6\"><input type=\"hidden\" name=\"competitions_new_id\" id=\"competitions_new_id\" value=\"".$new_id."\" /></td>\n";
		print "\t</tr>\n";
	}

	function update_competition_film() {
		$this->load->model('Competitionmodel','competition');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$competition = $this->competition->get_competition($this->input->post('compfilm-compname-update'));
		$festivals = $this->festival->get_all_festivals();
		$filmids = $this->films->get_all_internal_film_ids($competition[0]->festival_id);

		$festival_name = $movie_name = "";
		foreach ($festivals as $thisFestival) {
			if ($thisFestival->id == $competition[0]->festival_id) {
				$festival_name = $thisFestival->year." ".$thisFestival->name;
			}
		}
		foreach ($filmids as $thisMovie) {
			if ($thisMovie->movie_id == $this->input->post('compfilm-filmname-update')) {
				$movie_name = $thisMovie->label; break;
			}
		}
		
		$id = $this->input->post('comp-competition-film-update');
		$values = array(
			"competition_id" => $this->input->post('compfilm-compname-update'), 
			"movie_id" => $this->input->post('compfilm-filmname-update'),
			"video_url" => $this->input->post('compfilm-video-url-update'),
			"video_password" => $this->input->post('compfilm-video-password-update')
		);
		$updated_id = $this->competition->update_value($values,"wf_competition_films",$id);

		print "\t<tr valign=\"top\" class=\"evenrow\">\n";
		print "\t\t<td><button id=\"cf-".$updated_id."\" data-id=\"".$updated_id."\" class=\"edit\">Edit</button>";
		if ($this->session->userdata('limited') == 0) {
			print "&nbsp;<a href=\"#\" id=\"cf-del-".$updated_id."\" data-id=\"".$updated_id."\" data-filmname=\"".switch_title($movie_name)."\" class=\"delete\"><img src=\"/assets/images/cancel.png\" alt=\"Delete this Competition Film\" title=\"Delete this Competition Film\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer;\"></a>";
		}
		print "</td>\n";
		print "\t\t<td>".$festival_name."</td>\n";
		print "\t\t<td>".$competition[0]->name."</td>\n";
		print "\t\t<td>".switch_title($movie_name)."</td>\n";
		print "\t\t<td>".$values['video_url']."</td>\n";
		print "\t\t<td>".$values['video_password']."</td>\n";
		print "\t</tr>\n";
	}

	function delete_competition_film() {
		$this->load->model('Competitionmodel','competition');
		$this->competition->delete_value($this->input->post("compfilm_id"), "wf_competition_films");
	}

	function return_competition_json() {
		$this->load->model('Competitionmodel','competition');
		print json_encode($this->competition->get_all_competitions());
	}

	function return_competition_films_json() {
		$this->load->model('Competitionmodel','competition');
		print json_encode($this->competition->get_all_competition_films());
	}


}

/* End of file film_settings.php */
/* Location: ./system/application/controllers/admin/film_settings.php */
?>