<?php
class Guest_Transportation extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Festivalmodel','festival');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['days'] = array();
		$startdate = strtotime($data['festival'][0]->startdate);
		$enddate = strtotime($data['festival'][0]->enddate);
		for ($x = $startdate; $x <= $enddate; $x = $x + (24 * 60 * 60) ) {
			$data['days'][] = date("n/j", $x);
		}


		$vars['title'] = "List of Guest Transportation";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_transportation";
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_transportation', $data);
		$this->load->view('admin/footer', $vars);
	}

}

/* End of file guest_transportation.php */
/* Location: ./system/application/controllers/admin/guest_transportation.php */
?>