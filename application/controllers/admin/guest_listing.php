<?php
class Guest_Listing extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		check_login();
		require_once("helper_functions.php");	
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttypes');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Schedulemodel','schedule');
		
		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$guest_list = array(); foreach ($data['guestids'] as $guests) { $guest_list[] = $guests->guest_id; }
		if (count($guest_list) > 0) {
			$data['flights'] =  $this->flight->get_all_flights($guest_list, "asc");
			$data['hotels'] =  $this->hotel->get_all_hotels($guest_list, "asc");
			$data['guest_films'] = $this->guest->get_all_guest_film_affiliations($guest_list);
		} else {
			$data['flights'] = array();
			$data['hotels'] = array();
			$data['guest_films'] = array();
		}

		// Remove any film affiliations that are set to a '0' value.
		foreach ($data['guest_films'] as $thisFilmAff) {
			if ($thisFilmAff->movie_id == 0) {
				$this->guest->delete_value($thisFilmAff->aff_id, "fg_filmaffiliations");
			}
		}

		$data['guests'] = $this->guest->get_all_guests($current_fest);
		foreach ($data['guests'] as $guests) {
			$guests->DepartureDate = $guests->ArrivalDate = $guests->checkin = $guests->checkout = "";
			foreach ($data['flights'] as $flights) {
				if ($flights->festivalguest_id == $guests->guest_id) {
					if ($flights->DepartureDate != "0000-00-00" && $flights->DepartureDate != "1969-12-31") {
						$guests->DepartureDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->DepartureDate))."<br />";
					}
					if ($flights->ArrivalDate != "0000-00-00" && $flights->ArrivalDate != "1969-12-31") {
						$guests->ArrivalDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->ArrivalDate))."<br />";
					}
				}
			}
			foreach ($data['hotels'] as $hotels) {
				if ($hotels->festivalguest_id == $guests->guest_id) {
					if ($hotels->checkin != "0000-00-00" && $hotels->checkin != "1969-12-31") {
						$guests->checkin .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkin))."<br />";
					}
					if ($hotels->checkout != "0000-00-00" && $hotels->checkout != "1969-12-31") {
						$guests->checkout .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/icons/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkout))."<br />";
					}
				}
			}

			// Check to see if guest is affiliated with a film
			if ($guests->movie_id != "") {
				$filmTitle = $this->films->get_film_title($guests->movie_id);
				//print_r($filmTitle);
				if (count($filmTitle) > 0) {
					$guests->Film_Title = switch_title($filmTitle[0]->title_en);
				} else {
					$guests->Film_Title = "";
				}

				$match = FALSE;
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $guests->movie_id);
				foreach ($screenings as $thisScreening) {
					$film_array = explode(",", $thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilm) {
						if ($thisFilm == $guests->movie_id) { $match = TRUE; }
					}
					if ($match == TRUE) {
						$guests->Program_Title = $thisScreening->program_name;
					} else {
						$guests->Program_Title = "";
					}
				}
				if (count($screenings) == 0) {
					$guests->Program_Title = "";
				}
			} else {
				$guests->Program_Title = "";
				$guests->Film_Title = "";
			}
		}

		$data['screenings'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['guest_programs'] = array();
		foreach ($data['screenings'] as $thisScreening) {
			$match = FALSE;
			if ($thisScreening->program_name != "") {
				$program_id_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($data['guest_films'] as $thisGuestFilm) {
					foreach ($program_id_array as $thisProgramId) {
						if ($thisGuestFilm->movie_id == $thisProgramId) {
							$match = TRUE;
							break(2);
						}
					}
				}
				if ($match == TRUE) { $data['guest_programs'][] = $thisScreening; }
			}
		}
		
		// Used in Add A Guest Dialog
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['guesttypes'] = $this->guesttypes->get_all_type("guests","asc");

		$vars['title'] = "Guest Listing";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_listing";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_listing', $data);
		$this->load->view('admin/footer', $vars);
	}

	function search()
	{
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttypes');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Schedulemodel','schedule');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$guest_list = array(); foreach ($data['guestids'] as $guests) { $guest_list[] = $guests->guest_id; }
		if (count($guest_list) > 0) {
			$data['flights'] =  $this->flight->get_all_flights($guest_list, "asc");
			$data['hotels'] =  $this->hotel->get_all_hotels($guest_list, "asc");
			$data['guest_films'] = $this->guest->get_all_guest_film_affiliations($guest_list);
		} else {
			$data['flights'] = array();
			$data['hotels'] = array();
			$data['guest_films'] = array();
		}

		// Remove any film affiliations that are set to a '0' value.
		foreach ($data['guest_films'] as $thisFilmAff) {
			if ($thisFilmAff->movie_id == 0) {
				$this->guest->delete_value($thisFilmAff->aff_id, "fg_filmaffiliations");
			}
		}

		$data['guests'] = $this->guest->get_all_guests_search($current_fest,"fg_festivalguests.Lastname", "asc",0,9999,$this->input->post('guestSearch'));
		foreach ($data['guests'] as $guests) {
			$guests->DepartureDate = $guests->ArrivalDate = $guests->checkin = $guests->checkout = "";
			foreach ($data['flights'] as $flights) {
				if ($flights->festivalguest_id == $guests->guest_id) {
					if ($flights->DepartureDate != "0000-00-00" && $flights->DepartureDate != "1969-12-31") {
						$guests->DepartureDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->DepartureDate))."<br />";
					}
					if ($flights->ArrivalDate != "0000-00-00" && $flights->ArrivalDate != "1969-12-31") {
						$guests->ArrivalDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->ArrivalDate))."<br />";
					}
				}
			}
			foreach ($data['hotels'] as $hotels) {
				if ($hotels->festivalguest_id == $guests->guest_id) {
					if ($hotels->checkin != "0000-00-00" && $hotels->checkin != "1969-12-31") {
						$guests->checkin .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkin))."<br />";
					}
					if ($hotels->checkout != "0000-00-00" && $hotels->checkout != "1969-12-31") {
						$guests->checkout .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkout))."<br />";
					}
				}
			}

			// Check to see if guest is affiliated with a film
			if ($guests->movie_id != "") {
				$filmTitle = $this->films->get_film_title($guests->movie_id);
				$guests->Film_Title = switch_title($filmTitle[0]->title_en);

				$match = FALSE;
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $guests->movie_id);
				foreach ($screenings as $thisScreening) {
					$film_array = explode(",", $thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilm) {
						if ($thisFilm == $guests->movie_id) { $match = TRUE; }
					}
					if ($match == TRUE) {
						$guests->Program_Title = $thisScreening->program_name;
					} else {
						$guests->Program_Title = "";
					}
				}
				if (count($screenings) == 0) {
					$guests->Program_Title = "";
				}
			} else {
				$guests->Program_Title = "";
				$guests->Film_Title = "";
			}
		}
		$data['screenings'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['guest_programs'] = array();
		foreach ($data['screenings'] as $thisScreening) {
			$match = FALSE;
			if ($thisScreening->program_name != "") {
				$program_id_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($data['guest_films'] as $thisGuestFilm) {
					foreach ($program_id_array as $thisProgramId) {
						if ($thisGuestFilm->movie_id == $thisProgramId) {
							$match = TRUE;
							break(2);
						}
					}
				}
				if ($match == TRUE) { $data['guest_programs'][] = $thisScreening; }
			}
		}

		// Used in Add A Guest Dialog
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['guesttypes'] = $this->guesttypes->get_all_type("guests","asc");

		$vars['title'] = "Guest Listing - Search Results";
		$vars['admin'] = "YES";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_listing";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_listing', $data);
		$this->load->view('admin/footer', $vars);
	}

	function sort($sorttoken) {
		$this->load->helper('form');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->model('Guestmodel','guest');
		$this->load->model('Guesttypesmodel','guesttypes');
		$this->load->model('Flightmodel','flight');
		$this->load->model('Hotelmodel','hotel');
		$this->load->model('Schedulemodel','schedule');

		$data['festival'] = $this->festival->get_current_festival();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['guestids'] = $this->guest->get_all_guest_ids($current_fest);
		$guest_list = array(); foreach ($data['guestids'] as $guests) { $guest_list[] = $guests->guest_id; }

		if (count($guest_list) > 0) {
			$data['flights'] =  $this->flight->get_all_flights($guest_list, "asc");
			$data['hotels'] =  $this->hotel->get_all_hotels($guest_list, "asc");
			$data['guest_films'] = $this->guest->get_all_guest_film_affiliations($guest_list);
		} else {
			$data['flights'] = array();
			$data['hotels'] = array();
			$data['guest_films'] = array();
		}

		// Remove any film affiliations that are set to a '0' value.
		foreach ($data['guest_films'] as $thisFilmAff) {
			if ($thisFilmAff->movie_id == 0) {
				$this->guest->delete_value($thisFilmAff->aff_id, "fg_filmaffiliations");
			}
		}

		$sort = "asc";
		switch ($sorttoken) {
			case "namedesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "desc",0,9999); break;
			case "nameasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "asc",0,9999); break;

			case "typedesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"GuestType", "desc",0,9999); break;
			case "typeasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"GuestType", "asc",0,9999); break;

			case "programdesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "desc",0,9999); break;
			case "programasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "asc",0,9999); break;

			case "affiliationdesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"AffiliationOptional", "desc",0,9999); break;
			case "affiliationasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"AffiliationOptional", "asc",0,9999); break;

			case "arrivaldesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "desc",0,9999); $sort = "desc"; break;
			case "arrivalasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "asc",0,9999); $sort = "asc"; break;

			case "departuredesc": $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "desc",0,9999); $sort = "desc"; break;
			case "departureasc":  $data['guests'] = $this->guest->get_all_internal_guests_sort($current_fest,"fg_festivalguests.Lastname", "asc",0,9999); $sort = "asc"; break;
		}

		$data['screenings'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['guest_programs'] = array();
		foreach ($data['screenings'] as $thisScreening) {
			$match = FALSE;
			if ($thisScreening->program_name != "") {
				$program_id_array = explode(",",$thisScreening->program_movie_ids);
				foreach ($data['guest_films'] as $thisGuestFilm) {
					foreach ($program_id_array as $thisProgramId) {
						if ($thisGuestFilm->movie_id == $thisProgramId) {
							$match = TRUE;
							break(2);
						}
					}
				}
				if ($match == TRUE) { $data['guest_programs'][] = $thisScreening; }
			}
		}
		
		foreach ($data['guests'] as $guests) {
			$guests->DepartureDate = $guests->ArrivalDate = $guests->checkin = $guests->checkout = "";
			foreach ($data['flights'] as $flights) {
				if ($flights->festivalguest_id == $guests->guest_id) {
					if ($flights->DepartureDate != "0000-00-00" && $flights->DepartureDate != "1969-12-31") {
						$guests->DepartureDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->DepartureDate))."<br />";
					}
					if ($flights->ArrivalDate != "0000-00-00" && $flights->ArrivalDate != "1969-12-31") {
						$guests->ArrivalDate .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/airplane.png\" alt=\"Flight\" title=\"Flight\"> ". date("n/j/Y",strtotime($flights->ArrivalDate))."<br />";
					}
				}
			}
			foreach ($data['hotels'] as $hotels) {
				if ($hotels->festivalguest_id == $guests->guest_id) {
					if ($hotels->checkin != "0000-00-00" && $hotels->checkin != "1969-12-31") {
						$guests->checkin .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkin))."<br />";
					}
					if ($hotels->checkout != "0000-00-00" && $hotels->checkout != "1969-12-31") {
						$guests->checkout .= "<img width=\"16\" height=\"16\" border=\"0\" src=\"/assets/images/building.png\" alt=\"Hotel\" title=\"Hotel\"> ". date("n/j/Y",strtotime($hotels->checkout))."<br />";
					}
				}
			}

			// Check to see if guest is affiliated with a film
			if ($guests->movie_id != "") {
				$filmTitle = $this->films->get_film_title($guests->movie_id);
				$guests->Film_Title = switch_title($filmTitle[0]->title_en);

				$match = FALSE;
				$screenings = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $guests->movie_id);
				foreach ($screenings as $thisScreening) {
					$film_array = explode(",", $thisScreening->program_movie_ids);
					foreach ($film_array as $thisFilm) {
						if ($thisFilm == $guests->movie_id) { $match = TRUE; }
					}
					if ($match == TRUE) {
						$guests->Program_Title = $thisScreening->program_name;
					} else {
						$guests->Program_Title = "";
					}
				}
				if (count($screenings) == 0) {
					$guests->Program_Title = "";
				}
			} else {
				$guests->Program_Title = "";
				$guests->Film_Title = "";
			}
		}

		switch ($sorttoken) {
			case "programdesc": usort($data['guests'], 'programcmp');
				break;
			case "programasc":  usort($data['guests'], 'programcmp2');
				break;
		}

		// Used in Add A Guest Dialog
		$data['films'] = $this->films->get_all_internal_films($current_fest);
		$data['guesttypes'] = $this->guesttypes->get_all_type("guests","asc");

		$vars['title'] = "Guest Listing";
		$vars['admin'] = "YES";
		$vars['path'] = "/";
		$vars['selected_page'] = "guest_listing";
		$vars['guestJSON'] = json_encode($data['guestids']);
		$vars['festivals'] = $this->festival->get_all_festivals();

		$this->load->view('admin/header', $vars);
		$this->load->view('admin/nav_bar', $vars);
		$this->load->view('admin/guest_listing', $data);
		$this->load->view('admin/footer', $vars);
	}

	function add() {
		// This function will add the guest; It will also create the slug from the guest's name, since
		// this is how the guest will be referenced in the URL. It will then redirect to the edit guest
		// page for this newly added guest.
		$this->load->model('Filmsmodel','films');
		$this->load->model('Guestmodel','guest');

		$values = array(
			"Firstname" => $this->input->post('Firstname'),
			"Lastname" => $this->input->post('Lastname'),
			"guesttype_id" => $this->input->post('GuestType'),
			"AffiliationOptional" => $this->input->post('AffiliationOptional'),
			"slug" => "",
			"gb_pickedup" => "0",
			"badge_pickedup" => "0",
			"festival_id" => $this->input->post('festival_id'),
			"updated_date" => date("Y-m-d H:i:s")
		);
		$slug = create_slug($values["Firstname"]." ".$values["Lastname"]);		
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->films->check_existing_slug($slug);
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}
		$values["slug"] = $slug;
		$guest_id = $this->guest->add_value($values, "fg_festivalguests");

		if ($this->input->post('FilmAffiliation') != 0) {
			$values2 = array(
				"festivalguest_id" => $guest_id,
				"movie_id" => $this->input->post('FilmAffiliation')
			);
			$filmaffiliation_id = $this->guest->add_value($values2, "fg_filmaffiliations");
		}
		print $slug;
	}

	function toggle($field, $guest_id)
	{
		$this->load->model('Guestmodel','guest');

		$table = "fg_festivalguests"; $fieldrevised = $field;
		switch ($field) {
			// Guest Giftbags & Badges
			case "gb_pickedup":
			case "badge_pickedup": $table = "fg_festivalguests"; $data['guest'] = $this->guest->get_guest_toggle($guest_id); break;

			// Hotel Pickups & Translators
			case "pickuph": $fieldrevised = "pickup"; $table = "fg_accomodations"; $data['guest'] = $this->guest->get_hotel_toggle($guest_id); break;
			case "translatorh": $fieldrevised = "translator"; $table = "fg_accomodations"; $data['guest'] = $this->guest->get_hotel_toggle($guest_id); break;

			// Flight Pickups & Translators
			case "pickupf": $fieldrevised = "pickup"; $table = "fg_flights"; $data['guest'] = $this->guest->get_flight_toggle($guest_id); break;
			case "translatorf": $fieldrevised = "translator"; $table = "fg_flights"; $data['guest'] = $this->guest->get_flight_toggle($guest_id); break;

			// Screening Transport & Translators
			case "transportsc": $fieldrevised = "transport"; $table = "fg_screeningappearances"; $data['guest'] = $this->guest->get_scapp_toggle($guest_id); break;
			case "translatorsc": $fieldrevised = "translator"; $table = "fg_screeningappearances"; $data['guest'] = $this->guest->get_scapp_toggle($guest_id); break;

			// Special Event Transport & Translators
			case "transportsp": $fieldrevised = "transport"; $table = "fg_specialappearances"; $data['guest'] = $this->guest->get_spapp_toggle($guest_id); break;
			case "translatorsp": $fieldrevised = "translator"; $table = "fg_specialappearances"; $data['guest'] = $this->guest->get_spapp_toggle($guest_id); break;
		}
		
		// Don't do anything if we couldn't find the guest id.
		if ($data['guest']) {
			if ($data['guest'][0]->$field == 0) {
				$this->guest->toggle_tinyint($guest_id, $fieldrevised, $table, 1);
				print "\t<a id=\"".$field."-".$guest_id."\" href=\"#\" data-id=\"".$guest_id."\" data-type=\"".$field."\" class=\"icon_tick\"></a>\n";
			} elseif ($data['guest'][0]->$field == 1) {
				$this->guest->toggle_tinyint($guest_id, $fieldrevised, $table, 0);
				print "\t<a id=\"".$field."-".$guest_id."\" href=\"#\" data-id=\"".$guest_id."\" data-type=\"".$field."\" class=\"icon_cross\"></a>\n";
			}
		}	
	}
}

/* End of file guest_listing.php */
/* Location: ./system/application/controllers/admin/guest_listing.php */
?>