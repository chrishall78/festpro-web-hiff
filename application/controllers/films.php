<?php
class Films extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");
		require_once("admin/helper_functions.php");
	}
	
	function index($type="a", $page=0)
	{
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);
		$default_image = "/".$data['upload_dir']."/001-default-photo.jpg";
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		if ($this->logged_in == true) {
			// get internal screenings for logged in preview.
			$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate,$data['festival'][0]->enddate);
			$data['schedule'] = $data['full_schedule'];
			$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
		} else {
			$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
			$data['schedule'] = $data['full_schedule'];
			$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		}
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$title_array = array(); $num_to_insert = 0;
		if ($page == 9999) {
			$expected_results = count($film_id_array);
			for ($i=0; $i < $expected_results; $i++) {
				if (isset($data['filmids'][$i])) {
					$title_array[] = strtolower($data['filmids'][$i]->label);
				}
			}
		} else {
			$expected_results = DEFAULT_FILMS_PER_PAGE + $page;			
			for ($i=$page; $i < $expected_results; $i++) {
				if (isset($data['filmids'][$i])) {
					$title_array[] = strtolower($data['filmids'][$i]->label);
				}
			}
		}

		$title_array2 = array_filter($title_array);
		if (count($title_array2) > 0) {
			sort($title_array);
			$first = $title_array2[0];
			$last = $title_array2[count($title_array2)-1];
			foreach ($data['full_schedule'] as $thisScreening) {
				$movie_id_array = explode(',',$thisScreening->program_movie_ids);
				if (count($movie_id_array) > 2) {
					$title_array[] = strtolower($thisScreening->program_name);
				}
			}

			sort($title_array);
			$first = $title_array2[0];
			$last = $title_array2[count($title_array2)-1];
			$title_array3 = array_diff($title_array, $title_array2);
			$title_array4 = array();
			foreach ($title_array3 as $thisTitle) {
				if (strcmp($thisTitle, $first) > 0 && strcmp($thisTitle, $last) < 0) {
					$title_array4[] = $thisTitle;
				}
			}
			$num_to_insert = count($title_array4);
		}

		if ($page == 9999) {
			// get internal films for logged in preview.
			if ($this->logged_in == true) {
				$data['films'] = $this->films->get_all_internal_films_sort($current_fest, "wf_movie.title_en", "asc", 0, 9999);
			} else {
				$data['films'] = $this->films->get_all_films_sort($current_fest, "wf_movie.title_en", "asc", 0, 9999);
			}
		} else {
			// get internal films for logged in preview.
			if ($this->logged_in == true) {
				$data['films'] = $this->films->get_all_internal_films_sort($current_fest, "wf_movie.title_en", "asc", $page, DEFAULT_FILMS_PER_PAGE);
			} else {
				$data['films'] = $this->films->get_all_films_sort($current_fest, "wf_movie.title_en", "asc", $page, DEFAULT_FILMS_PER_PAGE);
			}
		}

		$film_id_array2 = convert_to_array3($data['films']);
		if (count($film_id_array2) == 0) { $film_id_array2 = array(0); }

		$data['film_program_photos'] = array();

		// Find shorts programs with multiple screenings and combine them into one
		// the next block references $shorts_screenings for all film programs, not just multiple screenings.
		$shorts_screenings = array();
		foreach ($data['full_schedule'] as $thisScreening) {
			$movie_id_array = explode(',',$thisScreening->program_movie_ids);
			if (count($movie_id_array) >= 2) {
				$program_slug = $thisScreening->program_slug;
				$datetime = $thisScreening->date." ".$thisScreening->time;
				$date = date("D, M d", strtotime($datetime));
				$time = date("g:iA", strtotime($datetime));
				$location = $thisScreening->displayname;

				if ($thisScreening->url != "") {
					if (FP_FRONT_ENABLE_FACEBOX) {
						$current_screening = "<p class=\"film_list_screening\"><a href=\"#!".$thisScreening->url."\" rel=\"facebox\">".$date." - ".$time."&nbsp;&nbsp;".$location."</a></p>";
					} else {
						$current_screening = "<p class=\"film_list_screening\"><a href=\"".$thisScreening->url."\" target=\"_blank\">".$date." - ".$time."&nbsp;&nbsp;".$location."</a></p>";
					}
				} else {
					$current_screening = "<p class=\"film_list_screening\">".$date." - ".$time."&nbsp;&nbsp;".$location."</p>";
				}

				if (!isset($shorts_screenings[$program_slug])) {
					$shorts_screenings[$program_slug] = $current_screening;
				} else {
					$shorts_screenings[$program_slug] = $shorts_screenings[$program_slug].$current_screening;
				}
			}
		}

		// Add film programs to films listing
		$addedScreenings = array();
		$program_description = "";
		foreach ($data['full_schedule'] as $thisScreening) {
			$movie_id_array = explode(',',$thisScreening->program_movie_ids);
			if (count($movie_id_array) >= 2) {
				$program_name = $thisScreening->program_name;
				$program_slug =  $thisScreening->program_slug;
				$name_sort = strtolower($program_name);
				$program_description = $thisScreening->program_desc;

				$addedAlready = false;
				foreach ($addedScreenings as $slug) {
					if ($slug == $program_slug) { $addedAlready = true; }
				}

				if ($addedAlready == true) {
					// skip this duplicate film program
				} else {
					// Only insert the film program if it should be within this batch of results
					if (strcmp($name_sort, $first) > 0 && strcmp($name_sort, $last) < 0) {
						$current_runtime = 0;
						$current_screening = '';
						$current_desc = '';
						$current_array = array();

						// Retrieve all first photos for films in program
						$data['film_program_photos'][$program_slug] = array();
						$program_photos = $this->photovideo->get_all_first_photos($movie_id_array);
						if (count($program_photos) == 0) { $data['film_program_photos'][$program_slug][] = $default_image; }
						foreach ($program_photos as $photo) {
							$data['film_program_photos'][$program_slug][] = $photo->url_cropsmall;
						}

						foreach ($movie_id_array as $thisMovieId) {
							foreach ($data['filmids'] as $thisFilm) {
								if ($thisMovieId == $thisFilm->movie_id) {
									$current_runtime = $current_runtime + $thisFilm->runtime_int;
									$info = array('slug' => $thisFilm->value, 'title' => $thisFilm->label);
									$current_array[] = $info;
								}
							}
						}
						// Sort the multidimensional array
					    //usort($current_array, "sort_film_titles");
						foreach ($current_array as $currentFilm) {
							$current_desc .= "<a href='/films/detail/".$currentFilm['slug']."'>".switch_title($currentFilm['title'])."</a>, ";
						}
						$current_desc = trim($current_desc,', ');

						$record = new stdClass;
						$record->id = 0;
						$record->slug = $program_slug;
						$record->movie_id = $thisScreening->program_movie_ids;
						$record->category_id = 0;
						$record->category_name = '';
						$record->format_name = '';
						$record->event_id = 0;
						$record->event_name = $shorts_screenings[$program_slug];
						$record->premiere_id = 0;
						$record->title = $current_desc;
						$record->title_en = $program_name;
						$record->year = 0;
						$record->runtime_int = $current_runtime;
						$record->synopsis = $program_description;
						$record->synopsis_short = '';
						$record->Screener = 0;
						$record->Confirmed = 1;
						$record->Complete = 1;
						if ($thisScreening->Published == 1) {
							$record->Published = 1;
						} else {
							$record->Published = 0;
						}
						$record->section_slug = '';
						$record->event_slug = '';

						$data['films'][] = $record;
					}
				}
				$addedScreenings[] = $program_slug;
			}
		}

		// Sort the film list by Film Title / Program Name so the added programs are in the correct place
		usort($data['films'], "filmtitlecmp");

		$this->load->library('pagination');		
		$config['base_url'] = '/films/index/a/';
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		$config['uri_segment'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['total_rows'] = count($data['filmids']);

		$c = count($addedScreenings);
		foreach ($data["filmids"] as $thisFilmID) {
			if ($thisFilmID->runtime_int >= 45) { $c++; }
		}
		$real_total_rows = $c;

		$this->pagination->initialize($config); 

		if ($page == 9999) {
			$data['start_row'] = 1;
		} else {
			$data['start_row'] = $page + 1;
		}
		if ($page + DEFAULT_FILMS_PER_PAGE < $real_total_rows)  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $real_total_rows;
		}
		$data['total_rows'] = $real_total_rows; 

		// Get only first photo for each film
		$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array2);
		if (count($film_id_array) > 0) {
			$data['countries'] = $this->country->get_all_countries($film_id_array);
			$data['genres'] = $this->genre->get_all_genres($film_id_array);
		}

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;		
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_list";
		$data['selected_page'] = "film_list";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function search($page=0) {
		$search = $this->input->post('filmSearch');
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['films'] = $this->films->get_all_films_search($current_fest,"wf_movie.title_en","asc", $page, DEFAULT_FILMS_PER_PAGE, $search);
		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$this->load->library('pagination');
		$config['base_url'] = '/films/search/';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		$config['total_rows'] = count($data['films']);
		$this->pagination->initialize($config); 

		$data['start_row'] = $page + 1;
		if ($page + DEFAULT_FILMS_PER_PAGE < $config['total_rows'])  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $config['total_rows'];
		}
		$data['total_rows'] = $config['total_rows']; 

		// Get only first photo for each film
		$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);
		if (count($film_id_array) > 0) {
			$data['countries'] = $this->country->get_all_countries($film_id_array);
			$data['genres'] = $this->genre->get_all_genres($film_id_array);
		}

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_search";
		$data['selected_page'] = "film_search";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function country($country,$page=0) {
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$data['countries'] = $this->country->get_all_countries($current_fest);
		$data['current_country'] = "All Countries";
		foreach ($data['countries'] as $thisCountry) {
			if ($country == $thisCountry->slug) { $data['current_country'] = $thisCountry->name; }
		}

		if ($country == "all") {
			// Sort by Country Name
			$data['films'] = $this->films->get_all_films_sort_country($current_fest,"country_name","asc", $page, DEFAULT_FILMS_PER_PAGE);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);

			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($film_id_array);
				$data['genres'] = $this->genre->get_all_genres($film_id_array);
			}
		} else {
			// Filter by Country Name
			$data['films'] = $this->films->get_all_films_filter_country($current_fest,"wf_movie.title_en","asc","wf_type_country.slug",$country, $page, DEFAULT_FILMS_PER_PAGE);
			$data['films_all'] = $this->films->get_all_films_filter_country($current_fest,"wf_movie.title_en","asc","wf_type_country.slug",$country, 0, count($data['filmids']));
			$filter_id_array = convert_to_array3($data['films']);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($filter_id_array);

			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($filter_id_array);
				$data['genres'] = $this->genre->get_all_genres($filter_id_array);
			}
		}

		$this->load->library('pagination');
		$config['base_url'] = '/films/country/'.$country.'/';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['uri_segment'] = 4;
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		if ($country == "all") {
			$config['total_rows'] = count($data['filmids']);
		} else {
			$config['total_rows'] = count($data['films_all']);
		}
		$this->pagination->initialize($config); 

		$data['start_row'] = $page + 1;
		if ($page + DEFAULT_FILMS_PER_PAGE < $config['total_rows'])  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $config['total_rows'];
		}
		$data['total_rows'] = $config['total_rows']; 

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_country";
		$data['selected_page'] = "film_country";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function section($section,$page=0) {
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$data['sections'] = $this->section->get_all_sections($current_fest);
		$data['current_section'] = "All Sections";
		foreach ($data['sections'] as $thisSection) {
			if ($section == $thisSection->slug) { $data['current_section'] = $thisSection->name; }
		}

		if ($section == "all") {
			// Sort by Section Name
			$data['films'] = $this->films->get_all_films_sort($current_fest,"category_name","asc", $page, DEFAULT_FILMS_PER_PAGE);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);

			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($film_id_array);
				$data['genres'] = $this->genre->get_all_genres($film_id_array);
			}
		} else {
			// Filter by Section Name
			$data['films'] = $this->films->get_all_films_filter($current_fest,"wf_movie.title_en","asc","wf_category.slug",$section, $page, DEFAULT_FILMS_PER_PAGE);
			$data['films_all'] = $this->films->get_all_films_filter($current_fest,"wf_movie.title_en","asc","wf_category.slug",$section, 0, count($data['filmids']));
			$filter_id_array = convert_to_array3($data['films']);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($filter_id_array);

			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($filter_id_array);
				$data['genres'] = $this->genre->get_all_genres($filter_id_array);
			}
		}

		$this->load->library('pagination');
		$config['base_url'] = '/films/section/'.$section.'/';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['uri_segment'] = 4;
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		if ($section == "all") {
			$config['total_rows'] = count($data['filmids']);
		} else {
			$config['total_rows'] = count($data['films_all']);
		}
		$this->pagination->initialize($config); 

		$data['start_row'] = $page + 1;
		if ($page + DEFAULT_FILMS_PER_PAGE < $config['total_rows'])  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $config['total_rows'];
		}
		$data['total_rows'] = $config['total_rows']; 

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_section";
		$data['selected_page'] = "film_section";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function genre($genre,$page=0) {
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$data['genres'] = $this->genre->get_all_genres($current_fest);
		$data['current_genre'] = "All Genres";
		foreach ($data['genres'] as $thisGenre) {
			if ($genre == $thisGenre->slug) { $data['current_genre'] = $thisGenre->name; }
		}

		if ($genre == "all") {
			// Sort by Genre Name
			$data['films'] = $this->films->get_all_films_sort_genre($current_fest,"genre_name","asc", $page, DEFAULT_FILMS_PER_PAGE);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);
			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($film_id_array);
				$data['genres'] = $this->genre->get_all_genres($film_id_array);
			}
		} else {
			// Filter by Genre Name
			$data['films'] = $this->films->get_all_films_filter_genre($current_fest,"wf_movie.title_en","asc","wf_type_genre.slug",$genre, $page, DEFAULT_FILMS_PER_PAGE);
			$data['films_all'] = $this->films->get_all_films_filter_genre($current_fest,"wf_movie.title_en","asc","wf_type_genre.slug",$genre, 0, count($data['filmids']));
			$filter_id_array = convert_to_array3($data['films']);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($filter_id_array);
			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($filter_id_array);
				$data['genres'] = $this->genre->get_all_genres($filter_id_array);
			}
		}

		$this->load->library('pagination');
		$config['base_url'] = '/films/genre/'.$genre.'/';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['uri_segment'] = 4;
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		if ($genre == "all") {
			$config['total_rows'] = count($data['filmids']);
		} else {
			$config['total_rows'] = count($data['films_all']);
		}
		$this->pagination->initialize($config); 

		$data['start_row'] = $page + 1;
		if ($page + DEFAULT_FILMS_PER_PAGE < $config['total_rows'])  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $config['total_rows'];
		}
		$data['total_rows'] = $config['total_rows']; 

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_genre";
		$data['selected_page'] = "film_genre";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function event($event,$page=0) {
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Genremodel','genre');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }

		$data['events'] = $this->filmtype->get_all_type("event","asc","no");
		$data['current_eventtype'] = "All Event Types";
		foreach ($data['events'] as $thisEvent) {
			if ($event == $thisEvent->slug) { $data['current_eventtype'] = $thisEvent->name; }
		}

		if ($event == "all") {
			// Sort by Event Type Name
			$data['films'] = $this->films->get_all_films_sort($current_fest,"event_name","asc", $page, DEFAULT_FILMS_PER_PAGE);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);
			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($film_id_array);
				$data['genres'] = $this->genre->get_all_genres($film_id_array);
			}
		} else {
			// Filter by Event Type Name
			$data['films'] = $this->films->get_all_films_filter($current_fest,"wf_movie.title_en","asc","wf_type_event.slug",$event, $page, DEFAULT_FILMS_PER_PAGE);
			$data['films_all'] = $this->films->get_all_films_filter($current_fest,"wf_movie.title_en","asc","wf_type_event.slug",$event, 0, count($data['filmids']));
			$filter_id_array = convert_to_array3($data['films']);

			// Get only first photo for each film
			$data['all_photos'] = $this->photovideo->get_all_first_photos($filter_id_array);
			if (count($film_id_array) > 0) {
				$data['countries'] = $this->country->get_all_countries($filter_id_array);
				$data['genres'] = $this->genre->get_all_genres($filter_id_array);
			}
		}

		$this->load->library('pagination');
		$config['base_url'] = '/films/event/'.$event.'/';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 3;
		$config['uri_segment'] = 4;
		$config['per_page'] = DEFAULT_FILMS_PER_PAGE;
		if ($event == "all") {
			$config['total_rows'] = count($data['filmids']);
		} else {
			$config['total_rows'] = count($data['films_all']);
		}
		$this->pagination->initialize($config); 

		$data['start_row'] = $page + 1;
		if ($page + DEFAULT_FILMS_PER_PAGE < $config['total_rows'])  {
			$data['end_row'] = $page + DEFAULT_FILMS_PER_PAGE;
		} else {
			$data['end_row'] = $config['total_rows'];
		}
		$data['total_rows'] = $config['total_rows']; 

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = "Film Listing - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_event";
		$data['selected_page'] = "film_event";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_list', $data);
		$this->load->view('footer', $vars);
	}

	function detail($slug, $type="a")
	{
		$this->load->helper('form');

		// load oEmbed with JSON (for videos only)
		$this->load->library('oembed');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Sponsormodel','sponsor');
		$this->load->model('Locationmodel','location');

		$data['film'] = $this->films->get_film_by_slug($slug);
	
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) { $this->session->set_userdata('festival', $data['film'][0]->festival_id); }

		$data['festival'] = $this->festival->get_current_festival_front();
		if ($this->session->userdata('festival') == $data['festival'][0]->id) {
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			$data['festival'] = $this->festival->get_festival($current_fest);
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['films'] = $this->films->get_all_films($current_fest);
		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

		$film_id_array = convert_to_array3($data['films']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }
		$id = $data['film'][0]->movie_id;
		
		$data['views'] = $this->films->get_film_view($data['film'][0]->movie_id);
		if (count($data['views']) == 0) {
			$view_id = $this->films->add_film_view($data['film'][0]->movie_id, "web", $data['film'][0]->festival_id); $viewcount = 1;
		} else {
			$viewcount = $this->films->update_film_view($data['film'][0]->movie_id, "web", $data['views'][0]->web);
		}
		
		if ($this->logged_in == true) {
			// get internal screenings for logged in preview.
			$data['full_schedule'] = $this->schedule->get_internal_schedule($data['festival'][0]->startdate,$data['festival'][0]->enddate);
			$data['schedule'] = $this->schedule->get_internal_movie_screenings($data['festival'][0]->startdate, $data['festival'][0]->enddate, $data['film'][0]->movie_id);
		} else {
			$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
			$data['schedule'] = $this->schedule->get_movie_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate, $data['film'][0]->movie_id);
		}
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		// Calculate first screening date as a timestamp
		$data['first_scrn_date'] = 0;
		foreach($data['schedule'] as $thisScreening) {
			$screening_date = strtotime($thisScreening->date);
			if ($data['first_scrn_date'] == 0) { $data['first_scrn_date'] = $screening_date; }
			else if ($screening_date < $data['first_scrn_date']) { $data['first_scrn_date'] = $screening_date; }
		}

		$data['countries'] = $this->country->get_movie_countries($id);
		$data['languages'] = $this->language->get_movie_languages($id);
		$data['subtitles'] = $this->language->get_movie_subtitle_languages($id);
		$data['genres']    = $this->genre->get_movie_genres($id);

		$data['premiere_id'] = $data['film'][0]->premiere_id;
		$data['premiere_type'] = $this->filmtype->get_all_type("premiere","asc");

		$data['personnel'] = $this->personnel->get_movie_personnel($id,"");
		$data['personnel_type'] = $this->filmtype->get_all_type_order("personnel","asc");

		$data['director'] = "";
		foreach ($data['personnel'] as $thisPerson) {
			foreach (explode(",",$thisPerson->personnel_roles) as $thisRole) {
				if ($thisRole == 1) {
					if ($thisPerson->lastnamefirst == 0) {
						$data['director'] .= $thisPerson->name." ".$thisPerson->lastname.", ";
					} else {
						$data['director'] .= $thisPerson->lastname." ".$thisPerson->name.", ";
					}
				}
			}
		} $data['director'] = trim($data['director'],", ");
		
		$data['sponsor_logos'] = $this->sponsor->get_all_sponsor_logos($data['film'][0]->movie_id, $current_fest);
		$data['sponsorlogo_type'] = $this->filmtype->get_all_type_order("sponsorlogo", "asc");

		$data['sections'] = $this->section->get_all_sections($current_fest);
		$data['country'] = find_clg_name_link($data['countries'], $data['film'], 'c');
		$data['language'] = find_clg_name_link($data['languages'], $data['film'], 'l');
		$data['subtitle'] = find_clg_name_link($data['subtitles'], $data['film'], 'l');
		$data['genre'] = find_clg_name_link($data['genres'], $data['film'], 'g');
		
		// Get existing photos and videos for this film
		$data['film_photos'] = $this->photovideo->get_all_movie_photos($data['film'][0]->movie_id);
		$data['film_videos'] = $this->photovideo->get_all_movie_videos($data['film'][0]->movie_id);
		if ( count($data['film_videos']) > 0) {
			switch ($data['film_videos'][0]->service_name) {
				case "hulu":
				case "revision3":
				case "qik":
				case "viddler":
				case "vimeo":
				case "youtube": $data['video'] = $this->oembed->call($data['film_videos'][0]->service_name, $data['film_videos'][0]->url_video);
								break;
			
				case "bliptv":
				case "dailymotion":
				case "googlevideo":
				case "metacafe": $data['video'] = $this->oembed->call('oohembed', $data['film_videos'][0]->url_video);
								 break;
			}
		}
		$myhiff_array = array();
		if ($data['film'][0]->myhiff_1 != 0) { $myhiff_array[] = $data['film'][0]->myhiff_1; }
		if ($data['film'][0]->myhiff_2 != 0) { $myhiff_array[] = $data['film'][0]->myhiff_2; }
		if ($data['film'][0]->myhiff_3 != 0) { $myhiff_array[] = $data['film'][0]->myhiff_3; }
		if (count($myhiff_array) != 0) {
			$data['myhiff_photos'] = $this->photovideo->get_all_first_photos($myhiff_array);
		} else {
			$data['myhiff_photos'] = array();
		}

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
		$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
		//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
		$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
		$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

		$vars['title'] = switch_title($data['film'][0]->title_en)." - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "film_detail";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('film_detail', $data);
		$this->load->view('footer', $vars);
	}

	function program($slug = null) {
		if ($slug != null) {
			$this->load->helper('form');

			// load oEmbed with JSON (for videos only)
			//$this->load->library('oembed');

			$this->load->model('Filmsmodel','films');
			$this->load->model('Festivalmodel','festival');
			$this->load->model('Schedulemodel','schedule');
			$this->load->model('Countrymodel','country');
			$this->load->model('Languagemodel','language');
			$this->load->model('Genremodel','genre');
			$this->load->model('Personnelmodel','personnel');
			$this->load->model('Photovideomodel','photovideo');
			$this->load->model('Filmtypesmodel','filmtype');
			$this->load->model('Sectionmodel','section');
			$this->load->model('Sponsormodel','sponsor');
			$this->load->model('Locationmodel','location');

			$data['festival'] = $this->festival->get_current_festival_front();
			// If there is no festival value, set one using this film's festival id.
			if ($this->session->userdata('festival') == FALSE) {
				$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
				$current_fest = $data['festival'][0]->id;
			} else {
				$current_fest = $this->session->userdata('festival');
				if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
			}

			if ($this->logged_in == true) {
				$data['program'] = $this->schedule->get_internal_program_by_slug($slug, $data['festival'][0]->startdate, $data['festival'][0]->enddate);
			} else {
				$data['program'] = $this->schedule->get_program_by_slug($slug, $data['festival'][0]->startdate, $data['festival'][0]->enddate);
			}

			if (count($data['program']) > 0) {
				// If there is no festival value, set one using this film's festival id.
				if ($this->session->userdata('festival') == FALSE) { $this->session->set_userdata('festival', $data['program'][0]->festival_id); }

				$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

				if ($this->logged_in == true) {
					$data['filmids'] = $this->films->get_all_internal_film_ids($current_fest);
				} else {
					$data['filmids'] = $this->films->get_all_film_ids($current_fest);					
				}
				$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);

				$film_id_array = convert_to_array3($data['filmids']);
				if (count($film_id_array) == 0) { $film_id_array = array(0); }

				$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
				$data['schedule'] = $data['full_schedule'];
				$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
				$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

				$program_movie_ids = explode(",", $data['program'][0]->program_movie_ids);
				$data['films'] = $this->films->get_film($program_movie_ids);

				// Sort shorts program films into original order (sql select statement does not come in that order)
				$data['filmstemp'] = array();
				foreach ($program_movie_ids as $movie_id) {
					foreach ($data['films'] as $filminfo) {
						if ($filminfo->movie_id == $movie_id) {
							$data['filmstemp'][] = $filminfo;
						}
					}
				}
				$data['films'] = $data['filmstemp'];

				$data['all_photos'] = $this->photovideo->get_all_first_photos($program_movie_ids);
				$data['all_videos'] = array();

				$data['director'] = array();
				$data['personnel_type'] = $this->filmtype->get_all_type_order("personnel","asc");
				$data['premiere_type'] = $this->filmtype->get_all_type("premiere","asc");

				$data['program_runtime'] = 0;
				foreach ($program_movie_ids as $thisMovieId) {
					foreach ($data['filmids'] as $thisFilm) {
						if ($thisMovieId == $thisFilm->movie_id) {
							$data['program_runtime'] = $data['program_runtime'] + $thisFilm->runtime_int;
						}
					}

					$personnel = $this->personnel->get_movie_personnel($thisMovieId,"");
					$data['director'][$thisMovieId] = "";
					foreach ($personnel as $thisPerson) {
						foreach (explode(",",$thisPerson->personnel_roles) as $thisRole) {
							if ($thisRole == 1) {
								if ($thisPerson->lastnamefirst == 0) {
									$data['director'][$thisMovieId] .= $thisPerson->name." ".$thisPerson->lastname.", ";
								} else {
									$data['director'][$thisMovieId] .= $thisPerson->lastname." ".$thisPerson->name.", ";
								}
							}
						}
					}

					$film_videos = $this->photovideo->get_all_movie_videos($thisMovieId);
					$data['all_videos'][$thisMovieId] = "";
					foreach ($film_videos as $thisVideo) {
						if ($thisMovieId == $thisVideo->movie_id) {
							$data['all_videos'][$thisMovieId] .= $thisVideo->url_video;
						}
					}
				}

				$vars['filmJSON'] = json_encode($data['filmids_search']);
				$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
				$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
				$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
				$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
				$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
				//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
				$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
				$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");

				$vars['title'] = $data['program'][0]->program_name." - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
				$vars['path'] = "/";
				$vars['selected_page'] = "film_detail";
				$vars['admin'] = "NO";
				if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

				$this->load->view('header', $vars);
				$this->load->view('film_program', $data);
				$this->load->view('footer', $vars);
			} else {
				print "No Film/Event Specified!";				
			}
		} else {
			print "No Film/Event Specified!";
		}
	}

	function appview($slug = null) {
		$this->load->model('Filmsmodel','films');

		if ($slug != null) {
			$data['film'] = $this->films->get_film_by_slug($slug);
			if (count($data['film']) > 0) {
				$data['views'] = $this->films->get_film_view($data['film'][0]->movie_id);
				if (count($data['views']) == 0) {
					$view_id = $this->films->add_film_view($data['film'][0]->movie_id, "mobile", $data['film'][0]->festival_id); $viewcount = 1;
				} else {
					$viewcount = $this->films->update_film_view($data['film'][0]->movie_id, "mobile", $data['views'][0]->mobile);
				}
				print $viewcount;
			} else {
				print "No Film/Event Specified!";				
			}
		} else {
			print "No Film/Event Specified!";
		}
	}

}

/* End of file films.php */
/* Location: ./system/application/controllers/films.php */
?>