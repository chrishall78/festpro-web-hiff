<?php
class Api extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("admin/helper_functions.php");
		require_once("films_functions.php");
	}
	
	/* API version 2.0 - Created Jan 20, 2012 - Updated March 19, 2016
	festival
	films
	films_since
	schedule
	schedule_since
	personnel
	personnel_since
	photos
	videos
	updates_since
	
	Testing:
	http://program.hiff.org/api/v1/2014_spring_showcase/
	http://program.hiff.org/api/v2/2014_hiff_fall_festival/
	
	*/
	function v2($festival="none", $api_call="none", $option1="none", $option2="none")
	{
		$API_VERSION = "2.0"; $data['error'] = ""; $site_url = ""; // blank site url because we're using external images
		$data['api_version'] = $API_VERSION; $data['site_url'] = FP_BASE_URL."/api/v2";

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Apiv2model','api');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Photovideomodel','photovideo');

		// Check to see that a valid festival slug was passed in for the first parameter	
		$data['festivals'] = $this->festival->get_all_festivals();
		$festival_id = 0;
		foreach ($data['festivals'] as $thisFestival) {
			// Correct match for festival
			if ($thisFestival->slug == $festival) {
				$festival_id = $thisFestival->id;
				$data['festivalData'] = $this->festival->get_festival($festival_id);
			}
		}

		if (!isset($data['festivalData'])) {
			$data['error'] .= "Invalid or no festival provided.<br>";
		} else {
			$api_array = array();
			$api_array['api_version'] = $API_VERSION;
			$api_array['api_call'] = $api_call;
			switch ($api_call) {
				case "festival":	$api_array['api_data'] = array();
									$api_array['api_data']['name'] = $data['festivalData'][0]->name;
									$api_array['api_data']['year'] = $data['festivalData'][0]->year;
									$api_array['api_data']['startdate'] = $data['festivalData'][0]->startdate;
									$api_array['api_data']['enddate'] = $data['festivalData'][0]->enddate;
									$api_array['api_data']['timezone'] = $data['festivalData'][0]->timezone;
									$api_array['api_data']['updates'] = $data['festivalData'][0]->program_updates;
									break;

				case "films":		// Select all films and then insert director names, countries, languages, genres. 
									$filmsTemp = $this->api->get_all_films($festival_id);
									foreach ($filmsTemp as $thisFilm) {
										$photosTemp = $this->photovideo->get_first_movie_photo($thisFilm->film_id);
										foreach ($photosTemp as $thisPhoto) { $thisFilm->imageUrl = $thisPhoto->url_cropsmall; }
										if (count($photosTemp) > 0) {
											$thisFilm->imageUrl = $site_url.$thisFilm->imageUrl;
										} else {
											$thisFilm->imageUrl = "";
										}

										$thisFilm->titleEnglish = convert_specialchars($thisFilm->titleEnglish);
										$thisFilm->titleRomanized = convert_specialchars($thisFilm->titleRomanized);
										$thisFilm->synopsisLong = convert_specialchars($thisFilm->synopsisLong);
										$thisFilm->synopsisShort = convert_specialchars($thisFilm->synopsisShort);
										$thisFilm->synopsisOriginal = convert_specialchars($thisFilm->synopsisOriginal);

										$movie_id = $thisFilm->film_id; $director_string = "";
										$movie_countries = convert_to_array2($this->country->get_movie_countries($movie_id));
										$movie_languages = convert_to_array2($this->language->get_movie_languages($movie_id));
										$movie_genres = convert_to_array2($this->genre->get_movie_genres($movie_id));
										$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Director");
										foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
										$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Co-Director");
										foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
										$director_string = trim($director_string, ", ");
										
										$c = substr($thisFilm->titleEnglish,0,1);
										switch ($c) {
											case "0":
											case "1":
											case "2":
											case "3":
											case "4":
											case "5":
											case "6":
											case "7":
											case "8":
											case "9":
											case "*":
											case "&":
											case "^":
											case "%":
											case "$":
											case "#":
											case "@":
											case "!":
											case "?":
											case "|":
											case "(":
											case "{":
											case "[":
											case "<":
											case ".":
											case ",":
											case ":":
											case ";":
											case "-":
											case "=":
											case "+":
											case "_":
											case "~":
											case "`":
											case "/":
											case "\\":
											case "\"":
											case "'": $thisFilm->titleAlpha = "#"; break;
											default: $thisFilm->titleAlpha = strtoupper($c); break;
										}

										$thisFilm->countries = implode(", ", $movie_countries);
										$thisFilm->languages = implode(", ", $movie_languages);
										$thisFilm->genres = implode(", ", $movie_genres);
										$thisFilm->directorName = $director_string;

										// Swap Eventival film id for FestPro film id so they don't change everytime there is a sync.
										$thisFilm->film_id = $thisFilm->eventival_film_id;
									}
									$api_array['api_data'] = $filmsTemp;
									//print strtotime("2011-11-01 00:00:00"); // 2011: 1293840000  // 2012: 1325376000
									break;

				case "films_since": if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
									else {
										$filmsTemp = $this->api->get_all_films($festival_id, $option1);
										foreach ($filmsTemp as $thisFilm) {
											$photosTemp = $this->photovideo->get_first_movie_photo($thisFilm->film_id);
											foreach ($photosTemp as $thisPhoto) { $thisFilm->imageUrl = $thisPhoto->url_cropsmall; }
											if (count($photosTemp) > 0) {
												$thisFilm->imageUrl = $site_url.$thisFilm->imageUrl;
											} else {
												$thisFilm->imageUrl = "";
											}

											$thisFilm->titleEnglish = convert_specialchars($thisFilm->titleEnglish);
											$thisFilm->titleRomanized = convert_specialchars($thisFilm->titleRomanized);
											$thisFilm->synopsisLong = convert_specialchars($thisFilm->synopsisLong);
											$thisFilm->synopsisShort = convert_specialchars($thisFilm->synopsisShort);
											$thisFilm->synopsisOriginal = convert_specialchars($thisFilm->synopsisOriginal);

											$movie_id = $thisFilm->film_id; $director_string = "";
											$movie_countries = convert_to_array2($this->country->get_movie_countries($movie_id));
											$movie_languages = convert_to_array2($this->language->get_movie_languages($movie_id));
											$movie_genres = convert_to_array2($this->genre->get_movie_genres($movie_id));
											$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Director");
											foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
											$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Co-Director");
											foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
											$director_string = trim($director_string, ", ");

											$c = substr($thisFilm->titleEnglish,0,1);
											switch ($c) {
												case "0":
												case "1":
												case "2":
												case "3":
												case "4":
												case "5":
												case "6":
												case "7":
												case "8":
												case "9":
												case "*":
												case "&":
												case "^":
												case "%":
												case "$":
												case "#":
												case "@":
												case "!":
												case "?":
												case "|":
												case "(":
												case "{":
												case "[":
												case "<":
												case ".":
												case ",":
												case ":":
												case ";":
												case "-":
												case "=":
												case "+":
												case "_":
												case "~":
												case "`":
												case "/":
												case "\\":
												case "\"":
												case "'": $thisFilm->titleAlpha = "#"; break;
												default: $thisFilm->titleAlpha = strtoupper($c); break;
											}
											$thisFilm->countries = implode(", ", $movie_countries);
											$thisFilm->languages = implode(", ", $movie_languages);
											$thisFilm->genres = implode(", ", $movie_genres);
											$thisFilm->directorName = $director_string;

											// Swap Eventival film id for FestPro film id so they don't change everytime there is a sync.
											$thisFilm->film_id = $thisFilm->eventival_film_id;
										}
										$api_array['api_data'] = $filmsTemp;
									}
									break;

				case "schedule":			$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate);
											foreach ($scheduleTemp as $thisSchedule) {
												$screening_id = $thisSchedule->screening_id; $films_string = ""; $films_slug = "";
												$films_id_array = explode(",", $thisSchedule->films);
												foreach ($films_id_array as $thisFilmId) {
													$filmsTemp = $this->films->get_film($thisFilmId);
													if (count($filmsTemp) > 0) {
														$films_string .= switch_title($filmsTemp[0]->title_en).", ";
														if (count($filmsTemp) == 1) {
															$films_slug = $filmsTemp[0]->slug;
														}
													}
												} $films_string = trim($films_string, ", ");

												$thisSchedule->programName = convert_specialchars($thisSchedule->programName);
												$thisSchedule->films = convert_specialchars($films_string);
												$thisSchedule->slug = $films_slug;
												$thisSchedule->dateString = date("Y-m-d",strtotime($thisSchedule->dateTime))." 00:00:00 ".$data['festivalData'][0]->timezone;
												$thisSchedule->dateTime = $thisSchedule->dateTime." ".$thisSchedule->time." ".$data['festivalData'][0]->timezone;
												$thisSchedule->totalRuntime = convert_time_to_min($thisSchedule->totalRuntime);

												// Swap Eventival film id's for FestPro film id's so they don't change everytime there is a sync.
												$thisSchedule->filmIds = $thisSchedule->eventival_program_movie_ids;
												$eventival_ids = explode(",", $thisSchedule->eventival_program_movie_ids);
												if (count($eventival_ids) > 0) {
													$thisSchedule->film_id = $eventival_ids[0];
												}
												
												unset($thisSchedule->time);
											}
											$api_array['api_data'] = $scheduleTemp;
											break;

				case "schedule_since":	if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate, $option1);
											foreach ($scheduleTemp as $thisSchedule) {
												$screening_id = $thisSchedule->id; $films_string = "";
												$films_id_array = explode(",", $thisSchedule->films);
												foreach ($films_id_array as $thisFilmId) {
													$filmsTemp = $this->films->get_film($thisFilmId);
													$films_string .= switch_title($filmsTemp[0]->title_en).", ";
												} $films_string = trim($films_string, ", ");
												
												$thisSchedule->programName = convert_specialchars($thisSchedule->programName);
												$thisSchedule->films = convert_specialchars($films_string);
												$thisSchedule->dateString = date("Y-m-d",strtotime($thisSchedule->dateTime))." 00:00:00 ".$data['festivalData'][0]->timezone;
												$thisSchedule->dateTime = $thisSchedule->dateTime." ".$thisSchedule->time." ".$data['festivalData'][0]->timezone;
												$thisSchedule->totalRuntime = convert_time_to_min($thisSchedule->totalRuntime);

												// Swap Eventival film id's for FestPro film id's so they don't change everytime there is a sync.
												$thisSchedule->filmIds = $thisSchedule->eventival_program_movie_ids;
												$eventival_ids = explode(",", $thisSchedule->eventival_program_movie_ids);
												if (count($eventival_ids) > 0) {
													$thisSchedule->film_id = $eventival_ids[0];
												}
												
												unset($thisSchedule->time);
											}
											$api_array['api_data'] = $scheduleTemp;
										}
										break;

				case "personnel":		$film_id_array = $eventival_film_id_array = array();
										$festivalFilms = $this->api->get_all_film_ids($festival_id);
										foreach ($festivalFilms as $thisFilm) {
											$eventival_film_id_array[$thisFilm->movie_id] = $thisFilm->eventival_film_id;
											$film_id_array[] = $thisFilm->movie_id;
										}
										$filmTypes = $this->filmtype->get_all_type("personnel", "asc");
										
										$personnelTemp = $this->api->get_all_personnel($film_id_array);
										foreach ($personnelTemp as $thisPerson) {
											$role_string_new = "";
											$role_array = explode(",",$thisPerson->role);
											foreach ($role_array as $thisRole) {
												foreach ($filmTypes as $thisType) {
													if ($thisType->id == $thisRole) { $role_string_new .= $thisType->name.", "; }
												}
											} $role_string_new = trim($role_string_new,", ");
											
											$thisPerson->role = $role_string_new;

											// Swap Eventival film ids for FestPro film ids
											foreach ($eventival_film_id_array as $fp_film_id => $ev_film_id) {
												if ($thisPerson->film_id == $fp_film_id) {
													$thisPerson->film_id = $ev_film_id;
													break(1);
												}
											}
										}
										$api_array['api_data'] = $personnelTemp;
										break;

				case "personnel_since": if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$film_id_array = $eventival_film_id_array = array();
											$festivalFilms = $this->api->get_all_film_ids($festival_id);
											foreach ($festivalFilms as $thisFilm) {
												$eventival_film_id_array[$thisFilm->movie_id] = $thisFilm->eventival_film_id;
												$film_id_array[] = $thisFilm->movie_id;
											}
											$filmTypes = $this->filmtype->get_all_type("personnel", "asc");
										
											$personnelTemp = $this->api->get_all_personnel($film_id_array, $option1);
											foreach ($personnelTemp as $thisPerson) {
												$role_string_new = "";
												$role_array = explode(",",$thisPerson->role);
												foreach ($role_array as $thisRole) {
													foreach ($filmTypes as $thisType) {
														if ($thisType->id == $thisRole) { $role_string_new .= $thisType->name.", "; }
													}
												} $role_string_new = trim($role_string_new,", ");
											
												$thisPerson->role = $role_string_new;

												// Swap Eventival film ids for FestPro film ids
												foreach ($eventival_film_id_array as $fp_film_id => $ev_film_id) {
													if ($thisPerson->film_id == $fp_film_id) {
														$thisPerson->film_id = $ev_film_id;
														break(1);
													}
												}
											}
											$api_array['api_data'] = $personnelTemp;
										}
										break;

				case "sponsors":		$film_id_array = $eventival_film_id_array = array();
										$festivalFilms = $this->api->get_all_film_ids($festival_id);
										foreach ($festivalFilms as $thisFilm) {
											$eventival_film_id_array[$thisFilm->movie_id] = $thisFilm->eventival_film_id;
											$film_id_array[] = $thisFilm->movie_id;
										}
										
										$sponsorTemp = $this->api->get_all_sponsors($film_id_array);
										foreach ($sponsorTemp as &$thisSponsor) {
											if ($thisSponsor->url_logo != "" && !is_null($thisSponsor->url_logo)) {
												$thisSponsor->url_logo = $site_url.$thisSponsor->url_logo;
											} else {
												$thisPhoto->url_logo = "";
											}

											// Swap Eventival film ids for FestPro film ids
											foreach ($eventival_film_id_array as $fp_film_id => $ev_film_id) {
												if ($thisSponsor->film_id == $fp_film_id) {
													$thisSponsor->film_id = $ev_film_id;
													break(1);
												}
											}
										}
										$api_array['api_data'] = $sponsorTemp;
										break;

				case "photos":			$film_id_array = $eventival_film_id_array = array();
										$festivalFilms = $this->api->get_all_film_ids($festival_id);
										foreach ($festivalFilms as $thisFilm) {
											$eventival_film_id_array[$thisFilm->movie_id] = $thisFilm->eventival_film_id;
											$film_id_array[] = $thisFilm->movie_id;
										}
										
										$photosTemp = $this->api->get_all_photos($film_id_array);
										foreach ($photosTemp as &$thisPhoto) {
											$thisPhoto->photo_small = $site_url.$thisPhoto->photo_small;
											$thisPhoto->photo_large = $site_url.$thisPhoto->photo_large;
											if ($thisPhoto->photo_xlarge != "" && !is_null($thisPhoto->photo_xlarge)) {
												$thisPhoto->photo_xlarge = $site_url.$thisPhoto->photo_xlarge;
											} else {
												$thisPhoto->photo_xlarge = "";
											}

											// Swap Eventival film ids for FestPro film ids
											foreach ($eventival_film_id_array as $fp_film_id => $ev_film_id) {
												if ($thisPhoto->film_id == $fp_film_id) {
													$thisPhoto->film_id = $ev_film_id;
													break(1);
												}
											}
										}
										$api_array['api_data'] = $photosTemp;
										break;

				case "videos":			$film_id_array = $eventival_film_id_array = array();
										$festivalFilms = $this->api->get_all_film_ids($festival_id);
										foreach ($festivalFilms as $thisFilm) {
											$eventival_film_id_array[$thisFilm->movie_id] = $thisFilm->eventival_film_id;
											$film_id_array[] = $thisFilm->movie_id;
										}

										$videosTemp = $this->api->get_all_videos($film_id_array);
										foreach ($videosTemp as &$thisVideo) {
											// Swap Eventival film ids for FestPro film ids
											foreach ($eventival_film_id_array as $fp_film_id => $ev_film_id) {
												if ($thisVideo->film_id == $fp_film_id) {
													$thisVideo->film_id = $ev_film_id;
													break(1);
												}
											}
										}
										$api_array['api_data'] = $videosTemp;
										break;

				case "updates_since":	// Look to see if there are updates for films / schedule / personnel / photos / videos
										if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$filmsTemp = $this->api->get_all_films($festival_id, $option1);
											$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate, $option1);

											$film_id_array = array();
											$festivalFilms = $this->films->get_all_film_ids2($festival_id);
											foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }

											$personnelTemp = $this->api->get_all_personnel($film_id_array, $option1);
											$photosTemp = $this->api->get_all_photos($film_id_array, $option1);
											$videosTemp = $this->api->get_all_videos($film_id_array, $option1);

											$updateTemp = array("films"=>count($filmsTemp), "schedule"=>count($scheduleTemp), "personnel"=>count($personnelTemp), "photos"=>count($photosTemp), "videos"=>count($videosTemp));
											$api_array['api_data'] = $updateTemp;
										}
										break;
				case "none": 
				default: $data['error'] .= "API Call is not specified or is incorrect."; break; 
			}
		}
		
		// If there are any errors, output the errors. If not, output the api data.
		if ($data['error'] != "") {
			$data['output'] = $data['error'];
		} else {
			$data['output'] = json_encode($api_array);
		}

		$this->load->view('api/v2', $data);
	}

	function v1($festival="none", $api_call="none", $option1="none", $option2="none")
	{
		$API_VERSION = "1.2"; $data['error'] = ""; $site_url = FP_BASE_URL;
		$data['api_version'] = $API_VERSION; $data['site_url'] = $site_url."/api/v1";

		$this->load->model('Festivalmodel','festival');
		$this->load->model('Apiv1model','api');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Photovideomodel','photovideo');

		// Check to see that a valid festival slug was passed in for the first parameter	
		$data['festivals'] = $this->festival->get_all_festivals();
		$festival_id = 0;
		foreach ($data['festivals'] as $thisFestival) {
			// Correct match for festival
			if ($thisFestival->slug == $festival) {
				$festival_id = $thisFestival->id;
				$data['festivalData'] = $this->festival->get_festival($festival_id);
			}
		}

		if (!isset($data['festivalData'])) {
			$data['error'] .= "Invalid or no festival provided.<br>";
		} else {
			$api_array = array();
			$api_array['api_version'] = $API_VERSION;
			$api_array['api_call'] = $api_call;
			switch ($api_call) {
				case "festival":	$api_array['api_data'] = array();
									$api_array['api_data']['name'] = $data['festivalData'][0]->name;
									$api_array['api_data']['year'] = $data['festivalData'][0]->year;
									$api_array['api_data']['startdate'] = $data['festivalData'][0]->startdate;
									$api_array['api_data']['enddate'] = $data['festivalData'][0]->enddate;
									$api_array['api_data']['timezone'] = $data['festivalData'][0]->timezone;
									$api_array['api_data']['updates'] = $data['festivalData'][0]->program_updates;
									break;

				case "films":		// Select all films and then insert director names, countries, languages, genres. 
									$filmsTemp = $this->api->get_all_films($festival_id);
									foreach ($filmsTemp as $thisFilm) {
										$photosTemp = $this->photovideo->get_first_movie_photo($thisFilm->film_id);
										foreach ($photosTemp as $thisPhoto) { $thisFilm->imageUrl = $thisPhoto->url_cropsmall; }
										if (count($photosTemp) > 0) {
											$thisFilm->imageUrl = $site_url.$thisFilm->imageUrl;
										} else {
											$thisFilm->imageUrl = "";
										}

										$thisFilm->titleEnglish = convert_specialchars($thisFilm->titleEnglish);
										$thisFilm->titleRomanized = convert_specialchars($thisFilm->titleRomanized);
										$thisFilm->synopsisLong = convert_specialchars($thisFilm->synopsisLong);
										$thisFilm->synopsisShort = convert_specialchars($thisFilm->synopsisShort);
										$thisFilm->synopsisOriginal = convert_specialchars($thisFilm->synopsisOriginal);

										$movie_id = $thisFilm->film_id; $director_string = "";
										$movie_countries = convert_to_array2($this->country->get_movie_countries($movie_id));
										$movie_languages = convert_to_array2($this->language->get_movie_languages($movie_id));
										$movie_genres = convert_to_array2($this->genre->get_movie_genres($movie_id));
										$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Director");
										foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
										$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Co-Director");
										foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
										$director_string = trim($director_string, ", ");
										
										$c = substr($thisFilm->titleEnglish,0,1);
										switch ($c) {
											case "0":
											case "1":
											case "2":
											case "3":
											case "4":
											case "5":
											case "6":
											case "7":
											case "8":
											case "9":
											case "*":
											case "&":
											case "^":
											case "%":
											case "$":
											case "#":
											case "@":
											case "!":
											case "?":
											case "|":
											case "(":
											case "{":
											case "[":
											case "<":
											case ".":
											case ",":
											case ":":
											case ";":
											case "-":
											case "=":
											case "+":
											case "_":
											case "~":
											case "`":
											case "/":
											case "\\":
											case "\"":
											case "'": $thisFilm->titleAlpha = "#"; break;
											default: $thisFilm->titleAlpha = strtoupper($c); break;
										}

										$thisFilm->countries = implode(", ", $movie_countries);
										$thisFilm->languages = implode(", ", $movie_languages);
										$thisFilm->genres = implode(", ", $movie_genres);
										$thisFilm->directorName = $director_string;
										
									}
									$api_array['api_data'] = $filmsTemp;
									//print strtotime("2011-11-01 00:00:00"); // 2011: 1293840000  // 2012: 1325376000
									break;

				case "films_since": if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
									else {
										$filmsTemp = $this->api->get_all_films($festival_id, $option1);
										foreach ($filmsTemp as $thisFilm) {
											$photosTemp = $this->photovideo->get_first_movie_photo($thisFilm->film_id);
											foreach ($photosTemp as $thisPhoto) { $thisFilm->imageUrl = $thisPhoto->url_cropsmall; }
											if (count($photosTemp) > 0) {
												$thisFilm->imageUrl = $site_url.$thisFilm->imageUrl;
											} else {
												$thisFilm->imageUrl = "";
											}

											$thisFilm->titleEnglish = convert_specialchars($thisFilm->titleEnglish);
											$thisFilm->titleRomanized = convert_specialchars($thisFilm->titleRomanized);
											$thisFilm->synopsisLong = convert_specialchars($thisFilm->synopsisLong);
											$thisFilm->synopsisShort = convert_specialchars($thisFilm->synopsisShort);
											$thisFilm->synopsisOriginal = convert_specialchars($thisFilm->synopsisOriginal);

											$movie_id = $thisFilm->film_id; $director_string = "";
											$movie_countries = convert_to_array2($this->country->get_movie_countries($movie_id));
											$movie_languages = convert_to_array2($this->language->get_movie_languages($movie_id));
											$movie_genres = convert_to_array2($this->genre->get_movie_genres($movie_id));
											$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Director");
											foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
											$movie_directors = $this->personnel->get_movie_personnel($movie_id, "Co-Director");
											foreach ($movie_directors as $thisDirector) { $director_string .= $thisDirector->name." ".$thisDirector->lastname.", "; }
											$director_string = trim($director_string, ", ");

											$c = substr($thisFilm->titleEnglish,0,1);
											switch ($c) {
												case "0":
												case "1":
												case "2":
												case "3":
												case "4":
												case "5":
												case "6":
												case "7":
												case "8":
												case "9":
												case "*":
												case "&":
												case "^":
												case "%":
												case "$":
												case "#":
												case "@":
												case "!":
												case "?":
												case "|":
												case "(":
												case "{":
												case "[":
												case "<":
												case ".":
												case ",":
												case ":":
												case ";":
												case "-":
												case "=":
												case "+":
												case "_":
												case "~":
												case "`":
												case "/":
												case "\\":
												case "\"":
												case "'": $thisFilm->titleAlpha = "#"; break;
												default: $thisFilm->titleAlpha = strtoupper($c); break;
											}
											$thisFilm->countries = implode(", ", $movie_countries);
											$thisFilm->languages = implode(", ", $movie_languages);
											$thisFilm->genres = implode(", ", $movie_genres);
											$thisFilm->directorName = $director_string;
										}
										$api_array['api_data'] = $filmsTemp;
									}
									break;

				case "schedule":			$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate);
											foreach ($scheduleTemp as $thisSchedule) {
												$screening_id = $thisSchedule->screening_id; $films_string = ""; $films_slug = "";
												$films_id_array = explode(",", $thisSchedule->films);
												foreach ($films_id_array as $thisFilmId) {
													$filmsTemp = $this->films->get_film($thisFilmId);
													if (count($filmsTemp) > 0) {
														$films_string .= switch_title($filmsTemp[0]->title_en).", ";
														if (count($filmsTemp) == 1) {
															$films_slug = $filmsTemp[0]->slug;
														}
													}
												} $films_string = trim($films_string, ", ");

												$thisSchedule->programName = convert_specialchars($thisSchedule->programName);
												$thisSchedule->films = convert_specialchars($films_string);
												$thisSchedule->slug = $films_slug;
												$thisSchedule->dateString = date("Y-m-d",strtotime($thisSchedule->dateTime))." 00:00:00 ".$data['festivalData'][0]->timezone;
												$thisSchedule->dateTime = $thisSchedule->dateTime." ".$thisSchedule->time." ".$data['festivalData'][0]->timezone;
												$thisSchedule->totalRuntime = convert_time_to_min($thisSchedule->totalRuntime);
												
												unset($thisSchedule->time);
											}
											$api_array['api_data'] = $scheduleTemp;
											break;

				case "schedule_since":	if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate, $option1);
											foreach ($scheduleTemp as $thisSchedule) {
												$screening_id = $thisSchedule->id; $films_string = "";
												$films_id_array = explode(",", $thisSchedule->films);
												foreach ($films_id_array as $thisFilmId) {
													$filmsTemp = $this->films->get_film($thisFilmId);
													$films_string .= switch_title($filmsTemp[0]->title_en).", ";
												} $films_string = trim($films_string, ", ");
												
												$thisSchedule->programName = convert_specialchars($thisSchedule->programName);
												$thisSchedule->films = convert_specialchars($films_string);
												$thisSchedule->dateString = date("Y-m-d",strtotime($thisSchedule->dateTime))." 00:00:00 ".$data['festivalData'][0]->timezone;
												$thisSchedule->dateTime = $thisSchedule->dateTime." ".$thisSchedule->time." ".$data['festivalData'][0]->timezone;
												$thisSchedule->totalRuntime = convert_time_to_min($thisSchedule->totalRuntime);
												
												unset($thisSchedule->time);
											}
											$api_array['api_data'] = $scheduleTemp;
										}
										break;

				case "personnel":		$film_id_array = array();
										$festivalFilms = $this->films->get_all_film_ids2($festival_id);
										foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }
										$filmTypes = $this->filmtype->get_all_type("personnel", "asc");
										
										$personnelTemp = $this->api->get_all_personnel($film_id_array);
										foreach ($personnelTemp as $thisPerson) {
											$role_string_new = "";
											$role_array = explode(",",$thisPerson->role);
											foreach ($role_array as $thisRole) {
												foreach ($filmTypes as $thisType) {
													if ($thisType->id == $thisRole) { $role_string_new .= $thisType->name.", "; }
												}
											} $role_string_new = trim($role_string_new,", ");
											
											$thisPerson->role = $role_string_new;
										}
										$api_array['api_data'] = $personnelTemp;
										break;

				case "personnel_since": if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$film_id_array = array();
											$festivalFilms = $this->films->get_all_film_ids2($festival_id);
											foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }
											$filmTypes = $this->filmtype->get_all_type("personnel", "asc");
										
											$personnelTemp = $this->api->get_all_personnel($film_id_array, $option1);
											foreach ($personnelTemp as $thisPerson) {
												$role_string_new = "";
												$role_array = explode(",",$thisPerson->role);
												foreach ($role_array as $thisRole) {
													foreach ($filmTypes as $thisType) {
														if ($thisType->id == $thisRole) { $role_string_new .= $thisType->name.", "; }
													}
												} $role_string_new = trim($role_string_new,", ");
											
												$thisPerson->role = $role_string_new;
											}
											$api_array['api_data'] = $personnelTemp;
										}
										break;

				case "sponsors":		$film_id_array = array();
										$festivalFilms = $this->films->get_all_film_ids2($festival_id);
										foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }
										
										$sponsorTemp = $this->api->get_all_sponsors($film_id_array);
										foreach ($sponsorTemp as &$thisSponsor) {
											if ($thisSponsor->url_logo != "" && !is_null($thisSponsor->url_logo)) {
												$thisSponsor->url_logo = $site_url.$thisSponsor->url_logo;
											} else {
												$thisPhoto->url_logo = "";
											}
										}
										$api_array['api_data'] = $sponsorTemp;
										break;

				case "photos":			$film_id_array = array();
										$festivalFilms = $this->films->get_all_film_ids2($festival_id);
										foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }
										
										$photosTemp = $this->api->get_all_photos($film_id_array);
										foreach ($photosTemp as &$thisPhoto) {
											$thisPhoto->photo_small = $site_url.$thisPhoto->photo_small;
											$thisPhoto->photo_large = $site_url.$thisPhoto->photo_large;
											if ($thisPhoto->photo_xlarge != "" && !is_null($thisPhoto->photo_xlarge)) {
												$thisPhoto->photo_xlarge = $site_url.$thisPhoto->photo_xlarge;
											} else {
												$thisPhoto->photo_xlarge = "";
											}
										}
										$api_array['api_data'] = $photosTemp;
										break;

				case "videos":			$film_id_array = array();
										$festivalFilms = $this->films->get_all_film_ids2($festival_id);
										foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }

										$videosTemp = $this->api->get_all_videos($film_id_array);
										$api_array['api_data'] = $videosTemp;
										break;
				case "updates_since":	// Look to see if there are updates for films / schedule / personnel / photos / videos
										if ($option1 == "none") { $data['error'] .= "The '".$api_call."' API call requires a timestamp as the last parameter."; }
										else {
											$filmsTemp = $this->api->get_all_films($festival_id, $option1);
											$scheduleTemp = $this->api->get_all_screenings($data['festivalData'][0]->startdate, $data['festivalData'][0]->enddate, $option1);

											$film_id_array = array();
											$festivalFilms = $this->films->get_all_film_ids2($festival_id);
											foreach ($festivalFilms as $thisFilm) { $film_id_array[] = $thisFilm->movie_id; }

											$personnelTemp = $this->api->get_all_personnel($film_id_array, $option1);
											$photosTemp = $this->api->get_all_photos($film_id_array, $option1);
											$videosTemp = $this->api->get_all_videos($film_id_array, $option1);

											$updateTemp = array("films"=>count($filmsTemp), "schedule"=>count($scheduleTemp), "personnel"=>count($personnelTemp), "photos"=>count($photosTemp), "videos"=>count($videosTemp));
											$api_array['api_data'] = $updateTemp;
										}
										break;
				case "none": 
				default: $data['error'] .= "API Call is not specified or is incorrect."; break; 
			}
		}
		
		// If there are any errors, output the errors. If not, output the api data.
		if ($data['error'] != "") {
			$data['output'] = $data['error'];
		} else {
			$data['output'] = json_encode($api_array);
		}

		$this->load->view('api/v1', $data);
	}
}

/* End of file api.php */
/* Location: ./system/application/controllers/api.php */
?>