<?php
class Redirect extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$vars['path'] = "/";
		$vars['title'] = "Redirect page";
		$vars['admin'] = "NO";

		$this->load->model('Festivalmodel','festival');

		$festival = $this->festival->get_current_festival_front();
		
		if (count($festival) == 0) {
			// No festivals defined, redirect to installation page
			header('Location: /install/');
		} else {
			// Festival is defined, proceed to films page
			header('Location: /films/');
		}
	}
	
	function change_festival($festival_id) {
		// Set new festival in session data
		$this->session->set_userdata('festival',$festival_id);
		
		// Redirect to film listing page
		header('Location: /films/');
	}

}

/* End of file redirect.php */
/* Location: ./system/application/controllers/redirect.php */
?>