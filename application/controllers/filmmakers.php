<?php
class Filmmakers extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");	
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->helper('file');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Locationmodel','location');

		$data['upcoming'] = $this->festival->get_current_festival();
		$upcoming_fest = $data['upcoming'][0]->id;
		$upcoming_name = $data['upcoming'][0]->name;
		$upcoming_year = $data['upcoming'][0]->year;
		
		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$film_id_array = convert_to_array3($data['filmids']);

		$data['countries'] = convert_to_array2($this->filmtype->get_all_type("country","asc"));
		$data['languages'] = convert_to_array2($this->filmtype->get_all_type("language","asc"));
		$data['genres'] = convert_to_array2($this->filmtype->get_all_type("genre","asc","limit"));
		$data['premiere'] = convert_to_array($this->filmtype->get_all_type("premiere","asc"));
		$data['format'] = convert_to_array($this->filmtype->get_all_type("format","asc","limit"));
		$data['aspectratio'] = convert_to_array($this->filmtype->get_all_type("aspectratio","asc"));
		$data['sound'] = convert_to_array($this->filmtype->get_all_type("sound","asc","limit"));
		$data['color'] = convert_to_array($this->filmtype->get_all_type("color","asc"));
		$data['personnel'] = convert_to_array2($this->filmtype->get_all_type("personnel","asc"));
		
		//$data['recaptcha'] = read_file('./assets/scripts/recaptcha/recaptcha_html.php');


		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		if(count($film_id_array) == 0) {
			$vars['sections'] = $vars['countries'] = $vars['languages'] = $vars['genres'] = $vars['eventtypes'] = array("--");
		} else {
			$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
			$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
			//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
			$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
			$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");
		}
		$vars['title'] = "Film Data Sheet - ".$upcoming_year." ".$upcoming_name;
		$vars['path'] = "/";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('filmmakers', $data);
		$this->load->view('footer', $vars);
	}

	function thank_you()
	{
		// This function will check the captcha, and either reload the form or submit the data and display a thank you page.
		$this->load->helper('form');
		$this->load->library('email');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Filmreviewmodel','filmreview');
		$this->load->model('Locationmodel','location');

		$data['upcoming'] = $this->festival->get_current_festival();
		$upcoming_fest = $data['upcoming'][0]->id;
		$upcoming_name = $data['upcoming'][0]->name;
		$upcoming_year = $data['upcoming'][0]->year;
		
		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$film_id_array = convert_to_array3($data['filmids']);
		
		// Insert data to film review table here.
		$new_id = $this->filmreview->insert_film_review_entry($this->input->post());
		
		if ($new_id != 0) {
			// Send notification email to programming staff
			$to = explode(",", FP_FILMMMAKERS_EMAIL); // defined in films_functions.php
			foreach ($to as &$email) { $email = trim($email); }
			$message  = "A new film has been submitted through the Film Data Sheet for the ".$upcoming_year." ".$upcoming_name.".\n\n";
			$message .= "Film Title: ".$this->input->post('EnglishTitle')."\n";
			$message .= "Contact Name: ".$this->input->post('PressContact')."\n";
			$message .= "Contact Email: ".$this->input->post('PressEmail')."\n";
			$message .= "Submission Date: ".date("m-d-Y",strtotime($this->input->post('SubmittingDate')))."\n\n";
			$message .= "FestPro Notifications\n";

			$this->email->from('info@hiff.org', 'FestPro Notifications');
			$this->email->to($to);
			$this->email->subject("New Film Submission - ".$this->input->post('EnglishTitle'));
			$this->email->message($message);
			if ( ! $this->email->send()) {
				// Generate error
			}

			// Send confirmation email to form submitter
			$message2 = "Hello ".$this->input->post('PressContact').",\n";
			$message2 .= "Thank you for sending information for ".$this->input->post('EnglishTitle')." to the ".$upcoming_year." ".$upcoming_name.". A festival representative will contact you shortly regarding final scheduling and a request for publicity materials.\n\n";
			$message2 .= "Hawaii International Film Festival Programming Team\n";

			$this->email->clear();
			$this->email->from('program@hiff.org', 'HIFF Programming Team');
			$this->email->to($this->input->post('PressEmail'));
			$this->email->subject("Screening Confirmation for ".$this->input->post('EnglishTitle'));
			$this->email->message($message2);
			if ( ! $this->email->send()) {
				// Generate error
			}
		}

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		if(count($film_id_array) == 0) {
			$vars['sections'] = $vars['countries'] = $vars['languages'] = $vars['genres'] = $vars['eventtypes'] = array("--");
		} else {
			$vars['sections'] = convert_to_array_slug($this->section->get_all_movies_sections($current_fest,$film_id_array), "Section");
			$vars['countries'] = convert_to_array_slug($this->filmtype->get_all_movies_type("country","asc",$film_id_array), "Country");
			//$vars['languages'] = convert_to_array_slug($this->filmtype->get_all_movies_type("language","asc",$film_id_array), "Language");
			$vars['genres'] = convert_to_array_slug($this->filmtype->get_all_movies_type("genre","asc",$film_id_array), "Genre");
			$vars['eventtypes'] = convert_to_array_slug($this->filmtype->get_all_events_type("asc",$film_id_array), "Event Type");
		}
		$vars['title'] = "Film Data Sheet - ".$upcoming_year." ".$upcoming_name;
		$vars['path'] = "/";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('filmmakersty', $data);
		$this->load->view('footer', $vars);
	}

	function add_personnel()
	{
		$this->load->model('Filmtypesmodel','filmtype');

		$first_name = $this->input->post("pers-first-name-new");
		$last_name = $this->input->post("pers-last-name-new");
		$lnf = $this->input->post("pers-last-name-first-new");

		$personnel = $this->filmtype->get_all_type("personnel","asc");
		$roles_array = $this->input->post('pers-role-new');
		$roles_string = "";
		$roles_id_string = convert_multiselect_to_string($roles_array);
		foreach ($roles_array as $role) {
			foreach ($personnel as $type) {
				if ($role == $type->id) {
					$roles_string .= $type->name."<br />";
					break;
				}
			}
		}
		
		$pers_num = $this->input->post("PersonnelNum") + 1;

		print "\t\t\t<tr valign=\"top\">\n";
        print "\t\t\t\t<td width=\"4%\"><button id=\"pers-del-".$pers_num."\">X</button></th>\n";
        print "\t\t\t\t<td width=\"32%\">".$first_name."<input type=\"hidden\" name=\"pers-first-name-".$pers_num."\" value=\"".$first_name."\"></td>\n";
        print "\t\t\t\t<td width=\"32%\">".$last_name."<input type=\"hidden\" name=\"pers-last-name-".$pers_num."\" value=\"".$last_name."\"></td>\n";
        print "\t\t\t\t<td width=\"32%\">".$roles_string."<input type=\"hidden\" name=\"pers-role-".$pers_num."\" value=\"".$roles_id_string."\"><input type=\"hidden\" name=\"pers-last-name-first-".$pers_num."\" value=\"".$lnf."\"></td>\n";
        print "\t\t\t</tr>\n";

	}
}

/* End of file filmmakers.php */
/* Location: ./system/application/controllers/filmmakers.php */
?>