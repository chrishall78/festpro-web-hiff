<?php
class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
		// Display standard login form page		
		$this->load->model('Usersmodel','users');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['error_logging_in'] = FALSE;
		$data['logged_out'] = FALSE;
		$data['festival'] = $this->festival->get_current_festival_front();
		$current_fest = $data['festival'][0]->id;
		$data['filmids'] = $this->films->get_all_film_ids($current_fest);

		$vars['allfestivals'] = array();
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['path'] = "/";
		$vars['title'] = "FestPro Administration Login";
		$vars['admin'] = "LOGIN";
		$this->load->helper('form');
		$this->load->view('header_nonav',$vars);
		$this->load->view('login',$data);
		$this->load->view('footer',$vars);
	}

	function verify()
	{
		// Check to see if credentials match the database		
		$this->load->model('Usersmodel','users');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');
		$this->load->library('encrypt');
		
		$userdata = $this->users->get_username($this->input->post('username'));
		
		if ($userdata) {
			$entered_password = $this->input->post('password');
			//print $entered_password."<br>";
			//print $this->encrypt->encode($entered_password);
			$encrypted_password = $this->encrypt->decode($userdata[0]->Password);
			
			$data['error_logging_in'] = FALSE;
			$data['account_disabled'] = FALSE;
			$data['logged_out'] = FALSE;
			
			// If login is valid and user account is enabled, proceed to the Event List
			if ($encrypted_password == $entered_password && $entered_password != "" && $userdata[0]->Enabled == 1) {
			
				$festivaldata = $this->festival->get_current_festival();
				
				// set session here so it remembers the login info?
				$newdata = array(
                   'user_id'  => $userdata[0]->id,
                   'username'  => $userdata[0]->Username,
				   'email' => $userdata[0]->Email,
				   'admin' => $userdata[0]->Admin,
				   'programming' => $userdata[0]->Programming,
				   'limited' => $userdata[0]->LimitedAccess,
				   'festival' => $festivaldata[0]->id,
                   'logged_in' => TRUE
                );
				$this->session->set_userdata($newdata);
				$this->users->set_last_login($userdata[0]->Username);

				$this->load->helper('url');
				redirect('/admin/film_listing', 'refresh');

			} else {

				// If login failed, redirect to the login page with an error message.
				$data['error_logging_in'] = TRUE;
				if ($userdata[0]->Enabled == 0) {
					$data['account_disabled'] = TRUE;
				}

				$data['festival'] = $this->festival->get_current_festival_front();
				$current_fest = $data['festival'][0]->id;
				$data['filmids'] = $this->films->get_all_film_ids($current_fest);

				$vars['allfestivals'] = array();
				$vars['filmJSON'] = json_encode($data['filmids']);
				$vars['path'] = "/";
				$vars['title'] = "FestPro Administration Login";
				$vars['admin'] = "LOGIN";
				$this->load->helper('form');
				$this->load->view('header_nonav',$vars);
				$this->load->view('login',$data);
				$this->load->view('footer',$vars);
			}

		} else {

			// If login failed, redirect to the login page with an error message.
			$data['error_logging_in'] = TRUE;
			$data['account_disabled'] = FALSE;

			$data['festival'] = $this->festival->get_current_festival_front();
			$current_fest = $data['festival'][0]->id;
			$data['filmids'] = $this->films->get_all_film_ids($current_fest);

			$vars['allfestivals'] = array();
			$vars['filmJSON'] = json_encode($data['filmids']);
			$vars['path'] = "/";
			$vars['title'] = "FestPro Administration Login";
			$vars['admin'] = "LOGIN";
			$this->load->helper('form');
			$this->load->view('header_nonav',$vars);
			$this->load->view('login',$data);
			$this->load->view('footer',$vars);
		}
	}

	function logout()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmsmodel','films');

		$data['error_logging_in'] = FALSE;
		$data['logged_out'] = $this->session->userdata('username');

		// Clear session/cookie data here
		$this->session->sess_destroy();

		// Redirect to the login page with a logout message.
		$data['festival'] = $this->festival->get_current_festival_front();
		$current_fest = $data['festival'][0]->id;
		$data['filmids'] = $this->films->get_all_film_ids($current_fest);

		$vars['allfestivals'] = array();
		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['path'] = "/";
		$vars['title'] = "FestPro Administration Login";
		$vars['admin'] = "LOGIN";
		$this->load->helper('form');
		$this->load->view('header_nonav',$vars);
		$this->load->view('login',$data);
		$this->load->view('footer',$vars);
	}

}

/* End of file login.php */
/* Location: ./system/application/controllers/login.php */
?>