<?php
// The default number of films returned for front end display
define("DEFAULT_FILMS_PER_PAGE",36);

define("FP_IOS_APP_ID","");
define("FP_FRONT_SHOW_PRINT_SOURCE",0);
define("FP_FRONT_SHOW_SIMILAR_FILMS",1);
define("FP_FRONT_ENABLE_FACEBOX",0);
define("FP_BASE_URL","http://festpro.hiff.org");

if (!defined("FP_BASE_TIMEZONE")) {
	define("FP_BASE_TIMEZONE","Pacific/Honolulu"); // Also change this in helper_functions.php for admin use
}

define("FP_ADMIN_USERNAME","administrator");
define("FP_ADMIN_PASSWORD","F3st1valPr0");
define("FP_ADMIN_EMAIL","chrishall78@gmail.com");

// Separate email recipients with commas
define("FP_FILMMMAKERS_EMAIL", "nancy@hiff.org");
define("FP_ACCRED_PRESS_EMAIL","eseel@afterbruce.com");
define("FP_ACCRED_INDUSTRY_EMAIL","guestservices@hiff.org");

define("SHARETHIS_PUBLISHER_ID","8b367738-403f-4fee-a74a-90b3679dce65");
define("GOOGLE_ANALYTICS_ID","UA-9659084-1");

/* Mobile view specific functions */
function printScheduleByWeek_mobile($schedule, $activeDay) {
	$days = convert_to_array_schedule3($schedule);
	$weekCount = $dayCount = 1;
	$dateString = $startdate = $finaldate = $firstdate = $lastdate = $prevdate = "";
	$weekfirstdate = "";

	foreach ($days as $thisDay) {
		$timestamp = strtotime($thisDay);
		if ($startdate == "") { $startdate = $timestamp; }
		if ($finaldate == "") { $finaldate = $timestamp; }
		if ($finaldate < $timestamp) { $finaldate = $timestamp; }
	}
	$totalDays = (($finaldate - $startdate) / 86400); $daysLeft = $totalDays;

	foreach ($days as $thisDay) {
		// figure out how many days between this date and previous date
		if ($prevdate == "") { $prevdate = $thisDay; }
		$difference = (strtotime($thisDay) - strtotime($prevdate)) / 86400;
		if ($difference == 0) { $difference = 1; }		
		if (($dayCount + $difference) > 8) { $weekCount++; $dayCount = 1; }	

		// new week has started
		if ($dayCount == 1) {
			$weekfirstdate = $thisDay;			
			$firstdate = strtotime($thisDay);
			$lastdate = strtotime($thisDay) + (6 * 86400);
			$dateString .= "<a href=\"/schedule/week/m/".$weekfirstdate."\" class=\"";
			if (strtotime($activeDay) >= $firstdate && strtotime($activeDay) <= $lastdate) { $dateString .= " activeWeek"; }
			$dateString .= "\">week of ".date("n/d",$firstdate)."-".date("n/d",$lastdate)."</a> ";
		}

		$dayCount = $dayCount + $difference;
		$prevdate = $thisDay;
	}
	
	print "<div class=\"m_schedule_week_links\">";
	print $dateString;
	print "<br clear=\"all\"></div>";
}

function find_screening_status_mobile($date, $url, $rush, $free) {
	$currentdate = date("Y-m-d");
	if ($currentdate > $date) {
		return "<img src=\"/assets/images/tickets-datepassed.png\" height=\"20\" width=\"80\" alt=\"Date Passed\" /></a>";
	} else {
		if ($rush == 1 && $url != "") {
			return "<a href=\"".$url."\" target=\"_blank\"><img src=\"/assets/images/tickets-rush.png\" height=\"20\" width=\"80\" alt=\"Rush Line\" /></a>";
		} elseif ($rush == 1 && $url == "") {
			return "<img src=\"/assets/images/tickets-rush.png\" height=\"20\" width=\"80\" alt=\"Rush Line\" />";
		} else {
			if ($free == 1) {
				return "<img src=\"/assets/images/tickets-free.png\" height=\"20\" width=\"80\" alt=\"Free Screening\" />";
			} else {
				if ($url != "") {
					return "<a href=\"".$url."\" target=\"_blank\"><img src=\"/assets/images/tickets-buy.png\" height=\"20\" width=\"80\" alt=\"Buy Tickets\" /></a>";
				} else {
					return "<img src=\"/assets/images/tickets-soon.png\" height=\"20\" width=\"80\" alt=\"Coming Soon\" />";
				}
			}
		}
	}
}


/* Normal view functions */
function printScheduleByWeek($schedule, $activeDay, $position = "top") {
	$days = convert_to_array_schedule3($schedule);
	$weekCount = $dayCount = 1;
	$dateString = $startdate = $finaldate = $firstdate = $lastdate = $prevdate = "";
	$weekfirstdate = "";

	foreach ($days as $thisDay) {
		$timestamp = strtotime($thisDay);
		if ($startdate == "") { $startdate = $timestamp; }
		if ($finaldate == "") { $finaldate = $timestamp; }
		if ($finaldate < $timestamp) { $finaldate = $timestamp; }
	}
	$totalDays = (($finaldate - $startdate) / 86400); $daysLeft = $totalDays;

	foreach ($days as $thisDay) {
		// figure out how many days between this date and previous date
		if ($prevdate == "") { $prevdate = $thisDay; }
		$difference = (strtotime($thisDay) - strtotime($prevdate)) / 86400;
		if ($difference == 0) { $difference = 1; }		
		if (($dayCount + $difference) > 8) { $weekCount++; $dayCount = 1; }	

		// new week has started
		if ($dayCount == 1) {
			$weekfirstdate = $thisDay;			
			$firstdate = strtotime($thisDay);
			$lastdate = strtotime($thisDay) + (6 * 86400);
			$dateString .= "<a href=\"/schedule/week/a/".$weekfirstdate."\" class=\"";
			$position == "top" ? $dateString .= "ui-corner-top no_bottom_border" : $dateString .= "ui-corner-bottom no_top_border";
			if (strtotime($activeDay) >= $firstdate && strtotime($activeDay) <= $lastdate) { $dateString .= " activeWeek"; }
			$dateString .= "\">week of ".date("n/d",$firstdate)."-".date("n/d",$lastdate)."</a> ";
		}

		$dayCount = $dayCount + $difference;
		$prevdate = $thisDay;
	}
	
	print "<div class=\"schedule_week_links\">";
	print $dateString;
	if ($position == "top") {
		$active_url = explode("/",$_SERVER['REQUEST_URI']);
		print "<div class=\"schedule_week_view\" style=\"float:right;\">View by: <input type=\"radio\" name=\"sched_view\" id=\"sched_list\"";
		if (isset($active_url[3])) {
			if ($active_url[3] == "filter" || $active_url[3] == "week" || $active_url[3] == "") { print " checked=\"checked\""; }
		}
		print "> List <input type=\"radio\" name=\"sched_view\" id=\"sched_grid\"";
		if (isset($active_url[3])) {
			if ($active_url[3] == "grid") { print " checked=\"checked\""; }
		}
		print "> Grid</div>";
	}
	print "</div>";
}

function printScheduleByDay($schedule, $activeDay, $type = "grid", $position = "top") {
	$days = convert_to_array_schedule3($schedule);
	$dateString = ""; $totalDays = count($days); $daysLeft = $totalDays;
	$weekCount = 1; $dayCount = 1; $firstdate = ""; $lastdate = "";

	foreach ($days as $thisDay) {
		if ($dayCount == 1) { $firstdate = $thisDay;}
		
		if ($type == "grid") {
			$dateString .= "<a href=\"/schedule/grid/".$thisDay."\" class=\"";
			$position == "top" ? $dateString .= "ui-corner-top no_bottom_border" : $dateString .= "ui-corner-bottom no_top_border";
		} elseif ($type == "list") {
			$dateString .= "<a href=\"/schedule/filter/".$thisDay."\" class=\"";
			$position == "top" ? $dateString .= "ui-corner-top no_bottom_border" : $dateString .= "ui-corner-bottom no_top_border";
		}
		if ($activeDay == $thisDay) { $dateString .= " activeWeek"; }
		$dateString .= "\">".date("n/d",strtotime($thisDay))."</a> ";
		$dayCount++;
	}
	
	print "<div class=\"schedule_week_links\">";
	if ($position == "top") {
		$active_url = explode("/",$_SERVER['REQUEST_URI']);
		print "<div class=\"schedule_week_view\" style=\"float:none; text-align:right;\">View by: <input type=\"radio\" name=\"sched_view\" id=\"sched_list\"";
		if (isset($active_url[3])) {
			if ($active_url[3] == "filter" || $active_url[3] == "week" || $active_url[3] == "") { print " checked=\"checked\""; }
		}
		print "> List <input type=\"radio\" name=\"sched_view\" id=\"sched_grid\"";
		if (isset($active_url[3])) {
			if ($active_url[3] == "grid") { print " checked=\"checked\""; }
		}
		print "> Grid</div>";
	}
	print $dateString;
	print "</div>";
}


function find_screening_status($date, $url, $rush, $free) {
	date_default_timezone_set(FP_BASE_TIMEZONE);
	$currentdate = date("Y-m-d");
	if ($currentdate > $date) {
		return "<img src=\"/assets/images/tickets-datepassed.png\" height=\"20\" width=\"80\" alt=\"Date Passed\" /></a>";
	} else {
		if ($rush == 1 && $url != "") {
			if (FP_FRONT_ENABLE_FACEBOX) {
				return "<a href=\"#!".$url."\" rel=\"facebox\"><img src=\"/assets/images/tickets-rush.png\" height=\"20\" width=\"80\" alt=\"Rush Line\" /></a>";
			} else {
				return "<a href=\"".$url."\" target=\"_blank\"><img src=\"/assets/images/tickets-rush.png\" height=\"20\" width=\"80\" alt=\"Rush Line\" /></a>";
			}
		} elseif ($rush == 1 && $url == "") {
			return "<img src=\"/assets/images/tickets-rush.png\" height=\"20\" width=\"80\" alt=\"Rush Line\" />";
		} else {
			if ($free == 1) {
				return "<img src=\"/assets/images/tickets-free.png\" height=\"20\" width=\"80\" alt=\"Free Screening\" />";
			} else {
				if ($url != "") {
					if (FP_FRONT_ENABLE_FACEBOX) {
						return "<a href=\"#!".$url."\" rel=\"facebox\"><img src=\"/assets/images/tickets-buy.png\" height=\"20\" width=\"80\" alt=\"Buy Tickets\" /></a>";
					} else {
						return "<a href=\"".$url."\" target=\"_blank\"><img src=\"/assets/images/tickets-buy.png\" height=\"20\" width=\"80\" alt=\"Buy Tickets\" /></a>";
					}
				} else {
					return "<img src=\"/assets/images/tickets-soon.png\" height=\"20\" width=\"80\" alt=\"Coming Soon\" />";
				}
			}
		}
	}
}

function find_cntrylang_name($query, $film) {
	$result = "";
	foreach ($query as $thisTable) {
		if ($thisTable->movie_id == $film[0]->movie_id ) {
			$result .= $thisTable->name.", ";
		}
	}
	return trim($result,", ");
}

function find_clg_name_link($query, $film, $type) {
	$result = "";
	switch ($type) {
		case 'c':
			$link = "/films/country/"; break;
		case 'l':
			$link = "/films/language/"; break;
		case 'g':
			$link = "/films/genre/"; break;		
		default:
			$link = ""; break;
	}

	foreach ($query as $thisTable) {
		if ($thisTable->movie_id == $film[0]->movie_id ) {
			$result .= "<a href=\"".$link.$thisTable->slug."\">".$thisTable->name."</a>, ";
		}
	}
	return trim($result,", ");
}

function find_personnel_name($query, $film) {
	$result = "";
	foreach ($query as $thisTable) {
		if ($thisTable->movie_id == $film[0]->movie_id ) {
			$result .= $thisTable->name." ".$thisTable->lastname.", ";
		}
	}
	return trim($result,", ");
}

function find_personnel_name2($personnel, $film, $id) {
	$result = "";
	if ($personnel->movie_id == $film) {
		$roles = explode(",", $personnel->personnel_roles);
		foreach ($roles as $thisRole) {
			if ($thisRole == $id) {
				$result .= $personnel->name." ".$personnel->lastname.", ";
			}
		}
	}
	return $result;
}

function find_thumb_path($imagepath) {
	$thumbspath = "";
	if ($imagepath != "") {
		$temp = explode("/",$imagepath);
		$count = count($temp)-2;
		$temp[$count] = $temp[$count] . "/thumbs";
	
		$thumbspath = implode("/",$temp);

	}
	return $thumbspath;	
}

function merge_films_programs($films_array, $startdate, $enddate) {
	$CI =&get_instance();
	$CI->load->model('Schedulemodel','schedule');

	$program_array = $CI->schedule->get_all_program_ids($startdate, $enddate);
	foreach ($program_array as $program) { $program->movie_id = 0; $program->runtime_int = 0; }
	$return_array = array_merge($films_array, $program_array);
	usort($return_array, create_function('$a,$b', 'if ($a->label == $b->label){return 0;}else{return$a->label<$b->label?-1:1;}'));
	foreach ($return_array as $film) { $film->label = switch_title($film->label); }

	return $return_array;
}

/* End of file films_functions.php */
/* Location: ./system/application/controllers/films_functions.php */
?>