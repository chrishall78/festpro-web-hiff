<?php
class Wsiw extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");	
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
		$this->load->helper('form');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Countrymodel','country');
		$this->load->model('Languagemodel','language');
		$this->load->model('Genremodel','genre');
		$this->load->model('Personnelmodel','personnel');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Photovideomodel','photovideo');
		$this->load->model('Locationmodel','location');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}

		$data['upload_dir'] = return_img_dir($data['festival'][0]->name, $data['festival'][0]->year);

		$data['full_schedule'] = $this->schedule->get_schedule($data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$data['schedule'] = $data['full_schedule'];
		$data['locations'] = $this->location->get_festival_locations( explode(",", $data['festival'][0]->locations) );
		$vars['venues'] = convert_to_array($data['locations'],"slug","displayname",false);

		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$data['filmids_search'] = merge_films_programs($data['filmids'], $data['festival'][0]->startdate, $data['festival'][0]->enddate);
		$film_id_array = convert_to_array3($data['filmids']);

		$data['film_grid'] = $this->films->get_all_films_sort($current_fest,"wf_movie.title_en","asc", 0, 9999);
		foreach ($data['film_grid'] as $thisFilm) {
			$ctemp = $ltemp = $gtemp = "";
			$ctemp2 = $ltemp2 = $gtemp2 = "";
			$countries = $this->country->get_movie_countries($thisFilm->movie_id);
			$languages = $this->language->get_movie_languages($thisFilm->movie_id);
			$genres = $this->genre->get_movie_genres($thisFilm->movie_id);
			
			foreach ($countries as $thisMovieCountry) { $ctemp .= $thisMovieCountry->slug." "; $ctemp2 .= $thisMovieCountry->name.", "; }
			foreach ($languages as $thisMovieLanguage) { $ltemp .= $thisMovieLanguage->slug." "; $ltemp2 .= $thisMovieLanguage->name.", "; }
			foreach ($genres as $thisMovieGenre) { $gtemp .= $thisMovieGenre->slug." "; $gtemp2 .= $thisMovieGenre->name.", "; }
			
			$thisFilm->country_slug = $ctemp;
			$thisFilm->language_slug = $ltemp;
			$thisFilm->genre_slug = $gtemp;
			$thisFilm->country_name = trim($ctemp2,", ");
			$thisFilm->language_name = trim($ltemp2,", ");
			$thisFilm->genre_name = trim($gtemp2,", ");
		}

		if (count($film_id_array) > 0) {
			$data['all_photos'] = $this->photovideo->get_all_first_photos($film_id_array);
			$data['sections'] = $this->section->get_all_movies_sections($current_fest,$film_id_array);
			$data['countries'] = $this->filmtype->get_all_movies_type("country","asc",$film_id_array);
			$data['languages'] = $this->filmtype->get_all_movies_type("language","asc",$film_id_array);
			$data['genres'] = $this->filmtype->get_all_movies_type("genre","asc",$film_id_array);
			$data['eventtypes'] = $this->filmtype->get_all_events_type("asc",$film_id_array);
		} else {
			$data['all_photos'] = $data['sections'] = $data['countries'] = $data['languages'] = $data['genres'] = $data['eventtypes'] = array();
		}

		$vars['filmJSON'] = json_encode($data['filmids_search']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['sections_sel'] = $vars['countries_sel'] =  $vars['genres_sel'] = $vars['eventtypes_sel'] =  $vars['schedule_sel'] = 0;
		$vars['schedule'] = convert_to_array_schedule($data['full_schedule']);
		
		$vars['sections'] = convert_to_array_slug($data['sections'], "Section");
		$vars['countries'] = convert_to_array_slug($data['countries'], "Country");
		//$vars['languages'] = convert_to_array_slug($data['languages'], "Language");
		$vars['genres'] = convert_to_array_slug($data['genres'], "Genre");
		$vars['eventtypes'] = convert_to_array_slug($data['eventtypes'], "Event Type");
		$vars['title'] = "What Should I Watch?";
		$vars['path'] = "/";
		$vars['admin'] = "NO";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header', $vars);
		$this->load->view('wsiw', $data);
		$this->load->view('footer', $vars);
	}
}

/* End of file wsiw.php */
/* Location: ./system/application/controllers/wsiw.php */
?>