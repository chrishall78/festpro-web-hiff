<?php
class Competitions extends CI_Controller {

	protected $logged_in;

	function __construct()
	{
		parent::__construct();
		$this->logged_in = check_login_front();

		// loads variables & functions common to Film pages
		require_once("films_functions.php");
		require_once("admin/helper_functions.php");
	}
	
	function index($slug = "none")
	{
		$this->load->helper('form');

		// load oEmbed with JSON (for videos only)
		$this->load->library('oembed');

		$this->load->model('Filmsmodel','films');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Schedulemodel','schedule');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Sectionmodel','section');
		$this->load->model('Competitionmodel','competition');

		$data['festival'] = $this->festival->get_current_festival_front();
		// If there is no festival value, set one using this film's festival id.
		if ($this->session->userdata('festival') == FALSE) {
			$this->session->set_userdata( array('festival' => $data['festival'][0]->id) );
			$current_fest = $data['festival'][0]->id;
		} else {
			$current_fest = $this->session->userdata('festival');
			if ($this->session->userdata('festival') != $data['festival'][0]->id) { $data['festival'] = $this->festival->get_festival($current_fest); }
		}
		$data['filmids'] = $this->films->get_all_film_ids($current_fest);
		$film_id_array = convert_to_array3($data['filmids']);
		if (count($film_id_array) == 0) { $film_id_array = array(0); }


		$data['competition'] = $this->competition->get_competition_by_slug($slug);
		if (count($data['competition']) > 0) {
			$data['competition_films'] = $this->competition->get_competition_films($data['competition'][0]->id);
			if (count($data['competition_films']) > 0) {
				$data['video_array'] = array();
				foreach ($data['competition_films'] as $thisFilm) {
					$url_array = explode("/", $thisFilm->video_url);
					$domain = trim($url_array[2],"www.");
					$service_name = explode(".",$domain);
					switch ($service_name[0]) {
						case "hulu":
						case "revision3":
						case "qik":
						case "viddler":
						case "vimeo":
						case "youtube": $data['video_array'][$thisFilm->movie_id] = $this->oembed->call($service_name[0], $thisFilm->video_url);
										break;
					
						case "bliptv":
						case "dailymotion":
						case "googlevideo":
						case "metacafe": $data['video_array'][$thisFilm->movie_id] = $this->oembed->call('oohembed', $thisFilm->video_url);
										 break;
					}
				}
			}
		} else {
			$data['competition_films'] = array();
			$data['video_array'] = array();
		}


		$vars['filmJSON'] = json_encode($data['filmids']);
		$vars['allfestivals'] = convert_festival_to_array2($this->festival->get_all_festivals_front()); $vars['festival_sel'] = $this->session->userdata('festival');
		$vars['title'] = "Juried Competition - ".$data['festival'][0]->year." ".$data['festival'][0]->name;
		$vars['path'] = "/";
		$vars['selected_page'] = "competition";
		$vars['admin'] = "LOGIN";
		if ($this->logged_in == true) { $vars['logged_in'] = $data['logged_in'] = true;  } else { $vars['logged_in'] = $data['logged_in'] = false; }

		$this->load->view('header_nonav', $vars);
		$this->load->view('competitions', $data);
		$this->load->view('footer', $vars);
	}

	function submit_vote() {
		$this->load->model('Competitionmodel','competition');

		$values = array(
			"competition_id" => $this->input->post('competition-id'), 
			"name" => $this->input->post('juror-name'),
			"ranking" => $this->input->post('film-ranking'),
			"notes" => $this->input->post('juror-notes')
		);
		$new_id = $this->competition->add_value($values,"wf_competition_votes");

		if ($new_id != 0) {
			print "<p style=\"font-weight:bold; color:green;\">Thank you, your vote has been recorded.</p>";
		} else {
			print "<p style=\"font-weight:bold; color:red;\">Sorry, an error occurred.</p>";
		}
	}
}

/* End of file competitions.php */
/* Location: ./system/application/controllers/competitions.php */
?>