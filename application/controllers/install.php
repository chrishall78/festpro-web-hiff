<?php
class Install extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once("films_functions.php");	
		require_once("admin/helper_functions.php");
	}
	
	function index()
	{
		$this->load->model('Usersmodel','users');
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmtypesmodel','filmtype');
		$this->load->model('Locationmodel','location');
		$this->load->model('Sectionmodel','section');

		$this->load->helper('form');
		$this->load->library('encrypt');


		// Step 1: Users
		$data['users'] = $this->users->get_all_users();
		if (count($data['users']) == 0) {
			// No users defined, create a super admin account
			$values = array(
				"id" => 1,
				"FirstName" => "FestPro",
				"LastName" => "Administrator",
				"Username" => FP_ADMIN_USERNAME,
				"Password" => $this->encrypt->encode(FP_ADMIN_PASSWORD),
				"Email" => FP_ADMIN_EMAIL,
				"Regdate" => date("Y-m-d"),
				"LastLogin" => NULL,
				"Enabled" => 1,
				"Admin" => 1,
				"Programming" => 1,
				"Guest" => 1
			);
			$new_id = $this->users->add_value($values, "users");
		}
		count($data['users']) == 0 ? $data['step1'] = false : $data['step1'] = true;

		// Step 2: Festivals
		$data['festivals'] = $this->festival->get_all_festivals();
		count($data['festivals']) == 0 ? $data['step2'] = false : $data['step2'] = true;
		
		// Step 3: Screening Locations
		$data['locations'] = $this->location->get_all_locations();
		count($data['locations']) == 0 ? $data['step3'] = false : $data['step3'] = true;
		
		// Step 4: Sections
		$data['sections'] = $this->section->get_all_sections(1);
		count($data['sections']) == 0 ? $data['step4'] = false : $data['step4'] = true;

		// Step 5: Default Photo Upload
		if ($data['step2'] == true) {
			$img_dir = return_img_dir($data['festivals'][0]->name, $data['festivals'][0]->year);
			$photo_path = $img_dir."/001-default-photo.jpg";
			if (FALSE === @file_get_contents($photo_path,0,null,0,1)) {
				// image does not exist
				$data['step5'] = false;
			} else {
				$data['step5'] = true;
				$data['photo_path'] = $photo_path;
			}
		} else {
			$data['step5'] = false;
		}

		// Step 6: Film Types
		$data['countries'] = $this->filmtype->get_all_type("country","asc");
		$data['languages'] = $this->filmtype->get_all_type("language","asc");
		$data['genres'] = $this->filmtype->get_all_type("genre","asc");	
		$data['aspectratio'] = $this->filmtype->get_all_type("aspectratio","asc");
		$data['color'] = $this->filmtype->get_all_type("color","asc");
		$data['distribution'] = $this->filmtype->get_all_type("distribution","asc");
		$data['eventtypes'] = $this->filmtype->get_all_type("event","asc");
		$data['courier'] = $this->filmtype->get_all_type("courier","asc");
		$data['premiere'] = $this->filmtype->get_all_type("premiere","asc");
		$data['soundformat'] = $this->filmtype->get_all_type("sound","asc");
		$data['videoformat'] = $this->filmtype->get_all_type("format","asc");
		if (count($data['countries']) == 0 && count($data['languages']) == 0 && count($data['genres']) == 0 &&
			count($data['aspectratio']) == 0 && count($data['color']) == 0 && count($data['distribution']) == 0 &&
			count($data['eventtypes']) == 0 && count($data['courier']) == 0 && count($data['premiere']) == 0 &&
			count($data['soundformat']) == 0 && count($data['videoformat']) == 0) {
			$data['step6'] = false;
		} else {
			$data['step6'] = true;
		}
		
		
		
		// Installation Complete
		$data['step7'] = false;
		
		
		$data['festivals_array'] = array(0 => "All Festivals");

		$vars['filmJSON'] = json_encode(array());
		$vars['path'] = "/";
		$vars['title'] = "FestPro Installation Page";
		$vars['admin'] = "NO";
		$this->load->view('header_nonav',$vars);
		$this->load->view('install',$data);
		$this->load->view('footer',$vars);
	}

	function add_user()
	{
		$this->load->model('Usersmodel','users');
		$this->load->library('encrypt');
		
		// Encrypt password
		$Password1 = $this->input->post('Password1');
		$Password2 = $this->input->post('Password2');
		$EncryptedPassword = "";
		
		if ($Password1 == $Password2 && $Password1 != "" && $Password2 != "") {
			$EncryptedPassword = $this->encrypt->encode($Password1);
		} else {
			$EncryptedPassword = "xXxPasswordMismatchxXx";
		}
		
		// Check on user access levels - default to admin for first user created.
		$enabled = 1; $limited = 0; $adminaccess = 1;
		
		// Assemble values for user insert
		$values = array(
			"Username" => $this->input->post('Username'),
			"FirstName" => $this->input->post('FirstName'),
			"LastName" => $this->input->post('LastName'),
			"Email" => $this->input->post('Email'),
			"Password" => $EncryptedPassword,
			"Regdate" => date("Y-m-d"),
			"LastLogin" => NULL,
			"Enabled" => $enabled,
			"Admin" => $adminaccess,
			"LimitedAccess" => $limited,
			"Programming" => "1",
			"Guest" => 1
		);
		$UsernameCheck = $this->users->get_username($values['Username']);
		
		if (count($UsernameCheck) >= 1) {
			// Username already exists, abort user add
			print "<div align=\"center\"><b>Sorry, this username is already taken. Please refresh the page and try again.</b></div>";
		} else {
			// Username does not exist, go ahead with user add
			$new_id = $this->users->add_value($values, "users");

			print "<div align=\"center\">Welcome to FestPro, <b>".$values["FirstName"]." ".$values["LastName"]."</b>. Your username is <b>".$values["Username"]."</b>, email address is <b>".$values["Email"]."</b>.</div>";
		}
	}
	
	function add_festival()
	{
		$this->load->model('Festivalmodel','festival');

		$values = array(
			"name" => $this->input->post('festival-new'), 
			"year" => $this->input->post('year-new'),
			"slug" => create_slug($this->input->post('festival-new'), $this->input->post('year-new')),
			"startdate" => $this->input->post('startdate-new'),
			"enddate" => $this->input->post('enddate-new'),
			"locations" => "",
			"current" => 1,
			"currentfront" => 1,
			"showonfront" => 1,
			"program_updates" => "<p>No Updates Yet</p>"			
		);
		// Create image directories for this festival
		$img_dir = return_img_dir($values['name'], $values['year']);

		$new_id = $this->festival->add_value($values,"wf_festival");
		print "<div align=\"center\">A festival for <b>".$values['year']." ".$values['name']."</b> has been created, running from <b>".date("m/d/Y", strtotime($values['startdate']))." to ".date("m/d/Y", strtotime($values['enddate']))."</b>.</div>";
	}

	function add_location()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Locationmodel','location');

		$values = array(
			"name" => $this->input->post('screeninglocation-new'), 
			"displayname" => $this->input->post('displayname-new'),
			"seats" => $this->input->post('seats-new'),
			"format" => ""
		);

		$new_id = $this->location->add_value($values,"wf_screeninglocation");
		$festivals = $this->festival->get_all_festivals();
		$upd_fest_id = $this->festival->update_value(array("locations" => $new_id), "wf_festival", $festivals[0]->id);

		print "<div align=\"center\">The <b>".$values['name']."</b> location has been created and added to the previously created festival.</div>";
	}

	function add_section()
	{
		$this->load->model('Sectionmodel','section');
		$this->load->model('Filmtypesmodel','filmtype');

		$slug = ""; $slug = create_simple_slug($this->input->post('section-new'));
		$counter = 2; $empty = true; $base_slug = $slug;
		while ($empty) {
			$empty = $this->filmtype->check_filmtype_slug($slug,"wf_category");
			if ($empty) { $slug = $base_slug."_".$counter; $counter++; }
		}

		$values = array(
			"name" => $this->input->post('section-new'),
			"slug" => $slug,
			"festival_id" => $this->input->post('sec-festival-new'),
			"Description" => $this->input->post('description-new')
		);
		$new_id = $this->section->add_value($values,"wf_category");
		print "<div align=\"center\">The <b>".$values['name']."</b> section has been created. More can be added once installation is completed.</div>";
	}

	function upload_photo()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Photovideomodel','photovideo');

		$festivals = $this->festival->get_all_festivals();

		$config['upload_path'] = return_img_dir($festivals[0]->name, $festivals[0]->year);
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size']	= '4096';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['remove_spaces']  = true;
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload("photo-upload-new")) {
			$error = array('error' => $this->upload->display_errors('', ''));
			
			print "<div id=\"photo-crop-data\">".$error['error']." Please press the 'Cancel' button and try again.</div>";
			
		} else 	{
			$data = array('upload_data' => $this->upload->data());
			$image_width = $data['upload_data']['image_width'];
			$image_height = $data['upload_data']['image_height'];
			
			if ($image_width == $image_height) {
				$ratiow = 1; $ratioh = 1;
			} else if ($image_width > $image_height) {
				$ratioh = $image_height / $image_width;
				$ratiow = $image_width / $image_height;
			} else if ($image_width < $image_height) {
				$ratiow = $image_width / $image_height;
				$ratioh = $image_height / $image_width;
			}

			$this->load->library('image_lib'); 
			
			// Options for Thumb Resize #1 - 525x300
			$config3['image_library'] = 'gd2';
			$config3['source_image'] = $data['upload_data']['full_path'];
			$config3['new_image'] = $data['upload_data']['file_path']."001-default-photo.jpg";
			$config3['maintain_ratio'] = TRUE;
			if ($image_width > $image_height || $image_width == $image_height) {
				// image is either square or wider than it is tall
				if ( round(525 * $ratioh) >= 300) {
					$config3['width'] = 525;
					$config3['height'] = round(525 * $ratioh);
				} else {
					$config3['width'] = round(300 * $ratiow);
					$config3['height'] = 300;
				}
			} else {
				$config3['width'] = 525;
				$config3['height'] = round(525 * $ratioh);
			}
			
			$this->image_lib->clear();			
			$this->image_lib->initialize($config3);
			if ( ! $this->image_lib->resize()) {
			    echo $this->image_lib->display_errors();
			}
			$croplarge_path = "/".$config['upload_path']."/001-default-photo.jpg";

            print "\t\t\t\t\t<div id=\"photo-crop-data\">\n";
			print "\t\t\t\t\t<input type='hidden' name='x1' value='0' id='x1' />\n";
			print "\t\t\t\t\t<input type='hidden' name='y1' value='0' id='y1' />\n";
			print "\t\t\t\t\t<input type='hidden' name='w' value='525' id='w' />\n";
			print "\t\t\t\t\t<input type='hidden' name='h' value='300' id='h' />\n";
			print "\t\t\t\t\t<input type='hidden' name='orig-filename1' id='orig-filename1' value='".$config3['new_image']."' />\n";
			print "\t\t\t\t\t<input type='hidden' name='photo-new-id' id='photo-new-id' value='0' />\n";
			print "\t\t\t\t\t<input type='hidden' name='photo-new-path' id='photo-new-path' value='".$croplarge_path."' />";
			print "\t\t\t\t</div>\n";
		}
		
		// Set default photo path for festival
		$upd_fest_id = $this->festival->update_value(array("default_photo_url " => $croplarge_path), "wf_festival", $festivals[0]->id);
		
	}

	function upload_photo_crop() {
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Photovideomodel','photovideo');

		$festivals = $this->festival->get_all_festivals();
		$img_dir = return_img_dir($festivals[0]->name, $festivals[0]->year);

		$this->load->library('image_lib'); 
		
		$x1 = $this->input->post('x1');
		$y1 = $this->input->post('y1');
		$width = $this->input->post('w');
		$height = $this->input->post('h');
		if ($width < 525) { $width = 525; }
		if ($height < 300) { $height = 300; }
		$orig_filename1 = $this->input->post('orig-filename1');
		$new_id = $this->input->post('photo-new-id');
		$new_path = $this->input->post('photo-new-path');

		// Options for Crop #1 - 525x300
		$config3['image_library'] = 'gd2';
		$config3['source_image'] = $orig_filename1;
		$config3['create_thumb'] = FALSE;
		$config3['maintain_ratio'] = FALSE;
		$config3['width'] = $width;
		$config3['height'] = $height;
		$config3['x_axis'] = $x1;
		$config3['y_axis'] = $y1;
		
		$this->image_lib->initialize($config3);
		if ( ! $this->image_lib->crop()) {
			echo $this->image_lib->display_errors();
		}

		print "<div><img src=\"".$new_path."\" height=\"300\" border=\"0\" class=\"added_photo\"></div>";
	}

	function add_filmtypes()
	{
		$this->load->model('Festivalmodel','festival');
		$this->load->model('Filmtypesmodel','filmtype');

		require_once(dirname(dirname(__FILE__))."/views/admin/install_filmtypes.php");

		// Countries		
		if ($this->input->post('country-new') == 1) {
			foreach ($wf_type_country as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_country"); }
		} else {
			$values = array("name" => "Sample", "slug" => "sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_country");
		}

		// Languages
		if ($this->input->post('language-new') == 1) {
			foreach ($wf_type_language as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_language"); }
		} else {
			$values = array("name" => "Sample", "slug" => "sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_language");
		}

		// Genres
		if ($this->input->post('genre-new') == 1) {
			foreach ($wf_type_genre as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_genre"); }
		} else {
			$values = array("name" => "Sample", "slug" => "sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_genre");
		}

		// Aspect Ratio
		if ($this->input->post('aspectratio-new') == 1) {
			foreach ($wf_type_aspectratio as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_aspectratio"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_aspectratio");
		}

		// Color
		if ($this->input->post('color-new') == 1) {
			foreach ($wf_type_color as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_color"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_color");
		}

		// Distribution Types
		if ($this->input->post('distribution-new') == 1) {
			foreach ($wf_type_distribution as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_distribution"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_distribution");
		}

		// Event Types
		if ($this->input->post('event-new') == 1) {
			foreach ($wf_type_event as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_event"); }
		} else {
			$values = array("name" => "Sample", "slug" => "sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_event");
		}

		// Package Couriers
		if ($this->input->post('courier-new') == 1) {
			foreach ($wf_type_courier as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_courier"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_courier");
		}

		// Permiere Status
		if ($this->input->post('premiere-new') == 1) {
			foreach ($wf_type_premiere as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_premiere"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_premiere");
		}

		// Sound Format
		if ($this->input->post('soundformat-new') == 1) {
			foreach ($wf_type_sound as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_sound"); }
		} else {
			$values = array("name" => "Sample", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_sound");
		}

		// Video Format
		if ($this->input->post('videoformat-new') == 1) {
			foreach ($wf_type_format as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_format"); }
		} else {
			$values = array("name" => "Sample", "color" => "CCCCCC", "limit" => 0);
			$new_id = $this->festival->add_value($values, "wf_type_format");
		}

		// Personnel
		foreach ($wf_type_personnel as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_personnel"); }

		// Sponsor Logo Types
		foreach ($wf_type_sponsorlogo as $thisFilmType) { $new_id = $this->festival->add_value($thisFilmType, "wf_type_sponsorlogo"); }

		print "<div align=\"center\">The film types have been added.</div>";


		// Installation is complete, set 'current' and 'currentfront' values
		$festivals = $this->festival->get_all_festivals();
		$upd_fest_id = $this->festival->update_value(array("current" => 1, "currentfront" => 1), "wf_festival", $festivals[0]->id);

	}

}

/* End of file install.php */
/* Location: ./system/application/controllers/install.php */
?>