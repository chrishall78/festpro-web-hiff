<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class My_Model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function add_value($values, $tablename) {
		// adds a new record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->insert($tablename);
		return $this->db->insert_id();
	}

	function update_value($values, $tablename, $id) {
		// updates a record
		foreach ($values as $key => $value) { $this->db->set($key,$value); }
		$this->db->where('id',$id);
		$this->db->update($tablename);		
		return $id;
	}

	function delete_value($id, $tablename) {
		// deletes a record
		$this->db->where('id',$id);
		$this->db->delete($tablename);
	}
}
?>