<?php
require_once('recaptchalib.php');

// Get a key from http://recaptcha.net/api/getkey
$publickey = "6LcZSAIAAAAAAHCYQWj_pmwlRE4XiHlxYQea4PTZ";

// the response from reCAPTCHA
$resp = null;

// the error code from reCAPTCHA, if any
$error = "Sorry, getting the CAPTCHA image failed. Please try again.";

// was there a reCAPTCHA response?
print recaptcha_get_html($publickey, $error);
?>
