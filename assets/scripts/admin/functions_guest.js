// Attempt at dialog opener that uses a generic function - needs more work

// Generic dialog opener that is called from other functions
function open_edit_dialog(id, json_obj, textValues, checkValues, dialogName) {
	$.each(json_obj, function (index, element) {
		if (id === element.id) {
			// Update the text form fields
			$.each(textValues, function(key, value) {
				$(key).val(value);
			});

			//window.alert("text values");

			// Update the check boxes
			$.each(checkValues, function(key, value) {
				if (value === "1") {
					$(key).prop('checked',true);
				} else {
					$(key).prop('checked',false);
				}
			});

			//window.alert("check values");
		}
	});

	// open the dialog box
	$(dialogName).dialog('open');
}

function open_edit_eventapp_dialog(id, json_obj) {
	var textValues = {
		"#event-location-edit" : "element.location",
		"#event-date-edit" : "element.date",
		"#event-time-edit" : "element.time"
	};
	var checkValues = {
		"#event-transport-edit" : "element.transport",
		"#event-translator-edit" : "element.translator"
	};
	var dialogName = "#editEventApp-dialog";

	open_edit_dialog(id, json_obj, textValues, checkValues, dialogName);
}


// Guest Listing - Toggle a film value from off to on, or on to off
function toggle_value_guest(field, guest_id) {
	var ajax_load = "<img src='/assets/images/load.gif' alt='loading...' />";
	var loadUrl = "/admin/guest_listing/toggle/"+field+"/"+guest_id;
	var toUpdate = "#"+field+"-"+guest_id;

	$.ajax({
		type: "POST",
		url: loadUrl,
		dataType: "html",
		success: function(msg){
			//window.alert( "Success! " + msg );
			$(toUpdate).html(ajax_load).replaceWith(msg);
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1 );
		}
	});
	return false;
}

// Edit a Guest - Delete this guest
function delete_guest(festivalguest_id, guest_name) {
	var decision = window.confirm("Are you sure you really want to delete "+guest_name+"?\n\nThis will not just delete the guest information, but also any associated:\n- film affiliations\n- hotels/hotel shares\n- flights\n- screening appearances\n- special event appearances.\n\nThis can NOT be undone. If you press cancel nothing will be deleted.");
	if (decision === true) {
		$.ajax({
			success: function(msg, text, xhr) {
				//window.alert(msg + text);
				$('#ajaxFeedback').html(msg);
			},
			complete: function(xhr, text) {
				// redirect to the film listing page after 5 seconds
				var timeoutID = window.setTimeout(function () { window.location="/admin/guest_listing/"; }, 5000);
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: $('#UpdateGuest').serialize(),
			url: '/admin/guest_edit/delete/',
			type: 'POST',
			dataType: 'html'
		});
	}
}

// Edit a Guest - Flights Dialog
function open_edit_flight_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {

			// Update the form fields
			$('#guest_id').val(element.festivalguest_id);
			$('#flight_id_update').val(element.id);

			$('#arr-date-edit').val(element.ArrivalDate);
			$('#arr-time-edit').val(element.ArrivalTime);
			$('#arr-city1-edit').val(element.ArrivalCity);
			$('#arr-city2-edit').val(element.ArrivalCity2);
			$('#arr-airline-edit').val(element.Airline);
			$('#arr-fltnum-edit').val(element.FlightNumber);

			$('#dep-date-edit').val(element.DepartureDate);
			$('#dep-time-edit').val(element.DepartureTime);
			$('#dep-city1-edit').val(element.DepartureCity);
			$('#dep-city2-edit').val(element.DepartureCity2);
			$('#dep-airline-edit').val(element.DepartureAirline);
			$('#dep-fltnum-edit').val(element.DepartureFlightNum);

			$('#flight-comp-edit').val(element.flightcomp_id);
			$('#flight-notes-edit').val(element.Notes);
			$('#flight-cost-edit').val(element.cost);
			$('#flight-mileage-edit').val(element.mileage);

			if (element.pickup === "1") {
				$('#flight-pickup-edit').prop('checked',true);
			} else {
				$('#flight-pickup-edit').prop('checked',false);
			}
			if (element.translator === "1") {
				$('#flight-translator-edit').prop('checked',true);
			} else {
				$('#flight-translator-edit').prop('checked',false);
			}
		}
	});

	// open the dialog box
	$('#editFlight-dialog').dialog('open');
}

// Edit a Guest - Flight dialogs - Update mileage when Flight Comp Dropdown is changed.
function update_flight_mileage(type, json_obj) {
	var flightcomp_id = $('#flight-comp-'+type).val();
	var toUpdate = $('#flight-mileage-'+type);

	if (flightcomp_id === 0) { toUpdate.val(0); }
	else {
		jQuery.each(json_obj, function (index, element) {
			if (flightcomp_id === element.id) { toUpdate.val(element.mileage); }
		});
	}
}

// Edit a Guest - Hotels Dialog
function open_edit_hotel_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {

			// Update the form fields
			$('#guest_id').val(element.festivalguest_id);
			$('#hotel_id_update').val(element.id);

			$('#hotel-edit').val(element.hotel);
			$('#checkin-edit').val(element.checkin);
			$('#checkout-edit').val(element.checkout);
			$('#checkouttime-edit').val(element.checkouttime);
			$('#hotel-comp-edit').val(element.hotelcomp_id);
			$('#comp-nights-edit').val(element.compnights);
			$('#paid-nights-edit').val(element.paidnights);
			$('#hotel-share-edit').val(element.share);
			$('#hotel-notes-edit').val(element.notes);

			var totalnights = (Date.parse(element.checkout) - Date.parse(element.checkin)) / 86400000;
			$('#total-nights-edit').html(totalnights);

			if (element.pickup === "1") {
				$('#hotel-pickup-edit').prop('checked',true);
			} else {
				$('#hotel-pickup-edit').prop('checked',false);
			}
			if (element.translator === "1") {
				$('#hotel-translator-edit').prop('checked',true);
			} else {
				$('#hotel-translator-edit').prop('checked',false);
			}
		}
	});

	// open the dialog box
	$('#editHotel-dialog').dialog('open');
}

// Edit a Guest - Hotel dialogs - Update total nights / paid nights
function update_total_nights(type) {
	var checkin = $('#checkin-'+type).val();
	var checkout = $('#checkout-'+type).val();
	var toUpdate = $('#total-nights-'+type);

	if (checkin !== "" && checkout !== "") {
		var checkin_time = Date.parse(checkin);
		var checkout_time = Date.parse(checkout);
		var time_diff = (checkout_time - checkin_time) / 86400000;

		if (time_diff < 0) { window.alert("Guest can not check out before they have checked in."); }
		else if (time_diff === 0) { window.alert("Check out date can not be the same as the check in date."); }
		else if (time_diff > 0) {
			toUpdate.html(time_diff);
			if ( $('#paid-nights-'+type).val() === "" && $('#comp-nights-'+type).val() === "") {
				$('#paid-nights-'+type).val(time_diff);
				$('#comp-nights-'+type).val(0);
			} else {
				if ( $('#comp-nights-'+type).val() >= 0 ) {
					var comp_nights = $('#comp-nights-'+type).val();
					var paid_nights = time_diff - comp_nights;
					var total_nights = $('#total-nights-'+type).html() * 1;

					if (paid_nights > 0) { // eliminate negative values for paid nights
						$('#paid-nights-'+type).val(paid_nights);
					} else {
						$('#paid-nights-'+type).val(0);
						$('#comp-nights-'+type).val(total_nights);
					}
				}
			}
		}
	}
}

// Edit a Guest - Hotel dialogs - Update paid nights
function update_paid_nights(type) {
	var comp_nights = $('#comp-nights-'+type).val();
	var total_nights = $('#total-nights-'+type).html() * 1;
	var toUpdate = $('#paid-nights-'+type);

	if (comp_nights < total_nights && comp_nights >= 0) { toUpdate.val(total_nights - comp_nights); }
	if (comp_nights > total_nights) { $('#comp-nights-'+type).val(total_nights); toUpdate.val(0); }
	if (comp_nights === total_nights) { toUpdate.val(0); }
	if (comp_nights < 0) { $('#comp-nights-'+type).val(0); toUpdate.val(total_nights); }
}

// Edit a Guest - Hotel dialogs - Update comp nights
function update_comp_nights(type) {
	var comp_nights = $('#comp-nights-'+type).val();
	var paid_nights = $('#paid-nights-'+type).val();
	var total_nights = $('#total-nights-'+type).html() * 1;
	var toUpdate = $('#comp-nights-'+type);

	if (paid_nights < total_nights && paid_nights >= 0) { toUpdate.val(total_nights - paid_nights); }
	if (paid_nights > total_nights) { $('#paid-nights-'+type).val(total_nights); toUpdate.val(0); }
	if (paid_nights === total_nights) { toUpdate.val(0); }
	if (paid_nights < 0) { $('#paid-nights-'+type).val(0); toUpdate.val(total_nights); }

}

// Edit a Guest - Add or Delete screening appearances
function toggle_screening_app(formname, button, type) {
	var button_ref = $(button);
	var url;
	var opposite_type;
	if (type === "add") {
		url = "/admin/guest_edit/add_screening_appearance/";
		opposite_type = "delete";
	} else {
		url = "/admin/guest_edit/delete_screening_appearance/";
		opposite_type = "add";
	}

	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg);
			$(button).closest("tr").replaceWith(msg);
			if (type === "add") {
				// This javascript below is not working properly.
				$(button).button().on('click',function() { toggle_screening_app(formname,button_ref,opposite_type); $(this).removeClass("ui-state-focus"); return false; });
			} else {
				$(button).button().on('click',function() { toggle_screening_app(formname,button_ref,opposite_type); $(this).removeClass("ui-state-focus"); return false; });
			}

			// Need to update the "attend_id" value after an add or a delete
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $(formname).serialize(),
		url: url,
		type: 'POST',
		dataType: 'html'
	});
}

// Edit a Guest - Event Appearances Dialog
function open_edit_eventapp_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {

			// Update the form fields
			$('#guest_id').val(element.festivalguest_id);
			$('#eventapp_id_update').val(element.id);

			$('#event-location-edit').val(element.location);
			$('#event-date-edit').val(element.date);
			$('#event-time-edit').val(element.time);

			if (element.transport === "1") {
				$('#event-transport-edit').prop('checked',true);
			} else {
				$('#event-transport-edit').prop('checked',false);
			}
			if (element.translator === "1") {
				$('#event-translator-edit').prop('checked',true);
			} else {
				$('#event-translator-edit').prop('checked',false);
			}
		}
	});

	// open the dialog box
	$('#editEventApp-dialog').dialog('open');
}

// Edit a Guest - Event Appearance 'add' dialog - Update values with existing event info
function update_eventapp_values(json_obj) {
	var event_id = $('#event-existing-add').val();
	var event_location = $('#event-location-add');
	var event_date = $('#event-date-add');
	var event_time = $('#event-time-add');

	jQuery.each(json_obj, function (index, element) {
		if (event_id === element.id) {
			event_location.val(element.location);
			event_date.val(element.date);
			event_time.val(element.time);
		}
	});
}



// Guest Review - Show/hide guest details
function show_guest_detail(guest_id) {
	var button = "#guest-view-"+guest_id;
	switch ($(button).children("span").html()) {
		case "View": $(button).children("span").html("Close"); break;
		case "Close": $(button).children("span").html("View"); break;
	}

	var infodiv = "#guest-info-"+guest_id;
	$(infodiv).slideToggle();
}

// Guest Review - Open the 'Import Guest' dialog and populate with information
function open_guest_import_dialog(id, name) {
	$('#guest-import-id').val(id);
	$('#guest_name').html(name);

	$('#status-import').val( $('#status-'+id).val() );
	$('#guesttype-import').val( $('#guesttype-'+id).val() );
	$('#fee-import').val( $('#fee-'+id).val() );
	$('#film-import').val( $('#film-'+id).val() );
	$('#company-import').val( $('#company-'+id).val() );

	// open the dialog box
	$('#guestImport-dialog').dialog('open');
}

// Guest Review - update status
function update_guest_review_status(formname) {
	var guest_id = $(formname+' INPUT').first().val();
	$.ajax({
		data: $(formname).serialize(),
		dataType: "html",
		type: "POST",
		url: "/admin/guest_review/update_status/",
		success: function(msg){
			//window.alert( "Success! " + msg );
			$(formname+" select").first().css("background-color","#F2EA1D");
			$("#guest-status-view-"+guest_id).html(msg);
			var timeoutID = window.setTimeout(function () { $(formname+" select").first().css("background-color","#FFFFFF"); }, 2000);
		}, error: function(xhr, msg1, msg2) {
			window.alert( "Failure! " + xhr + msg1 + msg2);
		}
	});

	// Check if status was updated to 'Approved' or 'Declined'
	var guest_status = $(formname+" select option:selected").val();

	$('#guest-email-id').val(guest_id);
	$('#guest-email-status').val(guest_status);
	$('#email_guest_name').html( $('#guestname-'+guest_id).val() );
	$('#email_guest_status').html(guest_status);
	$('#email_guest_email').html( $('#email-'+guest_id).val() );
	$('#emailto-email').val( $('#email-'+guest_id).val() );
	$('#emailfrom-email').val( $('#default-from-email').val() );
	if (guest_status == "Approved: Waived" || guest_status == "Approved: Paid") {
		$('#subject-email').val( $('#approved-subject').val() );
		$('#message-email').val( $('#approved-message').val() );

		$('#guestEmail-dialog').dialog('open');
	} else if (guest_status == "Declined") {
		$('#subject-email').val( $('#declined-subject').val() );
		$('#message-email').val( $('#declined-message').val() );

		$('#guestEmail-dialog').dialog('open');
	}
	return false;
}



// Guest Settings - Sets the value of a text input from a select.
function load_value_guest() {
	var text = $("option:selected", this).text();
	var name = "#"+$(this).attr("name")+"-current";

	if (text !== "--") {
		$(name).val(text);
	} else {
		$(name).val('');
	}
}

// Guest Settings - Sets the value of Guest Type text inputs from a json object.
function load_value_guesttype(id, json_obj) {
	if (id === "0") {
		$("#guesttype-current").val("");
		$("#getsbadge-current").val(0);
		$("#getsbag-current").val(0);
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {
			$("#guesttype-current").val(element.name);
			$("#getsbadge-current").val(element.badge);
			$("#getsbag-current").val(element.giftbag);
			return false;
		}
	});
}

// Guest Settings - Sets the value of Airport text inputs from a json object.
function load_value_airport(id, json_obj) {
	if (id === "0") {
		$("#airportname-current").val("");
		$("#airportcode-current").val("");
		$("#domestic-current").val(0);
		$("#priority-current").val(0);
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {
			$("#airportname-current").val(element.name);
			$("#airportcode-current").val(element.code);
			$("#domestic-current").val(element.domestic);
			$("#priority-current").val(element.priority);
			return false;
		}
	});
}

// Guest Settings - Update values for Guest Type, Deadlines & Fees, Airports, Flight Comp Limits, Flight Comp Options, Hotel Comp Options
function update_guest_setting(formname, toUpdate) {
	var loadUrl = "none";

	switch (formname) {
		case "#updateGuestType": loadUrl = "/admin/guest_settings/update_guesttype/"; break;
		case "#updateAirport": loadUrl = "/admin/guest_settings/update_airport/"; break;
		default: break;
	}

	if (loadUrl !== "none") {
		$.ajax({
			data: $(formname).serialize(),
			dataType: "html",
			type: "POST",
			url: loadUrl,
			async: false,
			success: function(msg){
				//window.alert( "Success! " + msg );
				var name = $(formname+" input").first().val();
				$(formname+" input").first().css("background","none repeat-x scroll 50% top #F2EA1D");
				var timeoutID = window.setTimeout(function () { $(formname+" input").first().css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
				var list = $(formname).find('select').first();
				$(toUpdate+" option:selected").text(name);
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2);
			}
		});
	}
	return false;
}

// Guest Settings - Add a Guest Type (with associated color)
function add_guest_type(type, value, color) {
	var bkgcolor = "#"+color;
	if (color === 0) { bkgcolor = "#F2EA1D"; }

	$.ajax({
		type: "POST",
		url: "/admin/guest_settings/add_type/"+type+"/",
		dataType: "html",
		data: $('#add-'+type).serialize(),
		success: function(msg){
			//window.alert( "Success! " + msg );
			$("#"+type+"-new").val("");
			$("#"+type+"-new").css("background","none repeat-x scroll 50% top "+bkgcolor);
			var timeoutID = window.setTimeout(function () { $("#"+type+"-new").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			$("#"+type).append(msg);
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Guest Settings - Update a Guest Type (with associated color)
function update_guest_type(type, value, color, id) {
	var bkgcolor = "#"+color;
	if (color === 0) { bkgcolor = "#F2EA1D"; }

	$.ajax({
		type: "POST",
		url: "/admin/guest_settings/update_type/"+type+"/",
		dataType: "html",
		data: $('#update-'+type).serialize(),
		success: function(msg){
			//window.alert( "Success! " + msg );
			$("#"+type+"-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
			var timeoutID = window.setTimeout(function () { $("#"+type+"-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			if (color === 0) { } else { $("#"+type+" option[value="+id+"]").attr("title",color); }
			$("#"+type+" option:selected").text(value);
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Guest Settings - Edit Deadlines/Fees Dialog
function open_edit_deadline_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {

			// Update the form fields
			$('#deadlines_id_update').val(element.id);

			$('#accred-festival-current').val(element.festival_id);
			$('#accred-guesttype-current').val(element.guesttype_id);
			$('#early-deadline-current').val(element.early_deadline);
			$('#late-deadline-current').val(element.late_deadline);
			$('#final-deadline-current').val(element.final_deadline);
			$('#early-fee-current').val(element.early_fee);
			$('#late-fee-current').val(element.late_fee);
			$('#final-fee-current').val(element.final_fee);
		}
	});

	// open the dialog box
	$('#editDeadlines-dialog').dialog('open');
}

// Guest Settings - Edit Deadlines/Fees Dialog
function open_edit_hotelcomps_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {

			// Update the form fields
			$('#hotelcomps_id_update').val(element.id);

			$('#hotelc-festival-current').val(element.festival_id);
			$('#hotelc-name-current').val(element.name);
			$('#hotelc-hotel-current').val(element.hotel);
			$('#hotelc-roomtype-current').val(element.roomtype);
			$('#hotelc-limit-current').val(element.limit);
		}
	});

	// open the dialog box
	$('#editHotelcomps-dialog').dialog('open');
}

// Guest Settings - Add new values for Guest Type, Deadlines & Fees, Airports, Flight Comp Limits, Flight Comp Options, Hotel Comp Options
function add_guest_setting(formname, toUpdate) {
	var loadUrl = "none";
	switch (formname) {
		case "#addGuestType": loadUrl = "/admin/guest_settings/add_guesttype/"; break;
		case "#addDeadlinesFees": loadUrl = "/admin/guest_settings/add_deadlines_fees/"; break;
		case "#addHotelComps": loadUrl = "/admin/guest_settings/add_hotelcomps/"; break;
		case "#addAirport": loadUrl = "/admin/guest_settings/add_airport/"; break;
		default: break;
	}

	if (loadUrl !== "none") {
		$.ajax({
			data: $(formname).serialize(),
			dataType: "html",
			type: "POST",
			url: loadUrl,
			success: function(msg){
				//window.alert( "Success! " + msg );
				//textFields = $(formname).find('input[type!=submit]').val("");
				//textAreaFields = $(formname).find('textarea').val("");
				//selectFields = $(formname).find('select').val(0);
				$(formname+" input").first().css("background","none repeat-x scroll 50% top #F2EA1D");
				var timeoutID = window.setTimeout(function () { $(formname+" input").first().css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
				if (formname === "#addDeadlinesFees") {
					$(toUpdate).replaceWith(msg);

					var deadlines_id = $('#deadlines_new_id').val();
					$('#df-'+deadlines_id).button().on('click',function() { open_edit_deadline_dialog(deadlines_id,deadlinesfeesJSON); $(this).removeClass("ui-state-focus"); return false; });
				} else if (formname === "#addHotelComps") {
					$(toUpdate).replaceWith(msg);

					var hotelcomps_id = $('#hotelcomps_new_id').val();
					$('#hc-'+hotelcomps_id).button().on('click',function() { open_edit_hotelcomps_dialog(hotelcomps_id,hotelcompsJSON); $(this).removeClass("ui-state-focus"); return false; });
				} else {
					$(toUpdate).append(msg);
				}
			}, error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2);
			}
		});
	}
	return false;
}