// Film Listing - Toggle a film value from off to on, or on to off
function toggle_value(field, movie_id) {
	var ajax_load = "<img src='/assets/images/load.gif' alt='loading...' />";
	var loadUrl = "/admin/film_listing/toggle/"+field+"/"+movie_id;
	var toUpdate = "#"+field+"-"+movie_id;

	$.ajax({
		type: "POST",
		url: loadUrl,
		dataType: "html",
		success: function(msg){
			//window.alert( "Success! " + msg );
			$(toUpdate).html(ajax_load).replaceWith(msg);
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1 + " " + msg2 );
		}
	});
	return false;
}

// Edit a Film - Delete this film
function delete_film(festivalmovie_id, film_name) {
	var decision = window.confirm("Are you sure you really want to delete "+film_name+"?\n\nThis will not just delete the film information, but also any associated:\n- film personnel\n- screenings\n- photos/videos\n- print traffic records.\n\nThis can not be undone. If you press cancel nothing will be deleted.");
	if (decision === true) {
		$.ajax({
			success: function(msg, text, xhr) {
				//window.alert(msg + text);
				$('#ajaxFeedback').html(msg);
			},
			complete: function(xhr, text) {
				// redirect to the film listing page after 5 seconds
				window.setTimeout(function () { window.location="/admin/film_listing/"; }, 5000);
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: $('#UpdateFilm').serialize(),
			url: '/admin/film_edit/delete/',
			type: 'POST',
			dataType: 'html'
		});
	}
}

// Edit a Film - Film Details Tab - Insert new <select> with country, language or genre values
function add_cntry_lang_genre(field, movie_id) {
	var loadUrl = "/admin/film_edit/add_clg/"+field+"/"+movie_id;
	var toUpdate = "#add"+field+"Here";

	$.ajax({
		type: "POST",
		url: loadUrl,
		dataType: "html",
		success: function(msg){
			$(toUpdate).replaceWith(msg);
			var new_id;
			var existing;
			switch (field) {
				case "Country":
					new_id = $('#addCountryId').val();
					existing = $('INPUT[name=country_ids]').val();
					if (existing === "") {
						$('INPUT[name=country_ids]').val(new_id);
					} else {
						$('INPUT[name=country_ids]').val(existing+','+new_id);
					}
					$('#noCountries').css('display','none');
					break;
				case "Language":
					new_id = $('#addLanguageId').val();
					existing = $('INPUT[name=language_ids]').val();
					if (existing === "") {
						$('INPUT[name=language_ids]').val(new_id);
					} else {
						$('INPUT[name=language_ids]').val(existing+','+new_id);
					}
					$('#noLanguages').css('display','none');
					break;
				case "Genre":
					new_id = $('#addGenreId').val();
					existing = $('INPUT[name=genre_ids]').val();
					if (existing === "") {
						$('INPUT[name=genre_ids]').val(new_id);
					} else {
						$('INPUT[name=genre_ids]').val(existing+','+new_id);
					}
					$('#noGenres').css('display','none');
					break;
			}
			$('#tabs-1').isotope('reLayout');

		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1 + msg2 + xhr );
		}
	});
	return false;
}

// Edit a Film - Film Details Tab - Open the 'Edit Personnel' dialog and populate with information
function open_edit_pers_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#pers-id").val(element.id);
			$("#pers-first-name").val(element.name);
			$("#pers-last-name").val(element.lastname);
			if (element.lastnamefirst === "1") {
				$("#pers-last-name-first").val(element.lastnamefirst);
				$("#pers-last-name-first").prop('checked',true);
			}

			var myObject = (element.personnel_roles).toString().split(',');
			$("#pers-role option").each(function() { $(this).prop('selected',false); });
			$("#pers-role option").each(function() { for (var key in myObject) { if ($(this).val() === myObject[key]) { $(this).prop('selected',true); } } });
		}
	});

	// open the dialog box
	$('#editPersonnel-dialog').dialog('open');
}

// Edit a Film - Multimedia Tab - Delete Photo
function delete_photo(photo_id, container) {
	var decision = window.confirm("Are you sure you really want to delete this photo?");
	if (decision === true) {
		$.ajax({
			success: function(msg, text, xhr) {
				//window.alert(msg + text);
				$('#photo-del-'+photo_id).closest("li").remove();
				$('#ajaxFeedback').html(msg);
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: $(container).sortable('serialize'),
			url: '/admin/film_edit/delete_photo/'+photo_id,
			type: 'POST',
			async: false,
			dataType: 'html'
		});

		// Re-sort photos after deletion.
		$.ajax({
			success: function(msg, text, xhr) {
				//window.alert(msg);
				//$('#ajaxFeedback').html(msg)
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: $(container).sortable('serialize'),
			url: '/admin/film_edit/save_photo_sort/',
			type: 'POST',
			async: false,
			dataType: 'html'
		});
	}
}

// Edit a Film - Multimedia Tab - Open the 'Edit Video' dialog and populate with information
function open_edit_video_dialog(id, url, order) {
	$('#video-id-update').val(id);
	$('#video-trailerurl-update').val(url);
	$('#video-current-order').val(order);

	// open the dialog box
	$('#editVideo-dialog').dialog('open');
}

// Edit a Film - Multimedia Tab - Reset selection for photo cropping
function preview(img, selection) {
	if (!selection.width || !selection.height) { return; }

	/*
	var scaleX = 100 / selection.width;
	var scaleY = 100 / selection.height;
	$('#preview img').css({
		width: Math.round(scaleX * 320 ),
		height: Math.round(scaleY * 213 ),
		marginLeft: -Math.round(scaleX * selection.x1),
		marginTop: -Math.round(scaleY * selection.y1)
	});
	*/

	$('#x1').val(selection.x1);
	$('#y1').val(selection.y1);
	$('#x2').val(selection.x2);
	$('#y2').val(selection.y2);
	$('#w').val(selection.width);
	$('#h').val(selection.height);
}

// Edit a Film - Multimedia Tab - Supposed to fix z-index problems with embedded trailers and dialog boxes - needs more work
function fixZindex() {
	//$(".oembed").css('z-index','1').css('position','relative');
	$("object").css('z-index','101').css('position','relative');
	$("embed").css('z-index','101').css('position','relative');

	var elements = document.getElementsByTagName('embed');
	for(var i=0; i< elements.length; i++){

			elements[i].setAttribute("wmode","transparent");

			var para = document.createElement("param");
			para.setAttribute("name","wmode");
			para.setAttribute("value","transparent");

			elements[i].parentNode.appendChild(para);
	}
}

// Edit a Film - Screening Tab - Toggle a screening value from off to on, or on to off
function toggle_value_screening(field, screening_id) {
	var ajax_load = "<img src='/assets/images/load.gif' alt='loading...' />";
	var loadUrl = "/admin/film_edit/toggle/"+field+"/"+screening_id;
	var toUpdate = "#"+field+"-"+screening_id;

	$.ajax({
		type: "POST",
		url: loadUrl,
		dataType: "html",
		success: function(msg){
			//window.alert( "Success! " + msg );
			$(toUpdate).html(ajax_load).replaceWith(msg);
			var off = false;
			if ( msg.search('cross.png') > -1) { off = true; }

			if (off === true) {
				$(toUpdate).closest("tr").css('background-color','#EEEEEE');
			} else if (field === "Rush" && off === false) {
				$(toUpdate).closest("tr").css('background-color','#FFCCCC');
			} else if (field === "Private" && off === false) {
				$(toUpdate).closest("tr").css('background-color','#DDDDDD');
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1 );
		}
	});
	return false;
}

// Edit a Film - Screening Tab - Adds tix.com link to ticket link field
function add_tixdotcom_link(toUpdate) {
	var url = "http://hiff2.tix.com/Event.aspx?EventCode=";
	//var url = "http://hiff2.tix.com/Event.asp?Event=";
	if ( $(toUpdate).val() === "") {
		$(toUpdate).val(url);
	} else {
		window.alert("Ticket link already exists!");
	}
	return false;
}

// Edit a Film - Screening Tab - Figures out the total runtime from the screening breakdown (add).
function recalculate_total_runtime() {
	var runtime = 0;
	var total_films = $('#total_films').val();

	if ( $('#intro').is(':checked') ) { runtime = runtime + parseInt($('#intro-length').val(),10); }
	if ( $('#fest-trailer').is(':checked') ) { runtime = runtime + parseInt($('#fest-trailer-length').val(),10); }
	if ( $('#sponsor-trailer1').is(':checked') ) { runtime = runtime + parseInt($('#sponsor-trailer1-length').val(),10); }
	if ( $('#sponsor-trailer2').is(':checked') ) { runtime = runtime + parseInt($('#sponsor-trailer2-length').val(),10); }
	for (var x=1; x<=total_films; x++) {
		if (x <= 9) {
			var z = "0";
			runtime = runtime + parseInt($('#scrn-film-'+z+x+' option:selected').attr('title'),10);
		} else {
			runtime = runtime + parseInt($('#scrn-film-'+x+' option:selected').attr('title'),10);
		}
	}
	if ( $('#q-and-a').is(':checked') ) { runtime = runtime + parseInt($('#q-and-a-length').val(),10); }

	var final_runtime = runtime;
	var tempstring = "";
	var hourcount = 0;
	while (runtime >= 60) { runtime = runtime - 60; hourcount++; }

	if (hourcount > 0) { tempstring = tempstring + hourcount+ " hours"; }
	if (hourcount > 0 && runtime !== 0 && runtime < 60) { tempstring = tempstring+" "; }
	if (runtime !== 0 && runtime < 60) { tempstring = tempstring +runtime+" mins";}

	$('#total-runtime').val(final_runtime);
	$('#scrn-runtime-2').html(tempstring);

}

// Edit a Film - Screening Tab - Figures out the total runtime from the screening breakdown (update).
function recalculate_total_runtime_update() {
	var runtime = 0;
	var total_films = $('#total_films_update').val();

	if ( $('#intro-update').is(':checked') ) { runtime = runtime + parseInt($('#intro-length-update').val(),10); }
	if ( $('#fest-trailer-update').is(':checked') ) { runtime = runtime + parseInt($('#fest-trailer-length-update').val(),10); }
	if ( $('#sponsor-trailer1-update').is(':checked') ) { runtime = runtime + parseInt($('#sponsor-trailer1-length-update').val(),10); }
	if ( $('#sponsor-trailer2-update').is(':checked') ) { runtime = runtime + parseInt($('#sponsor-trailer2-length-update').val(),10); }
	//window.alert(total_films);
	for (var x=1; x<=total_films; x++) {
		if (x <= 9) {
			var z = "0";
			runtime = runtime + parseInt($('#scrn-film-update-'+z+x+' option:selected').attr('title'),10);
		} else {
			runtime = runtime + parseInt($('#scrn-film-update-'+x+' option:selected').attr('title'),10);
		}
	}
	if ( $('#q-and-a-update').is(':checked') ) { runtime = runtime + parseInt($('#q-and-a-length-update').val(),10); }

	var final_runtime = runtime;
	var tempstring = "";
	var hourcount = 0;
	while (runtime >= 60) { runtime = runtime - 60; hourcount++; }

	if (hourcount > 0) { tempstring = tempstring + hourcount+ " hours"; }
	if (hourcount > 0 && runtime !== 0 && runtime < 60) { tempstring = tempstring+" "; }
	if (runtime !== 0 && runtime < 60) { tempstring = tempstring +runtime+" mins";}

	$('#total-runtime-update').val(final_runtime);
	$('#scrn-runtime-2-update').html(tempstring);
}

// Edit a Film - Screening Tab - Sets the value of a film's runtime.
function load_runtime() {
	var runtime = $("option:selected", this).attr("title");
	var toUpdate = "#"+$(this).attr("name")+"-length";

	$(toUpdate).html(runtime+" min");

	recalculate_total_runtime();
	recalculate_total_runtime_update();
}

// Edit a Film - Screening Tab - Removes a film from 'Add Screening' dialog box
function remove_screening_film(new_total) {
	var z = "";
	if (new_total <= 9) { z = "0"; }
	$('#scrn-film-'+z+new_total).die('change',function(){});
	$('#scrn-film-del-'+z+new_total).closest('DIV.newFilmEntry').remove();

	$('#total_films').val(parseInt(new_total,10)-1);
	recalculate_total_runtime();
	recalculate_total_runtime_update();
}

// Edit a Film - Screening Tab - Removes a film from 'Edit Screening' dialog box
function remove_screening_film_update(new_total) {
	var z = "";
	if (new_total <= 9) { z = "0"; }
	$('#scrn-film-update-'+z+new_total).die('change',function(){});
	$('#scrn-film-update-del-'+z+new_total).closest('DIV.newFilmEntry').remove();

	$('#total_films_update').val(parseInt(new_total,10)-1);
	recalculate_total_runtime();
	recalculate_total_runtime_update();
}

// Edit a Film - Screening Tab - Adds new film to 'Add Screening' dialog box
function add_screening_film() {
	var new_total = parseInt($('#total_films').val(),10)+1;

	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg);
			var z = "";
			$('#addNewFilmsHere').replaceWith(msg);
			$('#total_films').val(new_total);
			if (new_total <= 9) { z = "0"; }
			$('#scrn-film-'+z+new_total).on('change', load_runtime);
			$('#scrn-film-del-'+z+new_total).on('click', function() { remove_screening_film(new_total); return false; });
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#addScreeningForm').serialize(),
		url: '/admin/film_edit/add_screening_film/'+new_total,
		type: 'POST',
		dataType: 'html'
	});
}

// Edit a Film - Screening Tab - Adds new film to 'Edit Screening' dialog box
function add_screening_film_update(screening_id) {
	var new_total = parseInt($('#total_films_update').val(),10)+1;

	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg);
			var z = "";
			$('#addNewFilmsHere-update').replaceWith(msg);
			$('#total_films_update').val(new_total);
			if (new_total <= 9) { z = "0"; }
			$('#scrn-film-update-'+z+new_total).on('change', load_runtime);
			$('#scrn-film-update-del-'+z+new_total).on('click', function() { remove_screening_film_update(new_total); return false; });
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#editScreeningForm').serialize(),
		url: '/admin/film_edit/add_screening_film_update/'+screening_id+'/'+new_total,
		type: 'POST',
		dataType: 'html',
		async: false
	});
}

// Edit a Film - Screening Tab - Open the 'Edit Screening' dialog and populate with information
function open_edit_scrn_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$('#screening_id_update').val(element.id);
			$('#program_movie_ids_update').val(element.program_movie_ids);

			$("#scrn-program-name-update").val(element.program_name);
			$("#scrn-program-desc-update").val(element.program_desc);
			$("#scrn-notes-update").val(element.notes);

			$("#scrn-date-update").val(element.date);
			$("#scrn-time-update").val(element.time);
			$("#scrn-location-update").val(element.location_id);
			$("#scrn-location2-update").val(element.location_id2);
			$("#scrn-ticket-update").val(element.url);
			$("#scrn-host-update").val(element.host);
			$("#audience-count-update").val(element.AudienceCount);
			$("#screening-revenue-update").val(element.ScreeningRevenue);
			$("#q-and-a-count-update").val(element.QandACount);
			$("INPUT[name=scrn-published-update]").prop('checked',(element.Published === "1") ? true : false );
			$("INPUT[name=scrn-private-update]").prop('checked',(element.Private === "1") ? true : false );
			$("INPUT[name=scrn-rush-update]").prop('checked',(element.Rush === "1") ? true : false );
			$("INPUT[name=scrn-free-update]").prop('checked',(element.Free === "1") ? true : false );

			$("#intro-length-update").val(element.intro_length);
			$("#fest-trailer-length-update").val(element.fest_trailer_length);
			if (element.sponsor_trailer1_name == "") {
				$("#sponsor-trailer1-name-update").val('Sponsor Trailer 1');
			} else {
				$("#sponsor-trailer1-name-update").val(element.sponsor_trailer1_name);
			}
			if (element.sponsor_trailer2_name == "") {
				$("#sponsor-trailer2-name-update").val('Sponsor Trailer 2');
			} else {
				$("#sponsor-trailer2-name-update").val(element.sponsor_trailer2_name);
			}
			$("#sponsor-trailer1-length-update").val(element.sponsor_trailer1_length);
			$("#sponsor-trailer2-length-update").val(element.sponsor_trailer2_length);
			$("#intro-update").prop('checked',(element.intro === "1") ? true : false );
			$("#fest-trailer-update").prop('checked',(element.fest_trailer === "1") ? true : false );
			$("#sponsor-trailer1-update").prop('checked',(element.sponsor_trailer1 === "1") ? true : false );
			$("#sponsor-trailer2-update").prop('checked',(element.sponsor_trailer2 === "1") ? true : false );
			$('#q-and-a-length-update').val(element.q_and_a_length);
			$("#q-and-a-update").prop('checked',(element.q_and_a === "1") ? true : false );

			var screening_ids = (element.program_movie_ids).toString().split(',');
			var total = 0;
			jQuery.each(screening_ids, function () {
				total = parseInt($('#total_films_update').val(),10)+1;
				$('#total_films_update').val(total);
			});

			$.ajax({
				success: function(msg, text, xhr) {
					//window.alert(msg);
					$('#addNewFilmsHere-update').replaceWith(msg);
					var x = 1;
					while (x <= total) {
						var z = "";
						if (x <= 9) { z = "0"; }
						$('#scrn-film-update-'+z+x).on('change', load_runtime);
						if (x !== 1) {
							$(document).on("click", "#scrn-film-update-del-"+z+x, function() { remove_screening_film_update(x); return false; } );
						}
						x++;
					}
				},
				error: function(xhr, msg1, msg2){
					window.alert( "Failure! " + xhr + msg1 + msg2); },
				data: $('#editScreeningForm').serialize(),
				url: '/admin/film_schedule_list/add_screening_film_update_multiple/'+total,
				type: 'POST',
				dataType: 'html',
				async: false
			});
		}
	});

	// open the dialog box
	$('#editScreening-dialog').dialog('open');
}


// Edit a Film - Print Traffic Tab - Delete shipment on page and in database
function delete_shipment(shipment_id) {
	var decision = window.confirm("Are you sure you really want to delete this Shipment?");
	if (decision === true) {
		$.ajax({
			success: function(msg, text, xhr) {
				//window.alert(msg + text);
				$('#shipment-del-'+shipment_id).closest("tr").next("tr").slideUp();
				$('#shipment-del-'+shipment_id).closest("tr").slideUp();
				window.setTimeout(function () {
					$('#shipment-del-'+shipment_id).closest("tr").next("tr").remove();
					$('#shipment-del-'+shipment_id).closest("tr").remove();
				}, 2000);
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			url: '/admin/film_edit/delete_shipment/'+shipment_id,
			type: 'POST',
			dataType: 'html'
		});
	}
}

/*
// Edit a Film - Print Traffic Tab - Add fields for an inbound shipment
function add_inbound_shipment(festivalmovie_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg + text);
			$('#add_shipment_here').replaceWith(msg);

			var shipment_id = $('#added_printtraffic_id').val();
			$("#shipment-del-"+shipment_id).on('click', function() { delete_shipment(shipment_id); return false; });

			// Generate new datepicker instances for inbound shipment
			$("#InShipped-"+shipment_id).datepicker();
			$("#InExpectedArrival-"+shipment_id).datepicker();
			$("#InReceived-"+shipment_id).datepicker();

			var existing_ids = $('input[name=pt_shipment_ids]').val();
			$('input[name=pt_shipment_ids]').val(existing_ids+','+shipment_id);
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#addInboundForm').serialize(),
		url: '/admin/film_edit/add_inbound_shipment/'+festivalmovie_id,
		type: 'POST',
		dataType: 'html'
	});

}

// Edit a Film - Print Traffic Tab - Add fields for an outbound shipment
function add_outbound_shipment(festivalmovie_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg + text);
			$('#add_shipment_here').replaceWith(msg);

			var shipment_id = $('#added_printtraffic_id').val();
			$("#shipment-del-"+shipment_id).on('click', function() { delete_shipment(shipment_id); return false; });

			// Generate new datepicker instances for outbound shipment
			$("#OutScheduleBy-"+shipment_id).datepicker();
			$("#OutShipBy-"+shipment_id).datepicker();
			$("#OutArriveBy-"+shipment_id).datepicker();

			var existing_ids = $('input[name=pt_shipment_ids]').val();
			$('input[name=pt_shipment_ids]').val(existing_ids+','+shipment_id);

		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#addOutboundForm').serialize(),
		url: '/admin/film_edit/add_outbound_shipment/'+festivalmovie_id,
		type: 'POST',
		dataType: 'html'
	});
}
*/

// Edit a Film - Print Traffic Tab - Open the 'Edit Print Shipment' dialog and populate with information
function open_edit_ptraffic_dialog(id, json_obj) {
	var outbound = "0";
	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			outbound = element.outbound;
			$("#confirmed-update").val(element.confirmed);
			$("#printtraffic_id-update").val(element.id);
			$("#festivalmovie_id-update").val(element.festivalmovie_id);
			$("#courier_id-update").val(element.courier_id);
			$("#ShippingFee-update").val(element.ShippingFee);
			$("#AccountNum-update").val(element.AccountNum);
			$("#TrackingNum-update").val(element.TrackingNum);
			$("#wePay-update").val(element.wePay);
			$("#arranged-update").prop('checked',(element.arranged === "1") ? true : false );
			$("#notes-update").val(element.notes);

			if (outbound === "0") {
				if (element.InShipped !== "0000-00-00") { $("#InShipped-update").val(element.InShipped); }
				if (element.InReceived !== "0000-00-00") { $("#InReceived-update").val(element.InReceived); }
				if (element.InExpectedArrival !== "0000-00-00") { $("#InExpectedArrival-update").val(element.InExpectedArrival); }
			} else {
				if (element.OutScheduleBy !== "0000-00-00") { $("#OutScheduleBy-update").val(element.OutScheduleBy); }
				if (element.OutShipBy !== "0000-00-00") { $("#OutShipBy-update").val(element.OutShipBy); }
				if (element.OutShipped !== "0000-00-00") { $("#OutShipped-update").val(element.OutShipped); }
				if (element.OutArriveBy !== "0000-00-00") { $("#OutArriveBy-update").val(element.OutArriveBy); }
			}
		}
	});

	if (outbound === "0") {
        $("#outbound-update").val(1);
        $(".ptEditDialog .InboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
        $(".ptEditDialog .OutboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
	} else {
        $("#outbound-update").val(2);
        $(".ptEditDialog .InboundRow INPUT").prop("disabled",true).addClass("ui-state-error").val("");
        $(".ptEditDialog .OutboundRow INPUT").prop("disabled",false).removeClass("ui-state-error");
	}

	// open the dialog box
	$('#editPrintTraffic-dialog').dialog('open');
}

// New function to toggle Inbound shipment status
function pt_receive_shipment(shipment_id, value) {
	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg + text);
			$('#ptin-receive-'+shipment_id).replaceWith(msg);
			if (value == 0) {
				$('#ptin-receive-'+shipment_id).button().on('click', function() { pt_receive_shipment(shipment_id,1); $(this).removeClass("ui-state-focus"); return false; });
			} else {
				$('#ptin-receive-'+shipment_id).button().on('click', function() { pt_receive_shipment(shipment_id,0); $(this).removeClass("ui-state-focus"); return false; });
			}
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		url: '/admin/film_edit/receive_shipment/'+shipment_id+'/'+value,
		type: 'POST',
		dataType: 'html'
	});
}

// New function to toggle Outbound shipment status
function pt_ship_shipment(shipment_id, value) {
	$.ajax({
		success: function(msg, text, xhr) {
			//window.alert(msg + text);
			$('#ptout-ship-'+shipment_id).replaceWith(msg);
			if (value == 0) {
				$('#ptout-ship-'+shipment_id).button().on('click', function() { pt_ship_shipment(shipment_id,1); $(this).removeClass("ui-state-focus"); return false; });
			} else {
				$('#ptout-ship-'+shipment_id).button().on('click', function() { pt_ship_shipment(shipment_id,0); $(this).removeClass("ui-state-focus"); return false; });
			}
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		url: '/admin/film_edit/ship_shipment/'+shipment_id+'/'+value,
		type: 'POST',
		dataType: 'html'
	});
}



// Grid Schedule - Triggered while an item is being dragged over the grid, highlights the droppable
function sch_over(e, ui) {
	var offsetW = (window.innerWidth / 2) - 500;
	if (offsetW < 0) { offsetW = 10; }
	var y = parseInt(e.pageY,10)-180;
	var x = parseInt(e.pageX,10)-offsetW;
	$('#ajaxFeedback').html($(this).attr("title"));
	$('#ajaxFeedback').css("display","block");
	$('#ajaxFeedback').css("top",y+"px");
	$('#ajaxFeedback').css("left",x+"px");
}

// Grid Schedule - Removes feedback div 1 second after dragging has stopped
function sch_stop(event, ui) {
	window.setTimeout(function () { $('#ajaxFeedback').css("display","none"); }, 1000);
}

// Grid Schedule - Triggered when the new screening icon or an existing screening is dropped on the grid
function sch_drop(e, ui) {
	var drop_array; var drag_array; var screening_id; var location_id; var timestamp;
	var startDate; var startHour; var startMinute;

	if (ui.draggable.attr("id") === "addScreeningIcon") {
		drop_array = $(this).attr("id").split("-");
		location_id = drop_array[1];
		timestamp = drop_array[2];

		startDate = new Date(timestamp*1000);
		startHour = startDate.getHours();
		var am = true;
		if (startHour >= 12) { am = false; }
		if (startHour > 12) { startHour = startHour-12; }
		startMinute = startDate.getMinutes(); if (startMinute < 10) { startMinute = "0"+startMinute; }

		var startDay = startDate.getDate(); if (startDay < 10) { startDay = "0"+startDay; }
		var startMonth = startDate.getMonth()+1; if (startMonth < 10) { startMonth = "0"+startMonth; }
		var startYear = startDate.getYear(); if(startYear < 2000) { startYear = startYear + 1900; }

		$('#scrn-date-new').val(startYear+"-"+startMonth+"-"+startDay);
		if (am === true) {
			$('#scrn-time-new').val(startHour+":"+startMinute+" AM");
		} else {
			$('#scrn-time-new').val(startHour+":"+startMinute+" PM");
		}
		$('#scrn-location-new').val(location_id);
		$('#divToUpdate').val(location_id+'-'+startYear+startMonth+startDay);
		$('#addScreening-dialog').dialog('open');
	} else {
		drop_array = $(this).attr("id").split("-");
		drag_array = ui.draggable.attr("id").split("-");

		screening_id = drag_array[1];
		location_id = drop_array[1];
		timestamp = drop_array[2];

		$.ajax({
			success: function(msg, text, xhr) {
				$('#ajaxFeedback').html(msg);
			},
			error: function(xhr, msg1, msg2) {
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: "",
			url: '/admin/film_schedule/update_screening/'+screening_id+'/'+location_id+'/'+timestamp,
			type: 'POST',
			dataType: 'html'
		});

		var sched_id = ui.draggable.attr("id");
		var screeningLength = ui.draggable.css("width").substring(0,(ui.draggable.css("width").length - 2));
		startDate = new Date(timestamp*1000);
		var endDate =  new Date((timestamp*1000)+(screeningLength*60000));

		var startHour = startDate.getHours();
		if (startHour > 12) { startHour = startHour-12; }

		startMinute = startDate.getMinutes();
		if (startMinute < 10) { startMinute = "0"+startMinute; }

		var endHour = endDate.getHours();
		if (endHour > 12) { endHour = endHour-12; }

		var endMinute = endDate.getMinutes();
		if (endMinute < 10) { endMinute = "0"+endMinute; }

		$("#"+sched_id+" .time").html(startHour+":"+startMinute+" - "+endHour+":"+endMinute);
	}

	$('.date_container .vc').each(function() {
		if ( $(this).droppable( "option", "disabled") === false ) {
			$(this).droppable('disable');
		}
	});
}

// Grid Schedule - Triggered when a screening is dropped on the trash can
function sch_drop_delete(e, ui) {
	if( ui.draggable.attr("id") !== "addScreeningIcon") {
		var decision = window.confirm("Are you sure you want to delete this screening?");
		if (decision === true) {
			var drag_array = ui.draggable.attr("id").split("-");
			var screening_id = drag_array[1];

			$.ajax({
				success: function(msg, text, xhr) {
					$('#ajaxFeedback').html(msg);
					$("#"+ui.draggable.attr("id")).effect('puff',{},1000);
					$('#deleteScreeningIcon IMG').attr("src","/assets/images/icons/trash_full.png");
				},
				error: function(xhr, msg1, msg2){
					window.alert( "Failure! " + xhr + msg1 + msg2); },
				data: "",
				url: '/admin/film_schedule/delete_screening/'+screening_id,
				type: 'POST',
				dataType: 'html'
			});
		}
	} else {
		window.alert('You can\'t delete the new screening icon!');
	}

	$('.date_container .vc').each(function() {
		if ( $(this).droppable( "option", "disabled") === false ) {
			$(this).droppable('disable');
		}
	});
}

// Grid Schedule - Activates droppables for one date when mouse is dragged over the date container
function sch_container_init(event, ui) {
	$('.vc', this).each(function() {
		$(this).droppable({drop:sch_drop, tolerance:'pointer', addClasses:false, greedy:true});
		$(this).droppable('disable');
	});
}

// Grid Schedule - Enables droppables for one date when mouse is dragged over the date container
function sch_container_enable(event, ui) {
	$('.vc', this).each(function() {
		if ( $(this).droppable( "option", "disabled") === true ) {
			$(this).droppable('enable');
		}
	});
}


// List Schedule - Redirect to filtered view by screening location.
function list_schedule_redirect(type, value) {
	if (value !== "0") {
		window.location = '/admin/film_schedule_list/'+type+'/'+value;
	} else {
		window.location = '/admin/film_schedule_list/';
	}
}

// Film Review - Show/hide film details
function show_film_detail(film_id) {
	var button = "#film-view-"+film_id;
	switch ($(button).children("span").html()) {
		case "View": $(button).children("span").html("Close"); break;
		case "Close": $(button).children("span").html("View"); break;
	}

	var infodiv = "#film-info-"+film_id;
	$(infodiv).slideToggle();
}

// Film Review - Show/hide CFE details
function show_cfe_detail(film_id) {
	var button = "#cfe-view-"+film_id;
	switch ($(button).children("span").html()) {
		case "View": $(button).children("span").html("Close"); break;
		case "Close": $(button).children("span").html("View"); break;
	}

	var infodiv = "#cfe-info-"+film_id;
	$(infodiv).slideToggle();
}

// Film Review - Open the 'Film Import' dialog and populate with information
function open_film_import_dialog(id, name) {
	$('#film-import-id').val(id);
	$('#film_name').html(name);
	$('#title_en').val(name);

	// open the dialog box
	$('#filmImport-dialog').dialog('open');
}


// Print Traffic - Set outbound shipment as 'shipped'
function outbound_shipped(shipment_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			var button = $('#shipping-ship-'+shipment_id);
			button.find("span").text("Un-Ship");
			button.removeClass("ship");
			button.addClass("unship");

			var row_class1 = $(button).closest("tr").attr("class");
			var row_class2 = $(button).closest("tr").next("tr").attr("class");
			if (row_class1 === "oddrow") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("oddrow-shipped");
			}
			if (row_class2 === "oddrow2") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("oddrow2-shipped");
			}
			if (row_class1 === "evenrow") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("evenrow-shipped");
			}
			if (row_class2 === "evenrow2") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("evenrow2-shipped");
			}
			// requires jQuery UI to format date
			var todaysDate = $.datepicker.formatDate('mm-dd-yy', new Date());
			button.closest("tr").next("tr").find("span.date").first().text(todaysDate);
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#filmShippingForm-'+shipment_id).serialize(),
		url: '/admin/film_printtraffic/ship_outbound_shipment/'+shipment_id,
		type: 'POST',
		dataType: 'html'
	});
}

// Print Traffic - Set outbound shipment as 'not shipped'
function outbound_unshipped(shipment_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			var button = $('#shipping-ship-'+shipment_id);
			button.find("span").text("Ship");
			button.removeClass("unship");
			button.addClass("ship");

			var row_class1 = $(button).closest("tr").attr("class");
			var row_class2 = $(button).closest("tr").next("tr").attr("class");
			if (row_class1 === "oddrow-shipped") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("oddrow");
			}
			if (row_class2 === "oddrow2-shipped") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("oddrow2");
			}
			if (row_class1 === "evenrow-shipped") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("evenrow");
			}
			if (row_class2 === "evenrow2-shipped") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("evenrow2");
			}
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#filmShippingForm-'+shipment_id).serialize(),
		url: '/admin/film_printtraffic/unship_outbound_shipment/'+shipment_id,
		type: 'POST',
		dataType: 'html'
	});
}

// Print Traffic - Set inbound shipment as 'received'
function inbound_received(shipment_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			var button = $('#receiving-receive-'+shipment_id);
			button.find("span").text("Un-Receive");
			button.removeClass("receive");
			button.addClass("unreceive");

			var row_class1 = $(button).closest("tr").attr("class");
			var row_class2 = $(button).closest("tr").next("tr").attr("class");
			if (row_class1 === "oddrow") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("oddrow-received");
			}
			if (row_class2 === "oddrow2") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("oddrow2-received");
			}
			if (row_class1 === "evenrow") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("evenrow-received");
			}
			if (row_class2 === "evenrow2") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("evenrow2-received");
			}

			// requires jQuery UI to format date
			var todaysDate = $.datepicker.formatDate('mm-dd-yy', new Date());
			button.closest("tr").next("tr").find("span.date").first().text(todaysDate);
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#filmReceivingForm-'+shipment_id).serialize(),
		url: '/admin/film_printtraffic/receive_inbound_shipment/'+shipment_id,
		type: 'POST',
		dataType: 'html'
	});
}

// Print Traffic - Set inbound shipment as 'not received'
function inbound_unreceived(shipment_id) {
	$.ajax({
		success: function(msg, text, xhr) {
			var button = $('#receiving-receive-'+shipment_id);
			button.find("span").text("Receive");
			button.removeClass("unreceive");
			button.addClass("receive");

			var row_class1 = $(button).closest("tr").attr("class");
			var row_class2 = $(button).closest("tr").next("tr").attr("class");
			if (row_class1 === "oddrow-received") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("oddrow");
			}
			if (row_class2 === "oddrow2-received") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("oddrow2");
			}
			if (row_class1 === "evenrow-received") {
				$(button).closest("tr").removeClass();
				$(button).closest("tr").addClass("evenrow");
			}
			if (row_class2 === "evenrow2-received") {
				$(button).closest("tr").next("tr").removeClass();
				$(button).closest("tr").next("tr").addClass("evenrow2");
			}
		},
		error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2); },
		data: $('#filmReceivingForm-'+shipment_id).serialize(),
		url: '/admin/film_printtraffic/unreceive_inbound_shipment/'+shipment_id,
		type: 'POST',
		dataType: 'html'
	});
}

// Film Settings - Add new values for Section, Sponsor, Sponsor Logo, Location, Festival
function add_film_setting(formname, toUpdate) {
	var loadUrl = "none";
	switch (formname) {
		case "#addSection": loadUrl = "/admin/film_settings/add_section/"; break;
		case "#addSponsor": loadUrl = "/admin/film_settings/add_sponsor/"; break;
		case "#addSponsorLogo": loadUrl = "/admin/film_settings/add_sponsor_logo/"; break;
		case "#add-sponsorlogo": loadUrl = "/admin/film_settings/add_sponsor_type/"; break;
		case "#addLocation": loadUrl = "/admin/film_settings/add_location/"; break;
		case "#addVenue": loadUrl = "/admin/film_settings/add_venue/"; break;
		case "#addFestival": loadUrl = "/admin/film_settings/add_festival/"; break;
		case "#addCompetition": loadUrl = "/admin/film_settings/add_competition/"; break;
		case "#addCompetitionFilms": loadUrl = "/admin/film_settings/add_competition_film/"; break;
		default: break;
	}

	if (loadUrl !== "none") {
		$.ajax({
			data: $(formname).serialize(),
			dataType: "html",
			type: "POST",
			url: loadUrl,
			success: function(msg){
				//window.alert( "Success! " + msg );
				var textFields = $(formname).find('input[type!=submit]').val("");
				var textAreaFields = $(formname).find('textarea').val("");
				var selectFields = $(formname).find('select').val(0);
				$(formname+" input").first().css("background","none repeat-x scroll 50% top #F2EA1D");
				window.setTimeout(function () { $(formname+" input").first().css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);

				if (formname !== "#addCompetitionFilms") {
					$(toUpdate).prepend(msg);
				} else {
					$(toUpdate).replaceWith(msg);
				}
				if (formname === "#addSponsor") { $('#sponsor-logo').prepend(msg); }
				if (formname === "#add-sponsorlogo") { $('#sponsorlogo-type-new').prepend(msg); $('#sponsorlogo-type-update').prepend(msg); }
				if (formname === "#addLocation") { $('#locations-new').prepend(msg); $('#locations-current').prepend(msg); $('#venuelocations-new').prepend(msg); $('#venuelocations-current').prepend(msg); }
				if (formname === "#addVenue") {  }
				if (formname === "#addFestival") { $('#sec-festival-new').prepend(msg); $('#sec-festival-current').prepend(msg); }
				if (formname === "#addCompetition") { $('#compfilm-compname-new').prepend(msg); }
			}, error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2);
			}
		});
	}
	return false;
}

// Film Settings - Update values for Section, Sponsor, Location, Festival
function update_film_setting(formname, toUpdate) {
	var loadUrl = "none";
	switch (formname) {
		case "#updateSection": loadUrl = "/admin/film_settings/update_section/"; break;
		case "#updateSponsor": loadUrl = "/admin/film_settings/update_sponsor/"; break;
		case "#updateLocation": loadUrl = "/admin/film_settings/update_location/"; break;
		case "#updateVenue": loadUrl = "/admin/film_settings/update_venue/"; break;
		case "#updateFestival": loadUrl = "/admin/film_settings/update_festival/"; break;
		case "#update-sponsortype": loadUrl = "/admin/film_settings/update_sponsor_type/"; break;
		case "#updateCompetition": loadUrl = "/admin/film_settings/update_competition/"; break;
		case "#updateCompetitionFilms": loadUrl = "/admin/film_settings/update_competition_film/"; break;
		default: break;
	}

	if (loadUrl !== "none") {
		$.ajax({
			data: $(formname).serialize(),
			dataType: "html",
			type: "POST",
			url: loadUrl,
			async: false,
			success: function(msg){
				//window.alert( "Success! " + msg );
				var name = $(formname+" input").first().val();
				$(formname+" input").first().css("background","none repeat-x scroll 50% top #F2EA1D");
				window.setTimeout(function () { $(formname+" input").first().css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
				var list = $(formname).find('select').first();
				var toUpdateID;
				if (formname === "#updateSponsor") {
					toUpdateID = $('#sponsor').val();
					$('#sponsor-logo OPTION[value='+toUpdateID+']').html(name);
				}
				if (formname === "#update-sponsortype") {
					toUpdateID = $('#sponsortype').val();
					$('#sponsortype-type-new OPTION[value='+toUpdateID+']').html(name);
					$('#sponsortype-type-update OPTION[value='+toUpdateID+']').html(name);
				}
				if (formname === "#updateLocation") {
					toUpdateID = $('#screeninglocation').val();
					$('#locations-new OPTION[value='+toUpdateID+']').html(name);
					$('#locations-current OPTION[value='+toUpdateID+']').html(name);
					$('#venuelocations-new OPTION[value='+toUpdateID+']').html(name);
					$('#venuelocations-current OPTION[value='+toUpdateID+']').html(name);
				}
				if (formname === "#updateVenue") {
					toUpdateID = $('#screeningvenue').val();
				}
				if(formname === "#updateFestival") {
					var year = $('#year-current').val();
					$(toUpdate+" option:selected").text(year+" "+name);
					toUpdateID = $('#festival').val();
					$('#sec-festival-new OPTION[value='+toUpdateID+']').html(year+" "+name);
					$('#sec-festival-current OPTION[value='+toUpdateID+']').html(year+" "+name);
					$('#set-current-festival OPTION[value='+toUpdateID+']').html(year+" "+name);
					$('#set-current-festival2 OPTION[value='+toUpdateID+']').html(year+" "+name);
				} else if(formname === "#updateCompetitionFilms") {
					$(toUpdate).replaceWith(msg);
				} else if(formname === "#updateCompetition") {
					name = $('#comp-name-update').val();
					var festival = $('#comp-festival-update option:selected').text();
					var comp_id = $('#comp-competition-update option:selected').val();
					$(toUpdate+" option:selected").text(festival+' '+name);
					$('#compfilm-compname-new option[value='+comp_id+']').text(festival+' '+name);

					$(formname).find('input[type!=submit]').val("");
					$(formname).find('textarea').val("");
					$('#festival-name-update').val(festival);
				} else {
					$(toUpdate+" option:selected").text(name);
				}
			},
			complete: function(xhr, text) {
				// Only update the JSON object if we're updating Sponsor Types or Competition Films.
				if (formname == "#update-sponsortype") {
					$.ajax({
						success: function(msg, text, xhr) {
							window.sponsorTypeJSON = msg;
						},
						error: function(xhr, msg1, msg2){
							window.alert( "Failure! " + xhr + msg1 + msg2); },
						url: '/admin/film_settings/return_sptype_json/',
						type: 'POST',
						async: false,
						dataType: 'json'
					});
				} else if (formname == "#updateCompetition") {
					$.ajax({
						success: function(msg, text, xhr) {
							window.competitionJSON = msg;
						},
						error: function(xhr, msg1, msg2){
							window.alert( "Failure! " + xhr + msg1 + msg2); },
						url: '/admin/film_settings/return_competition_json/',
						type: 'POST',
						dataType: 'json'
					});
				} else if (formname == "#updateCompetitionFilms") {
					$.ajax({
						success: function(msg, text, xhr) {
							window.competitionfilmsJSON = msg;
						},
						error: function(xhr, msg1, msg2){
							window.alert( "Failure! " + xhr + msg1 + msg2); },
						url: '/admin/film_settings/return_competition_films_json/',
						type: 'POST',
						dataType: 'json'
					});
				}
			}, error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2);
			}
		});
	}
	return false;
}

// Film Settings - Set the current festival to be used in the rest of the site
function set_current_festival(formname) {
	var admin = 0;
	if (formname === "#setFestival") {
		admin = 1;
	} else {
		admin = 0;
	}

	$.ajax({
		data: $(formname).serialize(),
		dataType: "html",
		type: "POST",
		url: "/admin/film_settings/set_current_festival/"+admin,
		success: function(msg){
			//window.alert( "Success! " + msg );
			$(formname+" select").first().css("background","none repeat-x scroll 50% top #F2EA1D");
			window.setTimeout(function () { $(formname+" select").first().css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + xhr + msg1 + msg2);
		}
	});
	return false;
}

// Film Settings - Add a Film Type (with associated color)
function add_film_type(type, value, color) {
	var bkgcolor = "#F2EA1D";
	if (color !== 0) { bkgcolor = "#"+color; }

	$.ajax({
		type: "POST",
		url: "/admin/film_settings/add_type/"+type+"/",
		dataType: "html",
		data: $('#add-'+type).serialize(),
		success: function(msg){
			//window.alert( "Success! " + msg );
			$("#"+type+"-new").val("");
			$("#"+type+"-new").css("background","none repeat-x scroll 50% top "+bkgcolor);
			window.setTimeout(function () { $("#"+type+"-new").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			$("#"+type).append(msg);

			// Update video format on screening location tab as well
			if (type === "format") {
				$('#screeningformat-new').prepend(msg);
				$('#screeningformat-current').prepend(msg);
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Film Settings - Update a Film Type (with associated color)
function update_film_type(type, value, color, id) {
	var bkgcolor = "#F2EA1D";
	if (color !== 0) { bkgcolor = "#"+color; }

	$.ajax({
		type: "POST",
		url: "/admin/film_settings/update_type/"+type+"/",
		dataType: "html",
		data: $('#update-'+type).serialize(),
		success: function(msg){
			//window.alert( "Success! " + msg );
			$("#"+type+"-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
			window.setTimeout(function () { $("#"+type+"-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			if (color === "0") { } else { $("#"+type+" option[value="+id+"]").attr("title",color); }
			$("#"+type+" option:selected").text(value);

			if (type === "format") {
				//window.alert("got here! "+type+" "+value+" "+id);
				$('#screeningformat-new OPTION[value='+id+']').html(value);
				$('#screeningformat-current OPTION[value='+id+']').html(value);
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Film Settings - Delete a Film Type
// This function first checks to make sure value is not being used before deleting.
function delete_film_type(type, id) {
	var bkgcolor = "#F2EA1D";
	$.ajax({
		type: "POST",
		url: "/admin/film_settings/delete_type/"+type+"/",
		dataType: "html",
		data: $('#update-'+type).serialize(),
		success: function(msg){
			//window.alert( "Success! " + msg );
			if (msg === "0") {
				$("#"+type+" option:selected").remove();
				$("#"+type+"-current").val("");
				$("#"+type+"-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
				window.setTimeout(function () { $("#"+type+"-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			} else {
				//window.alert(msg);
				$("#setting-type").val(type);
				$("#todelete-id").val(id);
				$("#filmSettingReplacement").html(msg);
				$("#deleteFilmSetting-dialog").dialog("open");
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Film Settings - Delete a Section
// This function first checks to make sure value is not being used before deleting.
function delete_film_section(id) {
	var bkgcolor = "#F2EA1D";
	$.ajax({
		type: "POST",
		url: "/admin/film_settings/delete_section/"+id+"/",
		dataType: "html",
		data: $('#updateSection').serialize(),
		success: function(msg){
			if (msg === "0") {
				$("#section option:selected").remove();
				$("#sec-festival-current").val(0);
				$("#description-current").val("");
				$("#section-current").val("");
				$("#section-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
				window.setTimeout(function () { $("#section-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			} else {
				$("#section-todelete-id").val(id);
				$("#sectionReplacement").html(msg);
				$("#deleteSection-dialog").dialog("open");
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Film Settings - Delete a Screening Location
// This function first checks to make sure value is not being used before deleting.
function delete_screening_location(id) {
	var bkgcolor = "#F2EA1D";
	$.ajax({
		type: "POST",
		url: "/admin/film_settings/delete_location/"+id+"/",
		dataType: "html",
		data: $('#updateLocation').serialize(),
		success: function(msg){
			if (msg === "0") {
				$("#screeninglocation option:selected").remove();
				$("#screeninglocation-current").val("");
				$("#displayname-current").val("");
				$("#seats-current").val("");
				$("#screeningformat-current").find("option").attr("selected", false);
				$("#screeninglocation-current").css("background","none repeat-x scroll 50% top "+bkgcolor);
				window.setTimeout(function () { $("#screeninglocation-current").css("background","none repeat-x scroll 50% top #FFFFFF"); }, 2000);
			} else {
				$("#location-todelete-id").val(id);
				$("#locationReplacement").html(msg);
				$("#deleteLocation-dialog").dialog("open");
			}
		}, error: function(xhr, msg1, msg2){
			window.alert( "Failure! " + msg1);
		}
	});
	return false;
}

// Film Settings - Sets the value of a text input from a select.
function load_value() {
	var text = $("option:selected", this).text();
	var name = "#"+$(this).attr("name")+"-current";

	if (text !== "--") {
		$(name).val(text);
	} else {
		$(name).val('');
	}
}

// Film Settings - Sets the value of a text input and color from a select.
function load_value_color() {
	var text = $("option:selected", this).text();
	var name = "#"+$(this).attr("name")+"-current";
	var color = "#"+$("option:selected", this).attr("title");

	$('#colorSelector2 div').css('background-color',color);
	$('#format-color-update').val(color);

	if (text !== "--") {
		$(name).val(text);
	} else {
		$(name).val('');
	}
}

// Film Settings - Sets the value of Sponsor Type text inputs from a json object.
function load_value_sptype(id, json_obj) {
	if (id == "0") {
		$("#sponsortype-name-current").val("");
		$("#sponsortype-desc-current").val("");
		$("#sponsortype-order-current").val("");
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#sponsortype-name-current").val(element.name);
			$("#sponsortype-desc-current").val(element.description);
			$("#sponsortype-order-current").val(element.order);
			return false;
		}
	});
}

// Film Settings - Sets the value of Section text inputs from a json object.
function load_value_section(id, json_obj) {
	if (id == "0") {
		$("#section-current").val("");
		$("#sec-festival-current").val(0);
		//$("#sec-sponsor-current").val(0);
		$("#description-current").val("");
		$('#colorSelector2Section div').css('background-color','#000000');
		$('#section-color-update').val('000000');
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#section-current").val(element.name);
			$("#sec-festival-current").val(element.festival_id);
			//$("#sec-sponsor-current").val(element.sponsor_id);
			$("#description-current").val(element.Description);
			$('#colorSelector2Section div').css('background-color','#'+element.color);
			$('#section-color-update').val(element.color);

			return false;
		}
	});
}

// Film Settings - Sets the value of Location text inputs from a json object.
function load_value_location(id, json_obj) {
	if (id == "0") {
		$("#screeninglocation-current").val("");
		$("#displayname-current").val("");
		$("#seats-current").val("");
		$("#screeningformat-current option").each(function() { $(this).prop('selected',false); });
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#screeninglocation-current").val(element.name);
			$("#displayname-current").val(element.displayname);
			$("#seats-current").val(element.seats);
			var myObject = (element.format).toString().split(',');
			$("#screeningformat-current option").each(function() { $(this).prop('selected',false); });
			$("#screeningformat-current option").each(function() {
				for (var key in myObject) {
					if ($(this).val() === myObject[key]) {
						$(this).prop('selected',true);
					}
				}
			});
			return false;
		}
	});
}

// Film Settings - Sets the value of Venue text inputs from a json object.
function load_value_venue(id, json_obj) {
	if (id == "0") {
		$("#screeningvenue-current").val("");
		$("#venueaddress-current").val("");
		$("#venuecity-current").val("");
		$("#venuestate-current").val("");
		$("#venuezipcode-current").val("");
		$("#venuephone-current").val("");
		$("#venuelocations-current option").each(function() { $(this).prop('selected',false); });
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#screeningvenue-current").val(element.name);
			$("#venueaddress-current").val(element.address);
			$("#venuecity-current").val(element.city);
			$("#venuestate-current").val(element.state);
			$("#venuezipcode-current").val(element.postal_code);
			$("#venuephone-current").val(element.phone);
			var myObject = (element.location_ids).toString().split(',');
			$("#venuelocations-current option").each(function() { $(this).prop('selected',false); });
			$("#venuelocations-current option").each(function() {
				for (var key in myObject) {
					if ($(this).val() === myObject[key]) {
						$(this).prop('selected',true);
					}
				}
			});
			return false;
		}
	});
}


// Film Settings - Sets the value of Festival text inputs from a json object.
function load_value_festival(id, json_obj) {
	if (id == "0") {
		$("#festival-current").val("");
		$("#year-current").val("");
		$("#startdate-current").val("");
		$("#enddate-current").val("");
		$("#locations-current option").each(function() { $(this).prop('selected',false); });
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#festival-current").val(element.name);
			$("#year-current").val(element.year);
			$("#startdate-current").val(element.startdate);
			$("#enddate-current").val(element.enddate);
			var myObject = (element.locations).toString().split(',');
			$("#locations-current option").each(function() { $(this).prop('selected',false); });
			$("#locations-current option").each(function() {
				for (var key in myObject) {
					if ($(this).val() === myObject[key]) {
						$(this).prop('selected',true);
					}
				}
			});
			return false;
		}
	});
}

// Film Settings - Sets the value of Competition text inputs from a json object.
function load_value_competition(id, json_obj) {
	if (id == "0") {
		$("#comp-name-update").val("");
		$("#comp-festival-update").val(0);
		$("#comp-instructions-update").val("");
		return false;
	}

	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#comp-name-update").val(element.name);
			$("#comp-festival-update").val(element.festival_id);
			$("#comp-instructions-update").val(element.instructions);
			return false;
		}
	});
}

// Film Settings - Open the 'Edit/Delete Sponsor Logo' dialog and populate with information
function open_edit_logo_dialog(id, json_obj) {
	var sponsor_id = "";
	var sponsor_name = "";
	var sponsorlogo_type = "";
	var website_url = "";
	var logo_url = "";
	var films_sponsored = [];
	var to_update = [];

	// Loop through and find all of the films associated with this sponsorship, plus other data
	jQuery.each(json_obj, function (index, element) {
		if (id == element.sponsor_id) {
			sponsor_id = element.sponsor_id;
			sponsorlogo_type = element.sponsorlogo_id;
			sponsor_name = element.name;
			website_url = element.url_website;
			logo_url = element.url_logo;
			films_sponsored.push(element.movie_id);
			to_update.push(element.id);
		}
	});

	// Actually update the form fields
	$('#sponsor-logo-update').val(sponsor_id);
	$('#sponsorlogo-type-update').val(sponsorlogo_type);
	$('#websitelink-update').val(website_url);
	$('#currentlogo-url').val(logo_url);
	if (logo_url !== "") {
		$("#currentlogo-update").html("<img src='"+logo_url+"' width='150' />");
	} else {
		$("#currentlogo-update").html("None");
	}
	$('#sponsor-name-update').html(sponsor_name);
	$('#sponsored-films-update').val(films_sponsored);
	$('#sponsor-name').val(sponsor_name);
	$('#original-film-ids').val(films_sponsored);
	$('#logo-ids-to-update').val(to_update);

	// open the dialog box
	$('#editSponsorLogo-dialog').dialog('open');
}

// Film Settings - Open the 'Edit Competition Films' dialog and populate with information
function open_edit_competition_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id == element.id) {
			$("#comp-competition-film-update").val(element.id);
			$("#compfilm-compname-update").val(element.competition_id);
			$("#compfilm-filmname-update").val(element.movie_id);
			$("#compfilm-video-url-update").val(element.video_url);
			$("#compfilm-video-password-update").val(element.video_password);
		}
	});

	// open the dialog box
	$('#editCompetitionFilms-dialog').dialog('open');
}

// Film Settings - Remove a Competition Films
function delete_competition_film(compfilm_id, film_name) {
	var decision = window.confirm("Are you sure you really want to delete "+film_name+" from this competition?");
	if (decision === true) {
		$.ajax({
			success: function(msg, text, xhr) {
				$('#cf-del-'+compfilm_id).closest("tr").remove();
			},
			error: function(xhr, msg1, msg2){
				window.alert( "Failure! " + xhr + msg1 + msg2); },
			data: "compfilm_id="+compfilm_id,
			url: '/admin/film_settings/delete_competition_film/',
			type: 'POST',
			dataType: 'html'
		});
	}
}