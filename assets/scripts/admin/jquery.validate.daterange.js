/*
 * jQuery validation plug-in 1.7
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2008 Jörn Zaefferer
 *
 * $Id: jquery.validate.js 6403 2009-06-17 14:27:16Z joern.zaefferer $
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Date range code added 10/27/2011 - http://stackoverflow.com/questions/3761185/jquery-validate-date-range
 */

$.validator.addMethod('daterange', function(value, element, arg) {
    if (this.optional(element)) { return true; }

    var startDate = Date.parse(arg[0]),
        endDate = Date.parse(arg[1]),
        enteredDate = Date.parse(value);

    // If there is no date
    if(isNaN(enteredDate)) { return false; }
    // If date is before start date or after end date
    else if (enteredDate < startDate || enteredDate > endDate) { return false; }
    // If date is equal to end or start
    else if (enteredDate == startDate || enteredDate == endDate) { return true; }
    // If date is between start and end
    else if (enteredDate > startDate && enteredDate < endDate) { return true; }
}, $.validator.format("Please specify a date between {0} and {1}."));

$.validator.addMethod('greaterThan', function(value, element, params) {
	if (!/Invalid|NaN/.test(new Date(value))) {
		return new Date(value) >= new Date($(params).val());
	}
	//return isNaN(value) && isNaN($(params).val()) || (parseFloat(value) > parseFloat($(params).val())); 
}, $.validator.format("End Date cannot be before Start Date."));

