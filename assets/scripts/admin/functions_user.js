// Manage Users - Open the 'Edit/Delete User' dialog and populate with information
function open_edit_user_dialog(id, json_obj) {
	jQuery.each(json_obj, function (index, element) {
		if (id === element.id) {
			// Update the form fields
			$('#user-id-update').val(element.id);
			$('#last-login').val(element.LastLogin);
			$('#Username-update').val(element.Username);
			$('#FirstName-update').val(element.FirstName);
			$('#LastName-update').val(element.LastName);
			$('#Email-update').val(element.Email);
			
			if (element.Admin === "1") {
				$("#updateAccessLevel").val("Administrator").css("color","#00FF00");
				$("#updateSlider").slider("value", 75);
			} else if (element.LimitedAccess === "1") {
				$("#updateAccessLevel").val("Limited").css("color","#F6931F");
				$("#updateSlider").slider("value", 25);
			} else if (element.Enabled === "0") {
				$("#updateAccessLevel").val("Disabled").css("color","#FF0000");
				$("#updateSlider").slider("value", 0);
			} else {
				$("#updateAccessLevel").val("Normal").css("color","#F6931F");
				$("#updateSlider").slider("value", 50);
			}
		}
	});

	// open the dialog box
	$('.EditUsersDialog').dialog('open');
}