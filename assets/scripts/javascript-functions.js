	function wsiw_filter() {
		var sectionValue = $('#wsiw_section').val();
		var countryValue = $('#wsiw_country').val();
		var languageValue = $('#wsiw_language').val();
		var genreValue = $('#wsiw_genre').val();
		var eventtypeValue = $('#wsiw_eventtype').val();
		var classes = "";

		if (sectionValue != 0) { classes = classes+"."+sectionValue; }
		if (countryValue != 0) { classes = classes+"."+countryValue; }
		if (languageValue != 0) { classes = classes+"."+languageValue; }
		if (genreValue != 0) { classes = classes+"."+genreValue; }
		if (eventtypeValue != 0) { classes = classes+"."+eventtypeValue; }

		$('#wsiw').isotope({
			itemSelector: '.film',
			filter: classes
		});
	}

	function wsiw_reset() {
		$('#wsiw_section').val(0);
		$('#wsiw_country').val(0);
		$('#wsiw_language').val(0);
		$('#wsiw_genre').val(0);
		$('#wsiw_eventtype').val(0);
		$('#wsiw').isotope({
			itemSelector: '.film',
			filter: ''
		});
	}

	function film_filter_redirect(type, value) {
		if (value !== 0) {
			window.location = '/films/'+type+'/'+value;
		} else {
			return false;
		}
	}

	function schedule_filter_redirect(type, value) {
		if (value !== 0) {
			window.location = '/schedule/'+type+'/'+value;
		} else {
			return false;
		}
	}

	function change_festival_redirect(value) {
		if (value !== 0) {
			window.location = '/redirect/change_festival/'+value;
		} else {
			return false;
		}
	}