<?php
/*
Page:           _config-rating.php
Created:        Aug 2006
Last Mod:       Mar 18 2007
Holds info for connecting to the db, and some other vars
--------------------------------------------------------- 
ryan masuga, masugadesign.com
ryan@masugadesign.com 
Licensed under a Creative Commons Attribution 3.0 License.
http://creativecommons.org/licenses/by/3.0/
See readme.txt for full credit details.
--------------------------------------------------------- */

	// Pull in CodeIgniter database configuration details
	define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__)))));
	require(__ROOT__.'/application/config/database.php');

	//Connect to  your rating database
	$rating_dbhost        = $ci_hostname;
	$rating_dbuser        = $ci_username;
	$rating_dbpass        = $ci_password;
	$rating_dbname        = $ci_database;
	$rating_tableName     = 'wf_movie_ratings';
	$rating_path_db       = '/assets/scripts/ajax_star_rater/db.php'; // the path to your db.php file
	$rating_path_rpc      = '/assets/scripts/ajax_star_rater/rpc.php'; // the path to your rpc.php file
	
	$rating_unitwidth     = 30; // the width (in pixels) of each rating unit (star, etc.)
	// if you changed your graphic to be 50 pixels wide, you should change the value above
	
$rating_conn = mysql_connect($rating_dbhost, $rating_dbuser, $rating_dbpass) or die  ('Error connecting to mysql');
	//mysql_select_db($rating_dbname);
?>